<p align="center">
<img src="https://docs.sciveyor.com/images/icon-orange.svg" width="20%" height="auto" alt="Sciveyor Logo">
</p>

# Sciveyor Solr Example

This repository contains sample data and some scripts you can use to maintain a sample Solr instance for use in developing [Sciveyor.](https://www.sciveyor.com)

To use these, you'll need to have:

- Docker, installed and accessible via `sudo`
- MongoDB, installed and accessible to your local user
- [Sciveyor's `sciveyor-tool`](https://codeberg.org/sciveyor/sciveyor-tool), installed and compiled
- curl

Note that this process will create a MongoDB database called `sciveyor-dev` and a Solr data directory in `./solrdata`. Both of these are meant to be automatically managed, and will be reset, deleted, or changed by these scripts without warning!

## Scripts

- Create a new Docker container: `./create`

  This will spin up a new Docker container using our [Solr Docker image](https://codeberg.org/sciveyor/solr-docker).

- Start the Docker container: `./start`

  This will start the created Docker container, if it's not already running.

- Stop the Docker container: `./stop`

  This will stop the created Docker container, if running.

- Delete the Docker container: `./delete`

  This will delete the created Docker container, if it exists.

- Load data into the Docker container: `./load $MONGO_TOOL`

  This will load data into the created Docker container from the `data` folder, via the given path to an installed copy of `mongo-tool`.

## Versions

If you're looking for a Solr instance that works with:

- Sciveyor 1.x (version of Sciveyor starting in 2021): the **main** branch of this repository (you are here!)
- RLetters 3.x (versions of RLetters starting in May, 2018): [another repository](https://codeberg.org/rletters/solr-example-3.0)
- RLetters 2.x (versions of RLetters before May, 2018): [another repository](https://codeberg.org/rletters/solr-example-2.0)

## Licenses

The data here is taken from two sources: the [PLoS Neglected Tropical Diseases journal](http://journals.plos.org/plosntds/), available under the [CC-BY license,](https://creativecommons.org/licenses/by/2.0/) and from [Project Gutenberg,](https://www.gutenberg.org/) available under the [Project Gutenberg license.](https://www.gutenberg.org/wiki/Gutenberg:The_Project_Gutenberg_License)

Other code and scripts are available under the MIT License.
