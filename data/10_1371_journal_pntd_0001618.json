{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001618",
  "doi": "10.1371/journal.pntd.0001618",
  "externalIds": [
    "pii:PNTD-D-12-00028"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Untargeted Metabolomics Reveals a Lack Of Synergy between Nifurtimox and Eflornithine against Trypanosoma brucei",
  "authors": [
    {
      "name": "Isabel M. Vincent",
      "first": "Isabel M.",
      "last": "Vincent",
      "affiliation": "The Wellcome Trust Centre for Molecular Parasitology, Institute for Infection, Immunity and Inflammation, College of Medical, Veterinary and Life Sciences, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Darren J. Creek",
      "first": "Darren J.",
      "last": "Creek",
      "affiliation": "The Wellcome Trust Centre for Molecular Parasitology, Institute for Infection, Immunity and Inflammation, College of Medical, Veterinary and Life Sciences, University of Glasgow, Glasgow, United Kingdom; Glasgow Polyomics Facility, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Karl Burgess",
      "first": "Karl",
      "last": "Burgess",
      "affiliation": "The Wellcome Trust Centre for Molecular Parasitology, Institute for Infection, Immunity and Inflammation, College of Medical, Veterinary and Life Sciences, University of Glasgow, Glasgow, United Kingdom; Glasgow Polyomics Facility, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Debra J. Woods",
      "first": "Debra J.",
      "last": "Woods",
      "affiliation": "Pfizer Animal Health, Pfizer Inc, Kalamazoo, Michigan, United States of America"
    },
    {
      "name": "Richard J. S. Burchmore",
      "first": "Richard J. S.",
      "last": "Burchmore",
      "affiliation": "The Wellcome Trust Centre for Molecular Parasitology, Institute for Infection, Immunity and Inflammation, College of Medical, Veterinary and Life Sciences, University of Glasgow, Glasgow, United Kingdom; Glasgow Polyomics Facility, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Michael P. Barrett",
      "first": "Michael P.",
      "last": "Barrett",
      "affiliation": "The Wellcome Trust Centre for Molecular Parasitology, Institute for Infection, Immunity and Inflammation, College of Medical, Veterinary and Life Sciences, University of Glasgow, Glasgow, United Kingdom; Glasgow Polyomics Facility, University of Glasgow, Glasgow, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-05",
  "dateAccepted": "2012-03-05",
  "dateReceived": "2012-12-30",
  "volume": "6",
  "number": "5",
  "pages": "e1618",
  "tags": [
    "Biology"
  ],
  "abstract": "A non-targeted metabolomics-based approach is presented that enables the study of pathways in response to drug action with the aim of defining the mode of action of trypanocides. Eflornithine, a polyamine pathway inhibitor, and nifurtimox, whose mode of action involves its metabolic activation, are currently used in combination as first line treatment against stage 2, CNS-involved, human African trypanosomiasis (HAT). Drug action was assessed using an LC-MS based non-targeted metabolomics approach. Eflornithine revealed the expected changes to the polyamine pathway as well as several unexpected changes that point to pathways and metabolites not previously described in bloodstream form trypanosomes, including a lack of arginase activity and N-acetylated ornithine and putrescine. Nifurtimox was shown to be converted to a trinitrile metabolite indicative of metabolic activation, as well as inducing changes in levels of metabolites involved in carbohydrate and nucleotide metabolism. However, eflornithine and nifurtimox failed to synergise anti-trypanosomal activity in vitro, and the metabolomic changes associated with the combination are the sum of those found in each monotherapy with no indication of additional effects. The study reveals how untargeted metabolomics can yield rapid information on drug targets that could be adapted to any pharmacological situation.",
  "fullText": "Introduction Human African trypanosomiasis (HAT) is a parasitic infection in sub-Saharan Africa transmitted by tsetse flies. Its causative agent is the flagellated protozoan Trypanosoma brucei, with two sub-species, T. b. gambiense and T. b. rhodesiense responsible for human disease [1], [2]. There are five drugs in use against HAT. Of these five, only eflornithine has a confirmed mode of action (MOA), namely, inhibition of ornithine decarboxylase (ODC) [3], [4] with concomitant perturbation of the polyamine pathway. In addition to the four licensed drugs, nifurtimox has been recommended by the World Health Organisation for use against late-stage disease in combination with eflornithine [5]. The MOA for nifurtimox has, however, yet to be fully elucidated. For many years it was presumed to exert its action through the generation of oxidative stress associated with reduction of the nitro group with subsequent reduction of oxygen to toxic reactive oxygen species [6], [7]. In trypanosomes polyamines serve an unusual role in combining with glutathione to create the metabolite trypanothione [8], which carries out many of the cellular roles usually attributed to glutathione in other cell types, including protection against oxidative stress. This indicated that eflornithine, which inhibits polyamine biosynthesis [9], [10] and subsequently trypanothione biosynthesis, would synergise with nifurtimox as result of a reduced ability of cells to deal with oxidative stress. However, the data that lead to the conclusion that nifurtimox causes oxidative stress is inconclusive [7] and recent evidence shows that nifurtimox is activated upon metabolism to an open chain nitrile [11] and that this nitrile is as toxic as the parent drug. In mice there was no indication that either drug enhanced uptake of the other into brain [12], indeed eflornithine diminished brain penetration of nifurtimox in short term uptake assays. Moreover, isobologram analysis indicated that the two drugs were not synergistic in vitro [13]. It is very rare for a new chemotherapeutic agent to be licensed without prior knowledge of its MOA. In 2009, 19 drugs were approved by the FDA's centre for drug evaluation and research in the US, only one of which had a wholly unknown MOA [14]. A knowledge of the MOA reduces the risk of unexpected toxicity and allows synergism and resistance mechanisms to be predicted. Currently, the MOA of a drug is predicted using expensive and time-consuming enzyme-based assays, followed by targeted analyses of whether cellular death is associated with changes consistent with loss of the predicted target. Metabolomics is a relatively new technology that enables the simultaneous identification of hundreds of metabolites within a given system. In principle, if an enzyme is inhibited by a drug then the concentration of substrate should rise within a system and the concentration of product fall. We have recently introduced metabolomics approaches to investigate metabolism in trypanosomes [15]–[19]. Here we use our metabolomics platform to test the mode of action of eflornithine (an ornithine decarboxylase suicide inhibitor that has had its MOA validated) and nifurtimox (a drug for which the MOA is incompletely understood). The combination therapy was also"
}