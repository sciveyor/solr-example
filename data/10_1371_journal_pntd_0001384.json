{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001384",
  "doi": "10.1371/journal.pntd.0001384",
  "externalIds": [
    "pii:PNTD-D-11-00504"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Accuracy of Urine Circulating Cathodic Antigen (CCA) Test for Schistosoma mansoni Diagnosis in Different Settings of Côte d'Ivoire",
  "authors": [
    {
      "name": "Jean T. Coulibaly",
      "first": "Jean T.",
      "last": "Coulibaly",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland; Unité de Formation et de Recherche (UFR) Biosciences, Université de Cocody, Abidjan, Côte d'Ivoire; Centre Suisse de Recherches Scientifiques en Côte d'Ivoire, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Stefanie Knopp",
      "first": "Stefanie",
      "last": "Knopp",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Nicaise A. N'Guessan",
      "first": "Nicaise A.",
      "last": "N'Guessan",
      "affiliation": "Unité de Formation et de Recherche (UFR) Biosciences, Université de Cocody, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Kigbafori D. Silué",
      "first": "Kigbafori D.",
      "last": "Silué",
      "affiliation": "Unité de Formation et de Recherche (UFR) Biosciences, Université de Cocody, Abidjan, Côte d'Ivoire; Centre Suisse de Recherches Scientifiques en Côte d'Ivoire, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Thomas Fürst",
      "first": "Thomas",
      "last": "Fürst",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Laurent K. Lohourignon",
      "first": "Laurent K.",
      "last": "Lohourignon",
      "affiliation": "Unité de Formation et de Recherche (UFR) Biosciences, Université de Cocody, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Jean K. Brou",
      "first": "Jean K.",
      "last": "Brou",
      "affiliation": "Centre Hospitalier et Universitaire de Cocody, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Yve K. N'Gbesso",
      "first": "Yve K.",
      "last": "N'Gbesso",
      "affiliation": "Centre de Santé Rural d'Azaguié, Departement d'Agboville, Azaguié, Côte d'Ivoire"
    },
    {
      "name": "Penelope Vounatsou",
      "first": "Penelope",
      "last": "Vounatsou",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Eliézer K. N'Goran",
      "first": "Eliézer K.",
      "last": "N'Goran",
      "affiliation": "Unité de Formation et de Recherche (UFR) Biosciences, Université de Cocody, Abidjan, Côte d'Ivoire; Centre Suisse de Recherches Scientifiques en Côte d'Ivoire, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Jürg Utzinger",
      "first": "Jürg",
      "last": "Utzinger",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-11",
  "dateAccepted": "2011-09-18",
  "dateReceived": "2011-05-30",
  "volume": "5",
  "number": "11",
  "pages": "e1384",
  "tags": [
    "Diagnostic medicine",
    "Epidemiology",
    "Global health",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Public Health and Epidemiology",
    "Public health"
  ],
  "abstract": "Background\n          \nPromising results have been reported for a urine circulating cathodic antigen (CCA) test for the diagnosis of Schistosoma mansoni. We assessed the accuracy of a commercially available CCA cassette test (designated CCA-A) and an experimental formulation (CCA-B) for S. mansoni diagnosis.\n\n          Methodology\n          \nWe conducted a cross-sectional survey in three settings of Côte d'Ivoire: settings A and B are endemic for S. mansoni, whereas S. haematobium co-exists in setting C. Overall, 446 children, aged 8–12 years, submitted multiple stool and urine samples. For S. mansoni diagnosis, stool samples were examined with triplicate Kato-Katz, whereas urine samples were tested with CCA-A. The first stool and urine samples were additionally subjected to an ether-concentration technique and CCA-B, respectively. Urine samples were examined for S. haematobium using a filtration method, and for microhematuria using Hemastix dipsticks.\n\n          Principal Findings\n          \nConsidering nine Kato-Katz as diagnostic ‘gold’ standard, the prevalence of S. mansoni in setting A, B and C was 32.9%, 53.1% and 91.8%, respectively. The sensitivity of triplicate Kato-Katz from the first stool and a single CCA-A test was 47.9% and 56.3% (setting A), 73.9% and 69.6% (setting B), and 94.2% and 89.6% (setting C). The respective sensitivity of a single CCA-B was 10.4%, 29.9% and 75.0%. The ether-concentration technique showed a low sensitivity for S. mansoni diagnosis (8.3–41.0%). The specificity of CCA-A was moderate (76.9–84.2%); CCA-B was high (96.7–100%). The likelihood of a CCA-A color reaction increased with higher S. mansoni fecal egg counts (odds ratio: 1.07, p&lt;0.001). A concurrent S. haematobium infection or the presence of microhematuria did not influence the CCA-A test results for S. mansoni diagnosis.\n\n          Conclusion/Significance\n          \nCCA-A showed similar sensitivity than triplicate Kato-Katz for S. mansoni diagnosis with no cross-reactivity to S. haematobium and microhematuria. The low sensitivity of CCA-B in our study area precludes its use for S. mansoni diagnosis.",
  "fullText": "Introduction There is growing awareness, political commitment, and financial resources to control neglected tropical diseases (NTDs) [1]–[3]. Preventive chemotherapy, that is the repeated large-scale administration of drugs to at-risk populations, has become the key strategy for the control of several NTDs, including schistosomiasis [3]–[5]. Although the issue of diagnosis has received only token attention in the current era of preventive chemotherapy, its importance must be emphasized for rapid identification of high-risk communities warranting regular treatment, appraisal of drug efficacy, monitoring progress of control interventions, and improved patient management [6]–[8]. With regard to intestinal schistosomiasis due to Schistosoma mansoni and S. japonicum, the Kato-Katz technique is the most widely used diagnostic approach in epidemiological surveys [8], [9]. Although the Kato-Katz technique is relatively simple to perform, it requires a minimum of equipment (i.e., microscope, chemicals, and test kit material) and well-trained laboratory technicians [10]. Moreover, a shortcoming of the Kato-Katz technique is the only low-to-moderate sensitivity for S. mansoni diagnosis in low endemicity areas [11]–[13]. Hence, multiple Kato-Katz thick smears are required to enhance sensitivity [14], but this poses operational challenges and strains financial resources. The detection of circulating antigen of S. mansoni in urine has been suggested as an alternative to the Kato-Katz technique [15]–[17]. Indeed, both circulating anodic antigen (CAA) and circulating cathodic antigen (CCA) can be detected in sera and urine of individuals infected with S. mansoni [18]. Both antigen-detecting assays are sensitive and specific and correlate with the presence and intensity of infection [19]. Antigen detection in urine using a rapid diagnostic test (RDT) based on an enzyme-linked immunosorbent assay (ELISA) technique is potentially useful and non-invasive and could change the management of infected individuals, particularly at the peripheral level in endemic countries where microscopes and qualified laboratory technicians are often not available [6], [7]. A point-of-contact (POC) CCA urine test has been developed for the diagnosis of S. mansoni [15], which is now commercially available as a RDT in cassette form. In view of promising results obtained thus far [17], [20], [21], the Schistosomiasis Consortium for Operational Research and Evaluation (SCORE) initiated a multi-country study to assess the accuracy of a commercially available CCA cassette test for the diagnosis of S. mansoni. The study reported here is part of this multi-country evaluation. We assessed the accuracy of a commercially available urine CCA cassette test (designated CCA-A) for S. mansoni diagnosis. Additionally, we employed an experimental formulation of the test (CCA-B). Nine Kato-Katz thick smears from each participant served as diagnostic ‘gold’ standard. In addition, our team employed the ether-concentration method on sodium acetate-acetic acid-formalin (SAF)-fixed stool samples for the diagnosis of S. mansoni, urine filtration for the identification of S. haematobium eggs, and Hemastix dipsticks for the detection of microhematuria in urine. The study was carried out in south Côte d'Ivoire, in three settings where S. mansoni is endemic at different levels, whereas S. haematobium co-exists in one of the settings. Methods Ethics Statement The study protocol was approved by the institutional research commission of the"
}