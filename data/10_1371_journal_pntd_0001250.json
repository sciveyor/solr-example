{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001250",
  "doi": "10.1371/journal.pntd.0001250",
  "externalIds": [
    "pii:PNTD-D-11-00334"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Congenital Chagas Disease: Recommendations for Diagnosis, Treatment and Control of Newborns, Siblings and Pregnant Women",
  "authors": [
    {
      "name": "Yves Carlier",
      "first": "Yves",
      "last": "Carlier",
      "affiliation": "Laboratoire de Parasitologie, Faculté de Médecine, Université Libre de Bruxelles, Brussels, Belgium"
    },
    {
      "name": "Faustino Torrico",
      "first": "Faustino",
      "last": "Torrico",
      "affiliation": "Facultad de Medicina, Universidad Mayor de San Simon, Cochabamba, Bolivia"
    },
    {
      "name": "Sergio Sosa-Estani",
      "first": "Sergio",
      "last": "Sosa-Estani",
      "affiliation": "Centro Nacional de Diagnóstico e Investigación de Endemo-epidemias, Buenos Aires, Argentina"
    },
    {
      "name": "Graciela Russomando",
      "first": "Graciela",
      "last": "Russomando",
      "affiliation": "Instituto de Investigaciones en Ciencias de la Salud, Universidad Nacional de Asunción, Rio de la Plata y Lagerenza, Asunción, Paraguay"
    },
    {
      "name": "Alejandro Luquetti",
      "first": "Alejandro",
      "last": "Luquetti",
      "affiliation": "Laboratório de Pesquisa da doença de Chagas, Hospital das Clinicas, Universidade Federal de Goias, Goiania, Brazil"
    },
    {
      "name": "Hector Freilij",
      "first": "Hector",
      "last": "Freilij",
      "affiliation": "Programa Nacional de Chagas (Ministerio de Salud Pública) y Servicio de Parasitología y Chagas (Hospital de Niños Ricardo Gutiérrez), Buenos Aires, Argentina"
    },
    {
      "name": "Pedro Albajar Vinas",
      "first": "Pedro",
      "last": "Albajar Vinas",
      "affiliation": "Department of Control of Neglected Tropical Diseases, World Health Organization, Geneva, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-10",
  "volume": "5",
  "number": "10",
  "pages": "e1250",
  "tags": [
    "Biology",
    "Medicine"
  ],
  "fullText": "In May 2010, the sixty-third World Health Assembly adopted resolution WHA63.20 on the control and elimination of Chagas disease, highlighting the need “to promote the development of public health measures in disease-endemic and disease non-endemic countries, with special focus on endemic areas, for the early diagnosis of congenital transmission and management of cases” [1]. This article summarizes the recommendations of the Technical Group IVa on “Prevention and Control of Congenital Transmission and Case Management of Congenital Infections” of the World Health Organization's Programme on Control of Chagas disease (infection with Trypanosoma cruzi). The present recommendations derive from those obtained in the meetings listed in Box 1. Box 1. Meetings from Which the Present Recommendations Derive Meeting ULB (Belgium)/UMSS (Bolivia)), Cochabamba, Bolivia, November 6–8, 2002: “Congenital Infection with Trypanosoma cruzi: From Mechanisms of Transmission to Strategies for Diagnosis And Control”, Carlier Y and Torico F, Revista da Sociedade Brasileira de Medicina Tropical 2003, 6: 767–771. Meeting PAHO/CLAP/ULB (Belgium)/IRD (France), Montevideo, Uruguay, June 24–25, 2004: “Congenital Chagas Disease: Its Epidemiology and Management”, http://www.paho.org/English/AD/DPC/CD/dch-chagas-congenita-2004.htm Meeting PAHO/CLAP/ULB (Belgium), Montevideo, Uruguay, May 17–18, 2007: “Information, Education and Communication in Congenital Chagas Disease”, http://www.paho.org/English/AD/DPC/CD/dch-congenita-iec-07.doc Meeting WHO, Geneva, Switzerland, July 4–6, 2007: “Revisiting Chagas Disease: From a Latin American Health Perspective to a Global Health Perspective” Meeting of the WHO TG IVa (congenital and paediatric Chagas disease), New Orleans, Louisiana, United States, December 11, 2008, satellite meeting to the ASTMH 57th annual meeting Meeting of the 6th European Congress of Tropical Medicine and International Health, Verona, Italy, September 6–10, 2009: “Chagas Disease in Europe” Meeting of WHO-HQ and the WHO regional office for Europe, Geneva, Switzerland, December 17–18, 2009: “Consultation on Chagas Disease in Europe” Preliminary Considerations on Congenital Transmission of T. cruzi Infection Congenital transmission of T. cruzi infection is considered as such (i) when a neonate is born from an infectious mother (that is, a mother with positive serology or T. cruzi parasites circulating in the blood), and (ii) when T. cruzi parasites are identified at birth, or (iii) when T. cruzi parasites or specific antibodies not of maternal origin are detected after birth, and when previous transmission to infant by vectors and blood transfusion has been ruled out [2], [3]. Congenital transmission occurs in areas where the disease is endemic as well as in areas where vector transmission has been interrupted, in areas where the disease is non-endemic, and from one generation to another. This pattern of transmission facilitates uncontrolled spread of the parasite infection for long periods of time [2], [3]. According to epidemiological data from Latin America, the estimated number of cases of congenital T. cruzi infection is &gt;15,000 per year [4]. The incidence of congenital cases in non-endemic areas is not known, although several reports attest to its occurrence [5], [6]. Although some congenital cases can present non-specific symptoms as is seen in congenital infections with Toxoplasma gondii, rubella virus, cytomegalovirus, and herpes simplex virus (commonly identified in the acronym TORCH), most cases are asymptomatic. This warrants a control strategy of"
}