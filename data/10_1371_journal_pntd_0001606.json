{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001606",
  "doi": "10.1371/journal.pntd.0001606",
  "externalIds": [
    "pii:PNTD-D-11-00727"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Genetic and Anatomic Determinants of Enzootic Venezuelan Equine Encephalitis Virus Infection of Culex (Melanoconion) taeniopus",
  "authors": [
    {
      "name": "Joan L. Kenney",
      "first": "Joan L.",
      "last": "Kenney"
    },
    {
      "name": "A. Paige Adams",
      "first": "A. Paige",
      "last": "Adams"
    },
    {
      "name": "Rodion Gorchakov",
      "first": "Rodion",
      "last": "Gorchakov"
    },
    {
      "name": "Grace Leal",
      "first": "Grace",
      "last": "Leal"
    },
    {
      "name": "Scott C. Weaver",
      "first": "Scott C.",
      "last": "Weaver",
      "affiliation": "Institute for Human Infections and Immunity, Center for Tropical Diseases, and Department of Pathology, University of Texas Medical Branch, Galveston, Texas, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-04",
  "dateAccepted": "2012-02-27",
  "dateReceived": "2011-07-23",
  "volume": "6",
  "number": "4",
  "pages": "e1606",
  "tags": [
    "Biology",
    "Ecology",
    "Microbiology",
    "Vector biology",
    "Viral transmission and infection",
    "Virology"
  ],
  "abstract": "Venezuelan equine encephalitis (VEE) is a re-emerging, mosquito-borne viral disease with the potential to cause fatal encephalitis in both humans and equids. Recently, detection of endemic VEE caused by enzootic strains has escalated in Mexico, Peru, Bolivia, Colombia and Ecuador, emphasizing the importance of understanding the enzootic transmission cycle of the etiologic agent, VEE virus (VEEV). The majority of work examining the viral determinants of vector infection has been performed in the epizootic mosquito vector, Aedes (Ochlerotatus) taeniorhynchus. Based on the fundamental differences between the epizootic and enzootic cycles, we hypothesized that the virus-vector interaction of the enzootic cycle is fundamentally different from that of the epizootic model. We therefore examined the determinants for VEEV IE infection in the enzootic vector, Culex (Melanoconion) taeniopus, and determined the number and susceptibility of midgut epithelial cells initially infected and their distribution compared to the epizootic virus-vector interaction. Using chimeric viruses, we demonstrated that the determinants of infection for the enzootic vector are different than those observed for the epizootic vector. Similarly, we showed that, unlike A. taeniorhynchus infection with subtype IC VEEV, C. taeniopus does not have a limited subpopulation of midgut cells susceptible to subtype IE VEEV. These findings support the hypothesis that the enzootic VEEV relationship with C. taeniopus differs from the epizootic virus-vector interaction in that the determinants appear to be found in both the nonstructural and structural regions, and initial midgut infection is not limited to a small population of susceptible cells.",
  "fullText": "Introduction Venezuelan equine encephalitis virus (VEEV) has been recognized as an etiologic agent of neurologic disease in humans and equids for nearly 80 years. Closely related to eastern (EEEV) and western equine encephalitis viruses (WEEV), VEEV belongs to the family Togaviridae, genus Alphavirus. First recognized in the 1920s, Venezuelan equine encephalitis (VEE) outbreaks are typically episodic with several years elapsing between outbreaks. However, when outbreaks do occur, they can cause severe and sometimes fatal disease in hundreds-of-thousands of equids and humans. For instance, after an interval of 19 years with no documented cases between 1973 and 1992, clusters of cases emerged in Venezuela [1] and Chiapas, Mexico [2] prior to a major outbreak involving ca. 100,000 people in 1995 [3]. In general, disease manifestations of VEE range from flu-like illness to fatal encephalitis. It is estimated that central nervous system (CNS) involvement occurs in 4–14% of human cases, and children are at the greatest risk to develop encephalitis and to die from infection [4]. Of the four subtypes of VEEV, IC and IAB are considered epizootic as they are known to cause disease in horses, to use these hosts for amplification, and are also capable of utilizing a variety of epizootic mosquito vectors, such as Aedes (Ochlerotatus) taeniorhynchus, A. (Och.) sollicitans, Psorophora confinnis, Culex (Deinocerites) pseudes, Mansonia indubitans, and M. titillans, among others [5]–[10]. Many of these mosquitoes thrive near coastal brackish water, can fly long distances from larval sites, prefer to feed on humans or other large mammals, and can tolerate feeding in sunny areas, although they may rest in shaded sites. In contrast, enzootic VEEV subtypes IE and ID generally cause little or no viremia or disease in equids, but like the epizootic strains, can cause fatal disease in humans [11]–[14]. Mosquito vectors that maintain these enzootic viruses in nature include a variety of species within the Spissipes section of the subgenus Culex (Melanoconion), and subtype IE strains specifically utilize C. (Mel.) taeniopus. The enzootic cycle typically occurs in shaded, intact forests with stable pools of water that are available for larval development. Some larvae also require the presence of a specific aquatic plant (i.e., Pistia spp.) for respiration [15]. Recent identification of extensive endemic disease in Peru, Bolivia, Ecuador, Colombia and Mexico, caused by spillover of enzootic strains in subtypes ID and IE, indicates the importance of VEEV as a continuous public health threat in Central and South America [16], [17]. The recent documentation of widespread endemic disease is likely associated with increased surveillance as well as the clearing of sylvatic forest habitats to accommodate the expansion of agricultural land types in areas of Latin America where enzootic VEEV persists [18]–[20]. The resulting fragmentation of sylvatic habitats results in an increase in ecotones that can support the life cycle of enzootic VEEV mosquito vectors [21], which also increases the likelihood of an enzootic VEEV strain adapting to epizootic transmission [22]. Enzootic ID strains are known to be a source for the emergence of epizootic IC strains and"
}