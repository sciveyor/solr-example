{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001306",
  "doi": "10.1371/journal.pntd.0001306",
  "externalIds": [
    "pii:PNTD-D-11-00387"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Caspase Dependent Programmed Cell Death in Developing Embryos: A Potential Target for Therapeutic Intervention against Pathogenic Nematodes",
  "authors": [
    {
      "name": "Alok Das Mohapatra",
      "first": "Alok Das",
      "last": "Mohapatra",
      "affiliation": "Department of Infectious Disease Biology, Institute of Life Sciences, DBT, Ministry of Science and Technology, Government of India, Bhubaneswar, India"
    },
    {
      "name": "Sunil Kumar",
      "first": "Sunil",
      "last": "Kumar",
      "affiliation": "Department of Infectious Disease Biology, Institute of Life Sciences, DBT, Ministry of Science and Technology, Government of India, Bhubaneswar, India"
    },
    {
      "name": "Ashok Kumar Satapathy",
      "first": "Ashok Kumar",
      "last": "Satapathy",
      "affiliation": "Department of Applied Immunology, Regional Medical Research Centre, Indian Council of Medical Research, Government of India, Bhubaneswar, India"
    },
    {
      "name": "Balachandran Ravindran",
      "first": "Balachandran",
      "last": "Ravindran",
      "affiliation": "Department of Infectious Disease Biology, Institute of Life Sciences, DBT, Ministry of Science and Technology, Government of India, Bhubaneswar, India"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-09",
  "dateAccepted": "2011-07-21",
  "dateReceived": "2011-04-23",
  "volume": "5",
  "number": "9",
  "pages": "e1306",
  "tags": [
    "Biology",
    "Medicine",
    "Veterinary science"
  ],
  "abstract": "Background\n          \nSuccessful embryogenesis is a critical rate limiting step for the survival and transmission of parasitic worms as well as pathology mediated by them. Hence, blockage of this important process through therapeutic induction of apoptosis in their embryonic stages offers promise for developing effective anti-parasitic measures against these extra cellular parasites. However, unlike in the case of protozoan parasites, induction of apoptosis as a therapeutic approach is yet to be explored against metazoan helminth parasites.\n\n          Methodology/Principal Findings\n          \nFor the first time, here we developed and evaluated flow cytometry based assays to assess several conserved features of apoptosis in developing embryos of a pathogenic filarial nematode Setaria digitata, in-vitro as well as ex-vivo. We validated programmed cell death in developing embryos by using immuno-fluorescence microscopy and scoring expression profile of nematode specific proteins related to apoptosis [e.g. CED-3, CED-4 and CED-9]. Mechanistically, apoptotic death of embryonic stages was found to be a caspase dependent phenomenon mediated primarily through induction of intracellular ROS. The apoptogenicity of some pharmacological compounds viz. DEC, Chloroquine, Primaquine and Curcumin were also evaluated. Curcumin was found to be the most effective pharmacological agent followed by Primaquine while Chloroquine displayed minimal effect and DEC had no demonstrable effect. Further, demonstration of induction of apoptosis in embryonic stages by lipid peroxidation products [molecules commonly associated with inflammatory responses in filarial disease] and demonstration of in-situ apoptosis of developing embryos in adult parasites in a natural bovine model of filariasis have offered a framework to understand anti-fecundity host immunity operational against parasitic helminths.\n\n          Conclusions/Significance\n          \nOur observations have revealed for the first time, that induction of apoptosis in developing embryos can be a potential approach for therapeutic intervention against pathogenic nematodes and flow cytometry can be used to address different issues of biological importance during embryogenesis of parasitic worms.",
  "fullText": "Introduction Helminth infections account for the highest burden of neglected tropical diseases [NTD], which afflict the most impoverished population of the world. About two billion people are presently infected with these parasites while many more people living in endemic areas are at risk of acquiring these infections [1]-[3]. Chronic diseases caused by such metazoan parasites often inflict crippling morbidity and debilitating disability with profound economic, social and political consequences [1]. Accumulating evidence in literature suggests that coinfection with these helminth parasites increases susceptibility to or worsens progression of three major infectious diseases- HIV/AIDS, tuberculosis and malaria [4], [5]. The above facts accentuate the need for launching a global assault on parasitic worms. However, our ability to control diseases caused by these class of parasites is constrained by several factors including a limited repertoire of sub-optimal drugs and paucity of robust tools to investigate biology of nematode parasites [6]. The burden of human helminthiasis is mostly attributed to high prevalence diseases caused by pathogenic nematodes i.e. by intestinal nematodes - Ascariasis [807 million], Trichuriasis [604 million], Hook worm infections [574 million] and filarial nematodes - Lymphatic filariasis [120 million] and Onchocerciasis [37 million] respectively[1], [7]. Preventive chemotherapy through MDA programs is the mainstay for treatment and control of diseases caused by nematode pathogens, at present. However, constraints of currently available therapies including high cost, low therapeutic efficacy, rapid reinfection after treatment, poor safety profiles and patient compliance and emerging or existing drug resistance coupled with lack of robust biomarkers for detection of resistance of nematode parasites to the mainstay drugs of MDA programs etc. limit the utility of existing drugs [4], [8]–[12]. The problem is further compounded by the fact that none of the existing drugs are effective against all the life stages of the parasitic worms and almost all of them are remarkably ineffective against adult stage parasites [6], [8]. Additionally, poor understanding of the mode of action, pharmacology [6], [8], [13] and adverse side reactions associated with the front line antifilarial drugs e.g. Diethylcarbamazine [DEC] and Ivermectin [13], [14] etc. are issues of great concern. On the other hand, a number of studies on human immune responses to helminthic infections have been carried out so far, but no clear immune effector mechanism has emerged on which an effective vaccine could be designed against these parasites [2], [15]. Hence, control of these major tropical diseases warrants radically new approaches to chemotherapy as well as vaccine design. Induction of apoptosis in parasites for drug development is a novel possibility that has been explored in great detail in several unicellular pathogens [16] but has not been evaluated so far, against metazoan helminth parasites. As a form of programmed cell death, apoptosis was initially described and extensively studied in the free living soil nematode C.elegans. Homologs of mammalian apoptosis related proteins have been identified in a trematode parasitic worm Schistosoma and function of some of these proteins has been studied by over-expressing them in artificial systems viz., mammalian cells [17]. However, no attempts"
}