{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000496",
  "doi": "10.1371/journal.pntd.0000496",
  "externalIds": [
    "pii:09-PNTD-RA-0116R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Burkholderia pseudomallei Is Genetically Diverse in Agricultural Land in Northeast Thailand",
  "authors": [
    {
      "name": "Vanaporn Wuthiekanun",
      "first": "Vanaporn",
      "last": "Wuthiekanun",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Unit, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Direk Limmathurotsakul",
      "first": "Direk",
      "last": "Limmathurotsakul",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Unit, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Narisara Chantratita",
      "first": "Narisara",
      "last": "Chantratita",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Unit, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Edward J. Feil",
      "first": "Edward J.",
      "last": "Feil",
      "affiliation": "Department of Biology and Biochemistry, University of Bath, Bath, United Kingdom"
    },
    {
      "name": "Nicholas P. J. Day",
      "first": "Nicholas P. J.",
      "last": "Day",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Unit, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand; Center for Clinical Vaccinology and Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Churchill Hospital, Oxford, United Kingdom"
    },
    {
      "name": "Sharon J. Peacock",
      "first": "Sharon J.",
      "last": "Peacock",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Unit, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand; Center for Clinical Vaccinology and Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Churchill Hospital, Oxford, United Kingdom; Department of Medicine, University of Cambridge, Addenbrooke's Hospital, Cambridge, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-08",
  "dateAccepted": "2009-07-02",
  "dateReceived": "2009-03-17",
  "volume": "3",
  "number": "8",
  "pages": "e496",
  "tags": [
    "Microbiology/Environmental Microbiology"
  ],
  "abstract": "Background\n\nThe soil-dwelling Gram-negative bacterium Burkholderia pseudomallei is the cause of melioidosis. Extreme structuring of genotype and genotypic frequency has been demonstrated for B. pseudomallei in uncultivated land, but its distribution and genetic diversity in agricultural land where most human infections are probably acquired is not well defined.\n\nMethods\n\nFixed-interval soil sampling was performed in a rice paddy in northeast Thailand in which 100 grams of soil was sampled at a depth of 30 cm from 10×10 sampling points each measuring 2.5 m by 2.5 m. Soil was cultured for the presence of B. pseudomallei and genotyping of colonies present on primary culture plates was performed using a combination of pulsed-field gel electrophoresis (PFGE) and multilocus sequence typing (MLST).\n\nPrincipal Findings\n\nB. pseudomallei was cultured from 28/100 samples. Genotyping of 630 primary colonies drawn from 11 sampling points demonstrated 10 PFGE banding pattern types, which on MLST were resolved into 7 sequence types (ST). Overlap of genotypes was observed more often between sampling points that were closely positioned. Two sampling points contained mixed B. pseudomallei genotypes, each with a numerically dominant genotype and one or more additional genotypes present as minority populations.\n\nConclusions\n\nGenetic diversity and structuring of B. pseudomallei exists despite the effects of flooding and the physical and chemical processes associated with farming. These findings form an important baseline for future studies of environmental B. pseudomallei.",
  "fullText": "Introduction The Gram-negative bacterium Burkholderia pseudomallei is the cause of melioidosis, a serious human infection with a mortality rate as high as 50% in Thailand and 20% in Australia [1],[2]. The organism is found in the environment across much of southeast Asia and northern Australia where infection may be acquired following bacterial inoculation and contamination of wounds, inhalation or ingestion [1],[2]. Environmental sampling to detect the presence of B. pseudomallei serves to map its distribution and define geographical regions of risk to humans and livestock. Bacterial genotyping of isolates from the environment and cases of disease is an essential component of outbreak investigations to link isolates to a common contaminated source [3]–[5], and has also been performed to provide insights into the relationship between pathogenic B. pseudomallei and the environmental population from which they are drawn [6]. These applications require sampling strategies that detect an unbiased and true representation of the bacterial population under study. A previous study conducted by us to define the genetic diversity of B. pseudomallei isolated from uncultivated land in northeast Thailand demonstrated that genetic heterogeneity both within a single soil sample and between samples taken within a geographically restricted sampling site were much greater than recognised previously [7]. Genotyping of 200 primary B. pseudomallei colonies at each of three sampling points 7.6 to 13.3 meters apart demonstrated that each point contained a predominant B. pseudomallei multilocus sequencing typing (MLST) sequence type (ST) together with 2 or 3 additional STs present as minority populations, and that genotypes were rarely shared between the three sampling points [7]. These findings have major implications for genotyping studies of environmental B. pseudomallei, and indicate that delineation of genotypes present even in a small area of the natural environment requires sampling of multiple primary B. pseudomallei colonies from multiple points. Although these studies shed light on the structure of natural populations, it is not clear whether these results from uncultivated land are translatable to agricultural land where the majority of human exposure and disease acquisition probably occurs. Anthropogenic disturbance resulting from ploughing, planting, the presence of rice, flooding, the application of chemical fertilizers and pesticides and rice stubble burning could have a major impact on B. pseudomallei presence, genotypes and distribution of genotypes. The objective of the study described here was to undertake a detailed evaluation of B. pseudomallei in a rice paddy, and to compare and contrast the results to our previous data from uncultivated land. Methods Study site Soil sampling was performed on the 9th May 2007 (the start of the rainy season) in a single rice paddy situated in Amphoe Lao Sua Kok, Ubon Ratchathani, northeast Thailand. This is a rural rice-growing region where buffalo and hand-held implements are used for ploughing. The site had been used for rice cultivation for at least 25 years, and was isolated by raised earth walkways on three sides and by a dirt road on the fourth side. The soil type was sandy loam and was moist but not wet during sampling."
}