{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001235",
  "doi": "10.1371/journal.pntd.0001235",
  "externalIds": [
    "pii:PNTD-D-11-00009"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Freedom, Justice, and Neglected Tropical Diseases",
  "authors": [
    {
      "name": "Carlos Franco-Paredes",
      "first": "Carlos",
      "last": "Franco-Paredes",
      "affiliation": "Hospital Infantil de Mexico, Federico Gomez, Mexico, D.F., Mexico, and Rollins School of Public Health, Emory University, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Jose I. Santos-Preciado",
      "first": "Jose I.",
      "last": "Santos-Preciado",
      "affiliation": "Unidad de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autonoma de Mexico, Mexico, D.F., Mexico"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-08",
  "volume": "5",
  "number": "8",
  "pages": "e1235",
  "tags": [
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases"
  ],
  "fullText": "“What moves us, reasonably enough, is not the realization that the world falls short of being completely just—which few of us expect—but that there are clearly remediable injustices around us which we want to eliminate” [1] Neglected tropical diseases (NTDs) are remediable injustices of our times. Poverty is the starting point, and the ultimate outcome, of NTDs. Much about poverty is evident enough, but considering poverty as simply low income is insufficient [2]. In the context of NTDs, poverty should be seen as the relative deprivation of freedoms and capabilities dictating a lack of opportunities and choices in life [3], [4]. Capabilities refer to the person's freedom to lead one type of life or another, and freedom with the real opportunity to accomplish what we value as human beings [5]. Thus, NTDs are diseases of socially excluded populations that promote poverty by relatively depriving individuals from basic capabilities and freedoms. The social pathways of becoming ill with an NTD include socially determined failures including widespread illiteracy, malnutrition, poor living conditions, unemployment, and the overall failure of ownership relations in the form of entitlements [2], [5]. In turn, in a vicious cycle of destitution and dispossession, NTDs produce disability, disfigurement, stigma, and premature mortality. When addressing issues surrounding social equity and justice it becomes inescapable to revisit the writings of Amartya Sen, one of the world's leading intellectuals of our time. Throughout his books, but most emphatically in his recent one, The Idea of Justice, he argues for a framework for the critical assessment of judgments about justice whether based on freedoms, capabilities, resources, or well-being [1]. According to Sen, liberty is defined as the possible fields of application of equality, and equality as the pattern of distribution of liberty; and living may be seen as a set of interrelated functionings, consisting of beings and doings [5]. A person's achievements in this respect can be seen as the vector of constitutive functionings, including elementary ones such as being adequately nourished and being in good health [4], [5]. The health status of an individual in a specific social arrangement can be scrutinized from two different perspectives: the actual achievement of health, and the freedom to achieve it [3]. Health achievement tends to be a reliable guide to the underlying capabilities of an individual and a central consideration of human life. At the same time, the freedoms and capabilities that we are able to exercise in our lives are dependent on our health achievement [3], [5]. Taking into account the role of health in human life, social justice calls for a fair distribution as well as efficient creation of human capabilities and opportunities for individuals to achieve and maintain good health [3]. Therefore, achieving health free from escapable or preventable illness, disability, and premature death, which occurs with most NTDs, is an integral component of justice in our lives that is directly influenced by our existing freedoms and capabilities [6]. As human beings, our ability to manage our food supply around 10,000 years"
}