{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001222",
  "doi": "10.1371/journal.pntd.0001222",
  "externalIds": [
    "pii:PNTD-D-11-00208"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The Neglected Tropical Diseases of India and South Asia: Review of Their Prevalence, Distribution, and Control or Elimination",
  "authors": [
    {
      "name": "Derek A. Lobo",
      "first": "Derek A.",
      "last": "Lobo",
      "affiliation": "Department of Public Health, Manipal University (Retired – WHO-South-East Asia Regional Office), Mangalore, India"
    },
    {
      "name": "Raman Velayudhan",
      "first": "Raman",
      "last": "Velayudhan",
      "affiliation": "Department of Control of Neglected Tropical Diseases, World Health Organization, Geneva, Switzerland"
    },
    {
      "name": "Priya Chatterjee",
      "first": "Priya",
      "last": "Chatterjee",
      "affiliation": "George Washington University, Washington, D.C., United States of America"
    },
    {
      "name": "Harajeshwar Kohli",
      "first": "Harajeshwar",
      "last": "Kohli",
      "affiliation": "George Washington University, Washington, D.C., United States of America"
    },
    {
      "name": "Peter J. Hotez",
      "first": "Peter J.",
      "last": "Hotez",
      "affiliation": "Sabin Vaccine Institute and Texas Children's Center for Vaccine Development, and the National School of Tropical Medicine at Baylor College of Medicine, Houston, Texas, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-10",
  "volume": "5",
  "number": "10",
  "pages": "e1222",
  "tags": [
    "Epidemiology",
    "Global health",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Public Health and Epidemiology",
    "Public health",
    "Socioeconomic aspects of health"
  ],
  "fullText": "Introduction The neglected tropical diseases (NTDs) are the most common infections of the world's poorest people living in Africa, Asia, and the Americas [1]. Occurring predominantly among people who live on less than US$2 per day or below the World Bank poverty figure of US$1.25 per day, the NTDs represent a group of chronic parasitic and related bacterial and viral infections that actually promote poverty because of their impact on child development, pregnancy outcome, and worker productivity [2]. The NTDs differ significantly in their prevalence and disease burden according to their geographic and regional presence. Such features for the NTDs in sub-Saharan Africa [3], China and East Asia [4], and the Americas [5]–[7], respectively, were reviewed previously. Here, we summarize current knowledge on the prevalence, distribution, and disease burden of the NTDs in India and South Asia, focusing on aspects particular to the region. The review of the literature was conducted using the online database PubMed from 2003 to 2010 with the Medical Subject Headings, the specific diseases listed in the World Health Organization's (WHO) first report on NTDs [8], and the geographic regions and countries of South Asia. Reference lists of identified articles and reviews were also hand searched as were WHO databases (http://www.who.int/), including the WHO's Weekly Epidemiological Record. Recently, a comprehensive review on the continuing challenge of infectious diseases in India was published [9]. However, this review focuses exclusively on NTDs, many of which, especially the helminthiases, were not emphasized previously [9]. Overview of NTDs in India and South Asia There is no single and universally accepted definition of the geographic area known as South Asia; however, most definitions include the nations of Bangladesh, India, Maldives, Nepal, and Sri Lanka. The WHO South-East Asian region also adds DPR Korea, Indonesia, Myanmar, Thailand, and Timor-Leste (http://www.searo.who.int/). Because the prevalence and disease burden of the major NTDs in East Asia were previously reviewed and included those five countries [4], we instead adopted the World Bank's use of the term South Asia, which incorporates the eight nations of Afghanistan, Bangladesh, Bhutan, India, Maldives, Nepal, Pakistan, and Sri Lanka [10], [11] (Figure 1). With a few exceptions, very little data on the NTDs are available from Afghanistan, so the information provided here emphasizes the NTDs in the other seven countries. Together, the South Asian nations mentioned above represent a population of 1.5 billion, or almost one-quarter, of the global population [11]. The major countries and their populations are listed in Table 1, with India accounting for 75% of the number of people living in South Asia. Although the World Bank reports that South Asia has experienced an impressive economic rebound since the global recession in 2009, with approximately 7% overall economic growth in 2010 [11], this rising tide has left behind a substantial number of people who remain trapped in poverty. Today over 1 billion people in South Asia live on less than US$2 per day [10], [11]. Moreover, the prevalence of underweight children in South Asia exceeds 40% in India,"
}