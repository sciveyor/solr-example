{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001005",
  "doi": "10.1371/journal.pntd.0001005",
  "externalIds": [
    "pii:PNTD-D-10-00045"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Low-Tech Analytical Method for Diethylcarbamazine Citrate in Medicated Salt",
  "authors": [
    {
      "name": "Abigail Weaver",
      "first": "Abigail",
      "last": "Weaver",
      "affiliation": "Department of Chemistry and Biochemistry, University of Notre Dame, Notre Dame, Indiana, United States of America"
    },
    {
      "name": "Patrick Brown",
      "first": "Patrick",
      "last": "Brown",
      "affiliation": "Department of Chemistry and Biochemistry, University of Notre Dame, Notre Dame, Indiana, United States of America"
    },
    {
      "name": "Shannon Huey",
      "first": "Shannon",
      "last": "Huey",
      "affiliation": "Brigham Young University, Provo, Utah, United States of America"
    },
    {
      "name": "Marco Magallon",
      "first": "Marco",
      "last": "Magallon",
      "affiliation": "Department of Chemistry and Biochemistry, University of Notre Dame, Notre Dame, Indiana, United States of America"
    },
    {
      "name": "E. Brennan Bollman",
      "first": "E. Brennan",
      "last": "Bollman",
      "affiliation": "Department of Chemistry and Biochemistry, University of Notre Dame, Notre Dame, Indiana, United States of America"
    },
    {
      "name": "Dominique Mares",
      "first": "Dominique",
      "last": "Mares",
      "affiliation": "Group SPES, Port-au-Prince, Haiti"
    },
    {
      "name": "Thomas G. Streit",
      "first": "Thomas G.",
      "last": "Streit",
      "affiliation": "Department of Biology, University of Notre Dame, Notre Dame, Indiana, United States of America"
    },
    {
      "name": "Marya Lieberman",
      "first": "Marya",
      "last": "Lieberman",
      "affiliation": "Department of Chemistry and Biochemistry, University of Notre Dame, Notre Dame, Indiana, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-02",
  "dateAccepted": "2010-12-04",
  "dateReceived": "2010-09-28",
  "volume": "5",
  "number": "2",
  "pages": "e1005",
  "tags": [
    "Analytical chemistry",
    "Chemical analysis",
    "Chemistry",
    "Filariasis",
    "Global health",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Parasitic diseases"
  ],
  "abstract": "The World Health Organization has called for an effort to eliminate Lymphatic Filariasis (LF) around the world. In regions where the disease is endemic, local production and distribution of medicated salt dosed with diethylcarbamazine (DEC) has been an effective method for eradicating LF. A partner of the Notre Dame Haiti program, Group SPES in Port-au-Prince, Haiti, produces a medicated salt called Bon Sel. Coarse salt is pre-washed and sprayed with a solution of DEC citrate and potassium iodate. Iodine levels are routinely monitored on site by a titrimetric method. However, the factory had no method for monitoring DEC. Critical analytical issues include 1) determining whether the amount of DEC in each lot of Bon Sel is within safe and therapeutically useful limits, 2) monitoring variability within and between production runs, and 3) determining the effect of a common local practice (washing salt before use) on the availability of DEC. This paper describes a novel titrimetric method for analysis of DEC citrate in medicated salt. The analysis needs no electrical power and requires only a balance, volumetric glassware, and burets that most salt production programs have on hand for monitoring iodine levels. The staff of the factory used this analysis method on site to detect underloading of DEC on the salt by their sprayer and to test a process change that fixed the problem.",
  "fullText": "Introduction The World Health Organization has called for an effort to eliminate Lymphatic Filariasis (LF) around the world. [1] A nematode worm (Wuchereria bancrofti) is the cause of 90% of lymphatic filariasis cases globally. Mosquito bites transmit larval nematodes (microfilariae) present in the blood stream of infected persons, and although the adult nematodes are resistant to medical treatment, human transmission in endemic regions can be stopped by administering drugs, such as diethylcarbamazine (DEC), that kill the microfilariae. DEC has had a long history of safe use in mass drug administration (MDA) LF eradication programs, [2]–[4] and so far, W. bancrofti do not appear to have developed resistance to DEC. [5]–[6] A course of treatment of 6 mg/kg per day of DEC citrate for 12 days (daily dose around 300 mg) can significantly reduce the microfilariae count in an infected person. However, in regions where the disease is endemic, yearly drug administration to infected individuals must be continued over the adult worm lifetime of 4–6 years to eradicate the disease. As an alternative to pill-based MDA, DEC can be administered to local populations in the form of medicated cooking salt, with DEC citrate present at 0.2–0.4% w/w, which corresponds to a daily dose of 20–40 mg DEC citrate. Local production and distribution of medicated salt fortified with DEC has proved to be a particularly effective method [7]–[8] for eradicating LF from endemic regions [9]–[10]. A partner of the Notre Dame Haiti program, Group SPES in Port-au-Prince, Haiti, produces a double-supplemented salt called “Bon Sel”. [11] Coarse salt is pre-washed and sprayed with a solution of DEC citrate and potassium iodate. Iodine levels are routinely monitored on site by a titrimetric method. However, as of 2010, the factory had no analytical process for monitoring DEC levels. Critical analytical issues include 1) determining whether the amount of DEC citrate in each lot of Bon Sel is within safe and therapeutically useful limits, 2) monitoring variability within and between production runs, and 3) determining the effect of a common local practice (washing salt before use) on the availability of DEC. The “gold standard” assay for DEC citrate uses high-performance liquid chromatography (HPLC). [12] Sending samples out for analysis would impose unwanted costs and prevent real time analysis of production runs, yet it was impossible to implement this process at the factory in Haiti, which has no access to an HPLC or to the supplies and expertise necessary to maintain one. Color tests and spectrophotometry have been used for monitoring DEC-medicated salt production, [13]–[15] although usually for qualitative monitoring. [16] The facility in Haiti wanted quantitative information but did not have a spectrometer. The goal of our group was to develop a back titration assay for DEC citrate in medicated salt requiring only a balance, volumetric glassware, and burets, equipment that most iodized salt production programs have on hand for monitoring iodine levels, and compare this method against the benchmark HPLC method. Materials and Methods Materials Samples of untreated NaCl and pharmaceutical grade DEC citrate"
}