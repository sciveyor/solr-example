{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000944",
  "doi": "10.1371/journal.pntd.0000944",
  "externalIds": [
    "pii:10-PNTD-RA-1253R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Ruminant Brucellosis in the Kafr El Sheikh Governorate of the Nile Delta, Egypt: Prevalence of a Neglected Zoonosis",
  "authors": [
    {
      "name": "Yamen M. Hegazy",
      "first": "Yamen M.",
      "last": "Hegazy",
      "affiliation": "Department of Veterinary Clinical Sciences, Royal Veterinary College, London, United Kingdom; Department of Animal Medicine, Faculty of Veterinary Medicine, Kafr El Sheikh, Egypt"
    },
    {
      "name": "Amgad Moawad",
      "first": "Amgad",
      "last": "Moawad",
      "affiliation": "Department of Microbiology, Faculty of Veterinary Medicine, Kafr El Sheikh, Egypt"
    },
    {
      "name": "Salama Osman",
      "first": "Salama",
      "last": "Osman",
      "affiliation": "Department of Animal Medicine, Faculty of Veterinary Medicine, Kafr El Sheikh, Egypt"
    },
    {
      "name": "Anne Ridler",
      "first": "Anne",
      "last": "Ridler",
      "affiliation": "Department of Veterinary Clinical Sciences, Royal Veterinary College, London, United Kingdom"
    },
    {
      "name": "Javier Guitian",
      "first": "Javier",
      "last": "Guitian",
      "affiliation": "Department of Veterinary Clinical Sciences, Royal Veterinary College, London, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-01",
  "dateAccepted": "2010-12-10",
  "dateReceived": "2010-06-02",
  "volume": "5",
  "number": "1",
  "pages": "e944",
  "tags": [
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases"
  ],
  "abstract": "Background\n\nBrucellosis is a neglected tropical zoonosis allegedly reemerging in Middle Eastern countries. Infected ruminants are the primary source of human infection; consequently, estimates of the frequency of ruminant brucellosis are useful elements for building effective control strategies. Unfortunately, these estimates are lacking in most Middle East countries including Egypt. Our objectives are to estimate the frequency of ruminant brucellosis and to describe its spatial distribution in Kafr El Sheikh Governorate, Nile Delta, Egypt.\n\nMethodology/Principal Findings\n\nWe conducted a cross-sectional study in which 791 sheep, 383 goats, 188 cattle milk tanks and 173 buffalo milk tanks were randomly selected in 40 villages and tested for the presence of antibodies against Brucella spp. The seroprevalence among different species was estimated and visualized using choropleth maps. A spatial scanning method was used to identify areas with significantly higher proportions of seropositive flocks and milk tanks. We estimated that 12.2% of sheep and 11.3% of goats in the study area were seropositive against Brucella spp. and that 12.2% and 12% of cattle and buffalo milk tanks had antibodies against Brucella spp. The southern part of the governorate had the highest seroprevalence with significant spatial clustering of seropositive flocks in the proximity of its capital and around the main animal markets.\n\nConclusions/ Significance\n\nOur study revealed that brucellosis is endemic at high levels in all ruminant species in the study area and questions the efficacy of the control measures in place. The high intensity of infection transmission among ruminants combined with high livestock and human density and widespread marketing of unpasteurized milk and dairy products may explain why Egypt has one of the highest rates of human brucellosis worldwide. An effective integrated human-animal brucellosis control strategy is urgently needed. If resources are not sufficient for nationwide implementation, high-risk areas could be prioritized.",
  "fullText": "Introduction Brucellosis is one of the most common zoonotic diseases worldwide, and as such poses a major threat to human health and animal production [1]–[2]. It is considered a neglected zoonosis by the World Health Organization (WHO), and has been identified as having the highest public health burden across all sections of the community; livestock keepers, consumers of livestock products and general population [3]. Several Middle Eastern and central Asian countries have recently reported an increase in the incidence of human brucellosis and the appearance of new foci [4]. Among the Middle East countries, Syria, Saudi Arabia, Iraq, Iran and Turkey have reported the highest annual incidence rates of human brucellosis worldwide with the exception of Central and Inner Asian countries; 160, 21, 28, 24 and 26 cases/100,000 persons-years at risk, respectively [4]. In Egypt, brucellosis is endemic among humans and domestic ruminants [5], and it has recently been found that catfish in the Nile Delta region can be naturally infected with Brucella melitensis [6]. There is a lack of information on the frequency of human brucellosis at the national level in Egypt, with few available figures obtained mainly from small scale surveys and hospital-based studies [4]. In the Nile delta region, the incidence was estimated at 18 cases/100,000 population in 2000 [7] and the seroprevalence within a village in the Gharbia governorate was estimated at 1.7% in 2003 [8]. To try to address the lack of reliable information, Jennings et al. [9] used population-based surveillance data to estimate the frequency of human brucellosis in one of the Upper Egypt governorates (Al Fayoum). They reported an incidence of 64 and 70 cases /100,000 population in 2002 and 2003 respectively, and found that hospital based surveillance identified less than 6% of human brucellosis cases. Reliable estimates of the frequency of brucellosis among ruminants in Egypt are also lacking despite an official control policy based on annual serological testing of all ruminant species over 6 months of age. Failure to test all eligible animals every year as per official guidelines, and non-random selection of herds/flocks or animals to be tested, are the reasons why accurate estimates of the seroprevalence of ruminant brucellosis in the country are not available [10]. The largest survey conducted so far across all governorates was carried out from 1994 through 1997, when 40% of the total ruminant population in the country was serologically tested against Brucella spp. as part of a national brucellosis surveillance and control project funded by United States Agency for International Development (USAID). The seroprevalence of brucellosis was estimated then at 0.9%, 0.3%, 1.8% and 8.2% of the cattle, buffalo, sheep and goat population, respectively [5], [11]. A recent study of 126 herds found 17.2%, 26.6% and 18.9% of the cattle farms, sheep flocks and goat flocks tested to be seropositive [12], but no information is given about the selection of herds/flocks which seem to have been conveniently or purposively selected. Ruminant species infected with Brucella spp. are known to be the primary source of"
}