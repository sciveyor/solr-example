{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000705",
  "doi": "10.1371/journal.pntd.0000705",
  "externalIds": [
    "pii:09-PNTD-RA-0598R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Visceral Leishmaniasis Relapse in Southern Sudan (1999–2007): A Retrospective Study of Risk Factors and Trends",
  "authors": [
    {
      "name": "Stanislaw Gorski",
      "first": "Stanislaw",
      "last": "Gorski",
      "affiliation": "Médecins Sans Frontières, Amsterdam, The Netherlands"
    },
    {
      "name": "Simon M. Collin",
      "first": "Simon M.",
      "last": "Collin",
      "affiliation": "Department of Social Medicine, University of Bristol, Bristol, United Kingdom"
    },
    {
      "name": "Koert Ritmeijer",
      "first": "Koert",
      "last": "Ritmeijer",
      "affiliation": "Médecins Sans Frontières, Amsterdam, The Netherlands"
    },
    {
      "name": "Kees Keus",
      "first": "Kees",
      "last": "Keus",
      "affiliation": "Médecins Sans Frontières, Amsterdam, The Netherlands"
    },
    {
      "name": "Francis Gatluak",
      "first": "Francis",
      "last": "Gatluak",
      "affiliation": "Médecins Sans Frontières, Amsterdam, The Netherlands"
    },
    {
      "name": "Marius Mueller",
      "first": "Marius",
      "last": "Mueller",
      "affiliation": "Médecins Sans Frontières, Amsterdam, The Netherlands"
    },
    {
      "name": "Robert N. Davidson",
      "first": "Robert N.",
      "last": "Davidson",
      "affiliation": "Department of Infection and Tropical Medicine, Northwick Park Hospital, Harrow, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-06",
  "dateAccepted": "2010-04-19",
  "dateReceived": "2009-10-28",
  "volume": "4",
  "number": "6",
  "pages": "e705",
  "tags": [
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Public Health and Epidemiology/Epidemiology",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Background\n            \nRisk factors associated with L. donovani visceral leishmaniasis (VL; kala azar) relapse are poorly characterized.\n\n            Methods\n            \nWe investigated patient characteristics and drug regimens associated with VL relapse using data from Médecins Sans Frontières - Holland (MSF) treatment centres in Southern Sudan. We used MSF operational data to investigate trends in VL relapse and associated risk factors.\n\n            Results\n            \nWe obtained data for 8,800 primary VL and 621 relapse VL patients treated between 1999 and 2007. Records of previous treatment for 166 VL relapse patients (26.7%) were compared with 7,924 primary VL patients who had no record of subsequent relapse. Primary VL patients who relapsed had larger spleens on admission (Hackett grade ≥3 vs0, odds ratio (OR) for relapse = 3.62 (95% CI 1.08, 12.12)) and on discharge (Hackett grade ≥3 vs 0, OR = 5.50 (1.84, 16.49)). Age, sex, malnutrition, mobility, and complications of treatment were not associated with risk of relapse, nor was there any trend over time. Treatment with 17-day sodium stibogluconate/paromomycin (SSG/PM) combination therapy vs 30-day SSG monotherapy was associated with increased risk of relapse (OR = 2.08 (1.21, 3.58)) but reduced risk of death (OR = 0.27 (0.20, 0.37)), although these estimates are likely to be residually confounded. MSF operational data showed a crude upward trend in the proportion of VL relapse patients (annual percentage change (APC) = 11.4% (−3.4%, 28.5%)) and a downward trend in deaths (APC = −18.1% (−22.5%, −13.4%)).\n\n            Conclusions\n            \nSplenomegaly and 17-day SSG/PM vs 30-day SSG were associated with increased risk of VL relapse. The crude upward trend in VL relapses in Southern Sudan may be attributable to improved access to treatment and reduced mortality due to SSG/PM combination therapy.",
  "fullText": "Introduction Visceral leishmaniasis (VL, kala-azar) is a systemic parasitic disease caused by the Leishmania donovani species complex. VL manifests with irregular bouts of fever, substantial weight loss, hepatosplenomegaly, pancytopenia, and susceptibility to opportunistic infection [1]. VL is typically fatal unless treated. In immunocompetent individuals, effective drug treatment reduces Leishmania amastigotes to a level undetectable in aspirates. An effective life-long cellular immune response normally develops, and residual parasites are suppressed [2]. Despite apparent clinical and parasitologic response to treatment, a proportion of VL patients who are apparently otherwise immunocompetent, have a recurrence of VL. This usually occurs within 6 months of treatment [1], [3], and later recurrence is rare – suggesting that recrudescence rather than re-infection is the usual mechanism of relapse. While the role of HIV infection in VL relapse is well-documented, for example among patients in southern Europe, risk factors for VL relapse among HIV-negative patients in VL-endemic regions of Africa remain poorly characterised. Médecins Sans Frontières - Holland (MSF) has treated &gt;80,000 VL patients in Sudan and Ethiopia since 1989, and has maintained an electronic record of patient characteristics, treatments and outcomes. Although these routinely-collected data are intrinsically incomplete due to the very challenging environment in which they are collected, our previous analyses have yielded important findings [3]–[10]. Most recently, we analysed risk factors for VL relapse in patients in Northern Ethiopia who were co-infected with HIV [11]. In contrast, the prevalence of HIV in Southern Sudan has remained the lowest in Africa until very recently [12]. Here we investigate risk factors for VL relapse among patients from this largely HIV-negative population who were treated at MSF clinics in Southern Sudan from 1999 to 2007. Methods Diagnosis Diagnostic, treatment, and discharge procedures used were consistent with WHO guidelines [13]. VL was diagnosed in clinical suspects by high titer (≥1∶6,400) antibodies to Leishmania (freeze-dried Leishmania antigen supplied by the Royal Tropical Institute, Amsterdam, The Netherlands) in a direct agglutination test (DAT); or by microscopy of splenic or lymph node aspirates; or (since 2004) by rK39 rapid diagnostic test (DiaMed-IT-Leish supplied by DiaMed AG, Cressier sur Morat, Switzerland); or on rare occasions when laboratories were not functioning, by clinical judgment (criteria: fever &gt;2 weeks with exclusion of malaria and either splenomegaly or lymphadenopathy and wasting). Treatment Until 2002, standard treatment for primary VL comprised daily IM injections of sodium stibogluconate (SSG; Albert David Ltd, Calcutta; supplied by the International Dispensary Association, Amsterdam, The Netherlands) at a dose of 20mg/kg/day (minimum dose, 200mg; no maximum dose) for 30 days. Between 2001 and 2003, SSG monotherapy as standard treatment was replaced with combination therapy of paromomycin (PM) plus SSG. This comprised 17 daily intramuscular injections of SSG 20mg/kg and paromomycin sulfate (Pharmamed Parenterals Ltd, Malta, supplied by the International Dispensary Association, Amsterdam, The Netherlands) at a dose of 15mg/kg (equivalent to ∼11mg/kg of PM base). SSG/PM was initially withheld from females of childbearing age, and since 2004 withheld only from pregnant women. SSG/PM was administered only at MSF treatment centres with a"
}