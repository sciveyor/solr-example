{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001520",
  "doi": "10.1371/journal.pntd.0001520",
  "externalIds": [
    "pii:PNTD-D-11-00363"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Dengue Infection in Children in Ratchaburi, Thailand: A Cohort Study. II. Clinical Manifestations",
  "authors": [
    {
      "name": "Chukiat Sirivichayakul",
      "first": "Chukiat",
      "last": "Sirivichayakul",
      "affiliation": "Department of Tropical Pediatrics, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Kriengsak Limkittikul",
      "first": "Kriengsak",
      "last": "Limkittikul",
      "affiliation": "Department of Tropical Pediatrics, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Pornthep Chanthavanich",
      "first": "Pornthep",
      "last": "Chanthavanich",
      "affiliation": "Department of Tropical Pediatrics, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Vithaya Jiwariyavej",
      "first": "Vithaya",
      "last": "Jiwariyavej",
      "affiliation": "Ratchaburi Hospital, Ministry of Public Health, Ratchaburi, Thailand"
    },
    {
      "name": "Watcharee Chokejindachai",
      "first": "Watcharee",
      "last": "Chokejindachai",
      "affiliation": "Department of Tropical Pediatrics, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Krisana Pengsaa",
      "first": "Krisana",
      "last": "Pengsaa",
      "affiliation": "Department of Tropical Pediatrics, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Saravudh Suvannadabba",
      "first": "Saravudh",
      "last": "Suvannadabba",
      "affiliation": "Department of Disease Control, Ministry of Public Health, Nonthaburi, Thailand"
    },
    {
      "name": "Wut Dulyachai",
      "first": "Wut",
      "last": "Dulyachai",
      "affiliation": "Ratchaburi Hospital, Ministry of Public Health, Ratchaburi, Thailand"
    },
    {
      "name": "G. William Letson",
      "first": "G. William",
      "last": "Letson",
      "affiliation": "International Public Health Consultant, Denver, Colorado, United States of America"
    },
    {
      "name": "Arunee Sabchareon",
      "first": "Arunee",
      "last": "Sabchareon",
      "affiliation": "Department of Tropical Pediatrics, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-02",
  "dateAccepted": "2011-12-23",
  "dateReceived": "2011-04-20",
  "volume": "6",
  "number": "2",
  "pages": "e1520",
  "tags": [
    "Medicine"
  ],
  "abstract": "Background\n          \nDengue infection is one of the most important mosquito-borne diseases. More data regarding the disease burden and the prevalence of each clinical spectrum among symptomatic infections and the clinical manifestations are needed. This study aims to describe the incidence and clinical manifestations of symptomatic dengue infection in Thai children during 2006 through 2008.\n\n          Study Design\n          \nThis study is a school-based prospective open cohort study with a 9,448 person-year follow-up in children aged 3–14 years. Active surveillance for febrile illnesses was done in the studied subjects. Subjects who had febrile illness were asked to visit the study hospital for clinical and laboratory evaluation, treatment, and serological tests for dengue infection. The clinical data from medical records, diary cards, and data collection forms were collected and analyzed.\n\n          Results\n          \nDengue infections were the causes of 12.1% of febrile illnesses attending the hospital, including undifferentiated fever (UF) (49.8%), dengue fever (DF) (39.3%) and dengue hemorrhagic fever (DHF) (10.9%). Headache, anorexia, nausea/vomiting and myalgia were common symptoms occurring in more than half of the patients. The more severe dengue spectrum (i.e., DHF) had higher temperature, higher prevalence of nausea/vomiting, abdominal pain, rash, diarrhea, petechiae, hepatomegaly and lower platelet count. DHF cases also had significantly higher prevalence of anorexia, nausea/vomiting and abdominal pain during day 3–6 and diarrhea during day 4–6 of illness. The absence of nausea/vomiting, abdominal pain, diarrhea, petechiae, hepatomegaly and positive tourniquet test may predict non-DHF.\n\n          Conclusion\n          \nAmong symptomatic dengue infection, UF is most common followed by DF and DHF. Some clinical manifestations may be useful to predict the more severe disease (i.e., DHF). This study presents additional information in the clinical spectra of symptomatic dengue infection.",
  "fullText": "Introduction Dengue infection is one of the most important mosquito-borne viral diseases especially in tropical and subtropical regions of the world. World Health Organization (WHO) estimated that some 2.5 billion people – two-fifths of the world population are at risk from dengue and there may be 50 million dengue infections worldwide every year [1]. Its clinical spectrum ranges from asymptomatic infection to undifferentiated fever (UF), dengue fever (DF), and dengue hemorrhagic fever (DHF) [2]. In two studies, infected Thai children, ranging from 49 [3] to 87% [4], were asymptomatic. The available previous studies on the clinical manifestations in Thai children were a hospital-based study [5] which studied more severe disease, and a school-based study [3] which provided less detailed data on clinical manifestations. More data regarding the disease burden and the prevalence of each clinical spectrum (UF, DF, DHF) among symptomatic infections and the clinical manifestations are important for health care providers and policymakers to better understand the disease [6]. In 2005, a cohort epidemiological study of dengue infection in school-children aged 3–14 years was conducted in Ratchaburi Province, Thailand. There were 481 subjects recruited in 2005 and then 3056 subjects were enrolled in February, 2006. The initial plan was to conduct the study up to the end of 2008. In 2009, the surveillance for dengue epidemiology was extended. However, because many subjects withdrew from the study to participate a phase 2b dengue vaccine trial, the data on clinical manifestations was collected only up to the end of 2008. This article aims to describe the incidence and clinical manifestations of symptomatic dengue infection occurring over the 3-year study of this cohort, from 2006 to 2008. Methods Study Site Ratchaburi Province is approximately 100 km west of Bangkok, the capital province of Thailand. It was ranked among the top ten provinces of Thailand for dengue incidence rate as reported to the Thai Ministry of Public Health. The study site is Muang District which is the capital district of the province. The population in this area is quite stable. There are low rates of migration and high ethnic homogeneity. The total population and number of children aged 5–14 years in Muang District was approximately 188,000 and 24,000, respectively. The data from Ratchaburi Provincial Health Office (unpublished) showed that the incidences of dengue diseases during 2006–2008 were 1.05, 1.15, and 0.24% in children aged 5–9 years, 10–14 years and total population, respectively. The principal medical facility in this area is Ratchaburi Hospital (RH), which is an 855-bed hospital providing tertiary medical care. The hospital has 90 pediatric beds and 12 pediatricians on staff. Study Population School-children enrolled in 7 schools in Muang District were invited to participate in this cohort study. The inclusion criteria were healthy boys and girls aged 3–10 years at the time of recruitment, living in Muang District or nearby villages. Exclusion criteria included children suffering from serious or chronic severe diseases such as asthma, malignancy, hepatic, renal, cardiac disease or disease associated with insufficiency of immune system, and planning"
}