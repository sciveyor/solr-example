{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000275",
  "doi": "10.1371/journal.pntd.0000275",
  "externalIds": [
    "pii:08-PNTD-HP-0058R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Yaws: A Second (and Maybe Last?) Chance for Eradication",
  "authors": [
    {
      "name": "Andrea Rinaldi",
      "first": "Andrea",
      "last": "Rinaldi"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-08",
  "volume": "2",
  "number": "8",
  "pages": "e275",
  "tags": [
    "Genetics and Genomics/Microbial Evolution and Genomics",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Microbiology/Medical Microbiology",
    "Microbiology/Microbial Evolution and Genomics",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "fullText": "Some diseases are neglected; others are simply forgotten. Yaws belongs to the second group. One of the first diseases to be targeted for eradication on a global scale, yaws almost disappeared, thanks to a massive treatment program started in the 1950s that eliminated it from many endemic areas. Then something went wrong. The combat line somehow broke down in several points, people focused on other health emergencies, and yaws vanished from the political agenda. Not surprisingly, the disease has now resurged in several countries, where mass treatment has had to be reintroduced. Although rarely fatal, yaws leaves its indelible mark on many of those touched, horribly corroding their skin and bones. Victims are mostly young children, who end up disabled, stigmatized, and poor. However, despite its visible effects, its amenability for eradication, and all the past efforts made to eliminate it, this bacterial infection is still a menace. The present situation calls for immediate action to wipe yaws out forever. Its history, on the other hand, has much to teach on how eradication campaigns should—and should not—be planned and conducted. “Yaws Begins Where the Road Ends” Yaws (the name is believed to originate from “yaya,” which in the indigenous Caribbean language means sore), or framboesia tropica (also known by the local names of buba, pian, paru, and parangi) is a chronic, contagious, nonvenereal infection caused by the spirochete Treponema pallidum subspecies pertenue. A disease of the poor, yaws mainly affects populations living in rural areas of warm and humid subtropical countries, where conditions of overcrowding, poor water supply, and lack of sanitation and hygiene prevail. The original distribution of the infection spanned across Africa, Asia, South America, and Oceania, but past eradication campaigns have strongly reduced the geographic extension and global burden; a few foci resist, however, notably in South-East Asia (Indonesia, Timor-Leste, Papua New Guinea) and Africa (Ghana, Republic of the Congo). Direct skin-to-skin contact is the main route for transmission, together with breaks in the skin caused by injuries, bites, or excoriations [1]. According to older accounts, flies may also play a role as vectors for transmission [2]. Clinical manifestations occur in three distinct phases. At the primary, early stage, a papular “raspberry-like” lesion develops after three to four weeks of incubation at the Treponema inoculation site (usually a leg) and ulcerates; this “mother yaw” generally heals spontaneously after a few months, although it can last a year or longer. During the incubation period, spirochetes invade the lymphatics and spread across the body. The secondary stage is characterized by the widespread dissemination of smaller skin lesions that teem with treponemes. Goundou, an osteitis characterized by impressive swellings on each side of the nose and reported sporadically, mostly from West Africa, is recognized as a form of secondary-stage yaws [3]. At the third (late) stage, whose onset can be after a period of latency lasting several years, bone, cartilage, and soft tissue destruction may occur, jointly with severe skin ulceration and painful hyperkeratosis on palms and soles, leading to"
}