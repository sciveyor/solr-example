{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000684",
  "doi": "10.1371/journal.pntd.0000684",
  "externalIds": [
    "pii:10-PNTD-RA-0799R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Massively Parallel Sequencing and Analysis of the Necator americanus Transcriptome",
  "authors": [
    {
      "name": "Cinzia Cantacessi",
      "first": "Cinzia",
      "last": "Cantacessi",
      "affiliation": "Department of Veterinary Science, The University of Melbourne, Werribee, Victoria, Australia"
    },
    {
      "name": "Makedonka Mitreva",
      "first": "Makedonka",
      "last": "Mitreva",
      "affiliation": "Department of Genetics, Genome Sequencing Center, Washington University School of Medicine, St. Louis, Missouri, United States of America"
    },
    {
      "name": "Aaron R. Jex",
      "first": "Aaron R.",
      "last": "Jex",
      "affiliation": "Department of Veterinary Science, The University of Melbourne, Werribee, Victoria, Australia"
    },
    {
      "name": "Neil D. Young",
      "first": "Neil D.",
      "last": "Young",
      "affiliation": "Department of Veterinary Science, The University of Melbourne, Werribee, Victoria, Australia"
    },
    {
      "name": "Bronwyn E. Campbell",
      "first": "Bronwyn E.",
      "last": "Campbell",
      "affiliation": "Department of Veterinary Science, The University of Melbourne, Werribee, Victoria, Australia"
    },
    {
      "name": "Ross S. Hall",
      "first": "Ross S.",
      "last": "Hall",
      "affiliation": "Department of Veterinary Science, The University of Melbourne, Werribee, Victoria, Australia"
    },
    {
      "name": "Maria A. Doyle",
      "first": "Maria A.",
      "last": "Doyle",
      "affiliation": "Department of Biochemistry and Molecular Biology, Bio21 Molecular Science and Biotechnology Institute, The University of Melbourne, Victoria, Australia"
    },
    {
      "name": "Stuart A. Ralph",
      "first": "Stuart A.",
      "last": "Ralph",
      "affiliation": "Department of Biochemistry and Molecular Biology, Bio21 Molecular Science and Biotechnology Institute, The University of Melbourne, Victoria, Australia"
    },
    {
      "name": "Elida M. Rabelo",
      "first": "Elida M.",
      "last": "Rabelo",
      "affiliation": "Departamento de Parasitologia, Instituto de Ciências Biológicas, Universidade Federal de Minas Gerais, Belo Horizonte, Minas Gerais, Brazil"
    },
    {
      "name": "Shoba Ranganathan",
      "first": "Shoba",
      "last": "Ranganathan",
      "affiliation": "Department of Chemistry and Biomolecular Sciences, Macquarie University, Sydney, New South Wales, Australia"
    },
    {
      "name": "Paul W. Sternberg",
      "first": "Paul W.",
      "last": "Sternberg",
      "affiliation": "Howard Hughes Medical Institute and Division of Biology, California Institute of Technology, Pasadena, California, United States of America"
    },
    {
      "name": "Alex Loukas",
      "first": "Alex",
      "last": "Loukas",
      "affiliation": "James Cook University, Cairns, Queensland, Australia"
    },
    {
      "name": "Robin B. Gasser",
      "first": "Robin B.",
      "last": "Gasser",
      "affiliation": "Department of Veterinary Science, The University of Melbourne, Werribee, Victoria, Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-05",
  "dateAccepted": "2010-03-25",
  "dateReceived": "2010-01-11",
  "volume": "4",
  "number": "5",
  "pages": "e684",
  "tags": [
    "Computational Biology/Genomics",
    "Computational Biology/Transcriptional Regulation",
    "Genetics and Genomics/Bioinformatics"
  ],
  "abstract": "Background\n\nThe blood-feeding hookworm Necator americanus infects hundreds of millions of people worldwide. In order to elucidate fundamental molecular biological aspects of this hookworm, the transcriptome of the adult stage of Necator americanus was explored using next-generation sequencing and bioinformatic analyses.\n\nMethodology/Principal Findings\n\nA total of 19,997 contigs were assembled from the sequence data; 6,771 of these contigs had known orthologues in the free-living nematode Caenorhabditis elegans, and most of them encoded proteins with WD40 repeats (10.6%), proteinase inhibitors (7.8%) or calcium-binding EF-hand proteins (6.7%). Bioinformatic analyses inferred that the C. elegans homologues are involved mainly in biological pathways linked to ribosome biogenesis (70%), oxidative phosphorylation (63%) and/or proteases (60%); most of these molecules were predicted to be involved in more than one biological pathway. Comparative analyses of the transcriptomes of N. americanus and the canine hookworm, Ancylostoma caninum, revealed qualitative and quantitative differences. For instance, proteinase inhibitors were inferred to be highly represented in the former species, whereas SCP/Tpx-1/Ag5/PR-1/Sc7 proteins ( = SCP/TAPS or Ancylostoma-secreted proteins) were predominant in the latter. In N. americanus, essential molecules were predicted using a combination of orthology mapping and functional data available for C. elegans. Further analyses allowed the prioritization of 18 predicted drug targets which did not have homologues in the human host. These candidate targets were inferred to be linked to mitochondrial (e.g., processing proteins) or amino acid metabolism (e.g., asparagine t-RNA synthetase).\n\nConclusions\n\nThis study has provided detailed insights into the transcriptome of the adult stage of N. americanus and examines similarities and differences between this species and A. caninum. Future efforts should focus on comparative transcriptomic and proteomic investigations of the other predominant human hookworm, A. duodenale, for both fundamental and applied purposes, including the prevalidation of anti-hookworm drug targets.",
  "fullText": "Introduction Soil-transmitted helminths ( = geohelminths) are responsible for neglected tropical diseases (NTDs) mostly in developing countries [1]. In particular, the blood-feeding hookworms Necator americanus and Ancylostoma duodenale (Nematoda) infect ∼740 million people in rural areas of the tropics and subtropics [2], causing an estimated disease burden of 22 million disability-adjusted life years (DALYs) [3]. Geographically, N. americanus is the most widely distributed hookworm of humans globally [4]. The life cycle is direct, with thin-shelled eggs passed in the faeces from the infected host. Under suitable environmental conditions (e.g., 26°C and 100% humidity; [5]), the eggs hatch and develop through two free-living larval stages to the infective, third-stage (L3; filariform) larvae. The latter larvae penetrate human skin and migrate via the circulatory system and lung to finally reside as adults usually in the duodenum. The adult stages attach by their buccal capsule to the intestinal mucosa, rupture capillaries and feed on blood. The pathogenesis of hookworm disease is mainly a consequence of the blood loss, which occurs during attachment and feeding. The disease ( = necatoriasis) is commonly characterized by iron-deficiency anaemia, which can cause physical and mental retardation and sometimes deaths in children, adverse maternal-foetal outcomes [6]–[7] and, in chronically infected individuals, can result in a significant alteration of their immune response to helminths [8]. Traditionally, the control of hookworm disease has relied mostly on the treatment of infected individuals with anthelmintics, such as albendazole, mebendazole, pyrantel pamoate and/or levamisole. With mass treatment strategies now in place in a number of countries [9]–[10], there is an increased potential for hookworms to develop genetic resistance against the compounds administered, if they are used excessively and at suboptimal dosages. Thus, given the experience with drug resistance in parasitic nematodes of livestock [11], it is prudent to maintain a continual focus on the discovery of novel drugs against hookworms of humans. Such a discovery effort could be underpinned by an integrated genomic-bioinformatic approach, using functional genomic and phenomic information available for the free-living nematode Caenorhabditis elegans (see WormBase; www.wormbase.org). This nematode, which is the best characterized metazoan organism [12]–[13], is considered to be relatively closely related to nematodes of the order Strongylida (to which hookworms belong) [14]. Current evidence indicates that ∼60% of genes in strongylids (or hookworms) have orthologues/homologues in C. elegans [15]–[16], and that a range of biological pathways is conserved between strongylid nematodes/hookworms and this free-living nematode [17]–[20]. Therefore, conducting comparative explorations of molecular data sets between these nematodes should identify nematode-specific biological pathways, which, if essential for the development and survival, could provide new targets for nematocidal drugs. Next generation sequencing technologies, such as ABI-SOLiD, Illumina/Solexa (www.illumina.com; [21]), Helicos (www.helicosbio.com; [22]) and 454/Roche (www.454.com; [23]), together with the recent progress in bioinformatics, are providing unique opportunities for the high-throughput transcriptomic and genomic explorations of nematodes in far more detail than previously possible [24] and at a substantially lower cost than using conventional (Sanger) sequencing. To date, genomic and molecular studies of hookworms have mainly involved the canine hookworm, Ancylostoma caninum [19], [25]–[27],"
}