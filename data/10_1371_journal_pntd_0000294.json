{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000294",
  "doi": "10.1371/journal.pntd.0000294",
  "externalIds": [
    "pii:08-PNTD-RA-0105R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Hyaluronidase of Bloodsucking Insects and Its Enhancing Effect on Leishmania Infection in Mice",
  "authors": [
    {
      "name": "Vera Volfova",
      "first": "Vera",
      "last": "Volfova",
      "affiliation": "Department of Parasitology, Faculty of Science, Charles University in Prague, Czech Republic"
    },
    {
      "name": "Jitka Hostomska",
      "first": "Jitka",
      "last": "Hostomska",
      "affiliation": "Department of Parasitology, Faculty of Science, Charles University in Prague, Czech Republic"
    },
    {
      "name": "Martin Cerny",
      "first": "Martin",
      "last": "Cerny",
      "affiliation": "Department of Ecology, Faculty of Science, Charles University in Prague, Czech Republic"
    },
    {
      "name": "Jan Votypka",
      "first": "Jan",
      "last": "Votypka",
      "affiliation": "Department of Parasitology, Faculty of Science, Charles University in Prague, Czech Republic"
    },
    {
      "name": "Petr Volf",
      "first": "Petr",
      "last": "Volf",
      "affiliation": "Department of Parasitology, Faculty of Science, Charles University in Prague, Czech Republic"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-09",
  "dateAccepted": "2008-08-19",
  "dateReceived": "2008-04-04",
  "volume": "2",
  "number": "9",
  "pages": "e294",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Infectious Diseases/Skin Infections"
  ],
  "abstract": "Background\n\nSalivary hyaluronidases have been described in a few bloodsucking arthropods. However, very little is known about the presence of this enzyme in various bloodsucking insects and no data are available on its effect on transmitted microorganisms. Here, we studied hyaluronidase activity in thirteen bloodsucking insects belonging to four different orders. In addition, we assessed the effect of hyaluronidase coinoculation on the outcome of Leishmania major infection in BALB/c mice.\n\nPrincipal Findings\n\nHigh hyaluronidase activity was detected in several Diptera tested, namely deer fly Chrysops viduatus, blackflies Odagmia ornata and Eusimilium latipes, mosquito Culex quinquefasciatus, biting midge Culicoides kibunensis and sand fly Phlebotomus papatasi. Lower activity was detected in cat flea Ctenocephalides felis. No activity was found in kissing bug Rhodnius prolixus, mosquitoes Anopheles stephensi and Aedes aegypti, tse-tse fly Glossina fuscipes, stable fly Stomoxys calcitrans and human louse Pediculus humanus. Hyaluronidases of different insects vary substantially in their molecular weight, the structure of the molecule and the sensitivity to reducing conditions or sodium dodecyl sulphate. Hyaluronidase exacerbates skin lesions caused by Leishmania major; more severe lesions developed in mice where L. major promastigotes were coinjected with hyaluronidase.\n\nConclusions\n\nHigh hyaluronidase activities seem to be essential for insects with pool-feeding mode, where they facilitate the enlargement of the feeding lesion and serve as a spreading factor for other pharmacologically active compounds present in saliva. As this enzyme is present in all Phlebotomus and Lutzomyia species studied to date, it seems to be one of the factors responsible for enhancing activity present in sand fly saliva. We propose that salivary hyaluronidase may facilitate the spread of other vector-borne microorganisms, especially those transmitted by insects with high hyaluronidase activity, namely blackflies (Simuliidae), biting midges (Ceratopogonidae) and horse flies (Tabanidae).",
  "fullText": "Introduction Hyaluronidases are a family of enzymes that degrade hyaluronan (HA) and several other glycosaminoglycan constituents of the extracellular matrix of vertebrates (for review see [1]). In insects, hyaluronidases are well-known from venoms of Hymenoptera and represent clinically important allergens of honey-bees, wasps and hornets [2]–[4]. Hyaluronidases were found also in cDNA libraries of salivary glands (sialomes) of various bloodsucking insects [5]–[8] and the enzyme activity was found in saliva of three groups of Diptera, namely sand flies, blackflies, and horse flies [9],[10]. Salivary hyaluronidases of parasitic insects may have diverse effects on the host. They play an important role in blood meal acquisition; by degrading HA abundant in host skin, hyaluronidases increase tissue permeability for other salivary components that serve as antihaemostatic, vasodilatory or anti-inflammatory agents [5],[9]. This is why hyaluronidases are frequently called “spreading factors” [11]. The enzyme activity facilitates the enlargement of the feeding lesion and the insect acquires the blood meal more rapidly. In addition, HA fragments were shown to have immunomodulatory properties; they affect maturation and migration of dendritic cells, induction of iNOS and chemokine secretion by macrophages and proliferation of activated T cells (reviewed in [12]). As blood sucking insects represent the most important vectors of infectious diseases, local immunomodulation of the vertebrate host may positively enhance the infection. Leishmaniasis is one of the most prevalent vector-borne diseases. It is initiated by the intradermal inoculation of Leishmania promastigotes during the bite of an infected sand fly (Diptera: Phlebotominae). As shown first by Titus and Ribeiro [13] saliva of the sand fly vector exacerbates the initial phase of Leishmania infections in terms of parasite burden and size of the cutaneous lesion. Sand fly saliva was described to contain an array of pharmacologically active compounds affecting host hemostasis and immune mechanisms (reviewed in [14],[15]) but the information about molecules responsible for the exacerbating effect is still very limited. Morris et al. [16] showed that maxadilan, a well-known vasodilator of the New World vector Lutzomyia longipalpis, exacerbates Leishmania infection to the same degree as whole saliva. Maxadilan inhibits splenocyte proliferation induced in vitro and delayed type hypersensitivity in mice [17] and it also has several inhibitory effects on macrophages and monocytes that would support Leishmania survival in the host [18]. However, this important peptide was not found in Old World vectors of genus Phlebotomus (www.ncbi.nih.gov), including P. papatasi where exacerbating effect of saliva was repeatedly demonstrated [19],[20]. The vasodilatory activity of P. papatasi was instead ascribed to adenosine and AMP present in saliva of this sand fly [21]. In the present work, we studied hyaluronidase activity in bloodsucking insects of four different orders. In addition, we assessed the effect of hyaluronidase coinoculation on the outcome of Leishmania major skin lesions and spreading into draining lymph nodes. Materials and Methods Insects and preparation of samples Samples used are summarized in Table 1. The insects originated from laboratory colonies or were collected in the wild. Salivary glands were dissected out in Tris buffer (20 mM Tris, 150 mM NaCl,"
}