{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000817",
  "doi": "10.1371/journal.pntd.0000817",
  "externalIds": [
    "pii:10-PNTD-RA-1034R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Taenia solium Cysticercosis in the Democratic Republic of Congo: How Does Pork Trade Affect the Transmission of the Parasite?",
  "authors": [
    {
      "name": "Nicolas Praet",
      "first": "Nicolas",
      "last": "Praet",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium; Department of Infectious and Parasitic Diseases, Research Unit of Epidemiology and Risk Analysis Applied to Veterinary Sciences, Faculty of Veterinary Medicine, University of Liege, Liege, Belgium"
    },
    {
      "name": "Kirezi Kanobana",
      "first": "Kirezi",
      "last": "Kanobana",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Constantin Kabwe",
      "first": "Constantin",
      "last": "Kabwe",
      "affiliation": "Institut National de Recherche Biomédicale, Kinshasa, Democratic Republic of Congo"
    },
    {
      "name": "Vivi Maketa",
      "first": "Vivi",
      "last": "Maketa",
      "affiliation": "Tropical Medicine Department, Kinshasa University, Kinshasa, Democratic Republic of Congo"
    },
    {
      "name": "Philippe Lukanu",
      "first": "Philippe",
      "last": "Lukanu",
      "affiliation": "Kimpese Health Zone, Kimpese, Democratic Republic of Congo"
    },
    {
      "name": "Pascal Lutumba",
      "first": "Pascal",
      "last": "Lutumba",
      "affiliation": "Institut National de Recherche Biomédicale, Kinshasa, Democratic Republic of Congo"
    },
    {
      "name": "Katja Polman",
      "first": "Katja",
      "last": "Polman",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Peter Matondo",
      "first": "Peter",
      "last": "Matondo",
      "affiliation": "Laboratoire Vétérinaire de Kinshasa, Kinshasa, Democratic Republic of Congo"
    },
    {
      "name": "Niko Speybroeck",
      "first": "Niko",
      "last": "Speybroeck",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium; Institute of Health and Society, Université Catholique de Louvain, Brussels, Belgium"
    },
    {
      "name": "Pierre Dorny",
      "first": "Pierre",
      "last": "Dorny",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Julienne Sumbu",
      "first": "Julienne",
      "last": "Sumbu",
      "affiliation": "Laboratoire Vétérinaire de Kinshasa, Kinshasa, Democratic Republic of Congo"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-09",
  "dateAccepted": "2010-08-11",
  "dateReceived": "2010-04-02",
  "volume": "4",
  "number": "9",
  "pages": "e817",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Public Health and Epidemiology"
  ],
  "abstract": "Background\n\nTaenia solium, a zoonotic parasite that is endemic in most developing countries where pork is consumed, is recognised as the main cause of acquired epilepsy in these regions. T. solium has been reported in almost all of the neighboring countries of Democratic Republic of Congo (DRC) but data on the current prevalence of the disease in the country itself are lacking. This study, focusing on porcine cysticercosis (CC), makes part of a first initiative to assess whether cysticercosis is indeed actually present in DRC.\n\nMethods\n\nAn epidemiological study on porcine CC was conducted (1) on urban markets of Kinshasa where pork is sold and (2) in villages in Bas-Congo province where pigs are traditionally reared. Tongue inspection and ELISA for the detection of circulating antigen of the larval stage of T. solium were used to assess the prevalence of active CC in both study sites.\n\nFindings\n\nThe overall prevalence of pigs with active cysticercosis did not significantly differ between the market and the village study sites (38.8 [CI95%: 34–43] versus 41.2% [CI95%: 33–49], respectively). However, tongue cysticercosis was only found in the village study site together with a significantly higher intensity of infection (detected by ELISA).\n\nInterpretation\n\nPigs reared at village level are sold for consumption on Kinshasa markets, but it seems that highly infected animals are excluded at a certain level in the pig trade chain. Indeed, preliminary informal surveys on common practices conducted in parallel revealed that pig farmers and/or buyers select the low infected animals and exclude those who are positive by tongue inspection at village level. This study provides the only recent evidence of CC presence in DRC and gives the first estimates to fill an important gap on the African taeniasis/cysticercosis distribution map.",
  "fullText": "Introduction Taenia solium taeniasis/cysticercosis is a zoonotic disease with serious public health and agricultural consequences, which is endemic in most developing countries where pork is consumed [1]. The adult tapeworm occurs only in humans (taeniasis) but infection with the larval stage (cysticercosis (CC)) can affect both pigs and humans. Taeniasis has only mild clinical manifestations and may go unnoticed. Human cysticercosis occurs when cystic larvae lodge in muscles, subcutaneous tissues, eyes or brain. Localization in the brain or in the spinal cord causes neurocysticercosis (NCC) [2]. Seizures are the most common symptom of NCC and NCC has been reported to be the main cause of acquired epilepsy in developing countries [3], [4]. Porcine cysticercosis, equally caused by the establishment of larvae in tissues, is an economical important disease, because of condemnation of carcasses and/or reduction of meat price of infected pigs [5], [6]. The life cycle of the disease is sustained in regions with low hygienic standards, lack of sanitary conditions and traditional pig-production systems with free roaming pigs, thereby facilitating pig's access to contaminated feces from tapeworm carriers. Taeniasis/cysticercosis is a poverty-related disease [7]. It has been seriously neglected due to the lack of information and awareness of the extent of the problem in many countries, combined with the absence of suitable and sensitive diagnostic tools which can be applied at low cost and large scale in disease-endemic areas [8], [9]. This so far neglected situation issues also in part from the fact that CC has no overt disease-specific manifestations, neither in pigs nor in humans, which makes it difficult to sensitize responsible authorities, both in the veterinary and the medical sectors. However, recently, the World Health Organization included cysticercosis in its 2008–2015 strategic plans for the control of neglected tropical diseases NTDs. The Democratic Republic of Congo (DRC) is one of the largest, and also poorest countries of Sub Saharan Africa (SSA) with a prevalence of undernourishment of 76% as compared to 30% for SSA [10]. Moreover, the economy of the DRC is only slowly recovering from two decades of decline caused by conflicts and war. This war situation dramatically reduced national output and government revenue, increased external debt, and resulted in the deaths of more than 3.5 million people from violence, famine, and disease and consequently also reduced the government's priorities on public health [11]. It is evident that health priorities in resource poor countries focus on major diseases such as HIV/AIDS, malaria or tuberculosis. However, this does not preclude the need of addressing neglected diseases, for which relatively simple control and prevention measures are often available, but only effective if applied in an integrated and sustainable way [12]. Common NTD's such as, soil-transmitted helminth infections (STH), schistosomiasis, filariasis and onchocerciasis are known to be widespread among the poor in SSA, including the DRC [13]. NCC and CC have been reported in almost all of the neighboring countries of DRC [14], [15], but data on the current prevalence of the disease in the country itself are lacking."
}