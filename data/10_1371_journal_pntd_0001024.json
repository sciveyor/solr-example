{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001024",
  "doi": "10.1371/journal.pntd.0001024",
  "externalIds": [
    "pii:PNTD-D-10-00160"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Strategies for Introducing Wolbachia to Reduce Transmission of Mosquito-Borne Diseases",
  "authors": [
    {
      "name": "Penelope A. Hancock",
      "first": "Penelope A.",
      "last": "Hancock",
      "affiliation": "Department of Zoology, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Steven P. Sinkins",
      "first": "Steven P.",
      "last": "Sinkins",
      "affiliation": "Department of Zoology, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "H. Charles J. Godfray",
      "first": "H. Charles J.",
      "last": "Godfray",
      "affiliation": "Department of Zoology, University of Oxford, Oxford, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-04",
  "dateAccepted": "2011-02-16",
  "dateReceived": "2010-11-12",
  "volume": "5",
  "number": "4",
  "pages": "e1024",
  "tags": [
    "Biology",
    "Ecology",
    "Population biology",
    "Theoretical biology"
  ],
  "abstract": "Certain strains of the endosymbiont Wolbachia have the potential to lower the vectorial capacity of mosquito populations and assist in controlling a number of mosquito-borne diseases. An important consideration when introducing Wolbachia-carrying mosquitoes into natural populations is the minimisation of any transient increase in disease risk or biting nuisance. This may be achieved by predominantly releasing male mosquitoes. To explore this, we use a sex-structured model of Wolbachia-mosquito interactions. We first show that Wolbachia spread can be initiated with very few infected females provided the infection frequency in males exceeds a threshold. We then consider realistic introduction scenarios involving the release of batches of infected mosquitoes, incorporating seasonal fluctuations in population size. For a range of assumptions about mosquito population dynamics we find that male-biased releases allow the infection to spread after the introduction of low numbers of females, many fewer than with equal sex-ratio releases. We extend the model to estimate the transmission rate of a mosquito-borne pathogen over the course of Wolbachia establishment. For a range of release strategies we demonstrate that male-biased release of Wolbachia-infected mosquitoes can cause substantial transmission reductions without transiently increasing disease risk. The results show the importance of including mosquito population dynamics in studying Wolbachia spread and that male-biased releases can be an effective and safe way of rapidly establishing the symbiont in mosquito populations.",
  "fullText": "Introduction Mosquito-borne parasites and viruses cause some of the world's most important diseases, disproportionately affecting poor communities and representing a major public health challenge. Biological control techniques aimed at suppressing mosquito populations or reducing their capacity to transmit disease may be a useful addition to traditional vector control strategies, especially if resistance to chemical insecticides in mosquito populations continues to rise [1]. Recently there has been increased interest in the use of certain strains of Wolbachia bacteria to reduce transmission by mosquito vectors of human diseases [2]–[7]. Wolbachia are maternally-inherited endosymbiotic bacteria that are common in many insect species including mosquitoes. Wolbachia spread in mosquito populations by manipulating the host's reproduction using a mechanism known as cytoplasmic incompatibility (CI) [8]. CI occurs when Wolbachia in infected males modify the sperm of their host such that arrest of embryonic development occurs unless the egg also carries the bacterium. Uninfected females are therefore at a disadvantage, and the Wolbachia spreads by a process of positive frequency-dependent selection. Models of Wolbachia dynamics show that spread will occur if the proportion of infected hosts exceeds a threshold that is higher for Wolbachia that cause stronger reductions in host fitness [9]. Recent studies indicate that infecting mosquito populations with certain strains of Wolbachia may lower their rates of disease transmission for two reasons. First, the bacteria may reduce mean adult lifespan [6], [10]. Because most vector-borne pathogens have a relatively long extrinsic incubation period in the mosquito a reduction in average longevity disproportionately affects infectious individuals, with beneficial consequences for disease transmission [11], [12]. However, a reduction in longevity also lowers the fitness of Wolbachia carriers and hence increases the threshold infection frequency required for spread to occur [13]. An ideal strain would increase mortality only late in life as this would (i) particularly affect pathogen-carrying individuals; (ii) have a lesser effect on host fitness and thus require fewer individuals to be introduced to pass the threshold infection frequency; and (iii) lead to less selection for modulation of the harmful effects of these Wolbachia. Second, Wolbachia can inhibit the development, replication or dissemination of important mosquito-borne pathogens, including filarial nematode parasites [5] and dengue and chikungunya viruses in Aedes aegypti [2], [7], and Plasmodium malaria parasites in Aedes aegypti [7] and Anopheles gambiae [5]. The capacity of Wolbachia-infected mosquitoes to transmit these diseases may thus be much reduced. However, the ability of Wolbachia to assist in the control of mosquito-borne diseases will depend on their dynamics in natural mosquito populations. Understanding the ecology of Wolbachia infections in mosquito populations is important as programmes to establish Wolbachia in wild Ae. aegypti are currently under consideration [3]. Recently we developed a modelling framework that allows the spread of Wolbachia that reduce the longevity of their insect hosts to be analysed [14]. The models allow the study of the demographic consequences of releasing the significant numbers of individuals often needed to breach the threshold for Wolbachia to spread. They can be used to explore different schedules of Wolbachia"
}