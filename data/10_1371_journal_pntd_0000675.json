{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000675",
  "doi": "10.1371/journal.pntd.0000675",
  "externalIds": [
    "pii:09-PNTD-RA-0563R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Antileishmanial High-Throughput Drug Screening Reveals Drug Candidates with New Scaffolds",
  "authors": [
    {
      "name": "Jair L. Siqueira-Neto",
      "first": "Jair L.",
      "last": "Siqueira-Neto",
      "affiliation": "Center for Neglected Diseases Drug Discovery (CND3), Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Ok-Ryul Song",
      "first": "Ok-Ryul",
      "last": "Song",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Hyunrim Oh",
      "first": "Hyunrim",
      "last": "Oh",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Jeong-Hun Sohn",
      "first": "Jeong-Hun",
      "last": "Sohn",
      "affiliation": "Active Compound Space Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Gyongseon Yang",
      "first": "Gyongseon",
      "last": "Yang",
      "affiliation": "Center for Neglected Diseases Drug Discovery (CND3), Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Jiyoun Nam",
      "first": "Jiyoun",
      "last": "Nam",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Jiyeon Jang",
      "first": "Jiyeon",
      "last": "Jang",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Jonathan Cechetto",
      "first": "Jonathan",
      "last": "Cechetto",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Chang Bok Lee",
      "first": "Chang Bok",
      "last": "Lee",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Seunghyun Moon",
      "first": "Seunghyun",
      "last": "Moon",
      "affiliation": "Image Mining Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Auguste Genovesio",
      "first": "Auguste",
      "last": "Genovesio",
      "affiliation": "Image Mining Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Eric Chatelain",
      "first": "Eric",
      "last": "Chatelain",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland"
    },
    {
      "name": "Thierry Christophe",
      "first": "Thierry",
      "last": "Christophe",
      "affiliation": "Screening Technology & Pharmacology Group, Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    },
    {
      "name": "Lucio H. Freitas-Junior",
      "first": "Lucio H.",
      "last": "Freitas-Junior",
      "affiliation": "Center for Neglected Diseases Drug Discovery (CND3), Institut Pasteur Korea, Seongnam-si, Gyeonggi-do, South Korea"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-05",
  "dateAccepted": "2010-03-19",
  "dateReceived": "2009-10-13",
  "volume": "4",
  "number": "5",
  "pages": "e675",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Drugs currently available for leishmaniasis treatment often show parasite resistance, highly toxic side effects and prohibitive costs commonly incompatible with patients from the tropical endemic countries. In this sense, there is an urgent need for new drugs as a treatment solution for this neglected disease. Here we show the development and implementation of an automated high-throughput viability screening assay for the discovery of new drugs against Leishmania. Assay validation was done with Leishmania promastigote forms, including the screening of 4,000 compounds with known pharmacological properties. In an attempt to find new compounds with leishmanicidal properties, 26,500 structurally diverse chemical compounds were screened. A cut-off of 70% growth inhibition in the primary screening led to the identification of 567 active compounds. Cellular toxicity and selectivity were responsible for the exclusion of 78% of the pre-selected compounds. The activity of the remaining 124 compounds was confirmed against the intramacrophagic amastigote form of the parasite. In vitro microsomal stability and cytochrome P450 (CYP) inhibition of the two most active compounds from this screening effort were assessed to obtain preliminary information on their metabolism in the host. The HTS approach employed here resulted in the discovery of two new antileishmanial compounds, bringing promising candidates to the leishmaniasis drug discovery pipeline.",
  "fullText": "Introduction Leishmaniasis is a neglected emerging disease without any adequate treatment adapted to the field [1]. The disease can be characterized by skin ulcers (cutaneous leishmaniasis), mucous degeneration, especially from the mouth and internal nose (mucocutaneous leishmaniasis), and visceral organ damage (visceral leishmaniasis), which is lethal if untreated. The different forms of leishmaniasis manifestation depend mainly on the species of parasite but are also related to the host immune system. Official World Health Organization (WHO) numbers from the 1990s are still used and estimate 12 million infected people and 350 million at risk living in one of the 88 endemic countries in America, Europe, Africa, the Middle East and Asia [2]. The number of deaths as a consequence of leishmaniasis is higher than 50,000 per year, with an incidence of 1.5 million annual registered cases of the disfiguring cutaneous leishmaniasis and 0.5 million annual registered cases of the potentially fatal visceral leishmaniasis [3], but these numbers probably underestimate the real burden of the disease [4],[5]. Leishmaniasis is caused by the kinetoplastid species from the genus Leishmania. Infection takes place when a sandfly vector inoculates Leishmania promastigotes into the mammalian bloodstream; these extracellular flagellated forms of the parasite live in the insect midgut. Once in the bloodstream, parasites are phagocytosed by mononuclear blood cells, especially macrophages, differentiating into the obligatory intracellular amastigote form. Amastigotes proliferate inside the macrophages before inducing the bursting of the host cell and being released into the bloodstream. This process occurs repeatedly, leading to tissue damage [6]. Parasite species and the host immune system determine the clinical status of the disease, ranging from cutaneous ulcers (cutaneous leishmaniasis) [7] to visceral organ damage (visceral leishmaniasis) [8], especially of the spleen and the liver. Most of the antileishmanial drugs currently in use for treatment, from the long time established antimonials to the recently introduced miltefosine, have disadvantages, such as patient toxicity, side effects and/or parasite resistance [9]. Lead discovery is currently one of the bottlenecks in the pipeline for novel antileishmanial drugs [10]. High-throughput screening (HTS) optimizes the chance of finding lead compounds through the identification of active compounds from a large number of candidates [11]–[12]. We adapted an in vitro fluorometric assay to HTS format using the promastigote form of L. major [13], one of the causative species of cutaneous leishmaniasis. This was the first reference strain used to sequence the genome of this parasite, completed in 2005 [14], and genome information can be accessible for future studies, including target identification or mechanism of action determination. To validate the assay in HTS format, we screened a 4,000-compound library containing many bioactive compounds with known pharmacological properties, including currently used antileishmanials. Following validation, the assay was applied to the screening of a library containing 26,500 structurally diverse chemical compounds. A total of 567 compounds showing a minimum of 70% growth inhibition of the parasite (L. major) were identified during the primary screening at 10 µM. Further tests on their cytotoxicity on a human macrophage cell line and specificity filtering"
}