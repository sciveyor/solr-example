{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000941",
  "doi": "10.1371/journal.pntd.0000941",
  "externalIds": [
    "pii:10-PNTD-RA-1248R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Factors Associated with Acquisition of Human Infective and Animal Infective Trypanosome Infections in Domestic Livestock in Western Kenya",
  "authors": [
    {
      "name": "Beatrix von Wissmann",
      "first": "Beatrix",
      "last": "von Wissmann",
      "affiliation": "Centre for Infectious Diseases, School of Biomedical Sciences, College of Medicine and Veterinary Medicine, The University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Noreen Machila",
      "first": "Noreen",
      "last": "Machila",
      "affiliation": "Centre for Infectious Diseases, School of Biomedical Sciences, College of Medicine and Veterinary Medicine, The University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Kim Picozzi",
      "first": "Kim",
      "last": "Picozzi",
      "affiliation": "Centre for Infectious Diseases, School of Biomedical Sciences, College of Medicine and Veterinary Medicine, The University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Eric M. Fèvre",
      "first": "Eric M.",
      "last": "Fèvre",
      "affiliation": "School of Biological Sciences, College of Science and Engineering, The University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Barend M. deC. Bronsvoort",
      "first": "Barend M.",
      "last": "deC. Bronsvoort",
      "affiliation": "Epidemiology, Economics and Risk Assessment (EERA), The Roslin Institute and Royal (Dick) School of Veterinary Studies, The University of Edinburgh, Roslin, United Kingdom"
    },
    {
      "name": "Ian G. Handel",
      "first": "Ian G.",
      "last": "Handel",
      "affiliation": "Epidemiology, Economics and Risk Assessment (EERA), The Roslin Institute and Royal (Dick) School of Veterinary Studies, The University of Edinburgh, Roslin, United Kingdom"
    },
    {
      "name": "Susan C. Welburn",
      "first": "Susan C.",
      "last": "Welburn",
      "affiliation": "Centre for Infectious Diseases, School of Biomedical Sciences, College of Medicine and Veterinary Medicine, The University of Edinburgh, Edinburgh, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-01",
  "dateAccepted": "2010-12-07",
  "dateReceived": "2010-08-03",
  "volume": "5",
  "number": "1",
  "pages": "e941",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections"
  ],
  "abstract": "Background\n            \nTrypanosomiasis is regarded as a constraint on livestock production in Western Kenya where the responsibility for tsetse and trypanosomiasis control has increasingly shifted from the state to the individual livestock owner. To assess the sustainability of these localised control efforts, this study investigates biological and management risk factors associated with trypanosome infections detected by polymerase chain reaction (PCR), in a range of domestic livestock at the local scale in Busia, Kenya. Busia District also remains endemic for human sleeping sickness with sporadic cases of sleeping sickness reported.\n\n            Results\n            \nIn total, trypanosome infections were detected in 11.9% (329) out of the 2773 livestock sampled in Busia District. Multivariable logistic regression revealed that host species and cattle age affected overall trypanosome infection, with significantly increased odds of infection for cattle older than 18 months, and significantly lower odds of infection in pigs and small ruminants. Different grazing and watering management practices did not affect the odds of trypanosome infection, adjusted by host species. Neither anaemia nor condition score significantly affected the odds of trypanosome infection in cattle. Human infective Trypanosoma brucei rhodesiense were detected in 21.5% of animals infected with T. brucei s.l. (29/135) amounting to 1% (29/2773) of all sampled livestock, with significantly higher odds of T. brucei rhodesiense infections in T. brucei s.l. infected pigs (OR = 4.3, 95%CI 1.5-12.0) than in T. brucei s.l. infected cattle or small ruminants.\n\n            Conclusions\n            \nAlthough cattle are the dominant reservoir of trypanosome infection it is unlikely that targeted treatment of only visibly diseased cattle will achieve sustainable interruption of transmission for either animal infective or zoonotic human infective trypanosomiasis, since most infections were detected in cattle that did not exhibit classical clinical signs of trypanosomiasis. Pigs were also found to be reservoirs of infection for T. b. rhodesiense and present a risk to local communities.",
  "fullText": "Introduction Tsetse transmitted African trypanosomiasis poses a severe socio-economic impact throughout sub-Saharan Africa with losses to production estimated at over US$ 1.3 billion annually in terms of meat and milk yield in cattle [1]. Animal trypanosomiasis, is a serious constraint to productivity in Busia District in Western Province, Kenya, where there are also sporadic cases of human sleeping sickness reported [2]. An estimated 70% of the potential labour force of the district is engaged in subsistence mixed crop-livestock farming [3] in this poor rural area. Trypanosomiasis related losses include both direct livestock out-put (weight-loss, decrease in milk, decreased reproductive rate) as well as lost opportunity in terms of integration of livestock into crop production and the potential for crop-improvement (loss of draught power and manure) [1], [4]. Trypanosoma congolense (T. congolense), T. vivax and to a lesser extent T. brucei s.l. are the species that affect local African cattle in this region. Small ruminants are generally reported to be less susceptible to clinical trypanosomiasis [5], however they can harbour low grade chronic trypanosome infections, which can induce severe pathology when transmitted to cattle [6]. Pigs are moderately susceptible to T. congolense and T. brucei s.l. infections [7], [8]. T. brucei s.l. infections are generally less pathogenic in indigenous livestock than either T. vivax or T. congolense [9]. However in areas endemic for Rhodesian sleeping sickness, livestock play an important role as a reservoir for the human infective subspecies T. brucei rhodesiense (T. b. rhodesiense), frequently without displaying overt clinical signs of infection. Traditionally, control of trypanosomiasis in Kenya was state-run. Until the late 1980s, large scale aerial and ground-spraying campaigns had been used by public agencies as the mainstay of tsetse and thus of trypanosomiasis control [10]. Over the last two decades, ongoing cuts in the budget of the Veterinary Department, concentrated the remaining available funds on the provision of public-goods services [11]. Trypanosomiasis was no longer perceived as an acute risk to human health in Kenya, but viewed as a livestock production disease, the control of which was in the interest of the individual livestock-owners. This shift in responsibility radically changed the scale of control efforts from area wide programmes, to small-scale community based interventions [4]. Behavioural and geographical risk factors for human sleeping sickness have been identified at the local scale in a neighbouring district in south-east Uganda (Tororo District) [12], but less is known about the epidemiology of livestock trypanosomiasis at the local scale in areas endemic for trypanosomiasis and human sleeping sickness in Kenya. This study examines the biological and management risk factors associated with trypanosomiasis infections (both animal infective and human infective, zoonotic infections), in a range of domestic livestock in Busia District, Kenya. Making use of a unique census set of blood samples from the local livestock population in two study sites in Busia, in combination with sensitive polymerase chain reaction (PCR) technology for identification of trypanosome infections, this study aimed to establish animal inherent and management related risk factors for trypanosomiasis at the"
}