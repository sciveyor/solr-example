{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000531",
  "doi": "10.1371/journal.pntd.0000531",
  "externalIds": [
    "pii:09-PNTD-RA-0205R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Recent Rapid Rise of a Permethrin Knock Down Resistance Allele in Aedes aegypti in México",
  "authors": [
    {
      "name": "Gustavo Ponce García",
      "first": "Gustavo Ponce",
      "last": "García",
      "affiliation": "Laboratorio de Entomología Médica, Facultad de Ciencias Biológicas, Universidad Autónoma de Nuevo León, San Nicolás de los Garza, Nuevo León, México"
    },
    {
      "name": "Adriana E. Flores",
      "first": "Adriana E.",
      "last": "Flores",
      "affiliation": "Laboratorio de Entomología Médica, Facultad de Ciencias Biológicas, Universidad Autónoma de Nuevo León, San Nicolás de los Garza, Nuevo León, México"
    },
    {
      "name": "Ildefonso Fernández-Salas",
      "first": "Ildefonso",
      "last": "Fernández-Salas",
      "affiliation": "Laboratorio de Entomología Médica, Facultad de Ciencias Biológicas, Universidad Autónoma de Nuevo León, San Nicolás de los Garza, Nuevo León, México"
    },
    {
      "name": "Karla Saavedra-Rodríguez",
      "first": "Karla",
      "last": "Saavedra-Rodríguez",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Guadalupe Reyes-Solis",
      "first": "Guadalupe",
      "last": "Reyes-Solis",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Saul Lozano-Fuentes",
      "first": "Saul",
      "last": "Lozano-Fuentes",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "J. Guillermo Bond",
      "first": "J.",
      "last": "Guillermo Bond",
      "affiliation": "Centro Regional de Investigación en Salud Pública, Instituto Nacional de Salud Pública, Tapachula, Chiapas, México"
    },
    {
      "name": "Mauricio Casas-Martínez",
      "first": "Mauricio",
      "last": "Casas-Martínez",
      "affiliation": "Centro Regional de Investigación en Salud Pública, Instituto Nacional de Salud Pública, Tapachula, Chiapas, México"
    },
    {
      "name": "Janine M. Ramsey",
      "first": "Janine M.",
      "last": "Ramsey",
      "affiliation": "Centro Regional de Investigación en Salud Pública, Instituto Nacional de Salud Pública, Tapachula, Chiapas, México"
    },
    {
      "name": "Julián García-Rejón",
      "first": "Julián",
      "last": "García-Rejón",
      "affiliation": "Universidad Autónoma de Yucatán, Laboratorio de Arbovirología, Centro de Investigaciones Regionales Dr. Hideyo Noguchi, Universidad Autónoma de Yucatán, Mérida, Yucatán, México"
    },
    {
      "name": "Marco Domínguez-Galera",
      "first": "Marco",
      "last": "Domínguez-Galera",
      "affiliation": "Servicios Estatales de Salud de Quintana Roo, Servicios Estatales de Salud de Quintana Roo, Chetumal, Quintana Roo, México"
    },
    {
      "name": "Hilary Ranson",
      "first": "Hilary",
      "last": "Ranson",
      "affiliation": "Vector Group, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Janet Hemingway",
      "first": "Janet",
      "last": "Hemingway",
      "affiliation": "Vector Group, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Lars Eisen",
      "first": "Lars",
      "last": "Eisen",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "William C. Black IV",
      "first": "William C.",
      "last": "Black",
      "suffix": "IV",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Colorado State University, Fort Collins, Colorado, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-10",
  "dateAccepted": "2009-09-15",
  "dateReceived": "2009-05-01",
  "volume": "3",
  "number": "10",
  "pages": "e531",
  "tags": [
    "Genetics and Genomics/Gene Function",
    "Genetics and Genomics/Population Genetics",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Infectious Diseases/Viral Infections",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Background\n\nAedes aegypti, the ‘yellow fever mosquito’, is the primary vector to humans of dengue and yellow fever flaviviruses (DENV, YFV), and is a known vector of the chikungunya alphavirus (CV). Because vaccines are not yet available for DENV or CV or are inadequately distributed in developing countries (YFV), management of Ae. aegypti remains the primary option to prevent and control outbreaks of the diseases caused by these arboviruses. Permethrin is one of the most widely used active ingredients in insecticides for suppression of adult Ae. aegypti. In 2007, we documented a replacement mutation in codon 1,016 of the voltage-gated sodium channel gene (para) of Ae. aegypti that encodes an isoleucine rather than a valine and confers resistance to permethrin. Ile1,016 segregates as a recessive allele conferring knockdown resistance to homozygous mosquitoes at 5–10 µg of permethrin in bottle bioassays.\n\nMethods and Findings\n\nA total of 81 field collections containing 3,951 Ae. aegypti were made throughout México from 1996 to 2009. These mosquitoes were analyzed for the frequency of the Ile1,016 mutation using a melting-curve PCR assay. Dramatic increases in frequencies of Ile1,016 were recorded from the late 1990's to 2006–2009 in several states including Nuevo León in the north, Veracruz on the central Atlantic coast, and Yucatán, Quintana Roo and Chiapas in the south. From 1996 to 2000, the overall frequency of Ile1,016 was 0.04% (95% confidence interval (CI95) = 0.12%; n = 1,359 mosquitoes examined). The earliest detection of Ile1,016 was in Nuevo Laredo on the U.S. border in 1997. By 2003–2004 the overall frequency of Ile1,016 had increased ∼100-fold to 2.7% (±0.80% CI95; n = 808). When checked again in 2006, the frequency had increased slightly to 3.9% (±1.15% CI95; n = 473). This was followed in 2007–2009 by a sudden jump in Ile1,016 frequency to 33.2% (±1.99% CI95; n = 1,074 mosquitoes). There was spatial heterogeneity in Ile1,016 frequencies among 2007–2008 collections, which ranged from 45.7% (±2.00% CI95) in the state of Veracruz to 51.2% (±4.36% CI95) in the Yucatán peninsula and 14.5% (±2.23% CI95) in and around Tapachula in the state of Chiapas. Spatial heterogeneity was also evident at smaller geographic scales. For example within the city of Chetumal, Quintana Roo, Ile1,016 frequencies varied from 38.3%–88.3%. A linear regression analysis based on seven collections from 2007 revealed that the frequency of Ile1,016 homozygotes accurately predicted knockdown rate for mosquitoes exposed to permethrin in a bioassay (R2 = 0.98).\n\nConclusions\n\nWe have recorded a dramatic increase in the frequency of the Ile1,016 mutation in the voltage-gated sodium channel gene of Ae. aegypti in México from 1996 to 2009. This may be related to heavy use of permethrin-based insecticides in mosquito control programs. Spatial heterogeneity in Ile1,016 frequencies in 2007 and 2008 collections may reflect differences in selection pressure or in the initial frequency of Ile1,016. The rapid recent increase in Ile1,016 is predicted by a simple model of positive directional selection on a recessive allele. Unfortunately this model also predicts rapid fixation of Ile1,016 unless there is negative fitness associated with Ile1,016 in the absence of permethrin. If so, then spatial refugia of susceptible Ae. aegypti or rotational schedules of different classes of adulticides could be established to slow or prevent fixation of Ile1,016.",
  "fullText": "Introduction Aedes aegypti, the ‘yellow fever mosquito’, is the primary vector to humans of dengue and yellow fever flaviviruses (DENV, YFV) [1]–[3]. Vaccines are not yet available against DENV [4] and, despite the presence of a safe and effective YFV vaccine [5]–[7], the World Health Organization estimates there are 200,000 cases and 30,000 deaths attributable to yellow fever each year [8]. The principal means to reduce transmission of these arboviruses has therefore been through control or eradication of Ae. aegypti [9]. Historic eradication campaigns that combined source reduction to remove larval development sites with use of dichloro-diphenyl-trichloroethane (DDT) to kill adults were successful in eliminating the mosquito and its associated arboviruses, especially in the Americas, but these programs were not sustained and Ae. aegypti and DENV re-emerged in force [10],[11]. In recent decades, pyrethroid insecticides have played a major global role in the control of Ae. aegypti adults, often in combination with the organophosphate insecticide temephos to control immatures. However, the evolution of resistance to these and other insecticides in Ae. aegypti may compromise the effectiveness of control programs [12]–[14]. Since 1950, operational vector control programs in México have used a series of insecticides to control mosquito vectors and reduce arbovirus and malaria transmission (Official Regulations of México, NOM-032-SSA) [12]. The organochlorine insecticide DDT was used extensively for indoor house spraying from 1950–1960 and was still used in some locations until 1998. Organophosphate insecticides with malathion as the active ingredient were later used for ultra-low volume (ULV) space spraying of wide areas from 1981 to 1999. In 2000, vector control programs in México then switched to permethrin-based insecticides for adult control. This has provided prolonged and intense selection pressure for resistance evolution in Ae. aegypti. Indeed, pyrethroid insecticides with active ingredients such as permethrin, deltamethrin, resmethrin and sumithrin are now commonly applied across the world to kill adult mosquitoes and reduce the burden of mosquito-borne diseases. The future global use of bednets, curtains and other household items treated with pyrethroids for personal protection will likely increase dramatically [15]–[18]. This underscores the critical need to monitor and manage resistance to pyrethroid insecticides to maintain their use for vector control. Pyrethroids act by structure-related interactions with specific regions of voltage-dependent sodium channels that prolong the opening of these channels, and produce instant paralysis [19]. Nervous system stimulation proceeds from excitation to convulsions and tetanic paralysis. Metabolic resistance and target site insensitivity are both major forms of pyrethroid resistance [19],[20]. ‘Knockdown resistance’ (kdr) is a generic term applied to insects that fail to lose coordinated activity immediately following pyrethroid exposure. Typically kdr is unaffected by synergists that inhibit esterases and monooxygenases. Instead kdr arises through nonsynonymous mutations in the voltage-gated sodium channel transmembrane gene (orthologue of the paralysis locus in Drosophila melanogaster) [21] that reduce pyrethroid binding. Kdr usually limits the effectiveness of pyrethroids to varying degrees depending on whether the insecticide contains a descyano-3-phenoxybenzyl alcohol (type I pyrethroid) or an α-cyano-3-phenoxybenzyl alcohol (type II). Thus detection of kdr in the field may"
}