{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000972",
  "doi": "10.1371/journal.pntd.0000972",
  "externalIds": [
    "pii:10-PNTD-RA-1454R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Diagnostic Accuracy of PCR in gambiense Sleeping Sickness Diagnosis, Staging and Post-Treatment Follow-Up: A 2-year Longitudinal Study",
  "authors": [
    {
      "name": "Stijn Deborggraeve",
      "first": "Stijn",
      "last": "Deborggraeve",
      "affiliation": "Department of Parasitology, Institute of Tropical Medicine, Antwerp, Belgium; Rega Institute, Catholic University of Leuven, Leuven, Belgium"
    },
    {
      "name": "Veerle Lejon",
      "first": "Veerle",
      "last": "Lejon",
      "affiliation": "Department of Parasitology, Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Rosine Ali Ekangu",
      "first": "Rosine Ali",
      "last": "Ekangu",
      "affiliation": "Department of Parasitology, Institute of Tropical Medicine, Antwerp, Belgium; Department of Parasitology, Institut National de Recherche Biomédicale, Kinshasa, Democratic Republic of the Congo"
    },
    {
      "name": "Dieudonné Mumba Ngoyi",
      "first": "Dieudonné",
      "last": "Mumba Ngoyi",
      "affiliation": "Department of Parasitology, Institute of Tropical Medicine, Antwerp, Belgium; Department of Parasitology, Institut National de Recherche Biomédicale, Kinshasa, Democratic Republic of the Congo"
    },
    {
      "name": "Patient Pati Pyana",
      "first": "Patient",
      "last": "Pati Pyana",
      "affiliation": "Department of Parasitology, Institute of Tropical Medicine, Antwerp, Belgium; Department of Parasitology, Institut National de Recherche Biomédicale, Kinshasa, Democratic Republic of the Congo"
    },
    {
      "name": "Médard Ilunga",
      "first": "Médard",
      "last": "Ilunga",
      "affiliation": "Programme National de Lutte contre la Trypanosomiase Humaine Africaine, Mbuji-Mayi, Democratic Republic of the Congo"
    },
    {
      "name": "Jean Pierre Mulunda",
      "first": "Jean Pierre",
      "last": "Mulunda",
      "affiliation": "Programme National de Lutte contre la Trypanosomiase Humaine Africaine, Mbuji-Mayi, Democratic Republic of the Congo"
    },
    {
      "name": "Philippe Büscher",
      "first": "Philippe",
      "last": "Büscher",
      "affiliation": "Department of Parasitology, Institute of Tropical Medicine, Antwerp, Belgium"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-02",
  "dateAccepted": "2011-01-25",
  "dateReceived": "2010-08-13",
  "volume": "5",
  "number": "2",
  "pages": "e972",
  "tags": [
    "Infectious Diseases/Infectious Diseases of the Nervous System",
    "Infectious Diseases/Protozoal Infections",
    "Infectious Diseases/Tropical and Travel-Associated Diseases"
  ],
  "abstract": "Background\n\nThe polymerase chain reaction (PCR) has been proposed for diagnosis, staging and post-treatment follow-up of sleeping sickness but no large-scale clinical evaluations of its diagnostic accuracy have taken place yet.\n\nMethodology/Principal Findings\n\nAn 18S ribosomal RNA gene targeting PCR was performed on blood and cerebrospinal fluid (CSF) of 360 T. brucei gambiense sleeping sickness patients and on blood of 129 endemic controls from the Democratic Republic of Congo. Sensitivity and specificity (with 95% confidence intervals) of PCR for diagnosis, disease staging and treatment failure over 2 years follow-up post-treatment were determined. Reference standard tests were trypanosome detection for diagnosis and trypanosome detection and/or increased white blood cell concentration in CSF for staging and detection of treatment failure. PCR on blood showed a sensitivity of 88.4% (84.4–92.5%) and a specificity of 99.2% (97.7–100%) for diagnosis, while for disease staging the sensitivity and specificity of PCR on cerebrospinal fluid were 88.4% (84.8–91.9%) and 82.9% (71.2–94.6%), respectively. During follow-up after treatment, PCR on blood had low sensitivity to detect treatment failure. In cerebrospinal fluid, PCR positivity vanished slowly and was observed until the end of the 2 year follow-up in around 20% of successfully treated patients.\n\nConclusions/Significance\n\nFor T.b. gambiense sleeping sickness diagnosis and staging, PCR performed better than, or similar to, the current parasite detection techniques but it cannot be used for post-treatment follow-up. Continued PCR positivity in one out of five cured patients points to persistence of living or dead parasites or their DNA after successful treatment and may necessitate the revision of some paradigms about the pathophysiology of sleeping sickness.",
  "fullText": "Introduction Human African trypanosomiasis (HAT) or sleeping sickness is caused by Trypanosoma brucei (T. b.) gambiense or T. b. rhodesiense and transmitted by tsetse flies. The disease is endemic in several countries in sub-Saharan Africa and usually found in remote rural areas. The Democratic Republic of the Congo (DRC) has the highest prevalence of T. b. gambiense HAT, with 8,000 to 25,000 cases diagnosed annually between 1997 and 2006 [1]. In the absence of prophylactic drugs or vaccines, disease control relies heavily on accurate diagnosis and effective treatment of patients. Diagnosis of T. b. gambiense HAT is generally based on serological screening of individuals followed by confirmation through parasite detection in lymph node aspirates or blood [2]. The disease progresses over two stages, the haemolymphatic (first stage) and the meningo-encephalitic phase (second stage), which require different drug regimens [3]. Hence, accurate staging of the disease is important for the therapeutic decision and done by microscopic analysis of the cerebrospinal fluid (CSF) [4]. The World Health Organization recommends a follow-up after treatment of 2 years before confirmation of definite cure, which is defined as the absence of trypanosomes, normalized white blood cell count in CSF and normalization of clinical signs [5]. In the past two decades, a wide range of different polymerase chain reaction (PCR) tests has been developed for trypanosome detection and identification [6]. PCR has frequently been presented as promising in HAT patient diagnosis, staging and post-treatment follow-up. However, very few PCR approaches have been evaluated on a large number of clinical specimens and none has been evaluated for detection of treatment failure in a prospective longitudinal study. Here we present the results of PCR on a series of 360 T. b. gambiense HAT patients who were enrolled in a 2-year prospective cohort. We evaluate the diagnostic accuracy of PCR in diagnosis, staging and detection of treatment failure. Methods Ethics statement Samples from HAT patients were collected within a prospective observational study (THARSAT) [7]. The Commission for Medical Ethics of Institute of Tropical Medicine, Antwerp, Belgium and the Ethical Commission of the Ministry of Public Health, DRC approved this study. Negative control samples were collected within the TRYLEIDIAG study, which was approved by the commission for Medical Ethics of the University of Antwerp, Belgium and the Ethical Commission of the Ministry of Health DRC. All participants had given their written informed consent before inclusion into the study. Study participants Human African trypanosomiasis patients were prospectively enrolled in the hospital of Mbuji Mayi, Kasai Province, DRC as described earlier [7] using the following inclusion criteria: trypanosomes in lymph node aspirate, blood or CSF; ≥12 years old; and living within a 100 km perimeter around Mbuji Mayi. Exclusion criteria were: pregnancy; no guarantee for follow-up; moribund condition; hemorrhagic CSF or; concurrent serious illness (tuberculosis, bacterial or cryptococcal meningitis). The cohort consisted of primary HAT cases (never treated previously for HAT) and retreatment HAT cases (previously treated for HAT, but with reappearance of trypanosomes in CSF at inclusion). All cases were treated"
}