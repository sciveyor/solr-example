{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000839",
  "doi": "10.1371/journal.pntd.0000839",
  "externalIds": [
    "pii:10-PNTD-ED-1351R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Rabies, Still Neglected after 125 Years of Vaccination",
  "authors": [
    {
      "name": "Hervé Bourhy",
      "first": "Hervé",
      "last": "Bourhy",
      "affiliation": "Institut Pasteur, Paris, France"
    },
    {
      "name": "Alice Dautry-Varsat",
      "first": "Alice",
      "last": "Dautry-Varsat",
      "affiliation": "Institut Pasteur, Paris, France"
    },
    {
      "name": "Peter J. Hotez",
      "first": "Peter J.",
      "last": "Hotez",
      "affiliation": "Sabin Vaccine Institute, Washington, D. C., United States of America; Department of Microbiology, Immunology, and Tropical Medicine, George Washington University, Washington, D.C., United States of America"
    },
    {
      "name": "Jérôme Salomon",
      "first": "Jérôme",
      "last": "Salomon",
      "affiliation": "Institut Pasteur, Paris, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-11",
  "volume": "4",
  "number": "11",
  "pages": "e839",
  "tags": [
    "Public Health and Epidemiology/Global Health",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "fullText": "Rabies is a viral zoonotic infection of the central nervous system caused by a lyssavirus. The disease is fatal without proper post exposure prophylaxis (PEP). In July 1885, Louis Pasteur obtained his first success against rabies by vaccinating Joseph Meister, a 9-year-old boy presenting with multiple deep bite wounds [1]. After more than 700 successful inoculations, Pasteur launched an international subscription and opened the world's first research institute dedicated to the prevention of rabies and other infectious diseases. The Institut Pasteur was born. Due to substantial advances during the 20th century, safe and effective human and animal vaccines based on tissue culture methodologies are available today. Today, 125 years later, Institut Pasteur is at the core of an international network of 30 institutes. Through its numerous institutions established in enzootic areas, the worldwide contribution of the Institut Pasteur International Network to rabies surveillance and control is still of paramount importance. Each year, the International Network is responsible for PEP administration to more than 180,000 exposed patients, mainly in Southeast Asia and Africa. Scientists and health staff are also involved in various national committees that work on the development and successful implementation of rabies control programs. Sadly, whereas extensive efforts in developed countries have largely controlled dog (the United States and Europe) and fox (western and central Europe) rabies [2], [3], dog rabies remains enzootic in much of the world, and 15 million people require PEP every year. Rabies is considered one of the most neglected diseases in the world's developing countries with the greatest burden in poor rural communities, and disproportionately in children. According to the World Health Organization (WHO), 30% to 50% of the 55,000 victims estimated each year are individuals under 15 years of age [4]. Over 95% of these human rabies cases are concentrated in Asia (especially in India) and Africa, and 99% of them are transmitted by dogs. There are several reasons for the lack of accurate data, including weak or non-existent rabies surveillance systems; under-reporting of cases by local communities and central authorities; unreliable diagnosis of cases, which is generally based on clinical criteria rather than laboratory confirmation; and inadequate legislation for compulsory notification of cases. The absence of accurate data on disease burden, upon which regional and national priorities for research and control are based, leads to a vicious cycle of indifference and neglect [5]. The question now is why, despite the availability of safe and effective human vaccines, human rabies deaths continue to escalate in many parts of the world. There are several general and specific explanations for the continued burden of dog rabies [6]–[8]. In general, there is a lack of awareness amongst policy-makers of the rabies burden and impacts and the need for prioritizing resources towards its control. Further, despite the widely advocated need for intersectoral collaboration between government ministries, the recognition of roles and responsibilities amongst agencies as well as integration of budgets across ministries still poses considerable challenges. Mass vaccination of dogs is the most cost-effective way to achieve"
}