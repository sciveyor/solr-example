{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000392",
  "doi": "10.1371/journal.pntd.0000392",
  "externalIds": [
    "pii:08-PNTD-RA-0329R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The Population Structure of Glossina palpalis gambiensis from Island and Continental Locations in Coastal Guinea",
  "authors": [
    {
      "name": "Philippe Solano",
      "first": "Philippe",
      "last": "Solano",
      "affiliation": "CIRDES/IRD UMR 177 IRD-CIRAD, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Sophie Ravel",
      "first": "Sophie",
      "last": "Ravel",
      "affiliation": "IRD, UMR 177 IRD-CIRAD, LRCT Campus International de Baillarguet, Montpellier, France"
    },
    {
      "name": "Jeremy Bouyer",
      "first": "Jeremy",
      "last": "Bouyer",
      "affiliation": "CIRDES/CIRAD UMR 15 CIRAD-INRA, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Mamadou Camara",
      "first": "Mamadou",
      "last": "Camara",
      "affiliation": "PNLTHA, Ministère de la Santé, Conakry, Guinea"
    },
    {
      "name": "Moise S. Kagbadouno",
      "first": "Moise S.",
      "last": "Kagbadouno",
      "affiliation": "PNLTHA, Ministère de la Santé, Conakry, Guinea"
    },
    {
      "name": "Naomi Dyer",
      "first": "Naomi",
      "last": "Dyer",
      "affiliation": "Vector Group, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Laetitia Gardes",
      "first": "Laetitia",
      "last": "Gardes",
      "affiliation": "IRD, UMR 177 IRD-CIRAD, LRCT Campus International de Baillarguet, Montpellier, France"
    },
    {
      "name": "Damien Herault",
      "first": "Damien",
      "last": "Herault",
      "affiliation": "IRD, UMR 177 IRD-CIRAD, LRCT Campus International de Baillarguet, Montpellier, France"
    },
    {
      "name": "Martin J. Donnelly",
      "first": "Martin J.",
      "last": "Donnelly",
      "affiliation": "Vector Group, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Thierry De Meeûs",
      "first": "Thierry",
      "last": "De Meeûs",
      "affiliation": "IRD, UMR 177 IRD-CIRAD, LRCT Campus International de Baillarguet, Montpellier, France; GEMI, UMR 2724 IRD/CNRS, Montpellier, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-03",
  "dateAccepted": "2009-02-09",
  "dateReceived": "2008-09-12",
  "volume": "3",
  "number": "3",
  "pages": "e392",
  "tags": [
    "Genetics and Genomics/Population Genetics",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Public Health and Epidemiology"
  ],
  "abstract": "Background\n\nWe undertook a population genetics analysis of the tsetse fly Glossina palpalis gambiensis, a major vector of sleeping sickness in West Africa, using microsatellite and mitochondrial DNA markers. Our aims were to estimate effective population size and the degree of isolation between coastal sites on the mainland of Guinea and Loos Islands. The sampling locations encompassed Dubréka, the area with the highest Human African Trypanosomosis (HAT) prevalence in West Africa, mangrove and savannah sites on the mainland, and two islands, Fotoba and Kassa, within the Loos archipelago. These data are discussed with respect to the feasibility and sustainability of control strategies in those sites currently experiencing, or at risk of, sleeping sickness.\n\nPrincipal Findings\n\nWe found very low migration rates between sites except between those sampled around the Dubréka area that seems to contain a widely dispersed and panmictic population. In the Kassa island samples, various effective population size estimates all converged on surprisingly small values (10&lt;Ne&lt;30) that suggest either a recent bottleneck, and/or other biological or ecological factors such as strong variance in the reproductive success of individuals.\n\nConclusion/Significance\n\nWhatever their origin, the small effective population sizes suggest high levels of inbreeding in tsetse flies within the island samples in marked contrast to the large diffuse deme in Dubréka zones. We discuss how these genetic results suggest that different tsetse control strategies should be applied on the mainland and islands.",
  "fullText": "Introduction Mating pattern, population size and migration represent key factors determining the population genetic structure of organisms and shape the evolutionary of species [1]–[4]. Estimating these parameters is a major objective of population and conservation genetics [5]–[7]. Molecular markers are useful for estimating these parameters without the need for costly capture-mark-release-recapture (MRR) studies; particularly for organisms such as parasites and their vectors, where techniques such as MRR are difficult, impossible or unethical to apply [2],[4]. Furthermore a detailed understanding of parasite and vector population dynamics is crucial for effective sustainable control [2], [8]–[10]. The World Health Organisation recently launched a Human African Trypanosomosis (HAT, or sleeping sickness) elimination programme to counter the recent decline in case detection and treatment, notably in Central Africa [11]–[12]. However the situation in West Africa and the epidemiology of HAT is less well described. Guinea (especially the coastal area) is believed to be the country most affected by this disease [13]. Guinea has a long history of sleeping sickness, which was particularly prevalent in the years 1930–40 [14]. Current data show prevalences of between 2 and 5% in villages in the coastal mangrove area (Dubreka focus) [15]. This coastal area is composed of mangrove on the coastal margins and savannah inland. Offshore but in close proximity to the Dubreka focus lie the Loos Islands. These islands, physically separated from the mainland about 5000 years ago (D. Bazzo, pers. com.) are known to harbour tsetse flies (Glossina palpalis gambiensis (Diptera: Glossinidae), the main vector of HAT in West Africa and the Guinean National Control Programme against HAT recently launched an tsetse elimination programme on the archipelago. To facilitate the work of the elimination programme we used microsatellite and mitochondrial markers to address the following questions which are key to the successful control of tsetse: What is the effective population size in this tsetse species? What is the extent of genetic differentiation between mainland sites, between the islands and the mainland, and between the different islands of Loos archipelago? By answering these questions we hope to improve the design of control strategies in the region, especially with respect to designing and implementing area wide strategies which must target genetically isolated populations if elimination is the objective [16]. Our results suggest that tsetse elimination is a feasible strategy on the Loos islands given both the genetic isolation between island and mainland populations and the small total surface to be controlled, but transmission reduction rather than elimination is more advisable for mainland tsetse populations. Methods Study area The Loos islands are a small archipelago of five islands separated from the mainland of Guinea and the capital Conakry, by 4 km of sea. Three of the islands are inhabited, Kassa, Fotoba and Room in order of decreasing population size, with a total of around 7,000 inhabitants. On Kassa Island, two areas were sampled for tsetse, one in the north and one in the south. The biggest focus of HAT in Guinea, Dubréka, is on the mainland in a mangrove some"
}