{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001040",
  "doi": "10.1371/journal.pntd.0001040",
  "externalIds": [
    "pii:10-PNTD-RA-1588R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Solution Structure of a Repeated Unit of the ABA-1 Nematode Polyprotein Allergen of Ascaris Reveals a Novel Fold and Two Discrete Lipid-Binding Sites",
  "authors": [
    {
      "name": "Nicola A. G. Meenan",
      "first": "Nicola A. G.",
      "last": "Meenan",
      "affiliation": "School of Chemistry, University of Glasgow, Glasgow, Scotland, United Kingdom"
    },
    {
      "name": "Graeme Ball",
      "first": "Graeme",
      "last": "Ball",
      "affiliation": "Edinburgh Biomolecular NMR Unit, School of Chemistry, University of Edinburgh, Edinburgh, Scotland, United Kingdom"
    },
    {
      "name": "Krystyna Bromek",
      "first": "Krystyna",
      "last": "Bromek",
      "affiliation": "Institute of Molecular, Cell and Systems Biology, University of Glasgow, Glasgow, Scotland, United Kingdom"
    },
    {
      "name": "Dušan Uhrín",
      "first": "Dušan",
      "last": "Uhrín",
      "affiliation": "Edinburgh Biomolecular NMR Unit, School of Chemistry, University of Edinburgh, Edinburgh, Scotland, United Kingdom"
    },
    {
      "name": "Alan Cooper",
      "first": "Alan",
      "last": "Cooper",
      "affiliation": "School of Chemistry, University of Glasgow, Glasgow, Scotland, United Kingdom"
    },
    {
      "name": "Malcolm W. Kennedy",
      "first": "Malcolm W.",
      "last": "Kennedy",
      "affiliation": "Institute of Molecular, Cell and Systems Biology, University of Glasgow, Glasgow, Scotland, United Kingdom; Institute of Infection, Immunity and Inflammation, University of Glasgow, Glasgow, Scotland, United Kingdom"
    },
    {
      "name": "Brian O. Smith",
      "first": "Brian O.",
      "last": "Smith",
      "affiliation": "Institute of Molecular, Cell and Systems Biology, University of Glasgow, Glasgow, Scotland, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-04",
  "dateAccepted": "2011-03-18",
  "dateReceived": "2010-11-10",
  "volume": "5",
  "number": "4",
  "pages": "e1040",
  "tags": [
    "Biochemistry",
    "Biochemistry/Biomacromolecule-Ligand Interactions",
    "Biochemistry/Protein Folding",
    "Biophysics",
    "Immunology/Allergy and Hypersensitivity",
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Molecular Biology"
  ],
  "abstract": "Background\n\nNematode polyprotein allergens (NPAs) are an unusual class of lipid-binding proteins found only in nematodes. They are synthesized as large, tandemly repetitive polyproteins that are post-translationally cleaved into multiple copies of small lipid binding proteins with virtually identical fatty acid and retinol (Vitamin A)-binding characteristics. They are probably central to transport and distribution of small hydrophobic compounds between the tissues of nematodes, and may play key roles in nutrient scavenging, immunomodulation, and IgE antibody-based responses in infection. In some species the repeating units are diverse in amino acid sequence, but, in ascarid and filarial nematodes, many of the units are identical or near-identical. ABA-1A is the most common repeating unit of the NPA of Ascaris suum, and is closely similar to that of Ascaris lumbricoides, the large intestinal roundworm of humans. Immune responses to NPAs have been associated with naturally-acquired resistance to infection in humans, and the immune repertoire to them is under strict genetic control.\n\nMethodology/Principal Findings\n\nThe solution structure of ABA-1A was determined by protein nuclear magnetic resonance spectroscopy. The protein adopts a novel seven-helical fold comprising a long central helix that participates in two hollow four-helical bundles on either side. Discrete hydrophobic ligand-binding pockets are found in the N-terminal and C-terminal bundles, and the amino acid sidechains affected by ligand (fatty acid) binding were identified. Recombinant ABA-1A contains tightly-bound ligand(s) of bacterial culture origin in one of its binding sites.\n\nConclusions/Significance\n\nThis is the first mature, post-translationally processed, unit of a naturally-occurring tandemly-repetitive polyprotein to be structurally characterized from any source, and it belongs to a new structural class. NPAs have no counterparts in vertebrates, so represent potential targets for drug or immunological intervention. The nature of the (as yet) unidentified bacterial ligand(s) may be pertinent to this, as will our characterization of the unusual binding sites.",
  "fullText": "Introduction Tandemly repetitive polyproteins (TRPs) are rare in nature. They are produced as large precursor polypeptides, comprising repeated units of similar or identical amino acid sequence that are post-translationally cleaved into a dozen or so copies of functionally similar proteins. Unlike in viral or some neuropeptide and hormone polyproteins, units of TRPs appear to be structurally similar and exhibit similar biochemical activities. The best characterised examples of TRPs are the filaggrins produced by the keratinocytes [1] and the polyprotein allergens of nematode worms (NPAs) [2]. No three-dimensional protein structural information has hitherto been available for a TRP. Repetitive polyproteins represent an efficient and economical means of synthesizing large quantities of a functional protein. Within a single transcript, multiple copies of the functional protein are encoded, interrupted by regularly-spaced proteinase cleavage sites. This economy of synthesis hypothesis is further supported by the fact that TRPs have very small or no introns in most of the genomic region encoding the tandemly repeated units [1], [2], and the post-translational processing may be similar for both the NPAs and the filaggrins [1], [3], [4], [5], [6]. Since the filaggrins are products of terminally-differentiating apoptotic cells, it has been postulated that the synthesis of proteins as TRPs is an adaptation for cells undergoing deteriorating transcription and translation conditions [1]. But, NPAs are produced in large quantities by organisms in whose tissues there is no sign of programmed cell death [2]. Filaggrins are structural components of keratinocytes and are functionally distinct from NPAs, but it is clear that there is no satisfactory explanation for the adaptive value for the synthesis of proteins as TRPs. Perhaps a more interesting question is why more proteins are not synthesized in this way. Although NPAs were first identified in human and animal-parasitic nematodes, they are also produced by plant-parasitic and free-living species [7], [8], in which they are synthesized in the intestinal cells and then exported to the pseudocoelomic fluid and the secretions of the worms [2], [8], [9], [10]. In the NPA precursor, post-translational cleavage occurs at regularly-spaced basic motifs (e.g. Lys/Arg–Xaa–Lys/Arg–Arg), similar to the furin cleavage motif at which cleavage is required for activation of filaggrins and some viral proteins [11], [12], [13]. Several species of disease-causing nematodes, including Ascaris lumbricodes and Brugia malayi of humans and Dictyocaulus viviparus, Ostertagia ostertagi, Haemonchus contortus and Dirofilaria immitis of domestic animals [9], [14], [15], [16], [17], [18], [19] express NPAs that are the target of strong immune responses, often of a type associated with hypersensitivities [4], [20], [21]. Examples of the latter range from the acute pulmonary hypersensitivity responses sometimes observed in Ascaris infection, to the debilitating chronic elephantiasis pathology of lymphatic filariasis (B. malayi). Immune responses to NPAs in infection have been shown to be under strict control of the major histocompatibility complex [22], and there is some evidence that NPAs merit attention for inclusion in vaccines [23]. In the case of humans infected with A. lumbricoides, there is epidemiological data that IgE (allergic-type) antibody responses are associated with"
}