<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD Journal Publishing DTD v3.0 20080202//EN" "http://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd">
<article xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" article-type="research-article" dtd-version="3.0" xml:lang="EN">
  <front>
    <journal-meta><journal-id journal-id-type="nlm-ta">PLoS Negl Trop Dis</journal-id><journal-id journal-id-type="publisher-id">plos</journal-id><journal-id journal-id-type="pmc">plosntds</journal-id><!--===== Grouping journal title elements =====--><journal-title-group><journal-title>PLoS Neglected Tropical Diseases</journal-title></journal-title-group><issn pub-type="epub">1935-2735</issn><publisher>
        <publisher-name>Public Library of Science</publisher-name>
        <publisher-loc>San Francisco, USA</publisher-loc>
      </publisher></journal-meta>
    <article-meta><article-id pub-id-type="publisher-id">PNTD-D-11-00009</article-id><article-id pub-id-type="doi">10.1371/journal.pntd.0001235</article-id><article-categories>
        <subj-group subj-group-type="heading">
          <subject>Editorial</subject>
        </subj-group>
        <subj-group subj-group-type="Discipline-v2">
          <subject>Medicine</subject>
          <subj-group>
            <subject>Infectious diseases</subject>
            <subj-group>
              <subject>Neglected tropical diseases</subject>
            </subj-group>
          </subj-group>
        </subj-group>
        <subj-group subj-group-type="Discipline">
          <subject>Infectious Diseases</subject>
        </subj-group>
      </article-categories><title-group><article-title>Freedom, Justice, and Neglected Tropical Diseases</article-title></title-group><contrib-group>
        <contrib contrib-type="author" xlink:type="simple">
          <name name-style="western">
            <surname>Franco-Paredes</surname>
            <given-names>Carlos</given-names>
          </name>
          <xref ref-type="aff" rid="aff1">
            <sup>1</sup>
          </xref>
          <xref ref-type="corresp" rid="cor1">
            <sup>*</sup>
          </xref>
        </contrib>
        <contrib contrib-type="author" xlink:type="simple">
          <name name-style="western">
            <surname>Santos-Preciado</surname>
            <given-names>Jose I.</given-names>
          </name>
          <xref ref-type="aff" rid="aff2">
            <sup>2</sup>
          </xref>
        </contrib>
      </contrib-group><aff id="aff1"><label>1</label><addr-line>Hospital Infantil de Mexico, Federico Gomez, Mexico, D.F., Mexico, and Rollins School of Public Health, Emory University, Atlanta, Georgia, United States of America</addr-line>       </aff><aff id="aff2"><label>2</label><addr-line>Unidad de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autonoma de Mexico, Mexico, D.F., Mexico</addr-line>       </aff><author-notes>
        <corresp id="cor1">* E-mail: <email xlink:type="simple">carlos.franco.paredes@gmail.com</email></corresp>
      <fn fn-type="conflict">
        <p>The authors have declared that no competing interests exist.</p>
      </fn></author-notes><pub-date pub-type="collection">
        <month>8</month>
        <year>2011</year>
      </pub-date><pub-date pub-type="epub">
        <day>30</day>
        <month>8</month>
        <year>2011</year>
      </pub-date><volume>5</volume><issue>8</issue><elocation-id>e1235</elocation-id><!--===== Grouping copyright info into permissions =====--><permissions><copyright-year>2011</copyright-year><copyright-holder>Franco-Paredes, Santos-Preciado</copyright-holder><license><license-p>This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</license-p></license></permissions><funding-group><funding-statement>The authors received no funding for this work.</funding-statement></funding-group><counts>
        <page-count count="2"/>
      </counts></article-meta>
  </front>
  <body>
    <sec id="s1">
      <title/>
      <disp-quote>
        <p><italic>“What moves us, reasonably enough, is not the realization that the world falls short of being completely just—which few of us expect—but that there are clearly remediable injustices around us which we want to eliminate”</italic> <xref ref-type="bibr" rid="pntd.0001235-Sen1">[1]</xref></p>
      </disp-quote>
      <p>Neglected tropical diseases (NTDs) are remediable injustices of our times. Poverty is the starting point, and the ultimate outcome, of NTDs. Much about poverty is evident enough, but considering poverty as simply low income is insufficient <xref ref-type="bibr" rid="pntd.0001235-Sen2">[2]</xref>. In the context of NTDs, poverty should be seen as the relative deprivation of freedoms and capabilities dictating a lack of opportunities and choices in life <xref ref-type="bibr" rid="pntd.0001235-Sen3">[3]</xref>, <xref ref-type="bibr" rid="pntd.0001235-Sen4">[4]</xref>. Capabilities refer to the person's freedom to lead one type of life or another, and freedom with the real opportunity to accomplish what we value as human beings <xref ref-type="bibr" rid="pntd.0001235-Sen5">[5]</xref>. Thus, NTDs are diseases of socially excluded populations that promote poverty by relatively depriving individuals from basic capabilities and freedoms. The social pathways of becoming ill with an NTD include socially determined failures including widespread illiteracy, malnutrition, poor living conditions, unemployment, and the overall failure of ownership relations in the form of entitlements <xref ref-type="bibr" rid="pntd.0001235-Sen2">[2]</xref>, <xref ref-type="bibr" rid="pntd.0001235-Sen5">[5]</xref>. In turn, in a vicious cycle of destitution and dispossession, NTDs produce disability, disfigurement, stigma, and premature mortality.</p>
      <p>When addressing issues surrounding social equity and justice it becomes inescapable to revisit the writings of Amartya Sen, one of the world's leading intellectuals of our time. Throughout his books, but most emphatically in his recent one, <italic>The Idea of Justice</italic>, he argues for a framework for the critical assessment of judgments about justice whether based on <italic>freedoms</italic>, <italic>capabilities</italic>, <italic>resources</italic>, or <italic>well-being</italic> <xref ref-type="bibr" rid="pntd.0001235-Sen1">[1]</xref>. According to Sen, liberty is defined as the possible fields of application of equality, and equality as the pattern of distribution of liberty; and living may be seen as a set of interrelated functionings, consisting of beings and doings <xref ref-type="bibr" rid="pntd.0001235-Sen5">[5]</xref>. A person's achievements in this respect can be seen as the vector of constitutive functionings, including elementary ones such as being adequately nourished and being in good health <xref ref-type="bibr" rid="pntd.0001235-Sen4">[4]</xref>, <xref ref-type="bibr" rid="pntd.0001235-Sen5">[5]</xref>. The health status of an individual in a specific social arrangement can be scrutinized from two different perspectives: the actual achievement of health, and the freedom to achieve it <xref ref-type="bibr" rid="pntd.0001235-Sen3">[3]</xref>. Health achievement tends to be a reliable guide to the underlying capabilities of an individual and a central consideration of human life. At the same time, the freedoms and capabilities that we are able to exercise in our lives are dependent on our health achievement <xref ref-type="bibr" rid="pntd.0001235-Sen3">[3]</xref>, <xref ref-type="bibr" rid="pntd.0001235-Sen5">[5]</xref>. Taking into account the role of health in human life, social justice calls for a fair distribution as well as efficient creation of human capabilities and opportunities for individuals to achieve and maintain good health <xref ref-type="bibr" rid="pntd.0001235-Sen3">[3]</xref>. Therefore, achieving health free from escapable or preventable illness, disability, and premature death, which occurs with most NTDs, is an integral component of justice in our lives that is directly influenced by our existing freedoms and capabilities <xref ref-type="bibr" rid="pntd.0001235-Sen6">[6]</xref>.</p>
      <p>As human beings, our ability to manage our food supply around 10,000 years ago set in motion a chain of social and cultural development that propelled us into the globalized modern world <xref ref-type="bibr" rid="pntd.0001235-Weiss1">[7]</xref>. Social arrangements of human populations transformed: political systems and economic structures developed; hierarchies, power relations, and social inequalities along with population growth emerged, switching the balance of freedoms and choices. These transitions have produced some benefits, but also some penalties, in the health status of the world's population <xref ref-type="bibr" rid="pntd.0001235-Weiss1">[7]</xref>.</p>
      <p>Indeed, throughout the history of humankind, suffering illnesses has governed our lives. Despite our sophisticated human design, vulnerability to disease is an inevitable part of our developmental origin as individuals and of our evolutionary origin as species <xref ref-type="bibr" rid="pntd.0001235-Nesse1">[8]</xref>. Our biological susceptibility is defined by a myriad of ancestral molecular compromises and trade-offs acquired during our biological history as a human species through ecological clashes with environmental factors <xref ref-type="bibr" rid="pntd.0001235-McCallum1">[9]</xref>. Beyond our biological predisposition, there are social processes that influence the occurrence of illnesses <xref ref-type="bibr" rid="pntd.0001235-Marmot1">[10]</xref>. Regretfully, there is consistent evidence that socially disadvantaged populations have poorer survival chances and premature death due to socially determined diseases than more socially favored groups <xref ref-type="bibr" rid="pntd.0001235-Gruskin1">[11]</xref>. No law in nature decrees that individuals die from diseases that are preventable and treatable such as occurs with most NTDs.</p>
      <p>In a seminal paper in 1992, Margaret Whitehead defined inequity in health as the occurrence of health differences considered unnecessary, avoidable, unfair, and unjust, thus adding a moral and ethical dimension to health inequalities <xref ref-type="bibr" rid="pntd.0001235-Whitehead1">[12]</xref>. Health equity does not refer only to the fairness in the distribution of health or the provision of health care; rather, it is linked with the larger issues of fairness and justice in social arrangements <xref ref-type="bibr" rid="pntd.0001235-Sen4">[4]</xref>. In this regard, many individuals positioned at the bottom of the social ladder find themselves living a life with few choices and few opportunities to avoid becoming ill, receive treatment, and prevent the long-term disability and premature death associated with most NTDs.</p>
      <p>Some notions of nominal political freedoms can be applied to the violation of freedoms leading to negative health outcomes associated with NTDs <xref ref-type="bibr" rid="pntd.0001235-Beyrer1">[13]</xref>. However, other destructive social arrangements place individuals at a risk of becoming ill with an NTD, such as inability to satisfy hunger or achieve sufficient nutrition; lack of freedom to obtain treatments for NTDs; insufficient opportunities to be adequately clothed or sheltered; gender inequalities; unavailability of clean water or sanitary facilities to prevent the acquisition of an NTD; the lack of public facilities and social care including health care and educational facilities; or effective institutions for maintenance of order and peace <xref ref-type="bibr" rid="pntd.0001235-Sen4">[4]</xref>, <xref ref-type="bibr" rid="pntd.0001235-Weiss1">[7]</xref>. Health inequities associated with NTDs systematically place populations at further social disadvantage <xref ref-type="bibr" rid="pntd.0001235-FrancoParedes1">[14]</xref>. For example, in Sri Lanka, lymphatic filariasis is a leading cause of deformity, disfigurement, permanent physical disability, and stigma that promotes social isolation, emotional distress, and delayed diagnosis treatment <xref ref-type="bibr" rid="pntd.0001235-Perera1">[15]</xref>. These factors, in turn, promote loss of productivity and income, which pushes this category of patients with low visibility and their households from extreme poverty to destitution. The life they lead is thus restricted and escaping the trap of poverty becomes distant.</p>
      <p>Similar to the example of lymphatic filariasis in Sri Lanka, a large number of the world's population suffers from NTDs that stem from inadequate social arrangements. These arrangements also impact the distribution of health resources, producing inequalities in the distribution of health care for those ill with an NTD or suffering from the long-term complications of an NTD (social policies linked to wealth, power, and prestige; and social hierarchies that deprive people of the opportunity for receiving or utilizing health resources, the allocation of health care resources, financing of health care; and quality of health care services) <xref ref-type="bibr" rid="pntd.0001235-Sen3">[3]</xref>. In many settings, NTDs affecting indigenous populations represent a historical legacy of social injustices. An example is the impact of the African slave trade in the spread of NTDs in Latin America and the Caribbean leading to a significant burden of disease. In this region, many indigenous populations are disenfranchised and poor, and thus NTDs have been largely forgotten diseases even though their collective disease burden exceeds that of HIV/AIDS, tuberculosis, or malaria <xref ref-type="bibr" rid="pntd.0001235-Hotez1">[16]</xref>.</p>
      <p>The health of people around the world is a global responsibility, and the right to the highest attainable standard of health goes far beyond health care to promote social entitlements and freedoms <xref ref-type="bibr" rid="pntd.0001235-Hunt1">[17]</xref>. Preventing, treating, and rehabilitating those at risk of or suffering from NTDs will promote people's capabilities and opportunities and return a sense of dignity and self-realization into their lives. In this sense, targeting NTDs in a comprehensive fashion represents a clear and feasible poverty alleviation strategy that ultimately fosters social equity. Reducing the burden of NTDs is a grand social intervention to promote social change, advance justice, and increase freedom of marginalized populations. Fairness, as the prospect of mutually advantageous cooperation among equal members of the human species <xref ref-type="bibr" rid="pntd.0001235-DeWaal1">[18]</xref>, is a trait that we should endure. Human suffering stemming from NTDs needs to cease being one of the shadows that delineate social inequity, injustice, and a biological destiny of poverty.</p>
    </sec>
  </body>
  <back>
    <ref-list>
      <title>References</title>
      <ref id="pntd.0001235-Sen1">
        <label>1</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Sen</surname><given-names>A</given-names></name></person-group>             <year>2009</year>             <source>The idea of justice</source>             <publisher-loc>Cambridge (Massachusetts)</publisher-loc>             <publisher-name>The Belknap Press of Harvard University Press</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Sen2">
        <label>2</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Sen</surname><given-names>A</given-names></name></person-group>             <year>1981</year>             <source>Poverty and famines. An essay on entitlement and deprivation</source>             <publisher-loc>Oxford</publisher-loc>             <publisher-name>Oxford University Press</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Sen3">
        <label>3</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Sen</surname><given-names>A</given-names></name></person-group>             <year>2002</year>             <article-title>Why health equity?</article-title>             <source>Health Econ</source>             <volume>11</volume>             <fpage>659</fpage>             <lpage>666</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Sen4">
        <label>4</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Sen</surname><given-names>A</given-names></name></person-group>             <year>2000</year>             <source>Development as freedom</source>             <publisher-loc>New York</publisher-loc>             <publisher-name>Anchor Books</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Sen5">
        <label>5</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Sen</surname><given-names>A</given-names></name></person-group>             <year>1995</year>             <source>Inequality reexamined</source>             <publisher-loc>Cambridge (Massachusetts)</publisher-loc>             <publisher-name>Harvard University Press &amp; Russell Sage Foundation</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Sen6">
        <label>6</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Sen</surname><given-names>A</given-names></name><name name-style="western"><surname>Farmer</surname><given-names>P</given-names></name></person-group>             <year>2003</year>             <article-title>Pathologies of power.</article-title>             <source>Health, human rights, and the new war on the poor</source>             <publisher-loc>Berkeley and Los Angeles</publisher-loc>             <publisher-name>University of California Press</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Weiss1">
        <label>7</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Weiss</surname><given-names>RA</given-names></name><name name-style="western"><surname>McMichael</surname><given-names>AJ</given-names></name></person-group>             <year>2004</year>             <article-title>Social and environmental risk factors in the emergence of infectious diseases.</article-title>             <source>Nat Med</source>             <volume>10</volume>             <supplement>12 Suppl</supplement>             <fpage>370</fpage>             <lpage>376</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Nesse1">
        <label>8</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Nesse</surname><given-names>RM</given-names></name><name name-style="western"><surname>Stearns</surname><given-names>SC</given-names></name></person-group>             <year>2008</year>             <article-title>The great opportunity: evolutionary applications to medicine and public health.</article-title>             <source>Evol App</source>             <fpage>28</fpage>             <lpage>48</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-McCallum1">
        <label>9</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>McCallum</surname><given-names>CJ</given-names></name></person-group>             <year>2007</year>             <article-title>Does medicine without evolution makes sense?</article-title>             <source>PLoS Biol</source>             <volume>5</volume>             <fpage>e112</fpage>             <comment>doi:<ext-link ext-link-type="uri" xlink:href="http://dx.doi.org/10.1371/journal.pbio.0050112" xlink:type="simple">10.1371/journal.pbio.0050112</ext-link></comment>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Marmot1">
        <label>10</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Marmot</surname><given-names>M</given-names></name></person-group>             <year>2006</year>             <article-title>Health in an unequal world.</article-title>             <source>Lancet</source>             <volume>368</volume>             <fpage>2081</fpage>             <lpage>2094</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Gruskin1">
        <label>11</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Gruskin</surname><given-names>S</given-names></name><name name-style="western"><surname>Braveman</surname><given-names>P</given-names></name></person-group>             <year>2003</year>             <article-title>Defining equity in health.</article-title>             <source>J Epidemiol Community Health</source>             <volume>57</volume>             <fpage>254</fpage>             <lpage>258</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Whitehead1">
        <label>12</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Whitehead</surname><given-names>M</given-names></name></person-group>             <year>1992</year>             <article-title>The concepts and principles of equity and health.</article-title>             <source>Int J Health Serv</source>             <volume>22</volume>             <fpage>429</fpage>             <lpage>445</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Beyrer1">
        <label>13</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Beyrer</surname><given-names>C</given-names></name><name name-style="western"><surname>Villar</surname><given-names>JC</given-names></name><name name-style="western"><surname>Suwanvanichkij</surname><given-names>V</given-names></name><name name-style="western"><surname>Snigh</surname><given-names>S</given-names></name><name name-style="western"><surname>Baral</surname><given-names>SD</given-names></name><etal/></person-group>             <year>2007</year>             <article-title>Neglected diseases, civil conflicts, and the right to health.</article-title>             <source>Lancet</source>             <volume>370</volume>             <issue>9587</issue>             <fpage>619</fpage>             <lpage>627</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-FrancoParedes1">
        <label>14</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Franco-Paredes</surname><given-names>C</given-names></name><name name-style="western"><surname>Jones</surname><given-names>D</given-names></name><name name-style="western"><surname>Rodriguez-Morales</surname><given-names>AJ</given-names></name><name name-style="western"><surname>Santos-Preciado</surname><given-names>JI</given-names></name></person-group>             <year>2007</year>             <article-title>Improving the health of neglected populations in Latin America.</article-title>             <source>BMC Public Health</source>             <volume>7</volume>             <fpage>11</fpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Perera1">
        <label>15</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Perera</surname><given-names>M</given-names></name><name name-style="western"><surname>Whitehead</surname><given-names>M</given-names></name><name name-style="western"><surname>Molyneux</surname><given-names>D</given-names></name><name name-style="western"><surname>Weerasooriya</surname><given-names>M</given-names></name><name name-style="western"><surname>Gunatilleke</surname><given-names>G</given-names></name></person-group>             <year>2007</year>             <article-title>Neglected patients with a neglected disease? A qualitative study of lymphatic filariasis.</article-title>             <source>PLoS Neglect Trop Dis</source>             <volume>1</volume>             <fpage>e128</fpage>             <comment>doi:<ext-link ext-link-type="uri" xlink:href="http://dx.doi.org/10.1371/journal.pntd.0000128" xlink:type="simple">10.1371/journal.pntd.0000128</ext-link></comment>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Hotez1">
        <label>16</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Hotez</surname><given-names>PJ</given-names></name><name name-style="western"><surname>Bottazzi</surname><given-names>ME</given-names></name><name name-style="western"><surname>Franco-Paredes</surname><given-names>C</given-names></name><name name-style="western"><surname>Ault</surname><given-names>SK</given-names></name><name name-style="western"><surname>Roses-Periago</surname><given-names>M</given-names></name></person-group>             <year>2008</year>             <article-title>The neglected tropical diseases of Latin America and the Caribbean: estimated disease burden and distribution and a roadmap for control and elimination.</article-title>             <source>PLoS Negl Trop Dis</source>             <volume>2</volume>             <fpage>e300</fpage>             <comment>doi:<ext-link ext-link-type="uri" xlink:href="http://dx.doi.org/10.1371/journal.pntd.0000300" xlink:type="simple">10.1371/journal.pntd.0000300</ext-link></comment>          </element-citation>
      </ref>
      <ref id="pntd.0001235-Hunt1">
        <label>17</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Hunt</surname><given-names>P</given-names></name></person-group>             <year>2006</year>             <article-title>The human right to the highest attainable standard of health: new opportunities and challenges.</article-title>             <source>Trans R Soc Trop Med Hyg</source>             <volume>100</volume>             <fpage>603</fpage>             <lpage>607</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001235-DeWaal1">
        <label>18</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>De Waal</surname><given-names>F</given-names></name></person-group>             <year>2008</year>             <source>Primates and philosophers. How morality evolved</source>             <publisher-loc>Princeton</publisher-loc>             <publisher-name>Princeton University Press</publisher-name>          </element-citation>
      </ref>
    </ref-list>
    
  </back>
</article>