<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD Journal Publishing DTD v3.0 20080202//EN" "http://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd">
<article xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" article-type="discussion" dtd-version="3.0" xml:lang="EN">
<front>
<journal-meta><journal-id journal-id-type="nlm-ta">PLoS Negl Trop Dis</journal-id><journal-id journal-id-type="publisher-id">plos</journal-id><journal-id journal-id-type="pmc">plosntds</journal-id><!--===== Grouping journal title elements =====--><journal-title-group><journal-title>PLoS Neglected Tropical Diseases</journal-title></journal-title-group><issn pub-type="epub">1935-2735</issn><publisher>
<publisher-name>Public Library of Science</publisher-name>
<publisher-loc>San Francisco, USA</publisher-loc></publisher></journal-meta>
<article-meta><article-id pub-id-type="publisher-id">08-PNTD-VP-0024R2</article-id><article-id pub-id-type="doi">10.1371/journal.pntd.0000274</article-id><article-categories><subj-group subj-group-type="heading"><subject>Viewpoints</subject></subj-group><subj-group subj-group-type="Discipline"><subject>Public Health and Epidemiology</subject><subject>Public Health and Epidemiology/Health Services Research and Economics</subject></subj-group></article-categories><title-group><article-title>Strategies for Aspiring Biomedical Researchers in Resource-Limited Environments</article-title></title-group><contrib-group>
<contrib contrib-type="author" xlink:type="simple"><name name-style="western"><surname>Garcia</surname><given-names>Patricia J.</given-names></name><xref ref-type="aff" rid="aff1"><sup>1</sup></xref><xref ref-type="corresp" rid="cor1"><sup>*</sup></xref></contrib>
<contrib contrib-type="author" xlink:type="simple"><name name-style="western"><surname>Curioso</surname><given-names>Walter H.</given-names></name><xref ref-type="aff" rid="aff1"><sup>1</sup></xref><xref ref-type="aff" rid="aff2"><sup>2</sup></xref></contrib>
</contrib-group><aff id="aff1"><label>1</label><addr-line>School of Public Health and Administration, Universidad Peruana Cayetano Heredia, Lima, Peru</addr-line>       </aff><aff id="aff2"><label>2</label><addr-line>School of Medicine, Universidad Peruana Cayetano Heredia, Lima, Peru</addr-line>       </aff><contrib-group>
<contrib contrib-type="editor" xlink:type="simple"><name name-style="western"><surname>Brooker</surname><given-names>Simon</given-names></name>
<role>Editor</role>
<xref ref-type="aff" rid="edit1"/></contrib>
</contrib-group><aff id="edit1">London School of Hygiene &amp; Tropical Medicine, United Kingdom</aff><author-notes>
<corresp id="cor1">* E-mail: <email xlink:type="simple">pattyg@u.washington.edu</email></corresp>
<fn fn-type="conflict"><p>The authors have declared that no competing interests exist.</p></fn></author-notes><pub-date pub-type="collection"><month>8</month><year>2008</year></pub-date><pub-date pub-type="epub"><day>27</day><month>8</month><year>2008</year></pub-date><volume>2</volume><issue>8</issue><elocation-id>e274</elocation-id><!--===== Grouping copyright info into permissions =====--><permissions><copyright-year>2008</copyright-year><copyright-holder>Garcia, Curioso</copyright-holder><license><license-p>This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</license-p></license></permissions><funding-group><funding-statement>This article was supported in part by the Global Health Demonstration Program in Peru (grant 5R25TW007490) and by grant 1R01TW007896, funded by the Fogarty International Center/National Institutes of Health. The funders had no role in study design, data collection and analysis, decision to publish, or preparation of the manuscript.</funding-statement></funding-group><counts><page-count count="3"/></counts></article-meta>
</front>
<body><sec id="s1">
<title>Introduction</title>
<p>Countries struggling with global health challenges desperately need local biomedical researchers to find health care solutions to address the deadly diseases that affect their populations. Building the scientific capacity of resource-limited countries is a clear priority among the scientific community <xref ref-type="bibr" rid="pntd.0000274-Bhutta1">[1]</xref>–<xref ref-type="bibr" rid="pntd.0000274-Garner1">[3]</xref>.</p>
<p>As the Global Forum for Health Research Report stated <xref ref-type="bibr" rid="pntd.0000274-Global1">[4]</xref>, “Strengthening research capacity in developing countries is one of the most effective and sustainable ways of advancing health and development in these countries and of helping correct the 10/90 gap in health research.” The 10/90 gap refers to the statistical finding of the Global Forum for Health Research that only 10% of all global health research funding is directed to research on the health problems that affect 90% of the world's population <xref ref-type="bibr" rid="pntd.0000274-Harris1">[5]</xref>.</p>
<p>Great efforts are now being made to correct this gap, and some call it the golden age of global health. Many researchers in resource-limited environments have the opportunity to train outside their country and are offered scholarships to do so, with the goal that they will return and help their country. For researchers willing to deal with developing world challenges (poor infrastructure and support), there are exciting opportunities to solve a nation's most pressing health problems and make a name for themselves along the way.</p>
<p>In this paper, we will share some key strategies for aspiring biomedical researchers based on the experiences of researchers affiliated with Universidad Peruana Cayetano Heredia (UPCH) in Lima, Peru—a major hub for global health training in South America.</p>
</sec><sec id="s2">
<title>Discussion: Strategies for Aspiring Biomedical Researchers</title>
<p>Faced with a lack of research funding, poor research facilities, limited career opportunities, threats of violence, and a culture not supportive of evidence-based decision-making, it's easy to see why fleeing the country and joining the brain drain is a real possibility <xref ref-type="bibr" rid="pntd.0000274-Kupfer1">[6]</xref>,<xref ref-type="bibr" rid="pntd.0000274-Kirigia1">[7]</xref>. But even in countries with the most limited resources, high-quality and meaningful research can be done <xref ref-type="bibr" rid="pntd.0000274-Sparks1">[8]</xref>, and the results may not be highly appreciated but can be highly visible.</p>
<p>Conducting research in such settings takes lots of patience. Approvals are cumbersome and time-consuming, and additional, unexpected approvals might be required later in the process. And there are often high-level changes and turnover in governmental offices, which can directly affect research projects. While science is increasingly performed by research groups rather than by individual scientists <xref ref-type="bibr" rid="pntd.0000274-Seglen1">[9]</xref>, and researchers often benefit from working as part of a multidisciplinary team <xref ref-type="bibr" rid="pntd.0000274-Hara1">[10]</xref>, working in teams is not easy. Differences can cause team members to contest or challenge one another's contributions, although these differences also enrich collaboration <xref ref-type="bibr" rid="pntd.0000274-Hara1">[10]</xref>. Cooley suggests that interdisciplinary teams can experience problems with group interaction because of a lack of organizational procedures, miscommunication, misunderstanding, and inadequate commitment <xref ref-type="bibr" rid="pntd.0000274-Cooley1">[11]</xref>, so it's important to take action to keep the project moving forward</p>
</sec><sec id="s3">
<title>Strategies to Apply</title>
<sec id="s3a">
<title>Partnerships/Collaborations</title>
<p>For new biomedical researchers, the best way to find collaborators is by asking those in your country who have conducted research or who have published with other colleagues. Professors at medical schools are often principal investigators in clinical trials and will recruit doctors to work with them for little or no money. Find out what outside universities these professors are linked with to see how connected they are to outside collaborators. There are also networks of researchers that you can consult for advice. For example, at UPCH we developed a Global Health Task Force made up of professors and researchers from different schools (medicine, public health, sciences, education, dentistry, and veterinarian medicine) interested in the advancement of global health in Peru <xref ref-type="bibr" rid="pntd.0000274-VillafuerteGalvez1">[12]</xref>. This group meets periodically to discuss curriculum issues on global health to be taught at UPCH. Collaborating with policy makers is also vital because it ensures relevance of research priorities and application of findings.</p>
</sec><sec id="s3b">
<title>Focus</title>
<p>Look for a topic you would like to spend most of your life working on so you will be passionate about the study. Each project is like a brick for building bigger projects in this area, and results from an initial study will help you get funding for other studies. Studies that address the major burdens of disease in your own country, such as malnutrition, tuberculosis, and HIV/AIDS, may be given funding preference, but consider other areas that may be important in global health.</p>
</sec><sec id="s3c">
<title>Innovation</title>
<p>With limited resources, you have to know how to do more with less, but this allows you to be innovative. For example, when a Bolivian researcher wanted to create a microcentrifuge, he used a blender, aluminum bowl, and water-tap adapters <xref ref-type="bibr" rid="pntd.0000274-Harris1">[5]</xref>. In Peru, scientist Leon-Barua developed a system to diagnose meteorism (a distention of the abdomen resulting from the accumulation of gas or air in the intestine or peritoneal cavity) in people by using a simple system of three vases to study feces <xref ref-type="bibr" rid="pntd.0000274-LeonBarua1">[13]</xref>,<xref ref-type="bibr" rid="pntd.0000274-LeonBarua2">[14]</xref>.</p>
</sec><sec id="s3d">
<title>Searching for Funding</title>
<p>Funding is by far a researcher's biggest challenge, but there are resources. Box 1 shows selected funding agencies in international health. The Community of Science (<ext-link ext-link-type="uri" xlink:href="http://fundingopps.cos.com/" xlink:type="simple">http://fundingopps.cos.com/</ext-link>) has a searchable database representing more than 400,000 funding opportunities worth more than US$33 billion. At the midway point through a current grant, or even before, always keep looking for more funding opportunities. Many times funds are available (specially at national agencies) but go unused because there is a lack of good proposals. Don't get discouraged; researchers all over the world have problems getting funding.</p>
</sec><sec id="s3e">
<title>Box 1. List of Selected Funding Agencies in International Health</title>
<sec id="s3e1">
<title>International Agencies</title>
<list list-type="bullet"><list-item>
<p>Fogarty International Center/US National Institutes of Health (<ext-link ext-link-type="uri" xlink:href="http://www.fic.nih.gov/" xlink:type="simple">http://www.fic.nih.gov/</ext-link>)</p>
</list-item><list-item>
<p>The Bill &amp; Melinda Gates Foundation (<ext-link ext-link-type="uri" xlink:href="http://www.gatesfoundation.org/" xlink:type="simple">http://www.gatesfoundation.org/</ext-link>)</p>
</list-item><list-item>
<p>The Canadian International Development Agency (<ext-link ext-link-type="uri" xlink:href="http://www.acdi-cida.gc.ca/" xlink:type="simple">http://www.acdi-cida.gc.ca/</ext-link>)</p>
</list-item><list-item>
<p>The Department for International Development (<ext-link ext-link-type="uri" xlink:href="http://www.dfid.gov.uk/" xlink:type="simple">http://www.dfid.gov.uk/</ext-link>)</p>
</list-item><list-item>
<p>The Global Fund (<ext-link ext-link-type="uri" xlink:href="http://www.theglobalfund.org/" xlink:type="simple">http://www.theglobalfund.org/</ext-link>)</p>
</list-item><list-item>
<p>The International Development Research Centre (<ext-link ext-link-type="uri" xlink:href="http://www.idrc.ca/" xlink:type="simple">http://www.idrc.ca/</ext-link>)</p>
</list-item><list-item>
<p>The Pan American Health Organization (<ext-link ext-link-type="uri" xlink:href="http://www.paho.org/" xlink:type="simple">http://www.paho.org/</ext-link>)</p>
</list-item><list-item>
<p>The Rockefeller Foundation (<ext-link ext-link-type="uri" xlink:href="http://www.rockfound.org/" xlink:type="simple">http://www.rockfound.org/</ext-link>)</p>
</list-item><list-item>
<p>The Wellcome Trust (<ext-link ext-link-type="uri" xlink:href="http://www.wellcome.ac.uk/" xlink:type="simple">http://www.wellcome.ac.uk/</ext-link>)</p>
</list-item><list-item>
<p>The World Bank (<ext-link ext-link-type="uri" xlink:href="http://www.worldbank.org/" xlink:type="simple">http://www.worldbank.org/</ext-link>)</p>
</list-item><list-item>
<p>United States Agency for International Development (<ext-link ext-link-type="uri" xlink:href="http://www.usaid.gov/" xlink:type="simple">http://www.usaid.gov/</ext-link>)</p>
</list-item><list-item>
<p>World Health Organization's Special Programme for Research and Training in Tropical Diseases (<ext-link ext-link-type="uri" xlink:href="http://www.who.int/tdr/" xlink:type="simple">http://www.who.int/tdr/</ext-link>)</p>
</list-item></list>
</sec><sec id="s3e2">
<title>National Agencies</title>
<list list-type="bullet"><list-item>
<p>National Institute of Health of your own country (e.g., the Peruvian National Institute of Health, available at: <ext-link ext-link-type="uri" xlink:href="http://www.ins.gob.pe/" xlink:type="simple">http://www.ins.gob.pe/</ext-link>)</p>
</list-item><list-item>
<p>National Councils of Science and Technology of your own country (e.g., the Peruvian National Council of Science and Technology, available at: <ext-link ext-link-type="uri" xlink:href="http://www.concytec.gob.pe/" xlink:type="simple">http://www.concytec.gob.pe/</ext-link>)</p>
</list-item><list-item>
<p>Some nongovernmental organizations fund proposals, depending on the topic of interest</p>
</list-item></list>
</sec></sec><sec id="s3f">
<title>Sharing</title>
<p>Share your work in every possible way. Present at national and international conferences and show how to apply your research findings in-country. According to a survey of physicians in secondary and tertiary hospitals and developing countries, health care institutions are more likely to change practices if the research is locally based <xref ref-type="bibr" rid="pntd.0000274-Page1">[15]</xref>. Know how to talk about your project in an understandable fashion that inspires your audience, especially authorities, policy makers, and funding agencies. Academic professionals in developing countries tend to work in relative isolation from primary care settings, and fewer still interact with public health policy makers <xref ref-type="bibr" rid="pntd.0000274-Krishnan1">[16]</xref>, so make an effort to share with these crucial audiences.</p>
</sec><sec id="s3g">
<title>Publishing</title>
<p>Publishing is one of the best ways to share your results with the international community and the way your scientific production will be measured. Usually you do not receive funds for publishing, but it does help your chances of getting future funding. If you don't publish, the likelihood of getting funded in the future is low. The most important thing is to get your ideas on paper, in whatever form, as a starting point. It doesn't have to be pretty or in good English—just get the concepts down to begin with and be patient with the editing process. You can publish in your own language, but it is essential to submit to an international journal as well. Before you start writing, decide the order of authors and how each person will contribute. Ask for help from others and don't get discouraged. People are more than happy to edit.</p>
</sec><sec id="s3h">
<title>Teaching</title>
<p>Teaching is important, but do not let it overwhelm you. If you are in an academic environment, try to negotiate the time you need to conduct your research and point out the overhead or administrative funding that the researcher brings to the university when he/she gets a grant. Don't let teaching take over too much of your research time and duties, and include your research in your teaching. Consider mentoring another researcher; if you are a new faculty member, look for a mentor for yourself. One of the best ways to find a mentor is by asking those inside or outside your university who have conducted research or published on the same topic as you.</p>
</sec><sec id="s3i">
<title>Promotion</title>
<p>Promote an open discussion of research in your institution, including the role of research in improving public health in the community. Promote capacity development through informatics grants, training grants, and other grants that aim to build up a laboratory or library. Promote and organize seminars disseminating your research so you can attract potential collaborators. Create a Web site to promote your research team, including an English version.</p>
</sec></sec><sec id="s4">
<title>Issues That Might Arise</title>
<sec id="s4a">
<title>Ethical Issues</title>
<p>In many resource-limited environments, ethical issues related to human research participants might be difficult and time-consuming, but the protection of human participants is an important topic and better practices are needed in research work <xref ref-type="bibr" rid="pntd.0000274-Zumla1">[17]</xref>. In addition, many places lack institutional review boards, and there are not many trained personnel to deal with these issues. The Fogarty Bioethics Program (<ext-link ext-link-type="uri" xlink:href="http://www.fic.nih.gov/programs/training_grants/bioethics/index.htm" xlink:type="simple">http://www.fic.nih.gov/programs/training_grants/bioethics/index.htm</ext-link>) is one of the few programs where you can get training in ethics. Try to organize a course, symposium, or conference to address ethical issues.</p>
</sec><sec id="s4b">
<title>Administrative Support</title>
<p>Human resources and infrastructure for the administrative, governance, financial, and management functions are key for institutions to deliver research excellence. You might find administrative support within your research facility. If not, try to create your own administrative system as an example to encourage your institution to create an official system. For example, the research and development project office at UPCH (a division of the Vice-Rectorate of Research) was created in 2002 and provides administrative support to researchers at the university (<ext-link ext-link-type="uri" xlink:href="http://www.upch.edu.pe/vrinve/" xlink:type="simple">http://www.upch.edu.pe/vrinve/</ext-link>).</p>
</sec><sec id="s4c">
<title>Gender Issues, Family, and Research</title>
<p>Women often face special challenges as aspiring researchers, due not only to cultural issues but also to additional responsibilities like motherhood. Women might get together and form informal support groups for each other, or tenured women faculty might “adopt” junior women and guide them through tenure, helping to keep them focused. Create a support network.</p>
</sec><sec id="s4d">
<title>Incentives to Return</title>
<p>Finally, institutions could apply some strategies to encourage researchers to return after training and stay in a developing setting. For example, institutions affiliated with the AIDS International Training and Research Program ask trainees to sign a “condition of appointment” or a return agreement prior to training <xref ref-type="bibr" rid="pntd.0000274-Kupfer1">[6]</xref>. This can be a valuable tool to encourage trainees to return home. However, this strategy cannot guarantee a researcher will return. One idea to encourage researchers to return is to promote re-entry grants, which are funds to help researchers to re-establish themselves and find their niches after some time in other countries.</p>
<p>Some institutions, like UPCH, institutionalized a grant for Ph.D.s returning to Peru to work at the university, which pays their salary for one to two years at a relatively high level compared to other university positions. In exchange, these returning Ph.D.s commit to working in research and looking for funding opportunities. We have found this experience very successful in transitioning these valuable researchers back to their home country.</p>
</sec></sec><sec id="s5">
<title>Conclusion</title>
<p>While starting out in research is not easy, it's always rewarding. Ask yourself: how do you see yourself in five to ten years, professionally? What would you need to do to reach that goal? Part of the process is to develop a critical mass of peer researchers that could push forward all the different activities to promote research in your environment (i.e., training, ethics, administration, advocacy, communication, fundraising, etc). Define some benchmarks. Remember that while this work can be a struggle and time-consuming, success is only that much sweeter. And any progress, no matter how small, is a step forward in building the health of a nation.</p>
</sec><sec id="s6">
<title>Supporting Information</title>
<supplementary-material id="pntd.0000274.s001" mimetype="application/msword" position="float" xlink:href="info:doi/10.1371/journal.pntd.0000274.s001" xlink:type="simple"><label>Alternative Language Abstract S1</label><caption>
<p>Spanish Translation of the Abstract by Walter H. Curioso</p>
<p>(0.03 MB DOC)</p>
</caption></supplementary-material></sec></body>
<back>
<ack>
<p>We thank Bobbi Nodell, MA, communications specialist at the University of Washington Department of Global Health, for editorial assistance.</p>
</ack>
<ref-list>
<title>References</title>
<ref id="pntd.0000274-Bhutta1"><label>1</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Bhutta</surname><given-names>Z</given-names></name>
</person-group>             <year>2003</year>             <article-title>Practising just medicine in an unjust world.</article-title>             <source>BMJ</source>             <volume>327</volume>             <fpage>1000</fpage>             <lpage>1001</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Costello1"><label>2</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Costello</surname><given-names>A</given-names></name>
<name name-style="western"><surname>Zumla</surname><given-names>A</given-names></name>
</person-group>             <year>2000</year>             <article-title>Moving to research partnerships in developing countries.</article-title>             <source>BMJ</source>             <volume>321</volume>             <fpage>827</fpage>             <lpage>829</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Garner1"><label>3</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Garner</surname><given-names>P</given-names></name>
<name name-style="western"><surname>Kale</surname><given-names>R</given-names></name>
<name name-style="western"><surname>Dickson</surname><given-names>R</given-names></name>
<name name-style="western"><surname>Dans</surname><given-names>T</given-names></name>
<name name-style="western"><surname>Salinas</surname><given-names>R</given-names></name>
</person-group>             <year>1998</year>             <article-title>Getting research findings into practice: Implementing research findings in developing countries.</article-title>             <source>BMJ</source>             <volume>317</volume>             <fpage>531</fpage>             <lpage>535</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Global1"><label>4</label><element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">Global Forum for Health Research</collab>             <year>2004</year>             <article-title>10/90 report on health research 2003–2004. World Health Organization.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://www.globalforumhealth.org/Site/002__What20we20do/005__Publications/001__10209020reports.php" xlink:type="simple">http://www.globalforumhealth.org/Site/002__What20we20do/005__Publications/001__10209020reports.php</ext-link>. Accessed 29 July 2008</comment>          </element-citation></ref>
<ref id="pntd.0000274-Harris1"><label>5</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Harris</surname><given-names>E</given-names></name>
</person-group>             <year>2004</year>             <article-title>Building scientific capacity in developing countries.</article-title>             <source>EMBO Rep</source>             <volume>5</volume>             <fpage>7</fpage>             <lpage>11</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Kupfer1"><label>6</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Kupfer</surname><given-names>L</given-names></name>
<name name-style="western"><surname>Hofman</surname><given-names>K</given-names></name>
<name name-style="western"><surname>Jarawan</surname><given-names>R</given-names></name>
<name name-style="western"><surname>McDermott</surname><given-names>J</given-names></name>
<name name-style="western"><surname>Bridbord</surname><given-names>K</given-names></name>
</person-group>             <year>2004</year>             <article-title>Roundtable. Strategies to discourage brain drain.</article-title>             <source>Bull World Health Organ</source>             <volume>82</volume>             <fpage>616</fpage>             <lpage>619</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Kirigia1"><label>7</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Kirigia</surname><given-names>JM</given-names></name>
<name name-style="western"><surname>Gbary</surname><given-names>AR</given-names></name>
<name name-style="western"><surname>Muthuri</surname><given-names>LK</given-names></name>
<name name-style="western"><surname>Nyoni</surname><given-names>J</given-names></name>
<name name-style="western"><surname>Seddoh</surname><given-names>A</given-names></name>
</person-group>             <year>2006</year>             <article-title>The cost of health professionals' brain drain in Kenya.</article-title>             <source>BMC Health Serv Res</source>             <volume>6</volume>             <fpage>89</fpage>          </element-citation></ref>
<ref id="pntd.0000274-Sparks1"><label>8</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Sparks</surname><given-names>BL</given-names></name>
<name name-style="western"><surname>Gupta</surname><given-names>SK</given-names></name>
</person-group>             <year>2004</year>             <article-title>Research in family medicine in developing countries.</article-title>             <source>Ann Fam Med</source>             <volume>2</volume><supplement>(Suppl 2)</supplement>             <fpage>S55</fpage>             <lpage>S59</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Seglen1"><label>9</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Seglen</surname><given-names>PP</given-names></name>
<name name-style="western"><surname>Aksnes</surname><given-names>DW</given-names></name>
</person-group>             <year>2000</year>             <article-title>Scientific productivity and group size: A bibliometric analysis of Norwegian microbiological research.</article-title>             <source>Scientometrics</source>             <volume>49</volume>             <fpage>125</fpage>             <lpage>143</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Hara1"><label>10</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Hara</surname><given-names>N</given-names></name>
<name name-style="western"><surname>Solomon</surname><given-names>P</given-names></name>
<name name-style="western"><surname>Kim</surname><given-names>S</given-names></name>
<name name-style="western"><surname>Sonnenwald</surname><given-names>DH</given-names></name>
</person-group>             <year>2003</year>             <article-title>An emerging view of scientific collaboration: Scientists' perspectives on collaboration and factors that impact collaboration.</article-title>             <source>J Am Soc Inf Sci Technol</source>             <volume>54</volume>             <fpage>952</fpage>             <lpage>965</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Cooley1"><label>11</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Cooley</surname><given-names>E</given-names></name>
</person-group>             <year>1994</year>             <article-title>Training interdisciplinary team in communication and decision making skills.</article-title>             <source>Small Group Res</source>             <volume>25</volume>             <fpage>5</fpage>             <lpage>25</lpage>          </element-citation></ref>
<ref id="pntd.0000274-VillafuerteGalvez1"><label>12</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Villafuerte-Galvez</surname><given-names>J</given-names></name>
<name name-style="western"><surname>Curioso</surname><given-names>WH</given-names></name>
</person-group>             <year>2007</year>             <article-title>Teaching global health at the frontlines.</article-title>             <source>PLoS Med</source>             <volume>4</volume>             <fpage>e130</fpage>             <comment>doi:10.1371/journal.pmed.0040130</comment>          </element-citation></ref>
<ref id="pntd.0000274-LeonBarua1"><label>13</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Leon-Barua</surname><given-names>R</given-names></name>
<name name-style="western"><surname>Zapata-Solari</surname><given-names>C</given-names></name>
</person-group>             <year>1977</year>             <article-title>Fecal fermentation in meteorism.</article-title>             <source>Acta Gastroenterol Latinoam</source>             <volume>7</volume>             <fpage>251</fpage>             <lpage>259</lpage>          </element-citation></ref>
<ref id="pntd.0000274-LeonBarua2"><label>14</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Leon-Barua</surname><given-names>R</given-names></name>
</person-group>             <year>1999</year>             <article-title>[From diagnosis to investigation in medicine]. [Article in Spanish].</article-title>             <source>Diagnóstico</source>             <volume>38</volume>             <fpage>293</fpage>             <lpage>296</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Page1"><label>15</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Page</surname><given-names>J</given-names></name>
<name name-style="western"><surname>Heller</surname><given-names>RF</given-names></name>
<name name-style="western"><surname>Kinlay</surname><given-names>S</given-names></name>
<name name-style="western"><surname>Lim</surname><given-names>LL</given-names></name>
<name name-style="western"><surname>Qian</surname><given-names>W</given-names></name>
<etal/></person-group>             <year>2003</year>             <article-title>Attitudes of developing world physicians to where medical research is performed and reported.</article-title>             <source>BMC Public Health</source>             <volume>3</volume>             <fpage>6</fpage>          </element-citation></ref>
<ref id="pntd.0000274-Krishnan1"><label>16</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Krishnan</surname><given-names>P</given-names></name>
</person-group>             <year>1992</year>             <article-title>Medical education.</article-title>             <source>Health Millions</source>             <volume>18</volume>             <fpage>42</fpage>             <lpage>44</lpage>          </element-citation></ref>
<ref id="pntd.0000274-Zumla1"><label>17</label><element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author">
<name name-style="western"><surname>Zumla</surname><given-names>A</given-names></name>
<name name-style="western"><surname>Costello</surname><given-names>A</given-names></name>
</person-group>             <year>2002</year>             <article-title>Ethics of healthcare research in developing countries.</article-title>             <source>J R Soc Med</source>             <volume>95</volume>             <fpage>275</fpage>             <lpage>276</lpage>          </element-citation></ref>
</ref-list>

</back>
</article>