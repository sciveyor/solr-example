<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD Journal Publishing DTD v3.0 20080202//EN" "http://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd">
<article xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" article-type="research-article" dtd-version="3.0" xml:lang="EN">
  <front>
    <journal-meta><journal-id journal-id-type="nlm-ta">PLoS Negl Trop Dis</journal-id><journal-id journal-id-type="publisher-id">plos</journal-id><journal-id journal-id-type="pmc">plosntds</journal-id><!--===== Grouping journal title elements =====--><journal-title-group><journal-title>PLoS Neglected Tropical Diseases</journal-title></journal-title-group><issn pub-type="epub">1935-2735</issn><publisher>
        <publisher-name>Public Library of Science</publisher-name>
        <publisher-loc>San Francisco, USA</publisher-loc>
      </publisher></journal-meta>
    <article-meta><article-id pub-id-type="publisher-id">PNTD-D-11-00102</article-id><article-id pub-id-type="doi">10.1371/journal.pntd.0001390</article-id><article-categories>
        <subj-group subj-group-type="heading">
          <subject>Viewpoints</subject>
        </subj-group>
        <subj-group subj-group-type="Discipline-v2">
          <subject>Medicine</subject>
          <subj-group>
            <subject>Drugs and devices</subject>
          </subj-group>
          <subj-group>
            <subject>Global health</subject>
          </subj-group>
          <subj-group>
            <subject>Infectious diseases</subject>
          </subj-group>
        </subj-group>
        <subj-group subj-group-type="Discipline">
          <subject>Infectious Diseases</subject>
        </subj-group>
      </article-categories><title-group><article-title>Improving Access to Medicines for Neglected Tropical Diseases in Developing Countries: Lessons from Three Emerging Economies</article-title></title-group><contrib-group>
        <contrib contrib-type="author" xlink:type="simple">
          <name name-style="western">
            <surname>Holt</surname>
            <given-names>Francesca</given-names>
          </name>
          <xref ref-type="aff" rid="aff1">
            <sup>1</sup>
          </xref>
          <xref ref-type="corresp" rid="cor1">
            <sup>*</sup>
          </xref>
        </contrib>
        <contrib contrib-type="author" xlink:type="simple">
          <name name-style="western">
            <surname>Gillam</surname>
            <given-names>Stephen J.</given-names>
          </name>
          <xref ref-type="aff" rid="aff2">
            <sup>2</sup>
          </xref>
        </contrib>
        <contrib contrib-type="author" xlink:type="simple">
          <name name-style="western">
            <surname>Ngondi</surname>
            <given-names>Jeremiah M.</given-names>
          </name>
          <xref ref-type="aff" rid="aff2">
            <sup>2</sup>
          </xref>
        </contrib>
      </contrib-group><aff id="aff1"><label>1</label><addr-line>St John's College, University of Cambridge, Cambridge, United Kingdom</addr-line>       </aff><aff id="aff2"><label>2</label><addr-line>Department of Public Health and Primary Care, University of Cambridge, Cambridge, United Kingdom</addr-line>       </aff><contrib-group>
        <contrib contrib-type="editor" xlink:type="simple">
          <name name-style="western">
            <surname>Correa-Oliveira</surname>
            <given-names>Rodrigo</given-names>
          </name>
          <role>Editor</role>
          <xref ref-type="aff" rid="edit1"/>
        </contrib>
      </contrib-group><aff id="edit1">René Rachou Research Center, Brazil</aff><author-notes>
        <corresp id="cor1">* E-mail: <email xlink:type="simple">fh265@cam.ac.uk</email></corresp>
      <fn fn-type="conflict">
        <p>The authors have declared that no competing interests exist.</p>
      </fn></author-notes><pub-date pub-type="collection">
        <month>2</month>
        <year>2012</year>
      </pub-date><pub-date pub-type="epub">
        <day>28</day>
        <month>2</month>
        <year>2012</year>
      </pub-date><volume>6</volume><issue>2</issue><elocation-id>e1390</elocation-id><!--===== Grouping copyright info into permissions =====--><permissions><copyright-year>2012</copyright-year><copyright-holder>Holt et al</copyright-holder><license><license-p>This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</license-p></license></permissions><funding-group><funding-statement>The authors have indicated that no funding was received for this work.</funding-statement></funding-group><counts>
        <page-count count="3"/>
      </counts></article-meta>
  </front>
  <body>
    <sec id="s1">
      <title>Introduction</title>
      <p>Neglected tropical diseases (NTDs) are those largely ignored by medical science, partly because they do not represent a viable commercial market for private pharmaceutical companies. These diseases are endemic in developing countries and have a significant impact at both personal and national levels. Globally, NTDs affect an estimated 2.7 billion people living on less than US$2 per day <xref ref-type="bibr" rid="pntd.0001390-Hotez1">[1]</xref> and potently reinforce the poverty cycle <xref ref-type="bibr" rid="pntd.0001390-Hotez2">[2]</xref>. At present, the prevailing strategy for improving access to medicines for these NTDs is drug donation programmes, which, despite providing some of the highest economic returns of public health programmes at 15%–30% <xref ref-type="bibr" rid="pntd.0001390-Hotez2">[2]</xref>, <xref ref-type="bibr" rid="pntd.0001390-Conteh1">[3]</xref>, have uncertain sustainability. Countries in demographic and economic transition are uniquely poised to be leaders in a shift towards a more sustainable, affordable means of providing access to medicines for NTDs.</p>
    </sec>
    <sec id="s2">
      <title>The Position of the Emerging Economies</title>
      <p>China, India, and Brazil are three of the largest of more than 20 countries in economic and demographic transition commonly referred to as emerging economies <xref ref-type="bibr" rid="pntd.0001390-Jain1">[4]</xref>. Along with Russia, they are referred to as the “BRIC” countries and are predicted to overtake the G6 countries (United States, Japan, United Kingdom, Germany, France, and Italy) in terms of gross domestic product (GDP) by 2040 <xref ref-type="bibr" rid="pntd.0001390-Wilson1">[5]</xref>. Their pharmaceutical industries are concomitantly booming; the pharmaceutical market in China is expected to drive US$40 billion in growth through 2013, whilst Brazil and India are each expected to add US$5–15 billion to the pharmaceutical market <xref ref-type="bibr" rid="pntd.0001390-Campbell1">[6]</xref>.</p>
      <p>However, in terms of disease burden and health care, these countries continue to suffer many of the problems found in developing countries, including NTDs. Brazil is estimated to be afflicted by six of the NTDs, whilst China and India are thought to harbour four <xref ref-type="bibr" rid="pntd.0001390-World1">[7]</xref>. Furthermore, India, China, and the Americas account for 59% of the world population without access to essential medicines <xref ref-type="bibr" rid="pntd.0001390-Leach1">[8]</xref>. But these countries are unique in that they have managed to cultivate their own pharmaceutical industry with the capacity to produce affordable drugs for NTDs for use in domestic and international markets.</p>
      <p>A review by Frew et al. identified 78 “innovative home-grown, small to medium-size health biotechnology companies in the emerging economies in Brazil, China, India and South Africa”. These “had a collective pipeline of nearly 500 products for more than 100 indications”. Of the products for HIV, malaria, tuberculosis, and NTDs, two-fifths (40.5%) of the products on the market and 62.9% of the products in development were for NTDs <xref ref-type="bibr" rid="pntd.0001390-Frew1">[9]</xref>. For example, Fungisome, a liposomal amphotericin B used to treat visceral leishmaniasis, was developed by an Indian company, Lifecare Innovations. Dermacerium, a topical agent used in the treatment or prophylaxis of skin infections in patients with leprosy, is produced by Silvestre Labs, Brazil <xref ref-type="bibr" rid="pntd.0001390-Frew1">[9]</xref>. Clearly, these countries recognise the worth of investment in developing medicinal tools to combat NTDs and have the capacity to do so.</p>
    </sec>
    <sec id="s3">
      <title>Experiences of India and Brazil</title>
      <p>So how are the emerging economies working to improve access to medicines for NTDs? Lower costs are helping to stimulate the development of medicines in these emerging economies. The cost of research and development (R&amp;D) and manufacturing in India and China are estimated to be one-eighth and one-fifth of the costs incurred by Western companies, respectively. This is attributed to lower fixed asset costs (i.e., lower costs of building manufacturing facilities), cheaper labour, lower costs of regulation, efficient manufacturing processes, a large suitable population to be recruited quickly and cheaply for clinical trials, and inexpensive marketing <xref ref-type="bibr" rid="pntd.0001390-Grace1">[10]</xref>.</p>
      <p>In India, the pharmaceutical industry grew through utilisation of low R&amp;D costs and its building of generic drug production facilities. Later, when forced by the Agreement on the Trade-Related Aspects of Intellectual Property Rights (TRIPS), it moved towards innovative drug production. Brazil went through a similar process of utilising low production costs and expanding its generic drug industry. However, there was a key difference: the manner in which it dealt with the introduction of the TRIPS by the World Trade Organization (WTO). Here, we see how government technological and industrial policies have proved extremely influential in stimulating R&amp;D <xref ref-type="bibr" rid="pntd.0001390-Hasenclever1">[11]</xref>.</p>
      <p>Under the TRIPS Agreement, members of the WTO have to provide patent protection for inventions, which includes medicines and their methods of manufacture. Patent protection has to last at least 20 years from the date the patent application was filed <xref ref-type="bibr" rid="pntd.0001390-Information1">[12]</xref>. The TRIPS Agreement permitted a transition period of 10 years in which developing countries could adapt. During the transition, a “mailbox” for patent deposits was set up. This system allowed patent deposits before the agreement to be placed in the “mailbox” and held there for the transition period before a decision need be made. India made full use of this transition period. The Indian Patents Act of 1970, which granted patent rights only to manufacturing processes, prevailed throughout the transition, until 2005. This act allowed Indian pharmaceutical companies to perform reverse engineering of branded drugs and sell them as generic drugs. The mailbox system also meant that Indian companies were able to predict which drugs were waiting for patent and adapt accordingly. When it came to the end of the transition period, patents were only granted in cases where there was no local production. Alongside policies to support development of local pharmaceutical companies such as high import tariffs, restriction of foreign direct investment, and price control, the way in which they adapted to the TRIPS Agreement allowed greater development of the national pharmaceutical industry <xref ref-type="bibr" rid="pntd.0001390-Hasenclever1">[11]</xref>.</p>
      <p>Brazil, however, became fully compliant with the agreement after 2 years (by 1996). Once the agreement was implemented, Brazil began granting patents. Every depositor who had obtained a patent in another country could request one in Brazil without assessment. This pipeline mechanism also allowed a retroactive examination of patent deposits from products that only became patentable after the enactment of the new law <xref ref-type="bibr" rid="pntd.0001390-Hasenclever1">[11]</xref>.</p>
      <p>The results of these different government policy decisions are marked. During the transition period, Brazil saw a decrease in its national market, whereas India had fostered a significant increase. Whilst Brazil was in negative trade balance for pharmaceutical and medicinal products, India was in positive trade balance and had achieved near self-sufficiency in most drugs and pharmaceuticals <xref ref-type="bibr" rid="pntd.0001390-Morel1">[13]</xref>. The multinational corporations with their “persistent hegemony” in the Brazilian market benefited from the application of TRIPS to Brazil, but the Brazilian companies did not <xref ref-type="bibr" rid="pntd.0001390-Bermudez1">[14]</xref>.</p>
      <p>As described by Morel et al., Brazil is trying to make up for lost ground with their new legal and regulatory framework, which includes the Law of Technological Innovation <xref ref-type="bibr" rid="pntd.0001390-Republic1">[15]</xref>, and their Biotechnology Development Policy. These policy and legal changes are helping to generate new drugs to treat NTDs by bridging the gap between basic research and drug development. For example, the Brazil Department for Science and Technology and Ministry of Science and Technology together invested US$10 million in 76 peer-reviewed projects as part of a pilot R&amp;D programme in 2007–2008 that aims to tackle dengue fever, Chagas' disease, leprosy, malaria, tuberculosis, and leishmaniasis. The programme builds on existing international networks of research and aims to strengthen capacity for research on NTDs, particularly in regions of Brazil where these diseases are endemic <xref ref-type="bibr" rid="pntd.0001390-Morel1">[13]</xref>. Furthermore, much can be learned about improving access to medicines from the successes of the Brazilian efforts to guarantee free antiretrovirals for patients with HIV/AIDS. Whilst local production of antiretroviral drugs was paramount, where this was not possible the drugs were imported as cheaply as could be negotiated. This was made possible by judicious use of compulsory licensing as a bargaining tool, and later by actual issue of a compulsory license <xref ref-type="bibr" rid="pntd.0001390-Galvo1">[16]</xref>, <xref ref-type="bibr" rid="pntd.0001390-Nunn1">[17]</xref>.</p>
    </sec>
    <sec id="s4">
      <title>Lessons and Their Application</title>
      <p>From observations of the experiences of emerging economies such as India and Brazil, it can be seen that developing a competitive national pharmaceutical industry has the potential to ensure sustainable and affordable development of drugs for NTDs. These observations also underscore the importance of managing intellectual property and laws that support innovation.</p>
      <p>Capacity for achieving comparable improvement in access to medicines in other emerging economies is as yet an untapped resource. It has been suggested, by Frew et al., that a “Global Health Accelerator” is needed to scale up the product development efforts of these emerging-economy firms <xref ref-type="bibr" rid="pntd.0001390-Frew1">[9]</xref>. The accelerator would create a shift away from the less sustainable business of product development at high costs followed by charitable donation, and towards affordable innovation. The accelerator model would utilise push and pull mechanisms to incentivise drug development, as well as support small firms in international business issues such as understanding regulatory environments, assessing markets, positioning products, identifying distribution channels, accessing financing, and identifying international commercialisation partners. It would also facilitate collaborative endeavours including public–private partnerships <xref ref-type="bibr" rid="pntd.0001390-Frew1">[9]</xref>. However, such ideas must go beyond the emerging economies. It is crucial that the lessons about stimulating innovation are shared effectively with the developing world.</p>
      <p>There are early signs of efforts to scale up nascent drug R&amp;D in developing nations. The first meeting of the new African Network for Drugs and Diagnostics Innovation (ANDI) was held in October 2008. The goal of the ANDI is to develop locally sustainable health R&amp;D coordinated through an African-based and -led organisation <xref ref-type="bibr" rid="pntd.0001390-TDR1">[18]</xref>, <xref ref-type="bibr" rid="pntd.0001390-ANDI1">[19]</xref>. Similarly, the Initiative to Strengthen Health Research Capacity in Africa (ISHRECA) “seeks to promote the creation of self-sustaining pools of excellence capable of initiating and carrying out high quality health research in Africa as well as translating research products into policy and practice through better integrated approaches of capacity building at individual, institutional and system levels” <xref ref-type="bibr" rid="pntd.0001390-ISHReCA1">[20]</xref>.</p>
      <p>Superimposed on these Africa-based organisations, The South-South Initiative works to promote collaboration between disease-endemic countries across Africa, Latin America, and Asia in the application of scientific, technological, and methodological advances to infectious diseases of poverty <xref ref-type="bibr" rid="pntd.0001390-SouthSouth1">[21]</xref>. As exemplified by the investment made by Cipla, a large Indian pharmaceutical company, in the manufacture of antiretrovirals by Uganda's Quality Chemicals Industries, such collaborations are already helping tackle access to medicines for HIV. In this case, the viability of such collaboration was provided by advanced market commitments made by the Ugandan government as well as the promise of a market across the whole of Eastern Africa <xref ref-type="bibr" rid="pntd.0001390-United1">[22]</xref>. The beginnings of similar collaborative initiatives for NTDs can be seen. For example, a research network linking Brazil with Ghana, Nigeria, Angola, and Mozambique has been set up so that research and experience on NTDs can be shared <xref ref-type="bibr" rid="pntd.0001390-TDRnews1">[23]</xref>.</p>
    </sec>
    <sec id="s5">
      <title>Conclusion</title>
      <p>Currently, the dominant strategy for ensuring access to medicines for NTDs is drug donation from Western pharmaceutical companies. But this dependence upon profit-driven organisations is precarious. Clearly, a more sustainable approach is required. Unlike many developing countries, the emerging economies not only have a large NTD burden, but they are also beginning to show us a means of improving access to medicines for NTDs in a more sustainable fashion. They are developing their own policies of innovation, their own pharmaceutical industries, and their own medical solutions to NTDs. Their experiential knowledge is surely invaluable and can help to guide developing countries towards sustainable strategies to control NTDs.</p>
    </sec>
  </body>
  <back>
    <ref-list>
      <title>References</title>
      <ref id="pntd.0001390-Hotez1">
        <label>1</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Hotez</surname><given-names>P</given-names></name><name name-style="western"><surname>Molyneux</surname><given-names>D</given-names></name><name name-style="western"><surname>Fenwick</surname><given-names>A</given-names></name><name name-style="western"><surname>Kumaresan</surname><given-names>J</given-names></name><name name-style="western"><surname>Sachs</surname><given-names>S</given-names></name><etal/></person-group>             <year>2007</year>             <article-title>Control of neglected tropical diseases.</article-title>             <source>N Engl J Med</source>             <volume>357</volume>             <issue>10</issue>             <fpage>1018</fpage>             <lpage>1027</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Hotez2">
        <label>2</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Hotez</surname><given-names>P</given-names></name><name name-style="western"><surname>Fenwick</surname><given-names>A</given-names></name><name name-style="western"><surname>Savioli</surname><given-names>L</given-names></name><name name-style="western"><surname>Molyneux</surname><given-names>D</given-names></name></person-group>             <year>2009</year>             <article-title>Rescuing the bottom billion through control of neglected tropical diseases.</article-title>             <source>Lancet</source>             <volume>373</volume>             <fpage>1570</fpage>             <lpage>1575</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Conteh1">
        <label>3</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Conteh</surname><given-names>L</given-names></name><name name-style="western"><surname>Engels</surname><given-names>T</given-names></name><name name-style="western"><surname>Molyneux</surname><given-names>D</given-names></name></person-group>             <year>2010</year>             <article-title>Socioeconomic aspects of neglected tropical diseases.</article-title>             <source>Lancet</source>             <volume>375</volume>             <fpage>239</fpage>             <lpage>247</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Jain1">
        <label>4</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Jain</surname><given-names>S</given-names></name></person-group>             <year>2006</year>             <source>Emerging economies and the transformation of international business</source>             <publisher-name>Edward Elgar Publishing</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Wilson1">
        <label>5</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Wilson</surname><given-names>D</given-names></name><name name-style="western"><surname>Purushothaman</surname><given-names>R</given-names></name></person-group>             <year>2003</year>             <article-title>Dreaming with BRICs: the path to 2050.</article-title>             <comment>Global economics paper no. 99. Available: <ext-link ext-link-type="uri" xlink:href="http://www2.goldmansachs.com/our-thinking/brics/brics-reports-pdfs/brics-dream.pdf" xlink:type="simple">http://www2.goldmansachs.com/our-thinking/brics/brics-reports-pdfs/brics-dream.pdf</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Campbell1">
        <label>6</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Campbell</surname><given-names>D</given-names></name><name name-style="western"><surname>Chui</surname><given-names>M</given-names></name></person-group>             <year>2010</year>             <article-title>Pharmerging shake-up: new imperatives in a redefined world.</article-title>             <comment>IMS Health. Available: <ext-link ext-link-type="uri" xlink:href="http://www.imshealth.com/deployedfiles/imshealth/Global/Content/StaticFile/Pharma_Shake-up_Imperatives_3_10.pdf" xlink:type="simple">http://www.imshealth.com/deployedfiles/imshealth/Global/Content/StaticFile/Pharma_Shake-up_Imperatives_3_10.pdf</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-World1">
        <label>7</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">World Health Organization</collab>             <year>2007</year>             <source>Annual report 2006 neglected tropical diseases. WHO/CDS/NTD/2007.4</source>             <publisher-loc>Geneva</publisher-loc>             <publisher-name>WHO</publisher-name>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Leach1">
        <label>8</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Leach</surname><given-names>B</given-names></name><name name-style="western"><surname>Palluzzi</surname><given-names>J</given-names></name><name name-style="western"><surname>Munderi</surname><given-names>P</given-names></name></person-group>             <year>2005</year>             <article-title>Prescription for healthy development: increasing access to medicines.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://www.unmillenniumproject.org/documents/TF5-medicines-Complete.pdf" xlink:type="simple">http://www.unmillenniumproject.org/documents/TF5-medicines-Complete.pdf</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Frew1">
        <label>9</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Frew</surname><given-names>S</given-names></name><name name-style="western"><surname>Liu</surname><given-names>V</given-names></name><name name-style="western"><surname>Singer</surname><given-names>P</given-names></name></person-group>             <year>2009</year>             <article-title>A business plan to help the “global South” in its fight against neglected diseases.</article-title>             <source>Health Aff (Millwood)</source>             <volume>28</volume>             <issue>6</issue>             <fpage>1760</fpage>             <lpage>1773</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Grace1">
        <label>10</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Grace</surname><given-names>C</given-names></name></person-group>             <year>2004</year>             <article-title>The effect of changing intellectual property on pharmaceutical industry prospects in India and China: considerations for access to medicines.</article-title>             <comment>DFID Health Systems Resource Centre. Available: <ext-link ext-link-type="uri" xlink:href="http://www.who.int/hiv/amds/Grace2China.pdf" xlink:type="simple">http://www.who.int/hiv/amds/Grace2China.pdf</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Hasenclever1">
        <label>11</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Hasenclever</surname><given-names>L</given-names></name><name name-style="western"><surname>Paranhos</surname><given-names>J</given-names></name></person-group>             <year>2008</year>             <article-title>The development of the pharmaceutical industry in Brazil and India: technological capability and industrial development.</article-title>             <comment>Working paper of the international conference “The emerging process: from trajectories to concepts”; Bordeaux; 27–28 November 2008</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Information1">
        <label>12</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">Information and Media Relations Division of the WTO Secretariat</collab>             <year>2006</year>             <article-title>Fact sheet: TRIPS and pharmaceutical patents.</article-title>             <comment>September 2006. Available: <ext-link ext-link-type="uri" xlink:href="http://www.wto.org/english/tratop_e/trips_e/factsheet_pharm02_e.htm" xlink:type="simple">http://www.wto.org/english/tratop_e/trips_e/factsheet_pharm02_e.htm</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Morel1">
        <label>13</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Morel</surname><given-names>C</given-names></name><name name-style="western"><surname>Carvalheiro</surname><given-names>J</given-names></name><name name-style="western"><surname>Romero</surname><given-names>C</given-names></name><name name-style="western"><surname>Costa</surname><given-names>E</given-names></name><name name-style="western"><surname>Buss</surname><given-names>P</given-names></name></person-group>             <year>2007</year>             <article-title>The road to recovery.</article-title>             <source>Nature</source>             <volume>449</volume>             <fpage>180</fpage>             <lpage>182</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Bermudez1">
        <label>14</label>
        <element-citation publication-type="other" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Bermudez</surname><given-names>J</given-names></name><name name-style="western"><surname>Epsztejn</surname><given-names>R</given-names></name><name name-style="western"><surname>Oliveira</surname><given-names>M</given-names></name><name name-style="western"><surname>Hasenclever</surname><given-names>L</given-names></name></person-group>             <year>2002</year>             <article-title>Access to drugs, the WTO TRIPS Agreement, and patent protection in Brazil: trends, perspectives, and recommendations to help find our way.</article-title>             <comment>MSF/DND Working Group Expert Paper</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Republic1">
        <label>15</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">Republic of Brazil</collab>             <year>2004</year>             <article-title>Law No. 10.973 of 2 December 2004 (Innovation and Research in Science and Technology).</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2004/Lei/L10.973.htm" xlink:type="simple">http://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2004/Lei/L10.973.htm</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Galvo1">
        <label>16</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Galvão</surname><given-names>J</given-names></name></person-group>             <year>2002</year>             <article-title>Access to antiretroviral drugs in Brazil.</article-title>             <source>Lancet</source>             <volume>360</volume>             <issue>9348</issue>             <fpage>1862</fpage>             <lpage>1865</lpage>          </element-citation>
      </ref>
      <ref id="pntd.0001390-Nunn1">
        <label>17</label>
        <element-citation publication-type="journal" xlink:type="simple">             <person-group person-group-type="author"><name name-style="western"><surname>Nunn</surname><given-names>A</given-names></name><name name-style="western"><surname>Fonseca</surname><given-names>E</given-names></name><name name-style="western"><surname>Bastos</surname><given-names>F</given-names></name><name name-style="western"><surname>Gruskin</surname><given-names>S</given-names></name><name name-style="western"><surname>Salomon</surname><given-names>J</given-names></name></person-group>             <year>2007</year>             <article-title>Evolution of antiretroviral drug costs in Brazil in the context of free and universal access to AIDS treatment.</article-title>             <source>PLoS Med</source>             <volume>4</volume>             <fpage>e305</fpage>             <comment>doi:<ext-link ext-link-type="uri" xlink:href="http://dx.doi.org/10.1371/journal.pmed.0040305" xlink:type="simple">10.1371/journal.pmed.0040305</ext-link></comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-TDR1">
        <label>18</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">TDR</collab>             <year>9 October 2008</year>             <article-title>R&amp;D in Africa—a new network says it can be done [press release].</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://apps.who.int/tdr/svc/news-events/news/andi-launch" xlink:type="simple">http://apps.who.int/tdr/svc/news-events/news/andi-launch</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-ANDI1">
        <label>19</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">ANDI</collab>             <year>2008</year>             <article-title>Draft summary minutes and recommendations ANDI launch meeting October 6–8, Abuja 2008.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://apps.who.int/tdr/news-events/news/pdf/ANDI-minutes.pdf" xlink:type="simple">http://apps.who.int/tdr/news-events/news/pdf/ANDI-minutes.pdf</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-ISHReCA1">
        <label>20</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">ISHReCA</collab>             <year>n.d.</year>             <article-title>About ISHReCA.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://ishreca.tropika.net/open-content" xlink:type="simple">http://ishreca.tropika.net/open-content</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-SouthSouth1">
        <label>21</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">South-South Initiative for Tropical Diseases Research</collab>             <year>n.d.</year>             <article-title>South-South Initiative for Tropical Diseases Research.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://www.ssi-tdr.net/index.php" xlink:type="simple">http://www.ssi-tdr.net/index.php</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-United1">
        <label>22</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">United Nations</collab>             <year>2011</year>             <article-title>Investment in pharmaceutical production in the least developed countries.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://www.unctad.org/en/docs/diaepcb2011d5_en.pdf" xlink:type="simple">http://www.unctad.org/en/docs/diaepcb2011d5_en.pdf</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
      <ref id="pntd.0001390-TDRnews1">
        <label>23</label>
        <element-citation publication-type="other" xlink:type="simple">             <collab xlink:type="simple">TDR<italic>news</italic></collab>             <year>2010</year>             <article-title>Africa-Brazil research network.</article-title>             <comment>Available: <ext-link ext-link-type="uri" xlink:href="http://apps.who.int/tdr/svc/publications/tdrnews/issue-85/africa-brazil" xlink:type="simple">http://apps.who.int/tdr/svc/publications/tdrnews/issue-85/africa-brazil</ext-link>. Accessed 24 January 2012</comment>          </element-citation>
      </ref>
    </ref-list>
    
  </back>
</article>