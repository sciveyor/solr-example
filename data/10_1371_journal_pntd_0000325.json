{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000325",
  "doi": "10.1371/journal.pntd.0000325",
  "externalIds": [
    "pii:08-PNTD-RA-0221R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Mycolactone Diffuses from Mycobacterium ulcerans–Infected Tissues and Targets Mononuclear Cells in Peripheral Blood and Lymphoid Organs",
  "authors": [
    {
      "name": "Hui Hong",
      "first": "Hui",
      "last": "Hong",
      "affiliation": "University of Cambridge, Department of Biochemistry, Cambridge, United Kingdom"
    },
    {
      "name": "Emmanuelle Coutanceau",
      "first": "Emmanuelle",
      "last": "Coutanceau",
      "affiliation": "Institut Pasteur, UP Pathogénomique Mycobactérienne Intégrée, Paris, France"
    },
    {
      "name": "Marion Leclerc",
      "first": "Marion",
      "last": "Leclerc",
      "affiliation": "Institut Pasteur, UP Pathogénomique Mycobactérienne Intégrée, Paris, France"
    },
    {
      "name": "Laxmee Caleechurn",
      "first": "Laxmee",
      "last": "Caleechurn",
      "affiliation": "Institut Pasteur, UP Pathogénomique Mycobactérienne Intégrée, Paris, France"
    },
    {
      "name": "Peter F. Leadlay",
      "first": "Peter F.",
      "last": "Leadlay",
      "affiliation": "University of Cambridge, Department of Biochemistry, Cambridge, United Kingdom"
    },
    {
      "name": "Caroline Demangel",
      "first": "Caroline",
      "last": "Demangel",
      "affiliation": "Institut Pasteur, UP Pathogénomique Mycobactérienne Intégrée, Paris, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-10",
  "dateAccepted": "2008-09-26",
  "dateReceived": "2008-07-02",
  "volume": "2",
  "number": "10",
  "pages": "e325",
  "tags": [
    "Pathology/Immunology"
  ],
  "abstract": "Background\n\nBuruli ulcer (BU) is a progressive disease of subcutaneous tissues caused by Mycobacterium ulcerans. The pathology of BU lesions is associated with the local production of a diffusible substance, mycolactone, with cytocidal and immunosuppressive properties. The defective inflammatory responses in BU lesions reflect these biological properties of the toxin. However, whether mycolactone diffuses from infected tissues and suppresses IFN-γ responses in BU patients remains unclear.\n\nMethodology/Principal Findings\n\nHere we have investigated the pharmacodistribution of mycolactone following injection in animal models by tracing a radiolabeled form of the toxin, and by directly quantifying mycolactone in lipid extracts from internal organs and cell subpopulations. We show that subcutaneously delivered mycolactone diffused into mouse peripheral blood and accumulated in internal organs with a particular tropism for the spleen. When mice were infected subcutaneously with M. ulcerans, this led to a comparable pattern of distribution of mycolactone. No evidence that mycolactone circulated in blood serum during infection could be demonstrated. However, structurally intact toxin was identified in the mononuclear cells of blood, lymph nodes and spleen several weeks before ulcerative lesions appear. Importantly, diffusion of mycolactone into the blood of M. ulcerans–infected mice coincided with alterations in the functions of circulating lymphocytes.\n\nConclusion\n\nIn addition to providing the first evidence that mycolactone diffuses beyond the site of M. ulcerans infection, our results support the hypothesis that the toxin exerts immunosuppressive effects at the systemic level. Furthermore, they suggest that assays based on mycolactone detection in circulating blood cells may be considered for diagnostic tests of early disease.",
  "fullText": "Introduction Buruli ulcer (BU) is a cutaneous disease caused by Mycobacterium ulcerans, leading to the formation of progressive ulcers, with extensive skin and soft tissue destruction. The presence of a coagulative necrosis forming a nidus for colonies of bacilli, accompanied by minimal inflammation, are considered the most reliable features for the histopathological diagnosis of BU disease [1]. These hallmarks of BU lesions in fact reflect the dual properties of a macrolide toxin produced by M. ulcerans, mycolactone, which plays a critical role in bacterial virulence [2],[3]. This unique polyketide has been shown to be a potent cytocidal molecule in vitro and in vivo [4],[5],[6],[7]. In addition, mycolactone displays significant immunosuppressive properties at non-cytotoxic doses towards a wide range of immune cells [5],[8],[9],[10]. Numerous studies have reported defective IFN-γ responses in BU patients, using assays of PBMC restimulation ex-vivo [11],[12],[13],[14],[15],[16]. IFN-γ responses to M. ulcerans antigens are reduced in BU patients compared to healthy controls [11],[12],[13],[14], particularly during the early stage of the disease [15],[16]. M. ulcerans infection-associated reduction of IFN-γ responses was initially thought to be restricted to mycobacterial antigens. In fact, systemic suppression of IFN-γ responses is not antigen-specific, and resolves after surgical excision of the lesion [17]. Notably, the optimal growth temperature of M. ulcerans is below 35°C [18], and animal studies suggest that the bacilli remain essentially localized within ulcerative lesions in subcutaneous tissues [5]. The fact that immunosuppression in BU patients resolves after removal of infected tissues therefore strongly suggests that bacterial factors, such as mycolactone, may diffuse from the bacilli colonies and exert immunosuppressive effects at the systemic level [19]. In the present study, we have investigated the pharmacodistribution of mycolactone following injection in animal models by tracing a radiolabeled form of the toxin in vivo, and by directly assessing the integrity and the quantity of mycolactone in lipid extracts from internal organs and cell subpopulations. Our observation that mycolactone diffuses in blood and spleen, and concentrates within distinct immune cellular subsets, supports the notion that mycolactone permits M. ulcerans to establish long-term infections by remotely neutralizing the development of cellular immunity. Methods Animals Six week old BalB/cByJIco and C57BL/6JIco female mice were purchased from Charles Rivers Laboratories. Mice were housed in a BSL-3 animal facility at the Institut Pasteur, in full compliance with French and European regulation and guidelines on experiments with live animal. Mycolactone preparation Mu 1615 wt (ATCC 35840) was obtained from the Trudeau collection. This strain produces a mixture of mycolactones A/B and C [20]. Bacteria were cultivated in Dubos medium complemented with oleic acid-albumin-dextrose 10% (OADC, Becton Dickinson) in spinner flasks at 32°C. To generate 14C-labeled mycolactone, exponentially growing cultures were supplemented weekly with 15 µl [1- 14C] propionic acid (MP, 40–60 mCi/mmol) and 15 µl [1,2- 14C] acetic acid (MP, 50–120 mCi/mmol). After three weeks, mycolactone was purified from bacterial pellets as previously described [2]. The resulting 14C-radiolabeled mycolactone showed an activity of 300 cpm/µg. Its biological activity, as measured by the assay described below, was equivalent to that"
}