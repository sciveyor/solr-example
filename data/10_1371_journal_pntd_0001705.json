{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001705",
  "doi": "10.1371/journal.pntd.0001705",
  "externalIds": [
    "pii:PNTD-D-12-00220"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Living Invisible: HTLV-1-Infected Persons and the Lack of Care in Public Health",
  "authors": [
    {
      "name": "Karina Franco Zihlmann",
      "first": "Karina Franco",
      "last": "Zihlmann",
      "affiliation": "Federal University of São Paulo, São Paulo, Brazil"
    },
    {
      "name": "Augusta Thereza de Alvarenga",
      "first": "Augusta Thereza",
      "last": "de Alvarenga",
      "affiliation": "Public Health Faculty, University of São Paulo, São Paulo, Brazil"
    },
    {
      "name": "Jorge Casseb",
      "first": "Jorge",
      "last": "Casseb",
      "affiliation": "Laboratory of Medical Investigation LIM-56/Faculty of Medicine, Institute of Tropical Medicine of São Paulo, University of São Paulo, São Paulo, Brazil"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-06",
  "dateAccepted": "2012-05-08",
  "dateReceived": "2012-02-16",
  "volume": "6",
  "number": "6",
  "pages": "e1705",
  "tags": [
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Public Health and Epidemiology",
    "Public health"
  ],
  "abstract": "Introduction\n          \nHuman T-cell lymphotropic virus type 1 (HTLV-1) infection is intractable and endemic in many countries. Although a few individuals have severe symptoms, most patients remain asymptomatic throughout their lives and their infections may be unknown to many health professionals. HTLV-1 can be considered a neglected public health problem and there are not many studies specifically on patients' needs and emotional experiences.\n\n          Objective\n          \nTo better understand how women and men living with HTLV-1 experience the disease and what issues exist in their healthcare processes.\n\n          Methods\n          \nA qualitative study using participant observation and life story interview methods was conducted with 13 symptomatic and asymptomatic patients, at the outpatient clinic of the Emilio Ribas Infectious Diseases Institute, in Sao Paulo, Brazil.\n\n          Results and Discussion\n          \nThe interviewees stated that HTLV-1 is a largely unknown infection to society and health professionals. Counseling is rare, but when it occurs, focuses on the low probability of developing HTLV-1 related diseases without adequately addressing the risk of infection transmission or reproductive decisions. The diagnosis of HTLV-1 can remain a stigmatized secret as patients deny their situations. As a consequence, the disease remains invisible and there are potentially negative implications for patient self-care and the identification of infected relatives. This perception seems to be shared by some health professionals who do not appear to understand the importance of preventing new infections.\n\n          Conclusions\n          \nPatients and medical staff referred that the main focus was the illness risk, but not the identification of infected relatives to prevent new infections. This biomedical model of care makes prevention difficult, contributes to the lack of care in public health for HTLV-1, and further perpetuates the infection among populations. Thus, HTLV-1 patients experience an “invisibility” of their complex demands and feel that their rights as citizens are ignored.",
  "fullText": "Introduction The human T-cell lymphotropic type 1 (HTLV-1) [1] infection causes a life-long infection for infected subjects. HTLV-1 is endemic in various parts of the world, including Japan and countries in Africa, the Caribbean and South America [2]. It has been estimated that over 10 million individuals are infected with HTLV-1 [3], but most infected persons are asymptomatic and probably are not aware of their serological status. In addition, asymptomatic individuals may still infect sexual partners or offspring. The most common diseases associated with HTLV-1 infection are adult T-cell leukemia/lymphoma (ATL) and HTLV-1-associated myelopathy/tropical spastic paraparesis (HAM/TSP) [4]. No accurate case statistics exist for ATL or HAM/TSP because these diseases are not reportable by the World Health Organization (WHO). Despite several publication reviews on pathogenesis or molecular biology of HTLV-1 [5], [6], few studies have addressed treatments for HTLV-1 or the psychological issues related to having the disease. Therefore, HTLV-1 infection is considered, and was recently reported, as a neglected disease [7]. Brazil has received worldwide recognition for the country's public health policies, especially with respect to how the nation has addressed the pandemic of HIV/AIDS [8]. Such success against HIV/AIDS was possible only because Brazil's constitution recognizes and guarantees healthcare as a right of every citizen and the country's public health ministry provides a multidisciplinary prevention program and free medication for HIV/AIDS. [8]. However, such public health policies have not been applied to HTLV-1 infection, which is not even listed as an infectious disease that should be addressed by public health action. Thus, HIV/AIDS may overshadow the problem of HTLV-1 in Brazil, and consequently, HTLV-1 has become essentially unknown to most of health professionals and should be considered a public health problem in Brazil [9]. Few HTLV-1 epidemiological studies have been conducted in the general population, but the average prevalence of HTLV-1/2 among donor blood banks is 0.46% nationwide [10] and one few based on study population showed prevalence of 1.7% in Salvador, with 40000 individuals are estimated to be infected by HTLV-1 [11]. The highest prevalence of HTLV-1 has been recorded in São Luís city in Maranhão, with 10.0/1000 and Salvador in Bahia with 9.4/1000 [11]. However, other cities may present lower prevalence despite geographical proximity, which indicates heterogeneous distribution and cluster pattern [12]. In 1993, it was recommended by ministry of health the mandatory tests for hepatitis C and HTLV-1/2 in blood banks. In 2002, Brazil also instituted a policy which provided free testing of HIV/AIDS and syphilis during prenatal care visits as an approach to preventing mother-to-child-transmission (PMTCT). However, the same policy was not provided for HTLV-1 infection and the Brazilian Ministry of Health only published the Guide Recommendations on HTLV Management in 2003, without update [13]. From a public health perspective, prevention of HTLV-1 is crucial, especially because HTLV-1 is a life-long infection and is currently incurable and untreatable. The health field recognizes that disease prevention requires strategies beyond strictly medical approaches. Thus, the aim of this study was to identify perceptions of HTLV-1 by"
}