{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000793",
  "doi": "10.1371/journal.pntd.0000793",
  "externalIds": [
    "pii:09-PNTD-RA-0653R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Trypanosoma vivax Infections: Pushing Ahead with Mouse Models for the Study of Nagana. II. Immunobiological Dysfunctions",
  "authors": [
    {
      "name": "Marie Christine Blom-Potar",
      "first": "Marie Christine",
      "last": "Blom-Potar",
      "affiliation": "Laboratoire d'Immunobiologie des Infections à Trypanosoma, Département d'Immunologie, Institut Pasteur, Paris, France"
    },
    {
      "name": "Nathalie Chamond",
      "first": "Nathalie",
      "last": "Chamond",
      "affiliation": "Laboratoire d'Immunobiologie des Infections à Trypanosoma, Département d'Immunologie, Institut Pasteur, Paris, France"
    },
    {
      "name": "Alain Cosson",
      "first": "Alain",
      "last": "Cosson",
      "affiliation": "Laboratoire d'Immunobiologie des Infections à Trypanosoma, Département d'Immunologie, Institut Pasteur, Paris, France"
    },
    {
      "name": "Grégory Jouvion",
      "first": "Grégory",
      "last": "Jouvion",
      "affiliation": "Unité de Recherche et d'Expertise Histotechnologie et Pathologie, Institut Pasteur, Paris, France"
    },
    {
      "name": "Sabrina Droin-Bergère",
      "first": "Sabrina",
      "last": "Droin-Bergère",
      "affiliation": "Unité de Recherche et d'Expertise Histotechnologie et Pathologie, Institut Pasteur, Paris, France"
    },
    {
      "name": "Michel Huerre",
      "first": "Michel",
      "last": "Huerre",
      "affiliation": "Unité de Recherche et d'Expertise Histotechnologie et Pathologie, Institut Pasteur, Paris, France"
    },
    {
      "name": "Paola Minoprio",
      "first": "Paola",
      "last": "Minoprio",
      "affiliation": "Laboratoire d'Immunobiologie des Infections à Trypanosoma, Département d'Immunologie, Institut Pasteur, Paris, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-08",
  "dateAccepted": "2010-07-14",
  "dateReceived": "2009-11-13",
  "volume": "4",
  "number": "8",
  "pages": "e793",
  "tags": [
    "Immunology/Immune Response",
    "Immunology/Leukocyte Development",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Trypanosoma vivax is the main species involved in trypanosomosis, but very little is known about the immunobiology of the infective process caused by this parasite. Recently we undertook to further characterize the main parasitological, haematological and pathological characteristics of mouse models of T. vivax infection and noted severe anemia and thrombocytopenia coincident with rising parasitemia. To gain more insight into the organism's immunobiology, we studied lymphocyte populations in central (bone marrow) and peripherical (spleen and blood) tissues following mouse infection with T. vivax and showed that the immune system apparatus is affected both quantitatively and qualitatively. More precisely, after an initial increase that primarily involves CD4+ T cells and macrophages, the number of splenic B cells decreases in a step-wise manner. Our results show that while infection triggers the activation and proliferation of Hematopoietic Stem Cells, Granulocyte-Monocyte, Common Myeloid and Megacaryocyte Erythrocyte progenitors decrease in number in the course of the infection. An in-depth analysis of B-cell progenitors also indicated that maturation of pro-B into pre-B precursors seems to be compromised. This interferes with the mature B cell dynamics and renewal in the periphery. Altogether, our results show that T. vivax induces profound immunological alterations in myeloid and lymphoid progenitors which may prevent adequate control of T. vivax trypanosomosis.",
  "fullText": "Introduction African trypanosomes are extracellular parasites that cause sleeping sickness in humans and Nagana in animals. They include T. brucei species which infect both humans and ruminants, but also T. congolense and particularly T. vivax which are responsible for the vast majority of animal trypanosomosis in sub-Saharan Africa, South America and South Asia [1]–[3]. Due mainly to technical constraints such as a lack of reproducible in vitro culture conditions and relatively poor accessibility to natural hosts, our understanding of the biology and fate of T. vivax in its vertebrate hosts largely stems from the extrapolation of data obtained from the experimental murine infection with T. brucei, T. congolense and T. ewansi, but just a few studies using T. vivax infected mice [4]–[8]. Recently, in a move to gain further insight into the host - T. vivax interaction, we further developed reproducible and reliable in vivo models of T. vivax infection using three different mouse strains and the IL 1392 West African isolate (see accompanying paper). Briefly, our studies showed that all the mouse strain infected with bloodstream forms of T. vivax developed the characteristic anemia and systemic alterations that include acute necrosis of the liver and spleen which are the hallmarks of animal trypanosomosis [9]–[12]. Previous immunobiological studies of trypanosomosis focused mainly on the interaction between trypanosome surface coat antigens (Variant Surface Glycoproteins, VSGs) and host cells [13]–[15]. The triggering of polyclonal B cell activation by trypanosomes and the ensuing hypergammaglobulinemia mainly composed of antibodies (Ab) that do not recognize parasite antigens or VSGs are also typical of the infection [16]–[19]. The mechanisms underlying this process are still largely unknown. Moreover, the involvement of VSGs in protecting the parasites against host specific immunoresponses provided until recently one of the most exquisite models for the study of antigenic variation. It therefore followed, for many years, that our understanding of the interaction between African trypanosomes and the immune system was limited to this “parasite-driven” view where the host's immune response was restricted to the production of specific Abs against VSGs. Whereas anti-VSG Ab doubtless contribute to early control of the infection, resistance to late phases is not only dependent on specific (parasite-directed) immunoglobulins but also seems to rely on T-independent processes since athymic mice and also complement-deficient mice infected with T. rhodesiense are able to mount anti parasitic responses that are sufficient to increase mouse survival and healing after an infectious challenge [7], [20]. Interestingly, the severity of the disease correlates with the control exerted by T. brucei- and T. congolense- specific Abs over the frequency and duration of parasitemia waves but not the level of circulating parasites. This contrasts with T. vivax infections where the efficiency of the host's Ab response and the parasite-induced negative feedback of Abs raised against the parasite are responsible for regulating both the level and duration of parasitemia waves, thus determining disease severity [21]. In an attempt to throw light on the early events induced by T. vivax in mouse B cell compartments that may contribute"
}