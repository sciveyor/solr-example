{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001023",
  "doi": "10.1371/journal.pntd.0001023",
  "externalIds": [
    "pii:PNTD-D-10-00225"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Mining a Cathepsin Inhibitor Library for New Antiparasitic Drug Leads",
  "authors": [
    {
      "name": "Kenny K. H. Ang",
      "first": "Kenny K. H.",
      "last": "Ang",
      "affiliation": "The Small Molecule Discovery Center, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Joseline Ratnam",
      "first": "Joseline",
      "last": "Ratnam",
      "affiliation": "The Small Molecule Discovery Center, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Jiri Gut",
      "first": "Jiri",
      "last": "Gut",
      "affiliation": "Department of Medicine, San Francisco General Hospital, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Jennifer Legac",
      "first": "Jennifer",
      "last": "Legac",
      "affiliation": "Department of Medicine, San Francisco General Hospital, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Elizabeth Hansell",
      "first": "Elizabeth",
      "last": "Hansell",
      "affiliation": "The Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Zachary B. Mackey",
      "first": "Zachary B.",
      "last": "Mackey",
      "affiliation": "The Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Katarzyna M. Skrzypczynska",
      "first": "Katarzyna M.",
      "last": "Skrzypczynska",
      "affiliation": "The Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Anjan Debnath",
      "first": "Anjan",
      "last": "Debnath",
      "affiliation": "The Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Juan C. Engel",
      "first": "Juan C.",
      "last": "Engel",
      "affiliation": "The Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Philip J. Rosenthal",
      "first": "Philip J.",
      "last": "Rosenthal",
      "affiliation": "Department of Medicine, San Francisco General Hospital, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "James H. McKerrow",
      "first": "James H.",
      "last": "McKerrow",
      "affiliation": "The Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America; Department of Pathology, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Michelle R. Arkin",
      "first": "Michelle R.",
      "last": "Arkin",
      "affiliation": "The Small Molecule Discovery Center, University of California San Francisco, San Francisco, California, United States of America; Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Adam R. Renslo",
      "first": "Adam R.",
      "last": "Renslo",
      "affiliation": "The Small Molecule Discovery Center, University of California San Francisco, San Francisco, California, United States of America; Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-05",
  "dateAccepted": "2011-02-14",
  "dateReceived": "2010-12-01",
  "volume": "5",
  "number": "5",
  "pages": "e1023",
  "tags": [
    "Biochemistry",
    "Biology",
    "Biomacromolecule-ligand interactions",
    "Chemistry",
    "Drug discovery",
    "Emerging infectious diseases",
    "Medicinal chemistry",
    "Microbiology",
    "Parasitology",
    "Small molecules"
  ],
  "abstract": "The targeting of parasite cysteine proteases with small molecules is emerging as a possible approach to treat tropical parasitic diseases such as sleeping sickness, Chagas' disease, and malaria. The homology of parasite cysteine proteases to the human cathepsins suggests that inhibitors originally developed for the latter may be a source of promising lead compounds for the former. We describe here the screening of a unique ∼2,100-member cathepsin inhibitor library against five parasite cysteine proteases thought to be relevant in tropical parasitic diseases. Compounds active against parasite enzymes were subsequently screened against cultured Plasmodium falciparum, Trypanosoma brucei brucei and/or Trypanosoma cruzi parasites and evaluated for cytotoxicity to mammalian cells. The end products of this effort include the identification of sub-micromolar cell-active leads as well as the elucidation of structure-activity trends that can guide further optimization efforts.",
  "fullText": "Introduction There is a critical need for new drugs to treat neglected tropical diseases [1], [2], [3], [4]. Current therapies are limited by inadequate efficacy, drug resistance, or toxicity. Chagas' disease, for example, remains the leading cause of heart disease in Latin America, with between 8 and 12 million people currently infected, and over 90 million at risk (as reported in WHO fact sheet No. 340, June 2010). Current therapy for Chagas' disease consists of the nitro heterocycles nifurtimox and benznidazole. Both drugs have frequent and serious side effects [5] limiting their efficacy, and each requires an extended (60–120 days) course of therapy. Furthermore, resistance to nifurtimox is emerging. Drug choices for those suffering from human African trypanosomiasis (HAT, sleeping sickness) are similarly poor, with organoarsenic derivatives (e.g. melarsoprol) still employed in the treatment of CNS disease despite a ∼5% rate of drug-associated mortality [6]. In the treatment of malaria, the artemisinin-based combination therapies (ACT) [7] are currently effective and well tolerated, but resistance to the partner drugs [8] and possibly to artemisinins as well [9] is emerging. One pragmatic strategy to develop new antiparasitic drug leads is to focus on targets that are shared by multiple pathogens. Cysteine proteases play essential roles in numerous protozoan and helminth parasites, and are therefore appropriate targets for antiparasitic chemotherapy [10]. The Clan CA [11] cysteine protease cruzain (cruzipain) has been advanced as a potential drug target in Trypanosoma cruzi, the parasite responsible for Chagas' disease [12]. Cruzain is highly expressed in the intracellular amastigote stage of the parasite that is responsible for human disease. Cruzain deficient strains of the parasite fail to establish infection in immune-competent experimental hosts [13]. Because genetic deletion of the cruzain gene is not feasible, the target has instead been validated with small molecules. Hence, irreversible inhibitors of cruzain produce a characteristic swelling of the parasite Golgi compartment in T. cruzi parasites, leading to subsequent dysmorphic changes in both Golgi and endoplasmic reticulum, and ultimately rupture of parasite cells [14]. A number of cysteine protease inhibitors have been shown to arrest T. cruzi development within mammalian cells in vitro, and some have been shown to arrest or cure infection in mouse models of disease [15]. In Trypanosoma brucei parasites, the cysteine proteases rhodesain (brucipain) and a cathepsin-B like protease (TbCatB) have been advanced as potential drug targets [16], [17]. The erythrocytic malaria parasite produces a variety of proteases that perform the essential function of hemoglobin hydrolysis, whereby the parasite acquires its principal source of amino acids [18], [19]. We and others have shown that cysteine protease inhibitors block the hydrolysis of hemoglobin by erythrocytic parasites, causing the food vacuole to fill with undegraded hemoglobin, preventing parasite development, and indicating that cysteine proteases play an essential role in hemoglobin hydrolysis [20]. Among the various proteases that mediate hemoglobin hydrolysis are the cysteine proteases falcipain-2 and falcipain-3. Disruption of the falcipain-2 gene leads to a block in trophozoite hemoglobin hydrolysis [21]; the falcipain-3 gene could not be disrupted, strongly suggesting"
}