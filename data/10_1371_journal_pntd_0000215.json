{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000215",
  "doi": "10.1371/journal.pntd.0000215",
  "externalIds": [
    "pii:07-PNTD-RA-0142R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Differential Gene Expression Changes in Children with Severe Dengue Virus Infections",
  "authors": [
    {
      "name": "Martijn D. de Kruif",
      "first": "Martijn D.",
      "last": "de Kruif",
      "affiliation": "Department of Internal Medicine, Slotervaart Hospital, Amsterdam, The Netherlands; Center for Experimental and Molecular Medicine, Academic Medical Center, University of Amsterdam, The Netherlands"
    },
    {
      "name": "Tatty E. Setiati",
      "first": "Tatty E.",
      "last": "Setiati",
      "affiliation": "Department of Pediatrics, Dr. Kariadi Hospital, Semarang, Indonesia"
    },
    {
      "name": "Albertus T. A. Mairuhu",
      "first": "Albertus T. A.",
      "last": "Mairuhu",
      "affiliation": "Department of Internal Medicine, Slotervaart Hospital, Amsterdam, The Netherlands"
    },
    {
      "name": "Penelopie Koraka",
      "first": "Penelopie",
      "last": "Koraka",
      "affiliation": "Institute of Virology, Erasmus Medical Centre, Rotterdam, The Netherlands"
    },
    {
      "name": "Hella A. Aberson",
      "first": "Hella A.",
      "last": "Aberson",
      "affiliation": "Center for Experimental and Molecular Medicine, Academic Medical Center, University of Amsterdam, The Netherlands"
    },
    {
      "name": "C. Arnold Spek",
      "first": "C. Arnold",
      "last": "Spek",
      "affiliation": "Center for Experimental and Molecular Medicine, Academic Medical Center, University of Amsterdam, The Netherlands"
    },
    {
      "name": "Albert D. M. E. Osterhaus",
      "first": "Albert D. M. E.",
      "last": "Osterhaus",
      "affiliation": "Institute of Virology, Erasmus Medical Centre, Rotterdam, The Netherlands"
    },
    {
      "name": "Pieter H. Reitsma",
      "first": "Pieter H.",
      "last": "Reitsma",
      "affiliation": "Center for Experimental and Molecular Medicine, Academic Medical Center, University of Amsterdam, The Netherlands"
    },
    {
      "name": "Dees P. M. Brandjes",
      "first": "Dees P. M.",
      "last": "Brandjes",
      "affiliation": "Department of Internal Medicine, Slotervaart Hospital, Amsterdam, The Netherlands"
    },
    {
      "name": "Augustinus Soemantri",
      "first": "Augustinus",
      "last": "Soemantri",
      "affiliation": "Department of Pediatrics, Dr. Kariadi Hospital, Semarang, Indonesia"
    },
    {
      "name": "Eric C. M. van Gorp",
      "first": "Eric C. M.",
      "last": "van Gorp",
      "affiliation": "Department of Internal Medicine, Slotervaart Hospital, Amsterdam, The Netherlands"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-04",
  "dateAccepted": "2008-02-19",
  "dateReceived": "2007-06-20",
  "volume": "2",
  "number": "4",
  "pages": "e215",
  "tags": [
    "Critical Care and Emergency Medicine/Pediatric Critical Care",
    "Critical Care and Emergency Medicine/Sepsis and Multiple Organ Failure",
    "Genetics and Genomics/Gene Expression",
    "Immunology/Immune Response",
    "Immunology/Immunity to Infections",
    "Immunology/Innate Immunity",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Infectious Diseases/Viral Infections",
    "Microbiology/Immunity to Infections",
    "Microbiology/Innate Immunity",
    "Pediatrics and Child Health/Pediatric Critical Care",
    "Virology/Effects of Virus Infection on Host Gene Expression",
    "Virology/Host Antiviral Responses"
  ],
  "abstract": "Background\n    \nThe host response to dengue virus infection is characterized by the production of numerous cytokines, but the overall picture appears to be complex. It has been suggested that a balance may be involved between protective and pathologic immune responses. This study aimed to define differential immune responses in association with clinical outcomes by gene expression profiling of a selected panel of inflammatory genes in whole blood samples from children with severe dengue infections.\n\n    Methodology/Principal Findings\n    \nWhole blood mRNA from 56 Indonesian children with severe dengue virus infections was analyzed during early admission and at day −1, 0, 1, and 5–8 after defervescence. Levels were related to baseline levels collected at a 1-month follow-up visit. Processing of mRNA was performed in a single reaction by multiplex ligation-dependent probe amplification, measuring mRNA levels from genes encoding 36 inflammatory proteins and 14 Toll-like receptor (TLR)-associated molecules. The inflammatory gene profiles showed up-regulation during infection of eight genes, including IFNG and IL12A, which indicated an antiviral response. On the contrary, genes associated with the nuclear factor (NF)-κB pathway were down-regulated, including NFKB1, NFKB2, TNFR1, IL1B, IL8, and TNFA. Many of these NF-κB pathway–related genes, but not IFNG or IL12A, correlated with adverse clinical events such as development of pleural effusion and hemorrhagic manifestations. The TLR profile showed that TLRs were differentially activated during severe dengue infections: increased expression of TLR7 and TLR4R3 was found together with a decreased expression of TLR1, TLR2, TLR4R4, and TLR4 co-factor CD14.\n\n    Conclusions/Significance\n    \nThese data show that different immunological pathways are differently expressed and associated with different clinical outcomes in children with severe dengue infections.",
  "fullText": "Introduction Dengue disease is emerging in the developing world at an alarming rate [1]. Moreover, the disease is potentially lethal, but only supportive measures are available. A major obstacle in the development of novel therapeutic strategies is that the pathophysiology of dengue disease is poorly understood [2],[3]. Clinically, dengue disease is a mosquito-borne disease especially affecting children in endemic, mostly tropical regions and is caused by infection with dengue virus, a member of the Flaviviridae family [4]. Though most patients only suffer from a mild febrile illness called dengue fever (DF), a relatively small group of patients experiences more severe forms of disease which are characterized by increased vascular permeability leading to bleeding manifestations and shock, such as dengue hemorrhagic fever (DHF) and dengue shock syndrome (DSS) [5]. The mechanism leading to severe, critical disease is insufficiently understood. Antibody-dependent enhancement of viral replication is generally thought to play a role since most cases of DHF/DSS occur after secondary infection by a dengue virus serotype different from the first infection [2], [6]–[8]. Severe disease is characterized by high viremia titers, coagulation abnormalities and activation of the immune system [5],[9]. The host defense strongly depends on interferon (IFN)-γ production, but other cytokines are also involved, as well as antibody production and T cell responses [10],[11]. Typically, cytokine levels are highest among the day of defervescence, which is defined as the first day after the start of fever that the body temperature returns to normal. At the same day viremia titers drop to low or undetectable levels, whereas the risk of shock development is increased [12]. The relationship between cytokine production and severe disease is not fully understood, but it is increasingly recognized that a balance is involved between protective and pathologic immune responses [2]. In general, cytokine production depends on the recognition of pathogens via Toll-like receptors (TLRs). In viral disease, TLR3, TLR7, TLR8 and TLR9 are commonly involved, but specific knowledge about TLRs in dengue infections is limited [13]–[15]. RNA-based gene expression profiling methods offer the opportunity to examine complex biological processes in a large context. We previously developed and validated an RT-PCR based assay for the measurement of multiple mRNA levels in a single reaction, multiplex ligation-dependent probe amplification (MLPA) [16]–[19]. The MLPA panel includes 36 target genes of various mediators of inflammation, such as cytokines, chemokines and intracellular signalling molecules and in addition a second panel has been developed to measure 14 TLR associated genes [20]. In order to investigate patterns of innate immune responses in severe dengue infections in children at the level of gene transcription, blood samples were collected from a group of children with severe dengue disease in Indonesia. The samples were analyzed using MLPA technology and associated with clinical outcomes. Materials and Methods Study design The study was designed as a prospective, observational cohort study and was conducted in the Dr. Kariadi University Hospital in Semarang, Indonesia, simultaneously with an other study investigating the pathophysiology of hemorrhagic tendencies in dengue virus infections [21]; from this"
}