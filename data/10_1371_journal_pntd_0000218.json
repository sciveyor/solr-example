{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000218",
  "doi": "10.1371/journal.pntd.0000218",
  "externalIds": [
    "pii:07-PNTD-RA-0308R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Differential Release and Phagocytosis of Tegument Glycoconjugates in Neurocysticercosis: Implications for Immune Evasion Strategies",
  "authors": [
    {
      "name": "Jorge I. Alvarez",
      "first": "Jorge I.",
      "last": "Alvarez",
      "affiliation": "Department of Microbiology and Immunology, University of Texas Health Science Center at San Antonio, San Antonio, Texas, United States of America; Department of Biology and South Texas Center for Emerging Infectious Diseases, University of Texas at San Antonio, San Antonio, Texas, United States of America"
    },
    {
      "name": "Jennifer Rivera",
      "first": "Jennifer",
      "last": "Rivera",
      "affiliation": "Department of Microbiology and Immunology, University of Texas Health Science Center at San Antonio, San Antonio, Texas, United States of America"
    },
    {
      "name": "Judy M. Teale",
      "first": "Judy M.",
      "last": "Teale",
      "affiliation": "Department of Microbiology and Immunology, University of Texas Health Science Center at San Antonio, San Antonio, Texas, United States of America; Department of Biology and South Texas Center for Emerging Infectious Diseases, University of Texas at San Antonio, San Antonio, Texas, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-04",
  "dateAccepted": "2008-02-28",
  "dateReceived": "2007-11-29",
  "volume": "2",
  "number": "4",
  "pages": "e218",
  "tags": [
    "Immunology/Immunomodulation",
    "Infectious Diseases/Helminth Infections",
    "Pathology/Histopathology",
    "Pathology/Neuropathology"
  ],
  "abstract": "Neurocysticercosis (NCC) is an infection of the central nervous system (CNS) by the metacestode of the helminth Taenia solium. The severity of the symptoms is associated with the intensity of the immune response. First, there is a long asymptomatic period where host immunity seems incapable of resolving the infection, followed by a chronic hypersensitivity reaction. Since little is known about the initial response to this infection, a murine model using the cestode Mesocestoides corti (syn. Mesocestoides vogae) was employed to analyze morphological changes in the parasite early in the infection. It was found that M. corti material is released from the tegument making close contact with the nervous tissue. These results were confirmed by infecting murine CNS with ex vivo–labeled parasites. Because more than 95% of NCC patients exhibit humoral responses against carbohydrate-based antigens, and the tegument is known to be rich in glycoconjugates (GCs), the expression of these types of molecules was analyzed in human, porcine, and murine NCC specimens. To determine the GCs present in the tegument, fluorochrome-labeled hydrazides as well as fluorochrome-labeled lectins with specificity to different carbohydrates were used. All the lectins utilized labeled the tegument. GCs bound by isolectinB4 were shed in the first days of infection and not resynthesized by the parasite, whereas GCs bound by wheat germ agglutinin and concavalinA were continuously released throughout the infectious process. GCs bound by these three lectins were taken up by host cells. Peanut lectin-binding GCs, in contrast, remained on the parasite and were not detected in host cells. The parasitic origin of the lectin-binding GCs found in host cells was confirmed using antibodies against T. solium and M. corti. We propose that both the rapid and persistent release of tegumental GCs plays a key role in the well-known immunomodulatory effects of helminths, including immune evasion and life-long inflammatory sequelae seen in many NCC patients.",
  "fullText": "Introduction Neurocysticercosis (NCC), caused by the larval form of the tapeworm T. solium, is one of the most common parasitic infections of the CNS worldwide [1],[2]. Although the metacestodes reach their mature size within a few weeks, evidence indicates that prior to clinical manifestations, there is a long asymptomatic period (months or even years) thought to be the result of numerous mechanisms that the organism uses to modulate and inhibit the immune response [3]. Eventually, clinical symptoms appear and include headache, seizures, and hydrocephalus that can be devastating and lifelong. Symptoms are normally associated with chronic inflammatory responses suggesting a source of persistent antigen [2],[4]. Thus, immune evasion and persistent antigen appear to be important characteristics of this disease process. Our laboratory has developed a mouse model of NCC using the similar cestode M. corti. This organism does not infect the brain naturally as does T. solium and is therefore missing the normal progression of the immature larva to the more mature cysticerci and potential antigenic changes. Another drawback is that the parasite is able to proliferate and invade brain tissue. Nevertheless, our multiple studies of CNS infection-induced immune responses using M. corti have resulted in many findings that parallel that of the natural infection in humans and pigs [5]–[11]. Therefore, it remains an important model for helping to dissect mechanisms of disease pathogenesis. In the last two decades, major interest has been placed in understanding both the molecular nature of the antigens associated with disease and elucidation of their role in immune response and vaccine development [12]–[18]. It has been shown that the glycosidic portion of glycoproteins and other glycoconjugates (GCs) expressed by T. solium metacestodes are highly antigenic, being recognized by serum from infected patients and mainly studied as potential targets in serological diagnosis [14],[16],[19]. These GCs may also play an important role in parasite-host interactions as well as in the modulation of the immune response [3]. Part of this strategy concerns the tegument or external surface molecules present on the parasite. The tegument of helminths such as T. solium and M. corti consist of a syncytium organized into two zones; an anucleate area called distal cytoplasm and a nucleated area known as the proximal cytoplasm [20]–[22]. The distal cytoplasm contains some mitochondria, vesicles and discoidal bodies that appear to be involved in the formation and replacement of the outer-surface membranes [22],[23]. In helminths, the external surface is dynamically responsive to changing host environments or immune attack and under these adverse circumstances can rapidly shed layers [24]. Therefore, surface bound antibodies, complement and activated immune effector cells can be sloughed off. Material that is released from the tegument can act as a smokescreen diverting the immune response to static deposits of antigen separated from the parasite itself [24]. In addition, the high antigenicity of T. solium GCs may play a role in hypersensitivity reactions [11] and ultimately to pathological symptoms and disease. To better understand the role of tegument GCs, it is important to determine their localization and"
}