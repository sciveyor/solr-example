{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000914",
  "doi": "10.1371/journal.pntd.0000914",
  "externalIds": [
    "pii:10-PNTD-RA-1467R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Bayesian Geostatistical Analysis and Prediction of Rhodesian Human African Trypanosomiasis",
  "authors": [
    {
      "name": "Nicola A. Wardrop",
      "first": "Nicola A.",
      "last": "Wardrop",
      "affiliation": "Centre for Infectious Diseases, Division of Pathway Medicine, College of Medicine and Veterinary Medicine, University of Edinburgh, Edinburgh, United Kingdom; School of Geography, University of Southampton, Southampton, United Kingdom"
    },
    {
      "name": "Peter M. Atkinson",
      "first": "Peter M.",
      "last": "Atkinson",
      "affiliation": "School of Geography, University of Southampton, Southampton, United Kingdom"
    },
    {
      "name": "Peter W. Gething",
      "first": "Peter W.",
      "last": "Gething",
      "affiliation": "Spatial Ecology and Epidemiology Group, Department of Zoology, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Eric M. Fèvre",
      "first": "Eric M.",
      "last": "Fèvre",
      "affiliation": "Centre for Infectious Diseases, Institute of Immunology and Infection Research, University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Kim Picozzi",
      "first": "Kim",
      "last": "Picozzi",
      "affiliation": "Centre for Infectious Diseases, Division of Pathway Medicine, College of Medicine and Veterinary Medicine, University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Abbas S. L. Kakembo",
      "first": "Abbas S. L.",
      "last": "Kakembo",
      "affiliation": "Ministry of Health, Department of National Disease Control, Uganda"
    },
    {
      "name": "Susan C. Welburn",
      "first": "Susan C.",
      "last": "Welburn",
      "affiliation": "Centre for Infectious Diseases, Division of Pathway Medicine, College of Medicine and Veterinary Medicine, University of Edinburgh, Edinburgh, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-12",
  "dateAccepted": "2010-11-15",
  "dateReceived": "2010-08-19",
  "volume": "4",
  "number": "12",
  "pages": "e914",
  "tags": [
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Mathematics/Statistics"
  ],
  "abstract": "Background\n            \nThe persistent spread of Rhodesian human African trypanosomiasis (HAT) in Uganda in recent years has increased concerns of a potential overlap with the Gambian form of the disease. Recent research has aimed to increase the evidence base for targeting control measures by focusing on the environmental and climatic factors that control the spatial distribution of the disease.\n\n            Objectives\n            \nOne recent study used simple logistic regression methods to explore the relationship between prevalence of Rhodesian HAT and several social, environmental and climatic variables in two of the most recently affected districts of Uganda, and suggested the disease had spread into the study area due to the movement of infected, untreated livestock. Here we extend this study to account for spatial autocorrelation, incorporate uncertainty in input data and model parameters and undertake predictive mapping for risk of high HAT prevalence in future.\n\n            Materials and Methods\n            \nUsing a spatial analysis in which a generalised linear geostatistical model is used in a Bayesian framework to account explicitly for spatial autocorrelation and incorporate uncertainty in input data and model parameters we are able to demonstrate a more rigorous analytical approach, potentially resulting in more accurate parameter and significance estimates and increased predictive accuracy, thereby allowing an assessment of the validity of the livestock movement hypothesis given more robust parameter estimation and appropriate assessment of covariate effects.\n\n            Results\n            \nAnalysis strongly supports the theory that Rhodesian HAT was imported to the study area via the movement of untreated, infected livestock from endemic areas. The confounding effect of health care accessibility on the spatial distribution of Rhodesian HAT and the linkages between the disease's distribution and minimum land surface temperature have also been confirmed via the application of these methods.\n\n            Conclusions\n            \nPredictive mapping indicates an increased risk of high HAT prevalence in the future in areas surrounding livestock markets, demonstrating the importance of livestock trading for continuing disease spread. Adherence to government policy to treat livestock at the point of sale is essential to prevent the spread of sleeping sickness in Uganda.",
  "fullText": "Introduction The geographical ranges of Rhodesian human African trypanosomiasis (HAT, also known as sleeping sickness), caused by the Trypanosoma brucei rhodesiense parasite, and the Gambian form of the disease, caused by Trypanosoma brucei gambiense are not believed to overlap, and Uganda is the only country thought to support transmission of both diseases within its borders [1]. Since the 1980s, Rhodesian HAT has spread into eight districts in Uganda which have not previously supported transmission [1]–[6], narrowing substantially the zone currently distancing it from endemic foci of Gambian HAT [1]. Both forms of HAT are transmitted by tsetse flies (Glossina spp), and are fatal if untreated, although the speed of progression to death varies between the two (within approximately six months for Rhodesian HAT compared with years for Gambian HAT). A reservoir of infection is present for T. b. rhodesiense (predominantly livestock in Uganda) in contrast with T. b. gambiense for which no known reservoir exists other than humans. As a result, the most effective control options for the two forms of the disease differ, as do diagnostic procedures and treatment regimes. Currently, treatment is implemented based on knowledge of the areas affected by each type of HAT; Gambian HAT occurs in the north west of Uganda and Rhodesian HAT in the south east. Medical staff in endemic areas will presume infection is caused by the subtype known to exist in that area and implement the appropriate treatment regimen. A definitive diagnostic differentiation between the two parasite subtypes is difficult and requires expensive, complex methods which are not currently available in affected areas. Consequently, spatial concurrence of the two forms of HAT would compromise diagnostic and treatment protocols, resulting in a higher proportion of treatment failures and placing increased pressure on an already stretched health system [7]. Recent research has focused attention on the environmental and climatic variables involved in the spatial distribution and spread of Rhodesian HAT [4], [6], [8], [9]. It is believed that the spread of Rhodesian HAT into Tororo district in 1984 was encouraged by the political and economic situation in the country during an epidemic which began in the early 1970s. A lack of resources and trained personnel, insufficient control efforts and large volumes of uncontrolled population movements through tsetse infested bush ultimately led to the spread into Tororo district [2], [3], [10]. Subsequent movement further north west in 1998, into Soroti district, was attributed to the trading of untreated, infected livestock from T. b. rhodesiense endemic areas at a local livestock market [4]. Since this finding, regulations have been introduced requiring the treatment (with trypanocidal drugs) of all cattle from endemic areas prior to the issue of transport permits [11]. However, the further subsequent spread into Kaberamaido and Dokolo districts in 2004 has raised questions over the implementation of these regulations, with a recent study indicating that this new expansion could also be due to the movement of untreated livestock from endemic areas [6]. It is well documented that the focal distribution of human HAT"
}