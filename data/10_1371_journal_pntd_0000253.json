{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000253",
  "doi": "10.1371/journal.pntd.0000253",
  "externalIds": [
    "pii:07-PNTD-RA-0257R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Single-Step Sequencing Method for the Identification of Mycobacterium tuberculosis Complex Species",
  "authors": [
    {
      "name": "Zoheira Djelouadji",
      "first": "Zoheira",
      "last": "Djelouadji",
      "affiliation": "Unité des Rickettsies CNRS UMR6020, IFR 48, Faculté de Médecine, Université de la Méditerranée, Marseille, France"
    },
    {
      "name": "Didier Raoult",
      "first": "Didier",
      "last": "Raoult",
      "affiliation": "Unité des Rickettsies CNRS UMR6020, IFR 48, Faculté de Médecine, Université de la Méditerranée, Marseille, France"
    },
    {
      "name": "Mamadou Daffé",
      "first": "Mamadou",
      "last": "Daffé",
      "affiliation": "Département de Mécanismes Moléculaires des Infections Mycobactériennes, Institut de Pharmacologie et Biologie structurale, Toulouse, France"
    },
    {
      "name": "Michel Drancourt",
      "first": "Michel",
      "last": "Drancourt",
      "affiliation": "Unité des Rickettsies CNRS UMR6020, IFR 48, Faculté de Médecine, Université de la Méditerranée, Marseille, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-06",
  "dateAccepted": "2008-05-20",
  "dateReceived": "2007-10-11",
  "volume": "2",
  "number": "6",
  "pages": "e253",
  "tags": [
    "Microbiology"
  ],
  "abstract": "Background\n    \nThe Mycobacterium tuberculosis complex (MTC) comprises closely related species responsible for strictly human and zoonotic tuberculosis. Accurate species determination is useful for the identification of outbreaks and epidemiological links. Mycobacterium africanum and Mycobacterium canettii are typically restricted to Africa and M. bovis is a re-emerging pathogen. Identification of these species is difficult and expensive.\n\n    Methodology/Principal Findings\n    \nThe Exact Tandem Repeat D (ETR-D; alias Mycobacterial Interspersed Repetitive Unit 4) was sequenced in MTC species type strains and 110 clinical isolates, in parallel to reference polyphasic identification based on phenotype profiling and sequencing of pncA, oxyR, hsp65, gyrB genes and the major polymorphism tandem repeat. Inclusion of M. tuberculosis isolates in the expanding, antibiotic-resistant Beijing clone was determined by Rv0927c gene sequencing. The ETR-D (780-bp) sequence unambiguously identified MTC species type strain except M. pinnipedii and M. microti thanks to six single nucleotide polymorphisms, variable numbers (1–7 copies) of the tandem repeat and two deletions/insertions. The ETR-D sequencing agreed with phenotypic identification in 107/110 clinical isolates and with reference polyphasic molecular identification in all isolates, comprising 98 M. tuberculosis, 5 M. bovis BCG type, 5 M. canettii, and 2 M. africanum. For M. tuberculosis isolates, the ETR-D sequence was not significantly associated with the Beijing clone.\n\n    Conclusions/Significance\n    \nETR-D sequencing allowed accurate, single-step identification of the MTC at the species level. It circumvented the current expensive, time-consuming polyphasic approach. It could be used to depict epidemiology of zoonotic and human tuberculosis, especially in African countries where several MTC species are emerging.",
  "fullText": "Introduction The Mycobacterium tuberculosis complex (MTC) comprises several closely related species responsible for strictly human and zoonotic tuberculosis (Figure 1). In addition to M. tuberculosis, which represents the leading cause of human tuberculosis worldwide and is now emerging as extensively drug-resistant tuberculosis strains [1], other MTC species have been found in patients, typically in African countries (Figure 2). Mycobacterium bovis is a re-emerging, zoonotic agent of bovine tuberculosis [2] whose prevalence probably depends on variations in direct exposure to cattle and consumption of unpasteurised dairy products [3]. The prevalence of Mycobacterium africanum type I (West Africa) and type II (East Africa) [4] has decreased in several African countries over the last decades [5],[6]. Mycobacterium canettii, a rare MTC species, has been isolated recently in patients exposed in Africa [7]. Mycobacterium microti, a vole and small rodent pathogen [8] that is closely related to the so-called Dassie-bacillus and infects small mammals in South Africa and the Middle East [9],[10], has been isolated in humans [11]. Mycobacterium caprae is a rare cause of tuberculosis in cattle [12],[13] and zoonotic tuberculosis in humans [14] while Mycobacterium pinnipedii has been isolated from seal lions and fur seals [15]. A recent description of the re-emergence of M. bovis in cattle, along with the direct interhuman transmission of this zoonotic organism [16] in a six-case cluster that included one death in United Kingdom [17], illustrates the potential of emerging and re-emerging zoonotic tuberculosis due to MTC species other than M. tuberculosis and the necessity for accurate species identification. Accurate species identification of all MTC members is warranted in order to distinguish between strict human and zoonotic tuberculosis and to trace source exposure during epidemiological studies. Indeed, phenotypic methods of identification relying on colony morphology, oxygen preference, niacin accumulation, nitrate reductase activity, growth kinetics and resistance to thiophene-2-carboxylic acid hydrazide (TCH) and PZA [18] are hampered by slow growth of MTC members and subjective interpretation of colony morphology and cross-resistance to drugs [19]. They do not always allow unambiguous species identification in every case. Recent studies of MTC species responsible for animal and human tuberculosis in tropical countries have relied on molecular methods including mycobacterial interspersed repetitive-unit-variable-number tandem-repeat (MIRU-VNTR) typing, IS6110-RFLP and spoligotyping [20]–[22]. Molecular differentiation of MTC members has been complicated by low sequence variability at the nucleotide level, illustrated by a 85–100% DNA/DNA relatedness and a 99–100% 16S rDNA sequence similarity [23],[24]. Nucleic acid-based assays such as acridinium ester-labelled DNA probes (AccuProbe; Gene Probe Inc, San Diego, CA) have proven to be reliable tools for assigning an isolate to the MTC [25],[26], but they do not allow for identification at the species level. Molecular identification based on deleted regions (RD), RD1, RD9 and RD10 [27], are limited by the necessity of interpreting negative results in the case of the absence of a specific deletion. The detection of single nucleotide polymorphisms (SNP) in the pncA gene [28], the oxyR locus [29], the mtp40 gene [30], and the restriction fragment length polymorphism of the hupB gene [31]"
}