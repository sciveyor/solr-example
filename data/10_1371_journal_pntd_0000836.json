{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000836",
  "doi": "10.1371/journal.pntd.0000836",
  "externalIds": [
    "pii:10-PNTD-RA-1004R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Chagas Disease Risk in Texas",
  "authors": [
    {
      "name": "Sahotra Sarkar",
      "first": "Sahotra",
      "last": "Sarkar",
      "affiliation": "Section of Integrative Biology, University of Texas, Austin, Texas, United States of America; Department of Philosophy, University of Texas, Austin, Texas, United States of America"
    },
    {
      "name": "Stavana E. Strutz",
      "first": "Stavana E.",
      "last": "Strutz",
      "affiliation": "Section of Integrative Biology, University of Texas, Austin, Texas, United States of America"
    },
    {
      "name": "David M. Frank",
      "first": "David M.",
      "last": "Frank",
      "affiliation": "Department of Philosophy, University of Texas, Austin, Texas, United States of America"
    },
    {
      "name": "Chissa–Louise Rivaldi",
      "first": "Chissa–Louise",
      "last": "Rivaldi",
      "affiliation": "Section of Integrative Biology, University of Texas, Austin, Texas, United States of America"
    },
    {
      "name": "Blake Sissel",
      "first": "Blake",
      "last": "Sissel",
      "affiliation": "Section of Integrative Biology, University of Texas, Austin, Texas, United States of America"
    },
    {
      "name": "Victor Sánchez–Cordero",
      "first": "Victor",
      "last": "Sánchez–Cordero",
      "affiliation": "Instituto de Biología, Universidad Nacional Autónoma de México, México City, México"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-10",
  "dateAccepted": "2010-09-02",
  "dateReceived": "2010-04-05",
  "volume": "4",
  "number": "10",
  "pages": "e836",
  "tags": [
    "Computational Biology/Ecosystem Modeling",
    "Ecology/Spatial and Landscape Ecology",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Public Health and Epidemiology"
  ],
  "abstract": "Background\n\nChagas disease, caused by Trypanosoma cruzi, remains a serious public health concern in many areas of Latin America, including México. It is also endemic in Texas with an autochthonous canine cycle, abundant vectors (Triatoma species) in many counties, and established domestic and peridomestic cycles which make competent reservoirs available throughout the state. Yet, Chagas disease is not reportable in Texas, blood donor screening is not mandatory, and the serological profiles of human and canine populations remain unknown. The purpose of this analysis was to provide a formal risk assessment, including risk maps, which recommends the removal of these lacunae.\n\nMethods and Findings\n\nThe spatial relative risk of the establishment of autochthonous Chagas disease cycles in Texas was assessed using a five–stage analysis. 1. Ecological risk for Chagas disease was established at a fine spatial resolution using a maximum entropy algorithm that takes as input occurrence points of vectors and environmental layers. The analysis was restricted to triatomine vector species for which new data were generated through field collection and through collation of post–1960 museum records in both México and the United States with sufficiently low georeferenced error to be admissible given the spatial resolution of the analysis (1 arc–minute). The new data extended the distribution of vector species to 10 new Texas counties. The models predicted that Triatoma gerstaeckeri has a large region of contiguous suitable habitat in the southern United States and México, T. lecticularia has a diffuse suitable habitat distribution along both coasts of the same region, and T. sanguisuga has a disjoint suitable habitat distribution along the coasts of the United States. The ecological risk is highest in south Texas. 2. Incidence–based relative risk was computed at the county level using the Bayesian Besag–York–Mollié model and post–1960 T. cruzi incidence data. This risk is concentrated in south Texas. 3. The ecological and incidence–based risks were analyzed together in a multi–criteria dominance analysis of all counties and those counties in which there were as yet no reports of parasite incidence. Both analyses picked out counties in south Texas as those at highest risk. 4. As an alternative to the multi–criteria analysis, the ecological and incidence–based risks were compounded in a multiplicative composite risk model. Counties in south Texas emerged as those with the highest risk. 5. Risk as the relative expected exposure rate was computed using a multiplicative model for the composite risk and a scaled population county map for Texas. Counties with highest risk were those in south Texas and a few counties with high human populations in north, east, and central Texas showing that, though Chagas disease risk is concentrated in south Texas, it is not restricted to it.\n\nConclusions\n\nFor all of Texas, Chagas disease should be designated as reportable, as it is in Arizona and Massachusetts. At least for south Texas, lower than N, blood donor screening should be mandatory, and the serological profiles of human and canine populations should be established. It is also recommended that a joint initiative be undertaken by the United States and México to combat Chagas disease in the trans–border region. The methodology developed for this analysis can be easily exported to other geographical and disease contexts in which risk assessment is of potential value.",
  "fullText": "Introduction Chagas disease, a result of infection by the hemoflagellate kinetoplastid protozoan, Trypanosoma cruzi, remains an important public health threat in Latin America [1] with an estimated 16–18 million human incidences and deaths annually [2]. While the Southern Cone Initiative [3]–[6] has interrupted the transmission of Chagas disease in several South American countries, and similar efforts are being attempted for other countries of Latin America [5]–[7], the disease is also endemic in the southern United States, especially in Texas where it is yet to be designated as reportable [8]–[13]. Moreover, patterns of human migration into Texas from endemic regions of Latin America may contribute to an increase in the risk of Chagas disease [11], [14], [15]. Because the disease has a chronic phase that may last for decades, during which parasitaemia falls to undetectable levels [7], the extent of human infection in the southern United States is at present unknown. Based entirely on demographics, Hanford et al. [10] provided an extreme estimate of more than 1 million infections for the United States with of them being in Texas. However, Bern and Montgomery [11] have criticized that estimate for using the highest possible values for all contributory factors; they provide a more credible lower estimate of for the entire United States. Infections of zoonotic origin only add to the number of infections of demographic origin and the risk of disease. So far infected vectors or hosts have been found in 82 of the 254 counties of Texas (see Table S1) though only four vector–borne human autochthonous cases have been confirmed [16]. The parasite incidence rate in vectors in Texas has been reported as being [12], [16], [17] which is higher than the reported from Phoenix, Arizona [13], but lower than the reported from Guaymas in northwestern México [18]. In contrast to Texas, the disease is reportable in Arizona and Massachusetts even though there has not been an autochthonous human case in either state, compared to the four in Texas. The other autochthonous human cases confirmed for the United States are from California [19], Tennessee [20], and Louisiana [9]. The main human Chagas disease cycle consists of the parasite, T. cruzi, being transferred from a mammalian reservoir to a human host through a vector. However, infection through blood transfusion, organ transplants, and the ingestion of infected food are also recognized mechanisms of concern; infections may also occur through congenital transmission [7], [21], [22]. A large variety of mammal species can serve as reservoirs for T. cruzi including humans and dogs [7], which means that a focus on reservoirs would not be effective for disease control. Given that no vaccine exists [23], efforts to control the disease must focus on vector control [7]. Consequently, risk assessment for Chagas disease must focus primarily on the ecology and biogeography of vector species and the incidence of the parasite, besides human social and epidemiological factors [5]. This analysis consists of a five–stage risk assessment for Chagas disease in Texas: (i) an ecological risk analysis using predicted"
}