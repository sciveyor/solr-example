{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000819",
  "doi": "10.1371/journal.pntd.0000819",
  "externalIds": [
    "pii:10-PNTD-RA-1172R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Viability and Burden of Leishmania in Extralesional Sites during Human Dermal Leishmaniasis",
  "authors": [
    {
      "name": "Ibeth Romero",
      "first": "Ibeth",
      "last": "Romero",
      "affiliation": "Centro Internacional de Entrenamiento e Investigaciones Médicas (CIDEIM), Cali, Colombia"
    },
    {
      "name": "Jair Téllez",
      "first": "Jair",
      "last": "Téllez",
      "affiliation": "Centro Internacional de Entrenamiento e Investigaciones Médicas (CIDEIM), Cali, Colombia"
    },
    {
      "name": "Yazmín Suárez",
      "first": "Yazmín",
      "last": "Suárez",
      "affiliation": "Centro Internacional de Entrenamiento e Investigaciones Médicas (CIDEIM), Cali, Colombia"
    },
    {
      "name": "Maria Cardona",
      "first": "Maria",
      "last": "Cardona",
      "affiliation": "Centro Internacional de Entrenamiento e Investigaciones Médicas (CIDEIM), Cali, Colombia"
    },
    {
      "name": "Roger Figueroa",
      "first": "Roger",
      "last": "Figueroa",
      "affiliation": "Centro Internacional de Entrenamiento e Investigaciones Médicas (CIDEIM), Cali, Colombia"
    },
    {
      "name": "Adrian Zelazny",
      "first": "Adrian",
      "last": "Zelazny",
      "affiliation": "Microbiology Service, Department of Laboratory Medicine, Clinical Center, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Nancy Gore Saravia",
      "first": "Nancy",
      "last": "Gore Saravia",
      "affiliation": "Centro Internacional de Entrenamiento e Investigaciones Médicas (CIDEIM), Cali, Colombia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-09",
  "dateAccepted": "2010-08-12",
  "dateReceived": "2010-05-14",
  "volume": "4",
  "number": "9",
  "pages": "e819",
  "tags": [
    "Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Molecular Biology"
  ],
  "abstract": "Background\n\nThe clinical and epidemiological significance of Leishmania DNA in extralesional sites is obscured by uncertainty of whether the DNA derives from viable parasites. To examine dissemination of Leishmania during active disease and the potential participation of human infection in transmission, Leishmania 7SLRNA was exploited to establish viability and estimate parasite burden in extralesional sites of dermal leishmaniasis patients.\n\nMethods\n\nThe feasibility of discriminating parasite viability by PCR of Leishmania 7SLRNA was evaluated in relation with luciferase activity of luc transfected intracellular amastigotes in dose-response assays of Glucantime cytotoxicity. Monocytes, tonsil swabs, aspirates of normal skin and lesions of 28 cutaneous and 2 mucocutaneous leishmaniasis patients were screened by kDNA amplification/Southern blot. Positive samples were analyzed by quantitative PCR of Leishmania 7SLRNA genes and transcripts.\n\nResults\n\n7SLRNA amplification coincided with luciferase activity, confirming discrimination of parasite viability. Of 22 patients presenting kDNA in extralesional samples, Leishmania 7SLRNA genes or transcripts were detected in one or more kDNA positive samples in 100% and 73% of patients, respectively. Gene and transcript copy number amplified from extralesional tissues were comparable to lesions. 7SLRNA transcripts were detected in 13/19 (68%) monocyte samples, 5/12 (42%) tonsil swabs, 4/11 (36%) normal skin aspirates, and 22/25 (88%) lesions; genes were quantifiable in 15/19 (79%) monocyte samples, 12/13 (92%) tonsil swabs, 8/11 (73%) normal skin aspirates.\n\nConclusion\n\nViable parasites are present in extralesional sites, including blood monocytes, tonsils and normal skin of dermal leishmaniasis patients. Leishmania 7SLRNA is an informative target for clinical and epidemiologic investigations of human leishmaniasis.",
  "fullText": "Introduction Prevention and control of dermal leishmaniasis in the New World continues to elude measures based on case identification and treatment. Persistent infection [1] has favored the development of secondary resistance and relapse following treatment and resolution of lesions. Moreover, primary resistance [2] supports the dissemination of tolerant or resistant organisms via anthroponotic transmission. Understanding of the dynamics and distribution of Leishmania in the human host is fundamental to the targeting of control measures and indeed to their evaluation. The ability to detect and quantify live Leishmania using molecular tools would allow crucial gaps in the natural history of human infection with Leishmania of the Viannia subgenus to be addressed. Diverse genetic targets and amplification methods have been utilized for the detection, identification and quantification of Leishmania in lesions and other tissues [3], [4], [5]. The persistence of Leishmania infection following resolution of disease has been supported by serologic, immunohistochemical and molecular evidence. Leishmania kDNA has been the principal molecular target, yielding evidence of parasites in extralesional tissues including scars, normal skin, and blood monocytes [5], [6], [7]. However, the high stability of DNA molecules and the possibility of its persistence following parasite death has raised questions concerning the interpretation of its amplification from blood and apparently healthy tissues as indicative of the presence of viable Leishmania [8]. Because of its short half life and lability, RNA has been considered a plausible indicator of viability and a diagnostic target for diverse microbial infections [9]. Hence methods such as quantitative nucleic acid sequence-based amplification (QT-NASBA) and real time PCR have been developed and used to detect RNA of microbial pathogens [3], [10], [11]. The 18S ribosomal gene, as well as its abundant RNA transcripts, have been used to detect infection and to estimate parasite burden in leishmanial lesions [10]. However, Leishmania RNA targets have yet to be exploited to establish parasite viability or the presence of parasites in extralesional tissues. 7SLRNA is an integral component of the signal recognition particle, a ribonucleoprotein complex of 6 polypeptides and RNA that mediates protein translocation across the endoplasmic reticulum. The genetic divergence of mammalian and trypanosomatid 7SLRNA, its abundance in the cytoplasm [11], [12], and short half life support the feasibility of sensitive and specific detection of live parasites by amplification of this target. We show that 7SLRNA transcript copy number is proportional to the number of live parasites and demonstrate the presence of Leishmania 7SLRNA genes and transcripts in blood monocytes, normal skin and tonsilar mucosa of patients with dermal leishmaniasis. Materials and Methods Ethics statement The study protocol, informed consent and sampling procedures were approved by the CIDEIM institutional review board for studies involving human subjects and conducted in compliance with national and international guidelines for the protection of human subjects from research risks. Written informed consent was obtained from all participants. Parasites Promastigotes of L. (Viannia) panamensis (MHCOM/CO/86/1166) transfected with the firefly luciferase (LUC) (L. panamensis-LUC) reporter gene using the pGL2-α-NEOαLUC expression vector essentially as described [13] were cultured in Schneiders'"
}