{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000840",
  "doi": "10.1371/journal.pntd.0000840",
  "externalIds": [
    "pii:10-PNTD-SM-1162R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Muscle-Sparing Approach for Recurrent Hydatidosis of the Thigh and Psoas: Report of a Rare Case",
  "authors": [
    {
      "name": "Gaetano La Greca",
      "first": "Gaetano",
      "last": "La Greca",
      "affiliation": "Department of Surgical Sciences, Organ Transplantation and Advanced Technologies, University of Catania, Cannizzaro Hospital, Catania, Italy"
    },
    {
      "name": "Elia Pulvirenti",
      "first": "Elia",
      "last": "Pulvirenti",
      "affiliation": "Department of Surgical Sciences, Organ Transplantation and Advanced Technologies, University of Catania, Cannizzaro Hospital, Catania, Italy"
    },
    {
      "name": "Salvatrice Gagliardo",
      "first": "Salvatrice",
      "last": "Gagliardo",
      "affiliation": "Department of Surgical Sciences, Organ Transplantation and Advanced Technologies, University of Catania, Cannizzaro Hospital, Catania, Italy"
    },
    {
      "name": "Maria Sofia",
      "first": "Maria",
      "last": "Sofia",
      "affiliation": "Department of Surgical Sciences, Organ Transplantation and Advanced Technologies, University of Catania, Cannizzaro Hospital, Catania, Italy"
    },
    {
      "name": "Domenico Russello",
      "first": "Domenico",
      "last": "Russello",
      "affiliation": "Department of Surgical Sciences, Organ Transplantation and Advanced Technologies, University of Catania, Cannizzaro Hospital, Catania, Italy"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-01",
  "volume": "5",
  "number": "1",
  "pages": "e840",
  "tags": [
    "Infectious Diseases/Helminth Infections",
    "Surgery"
  ],
  "fullText": "Learning Points Psoas and thigh, although rare localizations, could represent the site of primary or secondary hydatidosis. Muscular hydatidosis should always be considered in patients with previous history of such a disease, especially in cases of previous surgical dissemination. A muscle- or nerve-sparing attempt should always be performed in cases of wide diffusion as well. Presentation of Case A 46-year-old male shepherd presented with a mildly painful mass 4.5 cm in diameter localized at the right groin and thigh, diffuse edema involving the right leg lasting for 2 weeks, and fever. The patient had had eight previous operations for diffuse hydatidosis with reported intraperitoneal seeding, but further information was unavailable. Laboratory tests are shown in Table 1. An abdominal ultrasound (US) and a Doppler US of the right leg detected the presence of multiple and partially confluent cysts localized up to the Scarpa's triangle. A computed tomography (CT) scan detected a multi-cystic 18-cm mass originating from the psoas muscle (Figure 1, I and II). Other cysts were localized deeply and behind the muscular aponeurotic plane of the femoral quadriceps and abductor muscles (Figure 1, III and IV). All these findings were suggestive of diffuse hydatidosis and the patient was promptly operated on with a muscle-sparing approach, for which a written consent was obtained. Piperacillin/tazobactam was administered from the date of admission to the day of surgery. Preoperative prophylaxis with benzimidazole derivatives was not performed due to the extent of the disease, the history of recurrences, and the need to perform the operation promptly to reduce the symptoms. At surgery, the retroperitoneum was accessed and tissues surrounding the cysts were covered with sponges soaked with hypertonic saline. The cystic content was evacuated and the interior of the cyst was repeatedly washed with protoscolicides. Due to the tight adhesions with the peritoneal sac, only the lateral-lower portion of the cystic wall could be resected. No daughter cyst was found. Subsequently, the right thigh was anteriorly incised and the cysts were evacuated. The washing treatment was repeated but extensive resection was avoided to prevent any risk of unnecessary damage. Histopathological examination did not detect viable protoscolices and routine cultures performed to individuate other pathogens were negative. A CT scan performed before discharge showed the integrity of the psoas muscle (Figure 2, I and II) and the lack of residual cysts in the thigh with conservation of the muscular structures (Figure 2, III and IV). The patient was discharged after 11 days with 6 months of administration of mebendazole. Further investigations could not be performed because the patient missed the planned follow-up. Case Discussion In secondary hydatidosis, whose reported incidence is between 1.1% and 25% [1], muscle involvement may represent a recurrence of previously treated disease [2]. The pathogenesis of this condition is different than primary hydatidosis, being related to the spilling of the cystic contents during a previous operation [1], together with the lack of post-operative administration of benzimidazoles [3], [4] and the lack of follow-up. Hydatid cysts of the psoas and thigh"
}