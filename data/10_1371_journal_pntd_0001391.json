{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001391",
  "doi": "10.1371/journal.pntd.0001391",
  "externalIds": [
    "pii:PNTD-D-11-00474"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Dog Bites in Humans and Estimating Human Rabies Mortality in Rabies Endemic Areas of Bhutan",
  "authors": [
    {
      "name": "Tenzin",
      "last": "Tenzin",
      "affiliation": "The Faculty of Veterinary Science, University of Sydney, Camden, Australia; Regional Livestock Development Centre, Gelephu, Bhutan"
    },
    {
      "name": "Navneet K. Dhand",
      "first": "Navneet K.",
      "last": "Dhand",
      "affiliation": "The Faculty of Veterinary Science, University of Sydney, Camden, Australia"
    },
    {
      "name": "Tashi Gyeltshen",
      "first": "Tashi",
      "last": "Gyeltshen",
      "affiliation": "Phuentsholing General Hospital, Phuentsholing, Bhutan"
    },
    {
      "name": "Simon Firestone",
      "first": "Simon",
      "last": "Firestone",
      "affiliation": "The Faculty of Veterinary Science, University of Sydney, Camden, Australia"
    },
    {
      "name": "Chhimi Zangmo",
      "first": "Chhimi",
      "last": "Zangmo",
      "affiliation": "Jigme Dorji Wangchuk National Referral Hospital, Thimphu, Bhutan"
    },
    {
      "name": "Chimi Dema",
      "first": "Chimi",
      "last": "Dema",
      "affiliation": "Phuentsholing General Hospital, Phuentsholing, Bhutan"
    },
    {
      "name": "Rawang Gyeltshen",
      "first": "Rawang",
      "last": "Gyeltshen",
      "affiliation": "Gelephu Regional Referral Hospital, Gelephu, Bhutan"
    },
    {
      "name": "Michael P. Ward",
      "first": "Michael P.",
      "last": "Ward",
      "affiliation": "The Faculty of Veterinary Science, University of Sydney, Camden, Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-11",
  "dateAccepted": "2011-09-26",
  "dateReceived": "2011-05-12",
  "volume": "5",
  "number": "11",
  "pages": "e1391",
  "tags": [
    "Rabies",
    "Veterinary diseases",
    "Veterinary epidemiology",
    "Veterinary science",
    "Zoonotic diseases"
  ],
  "abstract": "Background\n          \nDog bites in humans are a public health problem worldwide. The issues of increasing stray dog populations, rabies outbreaks, and the risk of dogs biting humans have been frequently reported by the media in Bhutan. This study aimed to estimate the bite incidence and identify the risk factors for dog bites in humans, and to estimate human deaths from rabies in rabies endemic south Bhutan.\n\n          Methods\n          \nA hospital-based questionnaire survey was conducted during 2009–2010 among dog bites victims who visited three hospitals in Bhutan for anti-rabies vaccine injection. Decision tree modeling was used to estimate human deaths from rabies following dog bite injuries in two rabies endemic areas of south Bhutan.\n\n          Results\n          \nThree hundred and twenty four dog bite victims were interviewed. The annual incidence of dog bites differed between the hospital catchment areas: 869.8 (95% CI: 722.8–1022.5), 293.8 (240–358.2) and 284.8 (251.2–323) per 100,000 people in Gelephu, Phuentsholing and Thimphu, respectively. Males (62%) were more at risk than females (P&lt;0.001). Children aged 5–9 years were bitten more than other age groups. The majority of victims (71%) were bitten by stray dogs. No direct fatal injury was reported. In two hospital areas (Gelephu and Phuentsholing) in south Bhutan the annual incidence of death from rabies was 3.14 (95% CI: 1.57–6.29) per 100,000 population. The decision tree model predicted an equivalent annual incidence of 4.67 (95% CI: 2.53–7.53) deaths/100,000 population at risk. In the absence of post exposure prophylaxis, the model predicted 19.24 (95% CI: 13.69–25.14) deaths/year in these two areas.\n\n          Conclusions\n          \nIncreased educational awareness of people about the risk of dog bites and rabies is necessary, particularly for children in rabies endemic areas of Bhutan.",
  "fullText": "Introduction Dog bites in human are a serious public health problem and have been well documented worldwide [1], [2]. In the United States, 4.7 million people were estimated to have been bitten by dogs in 1994 (an incidence rate of 16.1/1000 in adults and 24.5/1000 in children), of whom 800,000 required medical treatment [3]. Later, a survey conducted during 2001–2003 in the USA estimated 4.5 million dog bites each year (an incidence rate of 16.6/1000 in adults and 13.1/1000 in children), an increase of 3% in adults and a decrease of 47% in children [4]. There have been similar reports of human dog bites in the United Kingdom [5], Belgium [6], Spain [7], Switzerland [8], Australia [9], India [10], and in the United Republic of Tanzania [11]. There are also several reports of dog bites incidents from other countries [12]–[17] but most cases are believed to be unreported, especially in developing countries. The consequences of dog bites to humans are many. Although the most common issue is the direct physical injury, sometimes the injuries may cause permanent disfigurement of the victims requiring reconstructive surgery [4], [18], [19], psychological trauma and post traumatic stress [6], [20], [21], and rarely attacks can be fatal [2], [22]–[24]. Dog bites also result in a large monetary expense for treatment, emergency hospitalization and post-exposure treatment for rabies [1], [25]–[28]. For instance, the annual medical cost and other expenses associated with dog bites in the USA were estimated to be between $235.6 and $ 253.7 million in 1994 [28] while the French Postal Services reported 58,000 days of sick leave resulting from 3,357 bites to postal workers costing about US $ 2.5 million in 1985 [13]. Globally ≥15 million people receive rabies prophylaxis annually, mainly for dog bite injuries [29]. In addition, dog bite incidents also have direct impacts on the dogs involved in the bites, resulting in their relinquishment to shelters and euthanasia [21], [30]–[32]. Legislative action (e.g. Dangerous Dog Acts) have also been implemented in some developed countries to ban specific breeds of dogs because of the issue of increased bite incidents [33], [34]; such legislation does not appear to have been effective in reducing the incidence and severity of the bites. However, the severity of dog bite incidents is striking in developing countries: a vast majority of victims die from rabies infection. There are an estimated 55,000 human deaths annually, particularly in Asia and Africa, due to endemic canine rabies [35]. Like other countries, dog bites are common in Bhutan because of the presence of a large number of stray dogs in the streets [36], [37]. Although no cases of directly fatal dog attacks on humans have been documented, deaths due to rabies from dog bites have been reported in south Bhutan [38], [39]. Dog bites and the presence of rabies in the south border areas of Bhutan also results in substantial cost to the government [40], [41]. For example, approximately Bhutanese ngultrum (Nu.) one million was spent on rabies vaccine from 2002"
}