{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001513",
  "doi": "10.1371/journal.pntd.0001513",
  "externalIds": [
    "pii:PNTD-D-11-00851"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The Transcriptome Analysis of Strongyloides stercoralis L3i Larvae Reveals Targets for Intervention in a Neglected Disease",
  "authors": [
    {
      "name": "Antonio Marcilla",
      "first": "Antonio",
      "last": "Marcilla",
      "affiliation": "Área de Parasitología, Departamento de Biologia Celular y Parasitología, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "Gagan Garg",
      "first": "Gagan",
      "last": "Garg",
      "affiliation": "Department of Chemistry and Biomolecular Sciences, Macquarie University, Sydney, New South Wales, Australia"
    },
    {
      "name": "Dolores Bernal",
      "first": "Dolores",
      "last": "Bernal",
      "affiliation": "Departamento de Bioquímica y Biología Molecular, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "Shoba Ranganathan",
      "first": "Shoba",
      "last": "Ranganathan",
      "affiliation": "Department of Chemistry and Biomolecular Sciences, Macquarie University, Sydney, New South Wales, Australia; Department of Biochemistry, Yong Loo Lin School of Medicine, National University of Singapore, Singapore, Singapore"
    },
    {
      "name": "Javier Forment",
      "first": "Javier",
      "last": "Forment",
      "affiliation": "Servicio de Bioinformática, Instituto de Biologia Molecular y Celular de Plantas, Universitat Politécnica de València, Ingeniero Fausto Elio, Valencia, Spain"
    },
    {
      "name": "Javier Ortiz",
      "first": "Javier",
      "last": "Ortiz",
      "affiliation": "Unidad de Bioinformática, SCSIE, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "Carla Muñoz-Antolí",
      "first": "Carla",
      "last": "Muñoz-Antolí",
      "affiliation": "Área de Parasitología, Departamento de Biologia Celular y Parasitología, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "M. Victoria Dominguez",
      "first": "M. Victoria",
      "last": "Dominguez",
      "affiliation": "Servicio de Microbiología y Parasitología, Hospital General de Castellón, Castellón, Spain"
    },
    {
      "name": "Laia Pedrola",
      "first": "Laia",
      "last": "Pedrola",
      "affiliation": "LifeSequencing S.L., Parc Científic Universitat de València, Paterna, Valencia, Spain"
    },
    {
      "name": "Juan Martinez-Blanch",
      "first": "Juan",
      "last": "Martinez-Blanch",
      "affiliation": "LifeSequencing S.L., Parc Científic Universitat de València, Paterna, Valencia, Spain"
    },
    {
      "name": "Javier Sotillo",
      "first": "Javier",
      "last": "Sotillo",
      "affiliation": "Área de Parasitología, Departamento de Biologia Celular y Parasitología, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "Maria Trelis",
      "first": "Maria",
      "last": "Trelis",
      "affiliation": "Área de Parasitología, Departamento de Biologia Celular y Parasitología, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "Rafael Toledo",
      "first": "Rafael",
      "last": "Toledo",
      "affiliation": "Área de Parasitología, Departamento de Biologia Celular y Parasitología, Universitat de València, Burjassot, Valencia, Spain"
    },
    {
      "name": "J. Guillermo Esteban",
      "first": "J. Guillermo",
      "last": "Esteban",
      "affiliation": "Área de Parasitología, Departamento de Biologia Celular y Parasitología, Universitat de València, Burjassot, Valencia, Spain"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-02",
  "dateAccepted": "2011-12-20",
  "dateReceived": "2011-08-23",
  "volume": "6",
  "number": "2",
  "pages": "e1513",
  "tags": [
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Parasitic diseases"
  ],
  "abstract": "Background\n          \nStrongyloidiasis is one of the most neglected diseases distributed worldwide with endemic areas in developed countries, where chronic infections are life threatening. Despite its impact, very little is known about the molecular biology of the parasite involved and its interplay with its hosts. Next generation sequencing technologies now provide unique opportunities to rapidly address these questions.\n\n          Principal Findings\n          \nHere we present the first transcriptome of the third larval stage of S. stercoralis using 454 sequencing coupled with semi-automated bioinformatic analyses. 253,266 raw sequence reads were assembled into 11,250 contiguous sequences, most of which were novel. 8037 putative proteins were characterized based on homology, gene ontology and/or biochemical pathways. Comparison of the transcriptome of S. strongyloides with those of other nematodes, including S. ratti, revealed similarities in transcription of molecules inferred to have key roles in parasite-host interactions. Enzymatic proteins, like kinases and proteases, were abundant. 1213 putative excretory/secretory proteins were compiled using a new pipeline which included non-classical secretory proteins. Potential drug targets were also identified.\n\n          Conclusions\n          \nOverall, the present dataset should provide a solid foundation for future fundamental genomic, proteomic and metabolomic explorations of S. stercoralis, as well as a basis for applied outcomes, such as the development of novel methods of intervention against this neglected parasite.",
  "fullText": "Introduction Strongyloidiasis caused by Strongyloides stercoralis is a soil-transmitted helminthiasis distributed worldwide, affecting more than 100 million people, with endemic areas in Southeast Asia, Latin America, sub-Saharan Africa, and parts of the southeastern United States [1], [2]. Recently, it was classified as one of the most neglected tropical diseases (NTD) [3]. Chronic infections in endemic areas may be maintained asymptomatically for decades through the autoinfective cycle with the filariform larvae L3 [1][4], [5]. The diagnosis of these chronic infections requires more sensitive diagnostic methods, particularly in low-level infections and immunocompromised patients [1]. Epidemiological studies in developed countries have identified endemic areas where misdiagnosis, inadequate treatment and the facilitation of hyperinfection syndrome by immunosupression (i.e. by the administration of steroids) are too frequent and can cause a high mortality rate ranging from 15 to 87% [5], [6]. Among these areas, an endemic area with chronic patients have been described at the Valencian Mediterranean coastal region of Spain related to environmental conditions [7]. The diagnosis of strongyloidiasis is suspected when clinical signs and symptoms, or eosinophilia is observed [8], but current definitive diagnosis of strongyloidiasis is usually made on the basis of detection of larvae in agar plate coproculture and serological diagnosis by ELISA [9], [10]. Those methods have the drawbacks of being time consuming and requiring expertise in the first case, and of low specificity due to remaining antibodies from previous infection or cross-reactive antibodies [11]. A recent paper has described a promising coproantigen ELISA based on a polyclonal rabbit antiserum raised against excretory/secretory (ES) antigens from the closely relative Strongyloides ratti [12], but the identification of S. stercoralis specific ES proteins that could be new potential targets for diagnosis is still required. Control of strongyloidiasis has relied mostly on the treatment of infected individuals with only three anthelmintic drugs: thiabendazole (no longer available), albendazole, and more recently ivermectin [3], [13]. A recent study by Suputtamongkol et al. (2011) has confirmed that both a single and double dose of oral ivermectin are more effective than a 7-day course of high dose albendazole for patients with chronic infection due to S. stercoralis [14]. The risk of developing genetic resistance against the current drugs administered (if used excessively and at suboptimal dosages) exists and is based on the experience with drug resistance in parasitic nematodes of livestock [15]. Thus, the current focus is on the discovery of novel drugs against human parasites like S. stercoralis. Such a discovery effort could be strengthened with an integrated genomic and bioinformatics approach, using functional genomic and phenomic information available for the free-living nematode Caenorhabditis elegans (see WormBase; www.wormbase.org). This nematode, which is the best characterized metazoan organism [16], [17], is considered to be related to nematodes of the order Strongylida (to which Strongyloides belong) [18]. Recent studies have reported that nearly 60% of genes in strongyloides have orthologues/homologues in C. elegans, with a wide range of biological pathways being conserved between parasitic nematodes and C. elegans [19]. The comparison of molecular data sets between nematodes should"
}