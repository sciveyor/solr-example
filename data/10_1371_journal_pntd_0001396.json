{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001396",
  "doi": "10.1371/journal.pntd.0001396",
  "externalIds": [
    "pii:PNTD-D-11-00512"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Examining the Relationship between Urogenital Schistosomiasis and HIV Infection",
  "authors": [
    {
      "name": "Pamela Sabina Mbabazi",
      "first": "Pamela Sabina",
      "last": "Mbabazi",
      "affiliation": "Department of Control of Neglected Tropical Diseases, World Health Organization, Geneva, Switzerland"
    },
    {
      "name": "Olivia Andan",
      "first": "Olivia",
      "last": "Andan",
      "affiliation": "Department of Control of Neglected Tropical Diseases, World Health Organization, Geneva, Switzerland"
    },
    {
      "name": "Daniel W. Fitzgerald",
      "first": "Daniel W.",
      "last": "Fitzgerald",
      "affiliation": "Center for Global Health, Division of Infectious Diseases, Weill Cornell Medical College, New York, New York, United States of America"
    },
    {
      "name": "Lester Chitsulo",
      "first": "Lester",
      "last": "Chitsulo",
      "affiliation": "Department of Control of Neglected Tropical Diseases, World Health Organization, Geneva, Switzerland"
    },
    {
      "name": "Dirk Engels",
      "first": "Dirk",
      "last": "Engels",
      "affiliation": "Department of Control of Neglected Tropical Diseases, World Health Organization, Geneva, Switzerland"
    },
    {
      "name": "Jennifer A. Downs",
      "first": "Jennifer A.",
      "last": "Downs",
      "affiliation": "Center for Global Health, Division of Infectious Diseases, Weill Cornell Medical College, New York, New York, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-12",
  "dateAccepted": "2011-09-23",
  "dateReceived": "2011-05-20",
  "volume": "5",
  "number": "12",
  "pages": "e1396",
  "tags": [
    "Genitourinary infections",
    "HIV",
    "HIV epidemiology",
    "HIV prevention",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Obstetrics and gynecology",
    "Parasitic diseases",
    "Schistosomiasis",
    "Urology",
    "Viral diseases"
  ],
  "abstract": "Background\n          \nUrogenital schistosomiasis, caused by infection with Schistosoma haematobium, is widespread and causes substantial morbidity on the African continent. The infection has been suggested as an unrecognized risk factor for incident HIV infection. Current guidelines recommend preventive chemotherapy, using praziquantel as a public health tool, to avert morbidity due to schistosomiasis. In individuals of reproductive age, urogenital schistosomiasis remains highly prevalent and, likely, underdiagnosed. This comprehensive literature review was undertaken to examine the evidence for a cause-effect relationship between urogenital schistosomiasis and HIV/AIDS. The review aims to support discussions of urogenital schistosomiasis as a neglected yet urgent public health challenge.\n\n          Methodology/Principal Findings\n          \nWe conducted a systematic search of the literature including online databases, clinical guidelines, and current medical textbooks. We describe plausible local and systemic mechanisms by which Schistosoma haematobium infection could increase the risk of HIV acquisition in both women and men. We also detail the effects of S. haematobium infection on the progression and transmissibility of HIV in co-infected individuals. We briefly summarize available evidence on the immunomodulatory effects of chronic schistosomiasis and the implications this might have for populations at high risk of both schistosomiasis and HIV.\n\n          Conclusions/Significance\n          \nStudies support the hypothesis that urogenital schistosomiasis in women and men constitutes a significant risk factor for HIV acquisition due both to local genital tract and global immunological effects. In those who become HIV-infected, schistosomal co-infection may accelerate HIV disease progression and facilitate viral transmission to sexual partners. Establishing effective prevention strategies using praziquantel, including better definition of treatment age, duration, and frequency of treatment for urogenital schistosomiasis, is an important public health priority. Our findings call attention to this pressing yet neglected public health issue and the potential added benefit of scaling up coverage of schistosomal treatment for populations in whom HIV infection is prevalent.",
  "fullText": "Introduction An estimated 207 million people worldwide are infected with schistosomes [1], and 85% of these cases occur in Africa [1]–[3]. Schistosomiasis is a disease of poverty that arises in areas with poor sanitation where people come into contact with urine- or feces-contaminated water as part of their daily lives [4]. Individuals living in endemic countries are most commonly infected during childhood, and the prevalence peaks between the ages of 10 and 20 years [5], [6]. For those who are continually reinfected by contaminated water, schistosomiasis causes a chronic disease over decades. While the mortality caused by schistosomiasis is low, the morbidity is high, and includes anemia, stunted growth, and decreased ability to learn in children [1]. For these reasons, the World Health Organization (WHO) recommends annual treatment for school-aged children in areas of high endemicity [7]. Schistosoma haematobium causes more than half (at least 112 million) of worldwide schistosome infections [8]. Formerly known as urinary schistosomiasis, S. haematobium infection was recently renamed “urogenital schistosomiasis” in recognition that the disease affects both the urinary and genital tracts in up to 75% of infected individuals [9]. Adult S. haematobium worms inhabit the venules surrounding organs of the pelvis where they lay between 20 and 200 eggs daily [4]. These eggs subsequently penetrate the vessel wall and move towards the lumen of the bladder. An important proportion of the eggs become sequestered in the tissue of pelvic organs such as the urinary bladder, lower ureters, cervix, vagina, prostate gland, and seminal vesicles, where they cause chronic inflammation in the affected organs. This results in a number of symptoms and signs including pelvic pain, postcoital bleeding, and an altered cervical epithelium in women [10]–[11], and ejaculatory pain, hematospermia and leukocytospermia in men [12]–[13]. Epidemiologic mapping studies of HIV and S. haematobium in Africa depict a substantial overlap, in many regions, between areas in which S. haematobium is endemic and areas in which women have a high prevalence of HIV infection (Figure 1) [14]. In particular, HIV studies report an unexplained gender quotient disfavoring women over men [15]–[16]. In rural women whose limited access to clean water more often puts them at risk for schistosomiasis, HIV prevalence also peaks at younger ages than in urban women [17]. While some of this skewing has been attributed to social, behavioral, and cultural norms [17]–[20], this unexplained gender quotient also suggests that risk factors for HIV acquisition may be different between rural and urban populations [17]. Several cross-sectional studies have reported associations between urogenital schistosomiasis and HIV, but the infection still receives relatively little attention. Global Burden of Disease (e.g. disability-adjusted life years [DALY]) calculations treat schistosomiasis cases as a single sequel, leading others to argue that the estimates should be much higher [21]–[23]. Furthermore, DALY calculations have neither examined urogenital schistosomiasis as an entity separate from intestinal schistosomiasis nor considered it as a population-attributable risk factor for HIV transmission. Thus urogenital schistosomiasis remains a neglected disease, particularly in women and men of reproductive age. This comprehensive"
}