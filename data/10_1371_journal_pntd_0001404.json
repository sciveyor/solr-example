{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001404",
  "doi": "10.1371/journal.pntd.0001404",
  "externalIds": [
    "pii:10-PNTD-RA-1520"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Toward an Open-Access Global Database for Mapping, Control, and Surveillance of Neglected Tropical Diseases",
  "authors": [
    {
      "name": "Eveline Hürlimann",
      "first": "Eveline",
      "last": "Hürlimann",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Nadine Schur",
      "first": "Nadine",
      "last": "Schur",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Konstantina Boutsika",
      "first": "Konstantina",
      "last": "Boutsika",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Anna-Sofie Stensgaard",
      "first": "Anna-Sofie",
      "last": "Stensgaard",
      "affiliation": "Department of Biology, Center for Macroecology, Evolution and Climate, University of Copenhagen, Copenhagen, Denmark; Department of Veterinary Disease Biology, DBL-Centre for Health Research and Development, University of Copenhagen, Frederiksberg, Denmark"
    },
    {
      "name": "Maiti Laserna de Himpsl",
      "first": "Maiti",
      "last": "Laserna de Himpsl",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Kathrin Ziegelbauer",
      "first": "Kathrin",
      "last": "Ziegelbauer",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Nassor Laizer",
      "first": "Nassor",
      "last": "Laizer",
      "affiliation": "University of Basel, Basel, Switzerland; Informatics, Swiss Tropical and Public Health Institute, Basel, Switzerland; The Open University of Tanzania, Dar es Salaam, United Republic of Tanzania"
    },
    {
      "name": "Lukas Camenzind",
      "first": "Lukas",
      "last": "Camenzind",
      "affiliation": "University of Basel, Basel, Switzerland; Informatics, Swiss Tropical and Public Health Institute, Basel, Switzerland"
    },
    {
      "name": "Aurelio Di Pasquale",
      "first": "Aurelio",
      "last": "Di Pasquale",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Uwem F. Ekpo",
      "first": "Uwem F.",
      "last": "Ekpo",
      "affiliation": "Department of Biological Sciences, University of Agriculture, Abeokuta, Nigeria"
    },
    {
      "name": "Christopher Simoonga",
      "first": "Christopher",
      "last": "Simoonga",
      "affiliation": "Department of Community Medicine, University of Zambia, Lusaka, Zambia; Ministry of Health, Lusaka, Zambia"
    },
    {
      "name": "Gabriel Mushinge",
      "first": "Gabriel",
      "last": "Mushinge",
      "affiliation": "Department of Community Medicine, University of Zambia, Lusaka, Zambia"
    },
    {
      "name": "Christopher F. L. Saarnak",
      "first": "Christopher F. L.",
      "last": "Saarnak",
      "affiliation": "Department of Veterinary Disease Biology, DBL-Centre for Health Research and Development, University of Copenhagen, Frederiksberg, Denmark"
    },
    {
      "name": "Jürg Utzinger",
      "first": "Jürg",
      "last": "Utzinger",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Thomas K. Kristensen",
      "first": "Thomas K.",
      "last": "Kristensen",
      "affiliation": "Department of Veterinary Disease Biology, DBL-Centre for Health Research and Development, University of Copenhagen, Frederiksberg, Denmark"
    },
    {
      "name": "Penelope Vounatsou",
      "first": "Penelope",
      "last": "Vounatsou",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-12",
  "dateAccepted": "2011-10-11",
  "dateReceived": "2011-02-07",
  "volume": "5",
  "number": "12",
  "pages": "e1404",
  "tags": [
    "Computer Science",
    "Computer science",
    "Databases",
    "Information technology"
  ],
  "abstract": "Background\n          \nAfter many years of general neglect, interest has grown and efforts came under way for the mapping, control, surveillance, and eventual elimination of neglected tropical diseases (NTDs). Disease risk estimates are a key feature to target control interventions, and serve as a benchmark for monitoring and evaluation. What is currently missing is a georeferenced global database for NTDs providing open-access to the available survey data that is constantly updated and can be utilized by researchers and disease control managers to support other relevant stakeholders. We describe the steps taken toward the development of such a database that can be employed for spatial disease risk modeling and control of NTDs.\n\n          Methodology\n          \nWith an emphasis on schistosomiasis in Africa, we systematically searched the literature (peer-reviewed journals and ‘grey literature’), contacted Ministries of Health and research institutions in schistosomiasis-endemic countries for location-specific prevalence data and survey details (e.g., study population, year of survey and diagnostic techniques). The data were extracted, georeferenced, and stored in a MySQL database with a web interface allowing free database access and data management.\n\n          Principal Findings\n          \nAt the beginning of 2011, our database contained more than 12,000 georeferenced schistosomiasis survey locations from 35 African countries available under http://www.gntd.org. Currently, the database is expanded to a global repository, including a host of other NTDs, e.g. soil-transmitted helminthiasis and leishmaniasis.\n\n          Conclusions\n          \nAn open-access, spatially explicit NTD database offers unique opportunities for disease risk modeling, targeting control interventions, disease monitoring, and surveillance. Moreover, it allows for detailed geostatistical analyses of disease distribution in space and time. With an initial focus on schistosomiasis in Africa, we demonstrate the proof-of-concept that the establishment and running of a global NTD database is feasible and should be expanded without delay.",
  "fullText": "Introduction More than half of the world's population is at risk of neglected tropical diseases (NTDs), and over 1 billion people are currently infected with one or several NTDs concurrently, with helminth infections showing the highest prevalence rates [1], [2]. Despite the life-long disabilities the NTDs might cause, they are less visible and receive lower priorities compared to, for example, the ‘big three’, that is malaria, tuberculosis, and HIV/AIDS [3], [4], because NTDs mainly affect the poorest and marginalized populations in the developing world [3], [5], [6]. Efforts are under way to control or even eliminate some of the NTDs of which the regular administration of anthelmintic drugs to at-risk populations – a strategy phrased ‘preventive chemotherapy’ – is a central feature [7]–[11]. There is a paucity of empirical estimates regarding the distribution of infection risk and burden of NTDs at the national, district, or sub-district level in most parts of the developing world [12]–[16]. Such information, however, is vital to plan and implement cost-effective and sustainable control interventions where no or only sketchy knowledge on the geographical disease distribution is available. There is a risk of missing high endemicity areas and distributing drugs to places which are not at highest priority, hence wasting human and financial resources. Consequently, integrated control efforts should be tailored to a given epidemiological setting [14]. The establishment of georeferenced databases is important to identify areas with no information on disease burden, to foster geographical modeling over time and space, and to control and monitor NTDs. In 1987 the bilingual (English and French) ‘Atlas of the Global Distribution of Schistosomiasis’ was published, which entailed country-specific maps of schistosomiasis distribution based on historical records, published reports, hospital-based data, and unpublished Ministry of Health (MoH) data [17]. While recent projects like the Global Atlas of Helminth Infections (GAHI; http://www.thiswormyworld.org) [18] and the Global Atlas of Trachoma (http://trachomaatlas.org) [19] offer maps on the estimated spatial distribution of soil-transmitted helminthiasis, schistosomiasis, and trachoma prevalence, they do not provide the underlying data for further in-depth analyses conducted by different research groups. An open-access global parasitological database for NTDs, which provides the actual data, is not available. The Swiss Tropical and Public Health Institute (Swiss TPH) in Basel, Switzerland, together with partners from the University of Copenhagen, Denmark, and the University of Zambia (UNZA) in Lusaka, Zambia, were working together in a multidisciplinary project to enhance our understanding of schistosomiasis transmission (the CONTRAST project) [20], [21]. One of the CONTRAST goals was to create a data repository on location-specific schistosomiasis prevalence surveys in sub-Saharan Africa. In this manuscript, we describe the steps taken toward the development of such an open-access schistosomiasis database which is currently expanded to a global scale and to include other NTDs (e.g., soil-transmitted helminthiasis and leishmaniasis) and that can be constantly updated based on new publications and reports, as well as field data provided by contributors. Materials and Methods Guiding Framework We selected schistosomiasis as the first disease to establish a proof-of-concept and populate our global NTD"
}