{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000778",
  "doi": "10.1371/journal.pntd.0000778",
  "externalIds": [
    "pii:09-PNTD-RA-0692R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Cross-Reactivity of Antibodies against Leptospiral Recurrent Uveitis-Associated Proteins A and B (LruA and LruB) with Eye Proteins",
  "authors": [
    {
      "name": "Ashutosh Verma",
      "first": "Ashutosh",
      "last": "Verma",
      "affiliation": "Department of Microbiology, Immunology and Molecular Genetics, University of Kentucky College of Medicine, Lexington, Kentucky, United States of America"
    },
    {
      "name": "Pawan Kumar",
      "first": "Pawan",
      "last": "Kumar",
      "affiliation": "Gluck Equine Research Center, University of Kentucky, Lexington, Kentucky, United States of America"
    },
    {
      "name": "Kelly Babb",
      "first": "Kelly",
      "last": "Babb",
      "affiliation": "Department of Microbiology, Immunology and Molecular Genetics, University of Kentucky College of Medicine, Lexington, Kentucky, United States of America"
    },
    {
      "name": "John F. Timoney",
      "first": "John F.",
      "last": "Timoney",
      "affiliation": "Gluck Equine Research Center, University of Kentucky, Lexington, Kentucky, United States of America"
    },
    {
      "name": "Brian Stevenson",
      "first": "Brian",
      "last": "Stevenson",
      "affiliation": "Department of Microbiology, Immunology and Molecular Genetics, University of Kentucky College of Medicine, Lexington, Kentucky, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-08",
  "dateAccepted": "2010-06-30",
  "dateReceived": "2009-12-01",
  "volume": "4",
  "number": "8",
  "pages": "e778",
  "tags": [
    "Infectious Diseases/Bacterial Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Microbiology/Cellular Microbiology and Pathogenesis"
  ],
  "abstract": "Infection by Leptospira interrogans has been causally associated with human and equine uveitis. Studies in our laboratories have demonstrated that leptospiral lipoprotein LruA and LruB are expressed in the eyes of uveitic horses, and that antibodies directed against LruA and LruB react with equine lenticular and retinal extracts, respectively. These reactivities were investigated further by performing immunofluorescent assays on lenticular and retinal tissue sections. Incubation of lens tissue sections with LruA-antiserum and retinal sections with LruB-antiserum resulted in positive fluorescence. By employing two-dimensional gel analyses followed by immunoblotting and mass spectrometry, lens proteins cross-reacting with LruA antiserum were identified to be α-crystallin B and vimentin. Similarly, mass spectrometric analyses identified β-crystallin B2 as the retinal protein cross-reacting with LruB-antiserum. Purified recombinant human α-crystallin B and vimentin were recognized by LruA-directed antiserum, but not by control pre-immune serum. Recombinant β-crystallin B2 was likewise recognized by LruB-directed antiserum, but not by pre-immune serum. Moreover, uveitic eye fluids contained significantly higher levels of antiibodies that recognized α-crystallin B, β-crystallin B2 and vimentin than did normal eye fluids. Our results indicate that LruA and LruB share immuno-relevant epitopes with eye proteins, suggesting that cross-reactive antibody interactions with eye antigens may contribute to immunopathogenesis of Leptospira-associated recurrent uveitis.",
  "fullText": "Introduction Infectious disease caused by spirochetes of the genus Leptospira is a veterinary and public health problem of global proportions [1], [2]. Humans and other mammals are exposed to the organism when they contact groundwater contaminated with urine from carrier animals. The disease in humans varies from a mild flu-like form to a more severe syndrome involving multiorgan failure and death [3]. Uveitis is a common complication of systemic infection in humans affecting one or both eyes [4]. In equines, infection is mainly associated with spontaneous abortion in mares and recurrent uveitis [3]. After an initial infection, some horses develop a recurrent inflammation of the uveal tract of eye (iris, ciliary body and choroid), known as equine recurrent uveitis (ERU) or ‘moon blindness’. First described in 1819 by James Wardrop as a “specific inflammation” of uveal origin, it is the most common cause of blindness in horses worldwide [5], [6] with a prevalence of approximately 8–10% in the United States [7]. Onset of the disease is usually acute with variable degrees of severity and duration. The acute phase is followed by a quiescent phase of no or low inflammation [8]. Subsequent recurrence of inflammation results in pronounced lesions with guarded prognosis for preservation of visual acuity [8], [9], [10], [11]. The Appaloosa breed and horses with MHC class I haplotype ELA-A9 have been observed to be at increased risk of developing uveitis [12], [13]. Leptospira interrogans serovar Pomona is the most common and well-documented infectious cause of ERU in the United States [14]. Its association with pathogenic leptospires has been well established by presence of high titers of leptospiral agglutinins in the blood and aqueous humor [15], [16], by isolation of Leptospira from ocular fluids [17], [18] and the detection of leptospiral DNA by polymerase chain reaction in vitreous humor of uveitic horses [17]. Initial evidence of the association was provided by Morter et al. [19] when they induced uveitis in ponies by subcutaneous injection of guinea pig blood containing live L. interrogans serovar Pomona. The resulting ocular pathology in experimental ponies was found to be similar to that of spontaneous cases of Leptospira-associated ERU. By using ERU uveitic fluids to screen a lambda phage library of L. interrogans, we identified leptospiral lipoproteins, LruA and LruB, associated with recurrent uveitis in horses [20]. Uveitic equine eye fluids contained significantly higher levels of immunoglobulin A (IgA) and IgG specific for LruA and LruB than did companion sera, indicating strong local antibody responses. Moreover, monospecific antiserum to LruA and LruB reacted with extracts of equine ocular tissue. In the present study we have examined the reactivity of LruA- and LruB-antiserum with sections of lens and retinal tissue and identified the ocular proteins involved in the interaction. In addition, the significance of the identified autoantigens was assessed by measuring their immuno-reactivities in eye fluids of uveitic and healthy animals. Materials and Methods Ethics statement All animals were handled in strict accordance with relevant national and international guidelines, and all animal work was approved"
}