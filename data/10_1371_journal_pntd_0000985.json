{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000985",
  "doi": "10.1371/journal.pntd.0000985",
  "externalIds": [
    "pii:10-PNTD-RA-1330R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "BCG-Mediated Protection against Mycobacterium ulcerans Infection in the Mouse",
  "authors": [
    {
      "name": "Paul J. Converse",
      "first": "Paul J.",
      "last": "Converse",
      "affiliation": "Johns Hopkins University Center for Tuberculosis Research, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Deepak V. Almeida",
      "first": "Deepak V.",
      "last": "Almeida",
      "affiliation": "Johns Hopkins University Center for Tuberculosis Research, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Eric L. Nuermberger",
      "first": "Eric L.",
      "last": "Nuermberger",
      "affiliation": "Johns Hopkins University Center for Tuberculosis Research, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Jacques H. Grosset",
      "first": "Jacques H.",
      "last": "Grosset",
      "affiliation": "Johns Hopkins University Center for Tuberculosis Research, Baltimore, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-03",
  "dateAccepted": "2011-02-14",
  "dateReceived": "2010-06-25",
  "volume": "5",
  "number": "3",
  "pages": "e985",
  "tags": [
    "Immunology/Immunity to Infections",
    "Infectious Diseases/Bacterial Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Skin Infections",
    "Microbiology/Immunity to Infections",
    "Public Health and Epidemiology/Immunization"
  ],
  "abstract": "Background\n            \nVaccination with Mycobacterium bovis bacille Calmette-Guérin (BCG) is widely used to reduce the risk of childhood tuberculosis and has been reported to have efficacy against two other mycobacterial diseases, leprosy and Buruli ulcer caused by M. ulcerans (Mu). Studies in experimental models have also shown some efficacy against infection caused by Mu. In mice, most studies use the C57BL/6 strain that is known to develop good cell-mediated protective immunity. We hypothesized that there may be differences in vaccination efficacy between C57BL/6 and the less resistant BALB/c strain.\n\n            Methods\n            \nWe evaluated BCG vaccine efficacy against challenge with ∼3×105 M. ulcerans in the right hind footpad using three strains: initially, the Australian type strain, designated Mu1617, then, a Malaysian strain, Mu1615, and a recent Ghanaian isolate, Mu1059. The latter two strains both produce mycolactone while the Australian strain has lost that capacity. CFU of both BCG and Mu and splenocyte cytokine production were determined at intervals after infection. Time to footpad swelling was assessed weekly.\n\n            Principal Findings\n            \nBCG injection induced visible scars in 95.5% of BALB/c mice but only 43.4% of C57BL/6 mice. BCG persisted at higher levels in spleens of BALB/c than C57BL/6 mice. Vaccination delayed swelling and reduced Mu CFU in BALB/c mice, regardless of challenge strain. However, vaccination was only protective against Mu1615 and Mu1617 in C57BL/6 mice. Possible correlates of the better protection of BALB/c mice included 1) the near universal development of BCG scars in these mice compared to less frequent and smaller scars observed in C57BL/6 mice and 2) the induction of sustained cytokine, e.g., IL17, production as detected in the spleens of BALB/c mice whereas cytokine production was significantly reduced, e.g., IL17, or transient, e.g., Ifnγ, in the spleens of C57BL/6 mice.\n\n            Conclusions\n            \nThe efficacy of BCG against M. ulcerans, in particular, and possibly mycobacteria in general, may vary due to differences in both host and pathogen.",
  "fullText": "Introduction BCG vaccination is widely practiced around the world, primarily to protect against tuberculosis. BCG is a safe vaccine but its efficacy against tuberculosis varies by geographical region and possibly by BCG strain due to mutations related to culturing practices in multiple laboratories for many decades. The current consensus is that it protects against disseminated tuberculosis in young children but that it has limited value in protecting against adult pulmonary tuberculosis, perhaps affording 50% protection at best [1]. On the other hand, large trials [2] have shown that even where BCG has no discernible benefit against tuberculosis, it does protect against leprosy, a disease caused by another mycobacterium, M. leprae. Against yet another mycobacterial disease, known as Mycobacterium ulcerans disease or Buruli Ulcer (BU), retrospective and prospective studies have found that BCG vaccination appears to have protective efficacy for only up to 6 months but there may be longer term protection against severe forms of BU, such as osteomyelitis [3], [4], [5], [6], [7]. A case report indicated that the Th1 type immunity following BCG vaccination changed to a Th2 type after the onset of BU [8], [9]. Our preliminary investigation of a toxin-negative Mu strain and studies by others suggested differences in host response between C57BL/6 and BALB/c mice [10], [11], [12], and therefore, studies in mice might help identify the timing and nature of the switch and allow testing of alternative ways to maintain protective immunity. Mycobacterium ulcerans disease was first described in the medical literature in 1948 in Australian patients [13]. The disease still occurs there, primarily in coastal areas visited by vacationers. In contrast, many more cases have been documented to occur in Africa, initially in the Congo [14] and Uganda [15], and then, increasingly in West Africa where it primarily affects impoverished people in rural riverine and swampy areas [16], [17], [18]. The exact mode of transmission is controversial. Bug bites are frequently but not universally recalled. M. ulcerans grows slowly at ∼30–32 °C. It was the first mycobacterium shown to produce a toxin, an immunosuppressive macrolide, named mycolactone [19], [20], [21]. Toxin-producing colonies have a yellowish color. The toxin is encoded by the pksA gene, located on a giant plasmid [22], [23], [24]. The toxin destroys subcutaneous fat cells, apparently by both apoptotic and necrotic mechanisms [25], [26], [27]. Both guinea pigs and mice have been used to model the disease and study the organism [21], [28], [29], [30]. In mice injected in the hind footpad, there is gradual, infection-dose-dependent swelling that becomes severe before the onset of ulceration and, if allowed, may progress to foot and limb loss and death [31], [32]. Here, we vaccinated BALB/c and C57BL/6 mice with BCG (Pasteur) and, after 8 weeks, challenged vaccinated and unvaccinated mice with either M. ulcerans 1059 (Mu1059), a recent clinical isolate from Ghana, or with Mu1615, a strain originally isolated from Malaysia in the 1960s. Both strains produce mycolactone and both cause a gradually severe swelling in mouse footpads in unvaccinated mice. Materials"
}