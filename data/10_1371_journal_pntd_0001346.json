{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001346",
  "doi": "10.1371/journal.pntd.0001346",
  "externalIds": [
    "pii:PNTD-D-11-00357"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Epidemiological and Entomological Evaluations after Six Years or More of Mass Drug Administration for Lymphatic Filariasis Elimination in Nigeria",
  "authors": [
    {
      "name": "Frank O. Richards",
      "first": "Frank O.",
      "last": "Richards",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Abel Eigege",
      "first": "Abel",
      "last": "Eigege",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "Emmanuel S. Miri",
      "first": "Emmanuel S.",
      "last": "Miri",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "Alphonsus Kal",
      "first": "Alphonsus",
      "last": "Kal",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "John Umaru",
      "first": "John",
      "last": "Umaru",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "Davou Pam",
      "first": "Davou",
      "last": "Pam",
      "affiliation": "University of Jos, Plateau State, Jos, Nigeria"
    },
    {
      "name": "Lindsay J. Rakers",
      "first": "Lindsay J.",
      "last": "Rakers",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Yohanna Sambo",
      "first": "Yohanna",
      "last": "Sambo",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "Jacob Danboyi",
      "first": "Jacob",
      "last": "Danboyi",
      "affiliation": "Nasarawa State Ministry of Health, Lafia, Nigeria"
    },
    {
      "name": "Bako Ibrahim",
      "first": "Bako",
      "last": "Ibrahim",
      "affiliation": "University of Jos, Plateau State, Jos, Nigeria"
    },
    {
      "name": "Solomon E. Adelamo",
      "first": "Solomon E.",
      "last": "Adelamo",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "Gladys Ogah",
      "first": "Gladys",
      "last": "Ogah",
      "affiliation": "Nasarawa State Ministry of Health, Lafia, Nigeria"
    },
    {
      "name": "Danjuma Goshit",
      "first": "Danjuma",
      "last": "Goshit",
      "affiliation": "Plateau State Ministry of Health, Jos, Nigeria"
    },
    {
      "name": "O. Kehinde Oyenekan",
      "first": "O. Kehinde",
      "last": "Oyenekan",
      "affiliation": "The Carter Center, Jos, Nigeria"
    },
    {
      "name": "Els Mathieu",
      "first": "Els",
      "last": "Mathieu",
      "affiliation": "Centers for Disease Control, Atlanta, Georgia, United States of America"
    },
    {
      "name": "P. Craig Withers",
      "first": "P. Craig",
      "last": "Withers",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Yisa A. Saka",
      "first": "Yisa A.",
      "last": "Saka",
      "affiliation": "Nigeria Federal Ministry of Health, Abuja, Nigeria"
    },
    {
      "name": "Jonathan Jiya",
      "first": "Jonathan",
      "last": "Jiya",
      "affiliation": "Nigeria Federal Ministry of Health, Abuja, Nigeria"
    },
    {
      "name": "Donald R. Hopkins",
      "first": "Donald R.",
      "last": "Hopkins",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-10",
  "dateAccepted": "2011-08-22",
  "dateReceived": "2011-04-08",
  "volume": "5",
  "number": "10",
  "pages": "e1346",
  "tags": [
    "Epidemiology",
    "Infectious Diseases",
    "Infectious diseases",
    "Lymphatic filariasis",
    "Medicine",
    "Parasitic diseases",
    "Public Health and Epidemiology",
    "Public health"
  ],
  "abstract": "The current strategy for interrupting transmission of lymphatic filariasis (LF) is annual mass drug administration (MDA), at good coverage, for 6 or more years. We describe our programmatic experience delivering the MDA combination of ivermectin and albendazole in Plateau and Nasarawa states in central Nigeria, where LF is caused by anopheline transmitted Wuchereria bancrofti. Baseline LF mapping using rapid blood antigen detection tests showed mean local government area (LGA) prevalence of 23% (range 4–62%). MDA was launched in 2000 and by 2003 had been scaled up to full geographic coverage in all 30 LGAs in the two states; over 26 million cumulative directly observed treatments were provided by community drug distributors over the intervention period. Reported treatment coverage for each round was ≥85% of the treatment eligible population of 3.7 million, although a population-based coverage survey in 2003 showed lower coverage (72.2%; 95% CI 65.5–79.0%). To determine impact on transmission, we monitored three LF infection parameters (microfilaremia, antigenemia, and mosquito infection) in 10 sentinel villages (SVs) serially. The last monitoring was done in 2009, when SVs had been treated for 7–10 years. Microfilaremia in 2009 decreased by 83% from baseline (from 4.9% to 0.8%); antigenemia by 67% (from 21.6% to 7.2%); mosquito infection rate (all larval stages) by 86% (from 3.1% to 0.4%); and mosquito infectivity rate (L3 stages) by 76% (from 1.3% to 0.3%). All changes were statistically significant. Results suggest that LF transmission has been interrupted in 5 of the 10 SVs, based on 2009 finding of microfilaremia ≥1% and/or L3 stages in mosquitoes. Four of the five SVs where transmission persists had baseline antigenemia prevalence of &gt;25%. Longer or additional interventions (e.g., more frequent MDA treatments, insecticidal bed nets) should be considered for ‘hot spots’ where transmission is ongoing.",
  "fullText": "Introduction Lymphatic Filariasis (LF) is a mosquito transmitted parasitic infection that in Africa is caused by Wuchereria bancrofti. LF, which has no animal reservoir, is largely rural and transmitted by Anopheles mosquitoes in West Africa. The adult worms reside in the human lymphatic vessels and cause lymph flow dysfunction that can result in swelling of limbs (lymphedema, elephantiasis) and genital organs (hydrocele), and painful recurrent febrile attacks of acute adenolymphangitis. Microfilariae released by gravid female W. bancrofti worms gain access to the blood stream where they circulate at night and are available for the nocturnally feeding mosquitoes. Microfilariae so ingested pass through three larval molts to reach the L3 stage in about 1–2 weeks; L3 are able to infect humans when infectious mosquitoes return to feed again. The L3 develop to adult male and female worms, where they mate in the human lymphatic system and females produce microfilariae, thus completing the life cycle of the parasite [1]. LF is considered by the World Health Organization (WHO) as one of the ‘tool ready’ neglected tropical diseases (NTDs) [2] because LF transmission can be interrupted by safe oral medications that markedly reduce nocturnal microfilaremia, resulting in fewer mosquitoes being infected when they take a blood meal [3], [4], [5], [6], [7]. Three medicines (ivermectin, diethylcarbamazine, and albendazole) are recommended; each has variable lethal affects on the adult worms, so immediate cure of the LF infection is not achieved with a single treatment [8]. WHO recommends annual community-wide mass drug administration (MDA) with 150 ug/kg of ivermectin (Mectizan®, donated for this purpose by Merck) and 400 mg of albendazole (donated by GlaxoSmithKline) for sub Saharan African LF programs [2], [8]. The potential for global LF eradication was first suggested by the International Task Force for Disease Eradication in 1993 [9]. The current WHO endorsed strategy is based on a 1997 World Health Assembly resolution (WHA50.29) to eliminate LF as a public health problem in Africa by 2020 [3], [4], [5], [6], [10]. The strategy is for MDA programs to provide treatment annually, with good coverage, for 6 years [6], [7], [8]. This is based on the assumption that the ivermectin/albendazole combination will interrupt transmission in all epidemiological settings within 6 years of reaching full geographic coverage [6], [8], [11], a contention that some have challenged [12], [13], [14], [15], [16]. WHO has provided a series of evolving guidelines for monitoring and evaluating the coverage and impact of these programs [3], [7], [17], [18]. In order to reach the goal of LF elimination by 2020, African LF elimination programs in particular need to scale up MDA to reach all targeted LF endemic populations in the next few years [6], [7]. The most populous African nation, Nigeria has an estimated population of 150 million persons. The country is comprised of 36 states and a Federal Capital Territory that are further subdivided into 774 local government areas (LGAs). Given wide LF endemicity and the size of its population, Nigeria ranks third among the most LF endemic"
}