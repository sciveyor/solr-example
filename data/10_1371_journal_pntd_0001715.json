{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001715",
  "doi": "10.1371/journal.pntd.0001715",
  "externalIds": [
    "pii:PNTD-D-12-00041"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Burkholderia pseudomallei Known Siderophores and Hemin Uptake Are Dispensable for Lethal Murine Melioidosis",
  "authors": [
    {
      "name": "Brian H. Kvitko",
      "first": "Brian H.",
      "last": "Kvitko",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Rocky Mountain Regional Center of Excellence for Biodefense and Emerging Infectious Diseases Research, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Andrew Goodyear",
      "first": "Andrew",
      "last": "Goodyear",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Rocky Mountain Regional Center of Excellence for Biodefense and Emerging Infectious Diseases Research, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Katie L. Propst",
      "first": "Katie L.",
      "last": "Propst",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Rocky Mountain Regional Center of Excellence for Biodefense and Emerging Infectious Diseases Research, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Steven W. Dow",
      "first": "Steven W.",
      "last": "Dow",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Rocky Mountain Regional Center of Excellence for Biodefense and Emerging Infectious Diseases Research, Colorado State University, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Herbert P. Schweizer",
      "first": "Herbert P.",
      "last": "Schweizer",
      "affiliation": "Department of Microbiology, Immunology and Pathology, Rocky Mountain Regional Center of Excellence for Biodefense and Emerging Infectious Diseases Research, Colorado State University, Fort Collins, Colorado, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-06",
  "dateAccepted": "2012-05-16",
  "dateReceived": "2012-01-06",
  "volume": "6",
  "number": "6",
  "pages": "e1715",
  "tags": [
    "Animal models",
    "Bacterial pathogens",
    "Bacterial physiology",
    "Bacteriology",
    "Biology",
    "Emerging infectious diseases",
    "Gene identification and analysis",
    "Genetic mutation",
    "Genetics",
    "Genetics and Genomics",
    "Genetics of disease",
    "Gram negative",
    "Host-pathogen interaction",
    "Microbial metabolism",
    "Microbial pathogens",
    "Microbiology",
    "Model organisms",
    "Molecular genetics",
    "Mouse",
    "Mutagenesis",
    "Pathogenesis"
  ],
  "abstract": "Burkholderia pseudomallei is a mostly saprophytic bacterium, but can infect humans where it causes the difficult-to-manage disease melioidosis. Even with proper diagnosis and prompt therapeutic interventions mortality rates still range from &gt;20% in Northern Australia to over 40% in Thailand. Surprisingly little is yet known about how B. pseudomallei infects, invades and survives within its hosts, and virtually nothing is known about the contribution of critical nutrients such as iron to the bacterium's pathogenesis. It was previously assumed that B. pseudomallei used iron-acquisition systems commonly found in other bacteria, for example siderophores. However, our previous discovery of a clinical isolate carrying a large chromosomal deletion missing the entire malleobactin gene cluster encoding the bacterium's major high-affinity siderophore while still being fully virulent in a murine melioidosis model suggested that other iron-acquisition systems might make contributions to virulence. Here, we deleted the major siderophore malleobactin (mba) and pyochelin (pch) gene clusters in strain 1710b and revealed a residual siderophore activity which was unrelated to other known Burkholderia siderophores such as cepabactin and cepaciachelin, and not due to increased secretion of chelators such as citrate. Deletion of the two hemin uptake loci, hmu and hem, showed that Hmu is required for utilization of hemin and hemoglobin and that Hem cannot complement a Hmu deficiency. Prolonged incubation of a hmu hem mutant in hemoglobin-containing minimal medium yielded variants able to utilize hemoglobin and hemin suggesting alternate pathways for utilization of these two host iron sources. Lactoferrin utilization was dependent on malleobactin, but not pyochelin synthesis and/or uptake. A mba pch hmu hem quadruple mutant could use ferritin as an iron source and upon intranasal infection was lethal in an acute murine melioidosis model. These data suggest that B. pseudomallei may employ a novel ferritin-iron acquisition pathway as a means to sustain in vivo growth.",
  "fullText": "Introduction Burkholderia pseudomallei is a Gram-negative bacterial pathogen that normally survives as a saprophyte in soil and water, but is also capable of infecting most mammals and causing serious infections resulting in the multifaceted disease melioidosis [1]–[7]. Even with rapid diagnosis and prompt and aggressive treatment the fatality rate for melioidosis patients still ranges from 10–20% in Australia to over 40% in Thailand. B. pseudomallei is considered an emerging pathogen and infections have been increasingly reported in many countries in tropical and subtropical regions of the world [8]–[12]. Iron is essential for bacteria, yet in almost any abiotic or biotic environment bacteria are confronted with levels of soluble iron too low to sustain growth [13]. The two main strategies used by Gram-negative bacteria to acquire biotic iron are uptake of iron-siderophore complexes and uptake of heme [14]. Because of the necessity for iron uptake, siderophore dependent uptake mechanisms are considered virulence factors and corresponding mutants are severely attenuated in animal models of infection [15]–[20]. In Burkholderia species, iron acquisition mechanisms have been best characterized in members of the Burkholderia cepacia complex (Bcc) [21]. These bacteria produce as many as four different siderophores (ornibactin, pyochelin, cepabactin and cepaciachelin). In addition, Bcc bacteria possess mechanisms for acquiring iron from heme and ferritin [21], [22]. Very little is known about iron acquisition mechanisms in B. pseudomallei. The bacterium produces a hydroxamate-type siderophore, malleobactin, that can remove iron from lactoferrin and transferrin, allowing this bacterium to grow under iron-limiting conditions [23]–[25]. Genome-wide microarray expression and whole genome sequence analyses identified genes encoding a number of other iron acquisition systems such as a pyochelin (pch) gene cluster, a heme uptake locus (hmu) and plasma membrane iron transporters [26]–[29]. Despite the recognized importance of iron acquisition systems, no data have been published about the contribution of any of these to B. pseudomallei virulence. There is evidence that iron availability influences colony morphology [30], [31](our unpublished results), a not well understood characteristic of B. pseudomallei that affects virulence and antimicrobial susceptibility [30], [31]. We previously discovered that when compared to other sequenced strains the clinical isolate 708a contains a large (&gt;130 kb) genomic deletion [32]. This deleted region includes the amrAB-oprA efflux pump operon which explains the gentamicin sensitivity of 708a. The &gt;90 gene region also contains numerous other genes that may be pertinent for B. pseudomallei's physiology and pathogenesis. Of note is absence of the complete malleobactin biosynthetic gene cluster. Despite lack of this gene cluster and, presumably, malleobactin, 708a caused human melioidosis and was fully lethal in the acute murine melioidosis model [32]. This finding was somewhat surprising because in Pseudomonas aeruginosa the analogous siderophore pyoverdine is essential for infection and full virulence [15]. Similarly, B. cenocepacia mutants lacking ornibactin showed significantly reduced virulence [17]. As we could not rule out the presence of mutations in 708a that compensated in vivo for the loss of malleobactin synthesis, we sought to elucidate the contribution of this siderophore and other annotated iron acquisition systems, including pyochelin synthesis"
}