{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000886",
  "doi": "10.1371/journal.pntd.0000886",
  "externalIds": [
    "pii:10-PNTD-RA-0825R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "LXR Deficiency Confers Increased Protection against Visceral Leishmania Infection in Mice",
  "authors": [
    {
      "name": "Kevin W. Bruhn",
      "first": "Kevin W.",
      "last": "Bruhn",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    },
    {
      "name": "Chaitra Marathe",
      "first": "Chaitra",
      "last": "Marathe",
      "affiliation": "Department of Pathology, Molecular Biology Institute, Howard Hughes Medical Institute, University of California Los Angeles, Los Angeles, California, United States of America"
    },
    {
      "name": "Ana Cláudia Maretti-Mira",
      "first": "Ana Cláudia",
      "last": "Maretti-Mira",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    },
    {
      "name": "Hong Nguyen",
      "first": "Hong",
      "last": "Nguyen",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    },
    {
      "name": "Jacquelyn Haskell",
      "first": "Jacquelyn",
      "last": "Haskell",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    },
    {
      "name": "Thu Anh Tran",
      "first": "Thu Anh",
      "last": "Tran",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    },
    {
      "name": "Veena Vanchinathan",
      "first": "Veena",
      "last": "Vanchinathan",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    },
    {
      "name": "Upasna Gaur",
      "first": "Upasna",
      "last": "Gaur",
      "affiliation": "Departments of Internal Medicine, Epidemiology and Microbiology, University of Iowa and the Veterans Affairs Medical Center, Iowa City, Iowa, United States of America"
    },
    {
      "name": "Mary E. Wilson",
      "first": "Mary E.",
      "last": "Wilson",
      "affiliation": "Departments of Internal Medicine, Epidemiology and Microbiology, University of Iowa and the Veterans Affairs Medical Center, Iowa City, Iowa, United States of America"
    },
    {
      "name": "Peter Tontonoz",
      "first": "Peter",
      "last": "Tontonoz",
      "affiliation": "Department of Pathology, Molecular Biology Institute, Howard Hughes Medical Institute, University of California Los Angeles, Los Angeles, California, United States of America"
    },
    {
      "name": "Noah Craft",
      "first": "Noah",
      "last": "Craft",
      "affiliation": "Department of Medicine, Division of Dermatology and Infectious Diseases, Harbor-University of California Los Angeles Medical Center and Los Angeles Biomedical Research Institute, Torrance, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-11",
  "dateAccepted": "2010-10-19",
  "dateReceived": "2010-01-22",
  "volume": "4",
  "number": "11",
  "pages": "e886",
  "tags": [
    "Immunology",
    "Immunology/Cellular Microbiology and Pathogenesis",
    "Immunology/Immunity to Infections",
    "Immunology/Innate Immunity",
    "Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Microbiology/Immunity to Infections",
    "Microbiology/Innate Immunity",
    "Microbiology/Medical Microbiology",
    "Microbiology/Parasitology"
  ],
  "abstract": "Background\n            \nThe liver X receptors (LXRs) are a family of nuclear receptor transcription factors that are activated by oxysterols and have defined roles in both lipid metabolism and cholesterol regulation. LXRs also affect antimicrobial responses and have anti-inflammatory effects in macrophages. As mice lacking LXRs are more susceptible to infection by intracellular bacteria Listeria monocytogenes and Mycobacterium tuberculosis, we hypothesized that LXR might also influence macrophage responses to the intracellular protozoan parasite Leishmania chagasi/infantum, a causative agent of visceral leishmaniasis.\n\n            Methods and Findings\n            \nSurprisingly, both LXRα knock-out and LXRα/LXRβ double-knock-out (DKO) mice were markedly resistant to systemic L. chagasi/infantum infection compared to wild-type mice. Parasite loads in the livers and spleens of these animals were significantly lower than in wild-type mice 28 days after challenge. Bone marrow-derived macrophages from LXR-DKO mice infected with L. chagasi/infantum in vitro in the presence of IFN-γ were able to kill parasites more efficiently than wild-type macrophages. This enhanced killing by LXR-deficient macrophages correlated with higher levels of nitric oxide produced, as well as increased gene expression of IL-1β. Additionally, LXR ligands abrogated nitric oxide production in wild-type macrophages in response to infection.\n\n            Conclusions\n            \nThese observations suggest that LXR-deficient mice and macrophages mount antimicrobial responses to Leishmania infection that are distinct from those mounted by wild-type mice and macrophages. Furthermore, comparison of these findings to other intracellular infection models suggests that LXR signaling pathways modulate host antimicrobial responses in a complex and pathogen-specific manner. The LXR pathway thus represents a potential therapeutic target for modulating immunity against Leishmania or other intracellular parasites.",
  "fullText": "Introduction Liver X receptors (LXRs) are a family of nuclear transcription factors that play an integral role in both lipid metabolism and the regulation of inflammation [1], [2]. Two isoforms of LXR exist in both mouse and human: LXRα is mainly expressed in metabolically active tissues such as liver, intestine, kidney and adipose tissue, in addition to macrophages and myeloid dendritic cells (DCs) [3]; LXRβ has a relatively ubiquitous expression pattern [4]. LXRs form functional heterodimers with retinoid X receptors (RXRs) that are activated upon binding to intracellular oxysterols, thus functioning as sensors of cellular cholesterol levels. Upon activation, LXR-RXR complexes promote expression of genes involved in cholesterol efflux, absorption, conversion to bile acids, and lipogenesis [5]. In addition to controlling these key elements of lipid homeostasis, activated LXR also inhibits the development of inflammatory pathways via repression of NF-κB signaling, particularly in macrophages [6]. This dual ability to promote cholesterol efflux and inhibit inflammation supports a protective function for the LXRs against diseases such as atherosclerosis, which are characterized by cholesterol-laden foam cells and chronic inflammation [1]. Consistent with this model, treatment with synthetic LXR agonist molecules reduces disease incidence in animal models of atherosclerosis [7]. LXR-deficient mice, conversely, develop enlarged, cholesterol-laden livers and elevated serum cholesterol upon exposure to high cholesterol diets, and are highly prone to atherosclerosis [8]. In addition to their involvement in the development of these chronic metabolic diseases, macrophages play a fundamental role in innate immune activity. The expression of LXR in macrophages, combined with the ability of these receptors to regulate inflammatory pathways, suggest a role for LXR in combating specific microbial pathogens. Previously it has been demonstrated that LXR-deficient mice are more susceptible than wild-type mice to infection with the intracellular bacterial pathogens Listeria monocytogenes [9] and Mycobacterium tuberculosis [10]. The increased susceptibility to Listeria was associated with an LXR-regulated gene expressed in macrophages called SPα, identified as having anti-apoptotic function. LXR-deficient mice displayed increased macrophage apoptosis during the course of Listeria infection, correlating with a decreased ability to clear the bacteria. Demonstration that LXR-associated pathways impact macrophage responses to an intracellular bacterium suggested that these receptors and their downstream metabolic networks might also influence immunity against other microbial pathogens. The genus Leishmania, comprised of numerous distinct species of trypanosomatid protozoa, infects predominantly macrophage cells in their mammalian hosts. Leishmania species, particularly L. major, have been studied in detail over many decades, helping to define many aspects of host immunity, including initial characterizations of TH1- versus TH2-type, CD4+ T cell responses [11]. Invasion of phagocytic host cells occurs through complex interactions between Leishmania surface structures and phagocytic receptors in the host cell [12], which are organized within cholesterol-containing lipid rafts in the outer membrane. Upon cellular entry, promastigotes remain in endosomal structures, transform into amastigotes, multiply, and inhibit immune-mediated clearance by the host through various mechanisms of immunosuppression [13]. Infected macrophages and other immune cells express important proinflammatory cytokines and molecules that play roles in parasite resistance, including IL-1, IL-6, IL-12, TNF-α,"
}