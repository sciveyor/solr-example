{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000920",
  "doi": "10.1371/journal.pntd.0000920",
  "externalIds": [
    "pii:10-PNTD-RA-1072R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Quantifying the Spatial Dimension of Dengue Virus Epidemic Spread within a Tropical Urban Environment",
  "authors": [
    {
      "name": "Gonzalo M. Vazquez-Prokopec",
      "first": "Gonzalo M.",
      "last": "Vazquez-Prokopec",
      "affiliation": "Department of Environmental Studies, Emory University, Atlanta, Georgia, United States of America; Fogarty International Center, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Uriel Kitron",
      "first": "Uriel",
      "last": "Kitron",
      "affiliation": "Department of Environmental Studies, Emory University, Atlanta, Georgia, United States of America; Fogarty International Center, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Brian Montgomery",
      "first": "Brian",
      "last": "Montgomery",
      "affiliation": "Tropical Public Health Unit Network, Queensland Health, Cairns, Queensland, Australia"
    },
    {
      "name": "Peter Horne",
      "first": "Peter",
      "last": "Horne",
      "affiliation": "Tropical Public Health Unit Network, Queensland Health, Cairns, Queensland, Australia"
    },
    {
      "name": "Scott A. Ritchie",
      "first": "Scott A.",
      "last": "Ritchie",
      "affiliation": "Tropical Public Health Unit Network, Queensland Health, Cairns, Queensland, Australia; School of Public Health, Tropical Medicine and Rehabilitation Sciences, James Cook University, Cairns, Queensland, Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-12",
  "dateAccepted": "2010-11-18",
  "dateReceived": "2010-04-14",
  "volume": "4",
  "number": "12",
  "pages": "e920",
  "tags": [
    "Ecology/Spatial and Landscape Ecology",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Viral Infections",
    "Mathematics/Statistics",
    "Public Health and Epidemiology/Epidemiology"
  ],
  "abstract": "Background\n\nDengue infection spread in naive populations occurs in an explosive and widespread fashion primarily due to the absence of population herd immunity, the population dynamics and dispersal of Ae. aegypti, and the movement of individuals within the urban space. Knowledge on the relative contribution of such factors to the spatial dimension of dengue virus spread has been limited. In the present study we analyzed the spatio-temporal pattern of a large dengue virus-2 (DENV-2) outbreak that affected the Australian city of Cairns (north Queensland) in 2003, quantified the relationship between dengue transmission and distance to the epidemic's index case (IC), evaluated the effects of indoor residual spraying (IRS) on the odds of dengue infection, and generated recommendations for city-wide dengue surveillance and control.\n\nMethods and Findings\n\nWe retrospectively analyzed data from 383 DENV-2 confirmed cases and 1,163 IRS applications performed during the 25-week epidemic period. Spatial (local k-function, angular wavelets) and space-time (Knox test) analyses quantified the intensity and directionality of clustering of dengue cases, whereas a semi-parametric Bayesian space-time regression assessed the impact of IRS and spatial autocorrelation in the odds of weekly dengue infection. About 63% of the cases clustered up to 800 m around the IC's house. Most cases were distributed in the NW-SE axis as a consequence of the spatial arrangement of blocks within the city and, possibly, the prevailing winds. Space-time analysis showed that DENV-2 infection spread rapidly, generating 18 clusters (comprising 65% of all cases), and that these clusters varied in extent as a function of their distance to the IC's residence. IRS applications had a significant protective effect in the further occurrence of dengue cases, but only when they reached coverage of 60% or more of the neighboring premises of a house.\n\nConclusion\n\nBy applying sound statistical analysis to a very detailed dataset from one of the largest outbreaks that affected the city of Cairns in recent times, we not only described the spread of dengue virus with high detail but also quantified the spatio-temporal dimension of dengue virus transmission within this complex urban environment. In areas susceptible to non-periodic dengue epidemics, effective disease prevention and control would depend on the prompt response to introduced cases. We foresee that some of the results and recommendations derived from our study may also be applicable to other areas currently affected or potentially subject to dengue epidemics.",
  "fullText": "Introduction Dengue is a mosquito-borne infection that has re-emerged as a major international public health concern over the last four decades [1], [2], [3]. Caused by four closely related yet antigenically distinct single-stranded RNA viruses (genus Flavivirus, family Flaviridae), dengue viruses persist in a horizontal Aedes aegypti-human transmission cycle [4]. Transmission and spread of dengue infection are determined by the interplay of multiple factors including the level of herd immunity in the human population; virulence characteristics of the circulating viral strain; temperature and rainfall; survival, abundance, dispersal and blood feeding behavior of female Ae. aegypti; and human density, age structure, and behavior [5], [6]. As a consequence, contact rates between humans and mosquitoes are not random, but highly clustered in space and time [7]. The increasing trends in human population growth and urban redistribution that occurred over the past 40 years, coupled with the expansion of commercial trade and the rapid movement of humans (by air travel), have reshaped the global map of dengue transmission risk [8], [9]. Currently, about 70 to 100 million cases of classic dengue infection are reported every year, with an estimated 2.1 million cases of life-threatening disease in the form of Dengue Hemorrhagic Fever (DHF)/Dengue Shock Syndrome (DSS) [10]. Furthermore, the number of dengue fever epidemics has increased dramatically, and an expansion of the dengue endemic and hyperendemic areas is indisputable [2], [8], [9]. Under current socio-epidemiological scenarios, continued geographic expansion of epidemic dengue is expected to continue (as observed in north and central Argentina, where dengue transmission was registered for the first time in 2009 [11]), and severe dengue and DHF outbreaks are expected to follow once mixing of multiple serotypes occurs. Dengue epidemics in such areas are commonly originated by viremic travelers from endemic regions [9], and can cover large areas leading to a large number of cases as a consequence of the limited (or null) population exposure to dengue viruses, the prevailing high vector abundances and the challenges faced by local vector control programs on dealing with massive outbreaks. Consistent with global trends, outbreaks of dengue have become more frequent and severe in Australia, occurring exclusively in north Queensland (NQ) [12], [13], [14], [15], [16], [17]. Originated by viral introduction via infected travelers from endemic regions, dengue outbreaks in this region are characterized by a rapid spread both in time and space, as a result of the prevailing high Ae. aegypti populations [18] and the movement of residents and tourists within and between urban centers [15]. The recent occurrence of two major and widespread epidemics in 2003 and 2008–2009 has challenged local health authorities with the question of whether NQ will join the growing list of dengue endemic regions. In Queensland, the Tropical Population Health Unit (TPHU) dependent of Queensland Health is the institution responsible for regular vector and viral surveillance, vector control, outbreak response, public education, and operational research [12], [19]. In late February 2003, TPHU was notified of three locally acquired dengue fever cases in a small industrial area in"
}