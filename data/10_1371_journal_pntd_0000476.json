{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000476",
  "doi": "10.1371/journal.pntd.0000476",
  "externalIds": [
    "pii:09-PNTD-RA-0076R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Comparative Expression Profiling of Leishmania: Modulation in Gene Expression between Species and in Different Host Genetic Backgrounds",
  "authors": [
    {
      "name": "Daniel P. Depledge",
      "first": "Daniel P.",
      "last": "Depledge",
      "affiliation": "Centre for Immunology and Infection, Department of Biology/Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Krystal J. Evans",
      "first": "Krystal J.",
      "last": "Evans",
      "affiliation": "Centre for Immunology and Infection, Department of Biology/Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Alasdair C. Ivens",
      "first": "Alasdair C.",
      "last": "Ivens",
      "affiliation": "Fios Genomics, ETTC, King's Buildings, Edinburgh, United Kingdom"
    },
    {
      "name": "Naveed Aziz",
      "first": "Naveed",
      "last": "Aziz",
      "affiliation": "Technology Facility, Department of Biology, University of York, York, United Kingdom"
    },
    {
      "name": "Asher Maroof",
      "first": "Asher",
      "last": "Maroof",
      "affiliation": "Centre for Immunology and Infection, Department of Biology/Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Paul M. Kaye",
      "first": "Paul M.",
      "last": "Kaye",
      "affiliation": "Centre for Immunology and Infection, Department of Biology/Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Deborah F. Smith",
      "first": "Deborah F.",
      "last": "Smith",
      "affiliation": "Centre for Immunology and Infection, Department of Biology/Hull York Medical School, University of York, York, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-07",
  "dateAccepted": "2009-06-02",
  "dateReceived": "2009-02-25",
  "volume": "3",
  "number": "7",
  "pages": "e476",
  "tags": [
    "Genetics and Genomics/Comparative Genomics",
    "Genetics and Genomics/Gene Expression",
    "Immunology/Immune Response",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Infectious Diseases/Skin Infections"
  ],
  "abstract": "Background\n\nGenome sequencing of Leishmania species that give rise to a range of disease phenotypes in the host has revealed highly conserved gene content and synteny across the genus. Only a small number of genes are differentially distributed between the three species sequenced to date, L. major, L. infantum and L. braziliensis. It is not yet known how many of these genes are expressed in the disease-promoting intracellular amastigotes of these species or whether genes conserved between the species are differentially expressed in the host.\n\nMethods/Principal Findings\n\nWe have used customised oligonucleotide microarrays to confirm that all of the differentially distributed genes identified by genome comparisons are expressed in intracellular amastigotes, with only a few of these subject to regulation at the RNA level. In the first large-scale study of gene expression in L. braziliensis, we show that only ∼9% of the genes analysed are regulated in their RNA expression during the L. braziliensis life cycle, a figure consistent with that observed in other Leishmania species. Comparing amastigote gene expression profiles between species confirms the proposal that Leishmania transcriptomes undergo little regulation but also identifies conserved genes that are regulated differently between species in the host. We have also investigated whether host immune competence influences parasite gene expression, by comparing RNA expression profiles in L. major amastigotes derived from either wild-type (BALB/c) or immunologically compromised (Rag2−/− γc−/−) mice. While parasite dissemination from the site of infection is enhanced in the Rag2−/− γc−/− genetic background, parasite RNA expression profiles are unperturbed.\n\nConclusion/Significance\n\nThese findings support the hypothesis that Leishmania amastigotes are pre-adapted for intracellular survival and undergo little dynamic modulation of gene expression at the RNA level. Species-specific parasite factors contributing to virulence and pathogenicity in the host may be limited to the products of a small number of differentially distributed genes or the differential regulation of conserved genes, either of which are subject to translational and/or post-translational controls.",
  "fullText": "Introduction Infection with species of the kinetoplastid parasite, Leishmania, results in a spectrum of diseases in man, termed the leishmaniases [1],[2]. These range from the non-fatal chronic cutaneous lesions arising from L. major infection to mucocutaneous leishmaniasis usually associated with L. braziliensis (classified within the sub-genus L. Viannia) and the often fatal visceralising disease, most commonly associated with L. donovani infection in the Indian sub-continent, L. chagasi in Brazil and L. infantum in the Mediterranean basin. (The last two species are generally considered to be genetically identical [3]). While the species of infecting parasite can play a defining role in disease type, the genetic background and immune response of the host are also major factors in determining clinical outcome [4],[5],[6],[7],[8],[9]. Understanding the relative contribution of these different components may enhance our understanding of pathogenicity in the leishmaniases. Sequencing and comparison of the genomes of representative lab-adapted strains of L. major, L. infantum and L. braziliensis have revealed strong conservation of gene content and synteny, with only a small number of genes identified as differentially distributed between species [1],[10]. This subset of genes, together with sequences preferentially expressed in intracellular amastigotes and/or showing differential expression between species, may be important in facilitating parasite survival and maintenance within the host. The best-characterised example of the former class is the L. donovani complex-specific A2 gene coding for an amastigote protein of as yet unknown function which, when expressed in L. major, leads to increased parasite dissemination to the viscera [11]. Recent expression profiling has identified 3–9% of genes that are modulated at the RNA level between life cycle stages of several Leishmania species [12],[13],[14],[15]. Moreover, comparisons of L. mexicana amastigote parasites grown axenically with those maintained within macrophages, either in vitro or in vivo, have shown that axenic (extracellular) amastigotes are more similar to extracellular promastigotes than to macrophage-derived (intracellular) amastigotes in their RNA profiles [12]. These observations emphasise the importance of using parasites isolated ex vivo to investigate the mechanisms of intracellular survival. To date, no comparative expression profiling has been performed on L. Viannia spp., despite the relative divergence of the genome of L. braziliensis from that of L. major or L. infantum [1]. A complicating factor in the analysis of Leishmania gene expression is the almost complete absence of defined RNA polymerase II promotors in kinetoplastid species, coupled with the characteristic bidirectional polycistronic transcription units found on individual chromosomes [16],[17],[18]. In these organisms, polycistronic precursor RNAs (which may be expressed constitutively) are processed by coupled trans-splicing and polyadenylation [19] to generate mature mRNA transcripts for translation. Expression of individual genes is regulated post-transcriptionally, a process largely dependent on RNA stabilisation mechanisms [17],[18]. This post-transcriptional regulation, coupled with an as yet unknown extent of regulation at the level of translation, results in variable correlations between gene and protein expression levels [20],[21]. Such factors place greater emphasis on identifying those genes which undergo regulation during the life cycle while also looking for differences in the relative levels of expression of conserved genes."
}