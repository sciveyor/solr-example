{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000501",
  "doi": "10.1371/journal.pntd.0000501",
  "externalIds": [
    "pii:09-PNTD-RA-0229R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Co-authorship Network Analysis: A Powerful Tool for Strategic Planning of Research, Development and Capacity Building Programs on Neglected Diseases",
  "authors": [
    {
      "name": "Carlos Medicis Morel",
      "first": "Carlos Medicis",
      "last": "Morel",
      "affiliation": "National Institute for Science and Technology on Innovation on Neglected Diseases (INCT/IDN), Center for Technological Development in Health (CDTS), Oswaldo Cruz Foundation (Fiocruz), Rio de Janeiro, Brazil"
    },
    {
      "name": "Suzanne Jacob Serruya",
      "first": "Suzanne Jacob",
      "last": "Serruya",
      "affiliation": "Department of Science and Technology (DECIT), Secretary of Science, Technology and Strategic Goods (SCTIE), Ministry of Health, Brasilia, Brazil"
    },
    {
      "name": "Gerson Oliveira Penna",
      "first": "Gerson Oliveira",
      "last": "Penna",
      "affiliation": "Secretary of Health Surveillance (SVS), Ministry of Health, Brasilia, Brazil"
    },
    {
      "name": "Reinaldo Guimarães",
      "first": "Reinaldo",
      "last": "Guimarães",
      "affiliation": "Secretary of Science, Technology and Strategic Goods (SCTIE), Ministry of Health, Brasilia, Brazil"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-08",
  "dateAccepted": "2009-07-14",
  "dateReceived": "2009-05-14",
  "volume": "3",
  "number": "8",
  "pages": "e501",
  "tags": [
    "Science Policy"
  ],
  "abstract": "Background\n\nNew approaches and tools were needed to support the strategic planning, implementation and management of a Program launched by the Brazilian Government to fund research, development and capacity building on neglected tropical diseases with strong focus on the North, Northeast and Center-West regions of the country where these diseases are prevalent.\n\nMethodology/Principal Findings\n\nBased on demographic, epidemiological and burden of disease data, seven diseases were selected by the Ministry of Health as targets of the initiative. Publications on these diseases by Brazilian researchers were retrieved from international databases, analyzed and processed with text-mining tools in order to standardize author- and institution's names and addresses. Co-authorship networks based on these publications were assembled, visualized and analyzed with social network analysis software packages. Network visualization and analysis generated new information, allowing better design and strategic planning of the Program, enabling decision makers to characterize network components by area of work, identify institutions as well as authors playing major roles as central hubs or located at critical network cut-points and readily detect authors or institutions participating in large international scientific collaborating networks.\n\nConclusions/Significance\n\nTraditional criteria used to monitor and evaluate research proposals or R&amp;D Programs, such as researchers' productivity and impact factor of scientific publications, are of limited value when addressing research areas of low productivity or involving institutions from endemic regions where human resources are limited. Network analysis was found to generate new and valuable information relevant to the strategic planning, implementation and monitoring of the Program. It afforded a more proactive role of the funding agencies in relation to public health and equity goals, to scientific capacity building objectives and a more consistent engagement of institutions and authors from endemic regions based on innovative criteria and parameters anchored on objective scientific data.",
  "fullText": "Introduction The World Health Organization (WHO) classifies diseases as Type I, Type II and Type III, which largely corresponds to Global, Neglected and Most Neglected diseases in the vocabulary of the international organization Medécins Sans Frontières (MSF) [1],[2]. Type I/Global diseases know no geographic boundaries while Type II–III/Neglected-Most Neglected are predominantly or exclusively prevalent among populations of developing countries. Types II and III diseases (from now on “neglected diseases”), being prevalent in poor regions, are not prioritized by pharmaceutical and biotechnological industries responsible for the manufacture of goods such as vaccines, drugs and diagnostic kits. This generates what is known as ‘market failures’ - the inefficient allocation of products and services through usual free market mechanisms. Several procedures have been suggested to cope with the three types of “health failures”: (i) Science failures (insufficient knowledge prevents the development of health products such as malaria and HIV vaccines): Stimulate basic, fundamental research and technological development, (ii) Market failures (high prices prevent access of drugs by needy populations): Price reduction policies (resulting e.g. from negotiations between governments and industry) or creating subsidizing mechanisms leading to lower prices and (iii) Health service failures (inexpensive drugs do not reach the patients): Fighting corruption, reducing inequalities and coping with cultural, religious or infrastructure barriers, etc. that prevent access to cheap or free drugs by poor countries [3],[4]. Several initiatives have recently been proposed to stimulate research, technological development and production of vaccines, drugs and diagnostics for neglected diseases by both Big Pharma and Small Biotech of developed countries such as “Push” mechanisms, like Public Private Partnerships (PPPs) or Partnerships for the Development of Products (PDPs), funded in general by philanthropies or governments [5],[6] and “Pull” mechanisms, like Advance Market Commitments (AMCs), Orphan drug legislation (e.g. the US Orphan Drug Act of 1983) and Priority Review Vouchers issued under the Food and Drug Administration Amendments Act of 2007 (FDAAA). These mechanisms have in general been conceptualized and implemented by the developed world and either international or philanthropic organizations. They do not take full advantage of the brainpower and infrastructure existing in middle-income developing countries or in some innovative developing countries (IDCs) [7] such as Brazil, where considerable progress has recently been made in defining and implementing a national policy for science, technology and innovation in health [8],[9],[10]. Research and development on neglected diseases is one of the key strategic areas of Brazil's priority agenda for health research [8],[11]. In 2005 the Ministry of Health together with the Ministry of Science and Technology, through their funding agencies DECIT (Department of Science and Technology, http://dtr2001.saude.gov.br/sctie/decit/index.htm) and CNPq (National Council for Scientific and Technological Development, http://www.cnpq.br/english/cnpq/index.htm), launched a joint Program to support research, technological development and innovation on six diseases that disproportionately hit poor and marginalized populations in Brazil: dengue, Chagas disease, leishmaniases, leprosy, malaria and tuberculosis. In 2008 schistosomiasis was added to the list and a 2nd call for applications instituted (http://www.cnpq.br/editais/ct/2008/034.htm). For additional detais on this DECIT/CNPq Program see Serruya et al [11],[12]. As equity and capacity"
}