{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000197",
  "doi": "10.1371/journal.pntd.0000197",
  "externalIds": [
    "pii:07-PNTD-RA-0189R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Integrating an NTD with One of “The Big Three”: Combined Malaria and Trachoma Survey in Amhara Region of Ethiopia",
  "authors": [
    {
      "name": "Paul M. Emerson",
      "first": "Paul M.",
      "last": "Emerson",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Jeremiah Ngondi",
      "first": "Jeremiah",
      "last": "Ngondi",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America; Department of Public Health and Primary Care, University of Cambridge, Cambridge, United Kingdom"
    },
    {
      "name": "Estifanos Biru",
      "first": "Estifanos",
      "last": "Biru",
      "affiliation": "The Carter Center, Addis Ababa, Ethiopia"
    },
    {
      "name": "Patricia M. Graves",
      "first": "Patricia M.",
      "last": "Graves",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Yeshewamebrat Ejigsemahu",
      "first": "Yeshewamebrat",
      "last": "Ejigsemahu",
      "affiliation": "The Carter Center, Addis Ababa, Ethiopia"
    },
    {
      "name": "Teshome Gebre",
      "first": "Teshome",
      "last": "Gebre",
      "affiliation": "The Carter Center, Addis Ababa, Ethiopia"
    },
    {
      "name": "Tekola Endeshaw",
      "first": "Tekola",
      "last": "Endeshaw",
      "affiliation": "The Carter Center, Addis Ababa, Ethiopia"
    },
    {
      "name": "Asrat Genet",
      "first": "Asrat",
      "last": "Genet",
      "affiliation": "Amhara Regional Health Bureau, Bahir Dar, Ethiopia"
    },
    {
      "name": "Aryc W. Mosher",
      "first": "Aryc W.",
      "last": "Mosher",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Mulat Zerihun",
      "first": "Mulat",
      "last": "Zerihun",
      "affiliation": "The Carter Center, Bahir Dar, Amhara National Regional State, Ethiopia"
    },
    {
      "name": "Ayennew Messele",
      "first": "Ayennew",
      "last": "Messele",
      "affiliation": "The Carter Center, Bahir Dar, Amhara National Regional State, Ethiopia"
    },
    {
      "name": "Frank O. Richards Jr",
      "first": "Frank O.",
      "last": "Richards",
      "suffix": "Jr",
      "affiliation": "The Carter Center, Atlanta, Georgia, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-03",
  "dateAccepted": "2008-01-20",
  "dateReceived": "2007-08-03",
  "volume": "2",
  "number": "3",
  "pages": "e197",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections"
  ],
  "abstract": "Background\n    \nAmhara Regional State of Ethiopia has a population of approximately 19.6 million, is prone to unstable and epidemic malaria, and is severely affected by trachoma. An integrated malaria and trachoma control program is being implemented by the Regional Health Bureau. To provide baseline data, a survey was conducted during December 2006 to estimate malaria parasite prevalence, malaria indicators, prevalence of trachoma, and trachoma risk factors in households and people of all ages in each of the ten zones of the state, excluding three urban centers (0.4% of the population).\n\n    Methodology/Principal Findings\n    \nThe study was designed to provide prevalence estimates at zone and state levels. Using multi-stage cluster random sampling, 16 clusters of 25 households were randomly selected in each of the ten zones. Household heads were interviewed for malaria indicators and trachoma risk factors (N = 4,101). All people were examined for trachoma signs (N = 17,242), and those in even-numbered households provided blood films for malaria parasite detection (N = 7,745); both thick and thin blood films were read.\n\nZonal malaria parasite prevalence ranged from 2.4% to 6.1%, with the overall state-wide prevalence being 4.6% (95% confidence interval (CI): 3.8%–5.6%). The Plasmodium falciparum: Plasmodium vivax ratio ranged from 0.9–2.1 with an overall regional ratio of 1.2. A total of 14.8% of households reported indoor residual spraying in the past year, 34.7% had at least one mosquito net, and 16.1% had one or more long-lasting insecticidal net. Zonal trachoma prevalence (trachomatous inflammation follicular [WHO grade TF] in children aged 1–9 years) ranged from 12.6% to 60.1%, with the overall state-wide prevalence being 32.7% (95% CI: 29.2%–36.5%). State-wide prevalence of trachomatous trichiasis (TT) in persons aged over fifteen was 6.2% (95% CI: 5.3–7.4), and 0.3% (95% CI: 0.2–0.5) in children aged 0–14 years. Overall, an estimated 643,904 persons (lower bound 419,274, upper bound 975,635) have TT and require immediate corrective surgery.\n\n    Conclusions/Significance\n    \nThe results provide extensive baseline data to guide planning, implementation, and evaluation of the integrated malaria and trachoma control program in Amhara. The success of the integrated survey is the first step towards demonstration that control of priority neglected tropical diseases can be integrated with one of the “big three” killer diseases.",
  "fullText": "Introduction Ethiopia is a rapidly developing country that is burdened and held back by a high prevalence of communicable disease. Of the so-called ‘big three’ killer diseases, HIV/AIDs, tuberculosis, and malaria, malaria is the most frequent cause of out-patient presentation and in-patient admission nationwide and is second only to respiratory tract infections as a cause of death in children [1]. Co-endemic with the ‘big three’ in Ethiopia are the ‘neglected tropical diseases’ of which trachoma is the most geographically widespread and cause of greatest morbidity. The Ethiopian national blindness and low vision survey conducted in 2006 suggests that Ethiopia is the most trachoma affected country in the world. The entire rural population of approximately 65 million people are at risk of blindness from trachoma. At any time there are an estimated 9 million children with clinical signs of active disease, 1.2 million adults with trachomatous trichiasis, and 354,000 persons with blindness or low vision attributed to trachoma [2]. In addition to the effects on vision and the high likelihood of developing blindness if unoperated [3], trichiasis is a terrible condition in which the eyelashes rub against the surface of the eye ball, leaving sufferers in constant and disabling pain. The irritation and pain caused by the lashes on the surface of the eye ball and cornea is exacerbated by smoke, dust and bright light which prevents people from conducting their normal routine activities such as cooking over solid fuel fires, farming in dusty environments, gathering firewood and collecting water [4]. Of the ten states in Ethiopia, Amhara Regional State is disproportionately affected by trachoma, bearing an estimated minimum of 45% of the national trichiasis burden and with approximately one in twenty of all adults suffering from trichiasis [2]. It is recognized that the geographic distribution of trachoma within the state is not uniform with some health posts reporting trachoma as the primary cause of out-patient consultations and for others, trachoma ranks below malaria and respiratory tract infection as a cause of out-patient consultation. Trachoma is a barrier to development in Amhara and controlling this disease is a state priority. Trachoma control in Amhara, and the whole of Ethiopia, is based on the World Health Organization (WHO) endorsed SAFE strategy, in which S is corrective lid surgery for patients with trichiasis, A is antibiotic treatment for individuals with signs of active disease and for mass drug administration to at-risk populations, F is facial cleanliness and hygiene promotion to prevent transmission, and E is environmental improvements such as provision of sanitation and water that address trachoma risk factors [5]. Nationally, it has been recognized that malaria is a major barrier to the development. Ethiopia including Amhara , is prone to unstable and epidemic malaria [6],[7],[8]. The Federal Ministry of Health has launched a control program of unprecedented scale and scope to relieve this burden. The program is based on personal protection/vector control, and effective case detection and treatment. It is being targeted to the entire Ethiopian population at risk of malaria (estimated"
}