{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000748",
  "doi": "10.1371/journal.pntd.0000748",
  "externalIds": [
    "pii:09-PNTD-RA-0492R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Virulent Wolbachia Infection Decreases the Viability of the Dengue Vector Aedes aegypti during Periods of Embryonic Quiescence",
  "authors": [
    {
      "name": "Conor J. McMeniman",
      "first": "Conor J.",
      "last": "McMeniman",
      "affiliation": "School of Biological Sciences, The University of Queensland, St Lucia, Queensland, Australia"
    },
    {
      "name": "Scott L. O'Neill",
      "first": "Scott L.",
      "last": "O'Neill",
      "affiliation": "School of Biological Sciences, The University of Queensland, St Lucia, Queensland, Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-07",
  "dateAccepted": "2010-06-01",
  "dateReceived": "2009-09-23",
  "volume": "4",
  "number": "7",
  "pages": "e748",
  "tags": [
    "Ecology/Environmental Microbiology",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Viral Infections"
  ],
  "abstract": "A new approach for dengue control has been proposed that relies on life-shortening strains of the obligate intracellular bacterium Wolbachia pipientis to modify mosquito population age structure and reduce pathogen transmission. Previously we reported the stable transinfection of the major dengue vector Aedes aegypti with a life-shortening Wolbachia strain (wMelPop-CLA) from the vinegar fly Drosophila melanogaster. Here, we report a further characterization of the phenotypic effects of this virulent Wolbachia infection on several life-history traits of Ae. aegypti. Minor costs of wMelPop-CLA infection for pre-imaginal survivorship, development and adult size were found. However, we discovered that the wMelPop-CLA infection dramatically decreased the viability of desiccated Ae. aegypti eggs over time. Similarly, the reproductive fitness of wMelPop-CLA infected Ae. aegypti females declined with age. These results reveal a general pattern associated with wMelPop-CLA induced pathogenesis in this mosquito species, where host fitness costs increase during aging of both immature and adult life-history stages. In addition to influencing the invasion dynamics of this particular Wolbachia strain, we suggest that the negative impact of wMelPop-CLA on embryonic quiescence may have applied utility as a tool to reduce mosquito population size in regions with pronounced dry seasons or in regions that experience cool winters.",
  "fullText": "Introduction Aedes aegypti, the primary vector of dengue viruses throughout the tropics, is a mosquito species that has strong associations with human habitation [1]. In the past, control of dengue has been complicated by an inability to eradicate Ae. aegypti from urban environments and implement sustained vector control programs [2]. These challenges have highlighted the critical need for new approaches to curb a worldwide resurgence in dengue activity [3]. A novel approach for dengue control that has been proposed involves the introduction of the obligate intracellular bacterium Wolbachia pipientis into field populations of Ae. aegypti. Wolbachia are maternally inherited bacteria that naturally infect a wide diversity of invertebrate species [4], [5], and can rapidly spread through arthropod populations by manipulations to host reproduction such as cytoplasmic incompatibility [6]. Wolbachia infections could limit dengue transmission through two distinct mechanisms. The first by introducing Wolbachia strains that reduce the survival rate and associated vectorial capacity of the mosquito population [7], [8]. The second mechanism relies on the ability of some Wolbachia strains to interfere with the ability of RNA viruses to form productive infections in insects [9], [10] and potentially modulate the vector competence of Ae. aegypti for dengue viruses. Towards this aim, we previously reported the stable transinfection of Ae. aegypti with a life-shortening Wolbachia strain wMelPop-CLA (a mosquito cell-line adapted isolate of wMelPop) [11], originally derived from the vinegar fly Drosophila melanogaster [12]. In this mosquito host, wMelPop-CLA has been shown to both reduce adult life span [11] and directly interfere with dengue virus infection [13], suggesting that this Wolbachia strain may have applied utility as a biological tool to reduce dengue transmission. However, prior to application in a field setting, a thorough understanding of any fitness effects that occur in wMelPop-CLA infected mosquitoes is required to accurately model infection dynamics and the impact of wMelPop-CLA on Ae. aegypti populations. To further characterize this novel symbiosis and identify any fitness parameters likely to influence its spread throughout mosquito populations, we examined the phenotypic effects of wMelPop-CLA infection on several life-history traits across embryonic, pre-imaginal and adult stages of Ae. aegypti. We compared the developmental time and survivorship of pre-imaginal stages from infected and uninfected Ae. aegypti strains, and the effect of this infection on adult body size. We also considered the effect of wMelPop-CLA infection on embryonic viability during egg quiescence and reproductive fitness as mosquitoes age. Methods Ethics statement The work reported in this manuscript used human volunteers for mosquito feeding as approved by the University of Queensland Human Ethics Committee - Approval 2007001379. Written consent was obtained from each participant used for blood feeding. Mosquito strains and maintenance wMelPop-CLA infected PGYP1 and tetracycline-cured PGYP1.tet strains of Ae. aegypti [11] were maintained at 25°C, 75–85% relative humidity, with a 12∶12 h light∶dark photoperiod. Larvae were reared in plastic trays (30×40×8 cm) at a set density of 150 larvae in 3 L distilled water, and fed 150 mg fish food (TetraMin Tropical Tablets, Tetra, Germany) per pan every day until"
}