{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000630",
  "doi": "10.1371/journal.pntd.0000630",
  "externalIds": [
    "pii:09-PNTD-RA-0436R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Nodular Worm Infection in Wild Chimpanzees in Western Uganda: A Risk for Human Health?",
  "authors": [
    {
      "name": "Sabrina Krief",
      "first": "Sabrina",
      "last": "Krief",
      "affiliation": "UMR 7206-USM 104-Eco-anthropologie et ethnobiologie, Muséum National d'Histoire Naturelle, Paris, France"
    },
    {
      "name": "Benjamin Vermeulen",
      "first": "Benjamin",
      "last": "Vermeulen",
      "affiliation": "UMR 7206-USM 104-Eco-anthropologie et ethnobiologie, Muséum National d'Histoire Naturelle, Paris, France; UMR AFSSA, ENVA, Paris 12, BIPAR, Ecole Nationale Vétérinaire d'Alfort, Maisons-Alfort, France"
    },
    {
      "name": "Sophie Lafosse",
      "first": "Sophie",
      "last": "Lafosse",
      "affiliation": "UMR 7206-USM 104-Eco-anthropologie et ethnobiologie, Muséum National d'Histoire Naturelle, Paris, France"
    },
    {
      "name": "John M. Kasenene",
      "first": "John M.",
      "last": "Kasenene",
      "affiliation": "Makerere University Biological Field Station, Fort Portal, Uganda"
    },
    {
      "name": "Adélaïde Nieguitsila",
      "first": "Adélaïde",
      "last": "Nieguitsila",
      "affiliation": "UMR AFSSA, ENVA, Paris 12, BIPAR, Ecole Nationale Vétérinaire d'Alfort, Maisons-Alfort, France"
    },
    {
      "name": "Madeleine Berthelemy",
      "first": "Madeleine",
      "last": "Berthelemy",
      "affiliation": "UMR 7206-USM 104-Eco-anthropologie et ethnobiologie, Muséum National d'Histoire Naturelle, Paris, France"
    },
    {
      "name": "Monique L'Hostis",
      "first": "Monique",
      "last": "L'Hostis",
      "affiliation": "Service de Parasitologie, Ecole Nationale Vétérinaire, Agroalimentaire et de l'Alimentation Nantes-Atlantique, Nantes, France"
    },
    {
      "name": "Odile Bain",
      "first": "Odile",
      "last": "Bain",
      "affiliation": "UMR 7205 CNRS, Parasitologie comparée, Muséum National d'Histoire Naturelle, Paris, France"
    },
    {
      "name": "Jacques Guillot",
      "first": "Jacques",
      "last": "Guillot",
      "affiliation": "UMR AFSSA, ENVA, Paris 12, BIPAR, Ecole Nationale Vétérinaire d'Alfort, Maisons-Alfort, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-03",
  "dateAccepted": "2010-01-26",
  "dateReceived": "2009-08-18",
  "volume": "4",
  "number": "3",
  "pages": "e630",
  "tags": [
    "Ecology/Behavioral Ecology",
    "Ecology/Conservation and Restoration Ecology",
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "This study focused on Oeosophagostomum sp., and more especially on O. bifurcum, as a parasite that can be lethal to humans and is widespread among humans and monkeys in endemic regions, but has not yet been documented in apes. Its epidemiology and the role played by non-human primates in its transmission are still poorly understood. O. stephanostomum was the only species diagnosed so far in chimpanzees. Until recently, O. bifurcum was assumed to have a high zoonotic potential, but recent findings tend to demonstrate that O. bifurcum of non-human primates and humans might be genetically distinct. As the closest relative to human beings, and a species living in spatial proximity to humans in the field site studied, Pan troglodytes is thus an interesting host to investigate. Recently, a role for chimpanzees in the emergence of HIV and malaria in humans has been documented. In the framework of our long-term health monitoring of wild chimpanzees from Kibale National Park in Western Uganda, we analysed 311 samples of faeces. Coproscopy revealed that high-ranking males are more infected than other individuals. These chimpanzees are also the more frequent crop-raiders. Results from PCR assays conducted on larvae and dried faeces also revealed that O. stephanostomum as well as O. bifurcum are infecting chimpanzees, both species co-existing in the same individuals. Because contacts between humans and great apes are increasing with ecotourism and forest fragmentation in areas of high population density, this paper emphasizes that the presence of potential zoonotic parasites should be viewed as a major concern for public health. Investigations of the parasite status of people living around the park or working inside as well as sympatric non-human primates should be planned, and further research might reveal this as a promising aspect of efforts to reinforce measures against crop-raiding.",
  "fullText": "Introduction Nodular worms (Oesophagostomum spp.) are commonly found as nematode parasites of pigs, ruminants and primates, including humans. In endemic foci in Africa, especially in Ghana and Togo, a high prevalence of Oesophagostomum bifurcum infection has been reported in human populations, one million being estimated at risk [1],[2]. Patients are mostly children aged &lt;10 years [2]. Clinical disease, due to encysted larvae, known as oesophagostomosis, sometimes leads to death [1]–[3]. The distinction between hookworm and nodular worms eggs is not possible [2] and the definitive diagnosis of oesophagostomosis in humans involved exploratory surgery or ultrasound examination. Transmission occurs through the ingestion of the infective third-stage larvae (L3) but the factors explaining such a high regional prevalence remain unknown. Eight species of Oesophagostomum have been recognized so far to occur in non-human primates [4]. Among them, O. bifurcum, O. stephanostomum and O. aculeatum are also reported in humans [3]. Human cases have been attributed to a zoonotic origin, non-human primates being proposed as a potential reservoir [3]. However experimental infection of rhesus monkey (Macaca mulata) showed that O. bifurcum obtained from humans did not effectively infect monkeys [5]. In addition, significant variations exist in lengths of adult worms isolated from humans and non-human primates [4]. The geographic distribution in humans and some non-human primates is not overlapping [6],[7] and recent molecular findings demonstrated a genetic host-affiliated sub-structuring within O. bifurcum [6],[7]. Among great apes, especially chimpanzees, bonobos and gorillas, prevalence of strongyle eggs in stools is often high and O. stephanostomum was the only species of Oesophagostomum identified so far [8]–[10]. However, little is known about the intensity of infection in terms of parasite load and clinical signs in great apes. It has been reported that wild apes develop clinical signs of oesophagostomosis as soon as in captivity [11] while the presence of parasites remains asymptomatic in wild animals. Recently fatal cases have been described in African apes from sanctuaries [12] and collected parasites were diagnosed as O. stephanostomum. Nevertheless, because of the phylogenetic and spatial proximity between humans and chimpanzees, potential transmission is not excluded especially in Uganda where human oesophagostomosis has been reported [4]. Around Kibale, population density is high (up to 512 ind/km2) [13] and chimpanzees regularly crop-raid. Additionally recent findings confirmed that human-related diseases should be considered as a high threat for endangered apes [14]–[18]. As a consequence, it has been emphasized that investigations on potential cross-transmission should be reinforced. We report hereafter the results of our recent finding about nodular worm infection in wild chimpanzees (Pan troglodytes schweinfurthii) in the framework of a long-term health monitoring of the community of Kanyawara in Kibale National Park (Uganda). Methods Study site and study periods The studied chimpanzees (Pan troglodytes schweinfurthhii) belonged to one community in Kibale National Park (766 km2, 0°13′–0°41′N, 30°19′–30°32′E), located in Kanyawara area. This community counted 52 chimpanzees in 2006. Ages presented are those estimated in 2006. Their home range is close to the boundary of the Park and Kanyawara chimpanzees are sometimes entering plantations"
}