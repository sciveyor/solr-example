{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001026",
  "doi": "10.1371/journal.pntd.0001026",
  "externalIds": [
    "pii:PNTD-D-10-00137"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "First Report of Colonies of Sylvatic Triatoma infestans (Hemiptera: Reduviidae) in the Paraguayan Chaco, Using a Trained Dog",
  "authors": [
    {
      "name": "Miriam Rolón",
      "first": "Miriam",
      "last": "Rolón",
      "affiliation": "Centro para el Desarrollo de la Investigación Científica (CEDIC/Díaz Gill Medicina Laboratorial/Fundación Moisés Bertoni), Asunción, Paraguay"
    },
    {
      "name": "María Celeste Vega",
      "first": "María Celeste",
      "last": "Vega",
      "affiliation": "Centro para el Desarrollo de la Investigación Científica (CEDIC/Díaz Gill Medicina Laboratorial/Fundación Moisés Bertoni), Asunción, Paraguay"
    },
    {
      "name": "Fabiola Román",
      "first": "Fabiola",
      "last": "Román",
      "affiliation": "Centro para el Desarrollo de la Investigación Científica (CEDIC/Díaz Gill Medicina Laboratorial/Fundación Moisés Bertoni), Asunción, Paraguay"
    },
    {
      "name": "Ana Gómez",
      "first": "Ana",
      "last": "Gómez",
      "affiliation": "Centro para el Desarrollo de la Investigación Científica (CEDIC/Díaz Gill Medicina Laboratorial/Fundación Moisés Bertoni), Asunción, Paraguay"
    },
    {
      "name": "Antonieta Rojas de Arias",
      "first": "Antonieta",
      "last": "Rojas de Arias",
      "affiliation": "Centro para el Desarrollo de la Investigación Científica (CEDIC/Díaz Gill Medicina Laboratorial/Fundación Moisés Bertoni), Asunción, Paraguay"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-05",
  "dateAccepted": "2011-02-16",
  "dateReceived": "2010-10-29",
  "volume": "5",
  "number": "5",
  "pages": "e1026",
  "tags": [
    "Biodiversity",
    "Biology",
    "Community ecology",
    "Ecology",
    "Entomology",
    "Parasitology",
    "Species interactions",
    "Zoology"
  ],
  "abstract": "In the Gran Chaco region, control of Triatoma infestans has been limited by persistent domestic infestations despite the efforts of the Vector Control Services. In Paraguay, this region is the highest endemic area in the country, showing high levels of indoor and outdoor infestation. Although sylvatic T. infestans have been found in the Bolivian and Argentine Chaco, similar searches for sylvatic populations of this species in Paraguay had been unsuccessful over the last 20 years. Here we present a new approach to detecting sylvatic Triatominae, using a trained dog, which has successfully confirmed sylvatic populations of T. infestans and other triatomine species in Paraguay. A total of 22 specimens corresponding to dark morph forms of T. infestans were collected, and 14 were confirmed as T. infestans by the mitochondrial cytochrome B gene analysis. Through this analysis, one of which were previously reported and a second that was a new haplotype. Triatomines were captured from amongst vegetation such as dry branches and hollows trees of different species such Aspidosperma quebracho-blanco, Bulnesia sarmientoi and Stetsonia coryne. The colonies found have been small and without apparent infection with Trypanosoma cruzi. During the study, Triatoma sordida and Triatoma guasayana have also been found in ecotopes close to those of T. infestans.",
  "fullText": "Introduction Triatoma infestans (Hemiptera, Reduviidae) is the main vector of Chagas disease (American trypanosomiasis) in the Southern Cone of Latin America. Through the Southern Cone Initiative against Chagas disease, vectorial transmission to humans has been interrupted in Chile, Uruguay and Brazil, but Argentina and Paraguay have achieved this only in some regions [1]. In the Gran Chaco region, comprising parts of Argentina, Bolivia and Paraguay, control of the vectors has been limited due to the persistence of domestic infestations despite the efforts of the Vector Control Services in these countries [2], [3]. Studies conducted since the 1970s have shown high levels of indoor infestation of T. infestans in the Paraguayan Chaco, characterizing this region as the highest endemic area in the country [4]–[8]. However, sylvatic populations of this vector have only occasionally been reported in Paraguay [9] although nymphs of T. infestans were recently reported amongst vegetation near indigenous dwellings [10]. By contrast, sylvatic T. infestans have been more frequently reported from the Andean valleys of Cochabamba and La Paz in Bolivia, and also in the Bolivian Chaco [11]–[13] and the Argentine Chaco [9], [14]. The finding of dark morph (DM) T. infestans in parrot nests in Argentina [14], and the finding of extensive new foci of sylvatic triatomine populations in Bolivia [15] encouraged the intense search in the Paraguayan Chaco region, but the search for this species using light traps and manual checking of fallen trees and burrows had been unsuccessful. We report here a novel approach using a trained dog, which has revealed several sylvatic populations of T. infestans in the Paraguayan Chaco. Domestic dogs (Canis familiaris) are used by humans to locate a range of substances because of their superior olfactory acuity. Their area of olfactory epithelium (18 to 150 cm2) [16] is much greater than that of humans (3 cm2) [17]. They are widely used to detect non-biological (explosives, chemical contaminants, illegal drugs) and biological scents (human odours, animal scents) and have an important role in conservation [18]. Dogs have been trained for search and rescue of missing people [19], to search for brown tree snakes [20], insects that damage plants [21], birds [22], egg masses of gypsy moths [23], subterranean termites [24], screwworm-infested wounds [25], catfish off-flavour compounds [26], animal scat detection [27] and microbial organisms such as rot fungi, building moulds, and bacteria [28]. However, as far as we know, there are no previous attempts to train dogs to detect triatomine bugs. Triatominae produce volatile compounds, which seem to play a role in their defense and alarm processes, as well as in sexual communication and mating. The Brindley's glands, present in adult Triatominae, seem mainly to secrete isobutyric acid – believed to be involved in defense against predators [29], [30]. The metasternal glands, also present in adults, have been associated with sexual communication, and some highly volatile ketones (3-pentanone) and alcohols that are emitted by adults during mating have been identified [29], [30]. Moreover, the nymphs do not have Brindley's glands, metasternal glands, or"
}