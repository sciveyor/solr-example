{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000591",
  "doi": "10.1371/journal.pntd.0000591",
  "externalIds": [
    "pii:09-PNTD-VP-0496R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Rabies in the 21st Century",
  "authors": [
    {
      "name": "William H. Wunner",
      "first": "William H.",
      "last": "Wunner",
      "affiliation": "The Wistar Institute, Philadelphia, Pennsylvania, United States of America"
    },
    {
      "name": "Deborah J. Briggs",
      "first": "Deborah J.",
      "last": "Briggs",
      "affiliation": "Global Alliance for Rabies Control, Edinburgh, Scotland, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-03",
  "volume": "4",
  "number": "3",
  "pages": "e591",
  "tags": [
    "Immunology",
    "Immunology/Cellular Microbiology and Pathogenesis",
    "Immunology/Immunity to Infections",
    "Immunology/Innate Immunity",
    "Infectious Diseases",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Infectious Diseases of the Nervous System",
    "Virology",
    "Virology/Animal Models of Infection",
    "Virology/Antivirals, including Modes of Action and Resistance",
    "Virology/Diagnosis",
    "Virology/Effects of Virus Infection on Host Gene Expression",
    "Virology/Emerging Viral Diseases",
    "Virology/Host Antiviral Responses",
    "Virology/Immune Evasion"
  ],
  "fullText": "Why are 50,000–55,000 people dying from rabies worldwide each year, with 25,000–30,000 human deaths in India alone and over 3 billion people continuing to be at risk of rabies virus infection in over 100 countries in the 21st century? These are astonishing numbers, particularly as they represent individuals, a large proportion of whom are children, who have been attacked or are likely to be attacked by rabid dogs, the main source of rabies virus infection that, as yet, has not been brought under control in many parts of the world. The number of human deaths and the circumstances by which these deaths continue to occur are extraordinary, with over 95% of rabies victims reported residing in Asia and Africa and nearly all victims of a rabid dog bite. Rabies has been part of the history of civilization for several millennia, rooted in its enzootic environment (animal host) and causing severe threats to public health across continents. Rabies and the symptoms it presents can hardly be ignored, yet it appears to be unduly neglected in some parts of the world, notably in Asia and Africa, where the spread of canine rabies is not under control and is far from being eliminated. In other parts of the world, largely in developed countries, where elimination of canine rabies has been achieved, there are models to be followed and lessons learned that will challenge epidemiologists and molecular virologists alike in the future as they apply new techniques to achieve the elimination of canine and human rabies worldwide. Through the World Rabies Day (WRD) initiative (www.worldrabiesday.org), over 55 million people have received educational material about rabies prevention. Today, people from more than 85 countries are involved on all levels of society (government, medical and veterinary professionals, media, educators, and lay people), ready to take some action toward elimination of endemic rabies worldwide. Despite the many languages and different cultures involved and so little money to work with, the empowerment of people around the world to do something for their own communities and countries is what has made the WRD initiative successful. Educational materials have been created that are easily translated into different languages and distributed through electronic media, and with these materials people are becoming better educated about rabies prevention. People are learning that going to local healers for treatments that do not work, such as rubbing chili powder in wounds, incantations, and taking ineffective herbal medicines, is not the way to prevent rabies. Instead, people learn from the educational materials that the risk of exposure to rabies can be minimized and the disease can be prevented by using the right methods and treatments, and together these measures can make a difference in their own lives. In several recent PLoS Neglected Tropical Diseases papers on rabies (2009–2010)—marking the third anniversary of World Rabies Day—scientists describe the situation of canine rabies control in developing countries, as well as various recent advances in the development of vaccines and treatments for rabies that will contribute to the elimination"
}