{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001701",
  "doi": "10.1371/journal.pntd.0001701",
  "externalIds": [
    "pii:PNTD-D-11-01119"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Effects of Cu/Zn Superoxide Dismutase (sod1) Genotype and Genetic Background on Growth, Reproduction and Defense in Biomphalaria glabrata",
  "authors": [
    {
      "name": "Kaitlin M. Bonner",
      "first": "Kaitlin M.",
      "last": "Bonner",
      "affiliation": "Department of Zoology, Oregon State University, Corvallis, Oregon, United States of America"
    },
    {
      "name": "Christopher J. Bayne",
      "first": "Christopher J.",
      "last": "Bayne"
    },
    {
      "name": "Maureen K. Larson",
      "first": "Maureen K.",
      "last": "Larson"
    },
    {
      "name": "Michael S. Blouin",
      "first": "Michael S.",
      "last": "Blouin"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-06",
  "dateAccepted": "2012-05-03",
  "dateReceived": "2011-11-09",
  "volume": "6",
  "number": "6",
  "pages": "e1701",
  "tags": [
    "Biology",
    "Evolutionary Biology",
    "Evolutionary biology",
    "Evolutionary processes",
    "Evolutionary selection",
    "Genetic determinism",
    "Genetics",
    "Genetics and Genomics",
    "Genetics of disease",
    "Heredity",
    "Trait locus"
  ],
  "abstract": "Resistance of the snail Biomphalaria glabrata to the trematode Schistosoma mansoni is correlated with allelic variation at copper-zinc superoxide dismutase (sod1). We tested whether there is a fitness cost associated with carrying the most resistant allele in three outbred laboratory populations of snails. These three populations were derived from the same base population, but differed in average resistance. Under controlled laboratory conditions we found no cost of carrying the most resistant allele in terms of fecundity, and a possible advantage in terms of growth and mortality. These results suggest that it might be possible to drive resistant alleles of sod1 into natural populations of the snail vector for the purpose of controlling transmission of S. mansoni. However, we did observe a strong effect of genetic background on the association between sod1 genotype and resistance. sod1 genotype explained substantial variance in resistance among individuals in the most resistant genetic background, but had little effect in the least resistant genetic background. Thus, epistatic interactions with other loci may be as important a consideration as costs of resistance in the use of sod1 for vector manipulation.",
  "fullText": "Introduction Although vector-borne diseases account for approximately one-sixth of the global human disease burden [1], [2], we still lack effective drugs and vaccines for many of these diseases. Even when effective drugs are available, high-risk populations often cannot be adequately treated due to a lack of funding and infrastructure in the heavily impacted countries [1], [3]. Therefore, in the absence of vaccines, eradication efforts that include both drug therapy and vector control can be the most effective approach [4]. Vector control methods most often utilize chemicals for eradication [1], [4]. This approach has obvious drawbacks because it results in habitat degradation and risk of human exposure to pesticides. Also, recurrent pesticide application is often necessary because it is nearly impossible, with a single treatment, to completely remove all possible vector individuals from an epidemiologically relevant site [5]. Recent advances in understanding the genetics of host-parasite interactions have led to increased interest in driving resistance genes into susceptible vector populations [6]–[11]. In this context, the term “resistance” describes a continuously varying trait we define as the probability of becoming infected after being challenged by a parasite, rather than to mean the absolute inability to become infected (i.e. a population or genotype can have high or low average resistance). Making vector populations more resistant to infection could be a better long-term solution and an ecologically safer way of breaking transmission cycles. Unfortunately, this approach faces major population-genetic hurdles. A non-exhaustive list includes: (1) genotype-by-environment (GxE), where the performance of a gene or gene(s) of interest depends on environmental conditions such that interactions can affect how a resistance gene performs in the field versus in the lab [12]–[16], (2) parasites and hosts are genetically more variable in the field, and there can be interactions between host genotypes and parasite genotypes (genotype-by-genotype (GxG) interactions; [16]–[19]), (3) genetic background can influence how a resistance gene performs in a natural versus a lab population. In other words, the gene of interest may perform differently depending on the genomic context in which it is interacting (epistasis), and (4), there may be a cost of resistance such that natural selection in the absence of parasites favors the “wild-type” alleles that we wish to replace. Cost of resistance may be a particularly vexing problem for resistance-gene introduction programs. Such costs have been demonstrated in many host-parasite systems (reviewed in [20]–[26]). Nevertheless, costs of resistance are not universal [8], [27]–[31], and they may be context dependent (e.g. revealed only in stressful environments; [12], [32]–[36]). Costs of resistance presumably involve a reallocation of metabolic resources between one or more of the following life-history components: reproduction, growth, and somatic maintenance/immune function [24], [26], [37], [38]. Also, the severity of the cost should depend on the particular mechanism of resistance [29], [39]. For example, it was predicted that mechanisms involving over-expression of particular genes might be among the most costly [39]. This study was designed to measure costs of resistance and epistatic effects of genetic background associated with a single locus in Biomphalaria"
}