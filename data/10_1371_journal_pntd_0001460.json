{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001460",
  "doi": "10.1371/journal.pntd.0001460",
  "externalIds": [
    "pii:PNTD-D-11-00785"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Sero-Epidemiology as a Tool to Screen Populations for Exposure to Mycobacterium ulcerans",
  "authors": [
    {
      "name": "Dorothy Yeboah-Manu",
      "first": "Dorothy",
      "last": "Yeboah-Manu",
      "affiliation": "Noguchi Memorial Institute for Medical Research, University of Ghana, Legon, Ghana"
    },
    {
      "name": "Katharina Röltgen",
      "first": "Katharina",
      "last": "Röltgen",
      "affiliation": "Molecular Immunology, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "William Opare",
      "first": "William",
      "last": "Opare",
      "affiliation": "National Buruli Ulcer Control Programme, Disease Control Unit - GHS, Accra, Ghana"
    },
    {
      "name": "Kobina Asan-Ampah",
      "first": "Kobina",
      "last": "Asan-Ampah",
      "affiliation": "Noguchi Memorial Institute for Medical Research, University of Ghana, Legon, Ghana"
    },
    {
      "name": "Kwabena Quenin-Fosu",
      "first": "Kwabena",
      "last": "Quenin-Fosu",
      "affiliation": "Noguchi Memorial Institute for Medical Research, University of Ghana, Legon, Ghana"
    },
    {
      "name": "Adwoa Asante-Poku",
      "first": "Adwoa",
      "last": "Asante-Poku",
      "affiliation": "Noguchi Memorial Institute for Medical Research, University of Ghana, Legon, Ghana"
    },
    {
      "name": "Edwin Ampadu",
      "first": "Edwin",
      "last": "Ampadu",
      "affiliation": "National Buruli Ulcer Control Programme, Disease Control Unit - GHS, Accra, Ghana"
    },
    {
      "name": "Janet Fyfe",
      "first": "Janet",
      "last": "Fyfe",
      "affiliation": "Victorian Infectious Diseases Reference Laboratory, North Melbourne, Victoria, Australia"
    },
    {
      "name": "Kwadwo Koram",
      "first": "Kwadwo",
      "last": "Koram",
      "affiliation": "Noguchi Memorial Institute for Medical Research, University of Ghana, Legon, Ghana"
    },
    {
      "name": "Collins Ahorlu",
      "first": "Collins",
      "last": "Ahorlu",
      "affiliation": "Noguchi Memorial Institute for Medical Research, University of Ghana, Legon, Ghana"
    },
    {
      "name": "Gerd Pluschke",
      "first": "Gerd",
      "last": "Pluschke",
      "affiliation": "Molecular Immunology, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-01",
  "dateAccepted": "2011-11-21",
  "dateReceived": "2011-08-08",
  "volume": "6",
  "number": "1",
  "pages": "e1460",
  "tags": [
    "Biology",
    "Immunology",
    "Microbiology",
    "Population biology"
  ],
  "abstract": "Background\n          \nPrevious analyses of sera from a limited number of Ghanaian Buruli ulcer (BU) patients, their household contacts, individuals living in BU non-endemic regions as well as European controls have indicated that antibody responses to the M. ulcerans 18 kDa small heat shock protein (shsp) reflect exposure to this pathogen. Here, we have investigated to what extent inhabitants of regions in Ghana regarded as non-endemic for BU develop anti-18 kDa shsp antibody titers.\n\n          Methodology/Principal Findings\n          \nFor this purpose we determined anti-18 kDa shsp IgG titers in sera collected from healthy inhabitants of the BU endemic Densu River Valley and the Volta Region, which was so far regarded as BU non-endemic. Significantly more sera from the Densu River Valley contained anti-18 kDa shsp IgG (32% versus 12%, respectively). However, some sera from the Volta Region also showed high titers. When interviewing these sero-responders, it was revealed that the person with the highest titer had a chronic wound, which was clinically diagnosed and laboratory reconfirmed as active BU. After identification of this BU index case, further BU cases were clinically diagnosed by the Volta Region local health authorities and laboratory reconfirmed. Interestingly, there was neither a difference in sero-prevalence nor in IS2404 PCR positivity of environmental samples between BU endemic and non-endemic communities located in the Densu River Valley.\n\n          Conclusions\n          \nThese data indicate that the intensity of exposure to M. ulcerans in endemic and non-endemic communities along the Densu River is comparable and that currently unknown host and/or pathogen factors may determine how frequently exposure is leading to clinical disease. While even high serum titers of anti-18 kDa shsp IgG do not indicate active disease, sero-epidemiological studies can be used to identify new BU endemic areas.",
  "fullText": "Introduction Buruli ulcer (BU), a severe necrotizing skin disease, is caused by the environmental pathogen Mycobacterium ulcerans (M. ulcerans). Globally, it is the third most prevalent mycobacterial disease that affects immunocompetent individuals after tuberculosis and leprosy [1]. Currently more than 30 countries, mainly in the Tropics and sub-Tropics, are known to report BU cases [2]. The main countries that are severely affected lie along the Gulf of Guinea and include Ivory-Coast, Ghana, Togo, Benin and Cameroon. In the highly endemic countries BU is second after tuberculosis as the most prevalent mycobacterial disease [2], [3]. However, the global burden of BU is not clear, because efficient and comprehensive reporting systems are lacking in many of the BU endemic countries. One characteristic of BU is its focal distribution within highly endemic countries. Most cases occur in remote villages with limited access to the formal health sector, prompting affected people to seek health at traditional healers [4]. Even today, not all affected communities may be known to the National BU Control Programs. Therefore reliable tools to detect and monitor the presence of BU in communities are urgently needed. The disease presentation, which varies between individuals, starts either as a papule, nodule, plaque or edema and if these non-ulcerative early forms are not treated, extensive tissue destruction leads to the formation of large ulcerative lesions with characteristic undermined borders. Extensive tissue destruction frequently causes disfigurement and long lasting deformities such as loss of limbs and essential organs, like the eye [5], [6]. Many features of BU such as the mode of M. ulcerans transmission and risk factors for an infection with the pathogen are not clearly understood. However, BU is known to occur mainly in children less than 15 years of age and affects people in wetlands and disturbed environments [3], [7]. The pathology of BU is primarily associated with the secretion of the cytocidal and immunosuppressive polyketide toxin mycolactone [8]. Current methods for a laboratory confirmation of clinical BU diagnosis include microscopic detection of acid fast bacilli (AFB), culture of M. ulcerans, histopathology and detection of M. ulcerans DNA by PCR. Currently, PCR detection of the M. ulcerans specific insertion sequence IS2404 is the gold standard for BU diagnosis [9]. Yet, PCR requires elaborate infrastructure and expertise and therefore make it out of reach for primary health care facilities in BU endemic low resource countries. Serology represents a more attractive approach for the development of a simple test format that can be applied to facilities treating BU in low resourced countries. Unfortunately, various studies have shown that serological tests targeting M. ulcerans antigens are not suitable to differentiate between patients and exposed but healthy individuals as both groups may exhibit serum IgG titers against these antigens [10], [11]. However, serology may be a useful tool for monitoring exposure of populations to M. ulcerans, although great antigenic cross reactivity between M. ulcerans, M. tuberculosis, BCG and other environmental mycobacteria complicates this approach. We previously profiled an immunodominant 18 kDa small heat shock protein (shsp)"
}