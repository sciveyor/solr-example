{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001466",
  "doi": "10.1371/journal.pntd.0001466",
  "externalIds": [
    "pii:PNTD-D-11-00161"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Orientia tsutsugamushi in Human Scrub Typhus Eschars Shows Tropism for Dendritic Cells and Monocytes Rather than Endothelium",
  "authors": [
    {
      "name": "Daniel H. Paris",
      "first": "Daniel H.",
      "last": "Paris",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Programme, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand; Center for Tropical Medicine, Nuffield Department of Clinical Medicine, Churchill Hospital, Headington, Oxford, United Kingdom"
    },
    {
      "name": "Rattanaphone Phetsouvanh",
      "first": "Rattanaphone",
      "last": "Phetsouvanh",
      "affiliation": "Wellcome Trust-Mahosot Hospital-Oxford Tropical Medicine Research Collaboration, Mahosot Hospital, Vientiane, Lao People's Democratic Republic"
    },
    {
      "name": "Ampai Tanganuchitcharnchai",
      "first": "Ampai",
      "last": "Tanganuchitcharnchai",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Programme, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Margaret Jones",
      "first": "Margaret",
      "last": "Jones",
      "affiliation": "Nuffield Department of Clinical Laboratory Sciences, Oxford University, Oxford, United Kingdom"
    },
    {
      "name": "Kemajittra Jenjaroen",
      "first": "Kemajittra",
      "last": "Jenjaroen",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Programme, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand"
    },
    {
      "name": "Manivanh Vongsouvath",
      "first": "Manivanh",
      "last": "Vongsouvath",
      "affiliation": "Wellcome Trust-Mahosot Hospital-Oxford Tropical Medicine Research Collaboration, Mahosot Hospital, Vientiane, Lao People's Democratic Republic"
    },
    {
      "name": "David P. J. Ferguson",
      "first": "David P. J.",
      "last": "Ferguson",
      "affiliation": "Nuffield Department of Clinical Laboratory Sciences, Oxford University, Oxford, United Kingdom"
    },
    {
      "name": "Stuart D. Blacksell",
      "first": "Stuart D.",
      "last": "Blacksell",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Programme, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand; Center for Tropical Medicine, Nuffield Department of Clinical Medicine, Churchill Hospital, Headington, Oxford, United Kingdom"
    },
    {
      "name": "Paul N. Newton",
      "first": "Paul N.",
      "last": "Newton",
      "affiliation": "Center for Tropical Medicine, Nuffield Department of Clinical Medicine, Churchill Hospital, Headington, Oxford, United Kingdom; Wellcome Trust-Mahosot Hospital-Oxford Tropical Medicine Research Collaboration, Mahosot Hospital, Vientiane, Lao People's Democratic Republic"
    },
    {
      "name": "Nicholas P. J. Day",
      "first": "Nicholas P. J.",
      "last": "Day",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Programme, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand; Center for Tropical Medicine, Nuffield Department of Clinical Medicine, Churchill Hospital, Headington, Oxford, United Kingdom"
    },
    {
      "name": "Gareth D. H. Turner",
      "first": "Gareth D. H.",
      "last": "Turner",
      "affiliation": "Mahidol-Oxford Tropical Medicine Research Programme, Faculty of Tropical Medicine, Mahidol University, Bangkok, Thailand; Center for Tropical Medicine, Nuffield Department of Clinical Medicine, Churchill Hospital, Headington, Oxford, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-01",
  "dateAccepted": "2011-11-23",
  "dateReceived": "2011-02-19",
  "volume": "6",
  "number": "1",
  "pages": "e1466",
  "tags": [
    "Clinical immunology",
    "Dermatology",
    "Immunology",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine"
  ],
  "abstract": "Scrub typhus is a common and underdiagnosed cause of febrile illness in Southeast Asia, caused by infection with Orientia tsutsugamushi. Inoculation of the organism at a cutaneous mite bite site commonly results in formation of a localized pathological skin reaction termed an eschar. The site of development of the obligate intracellular bacteria within the eschar and the mechanisms of dissemination to cause systemic infection are unclear. Previous postmortem and in vitro reports demonstrated infection of endothelial cells, but recent pathophysiological investigations of typhus patients using surrogate markers of endothelial cell and leucocyte activation indicated a more prevalent host leucocyte than endothelial cell response in vivo. We therefore examined eschar skin biopsies from patients with scrub typhus to determine and characterize the phenotypes of host cells in vivo with intracellular infection by O. tsutsugamushi, using histology, immunohistochemistry, double immunofluorescence confocal laser scanning microscopy and electron microscopy. Immunophenotyping of host leucocytes infected with O. tsutsugamushi showed a tropism for host monocytes and dendritic cells, which were spatially related to different histological zones of the eschar. Infected leucocyte subsets were characterized by expression of HLADR+, with an “inflammatory” monocyte phenotype of CD14/LSP-1/CD68 positive or dendritic cell phenotype of CD1a/DCSIGN/S100/FXIIIa and CD163 positive staining, or occasional CD3 positive T-cells. Endothelial cell infection was rare, and histology did not indicate a widespread inflammatory vasculitis as the cause of the eschar. Infection of dendritic cells and activated inflammatory monocytes offers a potential route for dissemination of O. tsutsugamushi from the initial eschar site. This newly described cellular tropism for O. tsutsugamushi may influence its interaction with local host immune responses.",
  "fullText": "Introduction Scrub typhus is a common and neglected disease caused by Orientia tsutsugamushi in Southeast Asia, accounting for up to 28% of non-malarial fevers in a prospective fever study in Lao PDR [1], and causing an estimated 1 million clinical cases worldwide every year [2]. Recognition of the disease is difficult, due to its overlapping clinical spectrum with other common causes of fever in this population [3] and due to limitations of current diagnostic methods [4]. As the early diagnosis of typhus disease is key to directing appropriate therapy, understanding how the organism develops, disseminates within the host, and interacts with the cells of the host immune response is important. The proportion of acute primary infections with development of an eschar at the site of the chigger bite can vary widely with geographical areas, exposure of the population and level of endemicity of scrub typhus. In eastern Taiwan the percentage of eschars observed has been reported at 23% [5], in Japan a study observed 97% [6], and in Thai children from a highly endemic area this was 7% [7]. Accompanying lymphadenopathy is very common in patients with scrub typhus [1], [8], which was also observed in volunteers infected with laboratory-reared chiggers [9]. Fever, rash and non-specific symptoms are common, but the clinical course can be complicated by meningo-encephalitis, a disseminated intravascular coagulation (DIC)-like syndrome or severe pneumonitis, which may culminate in acute respiratory distress syndrome (ARDS) and death [10]–[12]. Definitive evidience of endothelial tropism of O. tsutsugamushi requires immunophenotyping and ultrastructural co-localisation, which is available in one report of human post-mortem autopsies [13]. Further data in the literature refers to histopathological reports from patient eschar biopsies and animal studies. Histopathological studies of eschars in humans [14]–[18] and cynomolgus monkeys [19] have described perivascular collections of mononuclear cells, including lymphocytes, plasma cells and macrophages. In recent studies, immunophenotyping revealed a dominance of CD3+ T-cells and CD68+ monocyte/macrophages within infiltrates [15] and an association of O. tsutsugamushi with the epithelial lining covering the surface of the sweat ducts and glands [14]. Eschars can have high bacterial loads and have been shown to be useful specimens for both PCR-based and immunohistochemical diagnosis [14], [20]. Parallel studies of human ex vivo eschar biopsies in the spotted fever group (SFG) rickettsioses show evidence for infection of endothelium and surrounding leucocytes, with a predominance of neutrophils in the infiltrates in cases of R. africae [16], R. rickettsii [17] and R. conorii [18] although lacking ultrastructural demonstration. In summary, the literature provides limited evidence for the in vivo cellular tropism of O. tsutsugamushi in humans, due to a lack of co-localization studies allowing phenotypic characterization of the infected leucocyte subsets. The eschar offers an accessible model for studying O. tsutsugamushi interactions during early infection of the human host, including colocalization studies for phenotyping of infected cells to elucidate the immune mechanisms involved in control or dissemination of O. tsutsugamushi from the local inoculation site. The study was designed to examine the cellular tropism of O. tsutsugamushi in"
}