{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001243",
  "doi": "10.1371/journal.pntd.0001243",
  "externalIds": [
    "pii:PNTD-D-11-00091"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Effects of Irritant Chemicals on Aedes aegypti Resting Behavior: Is There a Simple Shift to Untreated “Safe Sites”?",
  "authors": [
    {
      "name": "Hortance Manda",
      "first": "Hortance",
      "last": "Manda",
      "affiliation": "Department of Preventive Medicine and Biometrics, Uniformed Services University of the Health Sciences, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Luana M. Arce",
      "first": "Luana M.",
      "last": "Arce",
      "affiliation": "Department of Preventive Medicine and Biometrics, Uniformed Services University of the Health Sciences, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Tarra Foggie",
      "first": "Tarra",
      "last": "Foggie",
      "affiliation": "Department of Preventive Medicine and Biometrics, Uniformed Services University of the Health Sciences, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Pankhil Shah",
      "first": "Pankhil",
      "last": "Shah",
      "affiliation": "Department of Preventive Medicine and Biometrics, Uniformed Services University of the Health Sciences, Bethesda, Maryland, United States of America"
    },
    {
      "name": "John P. Grieco",
      "first": "John P.",
      "last": "Grieco",
      "affiliation": "Department of Preventive Medicine and Biometrics, Uniformed Services University of the Health Sciences, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Nicole L. Achee",
      "first": "Nicole L.",
      "last": "Achee",
      "affiliation": "Department of Preventive Medicine and Biometrics, Uniformed Services University of the Health Sciences, Bethesda, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-07",
  "dateAccepted": "2011-06-03",
  "dateReceived": "2011-02-06",
  "volume": "5",
  "number": "7",
  "pages": "e1243",
  "tags": [
    "Biology",
    "Entomology",
    "Microbiology",
    "Vector biology",
    "Viral vectors",
    "Zoology"
  ],
  "abstract": "Background\n          \nPrevious studies have identified the behavioral responses of Aedes aegypti to irritant and repellent chemicals that can be exploited to reduce man-vector contact. Maximum efficacy of interventions based on irritant chemical actions will, however, require full knowledge of variables that influence vector resting behavior and how untreated “safe sites” contribute to overall impact.\n\n          Methods\n          \nUsing a laboratory box assay, resting patterns of two population strains of female Ae. aegypti (THAI and PERU) were evaluated against two material types (cotton and polyester) at various dark:light surface area coverage (SAC) ratio and contrast configuration (horizontal and vertical) under chemical-free and treated conditions. Chemicals evaluated were alphacypermethrin and DDT at varying concentrations.\n\n          Results\n          \nUnder chemical-free conditions, dark material had significantly higher resting counts compared to light material at all SAC, and significantly increased when material was in horizontal configuration. Cotton elicited stronger response than polyester. Within the treatment assays, significantly higher resting counts were observed on chemical-treated dark material compared to untreated light fabric. However, compared to matched controls, significantly less resting observations were made on chemical-treated dark material overall. Most importantly, resting observations on untreated light material (or “safe sites”) in the treatment assay did not significantly increase for many of the tests, even at 25% SAC. Knockdown rates were ≤5% for all assays. Significantly more observations of flying mosquitoes were made in test assays under chemical-treatment conditions as compared to controls.\n\n          Conclusions/Significance\n          \nWhen preferred Ae. aegypti resting sites are treated with chemicals, even at reduced treatment coverage area, mosquitoes do not simply move to safe sites (untreated areas) following contact with the treated material. Instead, they become agitated, using increased flight as a proxy indicator. It is this contact irritant response that may elicit escape behavior from a treated space and is a focus of exploitation for reducing man-vector contact inside homes.",
  "fullText": "Introduction Dengue, primarily transmitted by Aedes aegypti (L.) (Diptera: Culicidae), is presently the most important mosquito-borne viral disease in the world with over 100 countries endemic, mostly in the tropics and subtropics [1], and an estimated 2.5 billion people at risk of infection. There is no vaccine against dengue and there are no drugs to treat dengue hemorrhagic fever and dengue shock syndrome. Hence, vector control remains the cornerstone for the prevention and control of dengue transmission [2]. Patterns of dengue virus transmission are influenced by the abundance, survival, and behavior of the principal mosquito vector, Ae. aegypti. Two main emphases for Ae. aegypti control exist: (1) reduction of the larval stage through environmental management (source reduction), larvicides and biological control; and (2) reduction of the adult stage using fumigation and/or residual spray of insecticides. Since the early 1900s [3], [4], it has been known that the most cost-effective means of preventing mosquito-borne disease is to target the adult vector, which transmit the pathogen. However, the prevailing paradigm for suppressing Ae. aegypti targets immature mosquitoes, the vast majority of which will not survive long enough to transmit virus [5]. For emergency interventions during dengue outbreaks, targeting the adult vector population by outdoor ultra-low-volume (ULV) application of insecticides and/or indoor thermal fogging remain the methods of choice [6], [7]. However, most control interventions that apply adulticides by space-spraying achieve relatively low effectiveness [8]–[13]. One reason for this reduced effectiveness can be attributed to vector behavior. Aedes aegypti is extensively adapted to exploit the human environment. The female almost exclusively takes blood from humans [14] and most commonly feeds and rests indoors. This species will also lay eggs in available oviposition and larval developmental sites inside the home [15]. This extensive use of the human indoor environment poses unique challenges to traditional adult control methods since chemical applied through outdoor and peridomestic ULV methods must pass through house portals to reach the interior space where the vector can make contact with the insecticide. This approach results in the loss of some chemical prior to reaching the interior space. Control in buildings usually accomplished with indoor residual or space spray are often hampered by limited access into homes and resource limitations [5]. On the other hand, Ae. aegypti's high affinity for the human indoor environment also provides opportunities for innovative approaches to control the adult vector [5]. Aedes aegypti has been characterized as having specific resting preferences based on visual cues (i.e., dark colors) [16], [17], and to be significantly attracted by black [18], yellow, orange and red colors [19]. Studies that have exploited Ae. aegypti's attraction to color contrast (i.e. simultaneous presentation of two colors, one which mosquitoes are attracted to in order to direct them to a target) have led to the development of host-seeking adult traps such as the Fay-Prince [20], counterflow geometry trap [21], and the BG Sentinel™ trap [22]. Previous studies in Thailand [23] demonstrated the utility of exploiting the resting preference of Ae. aegypti to develop attractant"
}