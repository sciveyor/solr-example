{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000970",
  "doi": "10.1371/journal.pntd.0000970",
  "externalIds": [
    "pii:10-PNTD-RA-1519R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A History of Chagas Disease Transmission, Control, and Re-Emergence in Peri-Rural La Joya, Peru",
  "authors": [
    {
      "name": "Stephen Delgado",
      "first": "Stephen",
      "last": "Delgado",
      "affiliation": "School of Geography and Development, University of Arizona, Tucson, Arizona, United States of America; Division of Epidemiology and Biostatistics, University of Arizona, Tucson, Arizona, United States of America"
    },
    {
      "name": "Ricardo Castillo Neyra",
      "first": "Ricardo",
      "last": "Castillo Neyra",
      "affiliation": "Bloomberg School of Public Health, Johns Hopkins University, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Víctor R. Quispe Machaca",
      "first": "Víctor R.",
      "last": "Quispe Machaca",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Jenny Ancca Juárez",
      "first": "Jenny",
      "last": "Ancca Juárez",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Lily Chou Chu",
      "first": "Lily",
      "last": "Chou Chu",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Manuela Renee Verastegui",
      "first": "Manuela Renee",
      "last": "Verastegui",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Giovanna M. Moscoso Apaza",
      "first": "Giovanna M.",
      "last": "Moscoso Apaza",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "César D. Bocángel",
      "first": "César D.",
      "last": "Bocángel",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Aaron W. Tustin",
      "first": "Aaron W.",
      "last": "Tustin",
      "affiliation": "Vanderbilt University School of Medicine, Nashville, Tennessee, United States of America"
    },
    {
      "name": "Charles R. Sterling",
      "first": "Charles R.",
      "last": "Sterling",
      "affiliation": "Department of Veterinary Science and Microbiology, University of Arizona, Tucson, Arizona, United States of America"
    },
    {
      "name": "Andrew C. Comrie",
      "first": "Andrew C.",
      "last": "Comrie",
      "affiliation": "School of Geography and Development, University of Arizona, Tucson, Arizona, United States of America"
    },
    {
      "name": "César Náquira",
      "first": "César",
      "last": "Náquira",
      "affiliation": "Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Juan G. Cornejo del Carpio",
      "first": "Juan G.",
      "last": "Cornejo del Carpio",
      "affiliation": "Gerencia Regional de Salud de Arequipa, Arequipa, Peru"
    },
    {
      "name": "Robert H. Gilman",
      "first": "Robert H.",
      "last": "Gilman",
      "affiliation": "Bloomberg School of Public Health, Johns Hopkins University, Baltimore, Maryland, United States of America; Urbanización Ingeniería, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Caryn Bern",
      "first": "Caryn",
      "last": "Bern",
      "affiliation": "Division of Parasitic Diseases and Malaria, Center for Global Health, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Michael Z. Levy",
      "first": "Michael Z.",
      "last": "Levy",
      "affiliation": "Department of Biostatistics and Epidemiology, University of Pennsylvania, Philadelphia, Pennsylvania, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-02",
  "dateAccepted": "2011-01-21",
  "dateReceived": "2010-09-20",
  "volume": "5",
  "number": "2",
  "pages": "e970",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Background\n\nThe history of Chagas disease control in Peru and many other nations is marked by scattered and poorly documented vector control campaigns. The complexities of human migration and sporadic control campaigns complicate evaluation of the burden of Chagas disease and dynamics of Trypanosoma cruzi transmission.\n\nMethodology/Principal Findings\n\nWe conducted a cross-sectional serological and entomological study to evaluate temporal and spatial patterns of T. cruzi transmission in a peri-rural region of La Joya, Peru. We use a multivariate catalytic model and Bayesian methods to estimate incidence of infection over time and thereby elucidate the complex history of transmission in the area. Of 1,333 study participants, 101 (7.6%; 95% CI: 6.2–9.0%) were confirmed T. cruzi seropositive. Spatial clustering of parasitic infection was found in vector insects, but not in human cases. Expanded catalytic models suggest that transmission was interrupted in the study area in 1996 (95% credible interval: 1991–2000), with a resultant decline in the average annual incidence of infection from 0.9% (95% credible interval: 0.6–1.3%) to 0.1% (95% credible interval: 0.005–0.3%). Through a search of archival newspaper reports, we uncovered documentation of a 1995 vector control campaign, and thereby independently validated the model estimates.\n\nConclusions/Significance\n\nHigh levels of T. cruzi transmission had been ongoing in peri-rural La Joya prior to interruption of parasite transmission through a little-documented vector control campaign in 1995. Despite the efficacy of the 1995 control campaign, T. cruzi was rapidly reemerging in vector populations in La Joya, emphasizing the need for continuing surveillance and control at the rural-urban interface.",
  "fullText": "Introduction An estimated 8 million people in Latin America are infected by the protozoan parasite Trypanosoma cruzi, the causative agent of Chagas disease [1]. Trypanosoma cruzi is typically transmitted to humans and other mammals through contact with feces of an infected blood-feeding triatomine insect. The primary vector species in southern Peru is Triatoma infestans, which has adapted to live in and around human dwellings. Infection can also occur via congenital transmission, blood transfusion, or organ transplantation [2]. Infection is generally life-long and while most infected individuals remain asymptomatic, 20–30% progress over a period of decades to chronic clinical manifestations, including life-threatening cardiac and/or gastrointestinal disease [3]. As a result, Chagas disease is estimated to be responsible for the loss of 670,000 disability-adjusted life years (DALYs) and 14,000 human lives annually [4]. Trypanosoma cruzi transmission by T. infestans has been interrupted in several South American countries through household application of pyrethroid insecticides, but a comprehensive approach to vector control has only recently been instituted in southern Peru [1], [5]. Throughout Latin America, however, Chagas disease vector control is complicated by the processes of urbanization and migration [6], [7]. In recent decades in southern Peru, extensive urbanization has occurred at the periphery of cities as well as within previously rural areas [8]. New localities are typically established by rural migrants and share the trait of being situated – geographically as well as socio-culturally – at a rural-urban interface [9]. To improve understanding of T. cruzi transmission in the peri-rural context, we performed cross-sectional serological and entomological surveys in four contiguous localities located 30 km from the city of Arequipa. We evaluated spatial and temporal patterns of T. cruzi infection, utilizing a multivariate catalytic model [10] and Bayesian methods to estimate incidence of infection over time. Methods Ethics statement The ethical review committees of the Johns Hopkins Bloomberg School of Public Health, the Universidad Peruana Cayetano Heredia, and the University of Pennsylvania approved the research protocol. The ethical review committee of the University of Arizona approved the usage of de-identified study data. All individuals ≥1 year old residing within the study area were invited to participate in the serological study. Signed informed consent was obtained prior to participation by adults and parents of participating children. Children also provided signed informed assent prior to participating. All households in the study area were invited to participate in the entomological study. Signed informed consent was obtained prior to participation by an adult resident of each household. Study area and population The district of La Joya (population 24,192) is located approximately 30 km southwest of the city of Arequipa (population 864,250) and encompasses a mosaic of rural and peri-rural communities [8]. In this article, peri-rural refers to communities with high-density human habitation within an otherwise rural landscape, whereas peri-urban describes localities with high-density human habitation located at the periphery of an urban center [9]. The La Joya study area (Figure 1) was comprised of four contiguous peri-rural localities, with 2,251 persons living in 678 households within a"
}