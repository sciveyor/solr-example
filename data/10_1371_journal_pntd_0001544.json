{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001544",
  "doi": "10.1371/journal.pntd.0001544",
  "externalIds": [
    "pii:PNTD-D-11-00895"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Poor-Quality Generic Drug for the Treatment of Visceral Leishmaniasis: A Case Report and Appeal",
  "authors": [
    {
      "name": "Thomas P. C. Dorlo",
      "first": "Thomas P. C.",
      "last": "Dorlo",
      "affiliation": "Division of Infectious Diseases, Academic Medical Center, University of Amsterdam, Amsterdam, The Netherlands; Department of Pharmacy & Pharmacology, Slotervaart Hospital / the Netherlands Cancer Institute, Amsterdam, The Netherlands"
    },
    {
      "name": "Teunis A. Eggelte",
      "first": "Teunis A.",
      "last": "Eggelte",
      "affiliation": "Division of Infectious Diseases, Academic Medical Center, University of Amsterdam, Amsterdam, The Netherlands"
    },
    {
      "name": "Gerard J. Schoone",
      "first": "Gerard J.",
      "last": "Schoone",
      "affiliation": "Department of Parasitology, KIT Biomedical Research, Amsterdam, The Netherlands"
    },
    {
      "name": "Peter J. de Vries",
      "first": "Peter J.",
      "last": "de Vries",
      "affiliation": "Division of Infectious Diseases, Academic Medical Center, University of Amsterdam, Amsterdam, The Netherlands"
    },
    {
      "name": "Jos H. Beijnen",
      "first": "Jos H.",
      "last": "Beijnen",
      "affiliation": "Department of Pharmacy & Pharmacology, Slotervaart Hospital / the Netherlands Cancer Institute, Amsterdam, The Netherlands"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-05",
  "volume": "6",
  "number": "5",
  "pages": "e1544",
  "tags": [
    "Drug licensing and regulation",
    "Drug research and development",
    "Drugs and devices",
    "Infectious Diseases",
    "Infectious diseases",
    "Leishmaniasis",
    "Medicine",
    "Neglected tropical diseases",
    "Parasitic diseases",
    "Pharmacology"
  ],
  "fullText": "A generic miltefosine pharmaceutical product containing no active pharmaceutical ingredient for the treatment of visceral leishmaniasis emerged in Bangladesh for use in the national elimination programme. Poor-quality drugs for the treatment of this fatal neglected tropical disease are life-threatening for the vulnerable patients using them but also have a devastating impact on public health and elimination programmes targeting this disease. National drug regulators should take responsibility and ensure without any concessions that procured drugs for neglected tropical diseases, either innovator or generic, adhere to international standards for drug quality and safety. Introduction Proper chemotherapy is pivotal in the management of visceral leishmaniasis (VL, also known as kala-azar); without an effective treatment this neglected parasitic disease is inevitably fatal [1]. Nevertheless, the few new and safer but more expensive treatment options that were developed in the past decade (i.e., liposomal amphotericin B and miltefosine) remain largely out of reach of the affected rural population who are most in need, mainly the poorest of the poor [2]–[4]. Miltefosine, an alkylphosphocholine drug, is an essential drug in the management of VL as it is the first effective oral treatment option with a reasonable safety profile [5]. Oral miltefosine allows the treatment of VL patients without an extended period of hospital admission and thus puts fewer demands on both patients and health services [6], [7]. Miltefosine is currently preferred for implementation in national VL elimination programmes [8], although the burden of high treatment costs incites the exploration of possibilities for a generic miltefosine product [9]. Unfortunately, the precarious position of VL patients was recently jeopardized as patients in Bangladesh were confronted with a new threat: the emergence of a new miltefosine product containing no active pharmaceutical ingredient [10], [11]. Case Description Together with Nepal and India, the government of Bangladesh has committed to eliminate VL by 2015, supported by the World Health Organization (WHO) [8], [12]. Interventions in this VL elimination programme comprise active case surveillance and implementation of vector control management strategies, but also improvement of the availability of appropriate drugs [13]. Oral miltefosine was recommended for this strategy and was therefore registered in Bangladesh [14]. Nevertheless, problems relating to its procurement and supply prohibited accessibility of this treatment and required a less costly alternative. Local procurement of miltefosine was therefore sought and a generic product supposedly containing miltefosine named “Miltefos” was manufactured by a local company for use in the Bangladeshi national elimination programme for VL. In early 2008, “Miltefos” was implemented as first-line therapy for VL in Bangladesh [8]. Although official numbers remain absent, reports from the field indicated abnormal “poor responses in hundreds of patients” after the use of “Miltefos” [8], [10], thereby clearly contradicting high historic efficacy rates (∼95%) of miltefosine in VL in nearby Indian and Nepalese provinces [15]. Therefore bioequivalence studies were planned to compare the local generic “Miltefos” product to the innovator “Impavido” product (Paladin Labs); however, the validity of the underlying assumption of pharmaceutical equivalence had to be established first. For this reason drug samples"
}