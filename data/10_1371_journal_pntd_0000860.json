{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000860",
  "doi": "10.1371/journal.pntd.0000860",
  "externalIds": [
    "pii:10-PNTD-RA-1077R4"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Annual Incidence of Snake Bite in Rural Bangladesh",
  "authors": [
    {
      "name": "Ridwanur Rahman",
      "first": "Ridwanur",
      "last": "Rahman",
      "affiliation": "Department of Medicine, Shaheed Suhrawardy Medical College, Dhaka, Bangladesh"
    },
    {
      "name": "M. Abul Faiz",
      "first": "M. Abul",
      "last": "Faiz",
      "affiliation": "Department of Medicine, Sir Salimullah Medical College, Dhaka, Bangladesh"
    },
    {
      "name": "Shahjada Selim",
      "first": "Shahjada",
      "last": "Selim",
      "affiliation": "Department of Medicine, Shaheed Suhrawardy Medical College, Dhaka, Bangladesh"
    },
    {
      "name": "Bayzidur Rahman",
      "first": "Bayzidur",
      "last": "Rahman",
      "affiliation": "School of Public Health and Community Medicine, Faculty of Medicine, University of New South Wales, Sydney, New South Wales, Australia"
    },
    {
      "name": "Ariful Basher",
      "first": "Ariful",
      "last": "Basher",
      "affiliation": "Department of Medicine, Sir Salimullah Medical College, Dhaka, Bangladesh"
    },
    {
      "name": "Alison Jones",
      "first": "Alison",
      "last": "Jones",
      "affiliation": "School of Medicine, University of Western Sydney, Sydney, New South Wales, Australia"
    },
    {
      "name": "Catherine d'Este",
      "first": "Catherine",
      "last": "d'Este",
      "affiliation": "School of Medicine and Public Health, Faculty of Health, Centre for Clinical Epidemiology and Biostatistics (CCEB), University of Newcastle, Newcastle, Australia"
    },
    {
      "name": "Moazzem Hossain",
      "first": "Moazzem",
      "last": "Hossain",
      "affiliation": "Directorate General of Health Services (DGHS), Mohakhali, Dhaka, Bangladesh"
    },
    {
      "name": "Ziaul Islam",
      "first": "Ziaul",
      "last": "Islam",
      "affiliation": "Department of Community Medicine, National Institute of Preventive and Social Medicine, Dhaka, Bangladesh"
    },
    {
      "name": "Habib Ahmed",
      "first": "Habib",
      "last": "Ahmed",
      "affiliation": "Mymensingh Medical College, Mymensingh, Bangladesh"
    },
    {
      "name": "Abul Hasnat Milton",
      "first": "Abul Hasnat",
      "last": "Milton",
      "affiliation": "School of Medicine and Public Health, Faculty of Health, Centre for Clinical Epidemiology and Biostatistics (CCEB), University of Newcastle, Newcastle, Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-10",
  "dateAccepted": "2010-09-28",
  "dateReceived": "2010-04-19",
  "volume": "4",
  "number": "10",
  "pages": "e860",
  "tags": [
    "Public Health and Epidemiology",
    "Public Health and Epidemiology/Epidemiology",
    "Public Health and Epidemiology/Global Health",
    "Public Health and Epidemiology/Preventive Medicine"
  ],
  "abstract": "Background\n\nSnake bite is a neglected public health problem in the world and one of the major causes of mortality and morbidity in many areas, particularly in the rural tropics. It also poses substantial economic burdens on the snake bite victims due to treatment related expenditure and loss of productivity. An accurate estimate of the risk of snake bite is largely unknown for most countries in the developing world, especially South-East Asia.\n\nMethodology/Principal Findings\n\nWe undertook a national epidemiological survey to determine the annual incidence density of snake bite among the rural Bangladeshi population. Information on frequency of snake bite and individuals' length of stay in selected households over the preceding twelve months was rigorously collected from the respondents through an interviewer administered questionnaire. Point estimates and confidence intervals of the incidence density of snake bite, weighted and adjusted for the multi-stage cluster sampling design, were obtained. Out of 18,857 study participants, over one year a total of 98 snake bites, including one death were reported in rural Bangladesh. The estimated incidence density of snake bite is 623.4 / 100,000 person years (95% C I 513.4–789.2 /100,000 person years). Biting occurs mostly when individuals are at work. The majority of the victims (71%) receive snake bites to their lower extremities. Eighty-six percent of the victims received some form of management within two hours of snake bite, although only three percent of the victims went directly to either a medical doctor or a hospital.\n\nConclusions/Significance\n\nIncidence density of snake bite in rural Bangladesh is substantially higher than previously estimated. This is likely due to better ascertainment of the incidence through a population based survey. Poor access to health services increases snake bite related morbidity and mortality; therefore, effective public health actions are warranted.",
  "fullText": "Introduction Snake bite particularly in the rural tropics is a major cause of mortality and morbidity, and it has a significant impact on human health and economy through treatment related expenditure and loss of productivity [1]. Snake bite is the single most important cause of envenoming worldwide and results in substantial mortality in many parts of Africa, Asia, and the Americas [2]. Snake bite is significantly neglected as a public health problem in the world as evidenced by the lack of available incidence data from most of the rural tropics where snake bites occur frequently. Global snakebites (envenomings) incidence has been estimated as 500,000 and mortality between 30000–40000 per year [3]. Chippaiux estimated that venomous snakes cause 5.4 million bites, approximately 2.5 million envenomings and over 125,000 deaths worldwide annually [4]. White estimated more than three million bites per year resulting in more than 150,000 deaths [5]. Details of the methods used to estimate these numbers have not been clearly described. More recently Anuradhani et al reported that, globally at least 421,000 envenomings occur annually, but this may be as high as 1,841,000 [6]. According to this estimate, the highest numbers of envenomings are estimated for South Asia (121,000) followed by South East Asia (111,000), and East Sub-Saharan Africa (43000). Global estimates of snakebite envenomings and deaths seem to be more accurate than previous estimates due to improved study methodology. However, this data may be inaccurate because of assumptions used in the calculations, lack of information relating to snake bites and related deaths in rural tropics. It is likely that the true numbers of these events may be substantially different from the estimates presented in this report. The true incidence of snake bite in rural Bangladesh is largely unknown. Previously, an incidence of 4.3 snake bites per 100,000 populations was reported with approximately 2000 deaths occurring annually in Bangladesh [7]. This estimate is based on data from a small study. During 1988–89, a small survey was conducted in 50 Upazillas (sub-districts) of Bangladesh that recorded 764 episodes of snakebite, of which 168 (22%) died [8]. Due to methodological limitations, these estimates are unlikely to be representative of the whole country population. According to Faiz, 1666 snake bite victims attended to the Chittagong Medical College Hospital (CMCH) for treatment between 1993 and 2003. Among those victims, 28.5% were bitten by poisonous snakes and only eight (0.5%) died [1]. In this context, this cross-sectional survey was carried out to determine the annual incidence density of snake bite in rural Bangladesh. In addition, the study also developed an epidemiologic profile of snake bites that includes age and sex specific incidence of snake bites, consequence of snake bite, treatment seeking behaviour of the patients, seasonal trend, and geographical distribution of snake bites in the context of rural Bangladesh. The study was conducted during February to June 2009 in Bangladesh. Methods Ethics statement This study was conducted according to the principles expressed in the Declaration of Helsinki. The study was approved by the Human Research"
}