{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000741",
  "doi": "10.1371/journal.pntd.0000741",
  "externalIds": [
    "pii:10-PNTD-EC-1225R1"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Global Elimination of Lymphatic Filariasis: Addressing the Public Health Problem",
  "authors": [
    {
      "name": "David G. Addiss",
      "first": "David G.",
      "last": "Addiss",
      "affiliation": "Fetzer Institute, Kalamazoo, Michigan, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-06",
  "volume": "4",
  "number": "6",
  "pages": "e741",
  "tags": [
    "Public Health and Epidemiology/Global Health",
    "Public Health and Epidemiology/Infectious Diseases",
    "Public Health and Epidemiology/Preventive Medicine"
  ],
  "fullText": "Background On July 15, 1997, two months after the World Health Assembly (WHA) passed a resolution calling for the “global elimination of lymphatic filariasis (LF) as a public health problem,” a small group of public health leaders and scientists gathered at Magnetic Island, near Townsville, Australia. They were meeting to consider the elements of a program to achieve such a lofty goal. The evening before the meeting began I asked several of those present, all veterans of global efforts to eradicate smallpox, polio, or Guinea worm disease, whether the LF elimination program should concern itself with providing care to those who already suffer the clinical manifestations of LF. Their response was uniformly negative: LF elimination should focus solely on interrupting transmission of the parasite. As with other disease eradication efforts, the intended beneficiaries were future generations; saddling the LF elimination program with responsibilities for clinical care could dilute focus, divert resources, and complicate strategies and partnerships. Two days later, the group unanimously endorsed a “two-pillar” strategy that included both interrupting transmission and providing care for those with disease [1]. Several arguments had shifted the group's position. First, there was the ethical issue: how could one ignore the suffering of 15 million people with lymphedema and 25 million men with urogenital disease, principally hydrocele? Hydrocele is readily treated with surgery, and evidence was beginning to accumulate that simple measures, including hygiene and skin care, could help arrest the progression of lymphedema [2], [3]. Second, the “public health problem” to which the WHA resolution referred was clinical disease; by itself, the presence of microfilaria in the blood does not constitute a public health problem. In affected communities, clearing the blood of microfilaria through annual mass drug administration (MDA) can interrupt transmission of the parasite. However, the scientific evidence remained divided on what effect, if any, these drugs have on established disease [4]. Finally, and perhaps most importantly, it was thought that providing care for those with filariasis-associated morbidity could increase community acceptance of MDA. The World Health Organization (WHO) launched the Global Programme to Eliminate Lymphatic Filariasis (GPELF) in 2000. Ten years on, progress in scaling up MDA has been phenomenal; 496 million persons received antifilarial drugs in 2008 [5], yielding impressive global health benefits [6]. In contrast, despite excellent pilot programs (e.g., [7]–[9]) and some at the state and national levels [10], morbidity management has generally languished. What are the reasons for this imbalance? For one, the concerns expressed at Magnetic Island had merit. The single focus of other disease elimination programs enabled them to be streamlined and efficient. The dual goals of interrupting transmission and managing morbidity may require different approaches, skills, and timeframes. Given limited resources and the ambitious goal of interrupting transmission by 2020, MDA has taken priority. Despite the experience of those who advocated a “two-pillar” strategy, there has been no scientific evidence that lymphedema management actually improves acceptance of MDA. Thus, it has been difficult to dispel the notion that investing in lymphedema management drains limited"
}