{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000484",
  "doi": "10.1371/journal.pntd.0000484",
  "externalIds": [
    "pii:09-PNTD-PP-0161R2"
  ],
  "license": "This is an open-access article distributed under the\n                terms of the Creative Commons Attribution License, which permits unrestricted use,\n                distribution, and reproduction in any medium, provided the original author and\n                source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "New, Improved Treatments for Chagas Disease: From the R&D\n                    Pipeline to the Patients",
  "authors": [
    {
      "name": "Isabela Ribeiro",
      "first": "Isabela",
      "last": "Ribeiro",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    },
    {
      "name": "Ann-Marie Sevcsik",
      "first": "Ann-Marie",
      "last": "Sevcsik",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    },
    {
      "name": "Fabiana Alves",
      "first": "Fabiana",
      "last": "Alves",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    },
    {
      "name": "Graciela Diap",
      "first": "Graciela",
      "last": "Diap",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    },
    {
      "name": "Robert Don",
      "first": "Robert",
      "last": "Don",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    },
    {
      "name": "Michael O. Harhay",
      "first": "Michael O.",
      "last": "Harhay",
      "affiliation": "Masters of Public Health Program, University\n                    of Pennsylvania School of Medicine, Philadelphia, Pennsylvania, United States of\n                    America"
    },
    {
      "name": "Shing Chang",
      "first": "Shing",
      "last": "Chang",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    },
    {
      "name": "Bernard Pecoul",
      "first": "Bernard",
      "last": "Pecoul",
      "affiliation": "Drugs for Neglected Diseases\n                        initiative, Geneva, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-07",
  "volume": "3",
  "number": "7",
  "pages": "e484",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Public Health and Epidemiology/Global Health"
  ],
  "fullText": "Introduction Endemic throughout Latin America with a prevalence rate of approximately 1.4%, Chagas disease (CD) is estimated to kill 14,000 people every year, which is more people in the region each year than any other parasite-born disease, including malaria [1],[2]. Brazilian physician Carlos Chagas first described CD exactly a century ago [3], and its socioeconomic impact makes it the most important parasitic disease in the Americas [4]. Estimated to infect somewhere between 8 to 14 million people, CD both afflicts the poor and, like other neglected tropical diseases, “promotes poverty” [2],[5]. Through its impact on worker productivity, and by causing premature disability and death, CD annually costs an estimated 667,000 disability-adjusted life years lost [1],[6]. In the case of Brazil alone, losses of over US$1.3 billion in wages and industrial productivity were due to the disabilities of workers with CD [7]. CD is an important public health issue, both in Latin America and increasingly around the world: the infection rate in endemic areas is estimated to be 1.4% [8], with geographic variation from 0.1% to 45.2% [9]. Vectorial transmission has been significantly reduced due to control efforts like the Southern Cone Initiative [10],[11] and others [11],[12]. However, there are areas producing new cases such as regions untouched by vector control efforts [13], special areas with non-domiciliated triatomine [14], and the Amazon region with recent cases reported via oral transmission and by wild triatomine [15]. And still to this day, millions of patients remain without adequate treatment for this silently debilitating and potentially fatal disease. Although no official global figures exist, it is estimated that no more than 1% of those infected are believed to receive any treatment at all. An increasing number of CD patients are also seen in non-endemic, developed countries because of globalization and the movement of unknowingly infected people from Latin America to other parts of the world [16],[17],[18]. The appearance of Trypanosoma cruzi in blood banks in the United States has led the Food and Drug Administration (FDA) to recently issue a draft guidance on CD screening [19]. The Need for New, Improved Treatments To better understand the need for new treatments, it is important to review a bit of CD pathology and clinical evolution. Caused by infection with the protozoan parasite T. cruzi, CD starts with an acute phase in which the parasitemia is often high and parasitological diagnosis can be made by direct microscopic examination of fresh blood. This disease phase (in which 2%–8% of children die) [20],[21] frequently passes undiagnosed in the absence of active screening programs, as CD manifests itself with a febrile and toxemic illness having non-specific symptoms reminiscent of any childhood infection. If untreated, the disease transitions into a clinically silent, indeterminate chronic phase. Later, 10 to 30 years after the initial infection, approximately 30% of infected people will experience the symptomatic, chronic stage characterized by severe organ pathologies primarily involving the cardiac and gastrointestinal systems [22],[23]. During the long-lasting chronic phase, parasites are primarily in the tissues, thereby rendering"
}