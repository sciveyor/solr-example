{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001045",
  "doi": "10.1371/journal.pntd.0001045",
  "externalIds": [
    "pii:10-PNTD-RA-1572R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Evaluation of Spatially Targeted Strategies to Control Non-Domiciliated Triatoma dimidiata Vector of Chagas Disease",
  "authors": [
    {
      "name": "Corentin Barbu",
      "first": "Corentin",
      "last": "Barbu",
      "affiliation": "UMR 5244 CNRS/UPVD/EPHE, ‘Biologie et Ecologie Tropicale et Méditerranéenne’, Université de Perpignan Via Domitia, Perpignan, France"
    },
    {
      "name": "Eric Dumonteil",
      "first": "Eric",
      "last": "Dumonteil",
      "affiliation": "Laboratorio de Parasitología, Centro de Investigaciones Regionales “Dr. Hideyo Noguchi”, Universidad Autónoma de Yucatán, Mérida, Yucatan, Mexico; Department of Tropical Medicine, School of Public Health and Tropical Medicine, Tulane University, New Orleans, Louisiana, United States of America"
    },
    {
      "name": "Sébastien Gourbière",
      "first": "Sébastien",
      "last": "Gourbière",
      "affiliation": "UMR 5244 CNRS/UPVD/EPHE, ‘Biologie et Ecologie Tropicale et Méditerranéenne’, Université de Perpignan Via Domitia, Perpignan, France; Centre for the Study of Evolution, School of Life Sciences, University of Sussex, Brighton, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-05",
  "dateAccepted": "2011-04-14",
  "dateReceived": "2010-09-22",
  "volume": "5",
  "number": "5",
  "pages": "e1045",
  "tags": [
    "Ecology/Population Ecology",
    "Ecology/Spatial and Landscape Ecology",
    "Ecology/Theoretical Ecology",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections"
  ],
  "abstract": "Background\n\nChagas disease is a major neglected tropical disease with deep socio-economical effects throughout Central and South America. Vector control programs have consistently reduced domestic populations of triatomine vectors, but non-domiciliated vectors still have to be controlled efficiently. Designing control strategies targeting these vectors is challenging, as it requires a quantitative description of the spatio-temporal dynamics of village infestation, which can only be gained from combinations of extensive field studies and spatial population dynamic modelling.\n\nMethodology/Principal Findings\n\nA spatially explicit population dynamic model was combined with a two-year field study of T. dimidiata infestation dynamics in the village of Teya, Mexico. The parameterized model fitted and predicted accurately both intra-annual variation and the spatial gradient in vector abundance. Five different control strategies were then applied in concentric rings to mimic spatial design targeting the periphery of the village, where vectors were most abundant. Indoor insecticide spraying and insect screens reduced vector abundance by up to 80% (when applied to the whole village), and half of this effect was obtained when control was applied only to the 33% of households closest to the village periphery. Peri-domicile cleaning was able to eliminate up to 60% of the vectors, but at the periphery of the village it has a low effect, as it is ineffective against sylvatic insects. The use of lethal traps and the management of house attractiveness provided similar levels of control. However this required either house attractiveness to be null, or ≥5 lethal traps, at least as attractive as houses, to be installed in each household.\n\nConclusion/Significance\n\nInsecticide and insect screens used in houses at the periphery of the village can contribute to reduce house infestation in more central untreated zones. However, this beneficial effect remains insufficient to allow for a unique spatially targeted strategy to offer protection to all households. Most efficiently, control should combine the use of insect screens in outer zones to reduce infestation by both sylvatic and peri-domiciliated vectors, and cleaning of peri-domicile in the centre of the village where sylvatic vectors are absent. The design of such spatially mixed strategies of control offers a promising avenue to reduce the economic cost associated with the control of non-domiciliated vectors.",
  "fullText": "Introduction Chagas disease, also called American trypanosomiasis, is caused by the protozoan parasite Trypanosoma cruzi, which is primarily transmitted to humans by blood-sucking bugs of the Triatominae subfamily. The disease is endemic throughout Latin America, where it is one of the most important parasitic diseases with large socioeconomic impact. According to various estimates, the prevalence rate in humans varies between 0.1 and 45.2% (with an average of 1.4%), 8 to 15 million people are infected with T. cruzi (with 40–50,000 yearly new cases), and 28–75 million individuals are at risk of infection [1]–[3]. The disease causes about 12,500 deaths a year, and is responsible for premature disabilities of workers that are estimated to cost 670,000 disability-adjusted life years lost [4]. Although international initiatives have been launched to reduce transmission of Chagas disease, especially through vector control and screening of blood or organ donors [5], there are still large regions with active vector transmission [6]. One of the main explanations for this is the transmission caused by non-domiciliated triatomines [7]. These vectors are not able to reproduce and develop in the domestic habitat, and thus constitute typical ‘sink’ domestic populations sustained by peri-domestic and/or sylvatic ‘source’ populations [8]. Non-domiciliated vectors tend to jeopardize the efficacy of vector control by insecticide spraying in the domestic habitat because of the re-infestation of treated houses [9], [10], [11]. This situation has been described for several vector species of triatomines as T. brasiliensis and T. pseudomaculata in Brazil [12], T. mexicana in central Mexico [13] and T. dimidiata in the Yucatan Peninsula of Mexico and Belize [14], [15]. Accordingly, the risk of transmission associated with non-domiciliated vectors is now identified as a major challenge for the future of Chagas disease control [16], [17], [18], and a key objective is to evaluate the efficacy of classical or alternative control strategies to reduce their abundance. Identifying optimal strategies can hardly be achieved through laboratory or field experiments, since testing a broad enough number of alternatives would require very large human and financial investments [11], [19]. Alternatively, mathematical models have proven to be very effective at evaluating the relative merit of various alternative strategies to control parasitic diseases [11; and references therein]. In addition, identifying optimal strategies clearly requires a detailed understanding of the vector spatial and temporal infestation dynamics. Valuable insights into such spatio-temporal dynamics can be gained using the framework of meta-population theory combined with presence/absence data [19]–[21]. Although appealing, the use of more elaborated models that include quantitative information on local population sizes requires even more data than the meta-population model sensus stricto [22]. In previous contributions, we developed spatially explicit population dynamics models that were able to reproduce and to predict the spatial and temporal dynamics of T. dimidiata house infestation observed at the village scale in the Yucatan Peninsula, Mexico. These models provided us with indirect estimates of the origin and characteristics of dispersal of these triatomines [23], [24]. Individuals found inside houses in the Yucatan Peninsula originated in similar proportions from both"
}