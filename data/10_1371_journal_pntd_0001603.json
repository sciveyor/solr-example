{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001603",
  "doi": "10.1371/journal.pntd.0001603",
  "externalIds": [
    "pii:PNTD-D-11-00539"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Research Agenda for Helminth Diseases of Humans: Social Ecology, Environmental Determinants, and Health Systems",
  "authors": [
    {
      "name": "Andrea Gazzinelli",
      "first": "Andrea",
      "last": "Gazzinelli",
      "affiliation": "Escola de Enfermagem, Universidade Federal de Minas Gerais, Belo Horizonte, Brazil; Instituto Nacional de Ciência e Tecnologia em Doenças Tropicais – INCT-DT, Belo Horizonte, Brazil"
    },
    {
      "name": "Rodrigo Correa-Oliveira",
      "first": "Rodrigo",
      "last": "Correa-Oliveira",
      "affiliation": "Instituto Nacional de Ciência e Tecnologia em Doenças Tropicais – INCT-DT, Belo Horizonte, Brazil; Centro de Pesquisas Rene Rachou, FIOCRUZ MG, Belo Horizonte, Brazil"
    },
    {
      "name": "Guo-Jing Yang",
      "first": "Guo-Jing",
      "last": "Yang",
      "affiliation": "Department of Schistosomiasis Control, Jiangsu Institute of Parasitic Diseases, Jiangsu, People's Republic of China"
    },
    {
      "name": "Boakye A. Boatin",
      "first": "Boakye A.",
      "last": "Boatin",
      "affiliation": "PO Box CT 1380, Accra, Ghana"
    },
    {
      "name": "Helmut Kloos",
      "first": "Helmut",
      "last": "Kloos",
      "affiliation": "Department of Epidemiology and Biostatistics, University of California Medical Center, San Francisco, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-04",
  "volume": "6",
  "number": "4",
  "pages": "e1603",
  "tags": [
    "Behavioral and social aspects of health",
    "Epidemiology",
    "Global health",
    "Infectious disease epidemiology",
    "Medicine",
    "Public Health and Epidemiology",
    "Public health",
    "Social epidemiology",
    "Socioeconomic aspects of health"
  ],
  "abstract": "In this paper, the Disease Reference Group on Helminth Infections (DRG4), established in 2009 by the Special Programme for Research and Training in Tropical Diseases (TDR), with the mandate to review helminthiases research and identify research priorities and gaps, focuses on the environmental, social, behavioural, and political determinants of human helminth infections and outlines a research and development agenda for the socioeconomic and health systems research required for the development of sustainable control programmes. Using Stockols' social-ecological approach, we describe the role of various social (poverty, policy, stigma, culture, and migration) and environmental determinants (the home environment, water resources development, and climate change) in the perpetuation of helminthic diseases, as well as their impact as contextual factors on health promotion interventions through both the regular and community-based health systems. We examine these interactions in regard to community participation, intersectoral collaboration, gender, and possibilities for upscaling helminthic disease control and elimination programmes within the context of integrated and interdisciplinary approaches. The research agenda summarises major gaps that need to be addressed.",
  "fullText": "Introduction A distinct feature of helminthiasis and other neglected tropical diseases (NTDs; see Box 1 for abbreviations list) is that they occur mostly in poor communities, where conditions for transmission are rife and where they play an important role in contributing to poverty [1]. In this context Hotez and Yamey [2] aptly included the helminth diseases in the group of “neglected infections of poverty”. This classification requires prevention, control, and elimination efforts, considering the wide range of social, cultural, economic, and political factors underlying vulnerabilities and disease risks. The study of these factors has been neglected, and attempts to closely work with other disciplines and sectors have remained largely tokenistic and rhetorical [3]. Furthermore, the characteristic of helminth diseases of having different life cycles, with water, soil, food, and insect vector transmission routes imposes an additional great challenge with significant diversity in disease ecologies that must be considered in the development of interventions to render them effective and sustainable. Box 1. List of Abbreviations AIDS, acquired immunodeficiency syndrome APOC, African Programme for Onchocerciasis Control ComDT, community-directed treatment DRG4, Disease Reference Group on Helminth Infections DALY, disability-adjusted life year HIV, human immunodeficiency virus LF, lymphatic filariasis MDA, mass drug administration MDG, Millennium Development Goal NGO, non-governmental organisation NTD, neglected tropical disease STH, soil-transmitted helminthiasis WHO, World Health Organization TDR, Special Programme for Research and Training in Tropical Diseases UN, United Nations The study of NTDs is often considered to be a biomedical endeavor centered on chemotherapy, spearheaded by mass drug administration (MDA) [4], [5]. The chemotherapy “tool” is widely considered to be the most appropriate and effective strategy to helminth control in view of the inherent difficulties of implementing alternative control strategies such as a safe water supply and sanitation [6], [7] and the characteristically slow pace of improvement in socioeconomic conditions in resource-poor areas [8]. Although major reductions in the prevalence, intensity, morbidity, and even socioeconomic impacts of helminths have been achieved through chemotherapy, it is generally accepted that improvements in domestic water supplies, environmental sanitation, housing, health education, access to health services for diagnosis and treatment, and vector control must be integrated in control and elimination programs to assure their effectiveness [9], [10]. These goals have not been met in many communities. Although these improvements may not be generalised to all infections in the context of control/elimination, for example access to water and sanitation will not impact on control/elimination efforts for onchocerciasis, they must be seen as important measures to improve the general health conditions of the population. These and other relevant social goals have not been achieved in many communities. The increasing need to plan, design, implement, monitor, and evaluate programs “outside the box” (beyond the health sector) in an effort to make them more effective and sustainable will require increased engagement of other disciplines, particularly the social sciences, but also the biological sciences and environmental engineering, as well as intersectoral and community-based approaches [3], [11]–[13]. Anthelmintic treatment needs to be seen as a necessary, but not sufficient,"
}