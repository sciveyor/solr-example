{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000725",
  "doi": "10.1371/journal.pntd.0000725",
  "externalIds": [
    "pii:09-PNTD-RA-0735R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Vaccination with DNA Plasmids Expressing Gn Coupled to C3d or Alphavirus Replicons Expressing Gn Protects Mice against Rift Valley Fever Virus",
  "authors": [
    {
      "name": "Nitin Bhardwaj",
      "first": "Nitin",
      "last": "Bhardwaj",
      "affiliation": "Department of Infectious Diseases and Microbiology, Graduate School of Public Health, University of Pittsburgh, Pittsburgh, Pennsylvania, United States of America; Center for Vaccine Research, University of Pittsburgh, Pittsburgh, Pennsylvania, United States of America"
    },
    {
      "name": "Mark T. Heise",
      "first": "Mark T.",
      "last": "Heise",
      "affiliation": "Department of Microbiology and Immunology, The Carolina Vaccine Institute, University of North Carolina, Chapel Hill, North Carolina, United States of America"
    },
    {
      "name": "Ted M. Ross",
      "first": "Ted M.",
      "last": "Ross",
      "affiliation": "Center for Vaccine Research, University of Pittsburgh, Pittsburgh, Pennsylvania, United States of America; Department of Microbiology and Molecular Genetics, University of Pittsburgh, Pittsburgh, Pennsylvania, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-06",
  "dateAccepted": "2010-05-03",
  "dateReceived": "2009-12-15",
  "volume": "4",
  "number": "6",
  "pages": "e725",
  "tags": [
    "Immunology/Immune Response",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Viral Infections",
    "Virology/Vaccines"
  ],
  "abstract": "Background\n            \nRift Valley fever (RVF) is an arthropod-borne viral zoonosis. Rift Valley fever virus (RVFV) is an important biological threat with the potential to spread to new susceptible areas. In addition, it is a potential biowarfare agent.\n\n            Methodology/Principal Findings\n            \nWe developed two potential vaccines, DNA plasmids and alphavirus replicons, expressing the Gn glycoprotein of RVFV alone or fused to three copies of complement protein, C3d. Each vaccine was administered to mice in an all DNA, all replicon, or a DNA prime/replicon boost strategy and both the humoral and cellular responses were assessed. DNA plasmids expressing Gn-C3d and alphavirus replicons expressing Gn elicited high titer neutralizing antibodies that were similar to titers elicited by the live-attenuated MP12 virus. Mice vaccinated with an inactivated form of MP12 did elicit high titer antibodies, but these antibodies were unable to neutralize RVFV infection. However, only vaccine strategies incorporating alphavirus replicons elicited cellular responses to Gn. Both vaccines strategies completely prevented weight loss and morbidity and protected against lethal RVFV challenge. Passive transfer of antisera from vaccinated mice into naïve mice showed that both DNA plasmids expressing Gn-C3d and alphavirus replicons expressing Gn elicited antibodies that protected mice as well as sera from mice immunized with MP12.\n\n            Conclusion/Significance\n            \nThese results show that both DNA plasmids expressing Gn-C3d and alphavirus replicons expressing Gn administered alone or in a DNA prime/replicon boost strategy are effective RVFV vaccines. These vaccine strategies provide safer alternatives to using live-attenuated RVFV vaccines for human use.",
  "fullText": "Introduction Rift Valley fever (RVF) is an arthropod-borne viral zoonosis. The causative agent Rift Valley fever virus (RVFV) belongs to the genus Phlebovirus of the family Bunyaviridae and was first discovered in the Rift Valley of Kenya in 1931 [1]. RVFV infections in livestock are characterized by an acute hepatitis, abortion and high mortality rates, especially in new born or young animals. Human infection with RVFV typically leads to a mild flu-like febrile illness. However, ∼2% of infected individuals have more severe complications, such as retinal degeneration, fatal hepatitis, severe encephalitis and hemorrhagic fever [2]. The ability of RVFV to cross geographic or national boundaries, coupled with the fact that RVFV replicates in a wide range of mosquito vectors, have raised concerns that the virus might spread further into non-endemic regions of the world. Before 1977, RVFV circulation was not detected beyond the Sub-Saharan countries. However, since 1997, RVFV outbreaks have occurred in Egypt [3], Mauritania in 1987 and 1998 [4], Saudi Arabia and Yemen [5]. In 2006–2007, RVFV outbreaks were recorded in Kenya, Somalia and Tanzania that resulted in human infections and deaths [6]. Thus, the ability of RVFV to cause explosive “virgin soil” outbreaks in previously unaffected regions demonstrates the need for prophylactic measures for this significant veterinary and public health threat. The virus genome is composed of three single-stranded negative-sense RNA segments. The large (L) segment (∼6.4kb) encodes for the RNA-dependent RNA polymerase [7]. A medium (M) segment (∼3.8kb) encodes for four known proteins in a single open reading frame (ORF). These include the two structural glycoproteins, Gn and Gc, and the 14kDa non-structural NSm protein and the 78kDa NSm-Gn fusion peptide [7], [8], [9]. The small (S) segment is ambisense and encodes for the 1.6kDa viral nucleoprotein (N) in genomic orientation, as well as a non-structural (NSs) protein in the anti-genomic orientation [7]. The nonstructural genes (NSs and NSm) function to suppress host antiviral responses [10], [11]. RVFV is an important zoonotic pathogen with the potential to emerge in new areas through the spread of infected insect vectors or livestock or though intentional release as a bioterror agent. [12]. Inactivated RVFV vaccine (TSI-GSD-200) have been shown to elicit protective immunity in humans [13], however multiple booster vaccinations are required to achieve protective immunity, and perhaps most importantly, for many individuals, immunity rapidly wanes in the absence of follow-up booster vaccinations [13]. A modified live virus vaccine, based upon the Smithburn strain, is available for livestock in Africa [14], but it can cause pathology, spontaneous abortions, and teratogenic effects [15], [16], furthermore, animals vaccinated with live attenuated RVFV strains cannot be differentiated from naturally infected livestock, which may preclude export of these animals to non-RVFV endemic areas. One vaccine candidate under evaluation for human use is MP12, which is a mutagen-attenuated strain of the Egyptian RVFV isolate, ZH548 [17]. This vaccine was developed for use in both humans and livestock, with encouraging results in initial animal trials, but may cause teratology in pregnant animals [18]. In addition"
}