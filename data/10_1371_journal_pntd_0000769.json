{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000769",
  "doi": "10.1371/journal.pntd.0000769",
  "externalIds": [
    "pii:09-PNTD-RA-0696R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Prediction of Dengue Disease Severity among Pediatric Thai Patients Using Early Clinical Laboratory Indicators",
  "authors": [
    {
      "name": "James A. Potts",
      "first": "James A.",
      "last": "Potts",
      "affiliation": "Center for Infectious Disease and Vaccine Research, University of Massachusetts Medical School, Worcester, Massachusetts, United States of America"
    },
    {
      "name": "Robert V. Gibbons",
      "first": "Robert V.",
      "last": "Gibbons",
      "affiliation": "Department of Virology, Armed Forces Research Institute of Medical Sciences, Bangkok, Thailand"
    },
    {
      "name": "Alan L. Rothman",
      "first": "Alan L.",
      "last": "Rothman",
      "affiliation": "Center for Infectious Disease and Vaccine Research, University of Massachusetts Medical School, Worcester, Massachusetts, United States of America"
    },
    {
      "name": "Anon Srikiatkhachorn",
      "first": "Anon",
      "last": "Srikiatkhachorn",
      "affiliation": "Center for Infectious Disease and Vaccine Research, University of Massachusetts Medical School, Worcester, Massachusetts, United States of America"
    },
    {
      "name": "Stephen J. Thomas",
      "first": "Stephen J.",
      "last": "Thomas",
      "affiliation": "Department of Virology, Armed Forces Research Institute of Medical Sciences, Bangkok, Thailand"
    },
    {
      "name": "Pra-on Supradish",
      "first": "Pra-on",
      "last": "Supradish",
      "affiliation": "Queen Sirikit National Institute of Child Health, Bangkok, Thailand"
    },
    {
      "name": "Stephenie C. Lemon",
      "first": "Stephenie C.",
      "last": "Lemon",
      "affiliation": "Division of Preventive and Behavioral Medicine, University of Massachusetts Medical School, Worcester, Massachusetts, United States of America"
    },
    {
      "name": "Daniel H. Libraty",
      "first": "Daniel H.",
      "last": "Libraty",
      "affiliation": "Center for Infectious Disease and Vaccine Research, University of Massachusetts Medical School, Worcester, Massachusetts, United States of America"
    },
    {
      "name": "Sharone Green",
      "first": "Sharone",
      "last": "Green",
      "affiliation": "Center for Infectious Disease and Vaccine Research, University of Massachusetts Medical School, Worcester, Massachusetts, United States of America"
    },
    {
      "name": "Siripen Kalayanarooj",
      "first": "Siripen",
      "last": "Kalayanarooj",
      "affiliation": "Queen Sirikit National Institute of Child Health, Bangkok, Thailand"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-08",
  "dateAccepted": "2010-06-21",
  "dateReceived": "2009-12-14",
  "volume": "4",
  "number": "8",
  "pages": "e769",
  "tags": [
    "Infectious Diseases/Viral Infections"
  ],
  "abstract": "Background\n\nDengue virus is endemic in tropical and sub-tropical resource-poor countries. Dengue illness can range from a nonspecific febrile illness to a severe disease, Dengue Shock Syndrome (DSS), in which patients develop circulatory failure. Earlier diagnosis of severe dengue illnesses would have a substantial impact on the allocation of health resources in endemic countries.\n\nMethods and Findings\n\nWe compared clinical laboratory findings collected within 72 hours of fever onset from a prospective cohort children presenting to one of two hospitals (one urban and one rural) in Thailand. Classification and regression tree analysis was used to develop diagnostic algorithms using different categories of dengue disease severity to distinguish between patients at elevated risk of developing a severe dengue illness and those at low risk. A diagnostic algorithm using WBC count, percent monocytes, platelet count, and hematocrit achieved 97% sensitivity to identify patients who went on to develop DSS while correctly excluding 48% of non-severe cases. Addition of an indicator of severe plasma leakage to the WHO definition led to 99% sensitivity using WBC count, percent neutrophils, AST, platelet count, and age.\n\nConclusions\n\nThis study identified two easily applicable diagnostic algorithms using early clinical indicators obtained within the first 72 hours of illness onset. The algorithms have high sensitivity to distinguish patients at elevated risk of developing severe dengue illness from patients at low risk, which included patients with mild dengue and other non-dengue febrile illnesses. Although these algorithms need to be validated in other populations, this study highlights the potential usefulness of specific clinical indicators early in illness.",
  "fullText": "Introduction Dengue fever (DF) and dengue hemorrhagic fever (DHF), the more severe form of dengue illness, are re-emerging viral diseases [1]. Dengue is endemic in countries in tropical and subtropical areas. Dengue viruses are transmitted through the bite of an infected mosquito [2]. Illnesses caused by dengue viruses can range from a nonspecific febrile illness, as in most DF cases, to more severe illness with bleeding, thrombocytopenia, and plasma leakage, in cases of DHF [3]. DHF with circulatory failure defines DHF grades 3 and 4, also termed dengue shock syndrome (DSS) [3]. However, strict adherence to WHO criteria for diagnosis of DHF has been difficult and some researchers have established different categories of severe dengue illnesses [4]–[7]. Dengue has a substantial economic impact in developing countries [8], [9]. Individuals and families are impacted by lost wages, cost of seeking care, cost of treatment, missed school, and extended effects of recovery [8]–[12]. Prevention and control strategies have been poorly implemented or unsustained and thus largely ineffective [13], [14]. Currently, there is no licensed vaccine or anti-viral against dengue. The treatment for patients with suspected dengue is supportive care consisting of rehydration and anti-pyretics [3]. Patients with suspected dengue are often hospitalized for close monitoring. Plasma leakage occurs around the time of defervescence. Prior to this critical phase, it has proven difficult to differentiate mild vs. severe dengue illness. Ideally, only severe cases of DF and DHF should be hospitalized. However, there are no diagnostic/prognostic tools available to distinguish severe dengue from non-severe dengue or other febrile illness (OFI) at early stages of illness. Such tools could improve clinical practice by decreasing the number of un-needed hospitalizations, improving utilization of limited hospital resources to treat more severely ill patients, improving outcomes of severely ill patients by administering needed care earlier, and improving the capability of physicians in developing or rural areas to make a more accurate early diagnosis. We conducted a prospective study of Thai children with acute febrile illness, consistent with dengue, enrolled from an early stage of illness onset [15]. We applied classification and regression tree (CART) analysis to this dataset to distinguish patients with severe dengue illness from those with mild dengue illness and OFI. CART was used to establish a diagnostic decision tree using clinical laboratory variables and patient characteristics collected at presentation. Methods Study Setting A longitudinal observational study was conducted at two hospitals in Thailand: (1) the Queen Sirikit National Institute of Child Health (QSNICH) in Bangkok during 1994–97, 1999–2002, and 2004–07, and (2) the Kamphaeng Phet Provincial Hospital (KPPH) in the Kamphaeng Phet providence in a rural northern section of Thailand during 1994–97. The study methods have been described in detail elsewhere [15]. In brief, children between the ages of six months and 15 years presenting with temperature ≥38°C for no more than 72 hours and no localizing symptoms were identified in the outpatient department or on the hospital ward were eligible for enrollment with parental consent. Exclusion criteria included: signs of shock at presentation,"
}