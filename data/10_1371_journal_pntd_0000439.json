{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000439",
  "doi": "10.1371/journal.pntd.0000439",
  "externalIds": [
    "pii:09-PNTD-RA-0108R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The Hookworm Tissue Inhibitor of Metalloproteases (Ac-TMP-1) Modifies Dendritic Cell Function and Induces Generation of CD4 and CD8 Suppressor T Cells",
  "authors": [
    {
      "name": "Carmen Cuéllar",
      "first": "Carmen",
      "last": "Cuéllar",
      "affiliation": "Departamento de Parasitología, Facultad de Farmacia, Universidad Complutense, Madrid, Spain"
    },
    {
      "name": "Wenhui Wu",
      "first": "Wenhui",
      "last": "Wu",
      "affiliation": "J.A. Baker Institute for Animal Health, College of Veterinary Medicine, Cornell University, Ithaca, New York, United States of America"
    },
    {
      "name": "Susana Mendez",
      "first": "Susana",
      "last": "Mendez",
      "affiliation": "J.A. Baker Institute for Animal Health, College of Veterinary Medicine, Cornell University, Ithaca, New York, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-05",
  "dateAccepted": "2009-04-21",
  "dateReceived": "2009-03-13",
  "volume": "3",
  "number": "5",
  "pages": "e439",
  "tags": [
    "Immunology/Immunomodulation"
  ],
  "abstract": "Hookworm infection is a major cause of disease burden for humans. Recent studies have described hookworm-related immunosuppression in endemic populations and animal models. A Tissue Inhibitor of Metalloproteases (Ac-TMP-1) has been identified as one of the most abundant proteins released by the adult parasite. We investigated the effect of recombinant Ac-TMP-1 on dendritic cell (DC) and T cell function. Splenic T cells from C57BL/6 mice injected with Ac-TMP-1 showed reduced proliferation to restimulation with anti CD3 or bystander antigens such as OVA. Incubation of bone marrow-derived DCs with Ac-TMP-1 decreased MHC Class I and, especially, Class II expression but increased CD86 and IL-10 expression. Co-incubation of splenic T cells with DCs pulsed with Ac-TMP-1 induced their differentiation into CD4+ and, particularly, CD8+ CD25+Foxp3+ T cells that expressed IL-10. These cells were able to suppress proliferation of naïve and activated CD4+ T cells by TGF-Β-dependent (CD4+ suppressors) or independent (CD8+ suppressors) mechanisms. Priming of DCs with non-hookworm antigens, such as OVA, did not result in the generation of suppressor T cells. These data indicate that Ac-TMP-1 initiates the development of a regulatory response through modifications in DC function and generation of suppressor T cells. This is the first report to propose a role of suppressor CD8+ T cells in gastrointestinal helminthic infections.",
  "fullText": "Introduction The human hookworms Necator americanus and Ancylostoma duodenale are directly transmitted nematode parasites of the small intestine, and the main species that cause human hookworm infection, a leading cause of iron-deficiency anemia and malnutrition with a prevalence of 600 million cases in the tropical developing world [1]. Though mortality is rare, the global burden of hookworm disease is high, with an estimated 22 million Disability-Adjusted Life Years (DALYs) lost each year [1]. These numbers alone outrank diseases such as African trypanosomiasis, dengue, Chagas' disease, schistosomiasis, and leprosy [2]. For many common helminthic infections, including ascariasis, trichuriasis, and schistosomiasis, the intensity of infection peaks during childhood and adolescence [3]. In contrast, there appears to be considerable variation in the age profile of hookworm infection. Although the hookworm burden may be heavy in children, especially those in sub-Saharan Africa [4],[5], the most commonly recognized pattern is a steady rise in the intensity of infection during childhood, with either a peak or a plateau in adulthood. This lack of exposure or age-related immunity indicates that hookworms can either evade or suppress host immune responses. Studies performed by us and others have confirmed that hookworm infections decrease the ability of the immune system to respond to hookworm and bystander antigens, as evidenced by decreased lymphocyte responses in hookworm-infected humans [6],[7],[8], dogs [9] and hamsters [10],[11], as well as elevated serum IL-10 and immunosuppression in patients infected with N. americanus [12], or infected and exposed to adult parasite extracts [13]. Chemotherapy against the parasite restores the immune response in humans [14] and increases the immunogenicity of anti-hookworm vaccines in hamsters [10],[11]. Most of the pathology caused by the hookworm results from the adult stage of the parasite [15],[16]. While feeding, adult worms release into host tissues a battery of pharmacologically and immunologically active molecules [17]. Work by several groups has begun to unravel the biochemical events linked to the resultant blood loss that develops as a consequence of parasite attachment [18]. Among the secreted antigens, a hookworm-secreted Tissue Inhibitor of Metalloproteases (Ac-TMP-1) has been identified in A. caninum [19] and A. ceylanicum [20] as one of the most abundant proteins released by the adult parasite, at a rate of 40 ng/h [19]. In this report, we aimed to investigate the effect of the recombinant protein Ac-TMP-1 on dendritic cell function (DC) and generation of suppressor T cells. Splenic T cells from mice treated with Ac-TMP-1 exhibited decreased lymphoproliferative responses when restimulated ex vivo with Ac-TIMP or anti CD3. To understand the mechanism behind this suppression of proliferation, we incubated bone marrow-derived dendritic cells (DCs) from C57BL/6 mice (B/6) with Ac-TMP-1, and discovered that DCs exposed to the hookworm antigen decreased expression of MHC Class I and II and increased expression of CD86 and IL-10, as well as production of TGF-Β. Moreover, co-incubation of naïve splenic T cells with DC pulsed with Ac-TMP-1 induced their differentiation of T cells into IL-10 producing CD4+ and CD8+ CD25+Foxp3+ regulatory T cells that suppressed proliferation of both naïve"
}