{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001433",
  "doi": "10.1371/journal.pntd.0001433",
  "externalIds": [
    "pii:PNTD-D-11-00650"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Post-Kala-azar Dermal Leishmaniasis in Nepal: A Retrospective Cohort Study (2000–2010)",
  "authors": [
    {
      "name": "Surendra Uranw",
      "first": "Surendra",
      "last": "Uranw",
      "affiliation": "Department of Internal Medicine, B.P. Koirala Institute of Health Sciences, Ghopa, Dharan, Nepal; Department of Public Health, Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Bart Ostyn",
      "first": "Bart",
      "last": "Ostyn",
      "affiliation": "Department of Public Health, Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Arpana Rijal",
      "first": "Arpana",
      "last": "Rijal",
      "affiliation": "Department of Dermatology, B.P. Koirala Institute of Health Sciences, Ghopa, Dharan, Nepal"
    },
    {
      "name": "Saru Devkota",
      "first": "Saru",
      "last": "Devkota",
      "affiliation": "Department of Internal Medicine, B.P. Koirala Institute of Health Sciences, Ghopa, Dharan, Nepal"
    },
    {
      "name": "Basudha Khanal",
      "first": "Basudha",
      "last": "Khanal",
      "affiliation": "Department of Microbiology, B.P. Koirala Institute of Health Sciences, Ghopa, Dharan, Nepal"
    },
    {
      "name": "Joris Menten",
      "first": "Joris",
      "last": "Menten",
      "affiliation": "Department of Public Health, Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Marleen Boelaert",
      "first": "Marleen",
      "last": "Boelaert",
      "affiliation": "Department of Public Health, Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Suman Rijal",
      "first": "Suman",
      "last": "Rijal",
      "affiliation": "Department of Internal Medicine, B.P. Koirala Institute of Health Sciences, Ghopa, Dharan, Nepal"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-12",
  "dateAccepted": "2011-10-31",
  "dateReceived": "2011-07-08",
  "volume": "5",
  "number": "12",
  "pages": "e1433",
  "tags": [
    "Infectious Diseases",
    "Infectious diseases",
    "Leishmaniasis",
    "Medicine",
    "Neglected tropical diseases"
  ],
  "abstract": "Introduction\n          \nPost-kala-azar dermal leishmaniasis (PKDL) is a cutaneous complication appearing after treatment of visceral leishmaniasis, and PKDL patients are considered infectious to sand flies and may therefore play a role in the transmission of VL. We estimated the risk and risk factors of PKDL in patients with past VL treatment in south-eastern Nepal.\n\n          Methods\n          \nBetween February and May 2010 we traced all patients who had received VL treatment during 2000–2009 in five high-endemic districts and screened them for PKDL-like skin lesions. Suspected cases were referred to a tertiary care hospital for confirmation by parasitology (slit skin smear (SSS)) and/or histopathology. We calculated the risk of PKDL using Kaplan-Meier survival curves and exact logistic regression for risk factors.\n\n          Results\n          \nOut of 680 past-treated VL patients, 37(5.4%) presented active skin lesions suspect of PKDL during the survey. Thirty-three of them underwent dermatological assessment, and 16 (2.4%) were ascertained as probable (2) or confirmed (14) PKDL. Survival analysis showed a 1.4% risk of PKDL within 2 years of VL treatment. All 16 had been previously treated with sodium stibogluconate (SSG) for their VL. In 5, treatment had not been completed (≤21 injections). Skin lesions developed after a median time interval of 23 months [interquartile range (IQR) 16–40]. We found a higher PKDL rate (29.4%) in those inadequately treated compared to those who received a full SSG course (2.0%). In the logistic regression model, unsupervised treatment [odds ratio (OR) = 8.58, 95% CI 1.21–374.77], and inadequate SSG treatment for VL in the past (OR = 11.68, 95% CI 2.71–45.47) were significantly associated with PKDL.\n\n          Conclusion\n          \nThe occurrence of PKDL after VL treatment in Nepal is low compared to neighboring countries. Supervised and adequate treatment of VL seems essential to reduce the risk of PKDL development and active surveillance for PKDL is needed.",
  "fullText": "Introduction Post-kala-azar dermal leishmaniasis (PKDL) is a late complication of visceral leishmaniasis (VL), which usually appears several months after treatment of a VL episode. PKDL is seen in areas where L.donovani is endemic i.e. in Asia (India, Nepal and Bangladesh) and in east Africa (Ethiopia, Kenya and Sudan [1]. In the Indian subcontinent, L.donovani is transmitted by the bite of a female sand fly of the Phlebotomus argentipes species, and the transmission cycle is considered to be anthroponotic with humans as the only known infection reservoir [2]. In Nepal, the standard treatment for VL with SSG was 20 mg/kg/day for 30 days without any upper limit recommended by WHO and drug was provided by the program to all government hospital in the endemic area. Due to associated toxicity and emerging drug resistance, SSG has been replaced in 2007 by Miltefosine 50 mg BID. PKDL is characterized by a spectrum of skin lesions ranging from hypo-pigmented macules, papules to nodules or combinations over the trunk and face that can be easily confused with other skin conditions such as vitiligo or leprosy [1], [3], [4]. So far, no convincing clinical predictors for PKDL have been identified [1] and its origin is believed to be multi-factorial and complex [5]. In Sudan, PKDL is more commonly reported in inadequately or irregularly treated VL cases [6]. PKDL is also sporadically reported in individuals without past history of VL [1], [7]. The incidence of PKDL varies from country to country for reasons that are not entirely clear. In Sudan, PKDL was described in 50–60% of cured VL patients within weeks to a few months after treatment [1]. In Bangladesh, a cross-sectional survey carried out in 2009 of patients who suffered from VL in 2002–2007 found 10% of them with active or past PKDL usually occurring within 36 months after VL treatment [8], [9]. In India, PKDL is reported in 5–10% of patients treated for VL usually after an interval of 2 to 4 years [3] and in 15–20% of PKDL cases there is no previous history of VL [3]. In Nepal VL is endemic in the south eastern Terai plains bordering the highly endemic districts of Bihar state of India, but systematic epidemiological data on PKDL are still lacking. PKDL patients have probably epidemiological importance in VL transmission as the lesions can harbour a large amount of Leishmania parasites, and as such could constitute a reservoir in the community capable of triggering a new epidemic [9]. As PKDL causes little or no clinical discomfort, and PKDL treatment with intramuscular SSG injections is long (3–4 months), painful and cumbersome, few patients seek treatment [1], [5], [10]–[13]. Since 2005, the government of Nepal is involved in a collaborative effort with India and Bangladesh to reduce VL incidence to less than 1 per 10 000 population by 2015 [14]. PKDL is not addressed so far in this elimination initiative, which poses a threat to its success [15]. Better information on the epidemiology and burden of PKDL might help the"
}