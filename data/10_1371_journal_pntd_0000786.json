{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000786",
  "doi": "10.1371/journal.pntd.0000786",
  "externalIds": [
    "pii:10-PNTD-RA-0827R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Nuclear Family A DNA Polymerase from Entamoeba histolytica Bypasses Thymine Glycol",
  "authors": [
    {
      "name": "Guillermo Pastor-Palacios",
      "first": "Guillermo",
      "last": "Pastor-Palacios",
      "affiliation": "Laboratorio Nacional de Genómica para la Biodiversidad, CINVESTAV, Irapuato, México"
    },
    {
      "name": "Elisa Azuara-Liceaga",
      "first": "Elisa",
      "last": "Azuara-Liceaga",
      "affiliation": "Posgrado en Ciencias Genómicas, Universidad Autónoma de la Ciudad de México, México Distrito Federal, México"
    },
    {
      "name": "Luis G. Brieba",
      "first": "Luis G.",
      "last": "Brieba",
      "affiliation": "Laboratorio Nacional de Genómica para la Biodiversidad, CINVESTAV, Irapuato, México"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-08",
  "dateAccepted": "2010-07-12",
  "dateReceived": "2010-01-19",
  "volume": "4",
  "number": "8",
  "pages": "e786",
  "tags": [
    "Biochemistry/Macromolecular Assemblies and Machines",
    "Biochemistry/Molecular Evolution",
    "Biochemistry/Replication and Repair",
    "Chemical Biology/Chemical Biology of the Cell"
  ],
  "abstract": "Background\n\nEukaryotic family A DNA polymerases are involved in mitochondrial DNA replication or translesion DNA synthesis. Here, we present evidence that the sole family A DNA polymerase from the parasite protozoan E. histolytica (EhDNApolA) localizes to the nucleus and that its biochemical properties indicate that this DNA polymerase may be involved in translesion DNA synthesis.\n\nMethodology and Results\n\nEhDNApolA is the sole family A DNA polymerase in E. histolytica. An in silico analysis places family A DNA polymerases from the genus Entamoeba in a separate branch of a family A DNA polymerases phylogenetic tree. Biochemical studies of a purified recombinant EhDNApolA demonstrated that this polymerase is active in primer elongation, is poorly processive, displays moderate strand displacement, and does not contain 3′–5′ exonuclease or editing activity. Importantly, EhDNApolA bypasses thymine glycol lesions with high fidelity, and confocal microscopy demonstrates that this polymerase is translocated into the nucleus. These data suggest a putative role of EhDNApolA in translesion DNA synthesis in E. histolytica.\n\nConclusion\n\nThis is the first report of the biochemical characterization of a DNA polymerase from E. histolytica. EhDNApolA is a family A DNA polymerase that is grouped into a new subfamily of DNA polymerases with translesion DNA synthesis capabilities similar to DNA polymerases from subfamily ν.",
  "fullText": "Introduction DNA replication and translesion DNA synthesis in eukaryotes is accomplished by a battery of DNA polymerases. For instance, the genome of Homo sapiens contains 15 DNA polymerases divided into four families: A, B, X, and Y according to their amino acid sequence homology [1]–[3]. Nuclear replicative DNA polymerases δ and ε · belong to family B, whereas DNA polymerases involved in translesion DNA synthesis are present in all four families. Entamoeba histolytica is a parasitic protozoa which causes amebic dysentery and liver abscess [4]. In comparison to eukaryotes that contain DNA in organelles like mitochondria or chloroplasts. E. histolytica is an early branching eukaryote in which its mitochondria diverged to form an organelle with no detectable DNA. This organelle is dubbed mitosome [5], [6], and although its function is not definitively established, experimental evidence suggests a role in sulfate activation [7] and oxygen detoxification [8]. Thus, the 24 Mbp genome of E. histolytica is exclusively nuclear and it encodes several putative DNA polymerases (Table S1) [9]. As an eukaryotic organism, the genome of E. histolytica is expected to be replicated by DNA polymerases δ and ε. Although a gene encoding DNA polymerase ε is not present in the current genome annotation of E. histolytica, a gene encoding DNA polymerase δ is present. E. histolytica contains homologs of Rev 1 and Rev 3 proteins, that compose the principal DNA polymerase involved in translesion synthesis of thymine dimers: DNA pol ζ [10], [11]. In addition, the genome of E. histolytica contains five DNA polymerases which share high sequence homology with DNA polymerases from autonomous replicating elements found in other protozoa and with the well-characterized DNA polymerase from bacteriophage φ29[12]. E. histolytica also contain one family A DNA polymerases in its genome. Family A DNA polymerases are modular enzymes consisting of three independent domains: a N-terminal 5′-3′ exonuclease domain, a 3′-5′ exonuclease domain, and a C-terminal polymerase domain [1], [13], [14]. Crystal structures of family A DNA polymerases revealed a modular organization of the polymerase domain and its division into three subdomains: palm, fingers, and thumb, which together form a cleft that binds the primer-template [15]. Family A DNA polymerases contain three conserved motifs: A, B, and C in the polymerization domain [16]. Motifs A and C are located at the palm subdomain and contain two carboxylates involved in the coordination of two metal ions involved in the nucleophilic attack of the incoming deoxynucleotide to the 3′ OH of the primer strand [13]. Motif B is located at the fingers subdomain and is involved in positioning the template strand into the polymerase active site [15]. In eukaryotes, family A polymerases are involved in the replication of mitochondrial and chloroplast genomes [17], [18]. The archetypical DNA polymerase in eukaryotes is DNA polymerase γ, which is the replicative mitochondrial DNA polymerase. Besides DNA polymerase γ, vertebrates contain two other family A DNA polymerases: DNA polymerase ν and DNA polymerase θ. In contrast to DNA polymerase γ, the localization of these polymerases is nuclear. Human"
}