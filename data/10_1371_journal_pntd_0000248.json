{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000248",
  "doi": "10.1371/journal.pntd.0000248",
  "externalIds": [
    "pii:07-PNTD-RA-0306R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Genomic-Based Approach Combining In Vivo Selection in Mice to Identify a Novel Virulence Gene in Leishmania",
  "authors": [
    {
      "name": "Wen-Wei Zhang",
      "first": "Wen-Wei",
      "last": "Zhang",
      "affiliation": "Department of Microbiology and Immunology, McGill University, Montreal, Quebec, Canada"
    },
    {
      "name": "Christopher S. Peacock",
      "first": "Christopher S.",
      "last": "Peacock",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Hinxton, Cambridgeshire, United Kingdom"
    },
    {
      "name": "Greg Matlashewski",
      "first": "Greg",
      "last": "Matlashewski",
      "affiliation": "Department of Microbiology and Immunology, McGill University, Montreal, Quebec, Canada"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-06",
  "dateAccepted": "2008-05-14",
  "dateReceived": "2007-11-29",
  "volume": "2",
  "number": "6",
  "pages": "e248",
  "tags": [
    "Genetics and Genomics/Comparative Genomics",
    "Genetics and Genomics/Functional Genomics",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases"
  ],
  "abstract": "Background\n    \nInfection with Leishmania results in a broad spectrum of pathologies where L. infantum and L. donovani cause fatal visceral leishmaniasis and L. major causes destructive cutaneous lesions. The identification and characterization of Leishmania virulence genes may define the genetic basis for these different pathologies.\n\n    Methods and Findings\n    \nComparison of the recently completed L. major and L. infantum genomes revealed a relatively small number of genes that are absent or present as pseudogenes in L. major and potentially encode proteins in L. infantum. To investigate the potential role of genetic differences between species in visceral infection, seven genes initially classified as absent in L. major but present in L. infantum were cloned from the closely related L. donovani genome and introduced into L. major. The transgenic L. major expressing the L. donovani genes were then introduced into BALB/c mice to select for parasites with increased virulence in the spleen to determine whether any of the L. donovani genes increased visceral infection levels. During the course of these experiments, one of the selected genes (LinJ32_V3.1040 (Li1040)) was reclassified as also present in the L. major genome. Interestingly, only the Li1040 gene significantly increased visceral infection in the L. major transfectants. The Li1040 gene encodes a protein containing a putative component of an endosomal protein sorting complex involved with protein transport.\n\n    Conclusions\n    \nThese observations demonstrate that the levels of expression and sequence variations in genes ubiquitously shared between Leishmania species have the potential to significantly influence virulence and tissue tropism.",
  "fullText": "Introduction Leishmania protozoa are transmitted by the bite of an infected sandfly and cause a spectrum of diseases ranging from self-healing cutaneous lesions to fatal visceral infection [1],[2]. There are an estimated 12 million cases in over 80 countries, with an annual incidence of 0.5 million new cases of the visceral leishmaniasis and 2.0 million cases of the cutaneous leishmaniasis [3]. More than 20 different Leishmania species can infect humans. Host health status and genetic background can influence the outcome of infection [4],[5] and HIV co-infection has dramatically increased the incidence of visceral leishmaniasis [5]. The major factor that determines the tropism and pathology of Leishmania infection is however the species of Leishmania [1],[2]. For example, L. donovani, L. infantum and L. chagasi are closely related members of the L. donovani complex that cause visceral leishmaniasis, which is fatal if not treated. L. major and L. tropica infections usually result in cutaneous lesions that remain localized at the site of the sandfly bite. L. (Viannia) braziliensis causes cutaneous leishmaniasis but can also migrate from the site of initial infection to the nasopharyngeal area resulting in highly destructive mucocutaneous leishmaniasis. The Leishmania genome projects are expected to help identify the genetic differences between these parasites which govern the pathology and tropism of infection caused by the different Leishmania species [6]–[8]. Our laboratory has previously identified the A2 gene family, which is present in Leishmania species that cause visceral infections including L. infantum and L. donovani but are not present in many of the Leishmania species that cause cutaneous infections including L. major and L. tropica [9],[10]. A2 proteins have been shown to be essential for visceral infection with L. donovani in BALB/c mice [11],[12]. Cross species transfection of the A2 gene from L. donovani into L. major rendered L. major more virulent in visceral organs but less virulent at cutaneous sites, phenotypes typical of L. donovani [12],[13]. This demonstrated that species-specific genes can play a role in virulence and the pathology of Leishmania infection and provided the justification for experimentally studying species-specific genes identified through sequencing of the Leishmania genomes. We used the genetic information from the completion of the L. major, L. infantum, and L. braziliensis genomes [6],[7] to identify genes that could potentially influence the pathology caused by these Leishmania species. Remarkably, out of more than 8000 genes within the Leishmania genome, only about 25 L. infantum-specific genes have been identified which are not present or are pseudogenes in L. major and L. braziliensis, and most encode for proteins with no known function [7]. Using the L. infantum genome sequence database, we have cloned 7 L. donovani ortholog genes that were absent or were pseudogenes in L. major and introduced these genes into L. major. The L. donovani gene containing transgenic L. major parasites were introduced into BALB/c mice to determine whether any of these genes increased virulence in visceral sites including the liver and spleen. During the course of this study, one of these selected L. infantum genes,"
}