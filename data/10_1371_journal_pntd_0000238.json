{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000238",
  "doi": "10.1371/journal.pntd.0000238",
  "externalIds": [
    "pii:07-PNTD-RA-0310R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Trypanosoma brucei rhodesiense Transmitted by a Single Tsetse Fly Bite in Vervet Monkeys as a Model of Human African Trypanosomiasis",
  "authors": [
    {
      "name": "John K. Thuita",
      "first": "John K.",
      "last": "Thuita",
      "affiliation": "Trypanosomiasis Research Centre, (KARI-TRC), Kikuyu, Kenya"
    },
    {
      "name": "John M. Kagira",
      "first": "John M.",
      "last": "Kagira",
      "affiliation": "Trypanosomiasis Research Centre, (KARI-TRC), Kikuyu, Kenya"
    },
    {
      "name": "David Mwangangi",
      "first": "David",
      "last": "Mwangangi",
      "affiliation": "Trypanosomiasis Research Centre, (KARI-TRC), Kikuyu, Kenya"
    },
    {
      "name": "Enock Matovu",
      "first": "Enock",
      "last": "Matovu",
      "affiliation": "Faculty of Veterinary Medicine, Makerere University, Kampala, Uganda"
    },
    {
      "name": "C. M. R. Turner",
      "first": "C. M. R.",
      "last": "Turner",
      "affiliation": "Division of Infection and Immunity, Institute of Biomedical and Life Sciences, and Wellcome Centre for Molecular Parasitology, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Daniel Masiga",
      "first": "Daniel",
      "last": "Masiga",
      "affiliation": "Molecular Biology and Biotechnology Department, International Centre of Insect Physiology and Ecology, Nairobi, Kenya; Department of Biochemistry and Biotechnology, Kenyatta University, Nairobi, Kenya"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-05",
  "dateAccepted": "2008-04-17",
  "dateReceived": "2007-11-29",
  "volume": "2",
  "number": "5",
  "pages": "e238",
  "tags": [
    "Infectious Diseases",
    "Infectious Diseases/Infectious Diseases of the Nervous System",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Pathology/Hematology"
  ],
  "abstract": "We have investigated the pathogenicity of tsetse (Glossina pallidipes)-transmitted cloned strains of Trypanosoma brucei rhodesiense in vervet monkeys. Tsetse flies were confirmed to have mature trypanosome infections by xenodiagnosis, after which nine monkeys were infected via the bite of a single infected fly. Chancres developed in five of the nine (55.6%) monkeys within 4 to 8 days post infection (dpi). All nine individuals were successfully infected, with a median pre-patent period of 4 (range = 4–10) days, indicating that trypanosomes migrated from the site of fly bite to the systemic circulation rapidly and independently of the development of the chancre. The time lag to detection of parasites in cerebrospinal fluid (CSF) was a median 16 (range = 8–40) days, marking the onset of central nervous system (CNS, late) stage disease. Subsequently, CSF white cell numbers increased above the pre-infection median count of 2 (range = 0–9) cells/µl, with a positive linear association between their numbers and that of CSF trypanosomes. Haematological changes showed that the monkeys experienced an early microcytic-hypochromic anaemia and severe progressive thrombocytopaenia. Despite a 3-fold increase in granulocyte numbers by 4 dpi, leucopaenia occurred early (8 dpi) in the monkey infection, determined mainly by reductions in lymphocyte numbers. Terminally, leucocytosis was observed in three of nine (33%) individuals. The duration of infection was a median of 68 (range = 22–120) days. Strain and individual differences were observed in the severity of the clinical and clinical pathology findings, with two strains (KETRI 3741 and 3801) producing a more acute disease than the other two (KETRI 3804 and 3928). The study shows that the fly-transmitted model accurately mimics the human disease and is therefore a suitable gateway to understanding human African trypanosomiasis (HAT; sleeping sickness).",
  "fullText": "Introduction In human African trypanosomiasis (HAT), the use of animal models has contributed enormously to what is currently known about the relationships between disease duration, parasite invasion of different body systems and the potential of resultant host clinical and biological changes as diagnostic and disease staging markers. Several host-parasite model systems have been developed, based on infection of various hosts with the livestock pathogen Trypanosoma brucei brucei and to a lesser extent the human pathogens T. b. rhodesiense and T. b. gambiense. Characterisation of these HAT models shows that the disease occurs in two stages irrespective of host: an early haemo-lymphatic trypanosome proliferation, and a late central nervous system (CNS) infection, indicating that the basic pattern is similar to the disease in humans. This is evidenced by demonstration of trypanosomes, first in the haemo-lymphatic system and later in the CNS of the mouse model with subsequent cerebral pathology [1],[2]. Models based on larger mammals such as the chimpanzee T. b. rhodesiense model [3], the vervet monkey T. b. rhodesiense model [4] and the sheep T. b. brucei model [5], also follow a similar two-stage disease pattern. These, unlike rodents, allow collection of cerebrospinal fluid (CSF) that has been used to demonstrate elevation of white cell counts and total protein levels as indicators of CNS stage disease [6]. The KETRI vervet monkey model has been reported to closely mimic HAT clinically, immunologically and pathologically [4], [7]–[9]. However, these previous studies were limited in scope in three important ways. Firstly, infections were initiated by intravenous inoculation (syringe) of bloodstream form trypanosomes as opposed to the natural human disease, which begins via the bite of a tsetse fly, with the intra-dermal inoculation of metacyclic trypanosomes. The difference between the two routes of infection has the potential to affect trypanosome virulence and subsequent disease pathogenesis that has been little explored to date. Second, disease progression has been monitored mainly in terms of clinical symptoms, gross pathology, histo-pathology and antibody responses [4],[7], with little reference to the development of blood pathology. Third, only a single strain of trypanosomes, KETRI 2537 [9], has been adequately characterised even though trypanosome strains vary in the severity of pathogenesis and virulence [10]–[11]. The present study was designed to address these limitations and thus improve further the potential utility of the model and our understanding of pathogenesis in trypanosome infections. We characterised the pathogenicity of T. b. rhodesiense in vervet monkeys, following infection from the bite of a single tsetse fly, hence mimicking the natural route of infection in man. This allowed us to measure a range of parameters in blood and cerebrospinal fluid (CSF) including several that had not previously been studied. It was thus possible to measure the development of clinical complications of HAT infections, such as anaemia, more precisely. We describe the development of clinical pathology resulting from infection with four cloned strains of T. b. rhodesiense. Materials and Methods Ethics This study was undertaken in adherence to experimental guidelines and procedures approved by the Institutional Animal"
}