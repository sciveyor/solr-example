{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001623",
  "doi": "10.1371/journal.pntd.0001623",
  "externalIds": [
    "pii:PNTD-D-12-00035"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Innate Immune Response to Rift Valley Fever Virus in Goats",
  "authors": [
    {
      "name": "Charles K. Nfon",
      "first": "Charles K.",
      "last": "Nfon",
      "affiliation": "National Center for Foreign Animal Disease, Canadian Food Inspection Agency, Winnipeg, Manitoba, Canada"
    },
    {
      "name": "Peter Marszal",
      "first": "Peter",
      "last": "Marszal",
      "affiliation": "National Center for Foreign Animal Disease, Canadian Food Inspection Agency, Winnipeg, Manitoba, Canada"
    },
    {
      "name": "Shunzhen Zhang",
      "first": "Shunzhen",
      "last": "Zhang",
      "affiliation": "National Center for Foreign Animal Disease, Canadian Food Inspection Agency, Winnipeg, Manitoba, Canada"
    },
    {
      "name": "Hana M. Weingartl",
      "first": "Hana M.",
      "last": "Weingartl",
      "affiliation": "National Center for Foreign Animal Disease, Canadian Food Inspection Agency, Winnipeg, Manitoba, Canada; Department of Microbiology, University of Manitoba, Winnipeg, Manitoba, Canada"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-04",
  "dateAccepted": "2012-03-06",
  "dateReceived": "2012-01-06",
  "volume": "6",
  "number": "4",
  "pages": "e1623",
  "tags": [
    "Biology",
    "Hemorrhagic fevers",
    "Immune response",
    "Immunology",
    "Immunomodulation",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Rift Valley fever",
    "Veterinary diseases",
    "Veterinary immunology",
    "Veterinary medicine",
    "Veterinary science",
    "Zoonotic diseases"
  ],
  "abstract": "Rift Valley fever (RVF), a re-emerging mosquito-borne disease of ruminants and man, was endemic in Africa but spread to Saudi Arabia and Yemen, meaning it could spread even further. Little is known about innate and cell-mediated immunity to RVF virus (RVFV) in ruminants, which is knowledge required for adequate vaccine trials. We therefore studied these aspects in experimentally infected goats. We also compared RVFV grown in an insect cell-line and that grown in a mammalian cell-line for differences in the course of infection. Goats developed viremia one day post infection (DPI), which lasted three to four days and some goats had transient fever coinciding with peak viremia. Up to 4% of peripheral blood mononuclear cells (PBMCs) were positive for RVFV. Monocytes and dendritic cells in PBMCs declined possibly from being directly infected with virus as suggested by in vitro exposure. Infected goats produced serum IFN-γ, IL-12 and other proinflammatory cytokines but not IFN-α. Despite the lack of IFN-α, innate immunity via the IL-12 to IFN-γ circuit possibly contributed to early protection against RVFV since neutralising antibodies were detected after viremia had cleared. The course of infection with insect cell-derived RVFV (IN-RVFV) appeared to be different from mammalian cell-derived RVFV (MAM-RVFV), with the former attaining peak viremia faster, inducing fever and profoundly affecting specific immune cell subpopulations. This indicated possible differences in infections of ruminants acquired from mosquito bites relative to those due to contact with infectious material from other animals. These differences need to be considered when testing RVF vaccines in laboratory settings.",
  "fullText": "Introduction Rift Valley fever (RVF) is a disease of ruminants and man caused by the mosquito transmitted Rift Valley fever virus (RVFV), genus Phlebovirus, family Bunyaviridae [1]. This spherical shaped, enveloped virus has a negative-sense single-stranded RNA genome made up of 3 segments. The large (L) segment encodes for the viral RNA-dependent RNA polymerase while the medium (M) segment encodes the external glycoproteins (Gn and Gc) and the non-structural protein (NSm). The small (S) segment is ambisense, coding for the nucleoprotein (N) in the antigenomic sense and the non-structural protein (NSs) in the genomic direction [2]. RVF outbreaks are frequently reported in Sub-Saharan African countries where the disease is endemic. These include Kenya, Tanzania, Somalia, South Africa, Sudan, Uganda, Madagascar and Senegal. However, outbreaks were also reported in Egypt, Yemen and Saudi Arabia indicating an expanding range for this disease [3]. RVFV is transmitted primarily by Aedes and Culex mosquitoes, with the latter serving as a magnifying host during outbreaks [2] . In addition to infectious mosquito bites, humans can also acquire RVF through contact with blood of diseased animals [4], [5]. Outbreaks of RVF in endemic countries usually coincide with conditions such as periods of heavy rainfall and flooding, which favour heavy breeding of mosquito vectors [6], [7]. RVF is characterized by large abortion storms and close to 100% mortality in newborn sheep, goats and cattle resulting in severe adverse socio-economic effects [8]. These animals carry high titres of virus (6 log10 to 8 log10 PFU/mL) in their blood resulting in fever, inappetence, nasal discharges and diarrhoea [3]. However, adult sheep, goats and cattle are more resistant to RVFV and experience lower mortality rates between 10–30% [3]. Human RVF usually manifests as a mild and self-limiting fever, but in some patients may progress to a haemorrhagic fever, neurological disorder or blindness [2], [3]. Innate and adaptive immune responses contribute to the clearance of RVFV in infected animals [3], [9]. Evidence for the role of innate immunity is mostly based on results from experimental models [9]–[12]. Interferon alpha (IFN-α) is believed to protect against RVFV because monkeys that secreted this cytokine within 12 h of being challenged with RVFV did not develop disease [11]. However, RVFV NSs protein inhibits IFN-α and IFN-β production/induction, thereby enabling early replication and viremia [12]–[14]. Anti-RVFV antibodies are detectable 4 to 8 days following infection [15]–[17]. Neutralising antibodies are believed to be crucial for the protection of infected animals [2], [11]. Although ruminants have since been recognized as the primary animal hosts, there is little knowledge of the pathogenesis of RVFV in goats. In 2–3 months old goats experimentally infected with RVFV, viremia was detected 24 h post subcutaneous inoculation and lasted for 3 days [18]. These goats also had a mild transient increase in rectal temperature. Mild fever was equally observed in goats inoculated by inhalation and virus could be recovered from throat washes 2 days after inoculation [18]. In addition, virus was apparently transmitted to contact goats. Clinical signs varied in severity depending"
}