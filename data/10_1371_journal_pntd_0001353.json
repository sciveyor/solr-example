{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001353",
  "doi": "10.1371/journal.pntd.0001353",
  "externalIds": [
    "pii:PNTD-D-11-00361"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Alternative Oxidase Mediates Pathogen Resistance in Paracoccidioides brasiliensis Infection",
  "authors": [
    {
      "name": "Orville Hernández Ruiz",
      "first": "Orville",
      "last": "Hernández Ruiz",
      "affiliation": "Instituto de Biología, Universidad de Antioquia, Medellín, Colombia; Cellular and Molecular Biology Unit, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia; Facultad de Ciencias de la Salud, Institución Universitaria Colegio Mayor de Antioquia, Medellín, Colombia"
    },
    {
      "name": "Angel Gonzalez",
      "first": "Angel",
      "last": "Gonzalez",
      "affiliation": "Medical and Experimental Mycology Group, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia; Escuela de Microbiologia, Universidad de Antioquia, Medellín, Colombia"
    },
    {
      "name": "Agostinho J. Almeida",
      "first": "Agostinho J.",
      "last": "Almeida",
      "affiliation": "Cellular and Molecular Biology Unit, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia"
    },
    {
      "name": "Diana Tamayo",
      "first": "Diana",
      "last": "Tamayo",
      "affiliation": "Cellular and Molecular Biology Unit, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia"
    },
    {
      "name": "Ana Maria Garcia",
      "first": "Ana Maria",
      "last": "Garcia",
      "affiliation": "Cellular and Molecular Biology Unit, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia"
    },
    {
      "name": "Angela Restrepo",
      "first": "Angela",
      "last": "Restrepo",
      "affiliation": "Medical and Experimental Mycology Group, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia"
    },
    {
      "name": "Juan G. McEwen",
      "first": "Juan G.",
      "last": "McEwen",
      "affiliation": "Cellular and Molecular Biology Unit, Corporación para Investigaciones Biológicas (CIB), Medellín, Colombia; Facultad de Medicina, Universidad de Antioquia, Medellín, Colombia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-10",
  "dateAccepted": "2011-08-25",
  "dateReceived": "2011-04-17",
  "volume": "5",
  "number": "10",
  "pages": "e1353",
  "tags": [
    "Biology",
    "Cell Biology",
    "Cellular stress responses",
    "Gene expression",
    "Gene function",
    "Genetics",
    "Genetics and Genomics",
    "Immune defense",
    "Immunity",
    "Microbiology",
    "Molecular Biology",
    "Molecular cell biology",
    "Mycology"
  ],
  "abstract": "Background\n          \nParacoccidioides brasiliensis is a human thermal dimorphic pathogenic fungus. Survival of P. brasiliensis inside the host depends on the adaptation of this fungal pathogen to different conditions, namely oxidative stress imposed by immune cells.\n\n          Aims and Methodology\n          \nIn this study, we evaluated the role of alternative oxidase (AOX), an enzyme involved in the intracellular redox balancing, during host-P. brasiliensis interaction. We generated a mitotically stable P. brasiliensis AOX (PbAOX) antisense RNA (aRNA) strain with a 70% reduction in gene expression. We evaluated the relevance of PbAOX during interaction of conidia and yeast cells with IFN-γ activated alveolar macrophages and in a mouse model of infection. Additionally, we determined the fungal cell's viability and PbAOX in the presence of H2O2.\n\n          Results\n          \nInteraction with IFN-γ activated alveolar macrophages induced higher levels of PbAOX gene expression in PbWt conidia than PbWt yeast cells. PbAOX-aRNA conidia and yeast cells had decreased viability after interaction with macrophages. Moreover, in a mouse model of infection, we showed that absence of wild-type levels of PbAOX in P. brasiliensis results in a reduced fungal burden in lungs at weeks 8 and 24 post-challenge and an increased survival rate. In the presence of H2O2, we observed that PbWt yeast cells increased PbAOX expression and presented a higher viability in comparison with PbAOX-aRNA yeast cells.\n\n          Conclusions\n          \nThese data further support the hypothesis that PbAOX is important in the fungal defense against oxidative stress imposed by immune cells and is relevant in the virulence of P. brasiliensis.",
  "fullText": "Introduction An essential event during cell growth and host invasion of pathogenic fungi is to avoid the toxic effects of reactive oxygen species (ROS), such as superoxide (O2), hydrogen peroxide (H2O2) and hydroxyl (OH−) radicals. ROS can either be produced by the fungal mitochondrial electron transport chain or derive from host immune cells (e.g., macrophages) during host-pathogen interaction [1], [2], [3]. Independently of its origin, these oxidizing agents may ultimately alter the bioenergetic status of the cell and affect essential metabolic pathways and/or its growth within host tissues, representing a toxic stimulus that decreases fungal survival [4], [5]. Human pathogenic fungi such as Paracoccidioides brasiliensis, Histoplasma capsulatum, Candida albicans, among others, possess a defense mechanism against ROS that attempts to modulate the oxidative attack. These mechanisms include some enzymes such as superoxide dismutase which catalyze dismutation of O2− producing H2O2 and O2 and catalases involved in decomposition of H2O2 to H2O [6]. In addition, it has been demonstrated that Aspergillus fumigatus produces an alternative oxidase (AOX) that contributes to both reduction of ROS generated by mitochondria and regulation of energy production and metabolism [7]. Recently, Magnani et al (2007) suggested that AOX is required for the A. fumigatus pathogenicity, mainly for the survival of A. fumigatus conidia during host infection and reduction of ROS generated by activated macrophages [1]. P. brasiliensis, the causal agent of Paracoccidioidomycosis (PCM) [8], is a thermal dimorphic fungus that at environmental temperature grows as a mold producing conidia (the infectious particle). Once inhaled by the host, these fungal cells reach the lung alveoli were they interact with epithelial cells and alveolar macrophages [9]. At 37°C the transition to the parasitic yeast form occurs; however, disease development depends on both the virulence of the fungal strain and host-related factors [10]. Thus, fungal survival inside the host cells depends on the capacity to respond to external factors, ranging from temperature changes to oxidative stress. In fact, as a facultative intracellular pathogen, P. brasiliensis can persist within the macrophage phagolysosomes due to the existence of defense mechanisms that allows the fungus to survive under nutritionally poor environments and in the presence of ROS [11], [12], [13]. Also, P. brasiliensis has been shown to express a powerful antioxidant defense system in the presence of ROS-mediated oxidative stress [14]. Recently, Maricato and co-workers (2010) showed that under oxidative stress induced by H2O2 and in the presence of activated macrophages, P. brasiliensis yeast increases the expression of the flavoprotein monoxigenase family, an important group of antioxidative enzymes [2]. In addition, the analysis of the mitochondrial function of P. brasiliensis yeasts revealed the existence of an alternative respiratory chain (AOX), previously demonstrated to play an important role in the control of ROS and other oxidative molecules [7], [15], [16]. Campos et al (2005) described in P. brasiliensis several genes in an expressed sequence tag (EST) database encoding proteins involved in antioxidant defense, such as catalase, superoxide dismutase, peroxiredoxin, and cytochrome c peroxidase, among others [17]. More recent studies in P. brasiliensis have"
}