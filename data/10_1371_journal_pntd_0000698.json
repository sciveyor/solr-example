{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000698",
  "doi": "10.1371/journal.pntd.0000698",
  "externalIds": [
    "pii:09-PNTD-RA-0603R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Human Complement Regulators C4b-Binding Protein and C1 Esterase Inhibitor Interact with a Novel Outer Surface Protein of Borrelia recurrentis",
  "authors": [
    {
      "name": "Sonja Grosskinsky",
      "first": "Sonja",
      "last": "Grosskinsky",
      "affiliation": "Infectious Immunology Group, Institute for Immunology, University of Heidelberg, Heidelberg, Germany"
    },
    {
      "name": "Melanie Schott",
      "first": "Melanie",
      "last": "Schott",
      "affiliation": "Infectious Immunology Group, Institute for Immunology, University of Heidelberg, Heidelberg, Germany"
    },
    {
      "name": "Christiane Brenner",
      "first": "Christiane",
      "last": "Brenner",
      "affiliation": "Infectious Immunology Group, Institute for Immunology, University of Heidelberg, Heidelberg, Germany"
    },
    {
      "name": "Sally J. Cutler",
      "first": "Sally J.",
      "last": "Cutler",
      "affiliation": "School of Health and Bioscience, University of East London, Stratford, London, United Kingdom"
    },
    {
      "name": "Markus M. Simon",
      "first": "Markus M.",
      "last": "Simon",
      "affiliation": "Infectious Immunology Group, Institute for Immunology, University of Heidelberg, Heidelberg, Germany"
    },
    {
      "name": "Reinhard Wallich",
      "first": "Reinhard",
      "last": "Wallich",
      "affiliation": "Infectious Immunology Group, Institute for Immunology, University of Heidelberg, Heidelberg, Germany"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-06",
  "dateAccepted": "2010-04-06",
  "dateReceived": "2009-10-29",
  "volume": "4",
  "number": "6",
  "pages": "e698",
  "tags": [
    "Immunology/Immunity to Infections",
    "Immunology/Innate Immunity",
    "Infectious Diseases/Bacterial Infections",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "The spirochete Borrelia recurrentis is the causal agent of louse-borne relapsing fever and is transmitted to humans by the infected body louse Pediculus humanus. We have recently demonstrated that the B. recurrentis surface receptor, HcpA, specifically binds factor H, the regulator of the alternative pathway of complement activation, thereby inhibiting complement mediated bacteriolysis. Here, we show that B. recurrentis spirochetes express another potential outer membrane lipoprotein, termed CihC, and acquire C4b-binding protein (C4bp) and human C1 esterase inhibitor (C1-Inh), the major inhibitors of the classical and lectin pathway of complement activation. A highly homologous receptor for C4bp was also found in the African tick-borne relapsing fever spirochete B. duttonii. Upon its binding to B. recurrentis or recombinant CihC, C4bp retains its functional potential, i.e. facilitating the factor I-mediated degradation of C4b. The additional finding that ectopic expression of CihC in serum sensitive B. burgdorferi significantly increased spirochetal resistance against human complement suggests this receptor to substantially contribute, together with other known strategies, to immune evasion of B. recurrentis.",
  "fullText": "Introduction B. recurrentis, the causative agent of louse-borne relapsing fever is transmitted to humans by contamination of abraded skin with either hemolymph from crushed, infected lice (Pediculus humanus humanus) or excreted feces thereof [1], [2]. The last century has seen multiple epidemics of louse-borne relapsing fever in Europe, with high mortality rates of up to 40%. Louse-borne relapsing fever has been epidemic in Africa throughout the 20th century with foci persisting in the highlands of Ethiopia [3], [4]. Clinically, louse-borne relapsing fever is characterized by a 5- to 7-day incubation period followed by one to five relapses of fever, and spirochetemia [5], [6]. Spontaneous mortality remains as high as 2–4% despite antibiotics, with patients suffering from distinctive hemorrhagic syndrome and/or Jarish-Herxheimer reactions [7]. To survive in human tissues, including blood, B. recurrentis has to escape innate and adaptive immune responses. Complement is a major component of first line host defense with the potential to eliminate microbes. However, pathogens have evolved strategies to evade complement-mediated lysis, either indirectly, by binding host-derived regulators to their surface or directly, by expressing endogenous complement inhibitors [8], [9]. In fact, we and others have recently demonstrated that tick- and louse-borne pathogens, i.e. B. hermsii and B. recurrentis, specifically bind complement regulatory proteins, i.e. CFH and CFHR-1, via their outer surface lipoproteins FhbA, BhCRASP-1 and HcpA, respectively [10]–[14]. Surface bound CFH was shown to interfere with the alternative complement pathway by inhibiting complement activation via accelerating the decay of the C3 convertase and inactivating newly formed C3b [15], [16]. However, complement may also attack pathogenic bacteria via the classical pathway, i.e. by interacting with previously bound antibodies, resulting in deposition of the membrane attack complex on the surface of bacteria and their final death [17]. The classical pathway is initiated by the binding and activation of the C1 complex, consisting of C1q, C1r and C1s. C1q can bind to clustered IgG and IgM bound to the surface of bacteria, and also directly to many bacteria through lipoteichoic acids or other structures [18], [19]. When C1q binds, its associated proteases, C1r and C1s, become activated and form the activated C1 complex, which cleaves C4 and C2 to generate the C3 convertase. The lectin pathway is initiated when mannose-binding lectin (MBL) or ficolins bind carbohydrates on the surface of a microbe [20]. A key endogenous regulator of the classical and lectin pathway is serum-derived C4b-binding protein (C4bp). C4bp is a cofactor in factor I-mediated cleavage of C4b to C4d and interferes with the assembly and decay of the C3-convertase (C4bC2a) of the classical and lectin pathway [21], [22]. It was recently shown that acquisition of the regulators CFH and C4bp on the surface of B. recurrentis and B. duttonii contributes to serum resistance in vitro [17]. However, the respective receptors on the spirochetal surface have not been identified. It was thus the aim of the present study to identify and characterize the putative receptor for C4bp of B. recurrentis and B. duttonii. Here, we show for the first"
}