{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000687",
  "doi": "10.1371/journal.pntd.0000687",
  "externalIds": [
    "pii:09-PNTD-RA-0102R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Lineage Analysis of Circulating Trypanosoma cruzi Parasites and Their Association with Clinical Forms of Chagas Disease in Bolivia",
  "authors": [
    {
      "name": "Ramona del Puerto",
      "first": "Ramona",
      "last": "del Puerto",
      "affiliation": "Department of Immunogenetics, Institute of Tropical Medicine (NEKKEN), Nagasaki University, Nagasaki, Japan"
    },
    {
      "name": "Juan Eiki Nishizawa",
      "first": "Juan Eiki",
      "last": "Nishizawa",
      "affiliation": "Clinica Siraní, Santa Cruz, Bolivia"
    },
    {
      "name": "Mihoko Kikuchi",
      "first": "Mihoko",
      "last": "Kikuchi",
      "affiliation": "Center for International Collaboration Research, Nagasaki University, Nagasaki, Japan"
    },
    {
      "name": "Naomi Iihoshi",
      "first": "Naomi",
      "last": "Iihoshi",
      "affiliation": "Centro Nacional de Enfermedades Tropicales, Santa Cruz, Bolivia"
    },
    {
      "name": "Yelin Roca",
      "first": "Yelin",
      "last": "Roca",
      "affiliation": "Centro Nacional de Enfermedades Tropicales, Santa Cruz, Bolivia"
    },
    {
      "name": "Cinthia Avilas",
      "first": "Cinthia",
      "last": "Avilas",
      "affiliation": "Centro Nacional de Enfermedades Tropicales, Santa Cruz, Bolivia"
    },
    {
      "name": "Alberto Gianella",
      "first": "Alberto",
      "last": "Gianella",
      "affiliation": "Centro Nacional de Enfermedades Tropicales, Santa Cruz, Bolivia"
    },
    {
      "name": "Javier Lora",
      "first": "Javier",
      "last": "Lora",
      "affiliation": "Centro Nacional de Enfermedades Tropicales, Santa Cruz, Bolivia"
    },
    {
      "name": "Freddy Udalrico Gutierrez Velarde",
      "first": "Freddy Udalrico",
      "last": "Gutierrez Velarde",
      "affiliation": "Hospital Universitario Japonés, Santa Cruz, Bolivia"
    },
    {
      "name": "Luis Alberto Renjel",
      "first": "Luis Alberto",
      "last": "Renjel",
      "affiliation": "Centro de Enfermedades Cardiovasculares (BIOCOR), Santa Cruz, Bolivia"
    },
    {
      "name": "Sachio Miura",
      "first": "Sachio",
      "last": "Miura",
      "affiliation": "Department of Tropical Medicine and Parasitology, School of Medicine, Keio University, Tokyo, Japan"
    },
    {
      "name": "Hiroo Higo",
      "first": "Hiroo",
      "last": "Higo",
      "affiliation": "Department of Parasitology, Graduate School of Medical Sciences, Kyushu University, Fukuoka, Japan"
    },
    {
      "name": "Norihiro Komiya",
      "first": "Norihiro",
      "last": "Komiya",
      "affiliation": "Department of Cardiovascular Medicine, Nagasaki University Graduate School of Biomedical Science, Nagasaki, Japan"
    },
    {
      "name": "Koji Maemura",
      "first": "Koji",
      "last": "Maemura",
      "affiliation": "Department of Cardiovascular Medicine, Nagasaki University Graduate School of Biomedical Science, Nagasaki, Japan"
    },
    {
      "name": "Kenji Hirayama",
      "first": "Kenji",
      "last": "Hirayama",
      "affiliation": "Department of Immunogenetics, Institute of Tropical Medicine (NEKKEN), Nagasaki University, Nagasaki, Japan; Global COE Program, Nagasaki University, Nagasaki, Japan"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-05",
  "dateAccepted": "2010-03-28",
  "dateReceived": "2009-03-11",
  "volume": "4",
  "number": "5",
  "pages": "e687",
  "tags": [
    "Cardiovascular Disorders/Heart Failure",
    "Cardiovascular Disorders/Myopathies",
    "Immunology/Autoimmunity",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Background\n\nThe causative agent of Chagas disease, Trypanosoma cruzi, is divided into 6 Discrete Typing Units (DTU): Tc I, IIa, IIb, IIc, IId and IIe. In order to assess the relative pathogenicities of different DTUs, blood samples from three different clinical groups of chronic Chagas disease patients (indeterminate, cardiac, megacolon) from Bolivia were analyzed for their circulating parasites lineages using minicircle kinetoplast DNA polymorphism.\n\nMethods and Findings\n\nBetween 2000 and 2007, patients sent to the Centro Nacional de Enfermedades Tropicales for diagnosis of Chagas from clinics and hospitals in Santa Cruz, Bolivia, were assessed by serology, cardiology and gastro-intestinal examinations. Additionally, patients who underwent colonectomies due to Chagasic magacolon at the Hospital Universitario Japonés were also included. A total of 306 chronic Chagas patients were defined by their clinical types (81 with cardiopathy, 150 without cardiopathy, 100 with megacolon, 144 without megacolon, 164 with cardiopathy or megacolon, 73 indeterminate and 17 cases with both cardiopathy and megacolon). DNA was extracted from 10 ml of peripheral venous blood for PCR analysis. The kinetoplast minicircle DNA (kDNA) was amplified from 196 out of 306 samples (64.1%), of which 104 (53.3%) were Tc IId, 4 (2.0%) Tc I, 7 (3.6%) Tc IIb, 1 (0.5%) Tc IIe, 26 (13.3%) Tc I/IId, 1 (0.5%) Tc I/IIb/IId, 2 (1.0%) Tc IIb/d and 51 (25.9%) were unidentified. Of the 133 Tc IId samples, three different kDNA hypervariable region patterns were detected; Mn (49.6%), TPK like (48.9%) and Bug-like (1.5%). There was no significant association between Tc types and clinical manifestations of disease.\n\nConclusions\n\nNone of the identified lineages or sublineages was significantly associated with any particular clinical manifestations in the chronic Chagas patients in Bolivia.",
  "fullText": "Introduction Despite concerted efforts to control Chagas disease in Bolivia, the prevalence of Trypanosoma cruzi remains high [1]. For example, one study has shown that in the village of Tarija, South Bolivia, the prevalence in pregnant women can reach up to 33.9% [2]. Furthermore, the vector infection rate of triatomines in the southern zone of Cochabamba, in the center of Bolivia, was found to be 79%, which translates to a very high infection risk for children in this area [3]. In addition, seroprevalence studies in Santa Cruz, in the east of Bolivia, recorded parasite exposure positive rates of 50% and concluded that there was a significant risk of infection via blood transfusion [4], [5], [6]. The chronic stage of Chagas disease may be categorized into three major clinical forms; those involving cardiopathy, digestive tract pathology (such as megacolon), and indeterminate (asymptomatic) forms [7], [8]. There appears to be geographical variation in the development of these clinical forms in Latin America, but in Bolivia all three clinical forms are observed [8], [9], [10], [11], [12]. This variation in pathological manifestation has been reported to be related to differences in the efficiency of the human host's immune response, such as ability to control parasitemia, the strength of inflammatory reactions, and the induction of autoimmune responses [13], [14], [15]. However, the host factors underlying the mechanisms of the outcome of the disease remain undetermined. Another possible factor that determines the clinical course of Chagas is the pathogenicity of T. cruzi itself. The genetic polymorphism of T. cruzi population may be related to its variability in pathogenicity. As there is no sexual stage in the parasite's life cycle (although some genetic exchange might occur) [16], the majority of parasites belong to clonal lineages. Population genetic studies of T. cruzi, carried out by zymodeme, ribosomal RNA, miniexon, and genomic DNA analyses, have revealed two primary phylogenetic lineages [17], [18], [19]. Within the last few years there has been an attempt to standardize the nomenclature. Trypanosoma cruzi is divided into 6 Discrete Typing Units (DTU) TcI, TcIIa, TcIIb, TcIIc, TcIId and TcIIe [20], [21], [22], [23], [24], [25]. Geographical and epidemiological studies showed that the distribution of TcI and TcII vary geographically. TcI is prevalent in the northern part of Brazil, Central and North America [26], [27], [28] while TcII is found predominantly in Southern cone countries of Latin America [29], [30]. In Bolivia, the TcIId was found to be the most common [30]. Furthermore, Virreira et al, 2006 [31] further sub-divided the TcIId based on patterns of minicircle Hypervariable Region (HVR), classifying three subdivisions as Mn-like, Bug-like and TPK-like. In this study, in order to elucidate parasite factors that might influence the outcome of clinical disease, we have systematically classified our Bolivian patients according to their Chagas-related clinical symptoms (indeterminate, cardiopathy and megacolon) and determined the lineages of circulating parasites in their blood using PCR amplification. Materials and Methods Patients Three hundred and six outpatients with chronic Chagas disease (144 men and 162 women, mean"
}