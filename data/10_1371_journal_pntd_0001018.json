{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001018",
  "doi": "10.1371/journal.pntd.0001018",
  "externalIds": [
    "pii:PNTD-D-10-00265"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Snakebite Mortality in India: A Nationally Representative Mortality Survey",
  "authors": [
    {
      "name": "Bijayeeni Mohapatra",
      "first": "Bijayeeni",
      "last": "Mohapatra",
      "affiliation": "Shri Ramachandra Bhanj Medical College, Cuttack, Orissa, India"
    },
    {
      "name": "David A. Warrell",
      "first": "David A.",
      "last": "Warrell",
      "affiliation": "Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom; Australian Venom Research Unit, University of Melbourne, Melbourne, Australia"
    },
    {
      "name": "Wilson Suraweera",
      "first": "Wilson",
      "last": "Suraweera",
      "affiliation": "Centre for Global Health Research (CGHR), Li Ka Shing Knowledge Institute, St. Michael's Hospital and Dalla Lana School of Public Health, University of Toronto, Toronto, Ontario, Canada"
    },
    {
      "name": "Prakash Bhatia",
      "first": "Prakash",
      "last": "Bhatia",
      "affiliation": "Indian Institute of Health and Family Welfare, Hyderabad, India"
    },
    {
      "name": "Neeraj Dhingra",
      "first": "Neeraj",
      "last": "Dhingra",
      "affiliation": "National AIDS Control Organization, New Delhi, India"
    },
    {
      "name": "Raju M. Jotkar",
      "first": "Raju M.",
      "last": "Jotkar",
      "affiliation": "Centre for Global Health Research (CGHR), Li Ka Shing Knowledge Institute, St. Michael's Hospital and Dalla Lana School of Public Health, University of Toronto, Toronto, Ontario, Canada; St. John's Research Institute, Bangalore, India"
    },
    {
      "name": "Peter S. Rodriguez",
      "first": "Peter S.",
      "last": "Rodriguez",
      "affiliation": "Centre for Global Health Research (CGHR), Li Ka Shing Knowledge Institute, St. Michael's Hospital and Dalla Lana School of Public Health, University of Toronto, Toronto, Ontario, Canada"
    },
    {
      "name": "Kaushik Mishra",
      "first": "Kaushik",
      "last": "Mishra",
      "affiliation": "Shri Ramachandra Bhanj Medical College, Cuttack, Orissa, India"
    },
    {
      "name": "Romulus Whitaker",
      "first": "Romulus",
      "last": "Whitaker",
      "affiliation": "Madras Crocodile Bank Trust and Centre for Herpetology, Chennai, India"
    },
    {
      "name": "Prabhat Jha",
      "first": "Prabhat",
      "last": "Jha",
      "affiliation": "Centre for Global Health Research (CGHR), Li Ka Shing Knowledge Institute, St. Michael's Hospital and Dalla Lana School of Public Health, University of Toronto, Toronto, Ontario, Canada"
    },
    {
      "name": "for the Million Death Study Collaborators"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-04",
  "dateAccepted": "2011-02-15",
  "dateReceived": "2010-11-25",
  "volume": "5",
  "number": "4",
  "pages": "e1018",
  "tags": [
    "Epidemiology",
    "Global health",
    "Medicine",
    "Public Health and Epidemiology",
    "Public health"
  ],
  "abstract": "Background\n          \nIndia has long been thought to have more snakebites than any other country. However, inadequate hospital-based reporting has resulted in estimates of total annual snakebite mortality ranging widely from about 1,300 to 50,000. We calculated direct estimates of snakebite mortality from a national mortality survey.\n\n          Methods and Findings\n          \nWe conducted a nationally representative study of 123,000 deaths from 6,671 randomly selected areas in 2001–03. Full-time, non-medical field workers interviewed living respondents about all deaths. The underlying causes were independently coded by two of 130 trained physicians. Discrepancies were resolved by anonymous reconciliation or, failing that, by adjudication.\n\nA total of 562 deaths (0.47% of total deaths) were assigned to snakebites. Snakebite deaths occurred mostly in rural areas (97%), were more common in males (59%) than females (41%), and peaked at ages 15–29 years (25%) and during the monsoon months of June to September. This proportion represents about 45,900 annual snakebite deaths nationally (99% CI 40,900 to 50,900) or an annual age-standardised rate of 4.1/100,000 (99% CI 3.6–4.5), with higher rates in rural areas (5.4/100,000; 99% CI 4.8–6.0), and with the highest state rate in Andhra Pradesh (6.2). Annual snakebite deaths were greatest in the states of Uttar Pradesh (8,700), Andhra Pradesh (5,200), and Bihar (4,500).\n\n          Conclusions\n          \nSnakebite remains an underestimated cause of accidental death in modern India. Because a large proportion of global totals of snakebites arise from India, global snakebite totals might also be underestimated. Community education, appropriate training of medical staff and better distribution of antivenom, especially to the 13 states with the highest prevalence, could reduce snakebite deaths in India.",
  "fullText": "Introduction Alexander the Great invaded India in 326 BC, and was greatly impressed by the skill of Indian physicians; especially in the treatment of snakebites [1]. Since then, India has remained notorious for its venomous snakes and the effects of their bites. With its surrounding seas, India is inhabited by more than 60 species of venomous snakes – some of which are abundant and can cause severe envenoming [2]. Spectacled cobra (Naja naja), common krait (Bungarus caeruleus), saw-scaled viper (Echis carinatus) and Russell's viper (Daboia russelii) have long been recognised as the most important, but other species may cause fatal snakebites in particular areas, such as the central Asian cobra (Naja oxiana) in the far north-west, monocellate cobra (N. kaouthia) in the north-east, greater black krait (B. niger) in the far north-east, Wall's and Sind kraits (B. walli and B. sindanus) in the east and west and hump-nosed pit-viper (Hypnale hypnale) in the south-west coast and Western Ghats [2]. Joseph Fayrer of the Indian Medical Service first quantified human snakebite deaths in 1869 for about half of “British India” (including modern Pakistan, Bangladesh and Burma), finding that 11,416 people had died of snakebites [3]. Subsequent estimates of human deaths from snakebite prior to Indian Independence ranged from 7,400 to 20,000 per year [4]–[6]. Government of India hospitals from all but six states reported only 1,364 snakebite deaths in 2008 [7] but this is widely believed to be an under-report as many victims of snakebite choose village-based traditional therapists and most die outside government hospitals. Community-based surveys in some localities have shown much higher annual mortality rates, ranging widely from 16.4 deaths/100,000 in West Bengal [8] to 161/100,000 in the neighbouring Nepal Terai [9]. However, such focal data cannot be extrapolated to provide national or even state totals because of the heterogeneity of snakebite incidence. These uncertainties have resulted in indirect estimates of annual snakebite mortality in India that varied from approximately 1,300 to 50,000 [6], [7], [10]–[13]. To fill this gap in knowledge, we estimated snakebite deaths directly from a large continuing study of mortality in India. Methods Ethics Statement Ethics approval for the Million Deaths Study (MDS) was obtained from the Post Graduate Institute of Medical Research, St. John's Research Institute and St. Michael's Hospital, Toronto, Ontario, Canada [14]–[15]. Most deaths in rural India take place at home without prior attention by any qualified healthcare worker, so most causes are not medically certified [14]–[15]. Other approaches are therefore needed to help determine the probable causes of such deaths. The Registrar General of India (RGI) organises the Sample Registration System (SRS), which monitors all births and deaths in a nationally representative selection of 1.1 million homes throughout all 28 states and seven union territories of India. India was divided into approximately one million areas for the 1991 census, each with about 1,000 inhabitants. In 1993, the RGI randomly selected 6,671 of these areas to be represented in the SRS. Household characteristics were recorded and then enumerated twice yearly thereafter, documenting"
}