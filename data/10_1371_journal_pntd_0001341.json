{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001341",
  "doi": "10.1371/journal.pntd.0001341",
  "externalIds": [
    "pii:PNTD-D-11-00510"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Soil-Transmitted Helminth Infections among Plantation Sector Schoolchildren in Sri Lanka: Prevalence after Ten Years of Preventive Chemotherapy",
  "authors": [
    {
      "name": "Kithsiri Gunawardena",
      "first": "Kithsiri",
      "last": "Gunawardena",
      "affiliation": "Department of Parasitology, Faculty of Medicine, University of Kelaniya, Ragama, Sri Lanka"
    },
    {
      "name": "Balachandran Kumarendran",
      "first": "Balachandran",
      "last": "Kumarendran",
      "affiliation": "Department of Public Health, Faculty of Medicine, University of Kelaniya, Ragama, Sri Lanka"
    },
    {
      "name": "Roshini Ebenezer",
      "first": "Roshini",
      "last": "Ebenezer",
      "affiliation": "Department of Infectious Disease Epidemiology, Faculty of Medicine, Partnership for Child Development, Imperial College London, St Mary's Campus, London, United Kingdom"
    },
    {
      "name": "Muditha Sanjeewa Gunasingha",
      "first": "Muditha Sanjeewa",
      "last": "Gunasingha",
      "affiliation": "Department of Parasitology, Faculty of Medicine, University of Kelaniya, Ragama, Sri Lanka"
    },
    {
      "name": "Arunasalam Pathmeswaran",
      "first": "Arunasalam",
      "last": "Pathmeswaran",
      "affiliation": "Department of Public Health, Faculty of Medicine, University of Kelaniya, Ragama, Sri Lanka"
    },
    {
      "name": "Nilanthi de Silva",
      "first": "Nilanthi",
      "last": "de Silva",
      "affiliation": "Department of Parasitology, Faculty of Medicine, University of Kelaniya, Ragama, Sri Lanka"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-09",
  "dateAccepted": "2011-08-16",
  "dateReceived": "2011-06-01",
  "volume": "5",
  "number": "9",
  "pages": "e1341",
  "tags": [
    "Ascariasis",
    "Epidemiology",
    "Hookworm",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Public Health and Epidemiology",
    "Soil-transmitted helminths",
    "Trichuriasis"
  ],
  "abstract": "Background\n          \nThe plantation sector in Sri Lanka lags behind the rest of the country in terms of living conditions and health. In 1992, a sector-wide survey of children aged 3–12 years and women of reproductive age showed &gt;90% prevalence of soil-transmitted helminth infections. Biannual mass de-worming targeting children aged 3–18 years started in 1994 and was continued until 2005. The present study was carried out to assess the status of infection four years after cessation of mass de-worming.\n\n          Methods/Findings\n          \nA school-based cross-sectional survey was carried out. Faecal samples from approximately 20 children from each of 114 schools in five districts were examined using the modified Kato-Katz technique. Data regarding the school, the child's family and household sanitation were recorded after inspection of schools and households. Multivariate analysis was carried out using logistic regression, to identify risk factors for infection. Faecal samples were obtained from 1890 children. In 4/5 districts, &gt;20% were infected with one or more helminth species. Overall combined prevalence was 29.0%; 11.6% had infections of moderate-heavy intensity. The commonest infection was Ascaris lumbricoides, present in all five districts, as was Trichuris trichiura. Hookworm was not detected in two districts. Multivariate analysis identified low altitude and maternal under-education as risk factors for all three infections. Poor household sanitation was identified as a risk factor for A. lumbricoides and hookworm, but not T. trichiura infections.\n\n          Conclusions/Significance\n          \nThe results indicate that regular mass de-worming of plantation sector children should be resumed along with more emphasis on better sanitation and health education. They show that even after 10 years of mass chemotherapy, prevalence can bounce back after cessation of preventive chemotherapy, if the initial force of transmission is strong and other long-term control measures are not concomitantly implemented.",
  "fullText": "Introduction Sri Lanka has already achieved several Millennium Development Goals such as universal primary school enrolment, gender parity in school enrolment, and is on track to achieve the desired reduction in under-five and infant mortality. Poverty has markedly reduced in the urban and rural sectors between 1990 and 2006, from 26.1% to 15.2% [1]. However, not all parts of the country have benefited equally from these gains. In the plantation sector, which has a resident population of about 939,000 living and working on tea and rubber plantations, there is widespread child malnutrition, maternal mortality rates are exceptionally high and poverty has increased by over 50% in the same period [1]. Soil-transmitted helminth (STH) infections are well-known accompaniments of poverty in the developing world [2]. A survey that covered the entire plantation sector in Sri Lanka in 1992 found over 90% of children to be infected [3]. A major de-worming program, offering bi-annual treatment with 500 mg mebendazole to children aged 3–18 years, was launched in 1994 [4]. However, this programme was discontinued after about ten years, due to lack of funds, without proper reassessment of the epidemiological situation. A national survey of the health of school children, carried out in 2003, found only 6.9% to be infected with any of the three major STH infections [5]. This is well below the threshold of 20% prevalence recommended by the WHO for implementation of mass de-worming of school children in endemic areas [6]. Despite ten years of mass de-worming between 1994 and 2005 in the plantation sector, the extremely high prevalence of STH infection at the outset of the de-worming programme, and the actual increase in poverty in the intervening period, suggested that the results of the national survey should not be extrapolated to this sector. This study was designed to estimate the current prevalence and intensity of STH infections among primary school children in the plantation sector and to describe the factors associated with infection, in order to provide data for rational design and targeting of school-based health and nutrition programmes. Methods Ethics statement Approval was obtained from the Ethics Review Committee of the Faculty of Medicine, University of Kelaniya (application no P103/08/2009). After selection of children in each school, their homes were visited and their parent(s) interviewed in order to obtain written, informed consent for the child's participation in the study, and to obtain information on the socio-economic status of the family. Only children whose parents gave consent were included in the study. All children were offered treatment with mebendazole 500 mg at the end of the study. Study design and setting This was a school-based, cross-sectional survey. The 2007 School Census of the Ministry of Education identifies 830 ‘plantation sector schools’, almost all of which provide instruction in the Tamil language (also referred to as ‘Tamil-medium schools’). These schools are divided into two categories: inside or outside a plantation. Tamil-medium schools located inside the plantations formed the setting of the study. Study area The study was performed in five"
}