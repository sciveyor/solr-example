{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001358",
  "doi": "10.1371/journal.pntd.0001358",
  "externalIds": [
    "pii:PNTD-D-11-00458"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Sleeping Sickness in Travelers - Do They Really Sleep?",
  "authors": [
    {
      "name": "Karin Urech",
      "first": "Karin",
      "last": "Urech",
      "affiliation": "Swiss Tropical and Public Heath Institute, Basel, Switzerland; Medical Faculty, University of Basel, Basel, Switzerland"
    },
    {
      "name": "Andreas Neumayr",
      "first": "Andreas",
      "last": "Neumayr",
      "affiliation": "Swiss Tropical and Public Heath Institute, Basel, Switzerland"
    },
    {
      "name": "Johannes Blum",
      "first": "Johannes",
      "last": "Blum",
      "affiliation": "Swiss Tropical and Public Heath Institute, Basel, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-11",
  "dateAccepted": "2011-08-11",
  "dateReceived": "2011-05-15",
  "volume": "5",
  "number": "11",
  "pages": "e1358",
  "tags": [
    "African trypanosomiasis",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases"
  ],
  "abstract": "The number of imported Human African Trypanosomiasis (HAT) cases in non-endemic countries has increased over the last years. The objective of this analysis is to describe the clinical presentation of HAT in Caucasian travelers. Literature was screened (MEDLINE, Pubmed) using the terms “Human African Trypanosomiasis”, “travelers” and “expatriates”; all European languages except Slavic ones were included. Publications without clinical description of patients were only included in the epidemiological analysis. Forty-five reports on Caucasians with T.b. rhodesiense and 15 with T.b. gambiense infections were included in the analysis of the clinical parameters. Both species have presented with fever (T.b. rhodesiense 97.8% and T.b. gambiense 93.3%), headache (50% each) and a trypanosomal chancre (T.b. rhodesiense 84.4%, T.b. gambiense 46.7%). While sleeping disorders dominate the clinical presentation of HAT in endemic regions, there have been only rare reports in travelers: insomnia (T.b. rhodesiense 7.1%, T.b. gambiense 21.4%), diurnal somnolence (T.b. rhodesiense 4.8%, T.b. gambiense none). Surprisingly, jaundice has been seen in 24.2% of the Caucasian T.b. rhodesiense patients, but has never been described in HAT patients in endemic regions. These results contrast to the clinical presentation of T.b. gambiense and T.b. rhodesiense HAT in Africans in endemic regions, where the presentation of chronic T.b. gambiense and acute T.b. rhodesiense HAT is different. The analysis of 14 reports on T.b. gambiense HAT in Africans living in a non-endemic country shows that neurological symptoms such as somnolence (46.2%), motor deficit (64.3%) and reflex anomalies (14.3%) as well as psychiatric symptoms such as hallucinations (21.4%) or depression (21.4%) may dominate the clinical picture. Often, the diagnosis has been missed initially: some patients have even been hospitalized in psychiatric clinics. In travelers T.b. rhodesiense and gambiense present as acute illnesses and chancres are frequently seen. The diagnosis of HAT in Africans living outside the endemic region is often missed or delayed, leading to presentation with advanced stages of the disease.",
  "fullText": "Introduction The increasing tourism to Africa is accompanied by an increasing number of imported tropical diseases including rare cases of Human African Trypanosomiasis (HAT). HAT, also known as Sleeping Sickness, is caused by the protozoan parasites Trypanosoma brucei gambiense (T.b. gambiense = West African form) and Trypanosoma brucei rhodesiense (T.b. rhodesiense = East African form), which are transmitted by the bite of the tsetse fly, Glossina spp. The T.b. gambiense HAT is characterized by a chronic progressive course, lasting months to years, leading to death if left untreated. The T.b. rhodesiense HAT, however, is more acute and death occurs within weeks or months. The disease appears in two stages: the first being the early or haemo-lymphatic stage, and the second being the late or meningo-encephalitic stage, characterized by the trypanosome invasion of the central nervous system (CNS). In endemic populations, a trypanosomal chancre (local infection at the location of the tsetse fly bite) is only seen as an exception in T.b. gambiense; however, it is seen in 19% of T.b. rhodesiense patients. Chronic and intermittent fever, headache, pruritus, lymphadenopathy, and - to a lesser extent - hepatosplenomegaly are the leading signs and symptoms of the first stage. In the second stage sleep disturbances and neuro-psychiatric disorders dominate the clinical presentation. The diagnosis is based on the visualisation of the parasite in peripheral blood, lymph node aspirate, cerebrospinal fluid (CSF), or on polymerase chain reaction (PCR) technology, and serological tests. As treatment differs markedly between first and second stage HAT, the staging of the disease by examining the CSF is essential. According to the definition of the WHO, an elevated white blood cell count (WBC&gt;5 cells/mm3) or the presence of trypanosomes in the CSF indicate a second stage disease [1]. In 1966 A.J. Duggan and M.P. Hutchinson reviewed 109 cases found in Europeans and North Americans who were infected with HAT between 1904 and 1963. They observed different clinical presentations in travelers and expatriates compared to natives of endemic regions. They reported that in Caucasians, the onset of disease is invariably acute, irrespective of the species involved. However, they documented their observations only partially with precise data. The first objective of this study is to assess the epidemiology and the clinical presentation of HAT in travelers and expatriates from non-endemic countries and to describe the differences between the T.b. gambiense and T.b. rhodesiense cases. The second objective is to describe the clinical features of HAT patients native to endemic regions who migrated to non-endemic regions, mainly Europe and North America. Materials and Methods We performed a Pubmed (MEDLINE) search of literature using the key words “Human African Trypasomiasis”, “travelers”, and “expatriates” and reviewed the available references – including the bibliographies of the retrieved references – published between 1967 and 2010 for eligible publications. The inclusion criteria were all available publications written in European languages except Slavic languages (Dutch, English, French, German or Norwegian) on: HAT patients from non-endemic regions who were diagnosed and treated in non-endemic regions HAT patients from endemic regions who were diagnosed"
}