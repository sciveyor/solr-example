{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001187",
  "doi": "10.1371/journal.pntd.0001187",
  "externalIds": [
    "pii:PNTD-D-10-00216"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Health Services for Buruli Ulcer Control: Lessons from a Field Study in Ghana",
  "authors": [
    {
      "name": "Mercy M. Ackumey",
      "first": "Mercy M.",
      "last": "Ackumey",
      "affiliation": "School of Public Health, College of Health Sciences, University of Ghana, Accra, Ghana; Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Cynthia Kwakye-Maclean",
      "first": "Cynthia",
      "last": "Kwakye-Maclean",
      "affiliation": "Ga-West Municipal Health Administration, Amasaman, Ghana"
    },
    {
      "name": "Edwin O. Ampadu",
      "first": "Edwin O.",
      "last": "Ampadu",
      "affiliation": "National Buruli Ulcer Control Programme, Accra, Ghana"
    },
    {
      "name": "Don de Savigny",
      "first": "Don",
      "last": "de Savigny",
      "affiliation": "Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Mitchell G. Weiss",
      "first": "Mitchell G.",
      "last": "Weiss",
      "affiliation": "Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-06",
  "dateAccepted": "2011-04-17",
  "dateReceived": "2010-11-28",
  "volume": "5",
  "number": "6",
  "pages": "e1187",
  "tags": [
    "Medicine",
    "Public Health and Epidemiology",
    "Public health"
  ],
  "abstract": "Background\n          \nBuruli ulcer (BU), caused by Mycobacterium ulcerans infection, is a debilitating disease of the skin and underlying tissue. The first phase of a BU prevention and treatment programme (BUPaT) was initiated from 2005–2008, in the Ga-West and Ga-South municipalities in Ghana to increase access to BU treatment and to improve early case detection and case management. This paper assesses achievements of the BUPaT programme and lessons learnt. It also considers the impact of the programme on broader interests of the health system.\n\n          Methods\n          \nA mixed-methods approach included patients' records review, review of programme reports, a stakeholder forum, key informant interviews, focus group discussions, clinic visits and observations.\n\n          Principal Findings\n          \nExtensive collaboration existed across all levels, (national, municipality, and community), thus strengthening the health system. The programme enhanced capacities of all stakeholders in various aspects of health services delivery and demonstrated the importance of health education and community-based surveillance to create awareness and encourage early treatment. A patient database was also created using recommended World Health Organisation (WHO) forms which showed that 297 patients were treated from 2005–2008. The proportion of patients requiring only antibiotic treatment, introduced in the course of the programme, was highest in the last year (35.4% in the first, 23.5% in the second and 42.5% in the third year). Early antibiotic treatment prevented recurrences which was consistent with programme aims.\n\n          Conclusions\n          \nTo improve early case management of BU, strengthening existing clinics to increase access to antibiotic therapy is critical. Intensifying health education and surveillance would ultimately increase early reporting and treatment for all cases. Further research is needed to explain the role of environmental factors for BU contagion. Programme strategies reported in our study: collaboration among stakeholders, health education, community surveillance and regular antibiotic treatment can be adopted for any BU-endemic area in Ghana.",
  "fullText": "Introduction In the absence of a proven strategy for preventing infection, control of Buruli Ulcer (BU) relies on efficient health services to prevent progression of pre-ulcerative conditions and treat ulcers. According to the World Health Organisation (WHO), service delivery is the primary function of any health system and entails the provision of “effective, safe, good quality care to those that need it with minimal waste”, [1] and to address health care needs through promotion, prevention, treatment and rehabilitation. WHO defines a health system as “all organisations, people and actions whose primary intent is to promote or to restore health” [1]. Buruli ulcer, caused by Mycobacterium ulcerans infection is a debilitating disease of the skin and underlying tissue which starts as a painless nodule, oedema or plaque and could develop into painful and massive ulcers if left untreated [2]. It is the third most common mycobacterial pathogen of humans, after M. tuberculosis (tuberculosis) and M. leprae (leprosy), but the most poorly understood [2], [3]. Even though case fatality is low, morbidity is high for all age groups [3]–[5] and the socio-economic implications to the individual and cost of management to the health system are enormous [6], [7]. Surprisingly, estimates of Disability Adjusted Life Years (DALYs) for Buruli ulcer, like other neglected tropical diseases (NTDS) such as guinea worm, endemic syphilis and food-borne trematode infections are not explicitly stated [8]. BU has been reported in more than 33 tropical and sub-tropical climates particularly West African countries [2], [9], and Ghana reports an average of 1000 cases each year [9]. The first case of BU was reported in Ghana in 1972 in the Ga-district. [10]. A national case search in 1998 indicated a national prevalence of 20.7/100,000 and a prevalence of 87.7/100,000 for the former Ga-district (now the Ga-West and Ga-South municipalities), the fifth most endemic in the country, yet with the highest burden in terms of healed and active lesions [11]. The first phase of a BU prevention and treatment programme (BUPaT) was initiated from 2005–2008, in the Ga-West and Ga-South municipalities in the Greater-Accra region, Ghana, to increase access to BU treatment and improve early case detection. Before the inception of the BUPaT programme, surgery was the main treatment for all BU patients. There was limited accessibility to treatment since all surgeries had to be done at the Amasaman hospital (AH), the main treatment and referral hospital for all BU cases in the Ga-West municipality. Antibiotic treatment had not been introduced and health staff had limited expertise in surgical procedures and BU case management. The BUPaT programme employed WHO-recommended strategies which are: Building capacity of nurses and other para-medical staff for effective case detection, and management at designated health centres; training of community-based surveillance volunteers (CBSVs), school teachers, other health workers and traditional healers (THs), to enhance BU knowledge for early detection; establishing a community-based surveillance system with the help of CBSVs; compiling a database; providing surgical and antibiotic therapy for all BU patients [12]. These strategies were undertaken by a"
}