{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000658",
  "doi": "10.1371/journal.pntd.0000658",
  "externalIds": [
    "pii:09-PNTD-RA-0572R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The Genome Sequence of Trypanosoma brucei gambiense, Causative Agent of Chronic Human African Trypanosomiasis",
  "authors": [
    {
      "name": "Andrew P. Jackson",
      "first": "Andrew P.",
      "last": "Jackson",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Mandy Sanders",
      "first": "Mandy",
      "last": "Sanders",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Andrew Berry",
      "first": "Andrew",
      "last": "Berry",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Jacqueline McQuillan",
      "first": "Jacqueline",
      "last": "McQuillan",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Martin A. Aslett",
      "first": "Martin A.",
      "last": "Aslett",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Michael A. Quail",
      "first": "Michael A.",
      "last": "Quail",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Bridget Chukualim",
      "first": "Bridget",
      "last": "Chukualim",
      "affiliation": "International Trypanotolerance Center, Banjul, The Gambia"
    },
    {
      "name": "Paul Capewell",
      "first": "Paul",
      "last": "Capewell",
      "affiliation": "Wellcome Centre for Molecular Parasitology, Glasgow Biomedical Research Centre, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Annette MacLeod",
      "first": "Annette",
      "last": "MacLeod",
      "affiliation": "Wellcome Centre for Molecular Parasitology, Glasgow Biomedical Research Centre, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Sara E. Melville",
      "first": "Sara E.",
      "last": "Melville",
      "affiliation": "Hughes Hall, Cambridge, United Kingdom"
    },
    {
      "name": "Wendy Gibson",
      "first": "Wendy",
      "last": "Gibson",
      "affiliation": "School of Biological Sciences, University of Bristol, Bristol, United Kingdom"
    },
    {
      "name": "J. David Barry",
      "first": "J. David",
      "last": "Barry",
      "affiliation": "Wellcome Centre for Molecular Parasitology, Glasgow Biomedical Research Centre, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Matthew Berriman",
      "first": "Matthew",
      "last": "Berriman",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    },
    {
      "name": "Christiane Hertz-Fowler",
      "first": "Christiane",
      "last": "Hertz-Fowler",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Cambridge, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-04",
  "dateAccepted": "2010-03-02",
  "dateReceived": "2009-10-19",
  "volume": "4",
  "number": "4",
  "pages": "e658",
  "tags": [
    "Evolutionary Biology/Evolutionary and Comparative Genetics",
    "Evolutionary Biology/Genomics",
    "Genetics and Genomics/Comparative Genomics",
    "Genetics and Genomics/Gene Discovery",
    "Genetics and Genomics/Genome Projects",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections"
  ],
  "abstract": "Background\n\nTrypanosoma brucei gambiense is the causative agent of chronic Human African Trypanosomiasis or sleeping sickness, a disease endemic across often poor and rural areas of Western and Central Africa. We have previously published the genome sequence of a T. b. brucei isolate, and have now employed a comparative genomics approach to understand the scale of genomic variation between T. b. gambiense and the reference genome. We sought to identify features that were uniquely associated with T. b. gambiense and its ability to infect humans.\n\nMethods and Findings\n\nAn improved high-quality draft genome sequence for the group 1 T. b. gambiense DAL 972 isolate was produced using a whole-genome shotgun strategy. Comparison with T. b. brucei showed that sequence identity averages 99.2% in coding regions, and gene order is largely collinear. However, variation associated with segmental duplications and tandem gene arrays suggests some reduction of functional repertoire in T. b. gambiense DAL 972. A comparison of the variant surface glycoproteins (VSG) in T. b. brucei with all T. b. gambiense sequence reads showed that the essential structural repertoire of VSG domains is conserved across T. brucei.\n\nConclusions\n\nThis study provides the first estimate of intraspecific genomic variation within T. brucei, and so has important consequences for future population genomics studies. We have shown that the T. b. gambiense genome corresponds closely with the reference, which should therefore be an effective scaffold for any T. brucei genome sequence data. As VSG repertoire is also well conserved, it may be feasible to describe the total diversity of variant antigens. While we describe several as yet uncharacterized gene families with predicted cell surface roles that were expanded in number in T. b. brucei, no T. b. gambiense-specific gene was identified outside of the subtelomeres that could explain the ability to infect humans.",
  "fullText": "Introduction Trypanosoma brucei subsp. gambiense is the causative agent of Human African Trypanosomiasis (HAT), or sleeping sickness, which is a vector-borne disease restricted to rural areas of sub-Saharan Africa. Trypanosomiasis in humans and livestock imposes substantial morbidity, representing a major impediment of agricultural production in the affected areas [1], and is fatal where untreated. The World Health Organization estimated in 1998 that up to 60 million people are at risk in approximately 250 distinct foci [2], although under-reporting has been estimated as high as 40% in some foci [3]. T. b. gambiense is the most clinically relevant sub-species, causing over 90% of all human disease. The gambiense disease is typically chronic, often lasting several years with few severe signs and symptoms until the late stage of nervous system involvement. T. b. gambiense is sensitive to treatment with pentamidine (early stage) and eflornithine (late stage), drugs which are frequently ineffective against T. b. rhodesiense [4], although the underlying biochemical reasons for these differences are unknown. Combination therapies against the late stage disease have performed encouragingly [5] but few drugs are available. Furthermore, unpleasant and in some cases severe side effects often result in poor patient compliance. Hence, new molecular targets are required to supply current drug discovery programmes [6]. T. brucei is subdivided into three subspecies based on infectivity to humans, pathogenicity and geographical distribution. T. b. gambiense and T. b. rhodesiense are human pathogens, causing Human African Trypanosomiasis (HAT) in West/Central and East Africa respectively. T. b. brucei cannot by definition infect humans and is found in a wide range of wild and domestic mammals. The human pathogens have also been found in various animal species and HAT caused by T. b. rhodesiense in East Africa is recognized as a zoonosis. T. b. gambiense comprises two groups; a genetically homogeneous group to which the majority of isolates belong (group 1), and a second represented by a handful of isolates from West Africa (group 2). Group 1 T. b. gambiense strains have the smallest genomes in the T. brucei species complex, having 71–82% of the highest DNA content measured for T. b. brucei [7]–[8]. Pulse-field gel analysis of T. b. gambiense chromosomes shows that few if any mini-chromosomes are present compared to the estimated 100 in T. b. brucei and T. b. rhodesiense, and the mini-chromosomes are also of a smaller size–average 25 kb in T. b. gambiense compared to 100 kb in T. b. brucei and T. b. rhodesiense [8]–[9]. Perhaps as a consequence of this reduced genome, T. b. gambiense also has a restricted repertoire of Variant Surface Glycoprotein (VSG) genes [8], [10]–[12]. At any time, bloodstream form trypanosomes possess a surface glycoprotein coat formed through the expression of a single gene from a large archive of VSGs [13]. This coat obfuscates the host immune system by shielding the invariant surface epitopes from view and, when an immune response is inevitably raised against the VSG monolayer and the active VSG is replaced by another, it allows parasites expressing the"
}