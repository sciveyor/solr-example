{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001265",
  "doi": "10.1371/journal.pntd.0001265",
  "externalIds": [
    "pii:PNTD-D-11-00385"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Postepidemic Analysis of Rift Valley Fever Virus Transmission in Northeastern Kenya: A Village Cohort Study",
  "authors": [
    {
      "name": "A. Desirée LaBeaud",
      "first": "A. Desirée",
      "last": "LaBeaud",
      "affiliation": "Center for Immunobiology and Vaccine Development, Children's Hospital Oakland Research Institute, Oakland, California, United States of America; Center for Global Health and Diseases, Case Western Reserve University, Cleveland, Ohio, United States of America"
    },
    {
      "name": "Samuel Muiruri",
      "first": "Samuel",
      "last": "Muiruri",
      "affiliation": "Division of Vector-Borne and Neglected Tropical Diseases, Ministry of Public Health and Sanitation, Nairobi, Kenya"
    },
    {
      "name": "Laura J. Sutherland",
      "first": "Laura J.",
      "last": "Sutherland",
      "affiliation": "Center for Global Health and Diseases, Case Western Reserve University, Cleveland, Ohio, United States of America"
    },
    {
      "name": "Saidi Dahir",
      "first": "Saidi",
      "last": "Dahir",
      "affiliation": "Division of Vector-Borne and Neglected Tropical Diseases, Ministry of Public Health and Sanitation, Nairobi, Kenya"
    },
    {
      "name": "Ginny Gildengorin",
      "first": "Ginny",
      "last": "Gildengorin",
      "affiliation": "Center for Immunobiology and Vaccine Development, Children's Hospital Oakland Research Institute, Oakland, California, United States of America"
    },
    {
      "name": "John Morrill",
      "first": "John",
      "last": "Morrill",
      "affiliation": "Center for Biodefense and Emerging Infectious Diseases, University of Texas Medical Branch, Galveston, Texas, United States of America"
    },
    {
      "name": "Eric M. Muchiri",
      "first": "Eric M.",
      "last": "Muchiri",
      "affiliation": "Division of Vector-Borne and Neglected Tropical Diseases, Ministry of Public Health and Sanitation, Nairobi, Kenya"
    },
    {
      "name": "Clarence J. Peters",
      "first": "Clarence J.",
      "last": "Peters",
      "affiliation": "Center for Biodefense and Emerging Infectious Diseases, University of Texas Medical Branch, Galveston, Texas, United States of America"
    },
    {
      "name": "Charles H. King",
      "first": "Charles H.",
      "last": "King",
      "affiliation": "Center for Global Health and Diseases, Case Western Reserve University, Cleveland, Ohio, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-08",
  "dateAccepted": "2011-06-21",
  "dateReceived": "2011-04-27",
  "volume": "5",
  "number": "8",
  "pages": "e1265",
  "tags": [
    "Epidemiology",
    "Infectious Diseases",
    "Infectious disease epidemiology",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Public Health and Epidemiology",
    "Rift Valley fever",
    "Viral diseases"
  ],
  "abstract": "Background\n          \nIn endemic areas, Rift Valley fever virus (RVFV) is a significant threat to both human and animal health. Goals of this study were to measure human anti-RVFV seroprevalence in a high-risk area following the 2006–2007 Kenyan Rift Valley Fever (RVF) epidemic, to identify risk factors for interval seroconversion, and to monitor individuals previously exposed to RVFV in order to document the persistence of their anti-RVFV antibodies.\n\n          Methodology/Findings\n          \nWe conducted a village cohort study in Ijara District, Northeastern Province, Kenya. One hundred two individuals tested for RVFV exposure before the 2006–2007 RVF outbreak were restudied to determine interval anti-RVFV seroconversion and persistence of humoral immunity since 2006. Ninety-two additional subjects were enrolled from randomly selected households to help identify risk factors for current seropositivity. Overall, 44/194 or 23% (CI95%:17%–29%) of local residents were RVFV seropositive. 1/85 at-risk individuals restudied in the follow-up cohort had seroconverted since early 2006. 27/92 (29%, CI95%: 20%–39%) of newly tested individuals were seropositive. All 13 individuals with positive titers (by plaque reduction neutralization testing (PRNT80)) in 2006 remained positive in 2009. After adjustment in multivariable logistic models, age, village, and drinking raw milk were significantly associated with RVFV seropositivity. Visual impairment (defined as ≤20/80) was much more likely in the RVFV-seropositive group (P&lt;0.0001).\n\n          Conclusions\n          \nOur results highlight significant variability in RVFV exposure in two neighboring villages having very similar climate, terrain, and insect density. Among those with previous exposure, RVFV titers remained at &gt;1∶40 for more than 3 years. In concordance with previous studies, residents of the more rural village were more likely to be seropositive and RVFV seropositivity was associated with poor visual acuity. Raw milk consumption was strongly associated with RVFV exposure, which may represent an important new focus for public health education during future RVF outbreaks.",
  "fullText": "Introduction Rift Valley Fever (RVF) is a life-threatening, mosquito-borne zoonotic disease found in many areas of sub-Saharan Africa and the Middle East [1]. Because Rift Valley fever virus (RVFV) readily infects both humans and their livestock, RVF poses a severe, dual threat to public health and to livestock food production in endemic regions [2], [3]. Of particular concern, the range of RVFV transmission has extended beyond sub-Saharan Africa over the last 35 years [4]–[6]. Future RVFV spread beyond its present enzootic areas, whether through natural livestock/vector movement or through bioterrorist action, poses a significant threat to many countries. RVFV, a member of the genus Phlebovirus, is a mosquito-borne virus that is maintained within ecosystems by vertical transmission among local floodwater Aedes spp. mosquitoes [7]. Typically, in enzootic regions, these transient vectors reintroduce RVFV into local mammalian fauna following periods of heavy rainfall, after which other hematophagous vectors, typically culicine mosquitoes, serve to perpetuate transmission [8]. In addition, transmission of RVFV can also occur via aerosol or direct contact with infected animals or their body fluids [9]. RVFV infection causes serious disease in both human and animal populations, resulting in significant agricultural, economic and public health consequences. Although in the majority of human cases RVFV causes a mild, acute febrile illness with fever, malaise, and myalgia, a minority of human cases are complicated by retinitis (10%), encephalitis (8%), and hemorrhagic fever (1%) with significant risk of related morbidity and mortality [9]–[18]. During outbreaks, livestock are at even greater risk, as RVF frequently causes hemorrhagic disease and “abortion storms” that are associated with high mortality among domestic sheep, goats, and cattle [3], [19], [20]. In 2006–2007, a major Rift Valley Fever outbreak resulted in significant human and animal disease across East Africa, including parts of Kenya, Tanzania, Sudan, and Somalia [21], [22]. In Kenya, 684 human cases were reported of whom 333 were from Northeastern Province, the focus of our present study [21]. Having conducted a serosurvey in Ijara District in 2006, just prior to the 2006–2007 outbreak, we then performed a follow-up survey in 2009 in order to: (i) quantify the new local level of anti-RVFV seroprevalence in the human population; (ii) identify risk for seroconversion; and (iii) monitor previously exposed individuals to estimate the persistence of their post-infection immune response. Methods Ethics Statement All adult participants provided written informed consent under a protocol approved by the Human Investigations Review Board of University Hospitals of Cleveland and by the Ethical Review Committee of the Kenya Medical Research Institute, Nairobi. Parents provided written informed consent for participating children; children &gt;7 years of age also provided individual assent. Location Our study was a household-based cluster sampling of human populations residing in 2 areas near Masalani Town, Ijara District, situated in a semiarid region of Northeastern Province, Kenya (Figure 1). The study was performed in August through November of 2009, ∼3 years after the previous RVF outbreak of 2006–2007 [22]. This population was previously tested for RVFV in early 2006 [23] prior to the"
}