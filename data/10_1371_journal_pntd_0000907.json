{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000907",
  "doi": "10.1371/journal.pntd.0000907",
  "externalIds": [
    "pii:10-PNTD-VP-1395R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Factory Tsetse Flies Must Behave Like Wild Flies: A Prerequisite for the Sterile Insect Technique",
  "authors": [
    {
      "name": "Marc J. B. Vreysen",
      "first": "Marc J. B.",
      "last": "Vreysen",
      "affiliation": "Insect Pest Control Laboratory, Joint FAO/IAEA Programme of Nuclear Techniques in Food and Agriculture, Vienna, Austria"
    },
    {
      "name": "Khalfan M. Saleh",
      "first": "Khalfan M.",
      "last": "Saleh",
      "affiliation": "Ministry of Agriculture, Natural Resources and Environment, Zanzibar, Tanzania"
    },
    {
      "name": "Renaud Lancelot",
      "first": "Renaud",
      "last": "Lancelot",
      "affiliation": "Cirad, UMR Contrôle des maladies animales exotiques et émergentes, Campus International de Baillarguet, Montpellier, France"
    },
    {
      "name": "Jérémy Bouyer",
      "first": "Jérémy",
      "last": "Bouyer",
      "affiliation": "Cirad, UMR Contrôle des maladies animales exotiques et émergentes, Campus International de Baillarguet, Montpellier, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-02",
  "volume": "5",
  "number": "2",
  "pages": "e907",
  "tags": [
    "Ecology/Behavioral Ecology",
    "Ecology/Population Ecology",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "fullText": "Tsetse flies are the vectors of human and animal African trypanosomoses, the former a major neglected disease, and the latter considered among the greatest constraints to livestock production in sub-Saharan Africa. To date, the disease is mainly contained through the prophylactic and curative treatment of livestock with trypanocidal drugs, which is not sustainable. The removal of the vector, the tsetse fly, would be the most efficient way of managing these diseases. A number of efficient tsetse control tactics are available that can be combined and applied following area-wide integrated pest management (AW-IPM) principles [1]. The concept entails (1) the integration of various control tactics, preferably combining those methods that are effective at high population densities with those that are effective at low population densities to obtain maximal efficiency, and (2) the control effort is directed against an entire tsetse population within a delimited area. This is particularly relevant in case eradication is the strategy of choice. Genetic control tactics such as the sterile insect technique (SIT) show great potential for integration in such AW-IPM programmes because they are very efficient for controlling low-density populations, which is not the case for most other techniques. Sterile male insects are reared and, after sterilization with ionizing radiation, sequentially released in large quantities to outnumber the wild male flies. A mating of a sterile male with a virgin wild female fly results in no offspring. Recently, transgenic and paratransgenic techniques have been proposed to sterilize male insects or to make strains refractory to disease parasites in the case of vectors [2]–[4]. However, to ensure the success of these control methods, factory-reared tsetse flies must be competitive with their wild counterparts and must exhibit a similar behaviour in a natural environment. The SIT as part of an AW-IPM approach is a robust technique, which has proven to be very efficient in eradicating, suppressing, or containing dipteran pests such as Cochliomyia hominivorax (New World screwworm) in Central America, Mexico, the United States, and Libya [5], [6], Ceratitis capitata (Mediterranean fruit fly) in Argentina, Chile, Israel, Mexico, Peru, Spain, and the US, Bactrocera cucurbitae (melon fly) in the Okinawa archipelago of Japan, and lepidopteran pests such as Cydia pomonella (codling moth) in Canada, Australian painted apple moth (Teia anartoides) in New Zealand, and Cactoblastic cactorum (cactus moth) and Pectinophora gossypiella (pink bollworm) in Mexico and the US [1]. Similarly, the SIT has been successfully integrated with other control tactics against several tsetse species, i.e., with aerial spraying of insecticides against Glossina morsitans morsitans in Tanzania, with insecticide-impregnated targets and traps against Glossina palpalis gambiensis and Glossina tachinoides in Burkina Faso and G. palpalis palpalis in Nigeria, and with the live-bait technique against Glossina austeni on Unguja Island (Zanzibar). These programmes showed that the SIT against tsetse is feasible, but with the exception of the programme on Unguja Island [7], they proved to be unsustainable. Some have thus questioned whether competitive sterile male tsetse flies can be produced [8], especially since learning mechanisms like site- [9] or host-fidelity"
}