{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000987",
  "doi": "10.1371/journal.pntd.0000987",
  "externalIds": [
    "pii:10-PNTD-RA-1453R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Evaluation of Mammalian and Intermediate Host Surveillance Methods for Detecting Schistosomiasis Reemergence in Southwest China",
  "authors": [
    {
      "name": "Elizabeth J. Carlton",
      "first": "Elizabeth J.",
      "last": "Carlton",
      "affiliation": "Division of Environmental Health Sciences, School of Public Health, University of California, Berkeley, California, United States of America"
    },
    {
      "name": "Michael N. Bates",
      "first": "Michael N.",
      "last": "Bates",
      "affiliation": "Division of Epidemiology, School of Public Health, University of California, Berkeley, California, United States of America"
    },
    {
      "name": "Bo Zhong",
      "first": "Bo",
      "last": "Zhong",
      "affiliation": "Institute of Parasitic Diseases, Sichuan Center for Disease Control and Prevention, Chengdu, China"
    },
    {
      "name": "Edmund Y. W. Seto",
      "first": "Edmund Y. W.",
      "last": "Seto",
      "affiliation": "Division of Environmental Health Sciences, School of Public Health, University of California, Berkeley, California, United States of America"
    },
    {
      "name": "Robert C. Spear",
      "first": "Robert C.",
      "last": "Spear",
      "affiliation": "Division of Environmental Health Sciences, School of Public Health, University of California, Berkeley, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-03",
  "dateAccepted": "2011-02-14",
  "dateReceived": "2010-08-13",
  "volume": "5",
  "number": "3",
  "pages": "e987",
  "tags": [
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Public Health and Epidemiology/Epidemiology",
    "Public Health and Epidemiology/Global Health",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Background\n\nSchistosomiasis has reemerged in China, threatening schistosomiasis elimination efforts. Surveillance methods that can identify locations where schistosomiasis has reemerged are needed to prevent the further spread of infections.\n\nMethods and Principal Findings\n\nWe tested humans, cows, water buffalo and the intermediate host snail, Oncomelania hupensis, for Schistosoma japonicum infection, assessed snail densities and extracted regional surveillance records in areas where schistosomiasis reemerged in Sichuan province. We then evaluated the ability of surveillance methods to identify villages where human infections were present. Human infections were detected in 35 of the 53 villages surveyed (infection prevalence: 0 to 43%), including 17 of 28 villages with no prior evidence of reemergence. Bovine infections were detected in 23 villages (infection prevalence: 0 to 65%) and snail infections in one village. Two common surveillance methods, acute schistosomiasis case reports and surveys for S. japonicum-infected snails, grossly underestimated the number of villages where human infections were present (sensitivity 1% and 3%, respectively). Screening bovines for S. japonicum and surveys for the presence of O. hupensis had modest sensitivity (59% and 69% respectively) and specificity (67% and 44%, respectively). Older adults and bovine owners were at elevated risk of infection. Testing only these high-risk human populations yielded sensitivities of 77% and 71%, respectively.\n\nConclusions\n\nHuman and bovine schistosomiasis were widespread in regions where schistosomiasis had reemerged but acute schistosomiasis and S. japonicum-infected snails were rare and, therefore, poor surveillance targets. Until more efficient, sensitive surveillance strategies are developed, direct, targeted parasitological testing of high-risk human populations should be considered to monitor for schistosomiasis reemergence.",
  "fullText": "Introduction The success of disease control programs in reducing schistosomiasis infections and morbidity have prompted consideration of the elimination of human schistosomiasis [1], [2]. Dramatic declines in Schistosoma haematobium and S. mansoni have been observed following widespread distribution of the antihelminthic drug, praziquantel, in six countries in sub-Saharan Africa [3]–[5]. Disease control efforts in China, including a ten-year partnership with the World Bank to promote treatment, have led to the interruption of S. japonicum transmission in 5 of 12 endemic provinces and 60% of endemic counties [6], [7]. Currently, China is aiming to eliminate schistosomiasis, setting an initial goal of reducing human and bovine infection prevalence below 1% in every endemic region by 2015 [8]. If successful, China's program may serve as a model for schistosomiasis control elsewhere. However, schistosomiasis has reemerged in previously controlled regions, highlighting the challenges of sustaining reductions in infections. In Sichuan, China, schistosomiasis was identified in 8 of 46 counties that had met Chinese Ministry of Health criteria for transmission control, which require the reduction of human and bovine infection prevalence below 1% in every endemic village [9]. Nationwide, 38 counties that have met transmission control criteria have been reclassified as reemerging [7]. In the absence of a vaccine or lasting immunity, and with at least forty competent mammalian reservoirs, S. japonicum reemergence remains a threat in controlled regions [10]. Little is known about the epidemiology of reemerging schistosomiasis, including how infections are distributed across human populations, other mammalian reservoirs and intermediate host snails. Surveillance systems that can identify areas where lapses in control have occurred are an essential component of disease elimination strategies [11], [12]. They can enable timely treatment of infected populations and interventions to prevent further spread of infections. In China, surveillance for schistosomiasis in controlled regions includes hospital-based surveillance for acute schistosomiasis, surveys for the intermediate host snail, Oncomelania hupensis, and direct testing of the human population [13]. Acute schistosomiasis is triggered by the migration of the parasite through the body shortly after infection, leading to rapid onset of symptoms including high fever, myalgia and eosinophilia [14]. Due to the quick and severe onset, acute schistosomiasis, which is a reportable disease in China, can serve as a sentinel event, signaling the reemergence or emergence of schistosomiasis, as occurred in the Yangtze River valley following flooding events and in Sichuan province [9], [15], [16]. But acute schistosomiasis is rare, comprising less than 1% of all schistosomiasis cases [17]. It is possible for schistosomiasis to reemerge more quietly, as schistosomiasis typically induces chronic morbidity [18]–[20], leading to uncertainties about the sensitivity of surveillance methods that rely on acute schistosomiasis case reports. Similarly, surveillance for schistosome-infected snails and children is recommended in regions approaching schistosomiasis elimination, but how well these surveillance targets can identify areas where human infections are present remains uncertain [21]. In an effort to inform surveillance for S. japonicum reemergence, we examined the distribution of S. japonicum infections in human, bovine and snail populations in regions where schistosomiasis had reemerged. We"
}