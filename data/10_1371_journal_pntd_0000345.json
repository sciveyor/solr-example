{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000345",
  "doi": "10.1371/journal.pntd.0000345",
  "externalIds": [
    "pii:08-PNTD-VP-0257R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Challenges in Retaining Research Scientists beyond the Doctoral Level in Kenya",
  "authors": [
    {
      "name": "Lynette Isabella Ochola",
      "first": "Lynette Isabella",
      "last": "Ochola",
      "affiliation": "Kenya Medical Research Institute (KEMRI)/Wellcome Trust Collaborative Research Programme, Centre for Geographic Medicine (Coast), Kilifi, Kenya"
    },
    {
      "name": "Evelyn Gitau",
      "first": "Evelyn",
      "last": "Gitau",
      "affiliation": "Kenya Medical Research Institute (KEMRI)/Wellcome Trust Collaborative Research Programme, Centre for Geographic Medicine (Coast), Kilifi, Kenya"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-03",
  "volume": "3",
  "number": "3",
  "pages": "e345",
  "tags": [
    "Science Policy/Education"
  ],
  "fullText": "Introduction An important goal of modern-day African governments should be to develop a sustainable research culture in higher education in order to provide human resources and expertise toward better health and scientific national policies. Regrettably, research in Kenya is mainly funded by Northern collaborators, with the Kenyan government spending only 6.2% of total government expenditure on health in 2001 [1], and even less on health-related research. As a result, the local institutions are not carrying out the bulk of research in the country; instead, most research conducted in Kenya is funded by Northern collaborators: for example, Kenya Medical Research Institute (KEMRI) programs are funded by the Wellcome Trust (United Kingdom), Centers for Disease Control and Prevention (United States of America), and Walter Reed Army Institute of Research (United States of America). These partnerships have contributed to the changing landscape of research in Kenya, and they continue to play an important role in training local scientists. Ongoing programs and projects culminating from these partnerships have significant components designed to build individual and institutional national capacities in a variety of disciplines at all levels. One of the ways this has been done is to provide postgraduate training to young scientists to the doctoral level both at local and overseas academic institutions. However, the issue of capacity retention following training has not been comprehensively tackled. In this Viewpoint, we highlight three competitive doctoral tracks available in Kenya and how the choices students make ultimately play a role in their search for postdoctoral training. Our Viewpoint is related to A. I. Leshner's Editorial in Science last year, which focuses on a change in American and British government funding strategies toward new investigators in research [2]. Capacity Building at the Doctoral Level Our first example of training in Kenya at the doctoral level is based on the needs of the researcher's home institute. In this instance, the institutions use existing collaborations with Northern partners to secure funds to train students to enable technology transfer. This track is mainly project-driven, and at the end of their training young researchers are expected to return to their home institute. At the end of the training, though, there is a lack of an enabling environment: e.g., the laboratory facilities do not support the introduction of cutting-edge technology, therefore the skills that have been acquired cannot be transferred to the South. Moreover, the pay is not commensurate with their training due to the low demand for their new skills and higher qualifications, and hence they may opt to return to the North for better-paying jobs. For example, as a research assistant, the second author trained in a Northern collaborating institute to use de novo technology, with a Ph.D. as an incentive. Like other scientists found in this position, she opted to change an aspect of her research to advance her postdoctoral training in the local institutes upon her return. Both authors have also found that on returning to local institutions, there is pressure to become an independently funded scientist either"
}