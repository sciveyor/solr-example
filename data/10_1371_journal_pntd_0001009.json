{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001009",
  "doi": "10.1371/journal.pntd.0001009",
  "externalIds": [
    "pii:PNTD-D-10-00126"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "An Atlas for Schistosoma mansoni Organs and Life-Cycle Stages Using Cell Type-Specific Markers and Confocal Microscopy",
  "authors": [
    {
      "name": "James J. Collins III",
      "first": "James J.",
      "last": "Collins",
      "suffix": "III",
      "affiliation": "Howard Hughes Medical Institute, Department of Cell and Developmental Biology, Neuroscience Program, University of Illinois at Urbana-Champaign, Urbana, Illinois, United States of America"
    },
    {
      "name": "Ryan S. King",
      "first": "Ryan S.",
      "last": "King",
      "affiliation": "Howard Hughes Medical Institute, Department of Cell and Developmental Biology, Neuroscience Program, University of Illinois at Urbana-Champaign, Urbana, Illinois, United States of America"
    },
    {
      "name": "Alexis Cogswell",
      "first": "Alexis",
      "last": "Cogswell",
      "affiliation": "Department of Immunology/Microbiology, Rush University Medical Center, Chicago, Illinois, United States of America"
    },
    {
      "name": "David L. Williams",
      "first": "David L.",
      "last": "Williams",
      "affiliation": "Department of Immunology/Microbiology, Rush University Medical Center, Chicago, Illinois, United States of America"
    },
    {
      "name": "Phillip A. Newmark",
      "first": "Phillip A.",
      "last": "Newmark",
      "affiliation": "Howard Hughes Medical Institute, Department of Cell and Developmental Biology, Neuroscience Program, University of Illinois at Urbana-Champaign, Urbana, Illinois, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-03",
  "dateAccepted": "2010-12-15",
  "dateReceived": "2010-10-28",
  "volume": "5",
  "number": "3",
  "pages": "e1009",
  "tags": [
    "Biology",
    "Helminthology",
    "Parasitology",
    "Zoology"
  ],
  "abstract": "Schistosomiasis (bilharzia) is a tropical disease caused by trematode parasites (Schistosoma) that affects hundreds of millions of people in the developing world. Currently only a single drug (praziquantel) is available to treat this disease, highlighting the importance of developing new techniques to study Schistosoma. While molecular advances, including RNA interference and the availability of complete genome sequences for two Schistosoma species, will help to revolutionize studies of these animals, an array of tools for visualizing the consequences of experimental perturbations on tissue integrity and development needs to be made widely available. To this end, we screened a battery of commercially available stains, antibodies and fluorescently labeled lectins, many of which have not been described previously for analyzing schistosomes, for their ability to label various cell and tissue types in the cercarial stage of S. mansoni. This analysis uncovered more than 20 new markers that label most cercarial tissues, including the tegument, the musculature, the protonephridia, the secretory system and the nervous system. Using these markers we present a high-resolution visual depiction of cercarial anatomy. Examining the effectiveness of a subset of these markers in S. mansoni adults and miracidia, we demonstrate the value of these tools for labeling tissues in a variety of life-cycle stages. The methodologies described here will facilitate functional analyses aimed at understanding fundamental biological processes in these parasites.",
  "fullText": "Introduction Flatworms of the genus Schistosoma are parasites (Phylum Platyhelmithes) that currently infect over 200 million people worldwide [1]. Similar to other trematodes, Schistosoma have complex life cycles consisting of both free-living and parasitic forms [2]. Upon passage from a vertebrate, schistosome eggs that reach freshwater will hatch and produce miracidia, that swim by ciliary movement to locate and penetrate a suitable snail host. Following entry into the snail, miracidia undergo a dramatic developmental conversion, resulting in the production of primary and then secondary sporocysts, which have the capacity to generate thousands of cercariae. Cercariae are then liberated from the snail into freshwater where they can penetrate the epidermis of a vertebrate host. These animals (now called schistosomula) enter the host's circulatory system, eventually migrating to the liver where they feed on blood and develop to adulthood as either male or female worms. Separate sexed worms then pair, begin reproducing, and complete the life cycle by laying eggs that are passed via the urine or feces depending on the schistosome species. Despite the daunting complexity of the schistosome life cycle, great strides have been made in developing modern tools to study these parasites. RNA interference has been used to disrupt gene function in eggs [3], [4], transforming cercariae [5], schistosomula [6], [7], [8], and adults [3], [9]. Such breakthroughs have opened the possibility for large-scale RNAi-based screens [8], [10]. Furthermore, transgenic approaches [11], and whole-mount in situ hybridization [15] techniques have been described, providing additional avenues to analyze gene function. These tools, combined with access to complete genome sequences for S. mansoni [16] and S. japonicum [17], will facilitate a greater understanding of these parasites, making this an exciting time for schistosome research. To fully realize the potential of these newly developed functional genomic tools, however, methods for analyzing changes in cell/tissue morphology following experimental perturbations will need to be developed. Furthermore, a collection of reagents to label distinct organ systems in a variety of life-cycle stages will facilitate a greater understanding of the complex developmental transitions these animals experience. One successful methodology for examining schistosome anatomy has been the combination of fluorescence microscopy with histological stains, such as carmine red or phalloidin. For example, carmine staining has been used to describe the reproductive anatomy of paired vs. unpaired adult females [18], [19] and phalloidin has been utilized to describe the musculature of a number of life-cycle stages [20], [21], [22]. Although these reagents will continue to be valuable tools, they are limited by their individual specificities; thus, there is a need to identify additional markers that will allow high-resolution analyses of distinct schistosome cell types and tissues. Immunofluorescence microscopy has become an indispensable tool for detailed examination of tissue morphology during development and following experimental perturbation. Although species-specific antibodies are widely available for classic model organisms (e.g. Drosophila and mouse), only a limited set of these reagents have been generated for organisms with smaller research communities (e.g. Schistosoma). In emerging model systems such as planarians, a free-living relative of"
}