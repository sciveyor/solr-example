{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001179",
  "doi": "10.1371/journal.pntd.0001179",
  "externalIds": [
    "pii:10-PNTD-RA-1524"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Economic Impact of Cystic Echinococcosis in Peru",
  "authors": [
    {
      "name": "Pedro L. Moro",
      "first": "Pedro L.",
      "last": "Moro",
      "affiliation": "Division of Healthcare Quality Promotion, Immunization Safety Office, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Christine M. Budke",
      "first": "Christine M.",
      "last": "Budke",
      "affiliation": "College of Veterinary Medicine and Biomedical Sciences, Texas A&M University, College Station, Texas, United States of America"
    },
    {
      "name": "Peter M. Schantz",
      "first": "Peter M.",
      "last": "Schantz",
      "affiliation": "Rollins School of Public Health, Emory University, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Julio Vasquez",
      "first": "Julio",
      "last": "Vasquez",
      "affiliation": "Portneuf Medical Center and Idaho State University, Pocatello, Idaho, United States of America"
    },
    {
      "name": "Saul J. Santivañez",
      "first": "Saul J.",
      "last": "Santivañez",
      "affiliation": "Instituto Peruano de Parasitologia Clinica y Experimental, Lima, Peru"
    },
    {
      "name": "Jaime Villavicencio",
      "first": "Jaime",
      "last": "Villavicencio",
      "affiliation": "Servicio Nacional de Sanidad Agraria, Ministerio de Agricultura, Lima, Peru"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-05",
  "dateAccepted": "2011-04-06",
  "dateReceived": "2010-09-07",
  "volume": "5",
  "number": "5",
  "pages": "e1179",
  "tags": [
    "Epidemiology",
    "Global health",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Public Health and Epidemiology",
    "Public health",
    "Veterinary epidemiology",
    "Veterinary science"
  ],
  "abstract": "Background\n          \nCystic echinococcosis (CE) constitutes an important public health problem in Peru. However, no studies have attempted to estimate the monetary and non-monetary impact of CE in Peruvian society.\n\n          Methods\n          \nWe used official and published sources of epidemiological and economic information to estimate direct and indirect costs associated with livestock production losses and human disease in addition to surgical CE-associated disability adjusted life years (DALYs) lost.\n\n          Findings\n          \nThe total estimated cost of human CE in Peru was U.S.$2,420,348 (95% CI:1,118,384–4,812,722) per year. Total estimated livestock-associated costs due to CE ranged from U.S.$196,681 (95% CI:141,641–251,629) if only direct losses (i.e., cattle and sheep liver destruction) were taken into consideration to U.S.$3,846,754 (95% CI:2,676,181–4,911,383) if additional production losses (liver condemnation, decreased carcass weight, wool losses, decreased milk production) were accounted for. An estimated 1,139 (95% CI: 861–1,489) DALYs were also lost due to surgical cases of CE.\n\n          Conclusions\n          \nThis preliminary and conservative assessment of the socio-economic impact of CE on Peru, which is based largely on official sources of information, very likely underestimates the true extent of the problem. Nevertheless, these estimates illustrate the negative economic impact of CE in Peru.",
  "fullText": "Introduction Cystic echinococcosis (CE) is caused by infection with the larval stage of the cestode Echinococcus granulosus. CE has a cosmopolitan distribution and is an important public health problem in sheep-raising areas of South America [1]. CE has a serious impact on human health and livestock production in Peru as demonstrated by epidemiological studies in endemic villages of Peru that have shown human infection prevalences ranging from 5.5% to 9.1% (see Table 1) [2], [3], with the prevalence of CE in sheep and cattle as high as 77% and 68%, respectively [2], [4]. Despite these levels of infection, CE is not a notifiable disease in Peru. CE not only causes disability and occasional death in humans, but also results in monetary losses due to treatment costs, lost wages, and animal productivity losses. An economic analysis of the losses due to CE in Uruguay revealed an estimated minimum cost of U.S.$ 2.9 million per year in losses in livestock productivity together with the cost of hospital treatment of human cases [5]. Despite the high levels of CE in Peru, no study has explored the socioeconomic or financial impact of this disease in Peru. Studies to estimate the economic impact of CE are important as they can serve as tools to help prioritize those areas that may need to be targeted in a control program. Disability-adjusted life years (DALYs) have been used to quantify disease burden for a wide range of afflictions globally, and it was the primary metric used in the Global Burden of Disease (GBD) Study [6],[7]. DALYs offer a standardized, non-monetary measure of morbidity and mortality that is comparable across various conditions and geographic regions. The DALY also serves as a useful tool for resource allocation and cost-effectiveness analyses. Calculation of DALYs involves summing the years of life lost due to premature mortality and the years of a healthy life lost due to the disability caused by the infection in a specific population, accounting for the degree of incapacity resulting from various conditions (disability weights) and the relative importance of a healthy life at different ages (age-weights). DALYs have previously been applied to assess the economic impact of CE on a regional and global scale [8], [9]. Given the absence of data on this subject, this study was designed to provide a preliminary estimate of the socio-economic impact of CE in Peru. Materials and Methods Human CE epidemiological parameters Data on annual reported cases of CE, by age, gender, and by cyst location (Table 2), were obtained from official government and published sources [10–12; Ministry of Health of Peru, unpublished data]. Government agencies in Peru record the number of new CE cases seen in outpatient clinics in public healthcare facilities, but no information is currently collected by the government on the number of CE cases that undergo surgical treatment. In order to estimate the number of cases of CE that underwent surgery nationally, we applied the proportion of CE cases who underwent surgical treatment from a known endemic area"
}