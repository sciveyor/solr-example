{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001389",
  "doi": "10.1371/journal.pntd.0001389",
  "externalIds": [
    "pii:PNTD-D-11-00534"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Fatal Attraction Phenomenon in Humans – Cat Odour Attractiveness Increased for Toxoplasma-Infected Men While Decreased for Infected Women",
  "authors": [
    {
      "name": "Jaroslav Flegr",
      "first": "Jaroslav",
      "last": "Flegr",
      "affiliation": "Department of Biology, Faculty of Science, Charles University, Prague, Czech Republic"
    },
    {
      "name": "Pavlína Lenochová",
      "first": "Pavlína",
      "last": "Lenochová",
      "affiliation": "Department of Biology, Faculty of Science, Charles University, Prague, Czech Republic; Department of Anthropology, Faculty of Humanities, Charles University, Prague, Czech Republic"
    },
    {
      "name": "Zdeněk Hodný",
      "first": "Zdeněk",
      "last": "Hodný",
      "affiliation": "Department of Genome Integrity, Institute of Molecular Genetics ASCR, v.v.i, Prague, Czech Republic"
    },
    {
      "name": "Marta Vondrová",
      "first": "Marta",
      "last": "Vondrová",
      "affiliation": "Department of Anthropology, Faculty of Humanities, Charles University, Prague, Czech Republic"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-11",
  "dateAccepted": "2011-09-23",
  "dateReceived": "2011-06-07",
  "volume": "5",
  "number": "11",
  "pages": "e1389",
  "tags": [
    "Animal behavior",
    "Animal cognition",
    "Behavioral ecology",
    "Behavioral neuroscience",
    "Biology",
    "Ecology",
    "Evolutionary ecology",
    "Neuroscience",
    "Parasitology",
    "Sensory perception",
    "Zoology"
  ],
  "abstract": "Background\n          \nLatent toxoplasmosis, a lifelong infection with the protozoan Toxoplasma gondii, has cumulative effects on the behaviour of hosts, including humans. The most impressive effect of toxoplasmosis is the “fatal attraction phenomenon,” the conversion of innate fear of cat odour into attraction to cat odour in infected rodents. While most behavioural effects of toxoplasmosis were confirmed also in humans, neither the fatal attraction phenomenon nor any toxoplasmosis-associated changes in olfactory functions have been searched for in them.\n\n          Principal Findings\n          \nThirty-four Toxoplasma-infected and 134 noninfected students rated the odour of urine samples from cat, horse, tiger, brown hyena and dog for intensity and pleasantness. The raters were blind to their infection status and identity of the samples. No signs of changed sensitivity of olfaction were observed. However, we found a strong, gender dependent effect of toxoplasmosis on the pleasantness attributed to cat urine odour (p = 0.0025). Infected men rated this odour as more pleasant than did the noninfected men, while infected women rated the same odour as less pleasant than did noninfected women. Toxoplasmosis did not affect how subjects rated the pleasantness of any other animal species' urine odour; however, a non-significant trend in the same directions was observed for hyena urine.\n\n          Conclusions\n          \nThe absence of the effects of toxoplasmosis on the odour pleasantness score attributed to large cats would suggest that the amino acid felinine could be responsible for the fatal attraction phenomenon. Our results also raise the possibility that the odour-specific threshold deficits observed in schizophrenia patients could be caused by increased prevalence of Toxoplasma-infected subjects in this population rather than by schizophrenia itself. The trend observed with the hyena urine sample suggests that this carnivore, and other representatives of the Feliformia suborder, should be studied for their possible role as definitive hosts in the life cycle of Toxoplasma.",
  "fullText": "Introduction The protozoan parasite Toxoplasma gondii is well known for changing behaviour of intermediate hosts, for review see [1], [2]. In general, it is believed that specific behavioural changes represent a biological adaptation of the parasite aimed to enhance its transmission from an intermediate host (e.g. a rodent) to a definitive host (any cat species) by predation. Consequently, the infected rodents have prolonged reaction times [3], increased (rats) or decreased (mice) preference for novel stimuli [4]–[7], impaired learning ability [6], [8], [9], and increased activity [10], [11] including spontaneous activity in running wheel tests [6], [12]. Additionally, infected rats have a higher probability to be captured with traps [4], which is consistent with the results of the “predation experiments” performed with other taxonomically related parasites with similar life cycles (Sarcocystis and Frenkelia) [13]. The most impressive effect of Toxoplasma infection on the behaviour of rodent hosts is the so-called “fatal attraction phenomenon” [14]. In contrast with Toxoplasma-free rodents that have an innate fear of cat odour and avoid the places containing traces of cat urine, the Toxoplasma infected animals, both mice and rats, lose this fear and even gain attraction to cat odour instead. In the arena experiments, these animals spend more time in places containing a cat urine sample or piece of a worn cat collar. This effect is highly specific, the fatal attraction concerns only the odour of cat or bobcat and not of a rabbit, mink or dog [15]–[17]. The general qualities of olfaction as well as other fear or anxiety reactions of the infected animals remain unchanged [15]. Moreover, this specific effect of the Toxoplasma infection disappears when the infected animals are treated with drugs that specifically inhibit proliferation of Toxoplasma [17], [18]. In contrast with many other Toxoplasma infection-induced behavioural changes, the fatal attraction phenomenon is rather stable and remains detectable for a longer period of time than is the average life expectancy of rodents in their natural environment [19]. Toxoplasma infects any warm-blooded animal, including about one third of the human population of developed countries [20]. Thus, humans are very suitable models for studying possible manipulation activity of the Toxoplasma. Acute toxoplasmosis is a relatively mild and short-term disease in most of the immunocompetent subjects. Acute toxoplasmosis goes spontaneously into the latent phase, characterized by the lifelong presence of dormant stages of the parasite, the bradyzoites, mainly in the neural and muscular tissues, usually also by the lifelong presence of low but protective concentrations of specific antibodies in the serum of infected subjects [21]. Any behavioural changes that could be detected several years after the infection are either side-effects of latent infection or specific effects of manipulation activity of the parasite rather than carry over effects of the past acute phase of the infection [2]. This theoretical assumption was supported by evidence of positive correlation between the intensity of many behavioural changes and duration of the infection [22]–[25]. It was observed that infected humans have prolonged reaction times, increased risk of traffic accidents –"
}