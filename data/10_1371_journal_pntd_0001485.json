{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001485",
  "doi": "10.1371/journal.pntd.0001485",
  "externalIds": [
    "pii:PNTD-D-11-00551"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Virus Identification in Unknown Tropical Febrile Illness Cases Using Deep Sequencing",
  "authors": [
    {
      "name": "Nathan L. Yozwiak",
      "first": "Nathan L.",
      "last": "Yozwiak",
      "affiliation": "Division of Infectious Diseases and Vaccinology, School of Public Health, University of California, Berkeley, California, United States of America"
    },
    {
      "name": "Peter Skewes-Cox",
      "first": "Peter",
      "last": "Skewes-Cox",
      "affiliation": "Biological and Medical Informatics Program, University of California San Francisco, San Francisco, California, United States of America; Howard Hughes Medical Institute, University of California San Francisco, San Francisco, California, United States of America; Department of Biochemistry and Biophysics, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Mark D. Stenglein",
      "first": "Mark D.",
      "last": "Stenglein",
      "affiliation": "Howard Hughes Medical Institute, University of California San Francisco, San Francisco, California, United States of America; Department of Biochemistry and Biophysics, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Angel Balmaseda",
      "first": "Angel",
      "last": "Balmaseda",
      "affiliation": "Departamento de Virología, Centro Nacional de Diagnóstico y Referencia, Ministerio de Salud, Managua, Nicaragua"
    },
    {
      "name": "Eva Harris",
      "first": "Eva",
      "last": "Harris",
      "affiliation": "Division of Infectious Diseases and Vaccinology, School of Public Health, University of California, Berkeley, California, United States of America"
    },
    {
      "name": "Joseph L. DeRisi",
      "first": "Joseph L.",
      "last": "DeRisi",
      "affiliation": "Howard Hughes Medical Institute, University of California San Francisco, San Francisco, California, United States of America; Department of Biochemistry and Biophysics, University of California San Francisco, San Francisco, California, United States of America; Department of Medicine, University of California San Francisco, San Francisco, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-02",
  "dateAccepted": "2011-12-07",
  "dateReceived": "2011-06-09",
  "volume": "6",
  "number": "2",
  "pages": "e1485",
  "tags": [
    "Biology",
    "Computational Biology",
    "Computational biology",
    "Global health",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Microbiology",
    "Neglected tropical diseases",
    "Virology"
  ],
  "abstract": "Dengue virus is an emerging infectious agent that infects an estimated 50–100 million people annually worldwide, yet current diagnostic practices cannot detect an etiologic pathogen in ∼40% of dengue-like illnesses. Metagenomic approaches to pathogen detection, such as viral microarrays and deep sequencing, are promising tools to address emerging and non-diagnosable disease challenges. In this study, we used the Virochip microarray and deep sequencing to characterize the spectrum of viruses present in human sera from 123 Nicaraguan patients presenting with dengue-like symptoms but testing negative for dengue virus. We utilized a barcoding strategy to simultaneously deep sequence multiple serum specimens, generating on average over 1 million reads per sample. We then implemented a stepwise bioinformatic filtering pipeline to remove the majority of human and low-quality sequences to improve the speed and accuracy of subsequent unbiased database searches. By deep sequencing, we were able to detect virus sequence in 37% (45/123) of previously negative cases. These included 13 cases with Human Herpesvirus 6 sequences. Other samples contained sequences with similarity to sequences from viruses in the Herpesviridae, Flaviviridae, Circoviridae, Anelloviridae, Asfarviridae, and Parvoviridae families. In some cases, the putative viral sequences were virtually identical to known viruses, and in others they diverged, suggesting that they may derive from novel viruses. These results demonstrate the utility of unbiased metagenomic approaches in the detection of known and divergent viruses in the study of tropical febrile illness.",
  "fullText": "Introduction Viral infections pose a significant global health burden, especially in the developing world where most infectious disease deaths occur in children and are commonly due to preventable or treatable agents. Effective diagnostic and surveillance tools are crucial for reducing disability-adjusted-life-years (DALYs) due to infectious agents and for bolstering elimination and treatment programs [1]. Previously unrecognized and novel pathogens continually emerge due to globalization, climate change, and environmental encroachment, and pose important diagnostic challenges [2], [3]. Dengue virus (DENV) infection is the most common arthropod-borne viral disease of humans, with an estimated 50–100 million clinical infections occurring annually worldwide [4]. DENV infection manifests clinically as dengue fever or the more severe dengue hemorrhagic fever/dengue shock syndrome (DHF/DSS) [4]. The increased spread of dengue virus and its mosquito vectors in many subtropical regions over the past several decades, especially in Latin America and Asia [5], highlights the need for additional methods of dengue virus surveillance. Diagnosing dengue relies on detecting viral nucleic acid or antigens in the blood or confirming the presence of anti-DENV IgM and IgG antibodies and therefore traditionally depends on RT-PCR, ELISA, and viral cell culture methods [5]–[7]. Dengue diagnostics are of crucial importance due to its broad spectrum of clinical presentations, global emergence and spread, unique disease epidemiology, and possible clinical relation to other as-yet unknown tropical febrile pathogens. Traditional viral detection methods, such as serology, virus isolation, and PCR, are optimized for the detection of known agents [2]. However, novel and highly divergent viruses are not easily detected by approaches that rely on a priori sequence, antigen, or cell tropism knowledge. PCR-based assays that employ degenerate primers may successfully target conserved regions within related virus groups, but unlike bacteria, viruses lack universally conserved genetic regions, such as ribosomal RNA, that can be exploited to amplify all viruses [8]. Metagenomic analysis enables more systemic detection of both known and novel viral pathogens [9]–[12] and is approached through a variety of microarray and sequencing strategies [13], [14]. The Virochip is a pan-viral microarray platform that has been previously utilized in the detection and discovery of viruses from both human and animal samples [15]–[19]. Deep sequencing and shotgun sequencing of human clinical samples has been used for viral detection [20]–[23], novel virus discovery [24]–[27], and divergent virus genome recovery [28]. Viral metagenomic approaches have also been employed as a diagnostic supplement to pathogen detection as part of public health monitoring systems [22], but have been limited to shotgun sequencing of viral-enriched libraries and have yet to utilize deep sequencing data. Currently available sequencing platforms can generate millions to billions of sequencing reads per run, far exceeding large-scale shotgun sequencing [13]. Deep sequencing of clinical samples, in which hundreds of thousands to millions of sequencing reads are generated per sample, can be incorporated into stepwise virus detection pipelines [29]. Database searches using Basic Local Alignment Search Tool (BLAST) and other alignment tools [30] can be used to identify sequences in samples that correspond to known and novel viruses, including those"
}