{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000677",
  "doi": "10.1371/journal.pntd.0000677",
  "externalIds": [
    "pii:09-PNTD-RA-0392R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Novel Family of Cyst Proteins with Epidermal Growth Factor Repeats in Giardia lamblia",
  "authors": [
    {
      "name": "Pei-Wei Chiu",
      "first": "Pei-Wei",
      "last": "Chiu",
      "affiliation": "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China"
    },
    {
      "name": "Yu-Chang Huang",
      "first": "Yu-Chang",
      "last": "Huang",
      "affiliation": "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China"
    },
    {
      "name": "Yu-Jiao Pan",
      "first": "Yu-Jiao",
      "last": "Pan",
      "affiliation": "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China"
    },
    {
      "name": "Chih-Hung Wang",
      "first": "Chih-Hung",
      "last": "Wang",
      "affiliation": "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China"
    },
    {
      "name": "Chin-Hung Sun",
      "first": "Chin-Hung",
      "last": "Sun",
      "affiliation": "Department of Parasitology, College of Medicine, National Taiwan University, Taipei, Taiwan, Republic of China"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-05",
  "dateAccepted": "2010-03-23",
  "dateReceived": "2009-07-31",
  "volume": "4",
  "number": "5",
  "pages": "e677",
  "tags": [
    "Biochemistry/Cell Signaling and Trafficking Structures",
    "Cell Biology/Extra-Cellular Matrix",
    "Developmental Biology/Developmental Evolution",
    "Developmental Biology/Microbial Growth and Development",
    "Evolutionary Biology/Microbial Evolution and Genomics",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Infectious Diseases/Tropical and Travel-Associated Diseases"
  ],
  "abstract": "Background\n\nGiardia lamblia parasitizes the human small intestine to cause diarrhea and malabsorption. It undergoes differentiation from a pathogenic trophozoite form into a resistant walled cyst form. Few cyst proteins have been identified to date, including three cyst wall proteins (CWPs) and one High Cysteine Non-variant Cyst protein (HCNCp). They are highly expressed during encystation and are mainly targeted to the cyst wall.\n\nMethodology and Principal Findings\n\nTo identify new cyst wall proteins, we searched the G. lamblia genome data base with the sequence of the Cryptosporidium parvum oocyst wall protein as a query and found an Epidermal Growth Factor (EGF)-like Cyst Protein (EGFCP1). Sequence analysis revealed that the EGF-like repeats of the EGFCP1 are similar to those of the tenascin family of extracellular matrix glycoproteins. EGFCP1 and HCNCp have a higher percentage of cysteine than CWPs, but EGFCP1 has no C-terminal transmembrane region found in HCNCp. Like CWPs and HCNCp, the EGFCP1 protein (but not transcript) was expressed at higher levels during encystation and it was localized to encystation-specific vesicles in encysting trophozoites. Like HCNCp, EGFCP1 was localized to the encystation-specific vesicles, cyst wall and cell body of cysts, suggesting that they may share a common trafficking pathway. Interestingly, overexpression of EGFCP1 induced cyst formation and deletion of the signal peptide from EGFCP1 reduced its protein levels and cyst formation, suggesting that EGFCP1 may help mediate cyst wall synthesis. We also found that five other putative EGFCPs have similar expression profiles and similar locations and that the cyst formation was induced upon their overexpression.\n\nConclusions and Significance\n\nOur results suggest that EGFCPs may function like cyst wall proteins, involved in differentiation of G. lamblia trophozoites into cysts. The results lead to greater understanding of parasite cyst walls and provide valuable information that helps develop ways to interrupt the G. lamblia life cycle.",
  "fullText": "Introduction Giardia lamblia is a prevalent intestinal parasite causing waterborne diarrheal disease [1], [2]. G. lamblia trophozoite causes malabsorption and diarrhea without penetrating intestinal epithelial cells [3]. It has two synchronous nuclei, moves by the flagella and adheres via the ventral disk to the upper intestine of host, a place suitable for their proliferation [2]. When a trophozoite is carried downstream to the lower intestine, encystation occurs, cyst wall is formed and both nuclei divide simultaneously, resulting in a cyst with four nuclei [2]. Transmission of giardiasis arises when cysts are ingested from faecally contaminated food or water. The cyst form is the infective form capable of survival under hostile environments after excretion [2]. It has a resistant wall composed of proteins and polysaccharides to protect the parasite from hypotonic lysis by fresh water and from gastric acid during infection of the new host [4], [5]. The polysaccharide moiety is composed mainly of N-acetylgalactosamine homopolymer [6]. The strong interchain interactions of the polysaccharides and the strong interaction of the polysaccharide and the proteins may lead to a highly insoluble cyst wall [6]. The three known cyst wall proteins (CWPs) have similar expression levels, architectural motifs, and biological properties. Expression of the cwp1-3 genes and a gene encoding an enzyme in the cyst wall polysaccharide biosynthetic pathway (glucosamine-6-phosphate isomerase-B, G6PI-B) increases with similar kinetics during encystation [7]–[11], suggesting the importance of gene regulation at transcriptional and/or translational level. During encystation, CWPs are concentrated within large membrane-bounded encystation secretory vesicles (ESVs) before transport to the cyst wall [7]–[9]. The ESVs have been proposed to be the trans-Golgi network or Golgi equivalents of the other eukaryotic cells [12], [13]. All three CWPs have N-terminal signal peptides, four to five tandem leucine-rich repeats (LRRs) and &gt;14 positionally conserved cysteines [7]–[9]. Deletion of signal peptide or any one of LRRs prevented CWP3 from targeting to the ESVs or cyst wall [9]. Formation of intramolecular or intermolecular disulfide bonds between the cysteines may lead to heterooligomer formation between CWPs in the ESVs and cyst wall [9], [14]. Treatment of live encysting cells with DTT prevents the formation of disulfide bonds, ESVs and cyst wall [15], [13]. G. lamblia trophozoites are covered by variant surface proteins (VSPs) that are cysteine-rich type I integral membrane proteins, protecting them from protease and enzyme digestion in intestine [2], [16]. VSPs and CWPs have different expression patterns and subcellular localization profiles [17]. VSPs switch during vegetative growth or encystation and they are surface proteins that are transported to the trophozoite plasmalemma from endoplasmic reticulum (ER) pathway [17]. A High Cysteine Non-variant Cyst protein (HCNCp) has been identified recently [16]. It was originally annotated as a large VSP [16]. Like VSPs, it is a cysteine rich, acidic, type 1 integral membrane protein [16]. HCNCp has many CxC motifs that are rarely found in VSPs. Unlike CWPs, HCNCp has a higher molecular weight and many CxC or Cx2C motifs that are rarely found in CWPs and it does not have LRR motifs [7]–[9],"
}