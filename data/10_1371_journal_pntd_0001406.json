{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001406",
  "doi": "10.1371/journal.pntd.0001406",
  "externalIds": [
    "pii:PNTD-D-11-00316"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Single Dose Novel Salmonella Vaccine Enhances Resistance against Visceralizing L. major and L. donovani Infection in Susceptible BALB/c Mice",
  "authors": [
    {
      "name": "Juliane Schroeder",
      "first": "Juliane",
      "last": "Schroeder",
      "affiliation": "Institute of Immunology and Infection Research, University of Edinburgh, Edinburgh, United Kingdom"
    },
    {
      "name": "Najmeeyah Brown",
      "first": "Najmeeyah",
      "last": "Brown",
      "affiliation": "Centre for Immunology and Infection, Hull York Medical School and Department of Biology, University of York, York, United Kingdom"
    },
    {
      "name": "Paul Kaye",
      "first": "Paul",
      "last": "Kaye",
      "affiliation": "Centre for Immunology and Infection, Hull York Medical School and Department of Biology, University of York, York, United Kingdom"
    },
    {
      "name": "Toni Aebischer",
      "first": "Toni",
      "last": "Aebischer",
      "affiliation": "Institute of Immunology and Infection Research, University of Edinburgh, Edinburgh, United Kingdom; Robert Koch-Institute, Berlin, Germany"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-12",
  "dateAccepted": "2011-10-13",
  "dateReceived": "2011-04-04",
  "volume": "5",
  "number": "12",
  "pages": "e1406",
  "tags": [
    "Biology",
    "Immunity",
    "Immunizations",
    "Infectious Diseases",
    "Infectious diseases",
    "Leishmaniasis",
    "Medicine",
    "Microbiology",
    "Neglected tropical diseases",
    "Parasitic diseases"
  ],
  "abstract": "Visceral leishmaniasis is a major neglected tropical disease, with an estimated 500,000 new cases and more than 50,000 deaths attributable to this disease every year. Drug therapy is available but costly and resistance against several drug classes has evolved. Despite all efforts, no commercial, let alone affordable, vaccine is available to date. Thus, the development of cost effective, needle-independent vaccines is a high priority. Here, we have continued efforts to develop live vaccine carriers based on recombinant Salmonella. We used an in silico approach to select novel Leishmania parasite antigens from proteomic data sets, with selection criteria based on protein abundance, conservation across Leishmania species and low homology to host species. Five chosen antigens were differentially expressed on the surface or in the cytosol of Salmonella typhimurium SL3261. A two-step procedure was developed to select optimal Salmonella vaccine strains for each antigen, based on bacterial fitness and antigen expression levels. We show that vaccine strains of Salmonella expressing the novel Leishmania antigens LinJ08.1190 and LinJ23.0410 significantly reduced visceralisation of L. major and enhanced systemic resistance against L. donovani in susceptible BALB/c mice. The results show that Salmonella are valid vaccine carriers for inducing resistance against visceral leishmaniasis but that their use may not be suitable for all antigens.",
  "fullText": "Introduction The leishmaniases are regarded as neglected tropical diseases. The causative protozoan parasites are transmitted through the bite of sandfly vectors. Currently an estimated 12 million people are infected, while 350 million people in 88 countries worldwide are at risk to develop one of the diseases associated with Leishmania parasites (http://www.who.int/leishmaniasis/burden/en/; [1]). The most severe form is visceral leishmaniasis (VL; also known as kala azar in India) a disease that is fatal if untreated. An estimated 500 000 new cases and 50 000 deaths are reported every year, with 90% occurring in Bangladesh, Nepal, India, Sudan, Ethiopia and Brazil ([2]). VL caused by L. infantum/chagasi is zoonotic with dogs being the main reservoir; however, in areas endemic for L. donovani (e.g. India and Sudan) the disease is anthroponotic. In many cases infection remains asymptomatic, most likely indicating immune control. However, patients with symptomatic VL experience fever, fatigue, weight loss and weakness often accompanied by hepato-splenomegaly and anaemia and, if untreated, may die from bacterial co-infections, internal bleeding and anaemia (reviewed by (2]). Chemotherapy is available, but due to high toxicity, adverse side effects and emerging parasite resistance, treatment options are limited [3]–[6]. Long treatment regimens and associated costs are additional critical factors preventing patient access and compliance. For example paromomycin, a newly registered drug, is given by intra-muscular injections over a period of 21 days. Though the cheapest drug available, treatment still costs between 5 and 10 US$ per course, making this drug too expensive in relation to household income [5]. This economic burden of treatment is likely to remain for the foreseeable future. Thus, developing a vaccine for VL (and indeed for other forms of leishmaniasis) is high on the agenda of the World Health Assembly (resolution EB118.R3, Geneva 05/07). Vaccination is considered possible because of the efficacy of the century-old practice of leishmanization against old world cutaneous leishmaniasis (CL), a treatment that affords life long protection as proven during its large scale use to protect military personnel in Israel, Iran and the former Soviet Union [7]–[9]. However, in some individuals, development of non-healing lesions, exacerbation of chronic disease and immunosuppression as a result of this procedure has been observed [10]. The unsatisfactory safety profile, its questionable efficacy against infection with heterologous species and logistic hurdles render leishmanization problematic. Vaccines that relied on autoclaved or merthiolate-killed whole promastigotes formulated with or without Bacillus Calmette-Guerin as adjuvants were developed to remedy some of the shortcomings of leishmanization but a recent meta-analysis of clinical studies evaluating these vaccines did not support their efficacy [11]. Clinical testing of vaccines based on recombinant Leishmania antigens or fractionated parasite material is much less advanced, although numerous antigenic proteins have been shown to have vaccine potential in pre-clinical models (see reviews by [12]–[14]). These antigens were usually discovered by classical approaches, i.e. by screening with immune or hyperimmune sera from patients or infected animals. Antibody reactivity may not be an ideal criterion since protection is cell mediated and is thought to depend on both CD4+"
}