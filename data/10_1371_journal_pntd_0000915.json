{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000915",
  "doi": "10.1371/journal.pntd.0000915",
  "externalIds": [
    "pii:10-PNTD-RA-1529R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Sex, Subdivision, and Domestic Dispersal of Trypanosoma cruzi Lineage I in Southern Ecuador",
  "authors": [
    {
      "name": "Sofía Ocaña-Mayorga",
      "first": "Sofía",
      "last": "Ocaña-Mayorga",
      "affiliation": "Centro de Investigación en Enfermedades Infecciosas, Pontificia Universidad Católica del Ecuador, Quito, Ecuador"
    },
    {
      "name": "Martin S. Llewellyn",
      "first": "Martin S.",
      "last": "Llewellyn",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kindom"
    },
    {
      "name": "Jaime A. Costales",
      "first": "Jaime A.",
      "last": "Costales",
      "affiliation": "Centro de Investigación en Enfermedades Infecciosas, Pontificia Universidad Católica del Ecuador, Quito, Ecuador"
    },
    {
      "name": "Michael A. Miles",
      "first": "Michael A.",
      "last": "Miles",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kindom"
    },
    {
      "name": "Mario J. Grijalva",
      "first": "Mario J.",
      "last": "Grijalva",
      "affiliation": "Centro de Investigación en Enfermedades Infecciosas, Pontificia Universidad Católica del Ecuador, Quito, Ecuador; Biomedical Sciences Department, College of Osteopathic Medicine, Tropical Disease Institute, Ohio University, Athens, Ohio, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-12",
  "dateAccepted": "2010-11-15",
  "dateReceived": "2010-09-13",
  "volume": "4",
  "number": "12",
  "pages": "e915",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections"
  ],
  "abstract": "Background\n\nMolecular epidemiology at the community level has an important guiding role in zoonotic disease control programmes where genetic markers are suitably variable to unravel the dynamics of local transmission. We evaluated the molecular diversity of Trypanosoma cruzi, the etiological agent of Chagas disease, in southern Ecuador (Loja Province). This kinetoplastid parasite has traditionally been a paradigm for clonal population structure in pathogenic organisms. However, the presence of naturally occurring hybrids, mitochondrial introgression, and evidence of genetic exchange in the laboratory question this dogma.\n\nMethodology/Principal Findings\n\nEighty-one parasite isolates from domiciliary, peridomiciliary, and sylvatic triatomines and mammals were genotyped across 10 variable microsatellite loci. Two discrete parasite populations were defined: one predominantly composed of isolates from domestic and peridomestic foci, and another predominantly composed of isolates from sylvatic foci. Spatial genetic variation was absent from the former, suggesting rapid parasite dispersal across our study area. Furthermore, linkage equilibrium between loci, Hardy-Weinberg allele frequencies at individual loci, and a lack of repeated genotypes are indicative of frequent genetic exchange among individuals in the domestic/peridomestic population.\n\nConclusions/Significance\n\nThese data represent novel population-level evidence of an extant capacity for sex among natural cycles of T. cruzi transmission. As such they have dramatic implications for our understanding of the fundamental genetics of this parasite. Our data also elucidate local disease transmission, whereby passive anthropogenic domestic mammal and triatomine dispersal across our study area is likely to account for the rapid domestic/peridomestic spread of the parasite. Finally we discuss how this, and the observed subdivision between sympatric sylvatic and domestic/peridomestic foci, can inform efforts at Chagas disease control in Ecuador.",
  "fullText": "Introduction Chagas disease, caused by the protozoan Trypanosoma cruzi, is the most important parasitic infection in Latin America [1]. An estimated 10 million people carry the infection, while another 90 million live at risk [2]. This vector-borne zoonosis causes severely debilitating and potentially deadly disease in more than a third of infected people [3]. Mucosal or abrasion contact with the infected faeces of hematophagous triatomine bugs constitutes the major mode of transmission [2]. Chagas disease is endemic to several regions in Ecuador, including the warm inter-Andean valleys of the southern province of Loja, where the main vectors are Rhodnius ecuadoriensis, Triatoma carrioni, Panstrongylus chinai, and Panstrongylus rufotuberculatus [4], [5]. Loja Province is currently targeted by the Ecuadorian Chagas Disease Control Program. Complementing disease prevention efforts, recent progress has been made in understanding local vector dynamics [5]–[7]. However, parasite molecular epidemiology could also play a role in guiding effective intervention measures. Molecular diversity was first recognised in T. cruzi in the early 1970s [8]. Six major genetic subdivisions, known as discrete typing units (DTUs), are currently recognized (TcI–TcVI [9]), with distributions loosely defined by geography, transmission cycle, and ecology [1]. TcI predominates in northern South America, causes significant human disease [10], [11] and occurs in both domestic and sylvatic cycles of parasite transmission. Of major interest to those planning sustainable control strategies in this region is the extent to which these cycles are connected [12]–[14]. The provision of such data relies on the evaluation of molecular diversity ‘hidden’ at the sub-DTU level [15]–[17]. Hypervariable molecular markers, like microsatellites, have given new and unprecedented insight into the population genetics of other important parasitic zoonoses [18]–[22]. For the first time, specific hypotheses regarding parasite dispersal and reproduction can be addressed. However, the validity of molecular epidemiological data depends heavily on study design. Numerous confounders, including biased sampling (e.g., sampling only one host in a heteroxenous transmission system [23]), population subdivision in both space and time (leading to Wahlund effects [24]), and low sample size all influence the estimation of key population genetic parameters. Historically, such biases have acted as an impediment to obtaining useful epidemiological information from parasite molecular data, and, particularly in T. cruzi, to resolving the frequency of sex in natural populations. Here we present microsatellite data for 10 variable loci amplified from a large number of TcI isolates collected from domestic, peridomestic, and sylvatic hosts and vectors in and around several adjacent communities in Loja Province, Ecuador. We evaluate evidence for genetic subdivision between transmission cycles, anthropogenic dispersal of parasites between communities, and panmixia among a subset of strains. Methods Study area and sampling Sixteen communities in Loja Province, southern Ecuador, were sampled (Figure 1). These communities were located at altitudes less than 2,200 m and were representative of the ecological diversity of the province. Trypanosomes were isolated from triatomines and small mammals (rodents and opossums) captured at domestic (within dwellings), peridomestic (near dwellings and/or associated with human activities, e.g., crop stores, chicken roosts, wood and rock piles), and sylvatic"
}