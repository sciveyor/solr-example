{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000917",
  "doi": "10.1371/journal.pntd.0000917",
  "externalIds": [
    "pii:10-PNTD-RA-1320R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Revisiting the Immune Trypanolysis Test to Optimise Epidemiological Surveillance and Control of Sleeping Sickness in West Africa",
  "authors": [
    {
      "name": "Vincent Jamonneau",
      "first": "Vincent",
      "last": "Jamonneau",
      "affiliation": "Institut de Recherche pour le Développement (IRD), Unité Mixte de Recherche IRD-CIRAD 177, Montpellier, France; Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Bruno Bucheton",
      "first": "Bruno",
      "last": "Bucheton",
      "affiliation": "Institut de Recherche pour le Développement (IRD), Unité Mixte de Recherche IRD-CIRAD 177, Montpellier, France; Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Jacques Kaboré",
      "first": "Jacques",
      "last": "Kaboré",
      "affiliation": "Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Hamidou Ilboudo",
      "first": "Hamidou",
      "last": "Ilboudo",
      "affiliation": "Institut de Recherche pour le Développement (IRD), Unité Mixte de Recherche IRD-CIRAD 177, Montpellier, France; Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Oumou Camara",
      "first": "Oumou",
      "last": "Camara",
      "affiliation": "Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso; Programme National de Lutte contre la Trypanosomose Humaine Africaine, Conakry, Guinée"
    },
    {
      "name": "Fabrice Courtin",
      "first": "Fabrice",
      "last": "Courtin",
      "affiliation": "Institut de Recherche pour le Développement (IRD), Unité Mixte de Recherche IRD-CIRAD 177, Montpellier, France; Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Philippe Solano",
      "first": "Philippe",
      "last": "Solano",
      "affiliation": "Institut de Recherche pour le Développement (IRD), Unité Mixte de Recherche IRD-CIRAD 177, Montpellier, France; Centre International de Recherche-Développement sur l′Elevage en zones Subhumides (CIRDES), Unité de recherches sur les bases biologiques de la lutte intégrée, Bobo-Dioulasso, Burkina Faso"
    },
    {
      "name": "Dramane Kaba",
      "first": "Dramane",
      "last": "Kaba",
      "affiliation": "Institut Pierre Richet, Unité de Recherche « Trypanosomoses », Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Roger Kambire",
      "first": "Roger",
      "last": "Kambire",
      "affiliation": "Programme National de Lutte contre la Trypanosomose Humaine Africaine, Ouagadougou, Burkina Faso"
    },
    {
      "name": "Kouakou Lingue",
      "first": "Kouakou",
      "last": "Lingue",
      "affiliation": "Programme National d'Elimination de la Trypanosomose Humaine Africaine, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Mamadou Camara",
      "first": "Mamadou",
      "last": "Camara",
      "affiliation": "Programme National de Lutte contre la Trypanosomose Humaine Africaine, Conakry, Guinée"
    },
    {
      "name": "Rudy Baelmans",
      "first": "Rudy",
      "last": "Baelmans",
      "affiliation": "Institute of Tropical Medicine, Department of Parasitology, Antwerp, Belgium"
    },
    {
      "name": "Veerle Lejon",
      "first": "Veerle",
      "last": "Lejon",
      "affiliation": "Institute of Tropical Medicine, Department of Parasitology, Antwerp, Belgium"
    },
    {
      "name": "Philippe Büscher",
      "first": "Philippe",
      "last": "Büscher",
      "affiliation": "Institute of Tropical Medicine, Department of Parasitology, Antwerp, Belgium"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-12",
  "dateAccepted": "2010-11-15",
  "dateReceived": "2010-06-24",
  "volume": "4",
  "number": "12",
  "pages": "e917",
  "tags": [
    "Public Health and Epidemiology/Epidemiology"
  ],
  "abstract": "Background\n            \nBecause of its high sensitivity and its ease of use in the field, the card agglutination test for trypanosomiasis (CATT) is widely used for mass screening of sleeping sickness. However, the CATT exhibits false-positive results (i) raising the question of whether CATT-positive subjects who are negative in parasitology are truly exposed to infection and (ii) making it difficult to evaluate whether Trypanosoma brucei (T.b.) gambiense is still circulating in areas of low endemicity. The objective of this study was to assess the value of the immune trypanolysis test (TL) in characterising the HAT status of CATT-positive subjects and to monitor HAT elimination in West Africa.\n\n            Methodology/Principal Findings\n            \nTL was performed on plasma collected from CATT-positive persons identified within medical surveys in several West African HAT foci in Guinea, Côte d'Ivoire and Burkina Faso with diverse epidemiological statuses (active, latent, or historical). All HAT cases were TL+. All subjects living in a nonendemic area were TL−. CATT prevalence was not correlated with HAT prevalence in the study areas, whereas a significant correlation was found using TL.\n\n            Conclusion and Significance\n            \nTL appears to be a marker for contact with T.b. gambiense. TL can be a tool (i) at an individual level to identify nonparasitologically confirmed CATT-positive subjects as well as those who had contact with T.b. gambiense and should be followed up, (ii) at a population level to identify priority areas for intervention, and (iii) in the context of HAT elimination to identify areas free of HAT.",
  "fullText": "Introduction Human African trypanosomiasis (HAT) or sleeping sickness is caused by two subspecies of the protozoan flagellate Trypanosoma brucei. In West and Central Africa, T.b. gambiense causes the chronic form of sleeping sickness, while in East Africa, T.b. rhodesiense causes the more fulminant form [1]. T.b. brucei is normally not infectious to humans, like other species causing animal African trypanosomiasis (AAT) such as T. evansi, T. congolense, T. vivax and T. equiperdum. After the successful control campaigns dating from 1930 to 1960, T.b. gambiense sleeping sickness re-emerged in the 1980s, with tens of thousands of cases treated every year. As a result of control activities, reported cases decreased to a mere 11,382 patients in 2006 [2] and to less than the symbolic number of 10,000 in 2009 [3]. However, along with decreasing incidence, disease control efforts may be discontinued, thus allowing the epidemic to build up again [2]. At present, two West African countries are endemic for HAT [2], [4], [5]. Guinea is the most affected with about 100 HAT cases reported annually from the coastal mangroves. In Côte d'Ivoire, control activities since the 1980s [6] have resulted in a low disease prevalence with a few tens of HAT cases annually, mainly from the Central West foci. In Togo, Ghana, Benin, Mali and Burkina Faso, no autochthonous cases have been reported over the last few years. Although the epidemiological situation remains unknown in several countries, including Liberia and Sierra Leone, HAT elimination in West Africa seems attainable. Mass screening of the population at risk of T.b. gambiense is routinely performed using the card agglutination test for trypanosomiasis (CATT) on select individuals with antibodies against trypanosome antigens. CATT consists of bloodstream form trypomastigotes of T.b. gambiense variable antigen type (VAT) LiTat 1.3 purified from infected rat blood, fixed, stained and lyophilised [7]. When a drop of CATT reagent on a plastic card is mixed for 5 min with a drop of blood or diluted plasma or serum, the trypanosomes are agglutinated by antibodies that bind to the surface of the fixed cells resulting in a macroscopic agglutination reaction. Most of these antibodies will react with the VAT-specific epitopes on the cells. These highly immunogenic epitopes are present on the surface-exposed part of the densely packed variant surface glycoproteins (VSG). On living trypanosomes, only these VAT-specific epitopes are accessible for antibody binding. During the production of CATT reagent part of the VSG coat is shed and other epitopes on the VSG molecules that are not strictly VAT-specific, and from other surface proteins embedded between the VSGs, become available for antibody recognition and thus take part in the agglutination reaction [8]. This can lead to false-positive results, compromising the specificity of the test [9]. In the current elimination context in West Africa, when prevalence becomes low or transmission has stopped, the limited specificity of CATT becomes a considerable drawback because it results in low positive predictive values [10]–[12]. Recognising parasitologically unconfirmed but infected CATT-positive cases between many false-positives becomes problematic, since untreated, they"
}