{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001556",
  "doi": "10.1371/journal.pntd.0001556",
  "externalIds": [
    "pii:PNTD-D-11-00699"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Bacterial Diversity in Oral Samples of Children in Niger with Acute Noma, Acute Necrotizing Gingivitis, and Healthy Controls",
  "authors": [
    {
      "name": "Ignacio Bolivar",
      "first": "Ignacio",
      "last": "Bolivar",
      "affiliation": "Institut für Angewandte Immunologie, Zuchwil, Switzerland; GESNOMA, Unit of Plastic and Reconstructive Surgery, University of Geneva Hospitals, Geneva, Switzerland"
    },
    {
      "name": "Katrine Whiteson",
      "first": "Katrine",
      "last": "Whiteson",
      "affiliation": "Genomic Research Laboratory, University of Geneva Hospitals, Geneva, Switzerland"
    },
    {
      "name": "Benoît Stadelmann",
      "first": "Benoît",
      "last": "Stadelmann",
      "affiliation": "Institut für Angewandte Immunologie, Zuchwil, Switzerland; GESNOMA, Unit of Plastic and Reconstructive Surgery, University of Geneva Hospitals, Geneva, Switzerland"
    },
    {
      "name": "Denise Baratti-Mayer",
      "first": "Denise",
      "last": "Baratti-Mayer",
      "affiliation": "GESNOMA, Unit of Plastic and Reconstructive Surgery, University of Geneva Hospitals, Geneva, Switzerland"
    },
    {
      "name": "Yann Gizard",
      "first": "Yann",
      "last": "Gizard",
      "affiliation": "Genomic Research Laboratory, University of Geneva Hospitals, Geneva, Switzerland"
    },
    {
      "name": "Andrea Mombelli",
      "first": "Andrea",
      "last": "Mombelli",
      "affiliation": "GESNOMA, Unit of Plastic and Reconstructive Surgery, University of Geneva Hospitals, Geneva, Switzerland; Department of Periodontology and Oral Pathophysiology, School of Dental Medicine, University of Geneva Faculty of Medicine, Geneva, Switzerland"
    },
    {
      "name": "Didier Pittet",
      "first": "Didier",
      "last": "Pittet",
      "affiliation": "GESNOMA, Unit of Plastic and Reconstructive Surgery, University of Geneva Hospitals, Geneva, Switzerland; Infection Control Program and World Health Organization Collaborating Centre on Patient Safety, University of Geneva Hospitals and Faculty of Medicine, Geneva, Switzerland"
    },
    {
      "name": "Jacques Schrenzel",
      "first": "Jacques",
      "last": "Schrenzel",
      "affiliation": "GESNOMA, Unit of Plastic and Reconstructive Surgery, University of Geneva Hospitals, Geneva, Switzerland; Genomic Research Laboratory, University of Geneva Hospitals, Geneva, Switzerland; Clinical Microbiology Laboratory, University of Geneva Hospitals, Geneva, Switzerland"
    },
    {
      "name": "The Geneva Study Group on Noma (GESNOMA)"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-03",
  "dateAccepted": "2012-01-19",
  "dateReceived": "2011-07-15",
  "volume": "6",
  "number": "3",
  "pages": "e1556",
  "tags": [
    "Clinical genetics",
    "Clinical immunology",
    "Genetics and Genomics",
    "Immunology",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Oral medicine"
  ],
  "abstract": "Background\n          \nNoma is a gangrenous disease that leads to severe disfigurement of the face with high morbidity and mortality, but its etiology remains unknown. Young children in developing countries are almost exclusively affected. The purpose of the study was to record and compare bacterial diversity in oral samples from children with or without acute noma or acute necrotizing gingivitis from a defined geographical region in Niger by culture-independent molecular methods.\n\n          Methods and Principal Findings\n          \nGingival samples from 23 healthy children, nine children with acute necrotizing gingivitis, and 23 children with acute noma (both healthy and diseased oral sites) were amplified using “universal” PCR primers for the 16 S rRNA gene and pooled according to category (noma, healthy, or acute necrotizing gingivitis), gender, and site status (diseased or control site). Seven libraries were generated. A total of 1237 partial 16 S rRNA sequences representing 339 bacterial species or phylotypes at a 98–99% identity level were obtained. Analysis of bacterial composition and frequency showed that diseased (noma or acute necrotizing gingivitis) and healthy site bacterial communities are composed of similar bacteria, but differ in the prevalence of a limited group of phylotypes. Large increases in counts of Prevotella intermedia and members of the Peptostreptococcus genus are associated with disease. In contrast, no clear-cut differences were found between noma and non-noma libraries.\n\n          Conclusions\n          \nSimilarities between acute necrotizing gingivitis and noma samples support the hypothesis that the disease could evolve from acute necrotizing gingivitis in certain children for reasons still to be elucidated. This study revealed oral microbiological patterns associated with noma and acute necrotizing gingivitis, but no evidence was found for a specific infection-triggering agent.",
  "fullText": "Introduction Noma (cancrum oris) is a rapid and disfiguring gangrenous disease of the face with high morbidity and mortality [1]. Although it affects young children in developing countries almost exclusively, it was widespread before access to vaccines and improved nutrition became common. Noma has also been observed in HIV-infected individuals and patients with leukemia in developed countries, and occurred in concentration camps during World War II. There are no documented cases of other children living in the same village, siblings, or even identical twins developing noma at the same time, and it is impossible to transfer the infection to animal models [1]. Several factors have been linked to the disease, including malnutrition, immune dysfunction, lack of oral hygiene, and lesions of the mucosal gingival barrier, particularly the presence of acute necrotizing gingivitis (ANG) [2]–[4]. However, the etiology of noma remains poorly understood [1]–[3], [5]–[8]. While it seems reasonable to assume that noma is a multifactorial disease, it has long been suggested that bacteria are essential to the development of this condition. Noma infections respond to antibiotic treatment and the type of necrosis and odor of the lesions have been identified as bacterial in origin [1]. Studies have reported the presence of certain types of microorganisms in samples taken from noma cases. These include Prevotella melaninogenica, Actinomyces pyogenes, Fusobacterium nucleatum, Bacteroides fragilis, Bacillus cereus, P. intermedia, and F. necrophorum [5], [6], [9], with the latter two considered by some authors as significant pathogens in the etiology of noma. A study of advanced noma lesions in four children in Nigeria sequenced 212 16 s rRNA genes using culture-independent methods and found a great diversity of known oral and novel microbes, including those normally associated with soil and other non-human environments [10]. However, no clear association of a particular species with disease was observed in these patients. Often identified in all forms of periodontal disease and necrotizing gingivitis, P. intermedia has been viewed as a potential causative pathogen due to its frequent recovery from noma lesions using conventional culture techniques [5], [6]. F. necrophorum, a predominant animal pathogen, has attracted particular attention because of its association with necrotizing infections in both humans and animals. Anaerobic bacteria including Fusobacterium spp. and Prevotella spp. are known to become established in human infant mouths at around the same time as tooth eruption [11]. In at least one study, F. necrophorum was not found in the normal microbiota in malnourished Nigerian children without noma [9], while it was identified by culture methods in affected children [5], [9]. It is found in the gut of herbivores and can persist in wet soil with a high manure content on land used by cattle and sheep [12]. F. necrophorum infections develop in livestock when skin is damaged by wet or cold conditions, during tooth eruption, and other circumstances [12]–[14]. Fecal contamination in populations living close to cattle could be a source of F. necrophorum in noma infections [6]. Establishing the role of specific microorganisms in the pathogenesis of noma is"
}