{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001653",
  "doi": "10.1371/journal.pntd.0001653",
  "externalIds": [
    "pii:PNTD-D-11-01032"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Detection, Isolation and Confirmation of Crimean-Congo Hemorrhagic Fever Virus in Human, Ticks and Animals in Ahmadabad, India, 2010–2011",
  "authors": [
    {
      "name": "Devendra T. Mourya",
      "first": "Devendra T.",
      "last": "Mourya",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Pragya D. Yadav",
      "first": "Pragya D.",
      "last": "Yadav",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Anita M. Shete",
      "first": "Anita M.",
      "last": "Shete",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Yogesh K. Gurav",
      "first": "Yogesh K.",
      "last": "Gurav",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Chandrashekhar G. Raut",
      "first": "Chandrashekhar G.",
      "last": "Raut",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Ramesh S. Jadi",
      "first": "Ramesh S.",
      "last": "Jadi",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Shailesh D. Pawar",
      "first": "Shailesh D.",
      "last": "Pawar",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    },
    {
      "name": "Stuart T. Nichol",
      "first": "Stuart T.",
      "last": "Nichol",
      "affiliation": "Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Akhilesh C. Mishra",
      "first": "Akhilesh C.",
      "last": "Mishra",
      "affiliation": "National Institute of Virology, Pashan, Pune, India"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-05",
  "dateAccepted": "2012-04-09",
  "dateReceived": "2011-09-26",
  "volume": "6",
  "number": "5",
  "pages": "e1653",
  "tags": [
    "Biology"
  ],
  "abstract": "Background\n          \nIn January 2011, human cases with hemorrhagic manifestations in the hospital staff were reported from a tertiary care hospital in Ahmadabad, India. This paper reports a detailed epidemiological investigation of nosocomial outbreak from the affected area of Ahmadabad, Gujarat, India.\n\n          Principal Findings\n          \nSamples from 3 suspected cases, 83 contacts, Hyalomma ticks and livestock were screened for Crimean-Congo hemorrhagic fever (CCHF) virus by qRT-PCR of which samples of two medical professionals (case C and E) and the husband of the index case (case D) were positive for CCHFV. The sensitivity and specificity of indigenous developed IgM ELISA to screen CCHFV specific antibodies in human serum was 75.0% and 97.5% respectively as compared to commercial kit. About 17.0% domestic animals from Kolat, Ahmadabad were positive for IgG antibodies while only two cattle and a goat showed positivity by qRT-PCR. Surprisingly, 43.0% domestic animals (Buffalo, cattle, sheep and goat) showed IgG antibodies in the adjoining village Jivanpara but only one of the buffalo was positive for CCHFV. The Hyalomma anatolicum anatolicum ticks were positive in PCR and virus isolation. CCHFV was isolated from the blood sample of case C, E in Vero E-6 cells and Swiss albino mice. In partial nucleocapsid gene phylogeny from CCHFV positive human samples of the years 2010 and 2011, livestock and ticks showed this virus was similar to Tajikistan (strain TAJ/H08966), which belongs in the Asian/middle east genetic lineage IV.\n\n          Conclusions\n          \nThe likely source of CCHFV was identified as virus infected Hyalomma ticks and livestock at the rural village residence of the primary case (case A). In addition, retrospective sample analysis revealed the existence of CCHFV in Gujarat and Rajasthan states before this outbreak. An indigenous developed IgM ELISA kit will be of great use for screening this virus in India.",
  "fullText": "Introduction Crimean-Congo hemorrhagic fever (CCHF) is a severe acute febrile illness caused by the CCHF virus (CCHFV, family Bunyaviridae, genus Nairovirus), with overall case fatality of 9–50% [1]. CCHF was first recognized in the Crimean peninsula in the mid-1940s, [2], but the virus was first isolated from a patient in Kisangani, Democratic Republic of Congo, in 1956 [3]. Person-to-person transmission of CCHFV occurs through direct exposure to blood or other secretions, and instances of nosocomial transmission are well-documented [1]. The virus is maintained in nature predominantly in the Ixodid tick vectors, particularly ticks of the genus Hyalomma [4], [5]. CCHFV can persist in the tick throughout its life stages by transtadial transmission, and can be passed onto the offspring by transovarial transmission [5]. Among domestic animals, cattle, sheep, and goat play an important role in the natural cycle of the virus [6]. In these animals, CCHFV replicates to high titres in the lung, liver, spleen, and reticuloendothelial system in other organs [7], but generally causes only subclinical disease. In contrast, human infections often result in severe hemorrhagic fever (HF), with high levels of viral replication occurring in all major organs, including the liver [8]. In recent years, a number of zoonotic viral diseases have emerged in Southeast Asia as Nipah virus, and CCHFV [9], [10], [11], [12]. CCHF was recently confirmed for the first time in India, although the virus had been identified nearby in Pakistan and western China [11], [12]. In January 2011, human cases with hemorrhagic manifestations in the hospital staff were reported from a tertiary care hospital in Ahmadabad, Gujarat. The clinical samples of three hospitalized patients were referred to National Institute of Virology (NIV), Pune and laboratory investigations confirmed as CCHFV [13]. Here, we report detection and isolation of CCHFV associated with that nosocomial outbreak in Gujarat, India, and the presence of the virus in livestock and ticks in this region. The disease is newly recognized in India. It is required to create awareness about this disease in public health workers and physicians. CCHFV symptoms are difficult to distinguish not only from other HF, but that the real challenge is to distinguish the signs and symptoms from other, more common, febrile diseases. Materials and Methods Ethics Statement NIV, Pune is responsible for investigation of viral disease outbreaks of human including Zoonosis in India. As per the mandate of our institute, collection of the clinical samples from different species of animals for viral isolation and detection is required. This institute has policy to take approval on the projects which involves animals from the national committee called “Committee for the Purpose of Control &amp; Supervision of Experiments on Animals (CPCSEA) under the Ministry of Environment and Forests, Government of India. Our study project No. HCL01/NIV15/2010 is approved by the Institutional Animal Ethical Committee (IAEC) permitting the use of infant and adult mice as laboratory animals for isolation of virus and development of antibodies respectively. The present nosocomial CCHF outbreak was informed to Institutional Human Ethical Committee, NIV Pune."
}