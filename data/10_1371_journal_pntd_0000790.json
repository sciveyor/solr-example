{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000790",
  "doi": "10.1371/journal.pntd.0000790",
  "externalIds": [
    "pii:10-PNTD-RA-0945R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "FMRFamide-Like Peptides (FLPs) Enhance Voltage-Gated Calcium Currents to Elicit Muscle Contraction in the Human Parasite Schistosoma mansoni",
  "authors": [
    {
      "name": "Ekaterina Novozhilova",
      "first": "Ekaterina",
      "last": "Novozhilova",
      "affiliation": "Department of Biomedical Sciences and Neuroscience Program, Iowa State University, Ames, Iowa, United States of America"
    },
    {
      "name": "Michael J. Kimber",
      "first": "Michael J.",
      "last": "Kimber",
      "affiliation": "Department of Biomedical Sciences and Neuroscience Program, Iowa State University, Ames, Iowa, United States of America"
    },
    {
      "name": "Hai Qian",
      "first": "Hai",
      "last": "Qian",
      "affiliation": "Department of Biomedical Sciences and Neuroscience Program, Iowa State University, Ames, Iowa, United States of America"
    },
    {
      "name": "Paul McVeigh",
      "first": "Paul",
      "last": "McVeigh",
      "affiliation": "School of Biological Sciences, Queen's University Belfast, Belfast, Northern Ireland, United Kingdom"
    },
    {
      "name": "Alan P. Robertson",
      "first": "Alan P.",
      "last": "Robertson",
      "affiliation": "Department of Biomedical Sciences and Neuroscience Program, Iowa State University, Ames, Iowa, United States of America"
    },
    {
      "name": "Mostafa Zamanian",
      "first": "Mostafa",
      "last": "Zamanian",
      "affiliation": "Department of Biomedical Sciences and Neuroscience Program, Iowa State University, Ames, Iowa, United States of America"
    },
    {
      "name": "Aaron G. Maule",
      "first": "Aaron G.",
      "last": "Maule",
      "affiliation": "School of Biological Sciences, Queen's University Belfast, Belfast, Northern Ireland, United Kingdom"
    },
    {
      "name": "Tim A. Day",
      "first": "Tim A.",
      "last": "Day",
      "affiliation": "Department of Biomedical Sciences and Neuroscience Program, Iowa State University, Ames, Iowa, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-08",
  "dateAccepted": "2010-07-12",
  "dateReceived": "2010-03-08",
  "volume": "4",
  "number": "8",
  "pages": "e790",
  "tags": [
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Pharmacology",
    "Pharmacology/Drug Resistance",
    "Physiology/Cell Signaling",
    "Physiology/Integrative Physiology",
    "Physiology/Motor Systems"
  ],
  "abstract": "Schistosomes are amongst the most important and neglected pathogens in the world, and schistosomiasis control relies almost exclusively on a single drug. The neuromuscular system of schistosomes is fertile ground for therapeutic intervention, yet the details of physiological events involved in neuromuscular function remain largely unknown. Short amidated neuropeptides, FMRFamide-like peptides (FLPs), are distributed abundantly throughout the nervous system of every flatworm examined and they produce potent myoexcitation. Our goal here was to determine the mechanism by which FLPs elicit contractions of schistosome muscle fibers. Contraction studies showed that the FLP Tyr-Ile-Arg-Phe-amide (YIRFamide) contracts the muscle fibers through a mechanism that requires Ca2+ influx through sarcolemmal voltage operated Ca2+ channels (VOCCs), as the contractions are inhibited by classical VOCC blockers nicardipine, verapamil and methoxyverapamil. Whole-cell patch-clamp experiments revealed that inward currents through VOCCs are significantly and reversibly enhanced by the application of 1 µM YIRFamide; the sustained inward currents were increased to 190% of controls and the peak currents were increased to 180%. In order to examine the biochemical link between the FLP receptor and the VOCCs, PKC inhibitors calphostin C, RO 31–8220 and chelerythrine were tested and all produced concentration dependent block of the contractions elicited by 1 µM YIRFamide. Taken together, the data show that FLPs elicit contractions by enhancing Ca2+ influx through VOCC currents using a PKC-dependent pathway.",
  "fullText": "Introduction Schistosomes continue to inflict widespread suffering, infecting over 200 million worldwide, yet a proportional lack of scientific attention leaves schistosomiasis as one of the most prevalent neglected tropical diseases [1], [2]. Significant advances in schistosomiasis control have been achieved in the past few years, due primarily to well-organized control programmes that include aggressive chemotherapy [3], [4]. While these programmes produce unquestionable public health benefit, their assertive use of praziquantel raises concern about the potential emergence of resistance to the drug, which is the only antischistosomal available in most of the world. The schistosomiasis community has recognized the urgent need for new chemotherapeutic agents [5], [6]. Current approaches to drug discovery are significantly hindered by large gaps in our understanding of the basic biology of schistosomes at a molecular level. For example, even though most anthelmintic drugs have effects on the neuromuscular systems of worms, very little is known about the specifics of neuromuscular anatomy and physiology, nor the mechanisms involved in neuromuscular control. The neuropeptidergic system in parasitic flatworms is known to be remarkably rich [7], [8]. Flatworm Phe-Met-Arg-Phe-amide (FMRFamide)-like peptides (FLPs), one particular group of Arg-Phe-amides (RFamides), are known to play a central role in parasite neuromuscular biology; they are widespread throughout the nervous system of every flatworm examined, and they produce potent excitation of flatworm muscle [9], [10], [11], [12], [13], [14]. The RFamides present in vertebrates are structurally dissimilar to those in helminths, suggesting that flatworm FLPs and associated signaling molecules represent opportune targets for therapeutic control. Our objective here was to discover the mechanism by which these FLPs induce contraction of schistosome muscle fibers. Materials and Methods Parasites and Muscle Fiber Dispersion The studies here are focused on muscle fibers dispersed from adult Schistosoma mansoni. The fibers are obtained from adult worms recovered 45-60 days post-infection from the portal and mesenteric veins of female mice supplied by the Biomedical Research Institute, Rockville, MD USA [15]. Iowa State University's Institutional Animal Care and Use Committee (IACUC) approved the role of the mice. The incubation medium used for the fibers was based on a modified Dulbecco's Modified Eagle's Medium (DMEM, Sigma-Aldrich, USA) which was diluted to 67% of normal concentration and to which were added the following: 2.2 mM CaCl2, 2.7 mM MgSO4, 0.04 mM Na2HPO4, 61 mM glucose, 1.0 mM dithiothreitol, 10 µM serotonin, 1% gentamicin solution (Roche Applied Science, USA), 15 mM 4-(2-hydroxyethyl)-1-piperazine ethanesulfonic acid (HEPES) (pH 7.4). The dispersion medium was this same medium to which was further added 1 mM ethyleneglycol-bis-(β-aminoethyl ether) N, N, N' N'-tetraacetic acid (EGTA), 1 mM ethylenediamine tetraacetic acid (EDTA), and 0.1% bovine serum albumin. Briefly, live worms (25–30 pairs) were rinsed four times with the dispersion medium described above and then chopped approximately 35–40 times with a razor blade. Resultant worm pieces were rinsed four times, then incubated for 30 min at 37°C on a shaker table with the dispersion medium with added papain (1 mg/ml, Roche Applied Science, USA). After the enzymatic medium was removed, the worm"
}