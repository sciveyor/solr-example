{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001632",
  "doi": "10.1371/journal.pntd.0001632",
  "externalIds": [
    "pii:PNTD-D-11-00791"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Linking Oviposition Site Choice to Offspring Fitness in Aedes aegypti: Consequences for Targeted Larval Control of Dengue Vectors",
  "authors": [
    {
      "name": "Jacklyn Wong",
      "first": "Jacklyn",
      "last": "Wong",
      "affiliation": "Department of Entomology, University of California Davis, Davis, California, United States of America"
    },
    {
      "name": "Amy C. Morrison",
      "first": "Amy C.",
      "last": "Morrison",
      "affiliation": "Department of Entomology, University of California Davis, Davis, California, United States of America; United States Naval Medical Research Center Unit-6, Lima, Peru"
    },
    {
      "name": "Steven T. Stoddard",
      "first": "Steven T.",
      "last": "Stoddard",
      "affiliation": "Department of Entomology, University of California Davis, Davis, California, United States of America; Fogarty International Center, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Helvio Astete",
      "first": "Helvio",
      "last": "Astete",
      "affiliation": "United States Naval Medical Research Center Unit-6, Lima, Peru"
    },
    {
      "name": "Yui Yin Chu",
      "first": "Yui Yin",
      "last": "Chu",
      "affiliation": "Department of Entomology, University of California Davis, Davis, California, United States of America"
    },
    {
      "name": "Imaan Baseer",
      "first": "Imaan",
      "last": "Baseer",
      "affiliation": "Department of Entomology, University of California Davis, Davis, California, United States of America"
    },
    {
      "name": "Thomas W. Scott",
      "first": "Thomas W.",
      "last": "Scott",
      "affiliation": "Department of Entomology, University of California Davis, Davis, California, United States of America; Fogarty International Center, National Institutes of Health, Bethesda, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-05",
  "dateAccepted": "2012-03-19",
  "dateReceived": "2011-08-10",
  "volume": "6",
  "number": "5",
  "pages": "e1632",
  "tags": [
    "Behavioral ecology",
    "Biology",
    "Dengue fever",
    "Disease ecology",
    "Ecology",
    "Global health",
    "Infectious Diseases",
    "Infectious disease control",
    "Infectious diseases",
    "Medicine",
    "Mosquitoes",
    "Neglected tropical diseases",
    "Public Health and Epidemiology",
    "Public health",
    "Vectors and hosts"
  ],
  "abstract": "Background\n          \nCurrent Aedes aegypti larval control methods are often insufficient for preventing dengue epidemics. To improve control efficiency and cost-effectiveness, some advocate eliminating or treating only highly productive containers. The population-level outcome of this strategy, however, will depend on details of Ae. aegypti oviposition behavior.\n\n          Methodology/Principal Findings\n          \nWe simultaneously monitored female oviposition and juvenile development in 80 experimental containers located across 20 houses in Iquitos, Peru, to test the hypothesis that Ae. aegypti oviposit preferentially in sites with the greatest potential for maximizing offspring fitness. Females consistently laid more eggs in large vs. small containers (β = 9.18, p&lt;0.001), and in unmanaged vs. manually filled containers (β = 5.33, p&lt;0.001). Using microsatellites to track the development of immature Ae. aegypti, we found a negative correlation between oviposition preference and pupation probability (β = −3.37, p&lt;0.001). Body size of emerging adults was also negatively associated with the preferred oviposition site characteristics of large size (females: β = −0.19, p&lt;0.001; males: β = −0.11, p = 0.002) and non-management (females: β = −0.17, p&lt;0.001; males: β = −0.11, p&lt;0.001). Inside a semi-field enclosure, we simulated a container elimination campaign targeting the most productive oviposition sites. Compared to the two post-intervention trials, egg batches were more clumped during the first pre-intervention trial (β = −0.17, P&lt;0.001), but not the second (β = 0.01, p = 0.900). Overall, when preferred containers were unavailable, the probability that any given container received eggs increased (β = 1.36, p&lt;0.001).\n\n          Conclusions/Significance\n          \nAe. aegypti oviposition site choice can contribute to population regulation by limiting the production and size of adults. Targeted larval control strategies may unintentionally lead to dispersion of eggs among suitable, but previously unoccupied or under-utilized containers. We recommend integrating targeted larval control measures with other strategies that leverage selective oviposition behavior, such as luring ovipositing females to gravid traps or egg sinks.",
  "fullText": "Introduction At present, dengue virus transmission can be controlled or prevented only through suppressing mosquito vector populations [1]. Even with the advent of a licensed dengue vaccine, which is anticipated by 2015 [2], vector control will remain a necessary component of any sustainable program to eliminate dengue transmission in endemic areas or prevent virus introduction into new areas [3]. Unfortunately, few contemporary dengue control programs have achieved the high thresholds of vector population suppression (estimated to be &gt;90% at some locations [4], [5]) needed to prevent epidemics [6]. Controlling Aedes aegypti, the primary dengue vector worldwide, is challenging because it is well-adapted to the domestic environment [7], [8]. Adult mosquitoes rest indoors on clothing and underneath furniture, where they are difficult to reach using traditional aerosol or residual insecticides [7], [9]. Furthermore, females deposit their eggs in a wide assortment of man-made containers, ranging from water storage drums to discarded bottles and cans, making exhaustive larval control impractical in most cases [4], [10], [11]. Ae. aegypti productivity tends to be clustered at most field locations, with the majority of the adult population emerging from a small subset of water-holding containers [10]–[12]. Thus, targeting larviciding and container elimination efforts to these most productive containers may substantially improve the efficiency and cost-effectiveness of dengue control [13]. Proponents of targeted larval control predict that elimination of containers producing, for example, 80% of pupae will lead to a sustained linear reduction in the total adult density [10]. This expectation is based, however, upon two key assumptions: (1) all available Ae. aegypti larval development sites are already at carrying capacity and (2) oviposition behavior has little impact on population dynamics [10]. Field evaluations of targeted larval control programs have yielded mixed outcomes. Investigators in Myanmar and the Philippines reported nearly linear reductions (73–77%) in the Ae. aegypti Pupae per Person Index (PPI) after 5 months [12]. In Thailand, however, only a 15% reduction in PPI was observed after implementing a targeted control campaign designed to eliminate 80% of pupal production. In Iquitos, Peru, a 236% increase in PPI was noted after an intervention designed to eliminate 92% of pupal production [12]. Thus, the efficacy of targeted larval control varies substantially between settings and likely depends upon details of Ae. aegypti ecology and population dynamics at the local scale. Selection of an oviposition site by a female mosquito directly affects offspring survival and growth [14]–[16], and has consequences for population dynamics [17]. Because evolutionary theory predicts that animals should act to maximize their reproductive success, egg-laying females are expected to select the most suitable sites for their offspring based on reliable cues of habitat quality [18]–[20]. Whether and how female Ae. aegypti select oviposition sites, the impact of oviposition decisions on offspring fitness, and how females adjust to changes in oviposition site availability will affect the validity of the two key assumptions underlying targeted larval control. Previously, we demonstrated that free-ranging Ae. aegypti in Iquitos actively select egg-laying sites [21]. In particular, females exhibited a preference"
}