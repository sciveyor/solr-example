{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000895",
  "doi": "10.1371/journal.pntd.0000895",
  "externalIds": [
    "pii:10-PNTD-RA-1094R4"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Elimination of Active Trachoma after Two Topical Mass Treatments with Azithromycin 1.5% Eye Drops",
  "authors": [
    {
      "name": "Abdou Amza",
      "first": "Abdou",
      "last": "Amza",
      "affiliation": "Faculté des Sciences de la Santé, Niamey, Niger"
    },
    {
      "name": "Pablo Goldschmidt",
      "first": "Pablo",
      "last": "Goldschmidt",
      "affiliation": "Laboratoire du CHNO des Quinze Vingts, Paris, France"
    },
    {
      "name": "Ellen Einterz",
      "first": "Ellen",
      "last": "Einterz",
      "affiliation": "Kolofata District Health Service, Kolofata, Cameroon"
    },
    {
      "name": "Pierre Huguet",
      "first": "Pierre",
      "last": "Huguet",
      "affiliation": "Laboratoires THEA, Clermont-Ferrand, France"
    },
    {
      "name": "Celine Olmiere",
      "first": "Celine",
      "last": "Olmiere",
      "affiliation": "Laboratoires THEA, Clermont-Ferrand, France"
    },
    {
      "name": "Philippe Bensaid",
      "first": "Philippe",
      "last": "Bensaid",
      "affiliation": "Ophtalmo Sans Frontières, Luçon, France"
    },
    {
      "name": "Lucienne Bella-Assumpta",
      "first": "Lucienne",
      "last": "Bella-Assumpta",
      "affiliation": "Ministry of Health, Yaounde, Cameroon"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-11",
  "dateAccepted": "2010-10-27",
  "dateReceived": "2010-04-22",
  "volume": "4",
  "number": "11",
  "pages": "e895",
  "tags": [
    "Ophthalmology/Eye Infections"
  ],
  "abstract": "Background\n\nFollowing an epidemiological study carried out in 2006 showing a high prevalence of blinding trachoma in the Far North Region of Cameroon, a trachoma elimination programme using the SAFE strategy was initiated: three yearly trachoma mass treatments were to be performed.\n\nMethodology/Principal Findings\n\nThe entire district population (120,000 persons) was treated with azithromycin 1.5% eye drops in February 2008 and January 2009. To assess the effect of treatment on the prevalence of active trachoma, three epidemiological studies were conducted on a representative sample of children aged between 1 and 10 years. The first study was performed just prior to the first treatment, the second just prior to the 2nd treatment and the third one, one year later. The prevalence of active forms of trachoma (TF + TI) dropped from 31.5% (95%CI 26.4–37.5) before treatment to 6.3% (95%CI 4.1–9.6) one year after first treatment; a reduction of nearly 80%. One year after the second treatment, the prevalence decreased to 3.1% (95%CI 2.0–4.9), a total reduction of 90%. Furthermore, there were no more TI cases (only TF). There was no report of serious or systemic side effects. Tolerance was excellent.\n\nConclusions/Significance\n\nActive trachoma mass treatment with azithromycin 1.5% eye drops is feasible, well tolerated, and effective.",
  "fullText": "Introduction Trachoma is caused by Chlamydia trachomatis and it is spread by direct contact with eye, nose, and throat secretions from affected individuals or by contact with objects, such as towels and/or washcloths, which have had similar contact with these secretions [1], [2]. Flies can also be a route of mechanical transmission [1]. Children are the most susceptible to infection but the blinding effects or more severe symptoms are often not felt until adulthood. Infection is frequently passed from child to child but also from child to mother. Indeed, women are nearly two times [3] more affected than men by trachoma and trichiasis, probably because one of the primary activities of girls is taking care of their younger family members. This activity continues into adulthood, with women carrying the main responsibility of caring for children. Trachoma remains the leading infectious cause of blindness in the world [4]. Mass oral azithromycin distribution has been used in several programs. At present, azithromycin is available in a 1.5% eye drop formulation in Europe, Maghreb and French speaking African countries. The National Blindness Prevention Programme and a Vision 2020 plan were established as a result of the prevention policy of the Republic of Cameroon., In December 2006, in the Kolofata Health District (Far North Cameroon), a study assessing the prevalence of active and scarring trachoma, signalled the presence of endemic trachoma with significant blinding potential [5]. The National Blindness Prevention Programme decided to plan an elimination program by implementing the SAFE (Surgery, Antibiotics, Facial cleanliness, and Environmental change) strategy [6], addressing the “A” (antibiotic) component by conducting mass treatment targeting the entire district population and using azithromycin 1.5% eye drops. The objective of this study was to assess the feasibility, tolerance and effectiveness of repeated topical mass treatment with azithtromycin 1.5% eye drops, used for the first time on a large scale to reduce the prevalence of active forms of trachoma in a population. The first year, mass treatment gave promising results with a decrease in trachoma prevalence of more than 25% (from 31.5% to 6.3%) [7]. This article presents results after two rounds of treatment. Methods Mass treatment campaign In accordance with the WHO recommendations [8], the trachoma control programme in the Kolofata Health District called for one mass treatment per year for three years. The treatment consisted in one instillation of azithromycin 1.5% in both eyes in the morning and in the evening during three consecutive days. The study received authorisation from the Cameroon Ministry of Public Health in February 2008. The first round of treatment began on 23 February 2008 and ended on 10 March 2008, and the second one was undertaken between the 5th and 20th of January 2009 (Figure 1). Each year, Théa Laboratories donated 120,000 complete treatments (720,000 single doses) of azithromycin 1.5% eye drops, and sent them by air from Europe to Yaoundé, Cameroon, and by train and truck from Yaoundé to Kolofata. The target population was all residents of the Kolofata Health District [9]. The entire"
}