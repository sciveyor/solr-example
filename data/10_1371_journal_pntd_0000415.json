{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000415",
  "doi": "10.1371/journal.pntd.0000415",
  "externalIds": [
    "pii:07-PNTD-RA-0147R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Proteomic Comparison of Entamoeba histolytica and Entamoeba dispar and the Role of E. histolytica Alcohol Dehydrogenase 3 in Virulence",
  "authors": [
    {
      "name": "Paul H. Davis",
      "first": "Paul H.",
      "last": "Davis",
      "affiliation": "Department of Medicine, Washington University School of Medicine, St. Louis, Missouri, United States of America; Department of Molecular Microbiology, Washington University School of Medicine, St. Louis, Missouri, United States of America"
    },
    {
      "name": "Minghe Chen",
      "first": "Minghe",
      "last": "Chen",
      "affiliation": "Department of Medicine, Washington University School of Medicine, St. Louis, Missouri, United States of America"
    },
    {
      "name": "Xiaochun Zhang",
      "first": "Xiaochun",
      "last": "Zhang",
      "affiliation": "Department of Medicine, Washington University School of Medicine, St. Louis, Missouri, United States of America"
    },
    {
      "name": "C. Graham Clark",
      "first": "C. Graham",
      "last": "Clark",
      "affiliation": "Department of Infectious and Tropical Diseases, London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "R. Reid Townsend",
      "first": "R. Reid",
      "last": "Townsend",
      "affiliation": "Department of Medicine, Washington University School of Medicine, St. Louis, Missouri, United States of America"
    },
    {
      "name": "Samuel L. Stanley Jr.",
      "first": "Samuel L.",
      "last": "Stanley",
      "suffix": "Jr.",
      "affiliation": "Department of Medicine, Washington University School of Medicine, St. Louis, Missouri, United States of America; Department of Molecular Microbiology, Washington University School of Medicine, St. Louis, Missouri, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-04",
  "dateAccepted": "2009-03-17",
  "dateReceived": "2007-06-25",
  "volume": "3",
  "number": "4",
  "pages": "e415",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Microbiology/Cellular Microbiology and Pathogenesis",
    "Microbiology/Parasitology"
  ],
  "abstract": "The protozoan intestinal parasite Entamoeba histolytica infects millions of people worldwide and is capable of causing amebic dysentery and amebic liver abscess. The closely related species Entamoeba dispar colonizes many more individuals, but this organism does not induce disease. To identify molecular differences between these two organisms that may account for their differential ability to cause disease in humans, we used two-dimensional gel-based (DIGE) proteomic analysis to compare whole cell lysates of E. histolytica and E. dispar. We observed 141 spots expressed at a substantially (&gt;5-fold) higher level in E. histolytica HM-1∶IMSS than E. dispar and 189 spots showing the opposite pattern. Strikingly, 3 of 4 proteins consistently identified as different at a greater than 5-fold level between E. histolytica HM-1∶IMSS and E. dispar were identical to proteins recently identified as differentially expressed between E. histolytica HM-1∶IMSS and the reduced virulence strain E. histolytica Rahman. One of these was E. histolytica alcohol dehydrogenase 3 (EhADH3). We found that E. histolytica possesses a higher level of NADP-dependent alcohol dehydrogenase activity than E. dispar and that some EhADH3 can be localized to the surface of E. histolytica. Episomal overexpression of EhADH3 in E. histolytica trophozoites resulted in only subtle phenotypic differences in E. histolytica virulence in animal models of amebic colitis and amebic liver abscess, making it difficult to directly link EhADH3 levels to virulence differences between E. histolytica and less-pathogenic Entamoeba.",
  "fullText": "Introduction Entamoeba histolytica, a protozoan intestinal parasite, is the causative agent of amebic dysentery and amebic liver abscess [1], and is one of the leading causes of death from parasitic diseases. The closely related species, Entamoeba dispar, is morphologically indistinguishable from E. histolytica [2], and is highly prevalent in areas of poor sanitation. Importantly, E. dispar is a commensal and does not cause disease in humans, even in immunocompromised individuals. Previous studies have identified a number of Entamoeba molecules that appear to be linked to virulence, including cysteine proteinases, amoebapores, the Gal/GalNAc lectin and peroxiredoxin, but the virulence phenotype is unlikely to be secondary to only one, or even a few proteins [1], [3]–[8]. The ability to compare the genome and proteome of E. histolytica, and the related but nonpathogenic E. dispar, provides a powerful platform for more widespread screening for additional virulence factors of E. histolytica. Here we report the use of comparative proteomics of E. histolytica HM-1∶IMSS and E. dispar SAW760 to identify proteins that are differentially expressed between the two species, and the characterization of one of the differentially expressed proteins, EhADH3, identified by this screen. Materials and Methods Entamoeba species E. histolytica HM-1∶IMSS and E. dispar SAW760 were grown axenically in LYI-S-2 with 15% adult bovine serum medium at London School of Hygiene and Tropical Medicine [9]. For proteomic analysis, approximately 5×106 E. histolytica or E. dispar trophozoites were harvested and washed 3 times in ice-cold PBS to remove serum and medium proteins, then lysed in a buffer formulated to minimize post-lysis proteolysis (7 M Urea, 2 M thiourea, 4% CHAPS, 30 mM Tris, 5 mM magnesium acetate, 1× Roche Complete protease inhibitor cocktail with EDTA). Lysates were frozen at −80°C before analysis [3]. 2-D difference gel electrophoresis (DIGE) and protein identification using tandem mass spectrometry Trophozoite lysates were analyzed as previously described [3]. Briefly, lysates were thawed on wet ice and labeled with either Cy3 or Cy5 (GE Healthcare, Piscataway, NJ, USA) and quenched with lysine. The quenched Cy-labeled samples were then combined and added to an equal volume of 2× rehydration buffer (7 M urea, 2 M thiourea, 4% CHAPS, 4 mg/ml DTT) supplemented with 0.5% IPG (Immobilized pH gradient, GE Healthcare) buffer 3–11. Labeled protein extracts were separated by standard 2D gel electrophoresis. Following second-dimension focusing, the gel was fluorescently scanned using a Typhoon 9400 variable mode imager (GE Healthcare) to detect Cy3- and Cy5-specific emissions corresponding to protein concentration [10]. Fluorescent gel images were then analyzed using Decyder software (GE Healthcare), where individual spot volume ratios were calculated for each protein spot pair. Gel features were selected in the DeCyder software, then excised and transferred to a 96-well source plate. The gel pieces were digested in situ with trypsin as previously described [11]. Spectra of the peptide pools were obtained on a MALDI-TOF/TOF instrument (ABI 4700) and operated as previously described [12] using peptides from trypsin autolysis (m/z = 842.51, 1045.56, and 2211.10) [13] for internal calibration. The most intense MS signals (n = 7–20) were"
}