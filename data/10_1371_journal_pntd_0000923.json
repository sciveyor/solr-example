{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000923",
  "doi": "10.1371/journal.pntd.0000923",
  "externalIds": [
    "pii:10-PNTD-RA-1119R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Fexinidazole – A New Oral Nitroimidazole Drug Candidate Entering Clinical Development for the Treatment of Sleeping Sickness",
  "authors": [
    {
      "name": "Els Torreele",
      "first": "Els",
      "last": "Torreele",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland"
    },
    {
      "name": "Bernadette Bourdin Trunz",
      "first": "Bernadette",
      "last": "Bourdin Trunz",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland"
    },
    {
      "name": "David Tweats",
      "first": "David",
      "last": "Tweats",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland; The Medical School, University of Swansea, Swansea, United Kingdom"
    },
    {
      "name": "Marcel Kaiser",
      "first": "Marcel",
      "last": "Kaiser",
      "affiliation": "Parasite Chemotherapy, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Reto Brun",
      "first": "Reto",
      "last": "Brun",
      "affiliation": "Parasite Chemotherapy, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Guy Mazué",
      "first": "Guy",
      "last": "Mazué",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland"
    },
    {
      "name": "Michael A. Bray",
      "first": "Michael A.",
      "last": "Bray",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland"
    },
    {
      "name": "Bernard Pécoul",
      "first": "Bernard",
      "last": "Pécoul",
      "affiliation": "Drugs for Neglected Diseases initiative (DNDi), Geneva, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-12",
  "dateAccepted": "2010-11-22",
  "dateReceived": "2010-04-29",
  "volume": "4",
  "number": "12",
  "pages": "e923",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Pharmacology/Drug Development"
  ],
  "abstract": "Background\n\nHuman African trypanosomiasis (HAT), also known as sleeping sickness, is a fatal parasitic disease caused by trypanosomes. Current treatment options for HAT are scarce, toxic, no longer effective, or very difficult to administer, in particular for the advanced, fatal stage of the disease (stage 2, chronic HAT). New safe, effective and easy-to-use treatments are urgently needed. Here it is shown that fexinidazole, a 2-substituted 5-nitroimidazole rediscovered by the Drugs for Neglected Diseases initiative (DNDi) after extensive compound mining efforts of more than 700 new and existing nitroheterocycles, could be a short-course, safe and effective oral treatment curing both acute and chronic HAT and that could be implemented at the primary health care level. To complete the preclinical development and meet the regulatory requirements before initiating human trials, the anti-parasitic properties and the pharmacokinetic, metabolic and toxicological profile of fexinidazole have been assessed.\n\nMethods and Findings\n\nStandard in vitro and in vivo anti-parasitic activity assays were conducted to assess drug efficacy in experimental models for HAT. In parallel, a full range of preclinical pharmacology and safety studies, as required by international regulatory guidelines before initiating human studies, have been conducted. Fexinidazole is moderately active in vitro against African trypanosomes (IC50 against laboratory strains and recent clinical isolates ranged between 0.16 and 0.93 µg/mL) and oral administration of fexinidazole at doses of 100 mg/kg/day for 4 days or 200 mg/kg/day for 5 days cured mice with acute and chronic infection respectively, the latter being a model for the advanced and fatal stage of the disease when parasites have disseminated into the brain. In laboratory animals, fexinidazole is well absorbed after oral administration and readily distributes throughout the body, including the brain. The absolute bioavailability of oral fexinidazole was 41% in mice, 30% in rats, and 10% in dogs. Furthermore, fexinidazole is rapidly metabolised in vivo to at least two biologically active metabolites (a sulfoxide and a sulfone derivative) that likely account for a significant portion of the therapeutic effect. Key pharmacokinetic parameter after oral absorption in mice for fexinidazole and its sulfoxide and sulfone metabolites are a Cmax of 500, 14171 and 13651 ng/mL respectively, and an AUC0–24 of 424, 45031 and 96286 h.ng/mL respectively. Essentially similar PK profiles were observed in rats and dogs. Toxicology studies (including safety pharmacology and 4-weeks repeated-dose toxicokinetics in rat and dog) have shown that fexinidazole is well tolerated. The No Observed Adverse Event Levels in the 4-weeks repeated dose toxicity studies in rats and dogs was 200 mg/kg/day in both species, with no issues of concern identified for doses up to 800 mg/kg/day. While fexinidazole, like many nitroheterocycles, is mutagenic in the Ames test due to bacterial specific metabolism, it is not genotoxic to mammalian cells in vitro or in vivo as assessed in an in vitro micronucleus test on human lymphocytes, an in vivo mouse bone marrow micronucleus test, and an ex vivo unscheduled DNA synthesis test in rats.\n\nConclusions\n\nThe results of the preclinical pharmacological and safety studies indicate that fexinidazole is a safe and effective oral drug candidate with no untoward effects that would preclude evaluation in man. The drug has entered first-in-human phase I studies in September 2009. Fexinidazole is the first new clinical drug candidate with the potential for treating advanced-stage sleeping sickness in thirty years.",
  "fullText": "Introduction A major challenge for new drug development is the identification of pharmacologically active compounds with a favourable activity and toxicity profile that can be turned into new drug candidates. The contemporary approach to identifying such compounds is high-throughput screening of large and chemically diverse compound libraries to identify novel pharmacophores, followed by lead optimisation [1], [2]. Sometimes, this screening effort is narrowed down by using more targeted libraries that are thought to be enriched in compounds with a desired type of activity (e.g. kinase inhibitors [3]). However, promising candidates can also be found by revisiting the wealth of past drug discovery research, during which promising lines of research were sometimes not pursued for commercial or other strategic reasons. In this paper, we report the successful result of a proactive compound mining approach into a well-known class of anti-infectives, the nitroimidazoles, to rediscover fexinidazole, a long forgotten antiparasitic drug candidate. Fexinidazole turned out to be an excellent candidate to cure human African trypanosomiasis (HAT), including the advanced and fatal stage of the disease. An estimated sixty million people in 36 sub-Saharan African countries are at risk for HAT, especially poor and neglected populations living in remote rural areas [4], [5]. While the number of reported HAT cases has decreased in recent years due to intensified control activities, 50,000 to 70,000 people are estimated to be infected [6]. In west and central Africa, Trypanosoma brucei gambiense causes a chronic form of sleeping sickness, whereas in eastern and southern Africa T. b. rhodesiense causes an acute form of the disease [7], [8]. Both forms of HAT occur in two stages: stage 1 (early, hemolymphatic) is characterized by non-specific clinical symptoms such as malaise, headache, fever, and peripheral oedema, whereas stage 2 (late, meningoencephalic) is characterized by neurological symptoms including behavioural changes, severe sleeping disturbances, and convulsions, which, if left untreated, lead to coma and death [9], [10]. Available treatments for HAT [8] (Table 1) are few, old, and limited due to toxicity, diminishing efficacy in several geographical regions [11], [12], and complexity of use [13]. Treatment is stage-specific, with the more toxic and difficult-to-use treatments being used for stage 2 HAT. NECT, a combination treatment of a simplified course of intravenous eflornithine and oral nifurtimox, has been the only advance in the past 25 years [14], [15], and has been recently accepted into the WHO's Essential Medicines List as treatment for stage 2 HAT [16]. Despite being a clear improvement with reduced toxicity and treatment duration, the requirement for intravenous administration is still a limitation. It is estimated that less than 20% of currently infected people have access to treatment or are under any HAT surveillance, due to a combination of lack of effective and field-adapted diagnostics and treatments, combined with extreme poverty and remoteness of the affected populations, including in conflict zones [4], [17]. To change the dynamics of HAT control and access more patients while improving their case-management, a safe, effective, affordable, and easy-to-use (short course, preferably oral) treatment is"
}