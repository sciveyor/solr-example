{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000967",
  "doi": "10.1371/journal.pntd.0000967",
  "externalIds": [
    "pii:10-PNTD-RA-1575R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Immunological and Viral Determinants of Dengue Severity in Hospitalized Adults in Ha Noi, Viet Nam",
  "authors": [
    {
      "name": "Annette Fox",
      "first": "Annette",
      "last": "Fox",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Le Nguyen Minh Hoa",
      "first": "Le Nguyen Minh",
      "last": "Hoa",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Cameron P. Simmons",
      "first": "Cameron P.",
      "last": "Simmons",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Marcel Wolbers",
      "first": "Marcel",
      "last": "Wolbers",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Heiman F. L. Wertheim",
      "first": "Heiman F. L.",
      "last": "Wertheim",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom; South East Asia Infectious Diseases Clinical Research Network, Jakarta, Indonesia"
    },
    {
      "name": "Pham Thi Khuong",
      "first": "Pham Thi",
      "last": "Khuong",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Tran Thi Hai Ninh",
      "first": "Tran Thi Hai",
      "last": "Ninh",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Trinh Thi Minh Lien",
      "first": "Trinh Thi Minh",
      "last": "Lien",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Nguyen Thi Lien",
      "first": "Nguyen Thi",
      "last": "Lien",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Nguyen Vu Trung",
      "first": "Nguyen Vu",
      "last": "Trung",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Nguyen Duc Hien",
      "first": "Nguyen Duc",
      "last": "Hien",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    },
    {
      "name": "Jeremy Farrar",
      "first": "Jeremy",
      "last": "Farrar",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Peter Horby",
      "first": "Peter",
      "last": "Horby",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "Walter R. Taylor",
      "first": "Walter R.",
      "last": "Taylor",
      "affiliation": "Oxford University Clinical Research Unit, Wellcome Trust Major Overseas Programme, Hanoi, Socialist Republic of Vietnam; Centre for Tropical Medicine, Nuffield Department of Clinical Medicine, University of Oxford, Oxford, United Kingdom; South East Asia Infectious Diseases Clinical Research Network, Jakarta, Indonesia"
    },
    {
      "name": "Nguyen Van Kinh",
      "first": "Nguyen",
      "last": "Van Kinh",
      "affiliation": "National Hospital for Tropical Diseases, Hanoi, Socialist Republic of Vietnam"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-03",
  "dateAccepted": "2011-01-19",
  "dateReceived": "2010-09-21",
  "volume": "5",
  "number": "3",
  "pages": "e967",
  "tags": [
    "Immunology/Cellular Microbiology and Pathogenesis",
    "Immunology/Immunity to Infections",
    "Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Viral Infections",
    "Virology",
    "Virology/Mechanisms of Resistance and Susceptibility, including Host Genetics"
  ],
  "abstract": "Background\n\nThe relationships between the infecting dengue serotype, primary and secondary infection, viremia and dengue severity remain unclear. This cross-sectional study examined these interactions in adult patients hospitalized with dengue in Ha Noi.\n\nMethods and Findings\n\n158 patients were enrolled between September 16 and November 11, 2008. Quantitative RT-PCR, serology and NS1 detection were used to confirm dengue infection, determine the serotype and plasma viral RNA concentration, and categorize infections as primary or secondary. 130 (82%) were laboratory confirmed. Serology was consistent with primary and secondary infection in 34% and 61%, respectively. The infecting serotype was DENV-1 in 42 (32%), DENV-2 in 39 (30%) and unknown in 49 (38%). Secondary infection was more common in DENV-2 infections (79%) compared to DENV-1 (36%, p&lt;0.001). The proportion that developed dengue haemorrhagic fever (DHF) was 32% for secondary infection compared to 18% for primary infection (p = 0.14), and 26% for DENV-1 compared to 28% for DENV-2. The time until NS1 and plasma viral RNA were undetectable was shorter for DENV-2 compared to DENV-1 (p≤0.001) and plasma viral RNA concentration on day 5 was higher for DENV-1 (p = 0.03). Plasma viral RNA concentration was higher in secondary infection on day 5 of illness (p = 0.046). We didn't find an association between plasma viral RNA concentration and clinical severity.\n\nConclusion\n\nDengue is emerging as a major public health problem in Ha Noi. DENV-1 and DENV-2 were the prevalent serotypes with similar numbers and clinical presentation. Secondary infection may be more common amongst DENV-2 than DENV-1 infections because DENV-2 infections resulted in lower plasma viral RNA concentrations and viral RNA concentrations were higher in secondary infection. The drivers of dengue emergence in northern Viet Nam need to be elucidated and public health measures instituted.",
  "fullText": "Introduction Dengue virus (DENV) infections range in severity from asymptomatic to a syndrome characterized by a haemorrhagic tendency and vascular permeability [1]. The events that precipitate endothelial cell dysfunction and vascular leak are incompletely understood. Numerous studies including several prospective cohorts [2], [3], [4] demonstrate that the risk of severe dengue is higher during a secondary infection with a new serotype in children [5], [6], [7]. Severe dengue has also been associated with high viral loads [8], [9], [10], [11], [12], prolonged viremia [13] and high NS1 antigen levels [12], [14]. At sub-neutralizing concentrations, dengue specific antibodies can enhance dengue virus infection of mononuclear phagocytes [15]. In addition, antibodies to pre-membrane protein appear to enhance infection of all serotypes, even when present at high concentration [16]. It has therefore been proposed that antibody-dependent enhancement (ADE) of viremia is a risk factor for severe dengue [1]. However, ADE may not fully account for severe dengue as several studies have found no association between severity and secondary infection in adults [7], [17], [18], [19], or severity and viral load [20], [21], [22], [23]. In one study dengue virus titers were higher prior to defervescence in patients with secondary infection [8] but most other studies have found that titers are either similar or higher in primary infection [10], [17], [20], [24]. Demonstration of a link between enhancing antibody levels, viral load and disease severity in humans also remains elusive. The emerging picture is that multiple factors including prior immunity, viral load, age of the patient and infecting serotype and genotype may contribute to the severity of dengue infection [2], [3], [25], [26] but the nature of these interactions remains unclear. Dengue pathogenesis has largely been studied in dengue hyper-endemic regions where analysis is “confounded by a multiplicity of preexisting immunity patterns coupled with co-circulation of multiple serotypes” [3], [27]. Studies in low transmission settings, where few dengue serotypes circulate and primary infection in adults is common, potentially offer an opportunity to better identify factors associated with severity across serotype and immunity groups. We conducted a prospective study in Ha Noi, Viet Nam, to examine the association between primary and secondary infections, serotype, plasma viral RNA concentration, and the development of dengue haemorrhagic fever (DHF) in a low transmission setting. Methods Patient recruitment and clinical investigations The protocol for this study was approved by the scientific and ethical committees at the National Hospital of Tropical Diseases and The Oxford University Tropical Research Ethics Committee (OXTREC). Patients provided written informed consent to participate in this study. Patients were eligible for recruitment if they were admitted to the National Hospital of Tropical Diseases (NHTD) in Ha Noi, Viet Nam between September and November 2008 with a clinical diagnosis of dengue according to the WHO criteria [28]. These criteria were fever plus two or more of the following: headache; retro-orbital pain; myalgia/arthralgia; rash; bleeding or leukopenia. The study protocol included children but the number attending the National Hospital of Pediatrics (NHP) in Ha Noi during the study"
}