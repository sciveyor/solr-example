{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001639",
  "doi": "10.1371/journal.pntd.0001639",
  "externalIds": [
    "pii:PNTD-D-11-00974"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Infection and Transmission of Rift Valley Fever Viruses Lacking the NSs and/or NSm Genes in Mosquitoes: Potential Role for NSm in Mosquito Infection",
  "authors": [
    {
      "name": "Mary B. Crabtree",
      "first": "Mary B.",
      "last": "Crabtree",
      "affiliation": "Division of Vector-Borne Diseases, Centers for Disease Control and Prevention, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Rebekah J. Kent Crockett",
      "first": "Rebekah J.",
      "last": "Kent Crockett",
      "affiliation": "Division of Vector-Borne Diseases, Centers for Disease Control and Prevention, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Brian H. Bird",
      "first": "Brian H.",
      "last": "Bird",
      "affiliation": "Viral Special Pathogens Branch, Division of High-Consequence Pathogens and Pathology, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Stuart T. Nichol",
      "first": "Stuart T.",
      "last": "Nichol",
      "affiliation": "Viral Special Pathogens Branch, Division of High-Consequence Pathogens and Pathology, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Bobbie Rae Erickson",
      "first": "Bobbie Rae",
      "last": "Erickson",
      "affiliation": "Viral Special Pathogens Branch, Division of High-Consequence Pathogens and Pathology, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Brad J. Biggerstaff",
      "first": "Brad J.",
      "last": "Biggerstaff",
      "affiliation": "Division of Vector-Borne Diseases, Centers for Disease Control and Prevention, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Kalanthe Horiuchi",
      "first": "Kalanthe",
      "last": "Horiuchi",
      "affiliation": "Division of Vector-Borne Diseases, Centers for Disease Control and Prevention, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Barry R. Miller",
      "first": "Barry R.",
      "last": "Miller",
      "affiliation": "Division of Vector-Borne Diseases, Centers for Disease Control and Prevention, Fort Collins, Colorado, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-05",
  "dateAccepted": "2012-03-27",
  "dateReceived": "2011-10-05",
  "volume": "6",
  "number": "5",
  "pages": "e1639",
  "tags": [
    "Biology",
    "Clinical immunology",
    "Immunity",
    "Immunology",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Microbiology",
    "Mosquitoes",
    "RNA viruses",
    "Vaccination",
    "Vaccine development",
    "Vaccines",
    "Vector biology",
    "Vectors and hosts",
    "Viral classification",
    "Viral vaccines",
    "Virology"
  ],
  "abstract": "Background\n          \nRift Valley fever virus is an arthropod-borne human and animal pathogen responsible for large outbreaks of acute and febrile illness throughout Africa and the Arabian Peninsula. Reverse genetics technology has been used to develop deletion mutants of the virus that lack the NSs and/or NSm virulence genes and have been shown to be stable, immunogenic and protective against Rift Valley fever virus infection in animals. We assessed the potential for these deletion mutant viruses to infect and be transmitted by Aedes mosquitoes, which are the principal vectors for maintenance of the virus in nature and emergence of virus initiating disease outbreaks, and by Culex mosquitoes which are important amplification vectors.\n\n          Methodology and Principal Findings\n          \nAedes aegypti and Culex quinquefasciatus mosquitoes were fed bloodmeals containing the deletion mutant viruses. Two weeks post-exposure mosquitoes were assayed for infection, dissemination, and transmission. In Ae. aegypti, infection and transmission rates of the NSs deletion virus were similar to wild type virus while dissemination rates were significantly reduced. Infection and dissemination rates for the NSm deletion virus were lower compared to wild type. Virus lacking both NSs and NSm failed to infect Ae. aegypti. In Cx. quinquefasciatus, infection rates for viruses lacking NSm or both NSs and NSm were lower than for wild type virus.\n\n          Conclusions/Significance\n          \nIn both species, deletion of NSm or both NSs and NSm reduced the infection and transmission potential of the virus. Deletion of both NSs and NSm resulted in the highest level of attenuation of virus replication. Deletion of NSm alone was sufficient to nearly abolish infection in Aedes aegypti mosquitoes, indicating an important role for this protein. The double deleted viruses represent an ideal vaccine profile in terms of environmental containment due to lack of ability to efficiently infect and be transmitted by mosquitoes.",
  "fullText": "Introduction Rift Valley fever virus (RVFV), a human and animal pathogen that is endemic in much of Africa, has in recent decades spread to Saudi Arabia, Madagascar and Yemen and has the potential to spread to other parts of the world via transport of infected livestock, humans or mosquitoes or by an act of bioterrorism [1]–[6]. An arthropod-borne member of the Phlebovirus genus of the family Bunyaviridae, RVFV causes significant outbreaks of severe disease in livestock, including mortality in young animals, fetal deformities and abortion. RVFV infection in humans can result in a self-limiting febrile illness or more severe disease such as retinitis, hepatic necrosis, encephalitis, neurologic deficits or fatal hemorrhagic fever [7]–[9]. The primary maintenance host and source of RVFV initiating disease outbreaks is considered to be mosquitoes in the Aedes genus. Mosquitoes in the Culex genus are thought to be important in amplification of virus activity during outbreaks. The virus has also been detected in phlebotomine sand flies, Culicoides midges, and Amblyomma tick species although these infections are not thought to play an important role in the life cycle of the virus or in disease outbreak settings [5], [10]–[13]. In laboratory studies, several North American Aedes and Culex mosquito species have been shown to be competent vectors of the virus, indicating the potential for establishment of RVFV transmission cycles in North America [14]–[17]. Infection, replication and transmission of an arthropod-borne virus involve complex interactions between the virus and various cells/tissues/organs of the vector. Successful transmission requires that after being ingested in a viremic bloodmeal the virus must enter the epithelial cells of the midgut, replicate and escape from the midgut cells into the hemolymph. This is followed by infection of secondary organs, including the salivary glands, where the virus enters the saliva and can then be transmitted to a new host. Potential barriers in this process have been identified that can block infection, replication and/or transmission of a virus by the mosquito [18], [19]. These include the midgut infection and escape barriers and the salivary gland infection and escape barriers. The presence or absence of these barriers and the degree to which they are effective appears to be influenced by the genetics of both the virus and the vector [18]. The RVFV genome is comprised of three segments of single-stranded, negative sense RNA. The small (S) segment codes for the structural nucleoprotein (NP) and the nonstructural NSs protein, the medium (M) segment encodes the two structural glycoproteins, Gn and Gc, as well as two nonstructural proteins (NSm and NSm-Gn) and the large (L) segment codes for the viral RNA-dependent RNA polymerase. The nonstructural NSs and NSm proteins have been shown to function as virulence factors. The NSs protein has multiple functions that suppress the mammalian host cell antiviral response by inhibiting IFN-β gene transcription, promoting degradation of protein kinase (PKR) and suppressing host transcription [20]–[24]. The RVFV NSm protein plays a role in viral pathogenesis by suppression of virus-induced apoptosis in infected cells although it has been shown"
}