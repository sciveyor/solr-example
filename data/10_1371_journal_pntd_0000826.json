{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000826",
  "doi": "10.1371/journal.pntd.0000826",
  "externalIds": [
    "pii:10-PNTD-RA-0931R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Chagas Cardiomiopathy: The Potential of Diastolic Dysfunction and Brain Natriuretic Peptide in the Early Identification of Cardiac Damage",
  "authors": [
    {
      "name": "Ana Garcia-Alvarez",
      "first": "Ana",
      "last": "Garcia-Alvarez",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain; Fundación Centro Nacional de Investigaciones Cardiovasculares Instituto de Salud Carlos III, Madrid, Spain"
    },
    {
      "name": "Marta Sitges",
      "first": "Marta",
      "last": "Sitges",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain; Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), Barcelona, Spain"
    },
    {
      "name": "María-Jesús Pinazo",
      "first": "María-Jesús",
      "last": "Pinazo",
      "affiliation": "Barcelona Centre for International Health Research (CRESIB), Hospital Clinic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain"
    },
    {
      "name": "Ander Regueiro-Cueva",
      "first": "Ander",
      "last": "Regueiro-Cueva",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain"
    },
    {
      "name": "Elizabeth Posada",
      "first": "Elizabeth",
      "last": "Posada",
      "affiliation": "Barcelona Centre for International Health Research (CRESIB), Hospital Clinic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain"
    },
    {
      "name": "Silvia Poyatos",
      "first": "Silvia",
      "last": "Poyatos",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain"
    },
    {
      "name": "José Tomás Ortiz-Pérez",
      "first": "José Tomás",
      "last": "Ortiz-Pérez",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain"
    },
    {
      "name": "Magda Heras",
      "first": "Magda",
      "last": "Heras",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain; Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), Barcelona, Spain"
    },
    {
      "name": "Manel Azqueta",
      "first": "Manel",
      "last": "Azqueta",
      "affiliation": "Thorax Clinic Institute, Hospital Clínic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain; Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), Barcelona, Spain"
    },
    {
      "name": "Joaquim Gascon",
      "first": "Joaquim",
      "last": "Gascon",
      "affiliation": "Barcelona Centre for International Health Research (CRESIB), Hospital Clinic, Institut d'Investigacions Biomèdiques August Pi i Sunyer (IDIBAPS), University of Barcelona, Barcelona, Spain"
    },
    {
      "name": "Ginés Sanz",
      "first": "Ginés",
      "last": "Sanz",
      "affiliation": "Fundación Centro Nacional de Investigaciones Cardiovasculares Instituto de Salud Carlos III, Madrid, Spain"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-09",
  "dateAccepted": "2010-08-19",
  "dateReceived": "2010-02-26",
  "volume": "4",
  "number": "9",
  "pages": "e826",
  "tags": [
    "Cardiovascular Disorders/Cardiovascular Imaging",
    "Cardiovascular Disorders/Coronary Artery Disease",
    "Cardiovascular Disorders/Myocardial Infarction"
  ],
  "abstract": "Introduction\n\nChagas disease remains a major cause of mortality in several countries of Latin America and has become a potential public health problem in non-endemic countries as a result of migration flows. Cardiac involvement represents the main cause of mortality, but its diagnosis is still based on nonspecific criteria with poor sensitivity. Early identification of patients with cardiac involvement is desirable, since early treatment may improve prognosis. This study aimed to assess the role of diastolic dysfunction, abnormal myocardial strain and elevated brain natriuretic peptide (BNP) in the early identification of cardiac involvement in Chagas disease.\n\nMethodology/Principal Findings\n\nFifty-four patients divided into 3 groups—group 1 (undetermined form: positive serology without ECG or 2D-echocardiographic abnormalities; N = 32), group 2 (typical ECG abnormalities of Chagas disease but normal 2D-echocardiography; N = 14), and group 3 (regional wall motion abnormalities, left ventricular [LV] end-diastolic diameter &gt;55 mm or LV ejection fraction &lt;50% on echocardiography; N = 8)—and 44 control subjects were studied. Patients with significant non-cardiac diseases, other heart diseases and previous treatment with benznidazol were excluded. The median age was 37 (20–58) years; 40% were men. BNP levels, longitudinal and radial myocardial strain and LV diastolic dysfunction increased progressively from group 1 to 3 (p for trend &lt;0.01). Abnormal BNP levels (&gt;37 pg/ml) were noted in 0%, 13%, 29% and 63% in controls and groups 1 to 3, respectively. Half of patients in the undetermined form had impaired relaxation patterns, whereas half of patients with ECG abnormalities suggestive of Chagas cardiomyopathy had normal diastolic function. In group 1, BNP levels were statistically higher in patients with diastolic dysfunction as compared to those with normal diastolic function (27±26 vs. 11±8 pg/ml, p = 0.03).\n\nConclusion/Significance\n\nIn conclusion, the combination of diastolic function and BNP measurement adds important information that could help to better stratify patients with Chagas disease.",
  "fullText": "Introduction Chagas disease, a major cause of morbidity and mortality in several countries of Latin America [1], has become a potential public health problem in countries where the disease is not endemic as a result of migration flows [2], [3], [4]. Chagas cardiomyopathy is the most serious form of the chronic phase of the disease and represents the major cause of mortality in these patients. For this reason, accurate diagnosis of cardiac involvement is critical. However, Chagas disease remains a neglected disease [5] and the diagnosis of Chagas cardiomyopathy is still based on simple and nonspecific criteria including an increased cardiothoracic ratio (&gt;0.5) or ECG abnormalities such as complete right bundle-branch block, left anterior hemiblock, complete left bundle-branch block, as well as other conduction and rhythm disturbances [6], [7]. Echocardiography refined the diagnosis of Chagas cardiomyopathy, and regional wall motion abnormalities, reduced left ventricular ejection fraction (LVEF) &lt;50% and increased left ventricular (LV) end-diastolic diameter &gt;55 mm are now included as diagnostic criteria in some publications [8], [9]. In spite of that, the sensitivity of these parameters is far from perfect and they may indeed misclassify patients with early myocardial involvement into the undetermined form, as conventional 2D echocardiography only detects advanced myocardial involvement. On the other hand, patients without cardiac disease but having one of the described as typical but unspecific ECG findings could be considered to have Chagas cardiomyopathy. Therefore, a more accurate classification model, particularly to identify patients with early cardiac involvement from the undetermined form would be desirable, since an early treatment and closer follow-up might be beneficial on these patients [8]. Analysis of diastolic function by echocardiography, cardiac magnetic resonance (CMR) and several biomarkers, including brain natriuretic peptide (BNP) and inflammation markers, have emerged as useful tools in the diagnosis and monitoring of heart failure in different conditions. In fact, BNP has recently been included in the guidelines for the diagnosis and management of congestive heart failure [10]. Comprehensive evaluation of diastolic dysfunction and myocardial strain imaging has provided more accuracy and sensitivity to detect early myocardial involvement in different cardiomyopathies [11], [12]. However, these methods have been seldom utilized in characterizing patients with established Chagas cardiomyopathy [13], [14], [15], and its role in the identification of cardiac involvement in the earlier phases of the disease is unclear [16], [17], [18]. We conducted a prospective study aimed to analyze the added value of different techniques in identifying cardiac involvement in the undetermined stage of Chagas disease. Diastolic function, natriuretic peptide and inflammatory markers levels in different phases of the disease were measured and correlated with longitudinal and radial myocardial strain and delayed enhancement on CMR to find out their value in improving the stratification of patients with Chagas disease. Methods Study Population A cross-sectional analysis was performed in a prospective cohort of consecutive adult patients evaluated at our Institution from January 2008 to June 2009. Diagnosis of Chagas disease was based on a clinical record compatible with the epidemiology of the disease (individuals from endemic"
}