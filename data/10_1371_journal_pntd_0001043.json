{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001043",
  "doi": "10.1371/journal.pntd.0001043",
  "externalIds": [
    "pii:10-PNTD-RA-1542R3"
  ],
  "license": "This is an open-access article distributed under\n                the terms of the Creative Commons Attribution License, which permits unrestricted\n                use, distribution, and reproduction in any medium, provided the original author and\n                source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Gene Atlasing of Digestive and Reproductive Tissues in\n                        Schistosoma mansoni",
  "authors": [
    {
      "name": "Sujeevi S. K. Nawaratna",
      "first": "Sujeevi S. K.",
      "last": "Nawaratna",
      "affiliation": "Queensland Institute of Medical Research, Herston, Australia; School of Veterinary Sciences, The University of Queensland, Gatton,\n                    Australia"
    },
    {
      "name": "Donald P. McManus",
      "first": "Donald P.",
      "last": "McManus",
      "affiliation": "Queensland Institute of Medical Research, Herston, Australia"
    },
    {
      "name": "Luke Moertel",
      "first": "Luke",
      "last": "Moertel",
      "affiliation": "Queensland Institute of Medical Research, Herston, Australia"
    },
    {
      "name": "Geoffrey N. Gobert",
      "first": "Geoffrey N.",
      "last": "Gobert",
      "affiliation": "Queensland Institute of Medical Research, Herston, Australia; School of Veterinary Sciences, The University of Queensland, Gatton,\n                    Australia"
    },
    {
      "name": "Malcolm K. Jones",
      "first": "Malcolm K.",
      "last": "Jones",
      "affiliation": "Queensland Institute of Medical Research, Herston, Australia; School of Veterinary Sciences, The University of Queensland, Gatton,\n                    Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-04",
  "dateAccepted": "2011-03-25",
  "dateReceived": "2010-09-13",
  "volume": "5",
  "number": "4",
  "pages": "e1043",
  "tags": [
    "Infectious Diseases/Helminth Infections",
    "Microbiology/Parasitology",
    "Molecular Biology/Bioinformatics"
  ],
  "abstract": "Background\n                    \nWhile considerable genomic and transcriptomic data are available for\n                            Schistosoma mansoni, many of its genes lack significant\n                        annotation. A transcriptomic study of individual tissues and organs of\n                        schistosomes could play an important role in functional annotation of the\n                        unknown genes, particularly by providing rapid localisation data and thus\n                        giving insight into the potential roles of these molecules in parasite\n                        development, reproduction and homeostasis, and in the complex host-parasite\n                        interaction.\n\n                    Methodology/Principal Findings\n                    \nQuantification of gene expression in tissues of S. mansoni\n                        was achieved by a combination of laser microdissection microscopy (LMM) and\n                        oligonucleotide microarray analysis. We compared the gene expression profile\n                        of the adult female gastrodermis and male and female reproductive tissues\n                        with whole worm controls. The results revealed a total of 393 genes\n                        (contigs) that were up-regulated two-fold or more in the gastrodermis, 4,450\n                        in the ovary, 384 in the vitelline tissues of female parasites, and 2,171 in\n                        the testes. We have also supplemented these data with the identification of\n                        highly expressed genes in different regions of manually dissected male and\n                        female S. mansoni. Though relatively crude, this dissection\n                        strategy provides low resolution localisation data for critical regions of\n                        the adult parasites that are not amenable to LMM isolation.\n\n                    Conclusions\n                    \nThis is the first detailed transcriptomic study of the reproductive tissues\n                        and gastrodermis of S. mansoni. The results obtained will\n                        help direct future research on the functional aspects of these tissues,\n                        expediting the characterisation of currently unannotated gene products of\n                            S. mansoni and the discovery of new drug and vaccine\n                        targets.",
  "fullText": "Introduction The last decade has seen an exponential growth in the availability of genomic and proteomic data for Schistosoma mansoni and S. japonicum through genomic sequencing projects [1], [2] and numerous transcriptomic and proteomic studies [3], [4], [5], [6], [7], [8], [9]. However, the functions of many schistosome gene products remain unsolved. As argued by Jones et al [10], a major step towards elucidating function of these uncharacterised gene products lies in defining their sites of expression. Large-scale investigation of the expression repertoire of specific tissues is difficult to achieve because schistosome tissues generally cannot be isolated, due to their small tubiform bodies and the absence of a body cavity [10], [11], [12]. Laser micro-dissection microscopy (LMM), a tool that enables selection and sampling of small cell populations from histological sections, has been used to overcome the difficulty in separating the tissues for cell-enriched studies of these parasites [10], [13]. The approach has been used successfully by our team to study tissue specific transcriptomics of gastrodermal, ovary and vitelline tissues of adult females of S. japonicum [11]. In the current study, we describe the application of LMM, in conjunction with microarray gene expression analysis, to map the transcriptome of biologically important tissues of S. mansoni, augmenting our study on S. japonicum tissue specific transcriptomics [11]. The gastrodermis (absorptive gut lining) of females and the reproductive tissues of male (testes) and female (ovary, vitelline glands) worms were targeted with this gene atlasing strategy to determine the expression repertoire of these tissues. Materials and Methods Research involving animals in this study was approved by the Animal Ethics Committee of the Queensland Institute of Medical Research. The study was conducted according to guidelines of the National Health and Medical Research Council of Australia, as published in the Australian Code of Practice for the Care and Use of Animals for Scientific Purposes, 7th edition, 2004 (www.nhmrc.gov.au). Sample preparation The Puerto Rican strain of Schistosoma mansoni is maintained in ARC Swiss mice and Biomphalaria glabrata snails at QIMR from stocks originating from the National Institute of Allergy and Infectious Diseases Schistosomiasis Resource Centre, Biomedical Research institute (Rockville, Maryland, USA). Worm pairs were perfused from mice six weeks post-infection. Males and females were separated and single-sex samples, consisting of 30–40 worms, were transferred immediately into Tissue-Tek Optimal Cutting Temperature compound (OCT) (ProSciTech, Thuringowa, Australia) and frozen in dry ice. Four OCT blocks, each containing 30–40 worms, were used for the microdissection of each sex of S. mansoni. Worm sections were prepared for microdissection as described [11]. Cryosections were cut onto PALM 0.17 mm polyethylene naphthalene membrane covered slides (Carl Zeiss Microimaging GmbH, Bernried, Germany). Laser microdissection of schistosome tissues Thawed cryosections of worms were stained with 1% (w/v) toluidine blue (CHROMA, Stuttgart, Germany), washed in distilled water and air dried in a sterile biohazard hood immediately prior to microdissection [11]. A PALM MicroBeam Laser Catapult Microscope (Carl Zeiss Microimaging) was used to microdissect the gut, ovary, vitelline glands of females and testes from males from the"
}