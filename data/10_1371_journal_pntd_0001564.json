{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001564",
  "doi": "10.1371/journal.pntd.0001564",
  "externalIds": [
    "pii:PNTD-D-11-01212"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Enhanced Protective Efficacy of a Chimeric Form of the Schistosomiasis Vaccine Antigen Sm-TSP-2",
  "authors": [
    {
      "name": "Mark S. Pearson",
      "first": "Mark S.",
      "last": "Pearson",
      "affiliation": "Queensland Tropical Health Alliance and School of Public Health and Tropical Medicine, James Cook University, Cairns, Queensland, Australia"
    },
    {
      "name": "Darren A. Pickering",
      "first": "Darren A.",
      "last": "Pickering",
      "affiliation": "Queensland Tropical Health Alliance and School of Public Health and Tropical Medicine, James Cook University, Cairns, Queensland, Australia"
    },
    {
      "name": "Henry J. McSorley",
      "first": "Henry J.",
      "last": "McSorley",
      "affiliation": "Queensland Tropical Health Alliance and School of Public Health and Tropical Medicine, James Cook University, Cairns, Queensland, Australia"
    },
    {
      "name": "Jeffrey M. Bethony",
      "first": "Jeffrey M.",
      "last": "Bethony",
      "affiliation": "Department of Microbiology, Immunology and Tropical Medicine, George Washington University, Washington D.C., United States of America"
    },
    {
      "name": "Leon Tribolet",
      "first": "Leon",
      "last": "Tribolet",
      "affiliation": "Queensland Tropical Health Alliance and School of Public Health and Tropical Medicine, James Cook University, Cairns, Queensland, Australia"
    },
    {
      "name": "Annette M. Dougall",
      "first": "Annette M.",
      "last": "Dougall",
      "affiliation": "Queensland Tropical Health Alliance and School of Public Health and Tropical Medicine, James Cook University, Cairns, Queensland, Australia"
    },
    {
      "name": "Peter J. Hotez",
      "first": "Peter J.",
      "last": "Hotez",
      "affiliation": "Department of Pediatrics and Molecular Virology and Microbiology, and National School of Tropical Medicine, Sabin Vaccine Institute and Texas Children's Hospital Center for Vaccine Development, Baylor College of Medicine, Houston, Texas, United States of America"
    },
    {
      "name": "Alex Loukas",
      "first": "Alex",
      "last": "Loukas",
      "affiliation": "Queensland Tropical Health Alliance and School of Public Health and Tropical Medicine, James Cook University, Cairns, Queensland, Australia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-03",
  "dateAccepted": "2012-01-27",
  "dateReceived": "2011-11-29",
  "volume": "6",
  "number": "3",
  "pages": "e1564",
  "tags": [
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine"
  ],
  "abstract": "The large extracellular loop of the Schistosoma mansoni tetraspanin, Sm-TSP-2, when fused to a thioredoxin partner and formulated with Freund's adjuvants, has been shown to be an efficacious vaccine against murine schistosomiasis. Moreover, Sm-TSP-2 is uniquely recognised by IgG1 and IgG3 from putatively resistant individuals resident in S. mansoni endemic areas in Brazil. In the present study, we expressed Sm-TSP-2 at high yield and in soluble form in E. coli without the need for a solubility enhancing fusion partner. We also expressed in E. coli a chimera called Sm-TSP-2/5B, which consisted of Sm-TSP-2 fused to the immunogenic 5B region of the hookworm aspartic protease and vaccine antigen, Na-APR-1. Sm-TSP-2 formulated with alum/CpG showed significant reductions in adult worm and liver egg burdens in two separate murine schistosomiasis challenge studies. Sm-TSP-2/5B afforded significantly greater protection than Sm-TSP-2 alone when both antigens were formulated with alum/CpG. The enhanced protection obtained with the chimeric fusion protein was associated with increased production of anti-Sm-TSP-2 antibodies and IL-4, IL-10 and IFN-γ from spleen cells of vaccinated animals. Sera from 666 individuals from Brazil who were infected with S. mansoni were screened for potentially deleterious IgE responses to Sm-TSP-2. Anti-Sm-TSP-2 IgE to this protein was not detected (also shown previously for Na-APR-1), suggesting that the chimeric antigen Sm-TSP-2/5B could be used to safely and effectively vaccinate people in areas where schistosomes and hookworms are endemic.",
  "fullText": "Introduction Schistosomiasis ranks among the most important infectious diseases in tropical regions, resulting in a loss of between 4.5 and 92 million Disability-Adjusted Life Years (DALYs) annually and almost 300,000 deaths in sub-Saharan Africa alone [1], [2], [3]. High rates of post-treatment reinfection [1], the inability of periodic chemotherapy to interrupt transmission [4], the exclusive reliance on praziquantel as the only chemotherapeutic option [5], [6] and the unsustainability of mass drug administration [7] has led to the development of new anti-schistosomiasis control measures, inlcuding vaccines, to complement existing initiatives [5], [8], [9]. Molecules lodged in the apical membrane of the schistosome tegument represent vulnerable targets for immunological attack by host antibodies due to their intimate association with the host immune system. One such family of molecules – predicted by proteomic analyses of the schistosome tegument to be accessible to host immunoglobulin [10] – is the tetraspanin integral membrane proteins. Tetraspanins contain four transmembrane domains and two extracellular loops that are predicted to interact with exogenous ligands [11], [12]. Indeed, the second extracellular loop of one of these schistosome tetraspanins, Sm-TSP-2, has proven to be an effective anti-schistosomiasis vaccine, eliciting 57–64% protection in mice vaccinated with the antigen followed by challenge with S. mansoni cercariae [12]. Other schistosome tetraspanins are protective in mouse models of schistosomiasis [10], including Sm23 [13], [14] and Sj-TSP-2, an S. japonicum orthologue of Sm-TSP-2 [15]. Moreover, Sm-TSP-2 was strongly recognised by IgG1 and IgG3 from putatively resistant but not from chronically infected individuals [12], further highlighting the promise of this antigen as a subunit vaccine against human schistosomiasis. The tegument of adult and schistosomula of S. mansoni is thinner and distinctly more vacuolated compared to controls after in vitro treatment with Sm-tsp-2 double-stranded RNA (dsRNA) [16]. Moreover, injection of mice with schistosomula pre-treated with Sm-tsp-2 dsRNA resulted in the recovery of 83% fewer parasites from the mesenteries compared to controls [16], highlighting the importance of Sm-TSP-2 in proper tegument development and worm survival, and providing a potential mechanism by which the vaccine exerts its protective effect. In an earlier study, we reported the production of a chimeric form of Sm-TSP-2, consisting of Sm-TSP-2 fused to the immunodominant and neutralizing 5B region of the hookworm aspartic protease Na-APR-1, termed Sm-TSP-2/5B [17]. Hookworm infection and schistosomiasis caused by S. mansoni are co-endemic in much of sub-Saharan Africa and Brazil, and there is potential interest in developing a vaccine that targets both of these high prevalence and high disease burden helminths [18]. Na-APR-1/5B is a 40 amino acid fragment of the protease that contains an immunodominant alpha helix, A291Y, which is the target epitope recognized by polyclonal and monoclonal antibodies that are capable of neutralizing the catalytic activity of Na-APR-1 [17]. Na-APR-1/5B could not be produced in soluble form, but when fused to Sm-TSP-2, it was produced in soluble form by E. coli and induced antibodies upon vaccination that neutralized the enzymatic activity of Na-APR-1; the chimera is currently under investigation as a hookworm vaccine. Using a mouse model"
}