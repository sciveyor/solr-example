{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000609",
  "doi": "10.1371/journal.pntd.0000609",
  "externalIds": [
    "pii:09-PNTD-VP-0549R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Controlling Sleeping Sickness—“When Will They Ever Learn?”",
  "authors": [
    {
      "name": "David Molyneux",
      "first": "David",
      "last": "Molyneux",
      "affiliation": "Centre for Neglected Tropical Diseases, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Joseph Ndung'u",
      "first": "Joseph",
      "last": "Ndung'u",
      "affiliation": "FIND Diagnostics, Geneva, Switzerland"
    },
    {
      "name": "Ian Maudlin",
      "first": "Ian",
      "last": "Maudlin",
      "affiliation": "Centre for Infectious Diseases, College of Medicine and Veterinary Medicine, The University of Edinburgh, Summerhall, Edinburgh, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-05",
  "volume": "4",
  "number": "5",
  "pages": "e609",
  "tags": [
    "Immunology",
    "Infectious Diseases"
  ],
  "fullText": "The recent announcement that WHO has approved the use of a combination of nifurtimox and eflornithine to treat chronic Gambian sleeping sickness, caused by Trypanosoma brucei gambiense, is a welcome step in the seemingly interminable process of searching for less toxic drugs to treat this devastating disease [1]. Arsenical drugs were first used in 1905; melarsoprol remains the drug most frequently used for late stage disease and is a drug for which resistance is now a major problem [2]. Over the last 50 years the needs of countries afflicted by sleeping sickness and of the foci of infection have changed little, and neither have our priority needs for research and disease management: cheap point-of-care diagnostics and effective, non-toxic, and affordable drugs for late stage, or stage 2 disease. What is standing in the way of attaining these apparently modest research aims? Surprisingly, one problem is the very nature of the trypanosome and its vector the tsetse fly; these beautiful and biologically fascinating creatures continue to attract considerable research funding, resulting in a burgeoning industry; a PubMed search for Trypanosoma brucei reveals 2,624 papers published in the last decade producing outputs that, while admittedly elegant, are remote from the needs of patients from afflicted rural populations and are disproportionate to the sums needed to support research to assist disease management. Could it be that, as development economists suspect, “we have here a silent conspiracy of professional interests whose scientific work is justified on the basis of poverty reduction but who would be devastated if they were actually successful in these terms?” [3]. It would be timely now to take a very hard look at the global research agenda within the context of a forgotten hinterland which, up until the 1960s, demonstrated that this disease could be controlled effectively by unsophisticated means—a history all too conveniently forgotten by, or perhaps unknown to, most of the current generation of researchers. The ability of the medical services to translate effective tools and technologies into public health successes when faced with the devastating epidemics of the past was dependent on dedicated teams, skilled staff, and adequate and appropriate financing. In West Africa, epidemics of Gambian sleeping sickness were controlled by the use of chemoprophylactic treatment or “pentamidisation” of populations led by Jamot and military style campaigns; in East and southern Africa where the authorities were equally concerned with the health of livestock, the diagnosis and treatment approach for Rhodesian sleeping sickness was allied to vector control [4]. Targeted, effective, and appropriate research (supported largely by French and British aid) allied to realistic health service delivery options worked, and by the 1960s sleeping sickness was not considered a significant public health problem. The numbers of new cases each year was minimal and controlled effectively in all endemic countries of West and Central Africa through active screening by mobile teams who diagnosed cases by microscopy (gland puncture and lumbar puncture) and treated patients with pentamidine or suramin and melarsoprol as appropriate. Whilst there were relapses, there was"
}