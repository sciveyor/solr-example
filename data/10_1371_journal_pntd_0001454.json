{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001454",
  "doi": "10.1371/journal.pntd.0001454",
  "externalIds": [
    "pii:PNTD-D-11-00909"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "High Prevalence of Drug Resistance in Animal Trypanosomes without a History of Drug Exposure",
  "authors": [
    {
      "name": "Simbarashe Chitanga",
      "first": "Simbarashe",
      "last": "Chitanga",
      "affiliation": "Institute of Tropical Medicine Antwerp, Antwerp, Belgium; Department of Paraclinical Studies, School of Veterinary Medicine, University of Zambia, Lusaka, Zambia"
    },
    {
      "name": "Tanguy Marcotty",
      "first": "Tanguy",
      "last": "Marcotty",
      "affiliation": "Institute of Tropical Medicine Antwerp, Antwerp, Belgium; Department of Veterinary Tropical Diseases, University of Pretoria, Onderstepoort, South Africa"
    },
    {
      "name": "Boniface Namangala",
      "first": "Boniface",
      "last": "Namangala",
      "affiliation": "Department of Paraclinical Studies, School of Veterinary Medicine, University of Zambia, Lusaka, Zambia"
    },
    {
      "name": "Peter Van den Bossche",
      "first": "Peter",
      "last": "Van den Bossche",
      "affiliation": "Institute of Tropical Medicine Antwerp, Antwerp, Belgium; Department of Veterinary Tropical Diseases, University of Pretoria, Onderstepoort, South Africa"
    },
    {
      "name": "Jan Van Den Abbeele",
      "first": "Jan",
      "last": "Van Den Abbeele",
      "affiliation": "Institute of Tropical Medicine Antwerp, Antwerp, Belgium"
    },
    {
      "name": "Vincent Delespaux",
      "first": "Vincent",
      "last": "Delespaux",
      "affiliation": "Institute of Tropical Medicine Antwerp, Antwerp, Belgium"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-12",
  "dateAccepted": "2011-11-12",
  "dateReceived": "2011-09-13",
  "volume": "5",
  "number": "12",
  "pages": "e1454",
  "tags": [
    "Biology",
    "Microbiology",
    "Protozoology",
    "Veterinary diseases",
    "Veterinary parasitology",
    "Veterinary science"
  ],
  "abstract": "Background\n          \nTrypanosomosis caused by Trypanosoma congolense is a major constraint to animal health in sub-Saharan Africa. Unfortunately, the treatment of the disease is impaired by the spread of drug resistance. Resistance to diminazene aceturate (DA) in T. congolense is linked to a mutation modifying the functioning of a P2-type purine-transporter responsible for the uptake of the drug. Our objective was to verify if the mutation was linked or not to drug pressure.\n\n          Methodology/Principal Findings\n          \nThirty-four T. congolense isolates sampled from tsetse or wildlife were screened for the DA-resistance linked mutation using DpnII-PCR-RFLP. The results showed 1 sensitive, 12 resistant and 21 mixed DpnII-PCR-RFLP profiles. This suggests that the mutation is present on at least one allele of each of the 33 isolates. For twelve of the isolates, a standard screening method in mice was used by (i) microscopic examination, (ii) trypanosome-specific 18S-PCR after 2 months of observation and (iii) weekly trypanosome-specific 18S-PCR for 8 weeks. The results showed that all mice remained microscopically trypanosome-positive after treatment with 5 mg/kg DA. With 10 and 20 mg/kg, 8.3% (n = 72) and 0% (n = 72) of the mice became parasitologically positive after treatment. However, in these latter groups the trypanosome-specific 18S-PCR indicated a higher degree of trypanosome-positivity, i.e., with a unique test, 51.4% (n = 72) and 38.9% (n = 72) and with the weekly tests 79.2% (n = 24) and 66.7% (n = 24) for 10 and 20 mg/kg respectively.\n\n          Conclusion/Significance\n          \nThe widespread presence of the DA-resistance linked mutation in T. congolense isolated from wildlife suggests that this mutation is favourable to parasite survival and/or its dissemination in the host population independent from the presence of drug. After treatment with DA, those T. congolense isolates cause persisting low parasitaemias even after complete elimination of the drug and with little impact on the host's health.",
  "fullText": "Introduction Animal trypanosomosis is one of the major constraints to animal health and production in sub-Saharan Africa and has a major impact on people's livelihoods. The annual estimated direct and indirect losses due to the disease run into billions of dollars [1]. The fight against the disease is either managed by the control of the vector or of the parasite or a combination of both. However, in poor rural communities, which are mostly affected by the disease, control is mainly relying on the use of trypanocidal drugs [2]. The main drugs used by livestock keepers are isometamidium chloride (ISM) which has both curative and prophylactic effects and DA which has only curative properties. These drugs have been in use for more than half a century now. Geerts &amp; Holmes [3] estimated that ∼35 million doses of trypanocides are administered every year in sub-Saharan Africa, with ISM, ethidium bromide and DA representing 40%, 26% and 33% respectively. Despite the high usage of these veterinary trypanocides, the interest of pharmaceutical industries to invest in research for developing new products remains low, leaving farmers to rely on the existing drugs. Due to the privatization of veterinary services in most parts of Africa, farmers have easy access to these trypanocides and this has resulted in rampant misuse and under-dosage of the medications, actions which have been blamed for the emergence of trypanocidal drug resistance [4], [5]. To date, there are 18 countries in which trypanocidal drug resistance has been reported [2] and more recently in Benin, Ghana and Togo (Réseau d'épidémiosurveillance de la résistance aux trypanocides et aux acaricides en Afrique de l'Ouest – RESCAO, unpublished data). However, most of these reports seem to be confined to areas where the disease is endemic [6]. Whilst reports of the occurrence of trypanocidal drug resistance are increasing, it is not really clear whether this is due to a real increase of the trypanocide resistance problem or just an increased interest by scientists [2]. However, a report by Delespaux et al. [7] of a five-fold increase in the prevalence of DA resistance over a seven year period in the Eastern Province of Zambia, suggests that there is indeed an aggravation of the phenomenon. Even more worrying are the recent reports of multiple drug resistance (to ISM and DA) [8] because this is threatening the last stand to overcome drug resistance through the use of the sanative pair. Here, the concept of the sanative pair recommends the use of two trypanocides (e.g. DA and ISM) unlikely to induce cross-resistance. The first drug is used until resistant strains of trypanosomes appear and then, the second is substituted and used until the resistant strains have disappeared from cattle and tsetse [9]. DA uptake is predominantly driven by a P2-type purine transporter (TbAT1) in T. brucei and a set of six point mutations in this gene has been shown to be linked with resistance to the veterinary drug DA [10]. However, Delespaux et al. [11] found that in T. congolense a"
}