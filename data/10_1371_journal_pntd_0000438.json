{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000438",
  "doi": "10.1371/journal.pntd.0000438",
  "externalIds": [
    "pii:09-PNTD-RA-0005R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "A Four-Antigen Mixture for Rapid Assessment of Onchocerca volvulus Infection",
  "authors": [
    {
      "name": "Peter D. Burbelo",
      "first": "Peter D.",
      "last": "Burbelo",
      "affiliation": "Neurobiology and Pain Therapeutics Section, Laboratory of Sensory Biology, National Institute of Dental and Craniofacial Research, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Hannah P. Leahy",
      "first": "Hannah P.",
      "last": "Leahy",
      "affiliation": "Neurobiology and Pain Therapeutics Section, Laboratory of Sensory Biology, National Institute of Dental and Craniofacial Research, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Michael J. Iadarola",
      "first": "Michael J.",
      "last": "Iadarola",
      "affiliation": "Neurobiology and Pain Therapeutics Section, Laboratory of Sensory Biology, National Institute of Dental and Craniofacial Research, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Thomas B. Nutman",
      "first": "Thomas B.",
      "last": "Nutman",
      "affiliation": "Laboratory of Parasitic Diseases, National Institutes of Health, Bethesda, National Institutes of Health, Bethesda, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-05",
  "dateAccepted": "2009-04-21",
  "dateReceived": "2009-01-06",
  "volume": "3",
  "number": "5",
  "pages": "e438",
  "tags": [
    "Infectious Diseases/Helminth Infections"
  ],
  "abstract": "Background\n\nOnchocerciasis, an infection caused by the filarial nematode Onchocerca volvulus, is a major public health concern. Given the debilitating symptoms associated with onchocerciasis and concerns about recrudescence in areas of previous onchocerciasis control, more efficient tools are needed for diagnosis and monitoring of control measures. We investigated whether luciferase immunoprecipitation systems (LIPS) may be used as a more rapid, specific, and standardized diagnostic assay for Onchocerca volvulus infection.\n\nMethods\n\nFour recombinantly produced Onchocerca volvulus antigens (Ov-FAR-1, Ov-API-1, Ov-MSA-1 and Ov-CPI-1) were tested by LIPS on a large cohort of blinded sera comprised of both uninfected controls and patients with a proven parasitic infection including Onchocerca volvulus (Ov), Wuchereria bancrofti (Wb), Loa loa (Ll), Strongyloides stercoralis (Ss), and with other potentially cross-reactive infections. In addition to testing all four Ov antigens separately, a mixture that tested all four antigens simultaneously was evaluated in the standard 2-hour incubation format as well as in a 15-minute rapid LIPS format.\n\nFindings\n\nAntibody responses to the four different Ov antigens allowed for unequivocal differentiation between Ov-infected and uninfected control sera with 100% sensitivity and 100% specificity. Analysis of the antibody titers to each of these four antigens in individual Ov-infected sera revealed that they were markedly different and did not correlate (rS = –0.11 to 0.58; P = 0.001 to 0.89) to each other. Compared to Ov-infected sera, patients infected with Wb, Ll, Ss, and other conditions had markedly lower geometric mean antibody titers to each of the Ov 4 antigens (P&lt;0.0002 for each antigen). The simplified method of using a mixture of the 4 Ov antigens simultaneously in the standard format or a quick 15-minute format (QLIPS) showed 100% sensitivity and 100% specificity in distinguishing the Ov-infected sera from the uninfected control sera. Finally, the QLIPS format had the best performance with 100% sensitivity and specificity values of 76%, 84% and 93% for distinguishing Ov from Wb, Ll and Ss-infected sera.\n\nConclusions\n\nThe multi-antigen LIPS assay can be used as a rapid, high throughput, and specific tool to not only to diagnose individual Ov infections but also as a sensitive and potentially point-of-care method for early detection of recrudescent infections in areas under control and for mapping new areas of transmission of Ov infection.",
  "fullText": "Introduction As one of the neglected tropical diseases (NTDs), onchocerciasis (or ‘river blindness’), caused by the filarial parasite Onchocerca volvulus (Ov), can lead to blindness and disabling dermatitis. Past and ongoing control measures, aimed at interrupting transmission by vector control (Onchocerciasis Control Programme in West Africa, OCP), reducing the burden of morbidity to tolerable levels (African Programme for Onchocerciasis Control, APOC), and eliminating the reservoir of infection wherever possible (Onchocerciasis Elimination Program for the Americas, OEPA), have led to substantial decreases in the prevalence of infection and the risk of blindness [1]. Despite these measures, which currently rely almost exclusively on ivermectin distribution, estimates suggest that 37 million people remain infected with Ov with an additional 90 million people being at risk in Africa [2]. Superimposed on this estimate of Ov-infected individuals has been the concern about ivermectin resistance [3] and the serious adverse events associated with ivermectin administration in areas where another filarial parasite, Loa loa, is co-endemic [4]. In support of elimination programs for onchocerciasis, various criteria for elimination have been proposed that rely on sensitive molecular xenomonitoring of infection in the Simulium vectors, epidemiologic and clinical criteria, and proven diagnostic assessments [5]. For the diagnostics in support of certification programs for onchocerciasis elimination, detection of microfilariae in skin snips have long held primacy, although sensitive and specific serodiagnostic assays [6] have largely supplanted skin snipping because these antibody-based tests are less invasive, more sensitive and can detect pre-patent infection [7]. A variety of serological tests employing different Ov antigens have been described (reviewed in [8] including those that have used cocktails of antigens [9],[10],[11]. Each antigen, when tested, has had the characteristic of identifying Ov infection early (often pre-patency) in the infection. More recently a field-applicable diagnostic immunoassay based on one of these Ov-specific recombinant antigens, Ov-16, showed 80% sensitivity for detecting Ov-infected sera [12],[13], while an ELISA employing a recombinant hybrid Ov protein showed 93% sensitivity [14],[15]. Despite the high sensitivity of all these immunoassays, each of these tests have had some difficulty discriminating Ov-infected sera from some other filarial infections that can be co-endemic with O. volvulus such as Wuchereria bancrofti (a causal agent of lymphatic filariasis) and L. loa (the causal agent of loiasis). Recently, Renilla luciferase (Ruc)-antigen fusions produced in Cos1 cells were used in a simple immunoprecipitation assay called LIPS (denoting luciferase immunoprecipitation systems) to measure antibody responses to infections by the intestinal nematode Strongyloides stercoralis (Ss) [16] and the filarial nematode L. loa (Ll) [17]. In these studies, LIPS showed improved performance to existing ELISAs and offered a highly sensitive, robust and high-throughput testing format. In the present study, we utilized the LIPS technology for the assessment of Ov-specific antibodies. The results presented here demonstrate that LIPS assay detection of antibodies to a four-antigen cocktail in a standard 2-hour format or with a rapid 15-minute LIPS test (so-called QLIPS, for quick LIPS) generates a highly robust, sensitive and specific test for identifying O. volvulus infection. Materials and Methods Ethics statement Informed"
}