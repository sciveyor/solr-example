{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000271",
  "doi": "10.1371/journal.pntd.0000271",
  "externalIds": [
    "pii:07-PNTD-RA-0109R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Landscape Diversity Related to Buruli Ulcer Disease in Côte d'Ivoire",
  "authors": [
    {
      "name": "Télesphore Brou",
      "first": "Télesphore",
      "last": "Brou",
      "affiliation": "Université d'Artois, Faculté d'Histoire-Géographie, Arras, France"
    },
    {
      "name": "Hélène Broutin",
      "first": "Hélène",
      "last": "Broutin",
      "affiliation": "Génétique et Evolution des Maladies Infectieuses, GEMI-UMR 2724 IRD-CNRS, Equipe “Dynamique des Systèmes et Maladies Infectieuses”, Montpellier, France"
    },
    {
      "name": "Eric Elguero",
      "first": "Eric",
      "last": "Elguero",
      "affiliation": "Génétique et Evolution des Maladies Infectieuses, GEMI-UMR 2724 IRD-CNRS, Equipe “Dynamique des Systèmes et Maladies Infectieuses”, Montpellier, France"
    },
    {
      "name": "Henri Asse",
      "first": "Henri",
      "last": "Asse",
      "affiliation": "Institut Raoul Follereau d'Adopzé, Université de Cocody, UFR des Sciences Médicales, Abidjan, Côte d'Ivoire"
    },
    {
      "name": "Jean-François Guegan",
      "first": "Jean-François",
      "last": "Guegan",
      "affiliation": "Génétique et Evolution des Maladies Infectieuses, GEMI-UMR 2724 IRD-CNRS, Equipe “Dynamique des Systèmes et Maladies Infectieuses”, Montpellier, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-07",
  "dateAccepted": "2008-07-07",
  "dateReceived": "2007-05-29",
  "volume": "2",
  "number": "7",
  "pages": "e271",
  "tags": [
    "Ecology/Spatial and Landscape Ecology",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Background\n\nBuruli ulcer disease (BU), due to the bacteria Mycobacterium ulcerans, represents an important and emerging public health problem, especially in many African countries. Few elements are known nowadays about the routes of transmission of this environmental bacterium to the human population.\n\nMethodology/Principal Findings\n\nIn this study, we have investigated the relationships between the incidence of BU in Côte d'Ivoire, western Africa, and a group of environmental variables. These environmental variables concern vegetation, crops (rice and banana), dams, and lakes. Using a geographical information system and multivariate analyses, we show a link between cases of BU and different environmental factors for the first time on a country-wide scale. As a result, irrigated rice field cultures areas, and, to a lesser extent, banana fields as well as areas in the vicinity of dams used for irrigation and aquaculture purposes, represent high-risk zones for the human population to contract BU in Côte d'Ivoire. This is much more relevant in the central part of the country.\n\nConclusions/Significance\n\nAs already suspected by several case-control studies in different African countries, we strengthen in this work the identification of high-risk areas of BU on a national spatial scale. This first study should now be followed by many others in other countries and at a multi-year temporal scale. This goal implies a strong improvement in data collection and sharing in order to achieve to a global picture of the environmental conditions that drive BU emergence and persistence in human populations.",
  "fullText": "Introduction Buruli ulcer is a severe human skin disease caused by Mycobacterium ulcerans. It represents now the third mycobacterial infection in the world behind tuberculosis due to M. tuberculosis and leprosy, caused by M. leprae. The first clinical description of the disease agent was done in Australia in 1958 [1], even though disease cases have been recorded since the end of the XIXth century in Uganda, in the Buruli area [2]. During the last decades, a dramatic extension of the spatial distribution of Buruli ulcer disease as well as increase of the number of infected people has been reported in many parts of the world. Highest incidences are now observed in Western Africa with 20,000, 6,000 and 4,000 cases observed in 2005 in Côte d'Ivoire, Ghana and Benin, respectively [1],[3],[4]. Mycobacterium ulcerans is an environmental bacterium and its mode of transmission to humans is still unclear, this is why the disease is often referred to as the “mysterious disease” or the “new leprosy”. Recent findings on the life cycle of the Buruli ulcer's agent have enhanced current evidence on several points. First, it has been shown that M. ulcerans can develop as biofilms on the surface of aquatic plants [5]. More specifically, some freshwater aquatic plants might be involved in the mycobacterium life-cycle as potential intermediate hosts or trophic chain concentrators [5]–[7]. Secondly, contrasted animal species were found infected by the bacterium in natural conditions, e.g. fishes, frogs [8]–[9] or koalas (see [10] for review). Recently, an impressive field study also generated additional environmental data regarding M. ulcerans in nature [11]. Third, aquatic insects are also suspected to act as vector and to transmit the disease by biting. It has been demonstrated that M. ulcerans is present in salivary glands of African water bugs of the family Naucoridae, and that infected water bugs could transfer the pathogen to mice [6],[12],[13]. Infected insects were also found in endemic areas. Finally, it is generally admitted by medical and scientific communities that specific environmental niches, which still need to be precisely defined, favour the occurrence of the disease [11], [14]–[18]. Based on several case-control studies performed in different African countries, freshwater ecosystems like rivers, man-made ponds and lakes, or marshy zones and irrigated perimeters represent risk factors to BU [19]–[21]. At nation-wide or regional scale, other studies also showed relations between BU infections and different environmental factors [22]–[24], as for instance landscape cover attributes [25] or arsenic in water [26] in Ghana. Despite these specific studies, Buruli ulcer is still a mysterious disease and all new findings will contribute to help national and international public health authorities to fight this highly deleterious pathogen and neglected disease. For this reason, all information on the relations between the environment and the disease occurrence are highly relevant for a better understanding of the disease as a whole. Here we propose to perform a first nation-wide scale study in Côte d'Ivoire of the link between environmental but also socio-economic factors and Buruli ulcer cases, based on spatial"
}