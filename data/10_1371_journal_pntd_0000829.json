{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000829",
  "doi": "10.1371/journal.pntd.0000829",
  "externalIds": [
    "pii:10-PNTD-RA-1133R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Leishmania-Specific Surface Antigens Show Sub-Genus Sequence Variation and Immune Recognition",
  "authors": [
    {
      "name": "Daniel P. Depledge",
      "first": "Daniel P.",
      "last": "Depledge",
      "affiliation": "Centre for Immunology and Infection, Department of Biology, Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Lorna M. MacLean",
      "first": "Lorna M.",
      "last": "MacLean",
      "affiliation": "Centre for Immunology and Infection, Department of Biology, Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Michael R. Hodgkinson",
      "first": "Michael R.",
      "last": "Hodgkinson",
      "affiliation": "Centre for Immunology and Infection, Department of Biology, Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Barbara A. Smith",
      "first": "Barbara A.",
      "last": "Smith",
      "affiliation": "Centre for Immunology and Infection, Department of Biology, Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Andrew P. Jackson",
      "first": "Andrew P.",
      "last": "Jackson",
      "affiliation": "Wellcome Trust Sanger Institute, Wellcome Trust Genome Campus, Hinxton, United Kingdom"
    },
    {
      "name": "Saufung Ma",
      "first": "Saufung",
      "last": "Ma",
      "affiliation": "Centre for Immunology and Infection, Department of Biology, Hull York Medical School, University of York, York, United Kingdom"
    },
    {
      "name": "Silvia R. B. Uliana",
      "first": "Silvia R. B.",
      "last": "Uliana",
      "affiliation": "Departamento de Parasitologia, Instituto de Ciências Biomédicas, Universidade de São Paulo, São Paulo, Brazil"
    },
    {
      "name": "Deborah F. Smith",
      "first": "Deborah F.",
      "last": "Smith",
      "affiliation": "Centre for Immunology and Infection, Department of Biology, Hull York Medical School, University of York, York, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-09",
  "dateAccepted": "2010-08-31",
  "dateReceived": "2010-05-03",
  "volume": "4",
  "number": "9",
  "pages": "e829",
  "tags": [
    "Cell Biology/Gene Expression",
    "Genetics and Genomics/Genomics",
    "Infectious Diseases/Protozoal Infections",
    "Molecular Biology"
  ],
  "abstract": "Background\n\nA family of hydrophilic acylated surface (HASP) proteins, containing extensive and variant amino acid repeats, is expressed at the plasma membrane in infective extracellular (metacyclic) and intracellular (amastigote) stages of Old World Leishmania species. While HASPs are antigenic in the host and can induce protective immune responses, the biological functions of these Leishmania-specific proteins remain unresolved. Previous genome analysis has suggested that parasites of the sub-genus Leishmania (Viannia) have lost HASP genes from their genomes.\n\nMethods/Principal Findings\n\nWe have used molecular and cellular methods to analyse HASP expression in New World Leishmania mexicana complex species and show that, unlike in L. major, these proteins are expressed predominantly following differentiation into amastigotes within macrophages. Further genome analysis has revealed that the L. (Viannia) species, L. (V.) braziliensis, does express HASP-like proteins of low amino acid similarity but with similar biochemical characteristics, from genes present on a region of chromosome 23 that is syntenic with the HASP/SHERP locus in Old World Leishmania species and the L. (L.) mexicana complex. A related gene is also present in Leptomonas seymouri and this may represent the ancestral copy of these Leishmania-genus specific sequences. The L. braziliensis HASP-like proteins (named the orthologous (o) HASPs) are predominantly expressed on the plasma membrane in amastigotes and are recognised by immune sera taken from 4 out of 6 leishmaniasis patients tested in an endemic region of Brazil. Analysis of the repetitive domains of the oHASPs has shown considerable genetic variation in parasite isolates taken from the same patients, suggesting that antigenic change may play a role in immune recognition of this protein family.\n\nConclusions/Significance\n\nThese findings confirm that antigenic hydrophilic acylated proteins are expressed from genes in the same chromosomal region in species across the genus Leishmania. These proteins are surface-exposed on amastigotes (although L. (L.) major parasites also express HASPB on the metacyclic plasma membrane). The central repetitive domains of the HASPs are highly variant in their amino acid sequences, both within and between species, consistent with a role in immune recognition in the host.",
  "fullText": "Introduction Kinetoplastid parasites of the genus Leishmania cause a diverse spectrum of infectious diseases, the leishmaniases, in tropical and subtropical regions of the world (reviewed in [1]). Mammalian-infective Leishmania species are divided into two subgenera, Leishmania (Leishmania) and Leishmania (Viannia), that differ in their developmental cycles within the female sandfly vector. Transmission of species of both subgenera from vector to mammalian host requires parasite differentiation into non-replicative flagellated metacyclic promastigotes. These forms are inoculated when a female sandfly takes a blood meal; the parasites enter resident dermal macrophages and transform into replicative amastigotes that can be disseminated to other tissues, often inducing immuno-inflammatory responses and persistent infection. The fate of Leishmania amastigotes in the host determines disease type, which can range from cutaneous or mucocutaneous infection to diffuse cutaneous or the potentially fatal visceral leishmaniasis [1]. Comparative sequencing of three Leishmania genomes, L. (L.) major and L. (L.) infantum from the L. (Leishmania) sub-genus and L. (V.) braziliensis from the L. (Viannia) sub-genus, has revealed high conservation of gene content and synteny across the genus [2], [3], [4]. A number of loci show significant variation in size and gene complement between species, however. One example is the GP63 locus, containing tandemly arrayed genes coding for surface glycoproteins that are critical for macrophage invasion and virulence [5], [6]. This locus is present in all three sequenced Leishmania species but varies considerably in size and number of genes present. Another example is the LmcDNA16 locus, originally identified on chromosome 23 of L. (L.) major [7], [8], [9], [10] but since also found in L. (L.) donovani [11], L. (L.) infantum [3] and other L. (Leishmania) species. This locus is characterised by the presence of two Leishmania-specific gene families encoding hydrophilic acylated surface proteins (HASPs; [7], [9], [10], [12], [13]) and small hydrophilic endoplasmic reticulum associated proteins (SHERPs; [14]). The HASPs have conserved N- and C- termini but a sub-set, the HASPBs, possess divergent central domains containing hydrophilic amino acid repeats that exhibit both inter- and intra specific variation in their size and composition [10], [11], [15]. Acylation of the HASPBs involves N-terminal myristoylation and palmitoylation, modifications that are required for protein targeting to the parasite plasma membrane [13]. In L. major, HASPB expression is confined to mammalian-infective stages of the parasite life cycle, the metacyclics and amastigotes. While HASPBs are antigenic in the host [16], [17] and can induce protective immune responses [18], [19], [20], the biological functions of both the HASP and SHERP proteins remain unresolved [7]. To date, while the LmcDNA16 locus has been identified in all L. (Leishmania) species analysed, expression and localization of the encoded proteins has not been studied in New World L. (Leishmania) species. Here, we present analysis of HASPB expression in two representative sub-species, L. m. mexicana and L. m. amazonensis. Furthermore, the LmcDNA16 locus has been reported as absent from the published L. (V.) braziliensis genome, one of the few chromosomal regions showing strong divergence between sub-genera [3]. Instead, an apparently unrelated region containing"
}