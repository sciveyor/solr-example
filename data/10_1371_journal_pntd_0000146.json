{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000146",
  "doi": "10.1371/journal.pntd.0000146",
  "externalIds": [
    "pii:07-PNTD-RA-0143R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Helminthiasis and Hygiene Conditions of Schools in Ikenne, Ogun State, Nigeria",
  "authors": [
    {
      "name": "Uwem Friday Ekpo",
      "first": "Uwem Friday",
      "last": "Ekpo",
      "affiliation": "Department of Biological Sciences, University of Agriculture, Abeokuta, Nigeria"
    },
    {
      "name": "Simon Nnayere Odoemene",
      "first": "Simon Nnayere",
      "last": "Odoemene",
      "affiliation": "Department of Biological Sciences, University of Agriculture, Abeokuta, Nigeria"
    },
    {
      "name": "Chiedu Felix Mafiana",
      "first": "Chiedu Felix",
      "last": "Mafiana",
      "affiliation": "Department of Biological Sciences, University of Agriculture, Abeokuta, Nigeria"
    },
    {
      "name": "Sammy Olufemi Sam-Wobo",
      "first": "Sammy Olufemi",
      "last": "Sam-Wobo",
      "affiliation": "Department of Biological Sciences, University of Agriculture, Abeokuta, Nigeria"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-01",
  "dateAccepted": "2007-11-13",
  "dateReceived": "2007-06-22",
  "volume": "2",
  "number": "1",
  "pages": "e146",
  "tags": [
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Background\n    \nA study of the helminth infection status of primary-school children and the hygiene condition of schools in Ikenne Local Government Area of Ogun State, Nigeria was undertaken between November 2004 and February 2005 to help guide the development of a school-based health programme.\n\n    Methods and Findings\n    \nThree primary schools were randomly selected: two government-owned schools (one urban and the other rural) and one urban private school. No rural private schools existed to survey. A total of 257 schoolchildren aged 4–15 y, of whom 146 (56.8%) were boys and 111 (43.2%) were girls, took part in the survey. A child survey form, which included columns for name, age, sex, and class level, was used in concert with examination of stool samples for eggs of intestinal helminths. A school survey form was used to assess the conditions of water supply, condition of latrines, presence of soap for handwashing, and presence of garbage around the school compound. The demographic data showed that the number of schoolchildren gradually decreased as their ages increased in all three schools. The sex ratio was proportional in the urban school until primary level 3, after which the number of female pupils gradually decreased, whereas in the private school, sexes were proportionally distributed even in higher classes. The prevalence of helminth infection was 54.9% of schoolchildren in the urban government school, 63.5% in the rural government school, and 28.4% in the urban private school. Ascaris lumbricoides was the most prevalent species, followed by Trichuris trichiura, Taenia species, and hookworm in the three schools. Prevalence of infection in the government-owned schools was significantly higher than in the private school (χ2 = 18.85, df = 2, p&lt;0.0005). A survey of hygiene conditions in the three schools indicated that in the two government schools tapwater was unavailable, sanitation of latrines was poor, handwashing soap was unavailable, and garbage was present around school compounds. In the private school, in contrast, all hygiene indices were satisfactory.\n\n    Conclusions\n    \nThese results indicate that burden of parasite infections and poor sanitary conditions are of greater public health importance in government-owned schools than in privately owned schools. School health programmes in government-owned schools, including deworming, health education, and improvement of hygiene conditions are recommended.",
  "fullText": "Introduction It is estimated that more than one billion of the world's population is chronically infected with soil-transmitted helminths, and 200 million are infected with schistosomiasis [1]. The high prevalence of these infections is closely correlated with poverty, poor environmental hygiene, and impoverished health services [1],[2]. Parasitic helminths are known causes of morbidities such as nutritional deficiency [3], impaired physical development and learning ability [4], and socioeconomic deprivations in populations living in the tropics where poor hygiene conditions provide an optimal environment for their development and transmission [5]–[7]. In many parts of the developing world, children are reported to have an intestinal helminth infection prevalence rate ranging between 50% and 80% [8],[9]. Although several studies indicate that intestinal helminth infections are highly prevalent among schoolchildren in Ogun State, Nigeria [10]–[13], there is no reported statewide prevalence for intestinal helminths except for ascariasis [13]. Also there are no available data about the demography and hygiene conditions of the state's schools to help guide the development of school health programmes, which are a requirement for sustainable control of soil-transmitted helminths in schoolchildren [14]. At present there is no National School–based parasite or soil-transmitted helminth control programme in Nigeria. In the past, there have been sporadic and uncoordinated deworming programmes untaken by government officials without any baseline information or data. The present study has three aims: first, to evaluate demographic features and intestinal helminth infections among schoolchildren; second, to investigate hygiene conditions in schools; and third, to identify factors that are essential in the development of sustainable school health programmes. Materials and Methods Study area Ikenne Local Government Area (LGA) is one of the twenty LGAs in Ogun State, Nigeria. It is highly urbanized and popular due to the fact that many influential citizens are from Ikenne. The LGA lies in the rainforest vegetation belt of Nigeria. The local government is made up of three urban towns and two rural villages. The urban towns are Iperu, Ikenne, and Ilisan, while the rural villages are Irolu and Ogere. There are 20 government-owned primary schools and 25 approved private primary schools in this LGA. The inhabitants of the Ikenne LGA engage in various types of businesses and for water depend mainly on boreholes, taps, and rainwater stored in tanks, as there are few rivers and streams in the study area. Selection of schools for baseline surveys The study was carried out between November 2004 and February 2005. Three primary schools were chosen: A.U.D. Primary School, Irolu, is rural and government-owned; Salvation Army Primary School, Iperu, is urban and government-owned; and El-Shaddia Nursery and Primary School, Ilisan, is urban and private. These schools were randomly selected in the study area after stratifying to represent different ownership and predominant socieconomic status of location. There were no private schools located in rural area for inclusion in the study. In each school, all pupils were enrolled into the study so that their sociodemographic status could be determined. Consent and ethical approval At the beginning of the study, the reason"
}