{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000871",
  "doi": "10.1371/journal.pntd.0000871",
  "externalIds": [
    "pii:10-PNTD-RA-0956R4"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "CD8 Cells of Patients with Diffuse Cutaneous Leishmaniasis Display Functional Exhaustion: The Latter Is Reversed, In Vitro, by TLR2 Agonists",
  "authors": [
    {
      "name": "Joselín Hernández-Ruiz",
      "first": "Joselín",
      "last": "Hernández-Ruiz",
      "affiliation": "Departamento de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autónoma de México, Hospital General de México OD, México Distrito Federal, Mexico; Dirección de Investigación, Hospital General de México OD, México Distrito Federal, Mexico"
    },
    {
      "name": "Norma Salaiza-Suazo",
      "first": "Norma",
      "last": "Salaiza-Suazo",
      "affiliation": "Departamento de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autónoma de México, Hospital General de México OD, México Distrito Federal, Mexico"
    },
    {
      "name": "Georgina Carrada",
      "first": "Georgina",
      "last": "Carrada",
      "affiliation": "Secretaría de Salud del Estado de Tabasco y Universidad Juárez Autónoma de Tabasco, Villahermosa, Mexico"
    },
    {
      "name": "Sofía Escoto",
      "first": "Sofía",
      "last": "Escoto",
      "affiliation": "Departamento de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autónoma de México, Hospital General de México OD, México Distrito Federal, Mexico"
    },
    {
      "name": "Adriana Ruiz-Remigio",
      "first": "Adriana",
      "last": "Ruiz-Remigio",
      "affiliation": "Departamento de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autónoma de México, Hospital General de México OD, México Distrito Federal, Mexico"
    },
    {
      "name": "Yvonne Rosenstein",
      "first": "Yvonne",
      "last": "Rosenstein",
      "affiliation": "Instituto de Biotecnología y Posgrado en Ciencias Bioquímicas, Universidad Nacional Autónoma de México, Cuernavaca, Mexico"
    },
    {
      "name": "Alejandro Zentella",
      "first": "Alejandro",
      "last": "Zentella",
      "affiliation": "Departamento de Bioquímica, Instituto Nacional de Ciencias Médicas y Nutrición “Salvador Zubirán”, Secretaría de Salud, México Distrito Federal, Mexico; Instituto de Investigaciones Biomédicas, Universidad Nacional Autónoma de México, México Distrito Federal, Mexico"
    },
    {
      "name": "Ingeborg Becker",
      "first": "Ingeborg",
      "last": "Becker",
      "affiliation": "Departamento de Medicina Experimental, Facultad de Medicina, Universidad Nacional Autónoma de México, Hospital General de México OD, México Distrito Federal, Mexico"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-11",
  "dateAccepted": "2010-10-05",
  "dateReceived": "2010-03-11",
  "volume": "4",
  "number": "11",
  "pages": "e871",
  "tags": [
    "Immunology/Immunity to Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Infectious Diseases/Skin Infections",
    "Infectious Diseases/Tropical and Travel-Associated Diseases"
  ],
  "abstract": "Leishmania mexicana (Lm) causes localized (LCL) and diffuse (DCL) cutaneous leishmaniasis. DCL patients have a poor cellular immune response leading to chronicity. It has been proposed that CD8 T lymphocytes (CD8) play a crucial role in infection clearance, although the role of CD8 cytotoxicity in disease control has not been elucidated. Lesions of DCL patients have been shown to harbor low numbers of CD8, as compared to patients with LCL, and leishmanicidal treatment restores CD8 numbers. The marked response of CD8 towards Leishmania parasites led us to analyze possible functional differences between CD8 from patients with LCL and DCL. We compared IFNγ production, antigen-specific proliferation, and cytotoxicity of CD8 purified from PBMC against autologous macrophages (MO) infected with Leishmania mexicana (MOi). Additionally, we analyzed tissue biopsies from both groups of patients for evidence of cytotoxicity associated with apoptotic cells in the lesions. We found that CD8 cell of DCL patients exhibited low cytotoxicity, low antigen-specific proliferation and low IFNγ production when stimulated with MOi, as compared to LCL patients. Additionally, DCL patients had significantly less TUNEL+ cells in their lesions. These characteristics are similar to cellular “exhaustion” described in chronic infections. We intended to restore the functional capacity of CD8 cells of DCL patients by preincubating them with TLR2 agonists: Lm lipophosphoglycan (LPG) or Pam3Cys. Cytotoxicity against MOi, antigen-specific proliferation and IFNγ production were restored with both stimuli, whereas PD-1 (a molecule associated with cellular exhaustion) expression, was reduced. Our work suggests that CD8 response is associated with control of Lm infection in LCL patients and that chronic infection in DCL patients leads to a state of CD8 functional exhaustion, which could facilitate disease spread. This is the first report that shows the presence of functionally exhausted CD8 T lymphocytes in DCL patients and, additionally, that pre-stimulation with TLR2 ligands can restore the effector mechanisms of CD8 T lymphocytes from DCL patients against Leishmania mexicana-infected macrophages.",
  "fullText": "Introduction Leishmaniasis is a zoonotic disease that infects humans as well as a variety of mammalian species. Several Leishmania species, such as L. mexicana, L. amazonensis, L. braziliensis and L. aethiopica can cause two opposite clinical forms of cutaneous leishmaniasis: localized cutaneous leishmaniasis (LCL) and diffuse cutaneous leishmaniasis (DCL) [1]. While the former is relatively benign, consisting of a single ulcer that forms at the infection site, patients with DCL have a continuous uncontrolled spread of the parasite throughout the skin and, in advanced stages, these patients also show parasite invasion of the oro- and nasopharygeal mucosae. Although the prevalence of DCL patients in Mexico is low, they represent a public health problem for which no successful cure has been found. These patients lack an effective T cell immune response capable of activating MOi, and antimonial treatment only achieves transitory remission [2, 3 and 4]. Murine models infected with L. major have shown that both the innate and acquired immune responses are necessary for parasite clearance. Cells such as MO, dendritic cells, NK cells, CD8 and CD4 T lymphocytes; cytokines such as interleukin (IL)-12 and interferon gamma (IFNγ), pattern recognition receptors such as Toll like receptors (TLRs) [6], [7], [8] and effector molecules such as nitric oxide (NO) and superoxide anion (O2-) [9] have been reported to mediate protection, both in mouse models and in humans. It has also been proposed that CD8 T cells play a crucial role in infection clearance, although the role of CD8 cytotoxicity in disease control has not been elucidated. Elevated numbers of CD8 have been reported in blood and lesions of patients infected with L. major and L. mexicana and their protective role has been associated with IFNγ production [10]. Additionally, we have previously reported that the number of CD8 is importantly reduced in lesions of DCL patients infected with L. mexicana, as compared to LCL patients [11]. Thus, a comparative analysis of the overall immune effector functions of CD8 from LCL and DCL patients would permit a more precise definition of the role played by these cells in the disease outcome, both by their cytokine production as well as by their cytotoxicity. We here report a functional analysis of CD8 isolated from peripheral blood of LCL and DCL patients infected with Leishmania mexicana. Our results show significant differences between CD8 of both groups: while CD8 from LCL patients produce high levels of IFNγ and show cytotoxicity against autologous MOi, the CD8 from DCL patients show a diminished response both in cytokine production as well in Leishmania-specific cytotoxicity. Thereafter we analyzed if the differential cytotoxicity observed in vitro also correlated with the number of apoptotic cells in lesions of both groups of patients and found that DCL patients have significantly less TUNEL+ cells than LCL patients. The diminished CD8 response in DCL patients resembled the cellular “exhaustion” reported for CD8 in other chronic diseases [12], [13], [14], [15], where CD8-effector capacity could be restored by different mechanisms including TLR signaling [16], [17], [18]. Since"
}