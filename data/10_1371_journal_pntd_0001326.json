{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001326",
  "doi": "10.1371/journal.pntd.0001326",
  "externalIds": [
    "pii:10-PNTD-RA-1578"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Schistosomiais and Soil-Transmitted Helminth Control in Niger: Cost Effectiveness of School Based and Community Distributed Mass Drug Administration",
  "authors": [
    {
      "name": "Jacqueline Leslie",
      "first": "Jacqueline",
      "last": "Leslie",
      "affiliation": "Schistosomiasis Control Initiative, Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Amadou Garba",
      "first": "Amadou",
      "last": "Garba",
      "affiliation": "Réseau International Schistosomoses Environnement Aménagements et Lutte (RISEAL-Niger), Niamey, Niger; Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland; University of Basel, Basel, Switzerland"
    },
    {
      "name": "Elisa Bosque Oliva",
      "first": "Elisa Bosque",
      "last": "Oliva",
      "affiliation": "Schistosomiasis Control Initiative, Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Arouna Barkire",
      "first": "Arouna",
      "last": "Barkire",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland"
    },
    {
      "name": "Amadou Aboubacar Tinni",
      "first": "Amadou Aboubacar",
      "last": "Tinni",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland"
    },
    {
      "name": "Ali Djibo",
      "first": "Ali",
      "last": "Djibo",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland"
    },
    {
      "name": "Idrissa Mounkaila",
      "first": "Idrissa",
      "last": "Mounkaila",
      "affiliation": "Department of Epidemiology and Public Health, Swiss Tropical and Public Health Institute, Basel, Switzerland"
    },
    {
      "name": "Alan Fenwick",
      "first": "Alan",
      "last": "Fenwick",
      "affiliation": "Schistosomiasis Control Initiative, Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-10",
  "dateAccepted": "2011-08-08",
  "dateReceived": "2010-09-28",
  "volume": "5",
  "number": "10",
  "pages": "e1326",
  "tags": [
    "Global health",
    "Infectious Diseases",
    "Infectious disease control",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Schistosomiasis"
  ],
  "abstract": "Background\n          \nIn 2004 Niger established a large scale schistosomiasis and soil-transmitted helminths control programme targeting children aged 5–14 years and adults. In two years 4.3 million treatments were delivered in 40 districts using school based and community distribution.\n\n          Method and Findings\n          \nFour districts were surveyed in 2006 to estimate the economic cost per district, per treatment and per schistosomiasis infection averted. The study compares the costs of treatment at start up and in a subsequent year, identifies the allocation of costs by activity, input and organisation, and assesses the cost of treatment. The cost of delivery provided by teachers is compared to cost of delivery by community distributers (CDD).\n\nThe total economic cost of the programme including programmatic, national and local government costs and international support in four study districts, over two years, was US$ 456,718; an economic cost/treatment of $0.58. The full economic delivery cost of school based treatment in 2005/06 was $0.76, and for community distribution was $0.46. Including only the programme costs the figures are $0.47 and $0.41 respectively. Differences at sub-district are more marked. This is partly explained by the fact that a CDD treats 5.8 people for every one treated in school.\n\nThe range in cost effectiveness for both direct and direct and indirect treatments is quantified and the need to develop and refine such estimates is emphasised.\n\n          Conclusions\n          \nThe relative cost effectiveness of school and community delivery differs by country according to the composition of the population treated, the numbers targeted and treated at school and in the community, the cost and frequency of training teachers and CDDs. Options analysis of technical and implementation alternatives including a financial analysis should form part of the programme design process.",
  "fullText": "Introduction Schistosomiasis is one of the most prevalent chronic infectious diseases found world-wide and is associated with anaemia, chronic pain, diarrhoea, and under nutrition. It is recognised as a major public health problem in many rural areas, particularly in school-age children. With affordable and sustained control measures morbidity and transmission can be decreased. Robust studies on the implementation of large scale control of schistosomiasis and soil transmitted helminths (STH) are required to strengthen the evidence base on the cost-effectiveness and affordability of such investment [1]–[3]. In particular, evidence on effectiveness is needed to support the strategic planning for expanded treatment and global coverage as well as for national vertical and integrated Neglected Tropical Disease (NTD) programmes. The objectives of this study are to identify: the cost of the Mass Drug Administration (MDA) programme; the cost per person treated; and the costs of treatment as delivered by school based staff and community distributers. A number of studies have identified the costs of targeted and MDA for schistosomiasis control [4]–[9]. These have, with the exception of [4], [9], provided empirical evidence of school based approaches. This paper provides the cost of MDA treatment and compares the costs of the school and community based distribution systems used. It assesses the evidence from other MDA programmes taking account of factors such as the level of school enrolment, coverage levels to consider the general guidelines that can be taken from these works. Description of the Programme for Mass Drug Administration in Niger In 2004 Niger established a national programme to control schistosomiasis and soil-transmitted helminths (PNLBG) supported by the Schistosomiasis Control Initiative (SCI), funded by the Bill and Melinda Gates foundation [10]. Its objective in line with Resolution WHA54.19 was to treat 75% of school age children at risk of infection and in communities where prevalence is over 50% to also treat at risk adults. The purpose being to reduce the morbidity related to schistosomiasis infection to a level at which it would not constitute a public health problem [11]. The primary school net enrolment rate (NER) in 2004 in Niger was 41% (UNESCO UIS global education database Table 5, Enrolment ratios by International Standard Classification of Education (ISCED) level), lower in rural areas; and considerably less than the rate of 68% for Sub Saharan Africa (SSA). To achieve high treatment coverage in targeted school age children and at risk adults two treatment strategies, school-based and community-based distribution, were established. Treatment for S. haematobium was provided every two years in most endemic areas, and annually in high prevalence areas to reduce initial levels. School-based distribution was provided by trained teachers who distributed the drugs to students in the schools. Children not attending school could receive treatment either in the schools or from the Community Drug Distributor (CDD) at home or at another fixed treatment location. The MDA programme was established and rolled out over 2 years from April 2004 across 40 districts in all 7 regions of the country, including the capital city Niamey. The"
}