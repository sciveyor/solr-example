{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000834",
  "doi": "10.1371/journal.pntd.0000834",
  "externalIds": [
    "pii:10-PNTD-RA-0839R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Metabolomics-Based Discovery of Diagnostic Biomarkers for Onchocerciasis",
  "authors": [
    {
      "name": "Judith R. Denery",
      "first": "Judith R.",
      "last": "Denery",
      "affiliation": "Department of Chemistry and Worm Institute for Research and Medicine, The Scripps Research Institute, La Jolla, California, United States of America"
    },
    {
      "name": "Ashlee A. K. Nunes",
      "first": "Ashlee A. K.",
      "last": "Nunes",
      "affiliation": "Department of Chemistry and Worm Institute for Research and Medicine, The Scripps Research Institute, La Jolla, California, United States of America"
    },
    {
      "name": "Mark S. Hixon",
      "first": "Mark S.",
      "last": "Hixon",
      "affiliation": "Department of Chemistry and Worm Institute for Research and Medicine, The Scripps Research Institute, La Jolla, California, United States of America"
    },
    {
      "name": "Tobin J. Dickerson",
      "first": "Tobin J.",
      "last": "Dickerson",
      "affiliation": "Department of Chemistry and Worm Institute for Research and Medicine, The Scripps Research Institute, La Jolla, California, United States of America"
    },
    {
      "name": "Kim D. Janda",
      "first": "Kim D.",
      "last": "Janda",
      "affiliation": "Department of Chemistry and Worm Institute for Research and Medicine, The Scripps Research Institute, La Jolla, California, United States of America; Department of Immunology and Microbial Science, The Scripps Research Institute, La Jolla, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-10",
  "dateAccepted": "2010-09-01",
  "dateReceived": "2010-01-21",
  "volume": "4",
  "number": "10",
  "pages": "e834",
  "tags": [
    "Chemical Biology/Small Molecule Chemistry"
  ],
  "abstract": "Background\n\nDevelopment of robust, sensitive, and reproducible diagnostic tests for understanding the epidemiology of neglected tropical diseases is an integral aspect of the success of worldwide control and elimination programs. In the treatment of onchocerciasis, clinical diagnostics that can function in an elimination scenario are non-existent and desperately needed. Due to its sensitivity and quantitative reproducibility, liquid chromatography-mass spectrometry (LC-MS) based metabolomics is a powerful approach to this problem.\n\nMethodology/Principal Findings\n\nAnalysis of an African sample set comprised of 73 serum and plasma samples revealed a set of 14 biomarkers that showed excellent discrimination between Onchocerca volvulus–positive and negative individuals by multivariate statistical analysis. Application of this biomarker set to an additional sample set from onchocerciasis endemic areas where long-term ivermectin treatment has been successful revealed that the biomarker set may also distinguish individuals with worms of compromised viability from those with active infection. Machine learning extended the utility of the biomarker set from a complex multivariate analysis to a binary format applicable for adaptation to a field-based diagnostic, validating the use of complex data mining tools applied to infectious disease biomarker discovery and diagnostic development.\n\nConclusions/Significance\n\nAn LC-MS metabolomics-based diagnostic has the potential to monitor the progression of onchocerciasis in both endemic and non-endemic geographic areas, as well as provide an essential tool to multinational programs in the ongoing fight against this neglected tropical disease. Ultimately this technology can be expanded for the diagnosis of other filarial and/or neglected tropical diseases.",
  "fullText": "Introduction Onchocerciasis, commonly referred to as “river blindness” is classified by the World Health Organization (WHO) as a neglected tropical disease, afflicting approximately 37 million people in Africa, Central and South America and Yemen, with 89 million more at risk [1]. Symptoms of the disease include acute dermatitis and blindness, the result of which is the loss of 1 million disability-adjusted life years (DALYs) annually [2]. The causative agent, the filarial nematode Onchocerca volvulus, is transmitted in its larval stage between human hosts through the bite of a Simulium (sp.) black fly. Once these parasites have matured into the adult form, they can live for approximately 14 years in subcutaneous nodules within a human host [3]. The drug ivermectin (Mectizan) has served as the principal means of onchocerciasis control [4], however, after initially reducing the number of microfilariae, within a year, the microfilariae return to levels of 20% or higher than that prior to treatment [5]. The combination of the lack of effect of annual ivermectin treatment on adult worm survival and the fecundity of adult females, along with significant fly and human migration patterns has helped to perpetuate the disease. In Africa, where onchocerciasis control programs have been in place since the founding of the Onchocerciasis Control Programme in West Africa (OCP, 1974–2002) and are currently being conducted by the African Programme for Onchocerciasis Control (APOC, 1995-present), diagnosis is an essential aspect of the determination of treatment and distribution of medication. In the Western hemisphere, accurate and robust diagnostics are essential for attaining the goal of disease elimination. Twice yearly dosage of ivermectin, through the efforts of the Onchocerciasis Elimination Program for the Americas (OEPA, 1992-present), has lead to a minimization of infection to 13 foci within six countries in Central and South America. Although mass treatment of onchocerciasis foci in the Western hemisphere is slated to be suspended in 2012 [6], achieving the goal of elimination is contingent upon continued surveillance of the disease. However, proper surveillance is directly dependent on the availability of robust diagnostic technologies used for infection assessment. This need is further underscored in studies of antibiotic treatments being investigated for targeting Wolbachia endosymbiotic bacteria [7]–[10] as well as reports of sub-optimal response to ivermectin treatment [11], [12]. In both of these cases an accurate diagnostic is critical for the analysis of drug efficacy and patient drug response. Currently, multinational control and elimination programs primarily rely on various techniques for diagnosis including: entomological studies of Simulian flies, Ov specific antigen tests, antibody tests, analysis of microfilariae in skin snips, nodule palpation and quality of those nodules that can be excised. There are a number of technical concerns with each technique including: a lack of sensitivity and reproducibility, invasiveness, and the inability to distinguish past from present infection or between filarial diseases [13]–[15]. A small molecule/metabolite based test has the potential for reflecting a more accurate picture of infection status, as it is a comprehensive measure of the effects of posttranslational modification and regulation. Furthermore, small"
}