{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001012",
  "doi": "10.1371/journal.pntd.0001012",
  "externalIds": [
    "pii:PNTD-D-10-00140"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Collagenolytic Activities of the Major Secreted Cathepsin L Peptidases Involved in the Virulence of the Helminth Pathogen, Fasciola hepatica",
  "authors": [
    {
      "name": "Mark W. Robinson",
      "first": "Mark W.",
      "last": "Robinson",
      "affiliation": "Infection, Immunity and Innovation (i3) Institute, University of Technology Sydney (UTS), Sydney, New South Wales, Australia"
    },
    {
      "name": "Ileana Corvo",
      "first": "Ileana",
      "last": "Corvo",
      "affiliation": "Departmento de Genética, Facultad de Medicina, Universidad de la República (UDELAR), Montevideo, Uruguay"
    },
    {
      "name": "Peter M. Jones",
      "first": "Peter M.",
      "last": "Jones",
      "affiliation": "Infection, Immunity and Innovation (i3) Institute, University of Technology Sydney (UTS), Sydney, New South Wales, Australia"
    },
    {
      "name": "Anthony M. George",
      "first": "Anthony M.",
      "last": "George",
      "affiliation": "Infection, Immunity and Innovation (i3) Institute, University of Technology Sydney (UTS), Sydney, New South Wales, Australia"
    },
    {
      "name": "Matthew P. Padula",
      "first": "Matthew P.",
      "last": "Padula",
      "affiliation": "Infection, Immunity and Innovation (i3) Institute, University of Technology Sydney (UTS), Sydney, New South Wales, Australia"
    },
    {
      "name": "Joyce To",
      "first": "Joyce",
      "last": "To",
      "affiliation": "Infection, Immunity and Innovation (i3) Institute, University of Technology Sydney (UTS), Sydney, New South Wales, Australia"
    },
    {
      "name": "Martin Cancela",
      "first": "Martin",
      "last": "Cancela",
      "affiliation": "Departmento de Genética, Facultad de Medicina, Universidad de la República (UDELAR), Montevideo, Uruguay"
    },
    {
      "name": "Gabriel Rinaldi",
      "first": "Gabriel",
      "last": "Rinaldi",
      "affiliation": "Departmento de Genética, Facultad de Medicina, Universidad de la República (UDELAR), Montevideo, Uruguay"
    },
    {
      "name": "Jose F. Tort",
      "first": "Jose F.",
      "last": "Tort",
      "affiliation": "Departmento de Genética, Facultad de Medicina, Universidad de la República (UDELAR), Montevideo, Uruguay"
    },
    {
      "name": "Leda Roche",
      "first": "Leda",
      "last": "Roche",
      "affiliation": "Departmento de Genética, Facultad de Medicina, Universidad de la República (UDELAR), Montevideo, Uruguay"
    },
    {
      "name": "John P. Dalton",
      "first": "John P.",
      "last": "Dalton",
      "affiliation": "Institute of Parasitology, McGill University, St. Anne de Bellevue, Quebec, Canada"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-04",
  "dateAccepted": "2010-12-21",
  "dateReceived": "2010-11-08",
  "volume": "5",
  "number": "4",
  "pages": "e1012",
  "tags": [
    "Biochemistry",
    "Biochemistry simulations",
    "Biology",
    "Collagenase",
    "Emerging infectious diseases",
    "Enzyme classes",
    "Enzyme kinetics",
    "Enzyme structure",
    "Enzymes",
    "Helminthology",
    "Host-pathogen interaction",
    "Medicine",
    "Microbiology",
    "Parasitology",
    "Proteins",
    "Veterinary science",
    "Zoology"
  ],
  "abstract": "Background\n          \nThe temporal expression and secretion of distinct members of a family of virulence-associated cathepsin L cysteine peptidases (FhCL) correlates with the entry and migration of the helminth pathogen Fasciola hepatica in the host. Thus, infective larvae traversing the gut wall secrete cathepsin L3 (FhCL3), liver migrating juvenile parasites secrete both FhCL1 and FhCL2 while the mature bile duct parasites, which are obligate blood feeders, secrete predominantly FhCL1 but also FhCL2.\n\n          Methodology/Principal Findings\n          \nHere we show that FhCL1, FhCL2 and FhCL3 exhibit differences in their kinetic parameters towards a range of peptide substrates. Uniquely, FhCL2 and FhCL3 readily cleave substrates with Pro in the P2 position and peptide substrates mimicking the repeating Gly-Pro-Xaa motifs that occur within the primary sequence of collagen. FhCL1, FhCL2 and FhCL3 hydrolysed native type I and II collagen at neutral pH but while FhCL1 cleaved only non-collagenous (NC, non-Gly-X-Y) domains FhCL2 and FhCL3 exhibited collagenase activity by cleaving at multiple sites within the α1 and α2 triple helix regions (Col domains). Molecular simulations created for FhCL1, FhCL2 and FhCL3 complexed to various seven-residue peptides supports the idea that Trp67 and Tyr67 in the S2 subsite of the active sites of FhCL3 and FhCL2, respectively, are critical to conferring the unique collagenase-like activity to these enzymes by accommodating either Gly or Pro residues at P2 in the substrate. The data also suggests that FhCL3 accommodates hydroxyproline (Hyp)-Gly at P3-P2 better than FhCL2 explaining the observed greater ability of FhCL3 to digest type I and II collagens compared to FhCL2 and why these enzymes cleave at different positions within the Col domains.\n\n          Conclusions/Significance\n          \nThese studies further our understanding of how this helminth parasite regulates peptidase expression to ensure infection, migration and establishment in host tissues.",
  "fullText": "Introduction Papain-like cysteine peptidases, including cathepsins B and L, are ubiquitously secreted extracorporeally by helminth parasites of human and veterinary importance where they perform many important roles that are critical to the development and survival of the parasite within the mammalian host [1]. These roles include penetration and migration through host tissues [2], catabolism of host proteins to peptides and amino acids [3], [4], and modulation of the host immune response by cleaving immunoglobulin [5], [6] or by altering the activity of immune effector cells [7]. Accordingly, cathepsin peptidases are leading targets for novel anti-parasitic drugs and vaccines that block their function [8], [9]. Fasciola hepatica is the causative agent of liver fluke disease (fasciolosis) of domestic animals in regions with temperate climates. Although traditionally regarded as a disease of livestock, fasciolosis is now recognised as an important emerging foodborne zoonotic disease in rural areas of South America (particularly Bolivia, Peru and Equador), Egypt and Iran [10]. It is estimated that over 2.4 million people are infected with F. hepatica worldwide and around 91 million are at risk of infection [11]. To infect their mammalian hosts, F. hepatica larvae, which are ingested with vegetation contaminated with dormant cysts (metacercariae), penetrate the intestinal wall, enter the liver capsule and migrate through the parenchyma before invading into the bile ducts [12]. To facilitate this tissue migration, Fasciola secrete various members of a multigenic family of cathepsin L peptidases that exhibit overlapping but complementary substrate specificities and together cleave host macromolecules very efficiently [13], [14]. In fact, the ability of Fasciola to infect and adapt to a wide range of host species has been attributed to the effectiveness of this proteolytic machinery [14], [15]. Phylogenetic analyses have shown that the Fasciola cathepsin L gene family expanded by a series of gene duplications followed by divergence which gave rise to three clades expressed by tissue-migrating and adult worms (Clades 1, 2, and 5) and two clades specific to the early infective juvenile stage (Clades 3 and 4) [13], [14]. Consistent with these observations, our proteomics analysis identified representative enzymes from Clades 1, 2 and 5, but not from Clades 3 and 4, in the secretory products of adult F. hepatica [14]. More recently, we showed that the temporal expression and secretion of the specific cathepsin L clades correlated with the migration of the parasite through host issues; members of cathepsin L clade 3 (FhCL3) are secreted by Fasciola infective larvae and effected penetration of the host intestinal wall while clades 1, 2 and 5 (FhCL1, FhCL2 and FhCL5) peptidases are secreted by the immature liver-stage flukes and adult worms and function in preparing a migratory path through the liver and in the acquisition of nutrient by degrading host blood and tissue components. While clade 4 (FhCL4) peptidases are expressed by infective larvae they do not seem to be secreted and, therefore, may play an intracellular house-keeping function [13], [16]. Recent transcriptomic analyses of juvenile and adult stages have confirmed these observations [17], [18]. The"
}