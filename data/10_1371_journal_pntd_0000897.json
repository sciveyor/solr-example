{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000897",
  "doi": "10.1371/journal.pntd.0000897",
  "externalIds": [
    "pii:10-PNTD-RA-1011R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Observed Reductions in Schistosoma mansoni Transmission from Large-Scale Administration of Praziquantel in Uganda: A Mathematical Modelling Study",
  "authors": [
    {
      "name": "Michael D. French",
      "first": "Michael D.",
      "last": "French",
      "affiliation": "Schistosomiasis Control Initiative, Imperial College London, London, United Kingdom; Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Thomas S. Churcher",
      "first": "Thomas S.",
      "last": "Churcher",
      "affiliation": "Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Manoj Gambhir",
      "first": "Manoj",
      "last": "Gambhir",
      "affiliation": "Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Alan Fenwick",
      "first": "Alan",
      "last": "Fenwick",
      "affiliation": "Schistosomiasis Control Initiative, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Joanne P. Webster",
      "first": "Joanne P.",
      "last": "Webster",
      "affiliation": "Schistosomiasis Control Initiative, Imperial College London, London, United Kingdom; Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Narcis B. Kabatereine",
      "first": "Narcis B.",
      "last": "Kabatereine",
      "affiliation": "Vector Control Division, Ministry of Health, Kampala, Uganda"
    },
    {
      "name": "Maria-Gloria Basáñez",
      "first": "Maria-Gloria",
      "last": "Basáñez",
      "affiliation": "Department of Infectious Disease Epidemiology, Imperial College London, London, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-11",
  "dateAccepted": "2010-10-28",
  "dateReceived": "2010-03-29",
  "volume": "4",
  "number": "11",
  "pages": "e897",
  "tags": [
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Mathematics/Mathematical Computing",
    "Public Health and Epidemiology/Epidemiology",
    "Public Health and Epidemiology/Health Policy",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Background\n\nTo date schistosomiasis control programmes based on chemotherapy have largely aimed at controlling morbidity in treated individuals rather than at suppressing transmission. In this study, a mathematical modelling approach was used to estimate reductions in the rate of Schistosoma mansoni reinfection following annual mass drug administration (MDA) with praziquantel in Uganda over four years (2003–2006). In doing this we aim to elucidate the benefits of MDA in reducing community transmission.\n\nMethods\n\nAge-structured models were fitted to a longitudinal cohort followed up across successive rounds of annual treatment for four years (Baseline: 2003, Treatment: 2004–2006; n = 1,764). Instead of modelling contamination, infection and immunity processes separately, these functions were combined in order to estimate a composite force of infection (FOI), i.e., the rate of parasite acquisition by hosts.\n\nResults\n\nMDA achieved substantial and statistically significant reductions in the FOI following one round of treatment in areas of low baseline infection intensity, and following two rounds in areas with high and medium intensities. In all areas, the FOI remained suppressed following a third round of treatment.\n\nConclusions/Significance\n\nThis study represents one of the first attempts to monitor reductions in the FOI within a large-scale MDA schistosomiasis morbidity control programme in sub-Saharan Africa. The results indicate that the Schistosomiasis Control Initiative, as a model for other MDA programmes, is likely exerting a significant ancillary impact on reducing transmission within the community, and may provide health benefits to those who do not receive treatment. The results obtained will have implications for evaluating the cost-effectiveness of schistosomiasis control programmes and the design of monitoring and evaluation approaches in general.",
  "fullText": "Introduction In terms of its socioeconomic impact upon the afflicted populations, schistosomiasis constitutes the world's most important parasitic disease after malaria, infecting 207 million people worldwide, of whom 85% live in Africa [1]. The Schistosomiasis Control Initiative (SCI) was established in 2002 with the aim of helping establish sustainable schistosomiasis control programmes based on large-scale praziquantel (PZQ) administration. Uganda was the first country where SCI implemented control (and hence the area upon which this study focuses), capitalizing on the strength of its national expertise. In Uganda, intestinal schistosomiasis (caused by Schistosoma mansoni) is widespread, with highly endemic foci of infection around the waterbodies of Lake Albert, Lake Victoria, Lake Kyoga and along the Albert Nile (Figure 1). As there is very limited urinary schistosomiasis (due to S. haematobium) in Uganda, we focus on S. mansoni infections here. Morbidity is often caused by eggs rupturing the intestinal wall leading to blood loss and subsequent anaemia, and the immune response to eggs that become trapped in organs and tissues, leading to the development of hepatomegaly, splenomegaly, and eosinophilia [2]. S. mansoni adult worms reproduce sexually in humans, with eggs released with faeces into fresh water, where they can hatch, with free-living miracidia subsequently infecting a suitable freshwater (Biomphalaria) snail intermediate host, within which asexual reproduction occurs. Cercariae are then released by the snail back into water, which complete the life-cycle by infecting humans who come into contact with infested water. The infection in the (human) definitive host can be treated effectively with PZQ, a safe, affordable, and efficacious drug which kills the adult worms and therefore reduces egg counts. Although effective at clearing worm infections (with a cure rate efficacy of 50–80% and an egg reduction rate of 95% [3], [4], reinfection will occur following treatment (unless this is provided regularly for prolonged periods), and so, schistosomiasis control programmes based on chemotherapy have been aimed primarily at controlling morbidity rather than at suppressing transmission. Given this reinfection and the absence of direct multiplication within the human host, the severity of infection (and hence of morbidity) is likely to reflect the cumulative exposure of an individual to infection over a period of years [5], in addition to the operation of individual host immune responses and concomitant immunity via already-established worms. The rationale of control programmes is that by reducing worm burdens in humans, and particularly children (who are the most likely to be heavily infected), the more serious sequelae of infection (organomegaly and fibrosis) are less likely to develop, and are more easily reversed if they do develop [6]. There are many examples of significant success in controlling infection intensity and morbidity in schistosomiasis control programmes using a mass drug administration (MDA) approach [7], [8], [9], [10]. What has not yet been quantified is the benefit of large-scale MDA to the wider community, including to those who are untreated, via reductions in environmental transmission. Such reductions would manifest as a decreased force of infection (FOI), the rate at which new incoming worms establish"
}