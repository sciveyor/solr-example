{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000824",
  "doi": "10.1371/journal.pntd.0000824",
  "externalIds": [
    "pii:09-PNTD-RA-0274R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Application and Validation of PFGE for Serovar Identification of Leptospira Clinical Isolates",
  "authors": [
    {
      "name": "Renee L. Galloway",
      "first": "Renee L.",
      "last": "Galloway",
      "affiliation": "Bacterial Zoonoses Branch, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Paul N. Levett",
      "first": "Paul N.",
      "last": "Levett",
      "affiliation": "Saskatchewan Disease Control Laboratory, Regina, Saskatchewan, Canada"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-09",
  "dateAccepted": "2010-08-18",
  "dateReceived": "2009-07-07",
  "volume": "4",
  "number": "9",
  "pages": "e824",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Serovar identification of clinical isolates of Leptospira is generally not performed on a routine basis, yet the identity of an infecting serovar is valuable from both epidemiologic and public health standpoints. Only a small number of reference laboratories worldwide have the capability to perform the cross agglutinin absorption test (CAAT), the reference method for serovar identification. Pulsed-field gel electrophoresis (PFGE) is an alternative method to CAAT that facilitates rapid identification of leptospires to the serovar level. We employed PFGE to evaluate 175 isolates obtained from humans and animals submitted to the Centers for Disease Control and Prevention (CDC) between 1993 and 2007. PFGE patterns for each isolate were generated using the NotI restriction enzyme and compared to a reference database consisting of more than 200 reference strains. Of the 175 clinical isolates evaluated, 136 (78%) were identified to the serovar level by the database, and an additional 27 isolates (15%) have been identified as probable new serovars. The remaining isolates yet to be identified are either not represented in the database or require further study to determine whether or not they also represent new serovars. PFGE proved to be a useful tool for serovar identification of clinical isolates of known serovars from different geographic regions and a variety of different hosts and for recognizing potential new serovars.",
  "fullText": "Introduction Leptospirosis is a zoonotic infection found all over the world.[1] There is a wide range of animal hosts that maintain Leptospira organisms in their renal tubules and contaminate the environment.[2] Human cases usually occur due to contact with water or other environmental sources that have been contaminated with the urine of infected animals. Human cases can be severe and may cause multi-organ failure in previously healthy individuals.[3], [4] The genus Leptospira is divided into 20 species, of which fourteen contain pathogenic and intermediately-pathogenic strains.[5], [6] Currently there are more than 250 pathogenic serovars organized into 24 serogroups based on antigenic relatedness.[7]–[10] Serovar identification of clinical isolates of Leptospira is important for understanding the epidemiology of leptospirosis. It can lead to the recognition of carrier mammals and enable targeted prevention methods in order to contain outbreaks, and it is important in identifying new species or serovars. However, serovar identification is not routinely performed in laboratories due to the difficulties involved in performing the cross agglutinin absorption test (CAAT), which is considered the reference method for serovar identification. The CAAT method requires the maintenance of large panels of reference antisera and live antigens, is time-consuming, and requires laboratory expertise to perform.[11] PFGE is an alternative method for the identification of Leptospira serovars;[12]–[15] however it has not been validated in the identification of clinical isolates. PFGE is quicker and easier to perform than CAAT, and digital analysis makes standardization and interpretation more accurate. PFGE has the added capability of differentiating between strains of serovars that belong to different species, whereas CAAT is unable to distinguish species differences in serovars such as Grippotyphosa, which appear in more than one species.[12], [16] PFGE is also able to rapidly highlight isolates that may represent new species or serovars, which makes it a very useful tool for taxonomic purposes.[12] In this study, we present the results of serovar identification of clinical isolates obtained from both human and animal sources worldwide and validate the use of PFGE for serovar identification using CAAT. Methods Leptospira isolates from humans and animals were submitted for routine testing to the CDC between the years 2000 and 2007 from eight different countries for serovar identification. Two isolates received in 1993 and 1998 respectively were also included. A total of 175 isolates were analyzed by PFGE; a subset consisting of 36 isolates were also tested by CAAT to validate the PFGE method. Multilocus sequence typing (MLST) was also performed on 42 of the isolates as an additional molecular characterization method. PFGE was performed using the NotI restriction enzyme to generate fingerprint patterns as previously described[12] using Salmonella Braenderup H9812 as a size standard.[17] Fingerprint patterns were analyzed using BioNumerics software (Applied Maths, Inc., Austin, TX). Dendrograms were created by UPGMA cluster analyses based on the Dice band-based coefficient. Band comparison settings of 1.5% optimization and 1% position tolerance were used. Fingerprint patterns of clinical isolates were queried against a library of &gt;200 reference serovars (available to the public upon request) based on mean"
}