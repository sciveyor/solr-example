{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000668",
  "doi": "10.1371/journal.pntd.0000668",
  "externalIds": [
    "pii:09-PNTD-RA-0667R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Feasibility and Effectiveness of Basic Lymphedema Management in Leogane, Haiti, an Area Endemic for Bancroftian Filariasis",
  "authors": [
    {
      "name": "David G. Addiss",
      "first": "David G.",
      "last": "Addiss",
      "affiliation": "Division of Parasitic Diseases, National Center for Infectious Diseases, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America; Fetzer Institute, Kalamazoo, Michigan, United States of America"
    },
    {
      "name": "Jacky Louis-Charles",
      "first": "Jacky",
      "last": "Louis-Charles",
      "affiliation": "Hôpital Ste. Croix, Leogane, Haiti"
    },
    {
      "name": "Jacquelin Roberts",
      "first": "Jacquelin",
      "last": "Roberts",
      "affiliation": "Division of Parasitic Diseases, National Center for Infectious Diseases, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Frederic LeConte",
      "first": "Frederic",
      "last": "LeConte",
      "affiliation": "Hôpital Ste. Croix, Leogane, Haiti"
    },
    {
      "name": "Joyanna M. Wendt",
      "first": "Joyanna M.",
      "last": "Wendt",
      "affiliation": "Division of Parasitic Diseases, National Center for Infectious Diseases, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Marie Denise Milord",
      "first": "Marie Denise",
      "last": "Milord",
      "affiliation": "Hôpital Ste. Croix, Leogane, Haiti"
    },
    {
      "name": "Patrick J. Lammie",
      "first": "Patrick J.",
      "last": "Lammie",
      "affiliation": "Division of Parasitic Diseases, National Center for Infectious Diseases, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Gerusa Dreyer",
      "first": "Gerusa",
      "last": "Dreyer",
      "affiliation": "Non-governmental Organization Amaury Coutinho, Recife, Brazil"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-04",
  "dateAccepted": "2010-03-12",
  "dateReceived": "2009-11-18",
  "volume": "4",
  "number": "4",
  "pages": "e668",
  "tags": [
    "Infectious Diseases",
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Skin Infections",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Public Health and Epidemiology/Health Policy",
    "Public Health and Epidemiology/Preventive Medicine"
  ],
  "abstract": "Background\n\nApproximately 14 million persons living in areas endemic for lymphatic filariasis have lymphedema of the leg. Clinical studies indicate that repeated episodes of bacterial acute dermatolymphangioadenitis (ADLA) lead to progression of lymphedema and that basic lymphedema management, which emphasizes hygiene, skin care, exercise, and leg elevation, can reduce ADLA frequency. However, few studies have prospectively evaluated the effectiveness of basic lymphedema management or assessed the role of compressive bandaging for lymphedema in resource-poor settings.\n\nMethodology/Principal Findings\n\nBetween 1995 and 1998, we prospectively monitored ADLA incidence and leg volume in 175 persons with lymphedema of the leg who enrolled in a lymphedema clinic in Leogane, Haiti, an area endemic for Wuchereria bancrofti. During the first phase of the study, when a major focus of the program was to reduce leg volume using compression bandages, ADLA incidence was 1.56 episodes per person-year. After March 1997, when hygiene and skin care were systematically emphasized and bandaging discouraged, ADLA incidence decreased to 0.48 episodes per person-year (P&lt;0.0001). ADLA incidence was significantly associated with leg volume, stage of lymphedema, illiteracy, and use of compression bandages. Leg volume decreased in 78% of patients; over the entire study period, this reduction was statistically significant only for legs with stage 2 lymphedema (P = 0.01).\n\nConclusions/Significance\n\nBasic lymphedema management, which emphasized hygiene and self-care, was associated with a 69% reduction in ADLA incidence. Use of compression bandages in this setting was associated with an increased risk of ADLA. Basic lymphedema management is feasible and effective in resource-limited areas that are endemic for lymphatic filariasis.",
  "fullText": "Introduction Lymphedema of the leg and its advanced form, known as elephantiasis, are major causes of disability and morbidity in filariasis-endemic areas, with an estimated 14 million cases worldwide [1]. When the World Health Organization's Global Program to Eliminate Lymphatic Filariasis (GPELF) was launched in 1998, its stated goals included not only interrupting transmission of the parasite, but also providing care to persons who suffer from clinical disease [2], [3]. During the 1990s, several studies in filariasis-endemic areas highlighted the importance of repeated episodes of acute bacterial dermatolymphangioadenitis (ADLA) in the progression of lymphedema severity [4]–[8]. These inflammatory episodes, characterized by intense pain, swelling, fever, and chills, accelerate damage to the peripheral lymphatic channels in the skin, which leads to worsened lymphatic dysfunction, fibrosis, and increased risk of further ADLA episodes. Clinical studies suggest that basic lymphedema management – including hygiene, skin care, elevation of the limb, and range-of-motion exercises – can halt, or perhaps even partially reverse, this progression [9]–[14]. The current prospective study was done to test, under field conditions, the feasibility and effectiveness of basic lymphedema management as a public health intervention in a resource-poor setting. Leogane, Haiti, located approximately 30 km west of Port au Prince, has long been endemic for lymphatic filariasis; the prevalence of Wuchereria bancrofti microfilaremia was 16% in 2000 [15]. The outpatient clinic at Ste. Croix Hospital, the major health facility for Leogane Commune, was the site of this study. Lymphedema of the leg, which disproportionately affects women [16], is a major public health problem in Leogane [17]. As in many other areas where bancroftian filariasis is endemic, few persons with lymphedema of the leg remain infected with the parasite [18]. Methods The study was approved by the Ethics Committee of Ste. Croix Hospital and by the Institutional Review Board of the Centers for Disease Control and Prevention. Patients were included in the study if they had lymphedema of the leg, agreed to return to the hospital clinic for follow-up evaluations, had no obvious cause of edema such as tumor or congenital anomalies, and provided written informed consent. On their initial visit to the clinic, the volume of each leg was measured using a water displacement method, and each leg was classified as to stage of lymphedema using the 7-stage system of Dreyer and colleagues [19]. In this system, stage 1 is characterized by minimal swelling that reverses with horizontal rest. Stage 2 lymphedema does not completely reverse, but the skin appears normal. In stage 3 lymphedema the skin is thickened so that shallow skin folds are apparent. For purposes of analysis, we combined the Dreyer stages 4–7 into a single fourth-stage category; briefly, these are characterized by protrusions or “knobs” on the skin (stage 4), deep skin folds (stage 5), mossy lesions (stage 6) and severity that inhibits performing daily activities (stage 7) [19]. Lymphedema was considered bilateral if both legs had lymphedema of stage 2 or greater. Information was collected on patient age, gender, literacy, education, duration of lymphedema, and"
}