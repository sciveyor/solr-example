{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001636",
  "doi": "10.1371/journal.pntd.0001636",
  "externalIds": [
    "pii:PNTD-D-11-01097"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Development of a Humanized Antibody with High Therapeutic Potential against Dengue Virus Type 2",
  "authors": [
    {
      "name": "Pi-Chun Li",
      "first": "Pi-Chun",
      "last": "Li",
      "affiliation": "Graduate Institute of Life Sciences, National Defense Medical Center, Taipei, Taiwan; Institute of Cellular and Organismic Biology, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "Mei-Ying Liao",
      "first": "Mei-Ying",
      "last": "Liao",
      "affiliation": "Institute of Cellular and Organismic Biology, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "Ping-Chang Cheng",
      "first": "Ping-Chang",
      "last": "Cheng",
      "affiliation": "Institute of Cellular and Organismic Biology, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "Jian-Jong Liang",
      "first": "Jian-Jong",
      "last": "Liang",
      "affiliation": "Institute of Biomedical Sciences, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "I-Ju Liu",
      "first": "I-Ju",
      "last": "Liu",
      "affiliation": "Institute of Cellular and Organismic Biology, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "Chien-Yu Chiu",
      "first": "Chien-Yu",
      "last": "Chiu",
      "affiliation": "Institute of Cellular and Organismic Biology, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "Yi-Ling Lin",
      "first": "Yi-Ling",
      "last": "Lin",
      "affiliation": "Institute of Biomedical Sciences, Academia Sinica, Taipei, Taiwan"
    },
    {
      "name": "Gwong-Jen J. Chang",
      "first": "Gwong-Jen J.",
      "last": "Chang",
      "affiliation": "Arbovirus Diseases Branch, Division of Vector-Borne Infectious Diseases, Centers for Disease Control and Prevention, Public Health Service, United States Department of Health and Human Services, Fort Collins, Colorado, United States of America"
    },
    {
      "name": "Han-Chung Wu",
      "first": "Han-Chung",
      "last": "Wu",
      "affiliation": "Graduate Institute of Life Sciences, National Defense Medical Center, Taipei, Taiwan; Institute of Cellular and Organismic Biology, Academia Sinica, Taipei, Taiwan"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-05",
  "dateAccepted": "2012-03-20",
  "dateReceived": "2011-10-31",
  "volume": "6",
  "number": "5",
  "pages": "e1636",
  "tags": [
    "Biology",
    "Medicine"
  ],
  "abstract": "Background\n          \nDengue virus (DENV) is a significant public health threat in tropical and subtropical regions of the world. A therapeutic antibody against the viral envelope (E) protein represents a promising immunotherapy for disease control.\n\n          Methodology/Principal Findings\n          \nWe generated seventeen novel mouse monoclonal antibodies (mAbs) with high reactivity against E protein of dengue virus type 2 (DENV-2). The mAbs were further dissected using recombinant E protein domain I-II (E-DI-II) and III (E-DIII) of DENV-2. Using plaque reduction neutralization test (PRNT) and mouse protection assay with lethal doses of DENV-2, we identified four serotype-specific mAbs that had high neutralizing activity against DENV-2 infection. Of the four, E-DIII targeting mAb DB32-6 was the strongest neutralizing mAb against diverse DENV-2 strains. Using phage display and virus-like particles (VLPs) we found that residue K310 in the E-DIII A-strand was key to mAb DB32-6 binding E-DIII. We successfully converted DB32-6 to a humanized version that retained potency for the neutralization of DENV-2 and did not enhance the viral infection. The DB32-6 showed therapeutic efficacy against mortality induced by different strains of DENV-2 in two mouse models even in post-exposure trials.\n\n          Conclusions/Significance\n          \nWe used novel epitope mapping strategies, by combining phage display with VLPs, to identify the important A-strand epitopes with strong neutralizing activity. This study introduced potential therapeutic antibodies that might be capable of providing broad protection against diverse DENV-2 infections without enhancing activity in humans.",
  "fullText": "Introduction Dengue is the most important arthropod-borne viral disease in humans and an increasing public health concern in tropical and subtropical regions of the world. Approximately 50–100 million cases of dengue fever (DF) and 500,000 cases of dengue hemorrhagic fever (DHF) occur every year, and 2.5 billion people are at risk of dengue infection globally [1], [2]. Dengue infection may lead to fever, headache and joint pain in milder cases but may also lead to the more severe life-threatening DHF/dengue shock syndrome (DSS) has plasma leakage, thrombocytopenia, and hemorrhagic manifestations, possibly leading to shock [3], [4]. Dengue virus (DENV) is positive-sense single-stranded RNA virus of approximately 11 kb genome of the genus Flavivirus, a family Flaviviridae. It has four genetically and antigenically related viral serotypes: DENV-1, -2, -3 and -4. Flaviviruses encode a single polyprotein processed by host and viral protease to produce three structural proteins, including capsid (C) protein, precursor membrane/membrane (prM/M) and envelope (E) protein, and seven nonstructural proteins: NS1, NS2A, NS2B, NS3, NS4A, NS4B and NS5 [5]. The E protein, a 53 kDa glycoprotein important for attachment, entry, and viral envelope fusion, can bind to cellular receptors and induce neutralizing antibodies [6], [7]. The DENV consists of an icosahedral ectodomain, containing 180 copies of the E protein [8]. E protein monomer contains three structural and functional domains [9], [10]. E protein domain I (E-DI) is a central β-barrel structure. E protein domain II (E-DII) is organized into two long finger-like structures and contains the flaviviruses conserved fusion loop. E protein domain III (E-DIII) has an immunoglobulin-like fold and may mediate interactions between the virus and the receptors on the host cell [11]. Studies of the biological characteristics and epitope specificities of mouse monoclonal antibodies (mAbs) have elucidated the antigenic structure of flavivirus E proteins [12]–[15]. Serotype-specific mAbs with neutralizing activity against DENV-2 have been found to be located on the lateral ridge of E-DIII and the subcomplex-specific mAbs recognized A-strand of E-DIII [14], [16], [17]. Antibody-mediated neutralization has been found to alter the arrangement of viral surface glycoproteins that prevent cells from viral attachment [16]. Binding of an antibody to the viral surface can interfere with virus internalization or membrane fusion [6]. Primary DENV infection is believed to provide lifelong immunity against re-infection with the same serotype [18], [19]. However, humoral immune responses to DENV infection are complex [20]–[22], and may exacerbate the disease during heterologous virus infection [18], [19]. Antibody-dependent enhancement (ADE) in dengue pathogenesis results from the increase in the efficiency of virus infection in the presence of non-neutralizing or sub-neutralizing concentrations of anti-E or anti-prM immunoglobulins [21], [23]. The attachment of antibody-virus complex to such Fcγ receptor-bearing cells as monocytes and macrophages can lead to an increased virus replication [18], [24], [25]. A better understanding of the neutralizing epitopes may facilitate the generation of new antibody-based therapeutics against DENV infection. In this study, we generated several mAbs against DENV-2. We found that serotype-specific anti-E-DIII mAbs played an important role in the neutralization of virus"
}