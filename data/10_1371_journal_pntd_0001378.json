{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001378",
  "doi": "10.1371/journal.pntd.0001378",
  "externalIds": [
    "pii:PNTD-D-11-00395"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Population Dynamics of Aedes aegypti and Dengue as Influenced by Weather and Human Behavior in San Juan, Puerto Rico",
  "authors": [
    {
      "name": "Roberto Barrera",
      "first": "Roberto",
      "last": "Barrera",
      "affiliation": "Entomology and Ecology Activity, Dengue Branch, Centers for Disease Control and Prevention, Calle Cañada, San Juan, Puerto Rico"
    },
    {
      "name": "Manuel Amador",
      "first": "Manuel",
      "last": "Amador",
      "affiliation": "Entomology and Ecology Activity, Dengue Branch, Centers for Disease Control and Prevention, Calle Cañada, San Juan, Puerto Rico"
    },
    {
      "name": "Andrew J. MacKay",
      "first": "Andrew J.",
      "last": "MacKay",
      "affiliation": "Entomology and Ecology Activity, Dengue Branch, Centers for Disease Control and Prevention, Calle Cañada, San Juan, Puerto Rico"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-12",
  "dateAccepted": "2011-09-13",
  "dateReceived": "2011-05-02",
  "volume": "5",
  "number": "12",
  "pages": "e1378",
  "tags": [
    "Biology",
    "Ecology",
    "Population ecology"
  ],
  "abstract": "Previous studies on the influence of weather on Aedes aegypti dynamics in Puerto Rico suggested that rainfall was a significant driver of immature mosquito populations and dengue incidence, but mostly in the drier areas of the island. We conducted a longitudinal study of Ae. aegypti in two neighborhoods of the metropolitan area of San Juan city, Puerto Rico where rainfall is more uniformly distributed throughout the year. We assessed the impacts of rainfall, temperature, and human activities on the temporal dynamics of adult Ae. aegypti and oviposition. Changes in adult mosquitoes were monitored with BG-Sentinel traps and oviposition activity with CDC enhanced ovitraps. Pupal surveys were conducted during the drier and wetter parts of the year in both neighborhoods to determine the contribution of humans and rains to mosquito production. Mosquito dynamics in each neighborhood was compared with dengue incidence in their respective municipalities during the study. Our results showed that: 1. Most pupae were produced in containers managed by people, which explains the prevalence of adult mosquitoes at times when rainfall was scant; 2. Water meters were documented for the first time as productive habitats for Ae. aegypti; 3. Even though Puerto Rico has a reliable supply of tap water and an active tire recycling program, water storage containers and discarded tires were important mosquito producers; 4. Peaks in mosquito density preceded maximum dengue incidence; and 5. Ae. aegypti dynamics were driven by weather and human activity and oviposition was significantly correlated with dengue incidence.",
  "fullText": "Introduction There are three main patterns of dengue virus transmission that merit a better understanding of the involvement of Aedes aegypti: 1- Major dengue epidemics occur every few years [1], [2]; 2- Dengue viruses have become endemic or hyperendemic (co-circulation of two or more serotypes) in many countries [3], [4], and 3- There is intra-annual, seasonal dengue transmission with peak incidence during the second half of the year in the northern hemisphere and during the first half of the year in the southern hemisphere, each one associated with elevated temperature and precipitation [5]–[7]. There is a lack of long-term, longitudinal studies on the temporal dynamics of Ae. aegypti that would, otherwise, allow us to understand whether mosquito outbreaks are responsible for inter-annual dengue epidemics. It is common to observe relatively large densities of Ae. aegypti that do not result in major outbreaks, particularly after major epidemics [8], suggesting that other factors such as temporal changes in population immunity or the introduction of new serotypes can significantly influence inter-annual epidemic patterns [9]–[11]. Climate variability, particularly El Niño Southern Oscillation (ENSO) teleconnections with local weather, has been associated with inter-annual dengue epidemics [2], although it would seem that the relationship is complex, non-linear, and perhaps non-stationary [1], [12], [13]. Amarakoon et al. [6] found significant effects of temperature on dengue epidemics in the Caribbean, particularly one year after the onset of an ENSO event. Because dengue viruses are transmitted by the bite of infected mosquitoes, dengue virus endemicity or hyperendemicity requires the existence of sufficient vectors to produce uninterrupted transmission in spite of adverse, seasonal weather conditions (e.g., lack of rain), such as that observed in urban areas with long dry seasons [14]. Recurrent virus introductions facilitate dengue endemicity. In dengue endemic/hyperendemic countries, dengue viruses are disseminated among regions so that virus re-introductions are frequent and do not depend solely on virus import [4], [15]. There is evidence showing that even in relatively small countries such as Puerto Rico, some dengue genotypes can circulate uninterruptedly for prolonged periods of time [16]. Vertical transmission of dengue virus in Ae. aegypti could play a role in the maintenance of endemicity but more evidence is required to understand its role in nature [17]. Perhaps, the single, most important factor determining dengue endemicity is the habit of people of adding water to containers, which can be for drinking, cooking or bathing (water-storage) and for other purposes, such as ornamentation (fountains), watering plants, etc. Production of Ae. aegypti in those containers can be so important as to trigger dengue outbreaks during the dry season [18]. Additionally, the existence of cryptic containers with water producing large numbers of Ae. aegypti has been more frequently reported [19]–[22], and in at least one occasion, those recondite containers have been linked to local dengue virus transmission [23]. Some cryptic containers, such as septic tanks in Puerto Rico, can produce Ae. aegypti throughout the year [24]. There is evidence showing that the intra-annual cycle of dengue transmission is driven by weather and"
}