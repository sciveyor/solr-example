{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001382",
  "doi": "10.1371/journal.pntd.0001382",
  "externalIds": [
    "pii:PNTD-D-11-00335"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Toxocariasis in Cuba: A Literature Review",
  "authors": [
    {
      "name": "Idalia Sariego",
      "first": "Idalia",
      "last": "Sariego",
      "affiliation": "Institute of Tropical Medicine “Pedro Kourí”, Havana, Cuba"
    },
    {
      "name": "Kirezi Kanobana",
      "first": "Kirezi",
      "last": "Kanobana",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Lázara Rojas",
      "first": "Lázara",
      "last": "Rojas",
      "affiliation": "Institute of Tropical Medicine “Pedro Kourí”, Havana, Cuba"
    },
    {
      "name": "Niko Speybroeck",
      "first": "Niko",
      "last": "Speybroeck",
      "affiliation": "Institute of Health and Society, Université Catholique de Louvain, Brussels, Belgium"
    },
    {
      "name": "Katja Polman",
      "first": "Katja",
      "last": "Polman",
      "affiliation": "Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Fidel A. Núñez",
      "first": "Fidel A.",
      "last": "Núñez",
      "affiliation": "Institute of Tropical Medicine “Pedro Kourí”, Havana, Cuba"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-02",
  "volume": "6",
  "number": "2",
  "pages": "e1382",
  "tags": [
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Neglected tropical diseases",
    "Parasitic diseases"
  ],
  "abstract": "Human toxocariasis (HT) is a zoonotic disease caused by infection with the larval stage of Toxocara canis, the intestinal roundworm of dogs. Infection can be associated with a wide clinical spectrum varying from asymptomatic to severe organ injury. While the incidence of symptomatic human toxocariasis appears to be low, infection of the human population is widespread. In Cuba, a clear overview on the status of the disease is lacking. Here, we review the available information on toxocariasis in Cuba as a first step to estimate the importance of the disease in the country. Findings are discussed and put in a broader perspective. Data gaps are identified and suggestions on how to address these are presented. The available country data suggest that Toxocara infection of the definitive dog host and environmental contamination with Toxocara spp. eggs is substantial, but information on HT is less conclusive. The availability of adequate diagnostic tools in the country should be guaranteed. Dedicated studies are needed for a reliable assessment of the impact of toxocariasis in Cuba and the design of prevention or control strategies.",
  "fullText": "Introduction Human toxocariasis (HT) is one of the most common human parasitic infections in the world, affecting mainly the poorest communities of developing countries. It is caused by zoonotic infection with the larval stage of Toxocara canis, the intestinal roundworms of dogs, and probably by the roundworms of cats (Toxocara cati) as well [1]. Although the disease can be significant and debilitating, the incidence of severe clinical manifestations is unknown, and diagnosis is difficult. This leads to a false perception that the burden and public health impact are low and consequently results in the classification of HT as a neglected zoonosis. HT is acquired by the ingestion of eggs, which originate from the feces of the definite dog host and embryonate in the environment [2] (Figure S1). Children in their first decade of life are prone to infection because of their geophagic behavior and mouthing of objects, which is linked to a higher risk of exposure at playgrounds or sandboxes contaminated with dog feces [3]. HT is mostly asymptomatic but can be associated with severe clinical syndromes due to organ injury by migrating larvae [4]. Depending on the organs affected and the specificity of the symptoms, the predominant clinical syndromes are classified as visceral larva migrans (VLM), ocular larva migrans (OLM), and common, neurologic, and covert toxocariasis [5]. Diagnosis of HT is traditionally based on a combination of clinical and histopathological interpretations. Yet, sensitivity is low, as biopsy material may not always contain the larvae. Serology, using in vitro–obtained excretory-secretory products of the larvae (TES), is the best laboratory-based option for diagnosis [6], and is considered a useful predictor of T. canis infection when coupled to relevant clinical data. Worldwide, reported Toxocara seroprevalence data among apparently healthy individuals range (using either ELISA or western blot) from 2.4% in Denmark [7] to 92.8% in La Réunion [8]. In Cuba, there are an estimated 2 million dogs that have access to veterinary control. However, only 40% of these participate in the rabies vaccination program, illustrating the low compliance to recommended veterinary prevention [9]. Consequently, a large proportion of dogs are born with congenital toxocariasis. Combined with the high numbers of stray dogs that are not routinely dewormed, this points to a rich potential for environmental contamination and subsequent human exposure. However, a clear idea of the status and importance of toxocariasis within the country is lacking. To this end, we recently started a project dedicated to the epidemiology and the diagnosis of toxocariasis in Cuba. As a first step, we reviewed the available information on Toxocara in Cuba and put it in a broader perspective, identifying data gaps that should be addressed. Methods PubMed, Google Scholar, ISI Web of Knowledge, and CUMED databases were searched using combinations of keywords “toxocara”, “toxocariasis”, “larva migrans”, and “Cuba”. The search was conducted in February 2010 and was limited neither by language, study design, nor date of publication. The CUMED database contains all Cuban scientific publications in human medicine and related fields. Non-peer-reviewed articles and theses,"
}