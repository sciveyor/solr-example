{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000086",
  "doi": "10.1371/journal.pntd.0000086",
  "externalIds": [
    "pii:07-PNTD-RA-0088R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Host Gene Expression Profiling of Dengue Virus Infection in Cell Lines and Patients",
  "authors": [
    {
      "name": "Joshua Fink",
      "first": "Joshua",
      "last": "Fink",
      "affiliation": "Novartis Institute for Tropical Diseases, Chromos, Singapore"
    },
    {
      "name": "Feng Gu",
      "first": "Feng",
      "last": "Gu",
      "affiliation": "Novartis Institute for Tropical Diseases, Chromos, Singapore"
    },
    {
      "name": "Ling Ling",
      "first": "Ling",
      "last": "Ling",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Thomas Tolfvenstam",
      "first": "Thomas",
      "last": "Tolfvenstam",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Farzad Olfat",
      "first": "Farzad",
      "last": "Olfat",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Keh Chuang Chin",
      "first": "Keh Chuang",
      "last": "Chin",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Pauline Aw",
      "first": "Pauline",
      "last": "Aw",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Joshy George",
      "first": "Joshy",
      "last": "George",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Vladimir A. Kuznetsov",
      "first": "Vladimir A.",
      "last": "Kuznetsov",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    },
    {
      "name": "Mark Schreiber",
      "first": "Mark",
      "last": "Schreiber",
      "affiliation": "Novartis Institute for Tropical Diseases, Chromos, Singapore"
    },
    {
      "name": "Subhash G. Vasudevan",
      "first": "Subhash G.",
      "last": "Vasudevan",
      "affiliation": "Novartis Institute for Tropical Diseases, Chromos, Singapore"
    },
    {
      "name": "Martin L. Hibberd",
      "first": "Martin L.",
      "last": "Hibberd",
      "affiliation": "Genome Institute of Singapore, Genome, Singapore"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2007-11",
  "dateAccepted": "2007-08-13",
  "dateReceived": "2007-05-03",
  "volume": "1",
  "number": "2",
  "pages": "e86",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Background\n    \nDespite the seriousness of dengue-related disease, with an estimated 50–100 million cases of dengue fever and 250,000–500,000 cases of dengue hemorrhagic fever/dengue shock syndrome each year, a clear understanding of dengue pathogenesis remains elusive. Because of the lack of a disease model in animals and the complex immune interaction in dengue infection, the study of host response and immunopathogenesis is difficult. The development of genomics technology, microarray and high throughput quantitative PCR have allowed researchers to study gene expression changes on a much broader scale. We therefore used this approach to investigate the host response in dengue virus-infected cell lines and in patients developing dengue fever.\n\n    Methodology/Principal Findings\n    \nUsing microarray and high throughput quantitative PCR method to monitor the host response to dengue viral replication in cell line infection models and in dengue patient blood samples, we identified differentially expressed genes along three major pathways; NF-κB initiated immune responses, type I interferon (IFN) and the ubiquitin proteasome pathway. Among the most highly upregulated genes were the chemokines IP-10 and I-TAC, both ligands of the CXCR3 receptor. Increased expression of IP-10 and I-TAC in the peripheral blood of ten patients at the early onset of fever was confirmed by ELISA. A highly upregulated gene in the IFN pathway, viperin, was overexpressed in A549 cells resulting in a significant reduction in viral replication. The upregulation of genes in the ubiquitin-proteasome pathway prompted the testing of proteasome inhibitors MG-132 and ALLN, both of which reduced viral replication.\n\n    Conclusion/Significance\n    \nUnbiased gene expression analysis has identified new host genes associated with dengue infection, which we have validated in functional studies. We showed that some parts of the host response can be used as potential biomarkers for the disease while others can be used to control dengue viral replication, thus representing viable targets for drug therapy.",
  "fullText": "Introduction Although dengue-related disease results in an estimated 50–100 million cases of dengue fever and 250,000 to 500,000 cases of dengue hemorrhagic fever/dengue shock syndrome each year [1],[2], a clear understanding of dengue pathogenesis remains elusive. Dengue virus is an enveloped, positive-stranded RNA virus of the Flaviviridae family transmitted by the mosquito Aedes aegypti and Aedes albopictus. Four serotypes of dengue virus (DENV1-4) circulate in endemic areas. Although infection with one serotype of dengue virus confers life-long protective immunity to that serotype, it does not protect the host from infection with other serotypes [3]. The initial target cells during dengue infection are believed to be Langerhans cells [4]. Through means not yet fully understood, Langerhans cells spread the virus, via the lymphatic system, to other tissues such as liver, spleen, kidney and blood, whereas monocytes, macrophages and endothelial cells are the major cell types in which the virus replicates [5]. In the majority of symptomatic dengue infections, a fever of 5–7 days duration develops together with bone and joint pain, retro-orbital pain, nausea and fatigue, this is called dengue fever (DF). While the majority of DF patients recover without intervention, 2–5% develop a more severe form of the disease, called dengue hemorrhagic fever/dengue shock syndrome (DHF/DSS), characterized by thrombocytopenia and vascular leakage, causing hypervolemic shock and death if not promptly treated [6]. The cause of DHF/DSS is not clear. Antibody dependent enhancement (ADE) is the most widely supported theory explaining the higher risk of DHF/DSS associated with a heterologous secondary infection [7]. The phenomenon of T cell “original antigenic sin” has also been described [8]. Like many viruses, dengue inhibits IFNα and IFNβ signaling by suppressing Jak-Stat activation, resulting in reduced host antiviral response [9]. The combination of a reduced host defense, increased uptake of the virus and delayed viral clearance likely synergizes to produce higher viremia resulting in a more severe outcome. Because of the lack of a disease model in animals and the complex immune interaction in dengue infection, the study of host response and immunopathogenesis is difficult. The development of genomics technology has allowed researchers to study gene expression changes on a much larger scale. A summary of published microarray data from infection of different host cell types with bacteria, viruses, yeast, protozoa and helminthes revealed a common host-transcriptional-response consisting of a cluster of IFN-stimulated and immune mediating genes [10]. One particularly successful application of microarray technology was the identification of a novel drug target, c-kit, in endothelial cells infected with Kaposi's sarcoma-associated herpesvirus (KSHV) [11]. With regard to dengue infection, gene expression studies have been carried out in infected human umbilical vein endothelial cells (HUVECs) by differential display reverse transcription (DD-RTPCR) and Affymetrix oligonucleotide microarrays [12]. Genes having a role in the IFN antiviral response and immune defense such as 2′–5′ oligoadenylate synthetase (OAS), myxovirus protein A (MxA), TNFα, galectin-9, phospholipid scramblase 1 and human inhibitor of apoptosis-1 (IAP1) were shown to be upregulated upon infection [12]. Infection with dengue of a more transformed HUVEC-like cell"
}