{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000660",
  "doi": "10.1371/journal.pntd.0000660",
  "externalIds": [
    "pii:09-PNTD-RA-0388R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Macrofilaricidal Activity after Doxycycline Only Treatment of Onchocerca volvulus in an Area of Loa loa Co-Endemicity: A Randomized Controlled Trial",
  "authors": [
    {
      "name": "Joseph D. Turner",
      "first": "Joseph D.",
      "last": "Turner",
      "affiliation": "Filariasis Research Laboratory, Molecular and Biochemical Parasitology, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Nicholas Tendongfor",
      "first": "Nicholas",
      "last": "Tendongfor",
      "affiliation": "Department of Life Sciences, Faculty of Science, University of Buea, Buea, Cameroon; Research Foundation in Tropical Diseases and Environment (REFOTDE), Buea, Cameroon"
    },
    {
      "name": "Mathias Esum",
      "first": "Mathias",
      "last": "Esum",
      "affiliation": "Department of Life Sciences, Faculty of Science, University of Buea, Buea, Cameroon; Research Foundation in Tropical Diseases and Environment (REFOTDE), Buea, Cameroon"
    },
    {
      "name": "Kelly L. Johnston",
      "first": "Kelly L.",
      "last": "Johnston",
      "affiliation": "Filariasis Research Laboratory, Molecular and Biochemical Parasitology, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "R. Stuart Langley",
      "first": "R. Stuart",
      "last": "Langley",
      "affiliation": "Filariasis Research Laboratory, Molecular and Biochemical Parasitology, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Louise Ford",
      "first": "Louise",
      "last": "Ford",
      "affiliation": "Filariasis Research Laboratory, Molecular and Biochemical Parasitology, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Brian Faragher",
      "first": "Brian",
      "last": "Faragher",
      "affiliation": "Filariasis Research Laboratory, Molecular and Biochemical Parasitology, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Sabine Specht",
      "first": "Sabine",
      "last": "Specht",
      "affiliation": "Institute of Medical Microbiology, Immunology and Parasitology (IMMIP), University Hospital Bonn, Bonn, Germany"
    },
    {
      "name": "Sabine Mand",
      "first": "Sabine",
      "last": "Mand",
      "affiliation": "Institute of Medical Microbiology, Immunology and Parasitology (IMMIP), University Hospital Bonn, Bonn, Germany"
    },
    {
      "name": "Achim Hoerauf",
      "first": "Achim",
      "last": "Hoerauf",
      "affiliation": "Institute of Medical Microbiology, Immunology and Parasitology (IMMIP), University Hospital Bonn, Bonn, Germany"
    },
    {
      "name": "Peter Enyong",
      "first": "Peter",
      "last": "Enyong",
      "affiliation": "Tropical Medicine Research Station, Kumba, Cameroon"
    },
    {
      "name": "Samuel Wanji",
      "first": "Samuel",
      "last": "Wanji",
      "affiliation": "Department of Life Sciences, Faculty of Science, University of Buea, Buea, Cameroon; Research Foundation in Tropical Diseases and Environment (REFOTDE), Buea, Cameroon"
    },
    {
      "name": "Mark J. Taylor",
      "first": "Mark J.",
      "last": "Taylor",
      "affiliation": "Filariasis Research Laboratory, Molecular and Biochemical Parasitology, Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-04",
  "dateAccepted": "2010-03-04",
  "dateReceived": "2009-07-31",
  "volume": "4",
  "number": "4",
  "pages": "e660",
  "tags": [
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Microbiology/Parasitology"
  ],
  "abstract": "Background\n\nThe risk of severe adverse events following treatment of onchocerciasis with ivermectin in areas co-endemic with loiasis currently compromises the development of control programmes and the treatment of co-infected individuals. We therefore assessed whether doxycycline treatment could be used without subsequent ivermectin administration to effectively deliver sustained effects on Onchocerca volvulus microfilaridermia and adult viability. Furthermore we assessed the safety of doxycycline treatment prior to ivermectin administration in a subset of onchocerciasis individuals co-infected with low to moderate intensities of Loa loa microfilaraemia.\n\nMethods\n\nA double-blind, randomized, field trial was conducted of 6 weeks of doxycycline (200 mg/day) alone, doxycycline in combination with ivermectin (150 µg/kg) at +4 months or placebo matching doxycycline + ivermectin at +4 months in 150 individuals infected with Onchocerca volvulus. A further 22 individuals infected with O. volvulus and low to moderate intensities of Loa loa infection were administered with a course of 6 weeks doxycycline with ivermectin at +4 months. Treatment efficacy was determined at 4, 12 and 21 months after the start of doxycycline treatment together with the frequency and severity of adverse events.\n\nResults\n\nOne hundred and four (60.5%) participants completed all treatment allocations and follow up assessments over the 21-month trial period. At 12 months, doxycycline/ivermectin treated individuals had lower levels of microfilaridermia and higher frequency of amicrofilaridermia compared with ivermectin or doxycycline only groups. At 21 months, microfilaridermia in doxycycline/ivermectin and doxycycline only groups was significantly reduced compared to the ivermectin only group. 89% of the doxycycline/ivermectin group and 67% of the doxycycline only group were amicrofilaridermic, compared with 21% in the ivermectin only group. O. volvulus from doxycycline groups were depleted of Wolbachia and all embryonic stages in utero. Notably, the viability of female adult worms was significantly reduced in doxycycline treated groups and the macrofilaricidal and sterilising activity was unaffected by the addition of ivermectin. Treatment with doxycycline was well tolerated and the incidence of adverse event to doxycycline or ivermectin did not significantly deviate between treatment groups.\n\nConclusions\n\nA six-week course of doxycycline delivers macrofilaricidal and sterilizing activities, which is not dependent upon co-administration of ivermectin. Doxycycline is well tolerated in patients co-infected with moderate intensities of L. loa microfilariae. Therefore, further trials are warranted to assess the safety and efficacy of doxycycline-based interventions to treat onchocerciasis in individuals at risk of serious adverse reactions to standard treatments due to the co-occurrence of high intensities of L. loa parasitaemias. The development of an anti-wolbachial treatment regime compatible with MDA control programmes could offer an alternative to the control of onchocerciasis in areas of co-endemicity with loiasis and at risk of severe adverse reactions to ivermectin.\n\nTrial Registration\n\nControlled-Trials.com ISRCTN48118452",
  "fullText": "Introduction Onchocerciasis (also known as River Blindness) is a chronic disease induced by the filarial nematode Onchocerca volvulus. An estimated 37 million individuals are infected worldwide with 90 million at risk of infection, mainly in Sub-Saharan Africa. Adult worm infections establish within subcutaneous nodules (onchocercomas) and produce microfilariae (mf), which parasitize skin and eye tissues. Mf are the transmissive stage for black fly vectors and are also responsible for the major disease pathologies of onchocerciasis, including intense troublesome itching, dermatitis, atrophy, visual impairment and blindness. Currently, the only drug available to treat onchocerciasis is ivermectin (MectizanTM, Merck). Ivermectin is generally a safe and effective microfilaricide and has been used successfully in community-directed treatment programs aimed at both reducing the burden of disease and controlling transmission since 1987 [1],[2]. Ivermectin has some macrofilaricidal activity against female adult worms after 6 years of exposure [3], or when given repeatedly at three-monthly intervals [4], [5]. Higher doses of ivermectin do not improve on this activity and such regimens are contraindicated due to the occurrence of visual problems [6]. Another anti-filarial drug, diethylcarbamazine (DEC), is also contraindicated due to the incidence of treatment-associated blindness and the frequent development of potentially life threatening adverse reactions, known as Mazzotti Reactions [7], [8]. There are three major limitations of a sole reliance on ivermectin for onchocerciasis control. Firstly, its use in areas co-endemic with Loa loa, a tissue dwelling filariae that gives rise to blood circulating mf and found principally in forested regions in Africa. Reports of severe adverse reactions (SAE), including encephalopathy, coma and death, in the Central Africa region following mass distribution of ivermectin have introduced serious concerns and disruptions to onchocerciasis control programs [8]. Although the mechanism of ivermectin-associated SAE has not been fully elucidated, L. loa mf have been detected in the cerebral spinal fluid of patients suffering severe adverse reactions, indicating that mf can cross the blood brain barrier. The intensity of L. loa mf in the blood has been determined to be a major risk factor in the development of SAE [8]. Secondly, because ivermectin principally targets the mf stage, continuous delivery of annual treatment is required for at least 15–17 years to interrupt transmission as demonstrated in some endemic areas of Africa [9]. In other endemic areas of Africa this strategy is unlikely to lead to the interruption of transmission due in part to civil strife and conflict, insufficient health infrastructure and political commitment to funding for sustained control programmes, which together compromise the eradicability of onchocerciasis in Africa [10]. The third limitation is that such a long term, community-based strategy based on a single drug intervention is potentially vulnerable to the development of drug resistance. Recent reports from Ghana show evidence of sub-optimal efficacy of ivermectin in communities receiving 6–18 rounds of treatment [11], [12], [13]. Parasites from these communities show genetic changes associated with resistance to ivermectin in other nematodes and increase the concern of resistance to ivermectin developing in onchocerciasis [14], [15]. Considering the absence of any"
}