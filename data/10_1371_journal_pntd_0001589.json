{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001589",
  "doi": "10.1371/journal.pntd.0001589",
  "externalIds": [
    "pii:PNTD-D-11-01296"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Investigation of the Proteolytic Functions of an Expanded Cercarial Elastase Gene Family in Schistosoma mansoni",
  "authors": [
    {
      "name": "Jessica R. Ingram",
      "first": "Jessica R.",
      "last": "Ingram",
      "affiliation": "Tetrad Graduate Program, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Salma B. Rafi",
      "first": "Salma B.",
      "last": "Rafi",
      "affiliation": "Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "A. Alegra Eroy-Reveles",
      "first": "A. Alegra",
      "last": "Eroy-Reveles",
      "affiliation": "Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Manisha Ray",
      "first": "Manisha",
      "last": "Ray",
      "affiliation": "Tetrad Graduate Program, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Laura Lambeth",
      "first": "Laura",
      "last": "Lambeth",
      "affiliation": "Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Ivy Hsieh",
      "first": "Ivy",
      "last": "Hsieh",
      "affiliation": "Department of Pathology, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Debbie Ruelas",
      "first": "Debbie",
      "last": "Ruelas",
      "affiliation": "Department of Pathology, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "K. C. Lim",
      "first": "K. C.",
      "last": "Lim",
      "affiliation": "Department of Pathology, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Judy Sakanari",
      "first": "Judy",
      "last": "Sakanari",
      "affiliation": "Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Charles S. Craik",
      "first": "Charles S.",
      "last": "Craik",
      "affiliation": "Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Matthew P. Jacobson",
      "first": "Matthew P.",
      "last": "Jacobson",
      "affiliation": "Department of Pharmaceutical Chemistry, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "James H. McKerrow",
      "first": "James H.",
      "last": "McKerrow",
      "affiliation": "Department of Pathology, University of California San Francisco, San Francisco, California, United States of America; Sandler Center for Drug Discovery, University of California San Francisco, San Francisco, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-04",
  "dateAccepted": "2012-02-16",
  "dateReceived": "2011-12-19",
  "volume": "6",
  "number": "4",
  "pages": "e1589",
  "tags": [
    "Biochemistry",
    "Biology",
    "Microbiology"
  ],
  "abstract": "Background\n          \nCercarial elastase is the major invasive larval protease in Schistosoma mansoni, a parasitic blood fluke, and is essential for host skin invasion. Genome sequence analysis reveals a greatly expanded family of cercarial elastase gene isoforms in Schistosoma mansoni. This expansion appears to be unique to S. mansoni, and it is unknown whether gene duplication has led to divergent protease function.\n\n          Methods\n          \nProfiling of transcript and protein expression patterns reveals that cercarial elastase isoforms are similarly expressed throughout the S. mansoni life cycle. Computational modeling predicts key differences in the substrate-binding pockets of various cercarial elastase isoforms, suggesting a diversification of substrate preferences compared with the ancestral gene of the family. In addition, active site labeling of SmCE reveals that it is activated prior to exit of the parasite from its intermediate snail host.\n\n          Conclusions\n          \nThe expansion of the cercarial gene family in S. mansoni is likely to be an example of gene dosage. In addition to its critical role in human skin penetration, data presented here suggests a novel role for the protease in egress from the intermediate snail host. This study demonstrates how enzyme activity-based analysis complements genomic and proteomic studies, and is key in elucidating proteolytic function.",
  "fullText": "Introduction Schistosomes and related blood flukes are platyhelminth parasites of hosts ranging from fish to humans. In humans, schistosome infection leads to the disease schistosomiasis, which affects upwards of 200 million people worldwide [1]. Prevalent in developing nations, it is ranked second to malaria in terms of overall morbidity caused by parasitic disease [2], [3]. The parasite has a complex life cycle, infecting both an intermediate snail host and a definitive human host. At multiple life cycle stages, the parasite must migrate through host tissue, and breach substantial structural barriers, including the extracellular matrix [4]. The process of early human infection is well-characterized: the multi-cellular larval stage, termed cercaria(e), directly penetrates host skin in a process facilitated by secretions from a glandular network that runs the length of the larval body. These secretions contain multiple histolytic proteases [5], [6]. In Schistosoma mansoni, the most of abundant of these is cercarial elastase (SmCE), an S1A serine protease named for its ability to digest insoluble elastin. Completion of the S. mansoni genome sequence revealed an expanded cercarial elastase gene family, including eight full-length genes. Based on sequence identity, these isoforms were classified as belonging to one of two groups: Group I, comprised of SmCE1a.1, SmCE1a.2, SmCE1b and SmCE1c; and Group 2, comprised of SmCE 2a.1, SmCE2a.2, SmCE2a.3 and SmCE2b. Corresponding proteins were identified for SmCE 1a, 1b and 2a. SmCE 1c was determined to be a pseudogene, and no corresponding protein has been identified for SmCE2b transcript. Multiple CE isoforms exist in the related species Schistosoma haematobium, including homologs of SmCE1a, 1b, 2a and 2b isoform types [7], [8] The expansion of this family appears to be limited to the human-specific schistosomes: Schistosoma japonicum encodes only a single cercarial elastase gene, and there is substantial evidence that this species, along with the avian-specific schistosomes, utilizes another class of protease in host invasion [7], [9]–[12]. The expansion of the cercarial elastase gene family in S. mansoni, and the sequence divergence among isoforms, suggests two alternative evolutionary scenarios: that gene duplication has allowed for differential regulation or diversified substrate specificity, in order to facilitate multiple functions in host invasion; or that SmCE gene duplication directly enhances fitness by simply increasing the amount of protease present. In the absence of robust genetic tools for studying schistosome biology, we decided to take a biochemical approach to study the functions of SmCE isoforms, using a combination of quantitative PCR, activity-based profiling and computer modeling to identify unique characteristics of the isoforms. Here, we present data that supports the hypothesis that expansion of the SmCE gene family in S. mansoni directly increases gene dosage, i.e. the number of copies of SmCE genes in the S. mansoni genome, which is likely to lead to a corresponding increase in SmCE protein levels. Moreover, analysis of SmCE isoform expression and activity throughout the parasite life cycle confirms that the protease is present and active very early in cercarial development, which suggests a potential role for the protease in egress from the"
}