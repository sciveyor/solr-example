{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000499",
  "doi": "10.1371/journal.pntd.0000499",
  "externalIds": [
    "pii:09-PNTD-RA-0204R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The New Anthelmintic Tribendimidine is an L-type (Levamisole and Pyrantel) Nicotinic Acetylcholine Receptor Agonist",
  "authors": [
    {
      "name": "Yan Hu",
      "first": "Yan",
      "last": "Hu",
      "affiliation": "Section of Cell and Developmental Biology, University of California, San Diego, La Jolla, California, United States of America"
    },
    {
      "name": "Shu-Hua Xiao",
      "first": "Shu-Hua",
      "last": "Xiao",
      "affiliation": "National Institute of Parasitic Diseases, Chinese Center for Disease Control and Prevention, Shanghai, People's Republic of China"
    },
    {
      "name": "Raffi V. Aroian",
      "first": "Raffi V.",
      "last": "Aroian",
      "affiliation": "Section of Cell and Developmental Biology, University of California, San Diego, La Jolla, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-08",
  "dateAccepted": "2009-07-13",
  "dateReceived": "2009-05-01",
  "volume": "3",
  "number": "8",
  "pages": "e499",
  "tags": [
    "Infectious Diseases/Antimicrobials and Drug Resistance",
    "Infectious Diseases/Helminth Infections"
  ],
  "abstract": "Background\n\nIntestinal parasitic nematodes such as hookworms, Ascaris lumbricoides, and Trichuris trichiura are amongst most prevalent tropical parasites in the world today. Although these parasites cause a tremendous disease burden, we have very few anthelmintic drugs with which to treat them. In the past three decades only one new anthelmintic, tribendimidine, has been developed and taken into human clinical trials. Studies show that tribendimidine is safe and has good clinical activity against Ascaris and hookworms. However, little is known about its mechanism of action and potential resistance pathway(s). Such information is important for preventing, detecting, and managing resistance, for safety considerations, and for knowing how to combine tribendimidine with other anthelmintics.\n\nMethodology/Principal Findings\n\nTo investigate how tribendimidine works and how resistance to it might develop, we turned to the genetically tractable nematode, Caenorhabditis elegans. When exposed to tribendimidine, C. elegans hermaphrodites undergo a near immediate loss of motility; longer exposure results in extensive body damage, developmental arrest, reductions in fecundity, and/or death. We performed a forward genetic screen for tribendimidine-resistant mutants and obtained ten resistant alleles that fall into four complementation groups. Intoxication assays, complementation tests, genetic mapping experiments, and sequencing of nucleic acids indicate tribendimidine-resistant mutants are resistant also to levamisole and pyrantel and alter the same genes that mutate to levamisole resistance. Furthermore, we demonstrate that eleven C. elegans mutants isolated based on their ability to resist levamisole are also resistant to tribendimidine.\n\nConclusions/Significance\n\nOur results demonstrate that the mechanism of action of tribendimidine against nematodes is the same as levamisole and pyrantel, namely, tribendimidine is an L-subtype nAChR agonist. Thus, tribendimidine may not be a viable anthelmintic where resistance to levamisole or pyrantel already exists but could productively be used where resistance to benzimidazoles exists or could be combined with this class of anthelmintics.",
  "fullText": "Introduction Thirteen neglected tropical diseases have tremendous impact on the lives of billions of the poorest peoples in the world with an estimated total disease burden of 56.6 million disability-adjusted life years, exceeding that of malaria (46.5 million) and tuberculosis (34.7 million) [1],[2]. These diseases play a major role in keeping infected peoples mired in poverty and in a low socioeconomic state [1],[2]. The top three of these poverty-promoting tropical diseases are caused by intestinal nematodes: ascariasis (caused by Ascaris lumbricoides), trichuriasis (caused by Trichuris trichiura or whipworm), and hookworm disease (caused by Necator americanus and Acylostoma duodenale). These parasites (hookworms, Ascaris, and Trichuris or HAT) are amongst the most common human parasitic infections, with an estimated 576–740 million people infected with hookworms, 807–1221 million infected with Ascaris, and 604–795 million infected with Trichuris [3]. Extensive and detrimental impacts of HAT infections have been reported on human growth, nutrition, fitness, stature, metabolism, cognition, immunity, school attendance/performance, earnings, and pregnancy [3],[4],[5],[6]. A recent and thorough meta-analysis of deworming studies in children demonstrated that deworming children in areas for which HAT parasites are prevalent results in statistically significant improvements in almost all primary outcome measures (weight, height, mid-upper arm circumference, and triceps skin fold) and in all secondary outcome measures (e.g., weight-for-age, height-for-age, …) [5]. Although HAT infections are one of the most prevalent and important infectious diseases in the world, few treatment options exist. The World Health Organization (WHO) has approved two classes of compounds (anthelmintics) for treatment of intestinal nematode parasites: the benzimidazoles (i.e., mebendazole and albendazole) and the nicotinic acetylcholine receptor (nAChR) agonists (i.e., levamisole and pyrantel) [7]. For practical reasons (e.g., efficacy against hookworm, single dose application, weight-independent dosing), only one drug, albendazole, is the drug of choice for Mass Drug Administration [7],[8]. Given the limited number of drugs available, the enormous numbers of people to be treated, and the necessity for repeated treatment due to high reinfection rates and population dynamics of the parasites, the emergence of resistance to existing anthelmintics (already an enormous problem for veterinary anthelmintics [9]) poses a serious threat to large-scale deworming efforts. Thus there have been urgent and repeated calls for the development of new human anthelmintics [6],[7],[10]. In the past 30 years, only one new anthelmintic to treat human HAT infections has reached the clinic, tribendimidine. Tribendimidine, a symmetrical diamidine derivative of amidantel, is a broad-spectrum anthelmintic drug developed by the Chinese National Institute of Parasitic Diseases during the 1980s [11]. It was approved for human use by the China State Food and Drug Administration in 2004 and is currently undergoing clinical testing in China [11],[12]. Laboratory and clinical investigations demonstrate that this drug is safe and has a broad spectrum of single-dose activity against parasitic nematode infections in humans, including against Ascaris, hookworms and Strongyloides stercoralis with reported cure rates of 92–96%, 52–90%, and 55% respectively [11],[12]. A phase IV clinical trial of tribendimidine recently has been conducted in China [13]. In addition to intestinal nematode infections, tribendimidine has also"
}