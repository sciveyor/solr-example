{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000125",
  "doi": "10.1371/journal.pntd.0000125",
  "externalIds": [
    "pii:07-PNTD-RA-0097R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "An Oral Recombinant Vaccine in Dogs against Echinococcus granulosus, the Causative Agent of Human Hydatid Disease: A Pilot Study",
  "authors": [
    {
      "name": "Anne-Francoise Petavy",
      "first": "Anne-Francoise",
      "last": "Petavy",
      "affiliation": "Department of Parasitology and Medical Mycology, Lyon University, Lyon, France"
    },
    {
      "name": "Carlos Hormaeche",
      "first": "Carlos",
      "last": "Hormaeche",
      "affiliation": "University of Cambridge, Cambridge, United Kingdom"
    },
    {
      "name": "Samia Lahmar",
      "first": "Samia",
      "last": "Lahmar",
      "affiliation": "National Veterinary School, Sidi Thabet, Tunisia"
    },
    {
      "name": "Hammou Ouhelli",
      "first": "Hammou",
      "last": "Ouhelli",
      "affiliation": "Institut Agronimique et Vétérinaire Hassan II, Rabat, Morocco"
    },
    {
      "name": "Alejandro Chabalgoity",
      "first": "Alejandro",
      "last": "Chabalgoity",
      "affiliation": "Facultad de Medicina y Facultad de Sciencias, University Montevideo, Montevideo, Uruguay"
    },
    {
      "name": "Thierry Marchal",
      "first": "Thierry",
      "last": "Marchal",
      "affiliation": "Veterinary School, Lyon, France"
    },
    {
      "name": "Samira Azzouz",
      "first": "Samira",
      "last": "Azzouz",
      "affiliation": "Department of Parasitology and Medical Mycology, Lyon University, Lyon, France"
    },
    {
      "name": "Fernanda Schreiber",
      "first": "Fernanda",
      "last": "Schreiber",
      "affiliation": "Facultad de Medicina y Facultad de Sciencias, University Montevideo, Montevideo, Uruguay"
    },
    {
      "name": "Gabriela Alvite",
      "first": "Gabriela",
      "last": "Alvite",
      "affiliation": "Facultad de Medicina y Facultad de Sciencias, University Montevideo, Montevideo, Uruguay"
    },
    {
      "name": "Marie-Elisabeth Sarciron",
      "first": "Marie-Elisabeth",
      "last": "Sarciron",
      "affiliation": "Department of Parasitology and Medical Mycology, Lyon University, Lyon, France"
    },
    {
      "name": "Duncan Maskell",
      "first": "Duncan",
      "last": "Maskell",
      "affiliation": "University of Cambridge, Cambridge, United Kingdom"
    },
    {
      "name": "Adriana Esteves",
      "first": "Adriana",
      "last": "Esteves",
      "affiliation": "Facultad de Medicina y Facultad de Sciencias, University Montevideo, Montevideo, Uruguay"
    },
    {
      "name": "Georges Bosquet",
      "first": "Georges",
      "last": "Bosquet",
      "affiliation": "Department of Parasitology and Medical Mycology, Lyon University, Lyon, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-01",
  "dateAccepted": "2007-10-16",
  "dateReceived": "2007-05-16",
  "volume": "2",
  "number": "1",
  "pages": "e125",
  "tags": [
    "Immunology/Immune Response",
    "Public Health and Epidemiology/Environmental Health"
  ],
  "abstract": "Dogs are the main source of human cystic echinococcosis. An oral vaccine would be an important contribution to control programs in endemic countries. We conducted two parallel experimental trials in Morocco and Tunisia of a new oral vaccine candidate against Echinococcus granulosus in 28 dogs. The vaccine was prepared using two recombinant proteins from adult worms, a tropomyosin (EgTrp) and a fibrillar protein similar to paramyosin (EgA31), cloned and expressed in a live attenuated strain of Salmonella enterica serovar typhimurium.\n\nIn each country, five dogs were vaccinated with the associated EgA31 and EgTrp; three dogs received only the vector Salmonella; and six dogs were used as different controls. The vaccinated dogs received two oral doses of the vaccine 21 d apart, and were challenged 20 d later with 75,000 living protoscoleces. The controls were challenged under the same conditions. All dogs were sacrificed 26–29 d postchallenge, before the appearance of eggs, for safety reasons.\n\nWe studied the histological responses to both the vaccine and control at the level of the duodenum, the natural localization of the cestode. Here we show a significant decrease of parasite burden in vaccinated dogs (70% to 80%) and a slower development rate in all remaining worms. The Salmonella vaccine EgA31-EgTrp demonstrated a high efficacy against E. granulosus promoting its potential role in reducing transmission to humans and animals.",
  "fullText": "Introduction Cystic echinococcosis, also called hydatidosis, represents a severe public health and livestock problem, particularly in developing countries [1]–[3]. The causative agent is the cestode Echinococcus granulosus. The adult stage may be found in the small intestine of canine carnivores. Growth of the larval stage throughout the internal organs, especially the liver and lungs, causes clinical signs in the intermediate hosts, such as sheep, cattle, and camels. Humans may also become host to this metacestode. Usually, however, intermediate hosts become infected by grazing on vegetation contaminated by eggs shed by adult worms via canine feces [4]. In various endemic areas, prevention and control programs have been established [5]. These programs usually involve the repeat treatment of dogs with praziquantel alongside the establishment of health education programs. However, such programs represent a significant financial burden for developing countries. A vaccine against hydatidosis (the disease caused by the larval stage of the parasite), or echinococcosis (the disease caused by the adult stage), is thus highly desirable in order to provide long-term prevention of the disease and to complement control programs. An effective vaccine against ovine hydatidosis, based on a recombinant protein from parasite oncospheres (first larval stage of the parasite), has been developed that targets the larval stage of the parasite [6]. If used in the field, this vaccine would need to be administered to all animals in a herd, which may be very costly to control programs. In contrast, a vaccine directed at protecting dogs against the adult worm would have to be given to only a few animals to protect the environment, because dogs are less numerous than other animals in the herd. Mathematical modeling has recently confirmed the possible utility of this strategy [7], and several authors have demonstrated its feasibility by showing that dogs can develop protective immunity against E. granulosus [8],[9]. Based on these reports, subcutaneously injected anti-echinococcosis vaccines have been prepared from soluble native protoscolex (scoleces situated in brood capsules) proteins as well as from recombinant proteins normally expressed by mature adult worms [10]. This vaccine aims at decreasing worm growth and egg development. Here we describe a vaccine that uses a live attenuated Salmonella mutant strain as a vector to deliver two recombinant proteins expressed by the adult stage of E. granulosus: tropomyosin (EgTrp) and (EgA31), which share some sequence elements with paramyosins. The vaccine is intended to protect domestic and stray dogs as well as wild canids from infection with E. granulosus. We studied the histological and immunological responses to both the vaccine and to a challenge at the level of the upper duodenum in dogs. Materials and Methods Plasmids and bacterial strains EgA31 and EgTrp are recombinant proteins. As described by Saboulard et al. [11], a PstI/DraI cDNA fragment of the EgA31 cDNA (GenBank [http://www.ncbi.nlm.nih.gov/] accession no. AF067807) was cloned into the PQE80L expression vector (Qiagen). Plasmid pQE11-EgTrp encoding the C terminus of E. granulosus antigen EgTrp and plasmid pTECH2 1994 have been described elsewhere [12],[13]. S. enterica serovar typhimurium (S. typhimurium) aroC"
}