{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000667",
  "doi": "10.1371/journal.pntd.0000667",
  "externalIds": [
    "pii:09-PNTD-RA-0625R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Functional Impairment of Human Myeloid Dendritic Cells during Schistosoma haematobium Infection",
  "authors": [
    {
      "name": "Bart Everts",
      "first": "Bart",
      "last": "Everts",
      "affiliation": "Department of Parasitology, Leiden University Medical Centre, Leiden, The Netherlands"
    },
    {
      "name": "Ayola A. Adegnika",
      "first": "Ayola A.",
      "last": "Adegnika",
      "affiliation": "Department of Parasitology, Leiden University Medical Centre, Leiden, The Netherlands; Institute of Tropical Medicine, University of Tübingen, Tübingen, Germany; Medical Research Unit, Albert Schweitzer Hospital, Lambaréné, Gabon"
    },
    {
      "name": "Yvonne C. M. Kruize",
      "first": "Yvonne C. M.",
      "last": "Kruize",
      "affiliation": "Department of Parasitology, Leiden University Medical Centre, Leiden, The Netherlands"
    },
    {
      "name": "Hermelijn H. Smits",
      "first": "Hermelijn H.",
      "last": "Smits",
      "affiliation": "Department of Parasitology, Leiden University Medical Centre, Leiden, The Netherlands"
    },
    {
      "name": "Peter G. Kremsner",
      "first": "Peter G.",
      "last": "Kremsner",
      "affiliation": "Institute of Tropical Medicine, University of Tübingen, Tübingen, Germany; Medical Research Unit, Albert Schweitzer Hospital, Lambaréné, Gabon"
    },
    {
      "name": "Maria Yazdanbakhsh",
      "first": "Maria",
      "last": "Yazdanbakhsh",
      "affiliation": "Department of Parasitology, Leiden University Medical Centre, Leiden, The Netherlands"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-04",
  "dateAccepted": "2010-03-11",
  "dateReceived": "2009-11-03",
  "volume": "4",
  "number": "4",
  "pages": "e667",
  "tags": [
    "Immunology/Immune Response",
    "Immunology/Immunomodulation",
    "Immunology/Leukocyte Signaling and Gene Expression"
  ],
  "abstract": "Chronic Schistosoma infection is often characterized by a state of T cell hyporesponsiveness of the host. Suppression of dendritic cell (DC) function could be one of the mechanisms underlying this phenomenon, since Schistosoma antigens are potent modulators of dendritic cell function in vitro. Yet, it remains to be established whether DC function is modulated during chronic human Schistosoma infection in vivo. To address this question, the effect of Schistosoma haematobium infection on the function of human blood DC was evaluated. We found that plasmacytoid (pDC) and myeloid DC (mDC) from infected subjects were present at lower frequencies in peripheral blood and that mDC displayed lower expression levels of HLA-DR compared to those from uninfected individuals. Furthermore, mDC from infected subjects, but not pDC, were found to have a reduced capacity to respond to TLR ligands, as determined by MAPK signaling, cytokine production and expression of maturation markers. Moreover, the T cell activating capacity of TLR-matured mDC from infected subjects was lower, likely as a result of reduced HLA-DR expression. Collectively these data show that S. haematobium infection is associated with functional impairment of human DC function in vivo and provide new insights into the underlying mechanisms of T cell hyporesponsiveness during chronic schistosomiasis.",
  "fullText": "Introduction Chronic parasitic helminth infections are generally associated with a T helper 2 (Th2)-like immunological profile as well as immune hyporesponsiveness [1], [2]. The latter characteristic is probably the result of immune evasion strategies that these pathogens have developed to ensure long term survival within their hosts and to allow continued transmission. Also chronic infections with blood-dwelling trematodes of the genus Schistosoma are well known for their capacity to suppress immune responses, which is not only reflected in negative regulation of parasite specific immune responses but also by attenuation of effector responses to third party antigens, including allergens [3]–[5], and other pathogens [6], [7]. Dendritic cells (DC) are the most important antigen presenting cells (APC) that initiate immune responses and as such play a central role in the development and regulation of protective immunity against invading pathogens as well as tolerance under homeostatic conditions [8], [9]. In peripheral blood, two major DC subsets can be identified: the CD11c+ myeloid DC (mDC) and CD123+ plasmacytoid DC (pDC) [10]. Both are immature DC that are in the process of migration to their target sites. It is believed that these subsets perform different functions in both innate and adaptive immune responses. Human mDC express Toll-like receptors (TLR)-2, -4 and -8 and are thought to preferentially induce T cell responses to invading pathogens. On the other hand, human pDC have a different but complementary TLR expression profile that includes TLR-7 and -9 and are believed to play an important role in innate anti-viral responses as well as in induction of tolerance against self-antigens [11], [12]. One of the main characteristics of immune hyporesponsivess during chronic schistosomiasis is the impairment of effector T cell responses, a process in which T cell anergy and regulatory T cells have been proposed to play an important role [13], [14]. Given that DC are central players in the priming and regulation of T cell responses, suppression of these host immune responses by schistosomes through modulation of DC function would seem plausible. Indeed, studies have demonstrated that host derived anti-inflammatory mediators, such as IL-10 [15], which is elevated during chronic schistosomiasis [5], [16], [17], as well as Schistosoma-derived molecules [18], [19] have the potential to modulate DC function and its T cell priming capacity in vitro. However, it remains to be established whether human DC function is actually modulated in vivo during chronic schistosomiasis. To address this question, we evaluated the function of blood DC in a cross-sectional study in Gabon, a region endemic for Schistosoma haematobium [5]. We find that mDC isolated from infected subjects have an impaired capacity to respond to TLR ligands and to prime T cell responses. In addition, our data indicate that the former observation is due to a dampened pro-inflammatory signaling rather than lower TLR expression, and that the latter finding originates from a reduced expression of HLA-DR. Collectively, the data show a functional impairment of blood mDC during chronic S. haematobium infection and shed new light on the mechanisms underlying immune hyporesponsivess during"
}