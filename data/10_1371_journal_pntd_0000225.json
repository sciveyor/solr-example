{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000225",
  "doi": "10.1371/journal.pntd.0000225",
  "externalIds": [
    "pii:07-PNTD-PP-0286R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Pentamidine Dosage: A Base/Salt Confusion",
  "authors": [
    {
      "name": "Thomas P. C. Dorlo",
      "first": "Thomas P. C.",
      "last": "Dorlo",
      "affiliation": "Center for Infection and Immunity Amsterdam (CINIMA), Division of Infectious Diseases, Tropical Medicine and AIDS, Academic Medical Center, University of Amsterdam, Amsterdam, The Netherlands"
    },
    {
      "name": "Piet A. Kager",
      "first": "Piet A.",
      "last": "Kager",
      "affiliation": "Center for Infection and Immunity Amsterdam (CINIMA), Division of Infectious Diseases, Tropical Medicine and AIDS, Academic Medical Center, University of Amsterdam, Amsterdam, The Netherlands"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-05",
  "volume": "2",
  "number": "5",
  "pages": "e225",
  "abstract": "Pentamidine has a long history in the treatment of human African trypanosomiasis (HAT) and leishmaniasis. Early guidelines on the dosage of pentamidine were based on the base-moiety of the two different formulations available. Confusion on the dosage of pentamidine arose from a different labelling of the two available products, either based on the salt or base moiety available in the preparation. We provide an overview of the various guidelines concerning HAT and leishmaniasis over the past decades and show the confusion in the calculation of the dosage of pentamidine in these guidelines and the subsequent published reports on clinical trials and reviews. At present, only pentamidine isethionate is available, but the advised dosage for HAT and leishmaniasis is (historically) based on the amount of pentamidine base. In the treatment of leishmaniasis this is probably resulting in a subtherapeutic treatment. There is thus a need for a new, more transparent and concise guideline concerning the dosage of pentamidine, at least in the treatment of HAT and leishmaniasis.",
  "fullText": "Antiprotozoal Activity of Pentamidine The finding of the antiprotozoal activity of the diamidine family of drugs was largely a matter of serendipity. They were discovered during a search for hypoglycaemic compounds that could affect trypanosomes. Of the compounds synthesized, pentamidine (Figure 1) proved to be the most useful, and since the early 1940s it has been used in the treatment and prophylaxis of human African trypanosomiasis (HAT, also known as sleeping sickness) and to some extent in the treatment of visceral leishmaniasis in India. Nowadays, pentamidine is mainly used for prophylaxis and treatment of Pneumocystis jirovecii pneumonia (PCP), and in the treatment of first-stage HAT and of several forms of American cutaneous leishmaniasis. In the past, two galenic formulations, both lyophilized salts of pentamidine (see Box 1), were available, one the 2-hydroxyethanesulfonic acid salt, called pentamidine isethionate (Pentacarinat or Pentam), the other the methanesulfonic acid salt, called pentamidine methanesulfonate or mesylate (Lomidine). These two preparations were used interchangeably, depending on local availability and preference, until the early 1990s when production of pentamidine methanesulfonate was stopped and only pentamidine isethionate remained. Box 1. Pentamidine Salts Because of the instability of aqueous pentamidine solutions, pentamidine (C19H24N4O2) is available for clinical use in the form of a powdered salt and reconstituted with water prior to administration. Pentamidine is a weak diprotic base due to the two amidine groups at both ends of the molecule, which means it can accept two protons in total and thus requires two monoprotic acid molecules to form a salt. A few pentamidine salts are described in The Merck Index [18], which can be formed with the following acids: Hydrochloric acid, forming pentamidine dihydrochloride (C19H24N4O2.2HCl), which is not in clinical use at the moment. 2-Hydroxyethanesulfonic acid, forming pentamidine diisethionate (C19H24N4O2.2C2H6O4S, Figure 1), which is most often incorrectly described as “pentamidine isethionate”, also in The Merck Index [18]. Methanesulfonic acid, forming pentamidine dimethanesulfonate or dimesylate (C19H24N4O2.2CH4O3S, Figure 1), which is also confusingly named “pentamidine methanesulfonate” or “pentamidine mesylate” in The Merck Index [18]. In accordance with most of the medical literature concerning pentamidine, the labels of the available preparations, and for reasons of uniformity, we chose to use the chemically incorrect names “pentamidine isethionate” and “pentamidine methanesulfonate” to refer to the two aforementioned salts of pentamidine in this article. Guidelines on the Treatment of Human African Trypanosomiasis From the first published guidelines onwards, the dosage of pentamidine was expressed in and based on the active ingredient, the pentamidine base-moiety of the salt preparations. Pentamidine isethionate contains 1 g of base per 1.74 g of salt, while pentamidine methanesulfonate contains 1 g of base per 1.56 g of salt. The labelling of the ampoules of the different products is a source of confusion; pentamidine isethionate is labelled according to the amount of salt in the preparation (300 mg salt per ampoule), while pentamidine methanesulfonate was labelled according to the base-moiety (120 mg base per ampoule). The successive guidelines of the World Health Organization (WHO) for pentamidine in the treatment of Trypanosoma"
}