{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001029",
  "doi": "10.1371/journal.pntd.0001029",
  "externalIds": [
    "pii:PNTD-D-10-00062"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Recent Food Shortage Is Associated with Leprosy Disease in Bangladesh: A Case-Control Study",
  "authors": [
    {
      "name": "Sabiena G. Feenstra",
      "first": "Sabiena G.",
      "last": "Feenstra",
      "affiliation": "Department of Public Health, Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands"
    },
    {
      "name": "Quamrun Nahar",
      "first": "Quamrun",
      "last": "Nahar",
      "affiliation": "Health Systems and Infectious Diseases Division, ICDDR, B, Dhaka, Bangladesh"
    },
    {
      "name": "David Pahan",
      "first": "David",
      "last": "Pahan",
      "affiliation": "Rural Health Program Nilphamari, The Leprosy Mission Bangladesh, Dhaka, Bangladesh"
    },
    {
      "name": "Linda Oskam",
      "first": "Linda",
      "last": "Oskam",
      "affiliation": "KIT Biomedical Research, Amsterdam, The Netherlands"
    },
    {
      "name": "Jan Hendrik Richardus",
      "first": "Jan Hendrik",
      "last": "Richardus",
      "affiliation": "Department of Public Health, Erasmus MC, University Medical Center Rotterdam, Rotterdam, The Netherlands"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-05",
  "dateAccepted": "2011-02-25",
  "dateReceived": "2010-10-13",
  "volume": "5",
  "number": "5",
  "pages": "e1029",
  "tags": [
    "Infectious Diseases",
    "Infectious disease control",
    "Infectious diseases",
    "Leprosy",
    "Medicine",
    "Neglected tropical diseases",
    "Non-Clinical Medicine",
    "Non-clinical medicine",
    "Nutrition",
    "Socioeconomic aspects of health"
  ],
  "abstract": "Background\n          \nLeprosy is remaining prevalent in the poorest areas of the world. Intensive control programmes with multidrug therapy (MDT) reduced the number of registered cases in these areas, but transmission of Mycobacterium leprae continues in most endemic countries. Socio-economic circumstances are considered to be a major determinant, but uncertainty exists regarding the association between leprosy and poverty. We assessed the association between different socio-economic factors and the risk of acquiring clinical signs of leprosy.\n\n          Methods and Findings\n          \n We performed a case-control study in two leprosy endemic districts in northwest Bangladesh. Using interviews with structured questionnaires we compared the socio-economic circumstances of recently diagnosed leprosy patients with a control population from a random cluster sample in the same area. Logistic regression was used to compare cases and controls for their wealth score as calculated with an asset index and other socio-economic factors. The study included 90 patients and 199 controls. A recent period of food shortage and not poverty per se was identified as the only socio-economic factor significantly associated with clinical manifestation of leprosy disease (OR 1.79 (1.06–3.02); p = 0.030). A decreasing trend in leprosy prevalence with an increasing socio-economic status as measured with an asset index is apparent, but not statistically significant (test for a trend: OR 0.85 (0.71–1.02); p = 0.083).\n\n          Conclusions\n          \nRecent food shortage is an important poverty related predictor for the clinical manifestation of leprosy disease. Food shortage is seasonal and poverty related in northwest Bangladesh. Targeted nutritional support for high risk groups should be included in leprosy control programmes in endemic areas to reduce risk of disease.",
  "fullText": "Introduction Leprosy is known as a disease of poverty. Only in the poorest areas of the world the infectious disease caused by Mycobacterium leprae is still endemic. A causal relationship between poverty and leprosy is difficult to demonstrate, and uncertainty exists about how leprosy and poverty are associated [1],[2]. Bangladesh is one of the countries where the disease is still endemic. Despite reaching the ‘elimination’ target of less than one registered case per 10,000 inhabitants for the whole country in 1998, the prevalence is still above target in some of the poorest areas of Bangladesh [3],[4]. In the poverty stricken northwest part of the country, where The Leprosy Mission Bangladesh is operating a leprosy control programme, the new case detection rate was still 1.25 per 10,000 inhabitants in 2008. To generate more knowledge about risk factors for leprosy and to assess the effect of new interventions, a research project was initiated in northwest Bangladesh in 2001: the COLEP study, a prospective (sero-) epidemiological study on contact transmission and chemoprophylaxis in leprosy [5]. The first results of the study indicated that prophylactic treatment with rifampicin is a promising way to prevent leprosy in contacts of patients [6]. Physical distance to a patient and the severity of the disease (leprosy classification) were identified as risk factors associated with transmission of Mycobacterium leprae to contacts of a patient. Furthermore, the host characteristics “blood relationship to the patient” and “age” were identified as risk factors for the development of clinically apparent disease, while a previous vaccination with BCG had a preventive effect [7]. These findings indicate that innate and acquired immunity affects the development of clinical signs of leprosy. Clinical disease occurs most probably in only 1–5% of persons infected with Mycobacterium leprae, after an incubation period of several years. The objective of this study, which is part of the COLEP project, was to assess the association between poverty and leprosy more closely, by measuring the effects of different socio-economic factors on acquiring clinical signs of leprosy disease. Methods Study area and population A case-control study was carried out in August 2009 in the districts of Nilphamari and Rangpur in northwest Bangladesh. This large (3951 km2) - mainly rural - area has app. 4.5 million inhabitants and is one of the poorest parts of Bangladesh [8],[9]. The first 110 new leprosy patients registered in 2009 in the study area were selected as cases. These patients were diagnosed by The Leprosy Mission Bangladesh or government facilities according to the national guidelines [10]. Only one patient per household was interviewed to avoid bias due to clustering. From the initially selected group, 10 people could not been reached, while one was excluded because he was living in the same household as another selected patient. Controls without leprosy were randomly selected from a referent group, representative for the general population in the area. This group was selected at the start of the COLEP study in 2002 by a multi-cluster sampling procedure [11]. Twenty clusters of 1000 people each"
}