{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000559",
  "doi": "10.1371/journal.pntd.0000559",
  "externalIds": [
    "pii:09-PNTD-ED-0461R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Empowering Women and Improving Female Reproductive Health through Control of Neglected Tropical Diseases",
  "authors": [
    {
      "name": "Peter J. Hotez",
      "first": "Peter J.",
      "last": "Hotez",
      "affiliation": "Department of Microbiology, Immunology, and Tropical Medicine, The George Washington University, Washington, D. C., United States of America; Sabin Vaccine Institute, Washington, D. C., United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-11",
  "volume": "3",
  "number": "11",
  "pages": "e559",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Women's Health"
  ],
  "fullText": "Secretary Hillary Clinton has made the rights of women, especially those living in low-income countries, a central theme of her tenure at the United States Department of State. During her recent 11-day trip to sub-Saharan Africa, issues of gender equality and empowering women arguably had their highest profile ever [1], and there is a clear commitment by both the Secretary and the Obama administration to work aggressively toward achieving the two major Millennium Development Goals (MDGs) that specifically advocate for women, namely MDG 3 “Promote gender equality and empower women” and MDG 5 “Improve maternal health” (see http://www.un.org/millenniumgoals). Because the neglected tropical diseases (NTDs) are the most common infections among the world's poorest people [2], including girls and women, there is now a strong case to be made for controlling the NTDs as a means of directly addressing MDG 3 and MDG 5. Studies conducted over the last two decades provide an evidence base that the NTDs are important factors that (i) impair reproductive health in developing countries; (ii) increase the transmission of sexually transmitted infections (STIs); and (iii) promote stigma and gender inequality (Table 1). For these reasons, interventions focused on NTD control and elimination could offer an opportunity for improving the health and rights of girls and women in the poorest countries of Africa, Asia, and Latin America and the Caribbean. The Impact of NTDs on Female Reproductive Health Pregnancy and lactation place huge iron demands on the mother and her child. A 1994 report from the World Health Organization (WHO) concluded that a woman living in a developing country is practically always on the verge of iron deficiency anemia either because of pregnancy, which requires the transfer of 300 mg of iron to the fetus during the third trimester and an additional 500 mg of iron to accommodate an increase in red blood cell mass, or lactation, in which each episode transfers 0.75 mg of iron from mother to child [3]. Moreover, even before she becomes pregnant, a woman of childbearing age suffers substantial iron losses from menstruation [3]. Anemia, defined as a reduction in hemoglobin to &lt;11 g/dl in the first and third trimester and &lt;10.5 g/dl in the second trimester, creates a dangerous state of health for both mother and child [4]. It is estimated that 20% of maternal deaths in Africa are attributed to anemia, while simultaneously anemia represents a key risk factor for poor pregnancy outcome and low birth weight [4],[5]. It now appears that human hookworm infection, one of the most common NTDs affecting 576–740 million people in developing countries, considerably adds to the iron loss and anemia that occurs during pregnancy [6]. An estimated 44 million pregnant women are infected with hookworm at any one time [3], including up to one-third of all pregnant women in sub-Saharan Africa [7]. In Africa and Latin America, hookworm is a major contributor to anemia in pregnancy [7],[8], while in Nepal and presumably elsewhere in Asia hookworm is responsible for 54% of cases of moderate"
}