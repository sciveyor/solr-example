{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000710",
  "doi": "10.1371/journal.pntd.0000710",
  "externalIds": [
    "pii:09-PNTD-RA-0742R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Gene Expression Patterns of Dengue Virus-Infected Children from Nicaragua Reveal a Distinct Signature of Increased Metabolism",
  "authors": [
    {
      "name": "P'ng Loke",
      "first": "P'ng",
      "last": "Loke",
      "affiliation": "Department of Medical Parasitology, School of Medicine, New York University, New York, New York, United States of America"
    },
    {
      "name": "Samantha N. Hammond",
      "first": "Samantha N.",
      "last": "Hammond",
      "affiliation": "Sustainable Sciences Institute, Managua, Nicaragua"
    },
    {
      "name": "Jacqueline M. Leung",
      "first": "Jacqueline M.",
      "last": "Leung",
      "affiliation": "Department of Medical Parasitology, School of Medicine, New York University, New York, New York, United States of America"
    },
    {
      "name": "Charles C. Kim",
      "first": "Charles C.",
      "last": "Kim",
      "affiliation": "Department of Biochemistry and Biophysics, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Sajeev Batra",
      "first": "Sajeev",
      "last": "Batra",
      "affiliation": "Department of Biochemistry and Biophysics, University of California San Francisco, San Francisco, California, United States of America"
    },
    {
      "name": "Crisanta Rocha",
      "first": "Crisanta",
      "last": "Rocha",
      "affiliation": "Unidad de Infectología, Hospital Infantil Manuel Jesús de Rivera, Managua, Nicaragua"
    },
    {
      "name": "Angel Balmaseda",
      "first": "Angel",
      "last": "Balmaseda",
      "affiliation": "Departamento de Virología, Centro Nacional de Diagóstico y Referencia, Ministerio de Salud, Managua, Nicaragua"
    },
    {
      "name": "Eva Harris",
      "first": "Eva",
      "last": "Harris",
      "affiliation": "Division of Infectious Diseases and Vaccinology, School of Public Health, University of California, Berkeley, California, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-06",
  "dateAccepted": "2010-04-21",
  "dateReceived": "2009-12-18",
  "volume": "4",
  "number": "6",
  "pages": "e710",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Infectious Diseases/Viral Infections"
  ],
  "abstract": "Background\n\nInfection with dengue viruses (DENV) leads to a spectrum of disease outcomes. The pathophysiology of severe versus non-severe manifestations of DENV infection may be driven by host responses, which could be reflected in the transcriptional profiles of peripheral blood immune cells.\n\nMethodology/Principal Findings\n\nWe conducted genome-wide microarray analysis of whole blood RNA from 34 DENV-infected children in Nicaragua collected on days 3–6 of illness, with different disease manifestations. Gene expression analysis identified genes that are differentially regulated between clinical subgroups. The most striking transcriptional differences were observed between dengue patients with and without shock, especially in the expression of mitochondrial ribosomal proteins associated with protein biosynthesis. In the dengue hemorrhagic fever patients, one subset of differentially expressed genes encode neutrophil-derived anti-microbial peptides associated with innate immunity. By performing a meta-analysis of our dataset in conjunction with previously published datasets, we confirmed that DENV infection in vivo is associated with large changes to protein and nucleic acid metabolism. Additionally, whereas in vitro infection leads to an increased interferon signature, this was not consistently observed from in vivo patient samples, suggesting that the interferon response in vivo is relatively transient and was no longer observed by days 3–6 of illness.\n\nConclusions/Significance\n\nThese data highlight important differences between different manifestations of severity during DENV infection as well as identify some commonalities. Compilation of larger datasets in the future across multiple studies, as we have initiated in this report, may well lead to better prediction of disease manifestation via a systems biology approach.",
  "fullText": "Introduction Infection by the four serotypes of dengue virus (DENV 1–4) can lead to outcomes ranging from asymptomatic infection to hemorrhagic fever and shock [1]. While many epidemiological risk factors have been identified for the development of the severe forms of disease [2], dengue hemorrhagic fever (DHF) and dengue shock syndrome (DSS), there is still much uncertainty as to the molecular mechanisms underlying pathogenesis [1], [3], [4]. Identifying signatures of host genome-wide transcriptional patterns can be a tool for biomarker discovery as well as for understanding molecular mechanisms and pathophysiological signatures of disease states. This approach has been applied in cancer biology [5] and infectious diseases [6], [7]. Recently, there have been several reports of expression profiling studies in DENV-infected patients [8], [9], [10], [11] and macaques [12]. Other groups have monitored gene expression in cell lines and primary cells infected with DENV in vitro [13], [14], [15]. By integrating results from in vitro studies as well as multiple in vivo studies from different field sites, it may be possible to assemble a comprehensive picture of DENV-host interactions that lead to different disease outcomes. The four DENV serotypes have been reported to exhibit distinct clinical characteristics [16], [17], [18]. It is currently unknown whether different serotypes are associated with distinct transcriptional profiles from peripheral blood. Unlike Southeast Asia where all DENV serotypes circulate simultaneously, at our study site in Nicaragua, one serotype tends to predominate each dengue season [18]. In 2003, when this study was conducted, 87% of the isolated viruses were DENV-1 serotype. To our knowledge, this is the first reported transcriptional profiling study of predominantly DENV-1-infected patients in the Americas rather than Southeast Asia. The goal of our study was to conduct a transcriptional profiling analysis of pediatric patients from Nicaragua with a predominantly DENV-1 infection and to compare our results with Southeast Asian studies with mixed serotype infections. We find that there is a distinct molecular profile between Nicaraguan dengue patients with shock relative to patients with DHF, and a more muted difference between DF patients and patients with DHF. We also conducted a microarray ‘meta-analysis’ to compare the dataset from Nicaraguan patients with existing dengue expression profiling datasets to identify common themes and differences. Materials and Methods Study design and clinical definitions A cross-sectional study was conducted in the Hospital Infantil Manuel de Jesús Rivera (HIMJR), the national pediatric reference hospital, in the capital city of Managua, Nicaragua, from September 2003 to February 2004, which represents the 2003 dengue season (see Supplemental Methods). Enrollment criteria consisted of hospitalized patients younger than 15 years of age who completed the informed consent and assent process and who presented with acute febrile illness and two or more of the following symptoms: headache, retro-orbital pain, myalgia, arthralgia, rash, and hemorrhagic manifestations. Subjects reflected the gender, age, and ethnic composition of the local pediatric population. A standardized questionnaire was administered to collect demographic and clinical information at admission, and clinical data during hospitalization was prospectively collected using standardized forms that were"
}