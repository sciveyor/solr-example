{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001410",
  "doi": "10.1371/journal.pntd.0001410",
  "externalIds": [
    "pii:PNTD-D-11-00576"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Inhibition of Dengue Virus Entry and Multiplication into Monocytes Using RNA Interference",
  "authors": [
    {
      "name": "Mohammed Abdelfatah Alhoot",
      "first": "Mohammed Abdelfatah",
      "last": "Alhoot",
      "affiliation": "Department of Medical Microbiology, Faculty of Medicine, University of Malaya, Kuala Lumpur, Malaysia"
    },
    {
      "name": "Seok Mui Wang",
      "first": "Seok Mui",
      "last": "Wang",
      "affiliation": "Department of Medical Microbiology, Faculty of Medicine, University of Malaya, Kuala Lumpur, Malaysia"
    },
    {
      "name": "Shamala Devi Sekaran",
      "first": "Shamala Devi",
      "last": "Sekaran",
      "affiliation": "Department of Medical Microbiology, Faculty of Medicine, University of Malaya, Kuala Lumpur, Malaysia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-11",
  "dateAccepted": "2011-10-19",
  "dateReceived": "2011-06-13",
  "volume": "5",
  "number": "11",
  "pages": "e1410",
  "tags": [
    "Applied microbiology",
    "Biology",
    "Biotechnology",
    "Developmental Biology",
    "Developmental biology",
    "Emerging infectious diseases",
    "Gene expression",
    "Host-pathogen interaction",
    "Infectious Diseases",
    "Infectious diseases",
    "Medical microbiology",
    "Medicine",
    "Microbial control",
    "Microbial growth and development",
    "Microbiology",
    "Molecular Biology",
    "Molecular cell biology",
    "Public Health and Epidemiology",
    "Public health",
    "Small molecules",
    "Virology"
  ],
  "abstract": "Background\n          \nDengue infection ranks as one of the most significant viral diseases of the globe. Currently, there is no specific vaccine or antiviral therapy for prevention or treatment. Monocytes/macrophages are the principal target cells for dengue virus and are responsible for disseminating the virus after its transmission. Dengue virus enters target cells via receptor-mediated endocytosis after the viral envelope protein E attaches to the cell surface receptor. This study aimed to investigate the effect of silencing the CD-14 associated molecule and clathrin-mediated endocytosis using siRNA on dengue virus entry into monocytes.\n\n          Methodology/Principal Findings\n          \nGene expression analysis showed a significant down-regulation of the target genes (82.7%, 84.9 and 76.3% for CD-14 associated molecule, CLTC and DNM2 respectively) in transfected monocytes. The effect of silencing of target genes on dengue virus entry into monocytes was investigated by infecting silenced and non-silenced monocytes with DENV-2. Results showed a significant reduction of infected cells (85.2%), intracellular viral RNA load (73.0%), and extracellular viral RNA load (63.0%) in silenced monocytes as compared to non-silenced monocytes.\n\n          Conclusions/Significance\n          \nSilencing the cell surface receptor and clathrin mediated endocytosis using RNA interference resulted in inhibition of the dengue virus entry and subsequently multiplication of the virus in the monocytes. This might serve as a novel promising therapeutic target to attenuate dengue infection and thus reduce transmission as well as progression to severe dengue hemorrhagic fever.",
  "fullText": "Introduction Dengue infection ranks as one of the most clinically significant and prevalent mosquito-borne viral diseases of the globe. It is an expanding public health problem particularly in the tropical and subtropical areas [1]. Following an incubation period of 3 to 14 days, fever and a variety of symptoms occur, coinciding with the appearance of dengue virus (DENV) in blood [2]. Immunopathological studies suggest that many tissues may be involved during dengue infection, as viral antigens are expressed in liver, lymph node, spleen and bone marrow [3], [4], [5]. DENV can infect and replicate in different mammalian cells, including monocytes, macrophages, dendritic cells, B and T leukocytes, endothelial cells, and bone marrow-, hepatoma-, neuroblastoma- and kidney-derived cells. Based on several observations and antibody dependent enhancement hypothesis, monocyte lineage cells are the major target for DENV [6], [7], [8], [9]. These cells are responsible for replication and dissemination of the virus after the infection from mosquito bites. Since monocytes/macrophages are active phagocytic cells with cytoplasmic lysosomal components that can eliminate microorganisms [10], the interaction of DENV with monocytes/macrophages may have detrimental effects on both virus and cells. DENV infected monocytes/macrophages release soluble mediators that strongly influence the biological characteristics of endothelial cells and the hematopoietic cell population. This indicates that the interactions between DENV and monocytes/macrophages are important in the pathogenesis of dengue hemorrhagic fever (DHF) and dengue shock syndrome (DSS). Previous studies suggest that DENV enters target cells after the viral envelope protein E attaches to an uncharacterized cell receptor [11]. Current studies indicate that multiple cell surface molecules, including GRP78 [12], heat shock proteins (Hsp) 70 and 90 [13], [14], lipopolysaccharide-binding CD 14-associated molecule [8], [15], [16], laminin receptor [17], mannose receptor [18], and DC-SIGN [19], were involved in DENV binding and subsequent virus infection in different target cells. CD-14 associated molecule has implicated as a surface receptors on monocytes for DENV-2 entry [8], [15], [16]. CD-14 associated molecule is a membrane protein expressed by monocytes, antigen presenting cells and neutrophils and plays a role in the innate immune system. CD-14 associated molecule is necessary for the cellular response in infections mediated by bacterial lipopolysaccharide, which activates monocytes for the expression of cytokines, growth factors, and procoagulatory factors [20]. Failure of interaction of lipopolysaccharide with CD-14 has been implicated in susceptibility to infectious diseases [21]. After binding to the surface receptor, DENV internalizes into the cell cytoplasm by membrane fusion and this process may take place within intracellular vesicles (pH-dependent). This endocytic internalization offers the advantage of guiding the virion to an adequate site for replication and bypassing many cytoplasmatic barriers. Several endocytic pathways have recently been identified [22], but the clathrin-mediated endocytosis has been demonstrated as the main entry pathway for DENV-2 [23], [24], [25]. Clathrin-mediated endocytosis plays a crucial role in the formation of coated vesicles, antigen presentation, nutrient acquisition, clearance of apoptotic cells, pathogen entry, receptor regulation, hypertension, and synaptic transmission. Despite the importance and increasing incidence of DENV as a human pathogen, there are no"
}