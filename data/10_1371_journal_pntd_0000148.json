{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000148",
  "doi": "10.1371/journal.pntd.0000148",
  "externalIds": [
    "pii:07-PNTD-RA-0003R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "On the Origin of the Treponematoses: A Phylogenetic Approach",
  "authors": [
    {
      "name": "Kristin N. Harper",
      "first": "Kristin N.",
      "last": "Harper",
      "affiliation": "Department of Population Biology, Ecology, and Evolution, Emory University, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Paolo S. Ocampo",
      "first": "Paolo S.",
      "last": "Ocampo",
      "affiliation": "School of Medicine, Emory University, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Bret M. Steiner",
      "first": "Bret M.",
      "last": "Steiner",
      "affiliation": "Laboratory Reference and Research Branch, Division of Sexually Transmitted Diseases Prevention, NCHHSTP, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Robert W. George",
      "first": "Robert W.",
      "last": "George",
      "affiliation": "Laboratory Reference and Research Branch, Division of Sexually Transmitted Diseases Prevention, NCHHSTP, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Michael S. Silverman",
      "first": "Michael S.",
      "last": "Silverman",
      "affiliation": "Department of Medicine, Division of Infectious Diseases, University of Toronto, Ontario, Canada; Lakeridge Health Centre, Ontario, Canada"
    },
    {
      "name": "Shelly Bolotin",
      "first": "Shelly",
      "last": "Bolotin",
      "affiliation": "Department of Microbiology, Mount Sinai Hospital, Toronto, Canada"
    },
    {
      "name": "Allan Pillay",
      "first": "Allan",
      "last": "Pillay",
      "affiliation": "Laboratory Reference and Research Branch, Division of Sexually Transmitted Diseases Prevention, NCHHSTP, U.S. Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Nigel J. Saunders",
      "first": "Nigel J.",
      "last": "Saunders",
      "affiliation": "Sir William Dunn School of Pathology, University of Oxford, Oxford, United Kingdom"
    },
    {
      "name": "George J. Armelagos",
      "first": "George J.",
      "last": "Armelagos",
      "affiliation": "Department of Anthropology, Emory University, Atlanta, Georgia, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-01",
  "dateAccepted": "2007-11-15",
  "dateReceived": "2007-02-01",
  "volume": "2",
  "number": "1",
  "pages": "e148",
  "tags": [
    "Evolutionary Biology",
    "Evolutionary Biology/Evolutionary and Comparative Genetics",
    "Evolutionary Biology/Microbial Evolution and Genomics",
    "Genetics and Genomics/Microbial Evolution and Genomics",
    "Infectious Diseases/Bacterial Infections",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Sexually Transmitted Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Microbiology/Microbial Evolution and Genomics"
  ],
  "abstract": "Background\n    \nSince the first recorded epidemic of syphilis in 1495, controversy has surrounded the origins of the bacterium Treponema pallidum subsp. pallidum and its relationship to the pathogens responsible for the other treponemal diseases: yaws, endemic syphilis, and pinta. Some researchers have argued that the syphilis-causing bacterium, or its progenitor, was brought from the New World to Europe by Christopher Columbus and his men, while others maintain that the treponematoses, including syphilis, have a much longer history on the European continent.\n\n    Methodology/Principal Findings\n    \nWe applied phylogenetics to this problem, using data from 21 genetic regions examined in 26 geographically disparate strains of pathogenic Treponema. Of all the strains examined, the venereal syphilis-causing strains originated most recently and were more closely related to yaws-causing strains from South America than to other non-venereal strains. Old World yaws-causing strains occupied a basal position on the tree, indicating that they arose first in human history, and a simian strain of T. pallidum was found to be indistinguishable from them.\n\n    Conclusions/Significance\n    \nOur results lend support to the Columbian theory of syphilis's origin while suggesting that the non-sexually transmitted subspecies arose earlier in the Old World. This study represents the first attempt to address the problem of the origin of syphilis using molecular genetics, as well as the first source of information regarding the genetic make-up of non-venereal strains from the Western hemisphere.",
  "fullText": "Introduction As Naples fell before the invading army of Charles the VIII in 1495, a plague broke out among the French leader's troops [1]. When the army disbanded shortly after the campaign, the troops, composed largely of mercenaries, returned to their homes and disseminated the disease across Europe [2],[3]. Today, it is generally agreed that this outbreak was the first recorded epidemic of syphilis. Although its death toll remains controversial, there is no question that the infection devastated the continent [4]. Because the epidemic followed quickly upon the return of Columbus and his men from the New World, some speculated that the disease originated in the Americas [2]. Indeed, reports surfaced that indigenous peoples of the New World suffered from a similar malady of great antiquity [5] and that symptoms of this disease had been observed in members of Columbus's crew [3]. In the twentieth century, criticisms of the Columbian hypothesis arose, with some hypothesizing that Europeans had simply not distinguished between syphilis and other diseases such as leprosy prior to 1495 [6]. It was soon recognized that different varieties of treponemal disease exist. Unlike syphilis, which is caused by the spirochete T. pallidum subspecies pallidum, the other types normally strike during childhood and are transmitted through skin-to-skin or oral contact. All are quite similar with regard to symptoms and progression [7], but endemic syphilis, or bejel, caused by subsp. endemicum, has historically affected people living in hot, arid climates and yaws, caused by subsp. pertenue, is limited to hot and humid areas. Pinta, caused by Treponema carateum, is the most distinct member of this family of diseases. Once found in Central and South America, this mild disease is characterized solely by alterations in skin color. Today, the debate over the origin of treponemal disease encompasses arguments about whether the four infections are caused by distinct but related pathogens [8] or one protean bacterium with many manifestations [9]. Paleopathologists have played a pivotal role in addressing the question surrounding the origin of syphilis. The treponemal diseases, with the exception of pinta, leave distinct marks upon the skeleton and can thus be studied in past civilizations. Paleopathological studies of populations in the pre-Columbian New World show that treponemal disease was prevalent, with cases dating back 7,000 years and increasing over time [10]. In contrast, paleopathological studies of large pre-Columbian populations in Europe and Africa have yielded no evidence of treponemal disease [11]–[13]. However, isolated cases of pre-Columbian treponemal disease from other Old World excavation sites have been reported sporadically [14]. Although these cases have often been met with criticism regarding diagnosis, dating, and epidemiological context, they have convinced some that treponemal disease did exist in the pre-Columbian Old World [15],[16]. The T. pallidum genome is small (roughly 1,000 kilobases) and was sequenced in 1998 [17]. However, comparative genetic studies of T. pallidum [18]–[23] have been rare and relatively small in scope. One reason for this is the difficulty in obtaining non-venereal strains for study. Today only five known laboratory strains of"
}