{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001249",
  "doi": "10.1371/journal.pntd.0001249",
  "externalIds": [
    "pii:PNTD-D-11-00092"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Using Detergent to Enhance Detection Sensitivity of African Trypanosomes in Human CSF and Blood by Loop-Mediated Isothermal Amplification (LAMP)",
  "authors": [
    {
      "name": "Dennis J. Grab",
      "first": "Dennis J.",
      "last": "Grab",
      "affiliation": "Department of Pathology, The Johns Hopkins University School of Medicine, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Olga V. Nikolskaia",
      "first": "Olga V.",
      "last": "Nikolskaia",
      "affiliation": "Department of Pathology, The Johns Hopkins University School of Medicine, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Noboru Inoue",
      "first": "Noboru",
      "last": "Inoue",
      "affiliation": "National Research Center for Protozoan Diseases, Obihiro University of Agriculture and Veterinary Medicine, Obihiro, Japan"
    },
    {
      "name": "Oriel M. M. Thekisoe",
      "first": "Oriel M. M.",
      "last": "Thekisoe",
      "affiliation": "Department of Zoology and Entomology, University of the Free State, Qwaqwa Campus, Phuthaditjhaba, South Africa"
    },
    {
      "name": "Liam J. Morrison",
      "first": "Liam J.",
      "last": "Morrison",
      "affiliation": "Wellcome Trust Centre for Molecular Parasitology, University of Glasgow, Glasgow, United Kingdom"
    },
    {
      "name": "Wendy Gibson",
      "first": "Wendy",
      "last": "Gibson",
      "affiliation": "School of Biological Sciences, University of Bristol, Bristol, United Kingdom"
    },
    {
      "name": "J. Stephen Dumler",
      "first": "J. Stephen",
      "last": "Dumler",
      "affiliation": "Department of Pathology, The Johns Hopkins University School of Medicine, Baltimore, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-08",
  "dateAccepted": "2011-06-08",
  "dateReceived": "2011-02-05",
  "volume": "5",
  "number": "8",
  "pages": "e1249",
  "tags": [
    "Biology"
  ],
  "abstract": "Background\n          \nThe loop-mediated isothermal amplification (LAMP) assay, with its advantages of simplicity, rapidity and cost effectiveness, has evolved as one of the most sensitive and specific methods for the detection of a broad range of pathogenic microorganisms including African trypanosomes. While many LAMP-based assays are sufficiently sensitive to detect DNA well below the amount present in a single parasite, the detection limit of the assay is restricted by the number of parasites present in the volume of sample assayed; i.e. 1 per µL or 103 per mL. We hypothesized that clinical sensitivities that mimic analytical limits based on parasite DNA could be approached or even obtained by simply adding detergent to the samples prior to LAMP assay.\n\n          Methodology/Principal Findings\n          \nFor proof of principle we used two different LAMP assays capable of detecting 0.1 fg genomic DNA (0.001 parasite). The assay was tested on dilution series of intact bloodstream form Trypanosoma brucei rhodesiense in human cerebrospinal fluid (CSF) or blood with or without the addition of the detergent Triton X-100 and 60 min incubation at ambient temperature. With human CSF and in the absence of detergent, the LAMP detection limit for live intact parasites using 1 µL of CSF as the source of template was at best 103 parasites/mL. Remarkably, detergent enhanced LAMP assay reaches sensitivity about 100 to 1000-fold lower; i.e. 10 to 1 parasite/mL. Similar detergent-mediated increases in LAMP assay analytical sensitivity were also found using DNA extracted from filter paper cards containing blood pretreated with detergent before card spotting or blood samples spotted on detergent pretreated cards.\n\n          Conclusions/Significance\n          \nThis simple procedure for the enhanced detection of live African trypanosomes in biological fluids by LAMP paves the way for the adaptation of LAMP for the economical and sensitive diagnosis of other protozoan parasites and microorganisms that cause diseases that plague the developing world.",
  "fullText": "Introduction Tsetse fly-transmitted African trypanosomes are major pathogens of humans and livestock. Two subspecies of Trypanosoma brucei (T. b. rhodesiense and T. b. gambiense) cause human African trypanosomiasis (HAT, commonly called sleeping sickness). After replicating at the tsetse fly bite site, trypanosomes enter the hemolymphatic system (early stage or stage 1) (5, 9). Without treatment, the parasites go on to invade the central nervous system (CNS; late stage or stage 2), a process that takes months to years with T. b. gambiense (West and Central African HAT) or weeks to months with T. b. rhodesiense (East African HAT). The parasites cause a meningoencephalitis leading to progressive neurologic involvement with concomitant psychiatric disorders, fragmentation of the circadian sleep-wake cycle and ultimately to death if untreated (4, 5, 9). A key issue in the treatment of HAT is to distinguish stage 1 from stage 2 disease, as the drugs used for the treatment of stage 2 need to cross the blood-brain barrier [1], [2]. The most widely used drug is melarsoprol (developed in 1949), which is effective for T. b. gambiense and T. b. rhodesiense HAT, but unfortunately, melarsoprol leads to severe and fatal encephalitis in about 5–10% of recipients despite treatment for this condition [3], [4], [5]. Therefore, where HAT is endemic, accurate staging is critical, because failure to treat CNS involvement leads to death, yet inappropriate CNS treatment exposes an early-stage patient unnecessarily to highly toxic and life-threatening drugs. The diagnosis of HAT in the rural clinical setting, where most patients are found, still relies largely on the detection of parasitemia by blood smear and/or CSF microscopy [6], [7]. While T. b. rhodesiense detection in blood is frequently successful, T. b. gambiense infections, which constitute over 90% of all HAT cases, typically show very low parasitemias, and concentration techniques such as centrifugation or mini-anion exchange columns are usually necessary [6], [7], [8]. Stage determination still relies on lumbar puncture to examine CSF for trypanosomes or white cell count/protein concentration suggestive of chronic meningoencephalitis. Threshold values for these parameters are controversial, with the conventional value for stage 2 (&gt;5 cells/µL) now increased to &gt;10 or even 20 cells/µL [9]. In summary, diagnosis and staging of HAT is currently time consuming, intensive and difficult. DNA-based diagnostic methods such as PCR and LAMP now offer greater sensitivity than existing diagnostic methods, detecting DNA from the equivalent of 0.01 parasites or less. Based on PCR protocols for HAT [10], we described LAMP targeting the conserved paraflagellar rod A (PRFA) gene in all T. brucei subspecies and T. evansi [11]. LAMP is an isothermal DNA amplification method with excellent analytical sensitivity and specificity when employed for the detection of a variety of microorganisms (reviewed in [12]), including human and animal infective African trypanosomes [11], [13], [14], [15], [16], [17], [18], [19], [20], [21]. LAMP relies on autocycling strand displacement coupled to DNA synthesis by Bst DNA polymerase, a reaction similar to rolling-circle amplification [22] but with the added advantage that a heat-denaturing step is not"
}