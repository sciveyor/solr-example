{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000567",
  "doi": "10.1371/journal.pntd.0000567",
  "externalIds": [
    "pii:09-PNTD-RA-0357R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Chagas Disease, Migration and Community Settlement Patterns in Arequipa, Peru",
  "authors": [
    {
      "name": "Angela M. Bayer",
      "first": "Angela M.",
      "last": "Bayer",
      "affiliation": "Division of Infectious Diseases, David Geffen School of Medicine, University of California at Los Angeles, Los Angeles, California, United States of America; Asociación Benéfica Proyectos en Informática, Salud, Medicina y Agricultura (AB PRISMA), Lima, Peru"
    },
    {
      "name": "Gabrielle C. Hunter",
      "first": "Gabrielle C.",
      "last": "Hunter",
      "affiliation": "Asociación Benéfica Proyectos en Informática, Salud, Medicina y Agricultura (AB PRISMA), Lima, Peru"
    },
    {
      "name": "Robert H. Gilman",
      "first": "Robert H.",
      "last": "Gilman",
      "affiliation": "Asociación Benéfica Proyectos en Informática, Salud, Medicina y Agricultura (AB PRISMA), Lima, Peru; Department of International Health, Johns Hopkins Bloomberg School of Public Health, Baltimore, Maryland, United States of America"
    },
    {
      "name": "Juan G. Cornejo del Carpio",
      "first": "Juan G.",
      "last": "Cornejo del Carpio",
      "affiliation": "Dirección Regional del Ministerio de Salud, Arequipa, Peru"
    },
    {
      "name": "Cesar Naquira",
      "first": "Cesar",
      "last": "Naquira",
      "affiliation": "Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Caryn Bern",
      "first": "Caryn",
      "last": "Bern",
      "affiliation": "Division of Parasitic Diseases, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Michael Z. Levy",
      "first": "Michael Z.",
      "last": "Levy",
      "affiliation": "Center for Clinical Epidemiology and Biostatistics, Department of Biostatistics and Epidemiology, University of Pennsylvania School of Medicine, Philadelphia, Pennsylvania, United States of America; Fogarty International Center, National Institutes of Health, Bethesda, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-12",
  "dateAccepted": "2009-11-09",
  "dateReceived": "2009-07-17",
  "volume": "3",
  "number": "12",
  "pages": "e567",
  "tags": [
    "Infectious Diseases/Protozoal Infections",
    "Public Health and Epidemiology/Infectious Diseases",
    "Public Health and Epidemiology/Social and Behavioral Determinants of Health"
  ],
  "abstract": "Background\n\nChagas disease is one of the most important neglected tropical diseases in the Americas. Vectorborne transmission of Chagas disease has been historically rare in urban settings. However, in marginal communities near the city of Arequipa, Peru, urban transmission cycles have become established. We examined the history of migration and settlement patterns in these communities, and their connections to Chagas disease transmission.\n\nMethodology/Principal Findings\n\nThis was a qualitative study that employed focus group discussions and in-depth interviews. Five focus groups and 50 in-depth interviews were carried out with 94 community members from three shantytowns and two traditional towns near Arequipa, Peru. Focus groups utilized participatory methodologies to explore the community's mobility patterns and the historical and current presence of triatomine vectors. In-depth interviews based on event history calendars explored participants' migration patterns and experience with Chagas disease and vectors. Focus group data were analyzed using participatory analysis methodologies, and interview data were coded and analyzed using a grounded theory approach. Entomologic data were provided by an ongoing vector control campaign. We found that migrants to shantytowns in Arequipa were unlikely to have brought triatomines to the city upon arrival. Frequent seasonal moves, however, took shantytown residents to valleys surrounding Arequipa where vectors are prevalent. In addition, the pattern of settlement of shantytowns and the practice of raising domestic animals by residents creates a favorable environment for vector proliferation and dispersal. Finally, we uncovered a phenomenon of population loss and replacement by low-income migrants in one traditional town, which created the human settlement pattern of a new shantytown within this traditional community.\n\nConclusions/Significance\n\nThe pattern of human migration is therefore an important underlying determinant of Chagas disease risk in and around Arequipa. Frequent seasonal migration by residents of peri-urban shantytowns provides a path of entry of vectors into these communities. Changing demographic dynamics of traditional towns are also leading to favorable conditions for Chagas disease transmission. Control programs must include surveillance for infestation in communities assumed to be free of vectors.",
  "fullText": "Introduction Chagas disease, caused by infection with protozoan parasite Trypanosoma cruzi, causes more morbidity and mortality than any other parasitic disease in the Western Hemisphere [1]. T. cruzi is carried by numerous species of triatomine insects. Humans and other mammals usually become infected when the triatomine vector defecates during its blood meal, and fecal material containing the parasite is inoculated through the bite wound or mucous membranes [2]. Vector-borne transmission only occurs in the Americas, where 8–10 million people, including an estimated 192,000 Peruvians, are currently infected with T. cruzi [3],[4]. The member countries of the Southern Cone Initiative (INCOSUR) have worked since 1991 to eliminate household infestation with Triatoma infestans, the most important Chagas disease vector in the southern half of South America, through large-scale residual application of pyrethroid insecticides [5],[6]. Despite remarkable successes, major challenges remain to vector control, among them the increasing urbanization of the disease [7]. Chagas disease is traditionally associated with rural villages with adobe houses hospitable to T. infestans and other domestic vectors [8] and vector-borne transmission appears to be rare in urban settings [9]–[11]. However, in marginal communities of the city of Arequipa (pop. 750,000) in southern Peru, urban T. cruzi transmission cycles have become established [12],[13], and a vector control campaign has been in place in the city of Arequipa since 2002 [13]. The settlement and migration patterns in and around cities therefore may be important to understanding the dynamics that make certain communities more susceptible to Chagas disease vectors [14]. Latin America has experienced an overwhelming phenomenon of urbanization due in most part to in-migration, and Peru is no exception [15],[16]. Few studies have directly examined migration and settlement patterns, and their connections to Chagas disease transmission. Here we use qualitative methods to explore the migration and settlement patterns, and their links with vector infestation, in different communities around the city of Arequipa. Methods Ethics statement The research protocol was approved by the ethical review committees of the Asociación Benéfica PRISMA and the Johns Hopkins Bloomberg School of Public Health. All participants provided written informed consent prior to data collection, including consent for audio-recording. Study setting Arequipa is the second largest city in Peru, located in an arid zone 2,300 m above sea level [15]. The outskirts of the city contain hundreds of peri-urban pueblos jóvenes (young towns or shantytowns) and pueblos tradicionales (traditional towns). Pueblos jóvenes are low-income hillside squatter settlements founded over the past 60 years [17],[18]. Pueblos tradicionales tend to be in lower-lying flat areas, are inhabited by higher-income landowners, and date back to the late 19th or early 20th century. (See Figure 1 for photos of the two types of communities.) Because preliminary data from our research group indicated that T. infestans prevalence differed between these two types of towns [12], we compared migration and settlement patterns in 3 pueblos jóvenes and 2 pueblos tradicionales. Study participants The research team worked with 2–3 community leader “gatekeepers” in each community to ensure acceptance and to recruit people who"
}