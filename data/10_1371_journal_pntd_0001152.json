{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001152",
  "doi": "10.1371/journal.pntd.0001152",
  "externalIds": [
    "pii:PNTD-D-10-00244"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Clinical Manifestations Associated with Neurocysticercosis: A Systematic Review",
  "authors": [
    {
      "name": "Hélène Carabin",
      "first": "Hélène",
      "last": "Carabin",
      "affiliation": "Department of Biostatistics and Epidemiology, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    },
    {
      "name": "Patrick Cyaga Ndimubanzi",
      "first": "Patrick Cyaga",
      "last": "Ndimubanzi",
      "affiliation": "Department of Biostatistics and Epidemiology, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    },
    {
      "name": "Christine M. Budke",
      "first": "Christine M.",
      "last": "Budke",
      "affiliation": "Department of Veterinary Integrative Biosciences, College of Veterinary Medicine and Biomedical Sciences, Texas A&M University, College Station, Texas, United States of America"
    },
    {
      "name": "Hai Nguyen",
      "first": "Hai",
      "last": "Nguyen",
      "affiliation": "Department of Biostatistics and Epidemiology, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    },
    {
      "name": "Yingjun Qian",
      "first": "Yingjun",
      "last": "Qian",
      "affiliation": "National Institute of Parasitic Diseases, Shanghai, People's Republic of China"
    },
    {
      "name": "Linda Demetry Cowan",
      "first": "Linda Demetry",
      "last": "Cowan",
      "affiliation": "Department of Biostatistics and Epidemiology, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    },
    {
      "name": "Julie Ann Stoner",
      "first": "Julie Ann",
      "last": "Stoner",
      "affiliation": "Department of Biostatistics and Epidemiology, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    },
    {
      "name": "Elizabeth Rainwater",
      "first": "Elizabeth",
      "last": "Rainwater",
      "affiliation": "Department of Medicine, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    },
    {
      "name": "Mary Dickey",
      "first": "Mary",
      "last": "Dickey",
      "affiliation": "Department of Health Promotion Sciences, University of Oklahoma Health Sciences Center, Oklahoma City, Oklahoma, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-05",
  "dateAccepted": "2011-02-24",
  "dateReceived": "2010-12-10",
  "volume": "5",
  "number": "5",
  "pages": "e1152",
  "tags": [
    "Cysticercosis",
    "Epidemiology",
    "Epilepsy",
    "Headaches",
    "Hydrocephalus",
    "Infectious Diseases",
    "Infectious disease epidemiology",
    "Infectious diseases",
    "Infectious diseases of the nervous system",
    "Medicine",
    "Neglected tropical diseases",
    "Neurocysticercosis",
    "Neurological Disorders",
    "Neurology",
    "Public Health and Epidemiology"
  ],
  "abstract": "Background\n          \nThe clinical manifestations of neurocysticercosis (NCC) are poorly understood. This systematic review aims to estimate the frequencies of different manifestations, complications and disabilities associated with NCC.\n\n          Methods\n          \nA systematic search of the literature published from January 1, 1990, to June 1, 2008, in 24 different electronic databases and 8 languages was conducted. Meta-analyses were conducted when appropriate.\n\n          Results\n          \nA total of 1569 documents were identified, and 21 included in the analysis. Among patients seen in neurology clinics, seizures/epilepsy were the most common manifestations (78.8%, 95%CI: 65.1%–89.7%) followed by headaches (37.9%, 95%CI: 23.3%–53.7%), focal deficits (16.0%, 95%CI: 9.7%–23.6%) and signs of increased intracranial pressure (11.7%, 95%CI: 6.0%–18.9%). All other manifestations occurred in less than 10% of symptomatic NCC patients. Only four studies reported on the mortality rate of NCC.\n\n          Conclusions\n          \nNCC is a pleomorphic disease linked to a range of manifestations. Although definitions of manifestations were very rarely provided, and varied from study to study, the proportion of NCC cases with seizures/epilepsy and the proportion of headaches were consistent across studies. These estimates are only applicable to patients who are ill enough to seek care in neurology clinics and likely over estimate the frequency of manifestations among all NCC cases.",
  "fullText": "Introduction Neurocysticercosis (NCC) is primarily found in countries with poor sanitation and hygiene and improper slaughterhouse services. However, due to globalization and immigration, NCC is increasingly being reported in developed countries [1]. Humans become infected by ingesting Taenia solium eggs that later develop into oncospheres. These larvae can migrate to any organ in the body, but most reports have focused on cysts located in the Central Nervous System (CNS), eyes, muscles or subcutaneous tissues. The larvae have been found in several locations in the CNS. This diversity of locations is believed to partly explain the range of NCC's clinical manifestations. In addition, the signs and symptoms associated with NCC depend on the larvae's number, developmental stage (active, transitional or calcified), on the duration of the infection and the host's immune response [2]. Seizures and epilepsy are considered to be the most common manifestations of NCC. However, several other neurological disorders can also occur [3]. Unfortunately, these less common manifestations are rarely recognized as being linked to NCC, especially in low resource countries where imaging technology is scarce [4]. Thus, data on the full range of clinical expression of NCC are lacking, although such data are essential to accurately estimate the burden of NCC on different communities. This systematic review aims to estimate the frequency of the main clinical manifestations associated with NCC. Methods A systematic search of the literature, including documents published from January 1, 1990 to June 1, 2008, was conducted to capture data on clinical manifestations associated with NCC. Search strategy and data source PubMed, Commonwealth Agricultural Bureau (CAB) Abstracts, and 23 international databases were searched for data on NCC manifestations. Articles published in Chinese, English, French, Portuguese, Spanish, Italian, Romanian and German were searched. Two different searches were launched to cover both clinical manifestations and mortality associated with NCC infection. For the clinical manifestations, our search strategy in PubMed included terms: &quot;Cysticercosis/complications&quot; [MeSH] OR &quot;Cysticercosis/history&quot; [MeSH] OR “Cysticercosis/pathology&quot; [MeSH] OR &quot;Cysticercosis/psychology&quot; [MeSH] OR &quot;Cysticercosis/radiography&quot; [MeSH] OR &quot;Cysticercosis/radionuclide imaging&quot; [MeSH] OR &quot;Cysticercosis/ultrasonography&quot; [MeSH]. CAB Abstracts and the international search engines were queried using the following keywords: “Taenia solium”, “taeniasis” or “taeniosis”, “cysticercosis”, and “neurocysticercosis”. One Thesis in Medicine from Burkina Faso was identified through contacts in Sub-Saharan Africa and was included. For mortality associated with NCC, PubMed was searched using the terms: “cysticercosis/mortality” [MeSH] OR &quot;neurocysticercosis/mortality&quot; [MeSH]. In CAB Abstracts and the international search engines the keywords “neurocysticercosis and mortality” were used. Inclusion and exclusion criteria Documents reporting valid (defined as an absence of major biases, see later), original data on clinical manifestations associated with NCC were eligible for inclusion. Books and conference abstracts were excluded because they were unlikely to have sufficient details on the methodology used. All documents retrieved were screened based on the title and the abstract. The exclusion criteria for phase I were: 1) wrong agent; 2) animal data only; 3) no original data on the frequency of NCC's clinical manifestations; 4) case series with less than 20 participants; 5) review article without original"
}