{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001283",
  "doi": "10.1371/journal.pntd.0001283",
  "externalIds": [
    "pii:PNTD-D-11-00417"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "The Short Non-Coding Transcriptome of the Protozoan Parasite Trypanosoma cruzi",
  "authors": [
    {
      "name": "Oscar Franzén",
      "first": "Oscar",
      "last": "Franzén",
      "affiliation": "Science for Life Laboratory, Department of Cell and Molecular Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Erik Arner",
      "first": "Erik",
      "last": "Arner",
      "affiliation": "RIKEN Omics Sciences Center, RIKEN Yokohama Institute, Yokohama, Kanagawa, Japan"
    },
    {
      "name": "Marcela Ferella",
      "first": "Marcela",
      "last": "Ferella",
      "affiliation": "Rudbeck Laboratory, Department of Immunology, Genetics and Pathology, Uppsala University, Uppsala, Sweden"
    },
    {
      "name": "Daniel Nilsson",
      "first": "Daniel",
      "last": "Nilsson",
      "affiliation": "Institute of Cell Biology, University of Bern, Bern, Switzerland"
    },
    {
      "name": "Patricia Respuela",
      "first": "Patricia",
      "last": "Respuela",
      "affiliation": "Rudbeck Laboratory, Department of Immunology, Genetics and Pathology, Uppsala University, Uppsala, Sweden"
    },
    {
      "name": "Piero Carninci",
      "first": "Piero",
      "last": "Carninci",
      "affiliation": "RIKEN Omics Sciences Center, RIKEN Yokohama Institute, Yokohama, Kanagawa, Japan"
    },
    {
      "name": "Yoshihide Hayashizaki",
      "first": "Yoshihide",
      "last": "Hayashizaki",
      "affiliation": "RIKEN Omics Sciences Center, RIKEN Yokohama Institute, Yokohama, Kanagawa, Japan"
    },
    {
      "name": "Lena Åslund",
      "first": "Lena",
      "last": "Åslund",
      "affiliation": "Rudbeck Laboratory, Department of Immunology, Genetics and Pathology, Uppsala University, Uppsala, Sweden"
    },
    {
      "name": "Björn Andersson",
      "first": "Björn",
      "last": "Andersson",
      "affiliation": "Science for Life Laboratory, Department of Cell and Molecular Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Carsten O. Daub",
      "first": "Carsten O.",
      "last": "Daub",
      "affiliation": "RIKEN Omics Sciences Center, RIKEN Yokohama Institute, Yokohama, Kanagawa, Japan"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-08",
  "dateAccepted": "2011-07-04",
  "dateReceived": "2011-05-10",
  "volume": "5",
  "number": "8",
  "pages": "e1283",
  "tags": [
    "Biology",
    "Computational Biology",
    "Computational biology",
    "Genetics",
    "Genetics and Genomics",
    "Microbiology"
  ],
  "abstract": "The pathway for RNA interference is widespread in metazoans and participates in numerous cellular tasks, from gene silencing to chromatin remodeling and protection against retrotransposition. The unicellular eukaryote Trypanosoma cruzi is missing the canonical RNAi pathway and is unable to induce RNAi-related processes. To further understand alternative RNA pathways operating in this organism, we have performed deep sequencing and genome-wide analyses of a size-fractioned cDNA library (16–61 nt) from the epimastigote life stage. Deep sequencing generated 582,243 short sequences of which 91% could be aligned with the genome sequence. About 95–98% of the aligned data (depending on the haplotype) corresponded to small RNAs derived from tRNAs, rRNAs, snRNAs and snoRNAs. The largest class consisted of tRNA-derived small RNAs which primarily originated from the 3′ end of tRNAs, followed by small RNAs derived from rRNA. The remaining sequences revealed the presence of 92 novel transcribed loci, of which 79 did not show homology to known RNA classes.",
  "fullText": "Introduction Trypanosoma cruzi is a protozoan parasite and the causative agent of Chagas' disease, which has substantial health and socioeconomic impact in Latin America [1]. Treatment is currently restricted to a small number of drugs with insufficient efficacy and potentially harmful side effects. The genome of T. cruzi strain CL Brener is complex in terms of sequence repetitiveness and is a hybrid between two diverged haplotypes, named non-Esmeraldo-like and Esmeraldo-like: we refer to them here as non-Esmeraldo and Esmeraldo. Taken together, both haplotypes [2] sum up to approximately 110 Mb distributed over at least 80 chromosomes [3]. Similar to other trypanosomatids, genes are organized into co-directional clusters that undergo polycistronic transcription. Gene rich regions are frequently interrupted by sequence repeats, which comprise at least 50% of the genome [2]. Several gene variants occur in tandemly repeated copies which often collapse in shotgun assemblies [4]. The genome of a different, non-hybrid strain named Sylvio X10 was recently sequenced and partially assembled, showing a core gene content highly similar to CL Brener [5]. The T. cruzi life cycle is complex and consists of several distinct life stages, morphological states and hosts [1]. To achieve successful completion of the life cycle, the parasite must rapidly adapt to different environments by regulating its gene expression [6]. Transcription in T. cruzi often, but not exclusively, starts at strand switch regions [7], [8], where long transcripts are produced by RNA polymerase II and matured via trans-splicing and polyadenylation [9]. There is to date no definite model of how and if transcription is regulated, as RNA polymerase II promoters for protein-coding genes have not been identified. Thus, it is thought that gene expression is mainly regulated at the post-transcriptional level [9]. RNA interference (RNAi) and related pathways are widespread in animals and other metazoans, participating in a wide range of cellular processes; from chromatin organization to silencing of genes and selfish elements. RNAi relies on small RNA molecules, approximately 20–30 nucleotides in length, to trigger target silencing. In eukaryotes, several different types of small RNAs have been identified. Of these, the best characterized are microRNAs (miRNAs) and small interfering RNAs (siRNAs). See Table 1 for a summary of small RNAs discussed in this study. Two proteins are required for small RNA biogenesis and function: Dicer and Argonaute. Among protozoan parasites, the RNAi machinery has either been lost or retained. T. cruzi have lost the canonical RNAi machinery, which has been confirmed both functionally [10] and from the genome sequence [2], although RNAi is functional in certain other trypanosomatid species [11], [12], [13]. In the African trypanosome Trypanosoma brucei, convincing evidence has shown the presence of an active RNAi machinery (see [14] for references) and more recently pseudogene-derived small RNAs, which were reported to suppress gene expression [15]. A similar situation has been observed in the Leishmania genus. Leishmania braziliensis possesses a functional RNAi-pathway [16], whereas other members of this genus do not [12]. Analyses of the T. cruzi genome have revealed lack of both Dicer and Argonaute"
}