{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000510",
  "doi": "10.1371/journal.pntd.0000510",
  "externalIds": [
    "pii:09-PNTD-RA-0297R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Trypanosoma cruzi IIc: Phylogenetic and Phylogeographic Insights from Sequence and Microsatellite Analysis and Potential Impact on Emergent Chagas Disease",
  "authors": [
    {
      "name": "Martin S. Llewellyn",
      "first": "Martin S.",
      "last": "Llewellyn",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "Michael D. Lewis",
      "first": "Michael D.",
      "last": "Lewis",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "Nidia Acosta",
      "first": "Nidia",
      "last": "Acosta",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom; Departamento de Medicina Tropical, Instituto de Investigaciones en Ciencias de la Salud, Universidad Nacional de Asuncion, Paraguay"
    },
    {
      "name": "Matthew Yeo",
      "first": "Matthew",
      "last": "Yeo",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "Hernan J. Carrasco",
      "first": "Hernan J.",
      "last": "Carrasco",
      "affiliation": "Instituto de Medicina Tropical, Universidad Central de Venezuela, Los Chaguaramos, Caracas, Venezuela"
    },
    {
      "name": "Maikell Segovia",
      "first": "Maikell",
      "last": "Segovia",
      "affiliation": "Instituto de Medicina Tropical, Universidad Central de Venezuela, Los Chaguaramos, Caracas, Venezuela"
    },
    {
      "name": "Jorge Vargas",
      "first": "Jorge",
      "last": "Vargas",
      "affiliation": "Centro Nacional de Enfermedades Tropicales, Santa Cruz, Bolivia"
    },
    {
      "name": "Faustino Torrico",
      "first": "Faustino",
      "last": "Torrico",
      "affiliation": "Universidad Mayor de San Simon, Cochabamba, Bolivia"
    },
    {
      "name": "Michael A. Miles",
      "first": "Michael A.",
      "last": "Miles",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "Michael W. Gaunt",
      "first": "Michael W.",
      "last": "Gaunt",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-09",
  "dateAccepted": "2009-07-30",
  "dateReceived": "2009-06-16",
  "volume": "3",
  "number": "9",
  "pages": "e510",
  "tags": [
    "Ecology/Evolutionary Ecology",
    "Ecology/Population Ecology",
    "Evolutionary Biology/Microbial Evolution and Genomics",
    "Microbiology/Parasitology",
    "Molecular Biology/Molecular Evolution",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Trypanosoma cruzi, the etiological agent of Chagas disease, is highly genetically diverse. Numerous lines of evidence point to the existence of six stable genetic lineages or DTUs: TcI, TcIIa, TcIIb, TcIIc, TcIId, and TcIIe. Molecular dating suggests that T. cruzi is likely to have been an endemic infection of neotropical mammalian fauna for many millions of years. Here we have applied a panel of 49 polymorphic microsatellite markers developed from the online T. cruzi genome to document genetic diversity among 53 isolates belonging to TcIIc, a lineage so far recorded almost exclusively in silvatic transmission cycles but increasingly a potential source of human infection. These data are complemented by parallel analysis of sequence variation in a fragment of the glucose-6-phosphate isomerase gene. New isolates confirm that TcIIc is associated with terrestrial transmission cycles and armadillo reservoir hosts, and demonstrate that TcIIc is far more widespread than previously thought, with a distribution at least from Western Venezuela to the Argentine Chaco. We show that TcIIc is truly a discrete T. cruzi lineage, that it could have an ancient origin and that diversity occurs within the terrestrial niche independently of the host species. We also show that spatial structure among TcIIc isolates from its principal host, the armadillo Dasypus novemcinctus, is greater than that among TcI from Didelphis spp. opossums and link this observation to differences in ecology of their respective niches. Homozygosity in TcIIc populations and some linkage indices indicate the possibility of recombination but cannot yet be effectively discriminated from a high genome-wide frequency of gene conversion. Finally, we suggest that the derived TcIIc population genetic data have a vital role in determining the origin of the epidemiologically important hybrid lineages TcIId and TcIIe.",
  "fullText": "Introduction At least 10 million people are thought to carry the infectious agent of Chagas disease, Trypanosoma cruzi, which is considered to be responsible for ∼13,000 deaths annually (www.who.int, [1]). The disease is a vector-borne zoonosis and transmission in its wild transmission cycle is maintained by numerous species of mammal reservoir and over half of approximately 140 known species of haematophagous triatomine bug [2]. The geographical distribution of silvatic T. cruzi stretches from the Southern States of the USA to Southern Argentina. Domestic transmission is limited to Central and South America where domiciliated vector species occur. Human infection occurs primarily through mucosal or broken skin contact with contaminated triatomine faeces egested by the insect during feeding. Consistent with an ancient association with South America [3] T. cruzi populations are highly diverse, with at least six stable discrete typing units (DTUs) reported: TcI, TcIIa, TcIIb, TcIIIc, TcIId, and TcIIe. Among these, TcI and TcIIb are the most divergent groups in molecular terms - estimates based on nuclear genes date their most recent common ancestor at 3–10 million years ago (MYA) [4]. The phylogenetic status of TcIIc and TcIIa is in full debate [5],[6]. Based on mosaic patterns of nucleotide diversity across nine nuclear genes, Westenberger et al., (2005) proposed that both are the product of an early hybridisation event(s) between lineages TcI and TcIIb [6]. Others argue that TcIIc and TcIIa represent a single ancestral group in their own right [5], whereby these lineages share a characteristic mitochondrial genome distinct from both TcI and TcIIb. These hypotheses are not mutually exclusive and TcIIa and TcIIc are not easily distinguished based on mitochondrial sequences [4]. However, nuclear gene sequences consistently support their status as genetically separate clades [4], [6]–[8] and flow cytometric analysis across a panel of representative strains reveals that TcIIc and TcIIa genomes are divergent in terms of their absolute size [9]. The current tendency to group TcIIc and TCIIa as a single lineage is an oversimplification that may arise from Miles's original Z3 classification [10]. In fact Miles clearly defines an additional lineage in later publications – Z3/Z1 ASAT, which corresponds to TcIIc [11],[12]. Researchers attempting to classify a third major lineage, TcIII - corresponding loosely to TcIIc - almost entirely ignore TcIIa [5], as well the large divergence between North and South American TcIIa isolates [13]. By contrast, there is general consensus in the literature regarding the evolutionary origin of the two remaining lineages, TCIId and TCIIe. These are almost certainly hybrids and nucleotide sequence [4], microsatellite [5], and enzyme electrophoretic [14],[15] data show that the parents are TcIIc and TcIIb. In line with experimental data [16], maxicircle kinetoplast DNA inheritance in TcIId and TcIIe appears to have been uniparental [4],[5], and both retain a mitochondrial genome similar to that of TcIIc. TcIIc is infrequently isolated from domestic transmission cycles. Sporadic reports of this lineage occur from domestic mammals in the Chaco region of Paraguay and Argentina as well as southern Brazil (Canis familiaris [17]–[19]) from humans in"
}