{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000194",
  "doi": "10.1371/journal.pntd.0000194",
  "externalIds": [
    "pii:07-PNTD-RA-0198R5"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Albendazole versus Praziquantel in the Treatment of Neurocysticercosis: A Meta-analysis of Comparative Trials",
  "authors": [
    {
      "name": "Dimitrios K. Matthaiou",
      "first": "Dimitrios K.",
      "last": "Matthaiou",
      "affiliation": "Alfa Institute of Biomedical Sciences (AIBS), Athens, Greece"
    },
    {
      "name": "Georgios Panos",
      "first": "Georgios",
      "last": "Panos",
      "affiliation": "Alfa Institute of Biomedical Sciences (AIBS), Athens, Greece; 1st IKA Hospital, Athens, Greece"
    },
    {
      "name": "Eleni S. Adamidi",
      "first": "Eleni S.",
      "last": "Adamidi",
      "affiliation": "National Technical University of Athens, Athens, Greece"
    },
    {
      "name": "Matthew E. Falagas",
      "first": "Matthew E.",
      "last": "Falagas",
      "affiliation": "Alfa Institute of Biomedical Sciences (AIBS), Athens, Greece; Tufts University School of Medicine, Boston, Massachusetts, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2008-03",
  "dateAccepted": "2008-01-18",
  "dateReceived": "2007-08-21",
  "volume": "2",
  "number": "3",
  "pages": "e194",
  "tags": [
    "Infectious Diseases/Helminth Infections",
    "Infectious Diseases/Infectious Diseases of the Nervous System",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Tropical and Travel-Associated Diseases",
    "Neurological Disorders/Infectious Diseases of the Nervous System",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Background\n    \nNeurocysticercosis, infection of the brain with larvae of Taenia solium (pork tapeworm), is one of several forms of human cysticercosis caused by this organism. We investigated the role of albendazole and praziquantel in the treatment of patients with parenchymal neurocysticercosis by performing a meta-analysis of comparative trials of their effectiveness and safety.\n\n    Methods and Principal Findings\n    \nWe performed a search in the PubMed database, Cochrane Database of Controlled Trials, and in references of relevant articles. Six studies were included in the meta-analysis. Albendazole was associated with better control of seizures than praziquantel in the pooled data analysis, when the generic inverse variance method was used to combine the incidence of seizure control in the included trials (patients without seizures/[patients×years at risk]) (156 patients in 4 studies, point effect estimate [incidence rate ratio] = 4.94, 95% confidence interval 2.45–9.98). In addition, albendazole was associated with better effectiveness than praziquantel in the total disappearance of cysts (335 patients in 6 studies, random effects model, OR = 2.30, 95% CI 1.06–5.00). There was no difference between albendazole and praziquantel in reduction of cysts, proportion of patients with adverse events, and development of intracranial hypertension due to the administered therapy.\n\n    Conclusions\n    \nA critical review of the available data from comparative trials suggests that albendazole is more effective than praziquantel regarding clinically important outcomes in patients with neurocysticercosis. Nevertheless, given the relative scarcity of trials, more comparative interventional studies—especially randomized controlled trials—are required to draw a safe conclusion about the best regimen for the treatment of patients with parenchymal neurocysticercosis.",
  "fullText": "Introduction Neurocysticercosis is a parasitic disease caused by the larval form of Taenia solium, known as pork tapeworm, when the larvae lodge in the central nervous system (CNS). It happens when human ingests the eggs, acting as the intermediate host in the life cycle of T. solium. The eggs hatch in the intestine and the embrya penetrate the intestinal wall and are distributed via the blood, anchoring in the CNS as a larval form of the parasite [1]. With T. solium parasitosis, both self-reinfection and infection of household members are common. Neurocysticercosis is mosst commonly found among members of agricultural societies with poor sanitary conditions and economies based on breeding livestock, especially pigs, with low hygiene standards [2]. However, it has also started to emerge in developed countries, as a result of immigration from endemic to nonendemic areas [3]. Its natural pool lies mainly in Latin America, sub-Saharan Africa, and Southeast Asia, and is an important cause of morbidity among local populations [2]. Neurocysticercosis is divided into four categories depending on the anatomical locus in which the larvae lodge—cerebral or parenchymal, subarachnoid or cisternal, intraventricular, and spinal [1]. The most common clinical sign of neurocysticercosis is epilepsy of any type, which is usually late-onset; this sign is typically found in parenchymal neurocysticercosis. Other common signs are focal neurological deficits, cerebellar or brainstem signs, signs of increased intracranial pressure, meningoencephalitic signs, dementia, or even death [4]. The standard therapeutic intervention was surgery until the development of cysticidal agents, the most common being praziquantel and albendazole [5]. Although there have been many clinical trials testing these drugs, controversy remains about their therapeutic value [5]. The reasons for this dispute include the severity of adverse effects, the actual reduction of cysts, and the subsequent control of seizures. This disagreement seems to have been resolved after the recent publication of a meta-analysis that shows the superiority of these agents compared to placebo [6]. We sought to investigate which of the two agents are preferable in the treatment of neurocysticercosis. Some studies have been published on this issue, although they mostly examine small numbers of patients. Specifically, we investigated the role of albendazole versus praziquantel in the treatment of patients with parenchymal neurocysticercosis by performing a meta-analysis of comparative trials [7] of their effectiveness and safety. Methods Data sources The studies for our meta-analysis were obtained from the PubMed database, Cochrane Database of Controlled Trials, and from references of relevant articles. Search terms included “albendazole”, “praziquantel”, “neurocysticercosis”, and “Taenia solium”. Although the search was performed without limitation on the language of publications, the evaluable studies were published in English, French, German, and Italian. There was no limitation on the year of publication. Study selection Two independent reviewers (DKM and GP) performed the search and selected the studies that were relevant to the scope of our meta-analysis. Any discrepancy or disagreement between the reviewers was resolved by consensus in meetings involving all authors. A study was considered eligible if (1) it was a prospective trial, (2)"
}