{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000389",
  "doi": "10.1371/journal.pntd.0000389",
  "externalIds": [
    "pii:08-PNTD-RA-0364R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Post-Epidemic Chikungunya Disease on Reunion Island: Course of Rheumatic Manifestations and Associated Factors over a 15-Month Period",
  "authors": [
    {
      "name": "Daouda Sissoko",
      "first": "Daouda",
      "last": "Sissoko",
      "affiliation": "Institut de Veille Sanitaire, Cellule Interrégionale d'Epidémiologie Réunion Mayotte, Direction Régionale des Affaires Sanitaires et Sociales, Saint Denis, La Réunion, France"
    },
    {
      "name": "Denis Malvy",
      "first": "Denis",
      "last": "Malvy",
      "affiliation": "Département des Maladies Infectieuses et Tropicales, Centre Hospitalier Universitaire de Bordeaux and Centre René Labusquière, Université de Bordeaux 2, Bordeaux, France"
    },
    {
      "name": "Khaled Ezzedine",
      "first": "Khaled",
      "last": "Ezzedine",
      "affiliation": "Département des Maladies Infectieuses et Tropicales, Centre Hospitalier Universitaire de Bordeaux and Centre René Labusquière, Université de Bordeaux 2, Bordeaux, France"
    },
    {
      "name": "Philippe Renault",
      "first": "Philippe",
      "last": "Renault",
      "affiliation": "Institut de Veille Sanitaire, Cellule Interrégionale d'Epidémiologie Réunion Mayotte, Direction Régionale des Affaires Sanitaires et Sociales, Saint Denis, La Réunion, France"
    },
    {
      "name": "Frederic Moscetti",
      "first": "Frederic",
      "last": "Moscetti",
      "affiliation": "Médecine Générale, Saint Paul, La Réunion, France"
    },
    {
      "name": "Martine Ledrans",
      "first": "Martine",
      "last": "Ledrans",
      "affiliation": "Institut de Veille Sanitaire, Département International et Tropical, Saint-Maurice, France"
    },
    {
      "name": "Vincent Pierre",
      "first": "Vincent",
      "last": "Pierre",
      "affiliation": "Institut de Veille Sanitaire, Cellule Interrégionale d'Epidémiologie Réunion Mayotte, Direction Régionale des Affaires Sanitaires et Sociales, Saint Denis, La Réunion, France"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-03",
  "dateAccepted": "2009-02-05",
  "dateReceived": "2008-10-10",
  "volume": "3",
  "number": "3",
  "pages": "e389",
  "tags": [
    "Public Health and Epidemiology",
    "Public Health and Epidemiology/Global Health",
    "Public Health and Epidemiology/Infectious Diseases",
    "Rheumatology",
    "Virology/Emerging Viral Diseases"
  ],
  "abstract": "Although the acute manifestations of Chikungunya virus (CHIKV) illness are well-documented, few data exist about the long-term rheumatic outcomes of CHIKV-infected patients. We undertook between June and September 2006 a retrospective cohort study aimed at assessing the course of late rheumatic manifestations and investigating potential risk factors associated with the persistence of these rheumatic manifestations over 15 months. 147 participants (&gt;16 yrs) with laboratory-confirmed CHIKV disease diagnosed between March 1 and June 30, 2005, were identified through a surveillance database and interviewed by telephone. At the 15-month-period evaluation after diagnosis, 84 of 147 participants (57%) self-reported rheumatic symptoms. Of these 84 patients, 53 (63%) reported permanent trouble while 31 (37%) had recurrent symptoms. Age ≥45 years (OR = 3.9, 95% CI 1.7–9.7), severe initial joint pain (OR = 4.8, 95% CI 1.9–12.1), and presence of underlying osteoarthritis comorbidity (OR = 2.9, 95% CI 1.1–7.4) were predictors of nonrecovery. Our findings suggest that long-term CHIKV rheumatic manifestations seem to be a frequent underlying post-epidemic condition. Three independent risk factors that may aid in early recognition of patients with the highest risk of presenting prolonged CHIKV illness were identified. Such findings may be particularly useful in the development of future prevention and care strategies for this emerging virus infection.",
  "fullText": "Introduction Chikungunya virus (CHIKV, family Togaviridae, genus Alphavirus) is a mosquito-borne virus belonging to the Semliki Forest serocomplex, which includes Ross River virus (RRV), O'nyong-nyong (ONN), Mayaro (MAY), and Barmah Forest viruses (BFV). Common clinical manifestations caused by these viruses include abrupt onset of fever, headache, backache, and arthralgia [1]. Since its initial identification in Newala province (Tanzania) in 1953 [2], CHIKV has been associated with numerous outbreaks, mainly in Africa [3],[4] and Asia [5],[6]. In early 2005, CHIKV was introduced into the Southwestern Indian Ocean region, probably from infected viraemic travellers arriving from Lamu (Kenya), where an outbreak started in June 2004 [7]. Subsequently, it rapidly spread across the Southwestern Indian Ocean islands (Comoros, Madagascar, Mayotte, Seychelles, Mauritius, Reunion Island), resulting in an extensive regional epidemic in 2005 and 2006 [8]–[12]. Afterward, these outbreaks' viruses expanded into Asian countries [13],[14]. Concurrently, numerous imported cases were noticed in nontropical Western countries [15],[16]. Moreover, these imported cases raised concerns about the possibility of the emergence of CHIKV infection in Europe, as Aedes albopictus, the mosquito vector of CHIKV in Reunion Island [17], is present in several European countries [18], including Italy, where it was first recorded in the 1990s and is particularly common [19]. Indeed, this concern was born out by documentation of an autochthonous CHIKV outbreak in the Ravenna region of Italy during the summer of 2007, which was linked to a viraemic index patient originating in Kerala, India [20]. In Reunion Island, CHIKV outbreak evolved in two-wave phenomena with a first wave between March to June 2005 and the second one from December 2005 to June 2006. The overall attack rate in Reunion Island was estimated to be 35% in mid-2006. Subsequently, CHIKV presented with an endemo-sporadic pattern until the beginning of 2007, when it disappeared. However, in spite of this, chronic rheumatic symptoms have persisted in many previously infected individuals. Rheumatic manifestations of CHIKV infection typically consist of a febrile arthritis principally affecting the extremities (ankles, wrists, phalanges), although many others joints may be affected [21]. Patients adopt a characteristic stooped walking position which is the hallmark of the disease and from which it derives its name Chikungunya, meaning “he who walks bent over”, in the Kimakonde language of Mozambique. Although the acute rheumatic involvement is well-documented, less is known about the potential long-term clinical and functional outcome [21]–[23]. To our knowledge, the most detailed description is a cohort of 107 patients by Brighton et al. [24], which indicated that nearly 12% of patients had not fully recovered three years after initial infection. In that study, determinants of the course of rheumatic manifestations of CHIKV infections were not investigated. In order to address these shortcomings, we have evaluated rheumatic complications in a cohort of Reunion Island residents infected with CHIKV during the initial phase of the epidemic in 2005. The study had two objectives, firstly to assess the course of rheumatic manifestations over the 15 months following the acute illness, and secondly to investigate potential risk factors associated"
}