{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001495",
  "doi": "10.1371/journal.pntd.0001495",
  "externalIds": [
    "pii:PNTD-D-11-01175"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Control Using Genetically Modified Insects Poses Problems for Regulators",
  "authors": [
    {
      "name": "Michael J. Lehane",
      "first": "Michael J.",
      "last": "Lehane",
      "affiliation": "Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Serap Aksoy",
      "first": "Serap",
      "last": "Aksoy",
      "affiliation": "Yale School of Public Health, New Haven, Connecticut, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-01",
  "volume": "6",
  "number": "1",
  "pages": "e1495",
  "tags": [
    "Agricultural biotechnology",
    "Agriculture",
    "Biology",
    "Biotechnology",
    "Genetic engineering",
    "Political science",
    "Science Policy",
    "Science policy",
    "Social and behavioral sciences",
    "Technology regulations"
  ],
  "fullText": "Insects are the pre-eminent form of metazoan life on land, with as many as 1018 individuals alive at any one instant and over three-quarters of a million species described. Although it is estimated that there are as many as 14,000 species that are blood feeders [1], only three to 400 species regularly attract our attention [2]. Some of these are of immense importance to us, as vector-borne diseases still form a huge burden on both the human population (Table 1) and our domesticated animals. Much progress has been achieved in the control of some of these vector-borne diseases by targeting the vector. The following are two good examples. First, insecticide-treated mosquito nets (ITNs) have had a major impact in the control of malaria, even in some of the most difficult control settings. The evidence from large-scale assessments shows that households possessing ITNs show a 20% reduction in prevalence of Plasmodium falciparum infection in children under 5 and a 23% reduction in all-cause child mortality, findings that were consistent across a range of transmission settings [3]. Second, the Southern Cone Initiative has used indoor residual spraying against the domesticated triatomine vectors of Chagas disease to immense effect [4]. As a result, the overall distribution of Triatoma infestans in the Southern Cone region has been reduced from well over 6 million km2 (1990 estimates) to around 750,000 km2 mainly in the Chaco of northeast Argentina and Bolivia, while Rhodnius prolixus has been almost entirely eliminated from Central America, with all countries there now certified by the World Health Organization (WHO) and Pan American Health Organization (PAHO) as free of transmission due to this vector. However, the emergence and spread of insecticide resistance [5] represents a challenge to these successes and to other vector control activities, the vast majority of which depend in one way or another on the use of insecticides. The need for new insecticides (or novel means to use those we already have) and for other non-insecticidal means of vector control is quite clear. A good example of our need for new means of controlling insects is seen in dengue. Without a vaccine or drugs, disease control efforts are centred on control of the vector. But, because of the life histories of the vectors involved, the methods we currently have are inadequate [6]. One non-insecticidal method of vector control, which incidentally shows much promise for dengue control, is the use of genetically modified (GM) insects. Serious discussion of whether GM insects could be used in control began as soon as transgenic insects were first produced in the 1980s [7], and a range of means by which this could be achieved have been put forward [8]. The first generation of GM insects, designed to suppress rather than replace vector populations, is now being produced. For example, the OX3604C strain of Aedes aegypti is designed for the control of this dengue vector [9]. Field release of GM insects is under way [10], [11], as described by Reeves and colleagues in this issue"
}