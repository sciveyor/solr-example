{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000688",
  "doi": "10.1371/journal.pntd.0000688",
  "externalIds": [
    "pii:10-PNTD-RA-0791R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Chagas Cardiomyopathy in the Context of the Chronic Disease Transition",
  "authors": [
    {
      "name": "Alicia I. Hidron",
      "first": "Alicia I.",
      "last": "Hidron",
      "affiliation": "Division of Infectious Diseases, Department of Medicine, Emory University School of Medicine, Atlanta, Georgia, United States of America"
    },
    {
      "name": "Robert H. Gilman",
      "first": "Robert H.",
      "last": "Gilman",
      "affiliation": "Asociacion Benefica PRISMA, Lima, Peru; Facultad de Ciencias y Filosofía, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Juan Justiniano",
      "first": "Juan",
      "last": "Justiniano",
      "affiliation": "Hospital Universitario Japones, Santa Cruz de la Sierra, Bolivia"
    },
    {
      "name": "Anna J. Blackstock",
      "first": "Anna J.",
      "last": "Blackstock",
      "affiliation": "Division of Parasitic Diseases, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America; Atlanta Research and Education Foundation, Decatur, Georgia, United States of America"
    },
    {
      "name": "Carlos LaFuente",
      "first": "Carlos",
      "last": "LaFuente",
      "affiliation": "Hospital Universitario Japones, Santa Cruz de la Sierra, Bolivia"
    },
    {
      "name": "Walter Selum",
      "first": "Walter",
      "last": "Selum",
      "affiliation": "Hospital Universitario Japones, Santa Cruz de la Sierra, Bolivia"
    },
    {
      "name": "Martiza Calderon",
      "first": "Martiza",
      "last": "Calderon",
      "affiliation": "Facultad de Ciencias y Filosofía, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Manuela Verastegui",
      "first": "Manuela",
      "last": "Verastegui",
      "affiliation": "Facultad de Ciencias y Filosofía, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Lisbeth Ferrufino",
      "first": "Lisbeth",
      "last": "Ferrufino",
      "affiliation": "Hospital Universitario Japones, Santa Cruz de la Sierra, Bolivia"
    },
    {
      "name": "Eduardo Valencia",
      "first": "Eduardo",
      "last": "Valencia",
      "affiliation": "Facultad de Ciencias y Filosofía, Universidad Peruana Cayetano Heredia, Lima, Peru"
    },
    {
      "name": "Jeffrey A. Tornheim",
      "first": "Jeffrey A.",
      "last": "Tornheim",
      "affiliation": "Mount Sinai School of Medicine, New York, New York, United States of America"
    },
    {
      "name": "Seth O'Neal",
      "first": "Seth",
      "last": "O'Neal",
      "affiliation": "Oregon Health Sciences University, Portland, Oregon, United States of America"
    },
    {
      "name": "Robert Comer",
      "first": "Robert",
      "last": "Comer",
      "affiliation": "Wake Forest University Health Sciences, Winston-Salem, North Carolina, United States of America"
    },
    {
      "name": "Gerson Galdos-Cardenas",
      "first": "Gerson",
      "last": "Galdos-Cardenas",
      "affiliation": "Asociacion Benefica PRISMA, Lima, Peru"
    },
    {
      "name": "Caryn Bern",
      "first": "Caryn",
      "last": "Bern",
      "affiliation": "Division of Parasitic Diseases, Centers for Disease Control and Prevention, Atlanta, Georgia, United States of America"
    },
    {
      "name": "for the Chagas Disease Working Group in Peru and Bolivia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-05",
  "dateAccepted": "2010-03-29",
  "dateReceived": "2010-01-05",
  "volume": "4",
  "number": "5",
  "pages": "e688",
  "tags": [
    "Cardiovascular Disorders"
  ],
  "abstract": "Background\n\nPatients with Chagas disease have migrated to cities, where obesity, hypertension and other cardiac risk factors are common.\n\nMethodology/Principal Findings\n\nThe study included adult patients evaluated by the cardiology service in a public hospital in Santa Cruz, Bolivia. Data included risk factors for T. cruzi infection, medical history, physical examination, electrocardiogram, echocardiogram, and contact 9 months after initial data collection to ascertain mortality. Serology and PCR for Trypanosoma cruzi were performed. Of 394 participants, 251 (64%) had confirmed T. cruzi infection by serology. Among seropositive participants, 109 (43%) had positive results by conventional PCR; of these, 89 (82%) also had positive results by real time PCR. There was a high prevalence of hypertension (64%) and overweight (body mass index [BMI] &gt;25; 67%), with no difference by T. cruzi infection status. Nearly 60% of symptomatic congestive heart failure was attributed to Chagas cardiomyopathy; mortality was also higher for seropositive than seronegative patients (p = 0.05). In multivariable models, longer residence in an endemic province, residence in a rural area and poor housing conditions were associated with T. cruzi infection. Male sex, increasing age and poor housing were independent predictors of Chagas cardiomyopathy severity. Males and participants with BMI ≤25 had significantly higher likelihood of positive PCR results compared to females or overweight participants.\n\nConclusions\n\nChagas cardiomyopathy remains an important cause of congestive heart failure in this hospital population, and should be evaluated in the context of the epidemiological transition that has increased risk of obesity, hypertension and chronic cardiovascular disease.",
  "fullText": "Introduction Chagas disease, caused by the parasite Trypanosoma cruzi, affects an estimated 8 million people in the Americas [1]. Transmission occurs via inoculation of T. cruzi-infected feces of the triatomine vector through the bite wound or mucosal surfaces. Infection may also be acquired congenitally, through blood transfusion, organ transplant or through consumption of contaminated food or drink. The acute phase is associated with high level parasitemia, and mild, non-specific symptoms in the majority of individuals [2]. After 8–12 weeks, infected individuals pass into the chronic phase, in which parasites are no longer detectable by peripheral blood microscopy and diagnosis relies on demonstration of anti-T. cruzi antibodies. Infection is life-long in the absence of successful treatment. Over a period of decades, 20–30% of infected individuals develop specific patterns of end-organ damage. The most common form, chronic Chagas cardiomyopathy, is characterized by conduction system abnormalities, brady- and tachyarrhythmias, dilated cardiomyopathy, apical aneurysm, and thrombus formation in the aneurysm or enlarged left ventricle [3]. Patients with Chagas heart disease have a high rate of mortality from ventricular arrhythmias, pulmonary or cerebral emboli, and intractable congestive heart failure [3]. Historically, T. cruzi transmission occurred predominantly in rural areas of Latin America where poor housing conditions promoted vector infestation. Since 1991, Chagas disease control programs have made striking progress in decreasing vector- and blood-borne T. cruzi transmission, leading to dramatic declines in infection prevalence among children [4], [5]. However, millions of T.cruzi-infected adults remain, and massive population movements over the past 3 decades have brought many of these individuals to cities across Latin America. Urban populations are in transition from an epidemiology of predominantly infectious diseases to patterns similar to industrialized countries where obesity, hypertension, diabetes and atherosclerosis are the leading causes of illness and death [6], [7]. Adults infected with T. cruzi as children form a transitional generation, experiencing the simultaneous impact of past infectious exposures and current cardiovascular risk factors. Bolivia has the highest prevalence of T. cruzi infection in the world, estimated at 6% of the national population, and reaching 30–40% in surveys of pregnant women, blood donors or endemic community members [1], [8], [9], [10]. The major objective of this study was to assess T. cruzi cardiac morbidity and its coincidence with common cardiovascular risk factors and disease among patients attending a large urban public hospital. In addition, we explored risk factors for T. cruzi infection and disease severity, and clinical and epidemiological associations with positive results by T. cruzi PCR. Methods Ethics statement The protocol was approved by the institutional review boards of the study hospital, Asociación Benéfica PRISMA, and the Centers for Disease Control and Prevention. Study site and patient population The study was conducted in the Hospital Universitario Japonés in Santa Cruz, Bolivia from August 25 to November 13, 2008. The hospital is one of two public hospitals and serves approximately 60% of the city's uninsured population. Although the city of Santa Cruz does not have vector-borne T. cruzi transmission, infection prevalence is high because many residents migrated"
}