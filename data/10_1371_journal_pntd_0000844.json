{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000844",
  "doi": "10.1371/journal.pntd.0000844",
  "externalIds": [
    "pii:10-PNTD-RA-0847R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Systemic FasL and TRAIL Neutralisation Reduce Leishmaniasis Induced Skin Ulceration",
  "authors": [
    {
      "name": "Geremew Tasew",
      "first": "Geremew",
      "last": "Tasew",
      "affiliation": "Ethiopian Health and Nutrition Research Institute (EHNRI), Parasitology Laboratory, Addis Ababa, Ethiopia; Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Susanne Nylén",
      "first": "Susanne",
      "last": "Nylén",
      "affiliation": "Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Thorsten Lieke",
      "first": "Thorsten",
      "last": "Lieke",
      "affiliation": "Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden; Transplantationslabor, Klinik für Viszeral- und Transplantationschirurgie, Medizinische Hochschule Hannover, Hannover, Germany"
    },
    {
      "name": "Befekadu Lemu",
      "first": "Befekadu",
      "last": "Lemu",
      "affiliation": "St. Paulos General Specialized Hospital, Addis Ababa, Ethiopia"
    },
    {
      "name": "Hailu Meless",
      "first": "Hailu",
      "last": "Meless",
      "affiliation": "Ethiopian Health and Nutrition Research Institute (EHNRI), Parasitology Laboratory, Addis Ababa, Ethiopia"
    },
    {
      "name": "Nicolas Ruffin",
      "first": "Nicolas",
      "last": "Ruffin",
      "affiliation": "Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Dawit Wolday",
      "first": "Dawit",
      "last": "Wolday",
      "affiliation": "Ethiopian Health and Nutrition Research Institute (EHNRI), Parasitology Laboratory, Addis Ababa, Ethiopia"
    },
    {
      "name": "Abraham Asseffa",
      "first": "Abraham",
      "last": "Asseffa",
      "affiliation": "Armauer Hansen Research Institute (AHRI), Addis Ababa, Ethiopia"
    },
    {
      "name": "Hideo Yagita",
      "first": "Hideo",
      "last": "Yagita",
      "affiliation": "Department of Immunology, Juntendo University School of Medicine, Tokyo, Japan"
    },
    {
      "name": "Sven Britton",
      "first": "Sven",
      "last": "Britton",
      "affiliation": "Unit of Infectious Diseases, Department of Medicine Solna, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Hannah Akuffo",
      "first": "Hannah",
      "last": "Akuffo",
      "affiliation": "Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Francesca Chiodi",
      "first": "Francesca",
      "last": "Chiodi",
      "affiliation": "Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden"
    },
    {
      "name": "Liv Eidsmo",
      "first": "Liv",
      "last": "Eidsmo",
      "affiliation": "Department of Microbiology, Tumour and Cell Biology, Karolinska Institutet, Stockholm, Sweden; Unit of Dermatology and Venerology, Department of Medicine Solna, Karolinska Institutet, Stockholm, Sweden"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-10",
  "dateAccepted": "2010-09-09",
  "dateReceived": "2010-01-22",
  "volume": "4",
  "number": "10",
  "pages": "e844",
  "tags": [
    "Dermatology/Skin Infections",
    "Immunology/Immunomodulation",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Infectious Diseases/Skin Infections",
    "Microbiology/Parasitology"
  ],
  "abstract": "Cutaneous leishmaniasis (CL) is caused by Leishmania infection of dermal macrophages and is associated with chronic inflammation of the skin. L. aethiopica infection displays two clinical manifestations, firstly ulcerative disease, correlated to a relatively low parasite load in the skin, and secondly non-ulcerative disease in which massive parasite infiltration of the dermis occurs in the absence of ulceration of epidermis. Skin ulceration is linked to a vigorous local inflammatory response within the skin towards infected macrophages. Fas ligand (FasL) and Tumor necrosis factor-related apoptosis-inducing ligand (TRAIL) expressing cells are present in dermis in ulcerative CL and both death ligands cause apoptosis of keratinocytes in the context of Leishmania infection. In the present report we show a differential expression of FasL and TRAIL in ulcerative and non-ulcerative disease caused by L. aethiopica. In vitro experiments confirmed direct FasL- and TRAIL-induced killing of human keratinocytes in the context of Leishmania-induced inflammatory microenvironment. Systemic neutralisation of FasL and TRAIL reduced ulceration in a model of murine Leishmania infection with no effect on parasitic loads or dissemination. Interestingly, FasL neutralisation reduced neutrophil infiltration into the skin during established infection, suggesting an additional proinflammatory role of FasL in addition to direct keratinocyte killing in the context of parasite-induced skin inflammation. FasL signalling resulting in recruitment of activated neutrophils into dermis may lead to destruction of the basal membrane and thus allow direct FasL mediated killing of exposed keratinocytes in vivo. Based on our results we suggest that therapeutic inhibition of FasL and TRAIL could limit skin pathology during CL.",
  "fullText": "Introduction Leishmaniasis is a group of parasitic diseases associated with heterogeneous clinical manifestations. Symptoms range from lethal disease with overwhelming infection of the bone-marrow, spleen and liver to localised self-healing ulcers of the skin. Leishmania aethiopica is the main causative agent of CL in the highlands of Ethiopia. Upon infection, parasites reside and replicate within tissue macrophages during an initial silent phase of the infection and the clinical presentation of CL is mainly associated with the infiltration of circulating inflammatory cells into infected tissues. L. aethiopica infection leads to localised cutaneous leishmaniasis (LCL) or diffuse cutaneous leishmaniasis (DCL). LCL is characterised by erosive ulcers and a strong T cell mediated response [1] which typically results in spontaneous healing within a year, scar formation and solid protection against re-infection [2]. In contrast, DCL is linked to non-ulcerative chronic nodular disease with abundant parasitic infiltration of the dermal compartment of the skin and antigen specific T cell unresponsiveness [3], [4]. Structural differences [5] as well as different immunogenic properties [4], [6] between LCL and DCL causing parasites have been reported. The mechanisms of tissue destruction during ulcerative cutaneous leishmaniasis have not been fully clarified. We have previously reported that dermal FasL and TRAIL expressing cells are present in ulcerative L. major infection and that the number of FasL expressing dermal cells correlate to the level of epidermal apoptosis. Furthermore, in vitro experiments propose FasL and TRAIL as major players inducing apoptosis in keratinocytes during Leishmania induced inflammation [7], [8]. In the present study expression of FasL and TRAIL within the skin was investigated in ulcerative and non-ulcerative manifestations of L. aethiopica induced CL. More FasL and TRAIL expressing cells were detected in ulcerative self-healing LCL as compared to non-ulcerative chronic DCL. In line with these results, neutralisation of FasL and TRAIL in vivo during experimental leishmaniasis in BALB/c mice led to reduction of ulceration and was not associated with increased infective loads or increased spread of the infection through the lymphatics. Materials and Methods Ethical statement This study was conducted according to the principles expressed in the Declaration of Helsinki. The study was approved by the Institutional Ethical Review Board of Karolinska Institutet (reference number 31-5427/08) and by The National Ethical Clearance Committtee (NECC) at the Ethiopia Science and Technology Commission (reference number: RDHE/78-43/2002). All patients provided written informed consent for the collection of samples and subsequent analysis. All animals were handled in strict accordance with good animal practice as defined by the relevant national animal welfare bodies, and this study was approved by the Regional Animal Studies Ethical Committee, Stockholm North, Sweden (reference number N72/05 and 305/08). Patient material Skin biopsies were collected from healthy controls at St.Paulos General, Specialized Hospital, Addis Ababa, Ethiopia, and from Ethiopian CL patients at the Armauer Hansen Research Institute (AHRI), Addis Ababa, Ethiopia. Immunohistochemistry of skin biopsies FasL and TRAIL were visualised in formalin fixed tissue as previously described [7], [8] and evaluated in Leica fluorescent microscope. Photomicrographs were obtained using a Zeiss Axioskope 2,"
}