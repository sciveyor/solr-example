{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000587",
  "doi": "10.1371/journal.pntd.0000587",
  "externalIds": [
    "pii:09-PNTD-RA-0465R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Effect of Village-wide Use of Long-Lasting Insecticidal Nets on Visceral Leishmaniasis Vectors in India and Nepal: A Cluster Randomized Trial",
  "authors": [
    {
      "name": "Albert Picado",
      "first": "Albert",
      "last": "Picado",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "Murari L. Das",
      "first": "Murari L.",
      "last": "Das",
      "affiliation": "B. P. Koirala Institute of Health Sciences, Dharan, Nepal"
    },
    {
      "name": "Vijay Kumar",
      "first": "Vijay",
      "last": "Kumar",
      "affiliation": "Rajendra Memorial Research Institute of Medical Sciences, Patna, India"
    },
    {
      "name": "Shreekant Kesari",
      "first": "Shreekant",
      "last": "Kesari",
      "affiliation": "Rajendra Memorial Research Institute of Medical Sciences, Patna, India"
    },
    {
      "name": "Diwakar S. Dinesh",
      "first": "Diwakar S.",
      "last": "Dinesh",
      "affiliation": "Rajendra Memorial Research Institute of Medical Sciences, Patna, India"
    },
    {
      "name": "Lalita Roy",
      "first": "Lalita",
      "last": "Roy",
      "affiliation": "B. P. Koirala Institute of Health Sciences, Dharan, Nepal"
    },
    {
      "name": "Suman Rijal",
      "first": "Suman",
      "last": "Rijal",
      "affiliation": "B. P. Koirala Institute of Health Sciences, Dharan, Nepal"
    },
    {
      "name": "Pradeep Das",
      "first": "Pradeep",
      "last": "Das",
      "affiliation": "Rajendra Memorial Research Institute of Medical Sciences, Patna, India"
    },
    {
      "name": "Mark Rowland",
      "first": "Mark",
      "last": "Rowland",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    },
    {
      "name": "Shyam Sundar",
      "first": "Shyam",
      "last": "Sundar",
      "affiliation": "Banaras Hindu University, Varanasi, India"
    },
    {
      "name": "Marc Coosemans",
      "first": "Marc",
      "last": "Coosemans",
      "affiliation": "Prince Leopold Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Marleen Boelaert",
      "first": "Marleen",
      "last": "Boelaert",
      "affiliation": "Prince Leopold Institute of Tropical Medicine, Antwerp, Belgium"
    },
    {
      "name": "Clive R. Davies",
      "first": "Clive R.",
      "last": "Davies",
      "affiliation": "London School of Hygiene and Tropical Medicine, London, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-01",
  "dateAccepted": "2009-12-07",
  "dateReceived": "2009-09-08",
  "volume": "4",
  "number": "1",
  "pages": "e587",
  "tags": [
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases"
  ],
  "abstract": "Background\n\nVisceral leishmaniasis (VL) control in the Indian subcontinent is currently based on case detection and treatment, and on vector control using indoor residual spraying (IRS). The use of long-lasting insecticidal nets (LN) has been postulated as an alternative or complement to IRS. Here we tested the impact of comprehensive distribution of LN on the density of Phlebotomus argentipes in VL-endemic villages.\n\nMethods\n\nA cluster-randomized controlled trial with household P. argentipes density as outcome was designed. Twelve clusters from an ongoing LN clinical trial—three intervention and three control clusters in both India and Nepal—were selected on the basis of accessibility and VL incidence. Ten houses per cluster selected on the basis of high pre-intervention P. argentipes density were monitored monthly for 12 months after distribution of LN using CDC light traps (LT) and mouth aspiration methods. Ten cattle sheds per cluster were also monitored by aspiration.\n\nFindings\n\nA random effect linear regression model showed that the cluster-wide distribution of LNs significantly reduced the P. argentipes density/house by 24.9% (95% CI 1.80%–42.5%) as measured by means of LTs.\n\nInterpretation\n\nThe ongoing clinical trial, designed to measure the impact of LNs on VL incidence, will confirm whether LNs should be adopted as a control strategy in the regional VL elimination programs. The entomological evidence described here provides some evidence that LNs could be usefully deployed as part of the VL control program.\n\nTrial registration\n\nClinicalTrials.gov CT-2005-015374",
  "fullText": "Introduction Visceral leishmaniasis (VL), also known as kala azar, is a life-threatening vector-borne disease with a fatal outcome if left untreated. A large proportion of the 500,000 annual cases and 60,000 deaths occur in the poor rural communities within the Indian subcontinent [1]. In this region, VL is caused by Leishmania donovani and is transmitted by the sand fly Phlebotomus argentipes [2]. Elimination of VL in the region is deemed feasible because humans are the only known reservoir and VL is confined to contiguous areas within the Indian subcontinent. In the absence of a human leishmaniasis vaccine, the elimination program proposes to reduce VL incidence by active case detection and treatment, and the widespread use of residual insecticide spraying (IRS) of houses and cattle sheds with DDT in India and with pyrethroids in Nepal and Bangladesh. Public health policy makers in the region are aware that long-term house-spraying campaigns against VL vectors may also be difficult to sustain [3]–[5]. Hence, it has been suggested that, as for malaria control in India, it may be more efficient to provide insecticide treated nets (ITNs) to the population at risk for VL [5]. Given the well known difficulties in maintaining high re-impregnation rates of ITNs, malaria vector control programs are now implementing WHOPES-recommended long-lasting insecticidal nets (LNs), such as PermaNet 2.0 and Olyset. There is no clear evidence that use of ITNs would reduce VL incidence in the Indian subcontinent. However, circumstantial evidence drawn from P. argentipes behavioral studies provides hope that ITN will work. In particular P. argentipes is relatively endophagic [6] and indoor biting rhythms peaks during the middle of the night when people are in bed [6]–[8]. Evidence that village-wide use of ITNs can protect against other forms of leishmaniasis transmitted by a variety of sand fly species come from several small scale trials in Iran [9]–[11], Sudan [12], Syria [13] and Turkey [14] and two large scale trials in Iran [15] and Syria [16]. There is also encouraging circumstantial evidence from a retrospective evaluation of a large ITN VL control program in Sudan [17]. Studies on malaria vector control have provided evidence that ITNs, not only provide personal protection to the user, but can, in certain situations, such as when coverage is high enough, provide protection to the entire community, non users included, through the reduction of vector population density [18]–[20]. Until now there has been no reported evidence that community-wide use of ITNs can generate a similar effect against any form of leishmaniasis infection. Most entomological studies, such as the trials in Iran [9]–[11], Syria [13] and Turkey [14], have singularly failed to demonstrate any effect on sand fly density through community-wide use of ITNs. The only evidence we are aware of is an Iranian trial which detected a reduction in sand fly (P. sergenti) density in the ITN villages [15] and a Sudanese trial in which infections caused by P. orientalis may have been reduced in villages using ITNs although the results were non-statistically significant [21]. It"
}