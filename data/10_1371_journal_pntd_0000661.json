{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000661",
  "doi": "10.1371/journal.pntd.0000661",
  "externalIds": [
    "pii:09-PNTD-RA-0673R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Presence of Circulating Anti-Myosin Antibodies in Endomyocardial Fibrosis",
  "authors": [
    {
      "name": "Ana Olga Mocumbi",
      "first": "Ana Olga",
      "last": "Mocumbi",
      "affiliation": "Instituto do Coração, Maputo, Mozambique; Heart Science Centre, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Najma Latif",
      "first": "Najma",
      "last": "Latif",
      "affiliation": "Heart Science Centre, Imperial College London, London, United Kingdom"
    },
    {
      "name": "Magdi H. Yacoub",
      "first": "Magdi H.",
      "last": "Yacoub",
      "affiliation": "Instituto do Coração, Maputo, Mozambique; Magdi Yacoub Institute, London, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-04",
  "dateAccepted": "2010-03-04",
  "dateReceived": "2009-11-30",
  "volume": "4",
  "number": "4",
  "pages": "e661",
  "tags": [
    "Cardiovascular Disorders/Heart Failure",
    "Immunology/Autoimmunity"
  ],
  "abstract": "Background\n\nEndomyocardial Fibrosis (EMF) is a tropical restrictive cardiomyopathy of unknown etiology with high prevalence in Sub-Saharan Africa, for which it is unclear whether the primary target of injury is the endocardial endothelium, the subendocardial fibroblast, the coronary microcirculation or the myocyte. In an attempt to explore the possibility of endocardial lesions being a result of an immune response against the myocyte we assessed the presence and frequency of circulating anti-myocardial antibodies in EMF patients.\n\nMethodology/Principal Findings\n\nEMF classification, assessment of severity and staging was based on echocardiography. We used sodium dodecylsulfate polyacrylamide gel electrophoresis (SDS-PAGE) of myocardial proteins followed by western blotting to screen serum samples for antiheart antibodies G and M classes. The degree of serum reactivity was correlated with the severity and activity of EMF. We studied 56 EMF patients and 10 healthy controls. IgG reactivity against myocardial proteins was stronger and more frequent in patients with EMF when compared to controls (30/56; 53.6% vs. 1/10; 10%, respectively). IgM reactivity was weak in both groups, although higher in EMF patients (11/56; 19.6%) when compared to controls (n = 0). EMF patients showed greater frequency and reactivity of IgG antibodies against myocardial proteins of molecular weights 35 kD, 42 kD and 70 kD (p values &lt;0.01, &lt;0.01 and &lt;0.05 respectively).\n\nConclusions\n\nThe presence of antibodies against myocardial proteins was demonstrated in a subset of EMF patients. These immune markers seem to be related with activity and might provide an adjunct tool for diagnosis and classification of EMF, therefore improving its management by identifying patients who may benefit from immunosuppressive therapy. Further research is needed to clarify the role of autoimmunity in the pathogenesis of EMF.",
  "fullText": "Introduction Endomyocardial Fibrosis (EMF) is a tropical cardiomyopathy of unclear etiopathogenesis and poor prognosis, which is endemic in certain regions of sub-Saharan Africa [1]. It is probably the commonest form of restrictive cardiomyopathy, affecting primarily children and adolescents. The distinctive pathological feature of established EMF is endocardial thickening of one or both ventricles, more prominent at the apices and the inflow tracts, usually causing dysfunction of the atrioventricular valve [1], [2]. The diagnosis of EMF is usually made in late stages of the disease, when heart failure or its complications are already present, and is based on clinical and echocardiographic features. Although hypereosinophilia is a common finding in African patients, no biological marker is currently available for early detection. Medical management of EMF aims at controlling episodes of heart failure and its complications, as well as treating hypereosinophilia using oral corticosteroids [2], [3]. Surgery is recommended to symptomatic patients since it increases survival [4] and improves the quality of life, but has been associated with high morbidity and mortality [5], and has progressed slowly due to lack of facilities for open-heart surgery in most regions where the disease is endemic. The primary target of injury in EMF is not known. It has been suggested that the endomyocardial lesions may be the result of a primary injury to the endocardial endothelium, subendocardial fibroblast, coronary microcirculation or myocytes [3]. In an attempt to explore the possibility of endocardial lesions being a result of an autoimmune response against the myocytes we assessed the presence and frequency of circulating IgM and IgG class anti-myocardial antibodies in different forms and stages of the disease. Methods Serum was obtained from 56 consecutive EMF patients from the Mozambican clinical registry and 10 blood donors from the same population. All controls were submitted to transthoracic echocardiography to rule out the presence of cardiac disease. Ethics statement The National Bioethical Committee for Health from Mozambique approved the study protocol. Written informed consent was obtained from all patients and controls. Protocol for clinical evaluation of patients EMF diagnosis was based on the demonstration of mural and/or valvar endocardial thickening and other echocardiographic features of EMF described elsewhere [2]. The disease was defined as right (REFM), left (LEMF) and bilateral (BEMF) according to the predominance of structural lesions in one or both sides of the heart. The severity of endocardial lesions was determined using a standardized scoring system that defines four different grades, namely mild (I), moderate (II), severe (III) and advanced (IV) [6]. Finally, activity was defined based on the presence of clinical sings such as fever, periorbital edema, urticaria, recrudescence of heart failure, and laboratory findings of increased erythrosedimentation rate and severe hypereosinophilia (absolute eosinophil count &gt;1.5×109/L); in the absence of any of those clinical sings for more than 6 months patients were considered to have remission (quiescent disease) [7]. SDS-PAGE and Western blotting Normal ventricular myocardium was obtained from a donor heart and immediately frozen in liquid nitrogen. The myocardial samples were pulverized while still frozen and homogenized"
}