{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000712",
  "doi": "10.1371/journal.pntd.0000712",
  "externalIds": [
    "pii:09-PNTD-RA-0746R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Public Domain declaration which stipulates that, once placed in the public domain, this work may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Immunity to Lutzomyia intermedia Saliva Modulates the Inflammatory Environment Induced by Leishmania braziliensis",
  "authors": [
    {
      "name": "Tatiana R. de Moura",
      "first": "Tatiana R.",
      "last": "de Moura",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil"
    },
    {
      "name": "Fabiano Oliveira",
      "first": "Fabiano",
      "last": "Oliveira",
      "affiliation": "Vector Molecular Biology Unit, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Gabriele C. Rodrigues",
      "first": "Gabriele C.",
      "last": "Rodrigues",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil"
    },
    {
      "name": "Marcia W. Carneiro",
      "first": "Marcia W.",
      "last": "Carneiro",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil"
    },
    {
      "name": "Kiyoshi F. Fukutani",
      "first": "Kiyoshi F.",
      "last": "Fukutani",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil"
    },
    {
      "name": "Fernanda O. Novais",
      "first": "Fernanda O.",
      "last": "Novais",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil"
    },
    {
      "name": "José Carlos Miranda",
      "first": "José Carlos",
      "last": "Miranda",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil"
    },
    {
      "name": "Manoel Barral-Netto",
      "first": "Manoel",
      "last": "Barral-Netto",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil; Universidade Federal da Bahia, Salvador, Brazil; Instituto Nacional de Ciência e Tecnologia (INCT) de Investigação em Imunologia, São Paulo, Brazil"
    },
    {
      "name": "Claudia Brodskyn",
      "first": "Claudia",
      "last": "Brodskyn",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil; Universidade Federal da Bahia, Salvador, Brazil; Instituto Nacional de Ciência e Tecnologia (INCT) de Investigação em Imunologia, São Paulo, Brazil"
    },
    {
      "name": "Aldina Barral",
      "first": "Aldina",
      "last": "Barral",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil; Universidade Federal da Bahia, Salvador, Brazil; Instituto Nacional de Ciência e Tecnologia (INCT) de Investigação em Imunologia, São Paulo, Brazil"
    },
    {
      "name": "Camila I. de Oliveira",
      "first": "Camila I.",
      "last": "de Oliveira",
      "affiliation": "Centro de Pesquisas Gonçalo Moniz, Fundação Oswaldo Cruz (FIOCRUZ), Salvador, Brazil; Instituto Nacional de Ciência e Tecnologia (INCT) de Investigação em Imunologia, São Paulo, Brazil"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-06",
  "dateAccepted": "2010-04-22",
  "dateReceived": "2009-12-22",
  "volume": "4",
  "number": "6",
  "pages": "e712",
  "tags": [
    "Immunology/Immune Response",
    "Immunology/Immunity to Infections",
    "Immunology/Immunomodulation"
  ],
  "abstract": "Background\n            \nDuring blood feeding, sand flies inject Leishmania parasites in the presence of saliva. The types and functions of cells present at the first host-parasite contact are critical to the outcome on infection and sand fly saliva has been shown to play an important role in this setting. Herein, we investigated the in vivo chemotactic effects of Lutzomyia intermedia saliva, the vector of Leishmania braziliensis, combined or not with the parasite.\n\n            Methods and Findings\n            \nWe tested the initial response induced by Lutzomyia intermedia salivary gland sonicate (SGS) in BALB/c mice employing the air pouch model of inflammation. L. intermedia SGS induced a rapid influx of macrophages and neutrophils. In mice that were pre-sensitized with L. intermedia saliva, injection of SGS was associated with increased neutrophil recruitment and a significant up-regulation of CXCL1, CCL2, CCL4 and TNF-α expression. Surprisingly, in mice that were pre-exposed to SGS, a combination of SGS and L. braziliensis induced a significant migration of neutrophils and an important modulation in cytokine and chemokine expression as shown by decreased CXCL10 expression and increased IL-10 expression.\n\n            Conclusion\n            \nThese results confirm that sand fly saliva modulates the initial host response. More importantly, pre-exposure to L. intermedia saliva significantly modifies the host's response to L. braziliensis, in terms of cellular recruitment and expression of cytokines and chemokines. This particular immune modulation may, in turn, favor parasite multiplication.",
  "fullText": "Introduction The intracellular protozoan parasites of the Leishmania species are transmitted to vertebrate host through the bites of sand flies. Within the vertebrate host, Leishmania parasites reside in phagocytes and induce a spectrum of diseases ranging from a single self-healing cutaneous lesion to the lethal visceral form. It is currently estimated that leishmaniasis affects two million people per year worldwide [1]. Leishmania braziliensis, the main causative agent of cutaneous leishmaniasis (CL) in Brazil, can be transmitted to the human host by the bite of the sand fly Lutzomyia intermedia. [2], [3]. Several studies have shown that pre-exposure to saliva or to bites from uninfected sand flies results in protection against subsequent infection with Leishmania major [4]–[7], Leishmania. amazonensis [8], and Leishmania chagasi [9]. On the contrary, pre-exposure to Lutzomyia intermedia saliva enhanced infection with L. braziliensis in the mouse model; disease exacerbation was correlated with generation of a Th2 response evidenced by a reduction in the IFN-γ/IL-4 ratio [10]. Importantly, individuals with active CL showed higher humoral immune responses to L. intermedia saliva compared with control subjects, a finding also demonstrated with Old World CL [11] . These data indicate an association between disease and immune response to L. intermedia saliva in humans. In the case of L. intermedia, the lack of protection observed following pre-exposure to saliva in the murine model may be related to differences in the initial inflammatory response induced by the salivary proteins. Several studies have shown the potential of salivary antigens from Lutzomyia longipalpis, Phlebotomus duboscqi, Phlebotomus papatasi and Phlebotomus ariasi to modulate cell recruitment and production of immune response mediators [12]–[17] however, little is known regarding these effects when using L. intermedia saliva. Our group has previously shown that pre-treatment of human monocytes with L. intermedia followed by L. braziliensis infection led to a significant increase in TNF-α, IL-6, and IL-8 production [18], indicating the ability of L. intermedia saliva to alter the inflammatory milieu. To gain further information regarding the events associated with the initial host response to L. intermedia saliva, we employed the air pouch model of inflammation. This model simulates inoculation of the sand fly in a closed environment and allows for subsequent analysis of inflammatory parameters and mediators induced in vivo by distinct stimuli [19]. Using this model, we showed that saliva from L. longipalpis rapidly induced CCL2 expression and macrophage recruitment, in synergy with L. chagasi parasites, in BALB/c mice [20]. Here we describe the ability of L. intermedia salivary gland sonicate (SGS) to modulate the host immune response in naïve and in SGS-sensitized mice. We have demonstrated that L. intermedia salivary proteins induce neutrophil recruitment and modulate cytokine and chemokine expression. Crucially, a downregulation in CXCL10 paralleled by an increase in IL-10 expression was observed in SGS-sensitized mice stimulated with saliva+L. braziliensis. This correlates with disease exacerbation previously observed in mice immune to L. intermedia SGS and challenged with L. braziliensis [10]. Methods Parasite culture Leishmania braziliensis promastigotes (strain MHOM/BR/01/BA788 [21]) were grown in Schneider medium (Sigma Chemical"
}