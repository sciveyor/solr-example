{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000435",
  "doi": "10.1371/journal.pntd.0000435",
  "externalIds": [
    "pii:09-PNTD-RA-0056R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Prospects for Developing Odour Baits To Control Glossina fuscipes spp., the Major Vector of Human African Trypanosomiasis",
  "authors": [
    {
      "name": "Maurice O. Omolo",
      "first": "Maurice O.",
      "last": "Omolo",
      "affiliation": "International Center for Insect Physiology and Ecology, ICIPE, Nairobi, Kenya; Masinde Muliro University of Science & Technology, Kakamega, Kenya"
    },
    {
      "name": "Ahmed Hassanali",
      "first": "Ahmed",
      "last": "Hassanali",
      "affiliation": "International Center for Insect Physiology and Ecology, ICIPE, Nairobi, Kenya"
    },
    {
      "name": "Serge Mpiana",
      "first": "Serge",
      "last": "Mpiana",
      "affiliation": "Labovet, Kinshasa, Democratic Republic of Congo"
    },
    {
      "name": "Johan Esterhuizen",
      "first": "Johan",
      "last": "Esterhuizen",
      "affiliation": "Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Jenny Lindh",
      "first": "Jenny",
      "last": "Lindh",
      "affiliation": "Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Mike J. Lehane",
      "first": "Mike J.",
      "last": "Lehane",
      "affiliation": "Liverpool School of Tropical Medicine, Liverpool, United Kingdom"
    },
    {
      "name": "Philippe Solano",
      "first": "Philippe",
      "last": "Solano",
      "affiliation": "Institut de Recherche pour le Développement (IRD), UMR 177 IRD-CIRAD, Montpellier, France"
    },
    {
      "name": "Jean Baptiste Rayaisse",
      "first": "Jean Baptiste",
      "last": "Rayaisse",
      "affiliation": "Centre International de Recherche-Développement sur l'Elevage en Zone Subhumide (CIRDES), Bobo Dioulasso, Burkina Faso"
    },
    {
      "name": "Glyn A. Vale",
      "first": "Glyn A.",
      "last": "Vale",
      "affiliation": "Natural Resources Institute, University of Greenwich, Chatham, United Kingdom"
    },
    {
      "name": "Steve J. Torr",
      "first": "Steve J.",
      "last": "Torr",
      "affiliation": "Natural Resources Institute, University of Greenwich, Chatham, United Kingdom"
    },
    {
      "name": "Inaki Tirados",
      "first": "Inaki",
      "last": "Tirados",
      "affiliation": "Natural Resources Institute, University of Greenwich, Chatham, United Kingdom"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-05",
  "dateAccepted": "2009-04-16",
  "dateReceived": "2009-02-20",
  "volume": "3",
  "number": "5",
  "pages": "e435",
  "tags": [
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections"
  ],
  "abstract": "We are attempting to develop cost-effective control methods for the important vector of sleeping sickness, Glossina fuscipes spp. Responses of the tsetse flies Glossina fuscipes fuscipes (in Kenya) and G. f. quanzensis (in Democratic Republic of Congo) to natural host odours are reported. Arrangements of electric nets were used to assess the effect of cattle-, human- and pig-odour on (1) the numbers of tsetse attracted to the odour source and (2) the proportion of flies that landed on a black target (1×1 m). In addition responses to monitor lizard (Varanus niloticus) were assessed in Kenya. The effects of all four odours on the proportion of tsetse that entered a biconical trap were also determined. Sources of natural host odour were produced by placing live hosts in a tent or metal hut (volumes≈16 m3) from which the air was exhausted at ∼2000 L/min. Odours from cattle, pigs and humans had no significant effect on attraction of G. f. fuscipes but lizard odour doubled the catch (P&lt;0.05). Similarly, mammalian odours had no significant effect on landing or trap entry whereas lizard odour increased these responses significantly: landing responses increased significantly by 22% for males and 10% for females; the increase in trap efficiency was relatively slight (5–10%) and not always significant. For G. f. quanzensis, only pig odour had a consistent effect, doubling the catch of females attracted to the source and increasing the landing response for females by ∼15%. Dispensing CO2 at doses equivalent to natural hosts suggested that the response of G. f. fuscipes to lizard odour was not due to CO2. For G. f. quanzensis, pig odour and CO2 attracted similar numbers of tsetse, but CO2 had no material effect on the landing response. The results suggest that identifying kairomones present in lizard odour for G. f. fuscipes and pig odour for G. f. quanzensis may improve the performance of targets for controlling these species.",
  "fullText": "Introduction Between 1931 and 1961, the annual number of recorded Human African Trypanosomiasis (HAT) cases was reduced by &gt;90%, from &gt;60,000 reported cases/year to &lt;5000 cases/year, through the systematic screening and treatment of millions of individuals across sub-Saharan Africa [1]. When the incidence of HAT across the continent dropped to such low numbers, the newly-independent nations of sub-Saharan Africa reduced their efforts to monitor and control the disease. This reduction, combined with political and economic turbulence in some of the countries most affected by the disease (e.g., Uganda, Sudan, Angola, Democratic Republic of Congo) led to a resurgence in HAT across the continent, such that by the late 1990s there were &gt;30,000 recorded cases/year. Consequently, the World Health Organization (WHO) revived a major programme of disease surveillance and treatment which has now reduced the annual number of reported cases to &lt;15,000/year [1]. Thus, over the past 80 years, programmes against HAT have been based largely on the detection and treatment of disease in humans and this continues to be the case [1]. Interventions against tsetse flies (Glossina spp.) [2], the vector of the Trypanosoma spp which cause HAT, have, with some exceptions normally based on the rodesiense form of the disease [3], played a minor role. This emphasis on tackling the trypanosome rather than the tsetse is due to a variety of humanitarian, socio-economic [4],[5],[6] and epidemiological [7],[8] factors. By contrast, tsetse control has played a major role in the control of animal trypanosomiasis [4]. Should vector control play a greater role in tackling HAT? More than 90% of HAT cases are caused by T. brucei gambiense transmitted by Palpalis-group species of tsetse found in Central and West Africa [1]. Moreover, modern methods of tsetse control, based on the use of natural (insecticide-treated cattle) or artificial (traps or insecticide-treated targets) baits to lure and kill tsetse, have the particular advantage that they can be applied and afforded by local people. Such interventions could overcome the present dependence on outside agencies to deploy survey teams and provide drugs and medical personnel. Against this advantage, the application of baits against HAT faces two important problems. First, the use of insecticide-treated cattle [9] depends on cattle being present and forming a significant part of the diet of the local tsetse. In many of the HAT-affected areas of West Africa, cattle are not abundant (e.g., Guinea, southern Côte d'Ivoire, DRC [10] and/or cattle do not seem to be an important component of the diet of Palpalis-group tsetse [11]. Second, the use of artificial baits has been more successful with Morsitans- (e.g., G. pallidipes and G. m. morsitans) rather than Palpalis-group species of tsetse [12]. Morsitans-group species, especially G. pallidipes, are highly responsive to host odours. Insecticide-treated targets and traps, baited with synthetic blends of these odours and deployed at densities of ∼4 targets/km2, can eliminate populations of tsetse rapidly [13],[14],[15]. By contrast, there are no artificial attractants effective against important vectors of T. b. gambiense and thus baits must be deployed at densities of"
}