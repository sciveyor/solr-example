{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000636",
  "doi": "10.1371/journal.pntd.0000636",
  "externalIds": [
    "pii:09-PNTD-RA-0617R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Phylogeography and Population Structure of Glossina fuscipes fuscipes in Uganda: Implications for Control of Tsetse",
  "authors": [
    {
      "name": "Jon S. Beadell",
      "first": "Jon S.",
      "last": "Beadell",
      "affiliation": "Department of Ecology and Evolutionary Biology, Yale University, New Haven, Connecticut, United States of America"
    },
    {
      "name": "Chaz Hyseni",
      "first": "Chaz",
      "last": "Hyseni",
      "affiliation": "Department of Ecology and Evolutionary Biology, Yale University, New Haven, Connecticut, United States of America"
    },
    {
      "name": "Patrick P. Abila",
      "first": "Patrick P.",
      "last": "Abila",
      "affiliation": "National Livestock Resources Research Institute, Tororo, Uganda"
    },
    {
      "name": "Rogers Azabo",
      "first": "Rogers",
      "last": "Azabo",
      "affiliation": "National Livestock Resources Research Institute, Tororo, Uganda"
    },
    {
      "name": "John C. K. Enyaru",
      "first": "John C. K.",
      "last": "Enyaru",
      "affiliation": "Department of Biochemistry, Faculty of Science, Makerere University, Kampala, Uganda"
    },
    {
      "name": "Johnson O. Ouma",
      "first": "Johnson O.",
      "last": "Ouma",
      "affiliation": "Trypanosomiasis Research Centre, Kenya Agricultural Research Institute, Kikuyu, Kenya"
    },
    {
      "name": "Yassir O. Mohammed",
      "first": "Yassir O.",
      "last": "Mohammed",
      "affiliation": "Central Veterinary Research Laboratories, Animal Resources Research Corporation, Khartoum, Sudan"
    },
    {
      "name": "Loyce M. Okedi",
      "first": "Loyce M.",
      "last": "Okedi",
      "affiliation": "National Livestock Resources Research Institute, Tororo, Uganda"
    },
    {
      "name": "Serap Aksoy",
      "first": "Serap",
      "last": "Aksoy",
      "affiliation": "Department of Epidemiology and Public Health, Yale University, New Haven, Connecticut, United States of America"
    },
    {
      "name": "Adalgisa Caccone",
      "first": "Adalgisa",
      "last": "Caccone",
      "affiliation": "Department of Ecology and Evolutionary Biology, Yale University, New Haven, Connecticut, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2010-03",
  "dateAccepted": "2010-01-29",
  "dateReceived": "2009-10-30",
  "volume": "4",
  "number": "3",
  "pages": "e636",
  "tags": [
    "Evolutionary Biology/Evolutionary Ecology",
    "Genetics and Genomics/Population Genetics"
  ],
  "abstract": "Background\n\nGlossina fuscipes fuscipes, a riverine species of tsetse, is the main vector of both human and animal trypanosomiasis in Uganda. Successful implementation of vector control will require establishing an appropriate geographical scale for these activities. Population genetics can help to resolve this issue by characterizing the extent of linkage among apparently isolated groups of tsetse.\n\nMethodology/Principal Findings\n\nWe conducted genetic analyses on mitochondrial and microsatellite data accumulated from approximately 1000 individual tsetse captured in Uganda and neighboring regions of Kenya and Sudan. Phylogeographic analyses suggested that the largest scale genetic structure in G. f. fuscipes arose from an historical event that divided two divergent mitochondrial lineages. These lineages are currently partitioned to northern and southern Uganda and co-occur only in a narrow zone of contact extending across central Uganda. Bayesian assignment tests, which provided evidence for admixture between northern and southern flies at the zone of contact and evidence for northerly gene flow across the zone of contact, indicated that this structure may be impermanent. On the other hand, microsatellite structure within the southern lineage indicated that gene flow is currently limited between populations in western and southeastern Uganda. Within regions, the average FST between populations separated by less than 100 km was less than ∼0.1. Significant tests of isolation by distance suggested that gene flow is ongoing between neighboring populations and that island populations are not uniformly more isolated than mainland populations.\n\nConclusions/Significance\n\nDespite the presence of population structure arising from historical colonization events, our results have revealed strong signals of current gene flow within regions that should be accounted for when planning tsetse control in Uganda. Populations in southeastern Uganda appeared to receive little gene flow from populations in western or northern Uganda, supporting the feasibility of area wide control in the Lake Victoria region by the Pan African Tsetse and Trypanosomiasis Eradication Campaign.",
  "fullText": "Introduction Human African Trypanosomiasis, or sleeping sickness, is a vector-borne disease that kills thousands of people each year in sub-Saharan Africa [1]. Nagana, a related disease of livestock, can also be fatal and is a major impediment to agricultural development. Both diseases are caused by parasitic trypanosomes transmitted by tsetse flies. No vaccines exist to prevent the disease and drugs currently available to treat sleeping sickness in humans are expensive, can cause severe side effects and are difficult to administer in remote villages. Therefore, controlling the tsetse vector, via methods involving habitat elimination, trapping, insecticide-treated targets, aerial or ground spraying, insecticide-treated cattle, or the release of sterile or transgenic insects, may represent an effective alternative for controlling the disease [2]–[6]. In 2001, the Pan-African Tsetse and Trypanosomiasis Eradication Campaign (PATTEC) was formed with the goal of identifying discrete zones of infestation that could be systematically exterminated, one by one, using area-wide methods [6],[7]. Geographic features such as mountains and bodies of water were originally proposed as useful boundaries to delineate isolated target populations, however, biologically-relevant boundaries may be better defined by molecular techniques that explicitly account for the degree of interaction between neighboring groups of flies. The application of population genetic methods to disease vectors offers insights into facets of ecology, reproduction, dispersal patterns, population dynamics and genetic diversity that can have direct relevance for guiding control programs. At deeper timescales, population genetics can elucidate the historical processes that have influenced the current distribution of vector populations. Over more recent timescales, population genetics can delineate isolated vector populations that may be appropriate targets for local eradication and can identify dispersal routes that must be disrupted to prevent reinfestation of previously cleared zones. Characterization of barriers to gene flow is particularly important in planning the release of genetically modified or sterile vectors where the scale of success will depend on the scale at which the released vectors mate with wild counterparts [8],[9]. Beyond this, estimates of genetic diversity and effective size can provide insight into the relative importance of drift or selection in driving important phenotypes such as insecticide resistance and vector competence. Finally, if established early enough, ongoing genetic monitoring can help to characterize the effectiveness of control efforts (i.e., by estimating the reduction in effective population sizes resulting from control) and can help to identify the cause of any failures (i.e., whether flies have recolonized from relict populations within controlled areas or from neighboring populations outside of control areas). In Uganda, the primary vector of both human and animal forms of trypanosomiasis is Glossina fuscipes fuscipes Newstead 1910, which occurs in a peninsular distribution terminating around the shores of Lake Victoria at the far eastern edge of its range (see Figure 1). Given the possibility of severing this peninsula from the remainder of fuscipes' range in central Africa, the Lake Victoria region has been proposed as a potentially suitable target for area-wide control [6]. Although the severe HAT epidemics of the 20th century have largely subsided [10], the acute"
}