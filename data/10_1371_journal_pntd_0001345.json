{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001345",
  "doi": "10.1371/journal.pntd.0001345",
  "externalIds": [
    "pii:PNTD-D-11-00358"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Human Cellular Immune Response to the Saliva of Phlebotomus papatasi Is Mediated by IL-10-Producing CD8+ T Cells and Th1-Polarized CD4+ Lymphocytes",
  "authors": [
    {
      "name": "Maha Abdeladhim",
      "first": "Maha",
      "last": "Abdeladhim",
      "affiliation": "Department of Clinical Immunology, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Mélika Ben Ahmed",
      "first": "Mélika",
      "last": "Ben Ahmed",
      "affiliation": "Department of Clinical Immunology, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Soumaya Marzouki",
      "first": "Soumaya",
      "last": "Marzouki",
      "affiliation": "Department of Clinical Immunology, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Nadia Belhadj Hmida",
      "first": "Nadia",
      "last": "Belhadj Hmida",
      "affiliation": "Department of Clinical Immunology, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Thouraya Boussoffara",
      "first": "Thouraya",
      "last": "Boussoffara",
      "affiliation": "LIVGM, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Nabil Belhaj Hamida",
      "first": "Nabil",
      "last": "Belhaj Hamida",
      "affiliation": "Department of Medical Epidemiology, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Afif Ben Salah",
      "first": "Afif",
      "last": "Ben Salah",
      "affiliation": "Department of Medical Epidemiology, Pasteur Institute of Tunis, Tunis, Tunisia"
    },
    {
      "name": "Hechmi Louzir",
      "first": "Hechmi",
      "last": "Louzir",
      "affiliation": "Department of Clinical Immunology, Pasteur Institute of Tunis, Tunis, Tunisia; LIVGM, Pasteur Institute of Tunis, Tunis, Tunisia"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-10",
  "dateAccepted": "2011-08-19",
  "dateReceived": "2011-04-20",
  "volume": "5",
  "number": "10",
  "pages": "e1345",
  "tags": [
    "Biology"
  ],
  "abstract": "Background\n          \nThe saliva of sand flies strongly enhances the infectivity of Leishmania in mice. Additionally, pre-exposure to saliva can protect mice from disease progression probably through the induction of a cellular immune response.\n\n          Methodology/Principal Findings\n          \nWe analysed the cellular immune response against the saliva of Phlebotomus papatasi in humans and defined the phenotypic characteristics and cytokine production pattern of specific lymphocytes by flow cytometry. Additionally, proliferation and IFN-γ production of activated cells were analysed in magnetically separated CD4+ and CD8+ T cells. A proliferative response of peripheral blood mononuclear cells against the saliva of Phlebotomus papatasi was demonstrated in nearly 30% of naturally exposed individuals. Salivary extracts did not induce any secretion of IFN-γ but triggered the production of IL-10 primarily by CD8+ lymphocytes. In magnetically separated lymphocytes, the saliva induced the proliferation of both CD4+ and CD8+ T cells which was further enhanced after IL-10 blockage. Interestingly, when activated CD4+ lymphocytes were separated from CD8+ cells, they produced high amounts of IFN-γ.\n\n          Conclusion\n          \nHerein, we demonstrated that the overall effect of Phlebotomus papatasi saliva was dominated by the activation of IL-10-producing CD8+ cells suggesting a possible detrimental effect of pre-exposure to saliva on human leishmaniasis outcome. However, the activation of Th1 lymphocytes by the saliva provides the rationale to better define the nature of the salivary antigens that could be used for vaccine development.",
  "fullText": "Introduction Leishmaniasis includes a heterogeneous group of diseases that are caused by protozoan parasites of the genus Leishmania. The disease ranges from asymptomatic infections to self-limiting cutaneous lesion(s) or fatal visceral forms [1]. Leishmania parasites are transmitted to the vertebrate hosts by the bite of sand flies. During parasite inoculation in the host's skin, the vector injects the saliva that contains a large number of pharmacological components [2]–[4]. Several observations indicate that sand fly saliva is crucial in the establishment of leishmaniasis and disease pathogenesis [5]–[7]. The mechanism by which the vector's saliva enhances leishmania infection remains to be clarified. Sand fly saliva contains potent antihemostatic and vasodilatator compounds as well as potentially immunomodulatory molecules that can directly down-modulate macrophage effector functions and facilitate the establishment of the infection [8], [9]. The exacerbating effects of saliva may also be related to the early release of epidermal interleukin-4 (IL-4) [7]. Alternatively, it could be ascribed to the development of an adaptive immune response that would favor the commitment of a Th2 immunity against Leishmania. In mice, pre-exposure to saliva completely abrogated the effects of the sand fly saliva and protected the host from disease progression [7], [10], [11]. This protective effect correlated with a strong delayed-type hypersensitivity (DTH) response and an early and increased in situ production of IFN-γ and IL-12 [10]. Further experiments demonstrated that immunization with PpSP15 (gi|15963509) from P. papatasi saliva resulted in protection which was not ascribed to a humoral immune response [12]. Altogether, these data strongly supported the possibility that leishmaniasis could be prevented by vaccinating against sand fly saliva and suggest that the protective effect of the saliva might be associated with cell-mediated immunity. In humans, the data about the cellular immune responses are scarce. Herein, we analyzed the cellular immune response against the saliva of Phlebotomus papatasi developed in individuals naturally exposed to sand fly bites and demonstrated that the overall effect of Phlebotomus papatasi saliva was dominated by the activation of peripheral IL-10-producing CD8+ cells. Strikingly, the activation of IFN-γ-producing CD4+ T lymphocytes has been revealed after neutralization of IL-10 production or depletion of CD8 lymphocytes. Methods Ethic statement All experiments were conducted according to the principles expressed in the Declaration of Helsinki. The study was approved by the ethic committee of Institute Pasteur of Tunis. All patients provided written informed consent for the collection of samples and subsequent analysis. Study population and samples Peripheral blood samples were drawn from 36 donors (Table 1). The sampling has been performed on April, just before the transmission season of cutaneous leishmaniasis in Tunisia (between June and October). Ten donors (B1 to B10; age range 27–52 years, mean 31.9 years) were living in Tunis, a non endemic region for ZCL but in which the presence of P. papatasi has been reported at low frequency [13]. Twenty-six (B11 to B36; age range 15–73 years, mean 40.2 years) were living in Sidi Bouzid, a region located in the center of Tunisia, which is endemic for ZCL caused by"
}