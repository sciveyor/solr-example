{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001617",
  "doi": "10.1371/journal.pntd.0001617",
  "externalIds": [
    "pii:PNTD-D-11-01261"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Prevalence, Features and Risk Factors for Malaria Co-Infections amongst Visceral Leishmaniasis Patients from Amudat Hospital, Uganda",
  "authors": [
    {
      "name": "Erika van den Bogaart",
      "first": "Erika",
      "last": "van den Bogaart",
      "affiliation": "Department of Biomedical Research, Royal Tropical Institute (KIT), Amsterdam, The Netherlands"
    },
    {
      "name": "Marieke M. Z. Berkhout",
      "first": "Marieke M. Z.",
      "last": "Berkhout",
      "affiliation": "Department of Biomedical Research, Royal Tropical Institute (KIT), Amsterdam, The Netherlands"
    },
    {
      "name": "Emily R. Adams",
      "first": "Emily R.",
      "last": "Adams",
      "affiliation": "Department of Biomedical Research, Royal Tropical Institute (KIT), Amsterdam, The Netherlands"
    },
    {
      "name": "Pètra F. Mens",
      "first": "Pètra F.",
      "last": "Mens",
      "affiliation": "Department of Biomedical Research, Royal Tropical Institute (KIT), Amsterdam, The Netherlands"
    },
    {
      "name": "Elizabeth Sentongo",
      "first": "Elizabeth",
      "last": "Sentongo",
      "affiliation": "Department of Medical Microbiology, Makerere University College of Health Sciences, Kampala, Uganda"
    },
    {
      "name": "Dawson B. Mbulamberi",
      "first": "Dawson B.",
      "last": "Mbulamberi",
      "affiliation": "Ministry of Health, Kampala, Uganda"
    },
    {
      "name": "Masja Straetemans",
      "first": "Masja",
      "last": "Straetemans",
      "affiliation": "Department of Biomedical Research, Royal Tropical Institute (KIT), Amsterdam, The Netherlands"
    },
    {
      "name": "Henk D. F. H. Schallig",
      "first": "Henk D. F. H.",
      "last": "Schallig",
      "affiliation": "Department of Biomedical Research, Royal Tropical Institute (KIT), Amsterdam, The Netherlands"
    },
    {
      "name": "Francois Chappuis",
      "first": "Francois",
      "last": "Chappuis",
      "affiliation": "Médecins Sans Frontières, Geneva, Switzerland; Geneva University Hospitals and University of Geneva, Geneva, Switzerland"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-04",
  "dateAccepted": "2012-03-05",
  "dateReceived": "2011-12-16",
  "volume": "6",
  "number": "4",
  "pages": "e1617",
  "tags": [
    "Epidemiology",
    "Health screening",
    "Infectious Diseases",
    "Infectious disease control",
    "Infectious disease epidemiology",
    "Infectious diseases",
    "Leishmaniasis",
    "Malaria",
    "Medicine",
    "Neglected tropical diseases",
    "Parasitic diseases",
    "Protozoan infections",
    "Public Health and Epidemiology",
    "Public health"
  ],
  "abstract": "Background and methodology\n          \nDue to geographic overlap of malaria and visceral leishmaniasis (VL), co-infections may exist but have been poorly investigated. To describe prevalence, features and risk factors for VL-malaria co-infections, a case-control analysis was conducted on data collected at Amudat Hospital, Uganda (2000–2006) by Médecins sans Frontières. Cases were identified as patients with laboratory-confirmed VL and malaria at hospital admission or during hospitalization; controls were VL patients with negative malaria smears. A logistic regression analysis was performed to study the association between patients' characteristics and the occurrence of the co-infection.\n\n          Results\n          \nOf 2414 patients with confirmed VL, 450 (19%) were positively diagnosed with concomitant malaria. Most co-infected patients were males, residing in Kenya (69%). While young age was identified by multivariate analysis as a risk factor for concurrent VL and malaria, particularly the age groups 0–4 (odds ratio (OR): 2.44; 95% confidence interval (CI): 1.52–3.92) and 5–9 years (OR: 2.23, 95% CI: 1.45-3-45), mild (OR: 0.53; 95% CI: 0.32–0.88) and moderate (OR: 0.45; 95% CI: 0.27–0.77) anemia negatively correlated with the co-morbidity. VL patients harboring skin infections were nearly three times less likely to have the co-infection (OR: 0.35; 95% CI: 0.17–0.72), as highlighted by the multivariate model. Anorexia was slightly more frequent among co-infected patients (OR: 1.71; 95% CI: 0.96–3.03). The in-hospital case-fatality rate did not significantly differ between cases and controls, being 2.7% and 3.1% respectively (OR: 0.87; 95% CI: 0.46–1.63).\n\n          Conclusions\n          \nConcurrent malaria represents a common condition among young VL patients living in the Pokot region of Kenya and Uganda. Although these co-morbidities did not result in a poorer prognosis, possibly due to early detection of malaria, a positive trend towards more severe symptoms was identified, indicating that routine screening of VL patients living in malaria endemic-areas and close monitoring of co-infected patients should be implemented.",
  "fullText": "Introduction Due to extensive overlap in the geographical distribution of many infectious diseases, multiple infections appear to be the rule rather than the exception in many tropical and subtropical regions [1]. Polyparasitism, in particular, may predominate in rural areas of developing countries, where poor sanitation and economic conditions allow the uninterrupted transmission of many parasites [2]–[5]. Prevalences of multiparasite infections above 30% have been shown to occur regularly throughout South-East Asia and much of Central and West Africa, with communities harboring multiple parasites in up to 80% of their population [6]. Importantly, the different combinations in which pathogens might co-exist in a certain population and their distribution therein, do not result from a random process, but they are rather part of a selection governed by a variety of ecological and host factors, which include the biological interactions of the parasites within the host [3], [7]. These interactions may affect the pathogenicity of the infective agents, resulting in a spectrum of effects on the polyparasitized host, ranging from exacerbation to amelioration of disease severity [8]. Despite the recent upsurge in investigations targeting multiple helminth species and Plasmodium-helminth co-infections [9]–[12], the actual extent of polyparasitism and its pathological consequences remain largely unassessed [8]. The scarcity of information is particularly striking for infections that are clinically not apparent or lack pathognomonic signs. As the tropics and sub-tropics are burdened with infectious diseases sharing similar clinical pictures, recognition of these diseases occurring in the same patient might be difficult in poor resource settings. Visceral leishmaniasis (VL) is a life-threatening syndrome caused by protozoan parasites of the Leishmania donovani complex. Most cases occur in East Africa, South-East Asia and Brazil, where nearly 0.5 million people get infected each year [13], half of whom are children [14]. Differential diagnosis of VL often includes malaria amongst other febrile splenomegalies, due to its geographical and clinical overlap. Malaria, in fact, is widespread in tropical and sub-tropical regions of the world, where it accounts for more than 250 million cases annually, the vast majority of which occurs among children under 5 years old [15]. Transmission can occur throughout the year or be seasonal, depending on the region [16]. In the latter case, transmission seasons for VL and malaria may not coincide, but the two diseases still overlap, due to the longer incubation period of VL. The overlap in disease distribution suggests the two diseases could co-occur in the same host. Nonetheless, figures describing the extent of VL and malaria co-infections are not readily available in literature. To gather evidence on the occurrence of such co-morbidities, a systematic review of the present literature was first conducted (Figure S1). This review showed that cases of VL and malaria co-infections have been reported across various African and Asian countries, with the prevalence among VL patients ranging from 20.8% and 6.4% in Uganda [17], [18] to 10.7% in Sudan [19] and 1.2% in Bangladesh [20] and a rate of 5.9% among Indian patients with fever and splenomegaly [21]. With the exception of the"
}