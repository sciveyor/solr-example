{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000926",
  "doi": "10.1371/journal.pntd.0000926",
  "externalIds": [
    "pii:09-PNTD-RA-0756R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "RIG-I, MDA5 and TLR3 Synergistically Play an Important Role in Restriction of Dengue Virus Infection",
  "authors": [
    {
      "name": "A. M. A. Nasirudeen",
      "first": "A. M. A.",
      "last": "Nasirudeen",
      "affiliation": "Institute of Molecular and Cell Biology, Singapore, Singapore"
    },
    {
      "name": "Hui Hui Wong",
      "first": "Hui Hui",
      "last": "Wong",
      "affiliation": "Institute of Molecular and Cell Biology, Singapore, Singapore"
    },
    {
      "name": "Peiling Thien",
      "first": "Peiling",
      "last": "Thien",
      "affiliation": "School of Biological Sciences, Nanyang Technological University, Singapore, Singapore"
    },
    {
      "name": "Shengli Xu",
      "first": "Shengli",
      "last": "Xu",
      "affiliation": "Immunology Group, Bioprocessing Technology Institute, Singapore, Singapore"
    },
    {
      "name": "Kong-Peng Lam",
      "first": "Kong-Peng",
      "last": "Lam",
      "affiliation": "Immunology Group, Bioprocessing Technology Institute, Singapore, Singapore"
    },
    {
      "name": "Ding Xiang Liu",
      "first": "Ding Xiang",
      "last": "Liu",
      "affiliation": "Institute of Molecular and Cell Biology, Singapore, Singapore; School of Biological Sciences, Nanyang Technological University, Singapore, Singapore"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2011-01",
  "dateAccepted": "2010-11-26",
  "dateReceived": "2009-12-22",
  "volume": "5",
  "number": "1",
  "pages": "e926",
  "tags": [
    "Immunology/Cellular Microbiology and Pathogenesis",
    "Immunology/Immune Response",
    "Immunology/Immunity to Infections",
    "Immunology/Innate Immunity",
    "Microbiology/Cellular Microbiology and Pathogenesis",
    "Microbiology/Innate Immunity"
  ],
  "abstract": "Dengue virus (DV) infection is one of the most common mosquito-borne viral diseases in the world. The innate immune system is important for the early detection of virus and for mounting a cascade of defense measures which include the production of type 1 interferon (IFN). Hence, a thorough understanding of the innate immune response during DV infection would be essential for our understanding of the DV pathogenesis. A recent application of the microarray to dengue virus type 1 (DV1) infected lung carcinoma cells revealed the increased expression of both extracellular and cytoplasmic pattern recognition receptors; retinoic acid inducible gene-I (RIG-I), melanoma differentiation associated gene-5 (MDA-5) and Toll-like receptor-3 (TLR3). These intracellular RNA sensors were previously reported to sense DV infection in different cells. In this study, we show that they are collectively involved in initiating an effective IFN production against DV. Cells silenced for these genes were highly susceptible to DV infection. RIG-I and MDA5 knockdown HUH-7 cells and TLR3 knockout macrophages were highly susceptible to DV infection. When cells were silenced for only RIG-I and MDA5 (but not TLR3), substantial production of IFN-β was observed upon virus infection and vice versa. High susceptibility to virus infection led to ER-stress induced apoptosis in HUH-7 cells. Collectively, our studies demonstrate that the intracellular RNA virus sensors (RIG-I, MDA5 and TLR3) are activated upon DV infection and are essential for host defense against the virus.",
  "fullText": "Introduction Pathogen associated molecular patterns (PAMP) trigger innate immunity against pathogens and this response represents the first line of defense against various microorganisms [1]. Double strand RNA (dsRNA), a viral replication intermediate, is sensed by cytoplasmic RNA helicases retinoic acid-inducible gene I (RIG-I) and melanoma differentiation-associated gene 5 (MDA5) as well as by toll-like receptors-3 (TLR3) [2]. TLR3 and RNA helicases interact with different PAMP during the proximal signaling events triggered by the dsRNA. However, these two parallel viral recognition pathways converge at the level of IFN regulatory factor-3 (IRF3). Phosphorylation of IRF3 initiates antiviral responses, including the activation of type I interferon (IFN), interferon stimulating genes (ISGs) and proinflammatory cytokines [3], [4]. While TLR3 is primarily responsible for recognizing viral components such as viral nucleic acid and envelope glycoproteins in the extracellular and endosomal compartments [5], DExD/H box–containing RNA helicases - RIG-I, MDA5 - recognize intracellular dsRNA and they constitute the TLR-independent IFN induction pathway. Although both RIG-I and MDA-5 share high degree of functional and structural homology, they were observed to respond to different dsRNA moieties and RNA viruses. They contain caspase-recruiting domains (CARD) that allow them to interact with Interferon Promoter Stimulated 1 (IPS-1) (otherwise known as Virus-induced Signaling adapter (VISA); mitochondrial antiviral signaling protein (MAVS) or Cardif) [6]. Similar to TLR3, IPS-1 mediates activation of TBK1 and IKKε which in turn activates/phosphorylates IRF3. Phosphorylated IRF3 then homodimerises and translocates to the nucleus [7] to stimulate the expression of type I interferons – IFN-α and IFNβ. IFN-α/β, together with an array of other interferon stimulated genes (ISGs) and cytokines, lead to the establishment of an antiviral state which restricts virus spread in the host cells. Dengue virus was reported to induce type I IFN even in RIG-I or MDA5 null cells [8]. The same is observed with West Nile virus [9], another Flavivirius. Japanese encephalitis virus [10] and Hepatitis C virus [11], also belonging to the Flavivirdae family, on the other hand, are recognized only by RIG-I. These results suggest that Flaviviruses, despite their common genomic features and replication strategies, are differentially recognized by the host. Despite having the IFN pathway activated in response to viral infection, pathogenic viruses have evolved ways to manipulate the IFN system to favor their survival in the host cell. Reports have shown that DV can antagonize the IFN pathway via their non-structural proteins such as NS2A, NS2B, NS4B and NS5 [12], [13]. A recent microarray analysis of dengue virus type 1 (DV1)-infected lung carcinoma cell line, H1299, showed upregulation of a number of innate immune response genes. In particular, RIG-I, MDA5 and TLR3 were up-regulated more than 8-, 5- and 2-fold respectively [14]. Furthermore, Sumpter and colleagues [11] reported that inactivation of RIG-I in HUH-7 cells resulted in permissiveness of hepatitis C virus (HCV; a flavivirus) RNA replication. Since HUH-7 cells have low basal level of Toll-like receptor 3 (TLR 3) gene expression, this cell line would be a good in vitro model system to investigate RIG-I-dependent signaling in DV1 infection"
}