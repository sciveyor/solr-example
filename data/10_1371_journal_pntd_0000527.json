{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000527",
  "doi": "10.1371/journal.pntd.0000527",
  "externalIds": [
    "pii:09-PNTD-RA-0300R3"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Widespread Distribution of a Newly Found Point Mutation in Voltage-Gated Sodium Channel in Pyrethroid-Resistant Aedes aegypti Populations in Vietnam",
  "authors": [
    {
      "name": "Hitoshi Kawada",
      "first": "Hitoshi",
      "last": "Kawada",
      "affiliation": "Department of Vector Ecology & Environment, Institute of Tropical Medicine, Nagasaki University, Nagasaki, Japan"
    },
    {
      "name": "Yukiko Higa",
      "first": "Yukiko",
      "last": "Higa",
      "affiliation": "Department of Vector Ecology & Environment, Institute of Tropical Medicine, Nagasaki University, Nagasaki, Japan"
    },
    {
      "name": "Osamu Komagata",
      "first": "Osamu",
      "last": "Komagata",
      "affiliation": "National Institute of Infectious Diseases, Tokyo, Japan"
    },
    {
      "name": "Shinji Kasai",
      "first": "Shinji",
      "last": "Kasai",
      "affiliation": "National Institute of Infectious Diseases, Tokyo, Japan"
    },
    {
      "name": "Takashi Tomita",
      "first": "Takashi",
      "last": "Tomita",
      "affiliation": "National Institute of Infectious Diseases, Tokyo, Japan"
    },
    {
      "name": "Nguyen Thi Yen",
      "first": "Nguyen",
      "last": "Thi Yen",
      "affiliation": "National Institute of Hygiene and Epidemiology, Hanoi, Vietnam"
    },
    {
      "name": "Luu Lee Loan",
      "first": "Luu Lee",
      "last": "Loan",
      "affiliation": "Pasteur Institute, Ho Chi Minh City, Vietnam"
    },
    {
      "name": "Rodrigo A. P. Sánchez",
      "first": "Rodrigo A. P.",
      "last": "Sánchez",
      "affiliation": "Universidad Evangélica de El Salvador, San Salvador, El Salvador"
    },
    {
      "name": "Masahiro Takagi",
      "first": "Masahiro",
      "last": "Takagi",
      "affiliation": "Department of Vector Ecology & Environment, Institute of Tropical Medicine, Nagasaki University, Nagasaki, Japan"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-10",
  "dateAccepted": "2009-09-10",
  "dateReceived": "2009-06-18",
  "volume": "3",
  "number": "10",
  "pages": "e527",
  "tags": [
    "Genetics and Genomics/Gene Discovery"
  ],
  "abstract": "Background\n\nResistance of Aedes aegypti to photostable pyrethroid insecticides is a major problem for disease-vector control programs. Pyrethroids target the voltage-gated sodium channel on the insects' neurons. Single amino acid substitutions in this channel associated with pyrethroid resistance are one of the main factors that cause knockdown resistance in insects. Although kdr has been observed in several mosquito species, point mutations in the para gene have not been fully characterized in Ae. aegypti populations in Vietnam. The aim of this study was to determine the types and frequencies of mutations in the para gene in Ae. aegypti collected from used tires in Vietnam.\n\nMethods and Findings\n\nSeveral point mutations were examined that cause insensitivity of the voltage-gated sodium channel in the insect nervous system due to the replacement of the amino acids L1014F, the most commonly found point mutation in several mosquitoes; I1011M (or V) and V1016G (or I), which have been reported to be associated to knockdown resistance in Ae. aegypti located in segment 6, domain II; and a recently found amino acid replacement in F1269 in Ae. aegypti, located in segment 6, domain III. Among 756 larvae from 70 locations, no I1011M or I1011V nor L1014F mutations were found, and only two heterozygous V1016G mosquitoes were detected. However, F1269C mutations on domain III were distributed widely and with high frequency in 269 individuals among 757 larvae (53 collection sites among 70 locations surveyed). F1269C frequencies were low in the middle to north part of Vietnam but were high in the areas neighboring big cities and in the south of Vietnam, with the exception of the southern mountainous areas located at an elevation of 500–1000 m.\n\nConclusions\n\nThe overall percentage of homozygous F1269C seems to remain low (7.4%) in the present situation. However, extensive and uncontrolled frequent use of photostable pyrethroids might be a strong selection pressure for this mutation to cause serious problems in the control of dengue fever in Vietnam.",
  "fullText": "Introduction Pyrethroid is the general term for a group of synthetic chemicals that are structurally related to natural pyrethrins derived from Chrysanthemum flowers. There are two main groups of pyrethroids: One possessing high knockdown activity but low killing activity and the other possessing high killing activity. Pyrethroids in the former group such as d-allethrin, prallethrin, metofluthrin etc. are labeled as knockdown agents, and those in the latter group such as permethrin, deltamethrin, cypermethrin etc., as killing agents. The pyrethroids belonging to the former group generally exhibit low stability in the environment. Pyrethroids are typically used as a ‘spatial repellent’ in mosquito coils, mats, and vaporizer liquids to prevent mosquito bites. The use of such pyrethroids is believed to be biorational since it does not kill the affected insects, it causes no selection pressure on insect populations, and mosquitoes develop minimum physiological resistance. The pyrethroids belonging to the latter group, on the other hand, generally exhibit high photostability that enables their outdoor use. Due to their high killing activity, photostable pyrethroids are emerging as the predominant insecticides for vector control. In fact, globally, photostable pyrethroids comprise 40% of the insecticides used annually for indoor residual spraying against malaria vectors and 100% of the World Health Organization (WHO)-recommended insecticides for the treatment of mosquito nets [1]. In Vietnam, photostable pyrethroids have been extensively used in large amounts for malaria and dengue vector control after abandonment of DDT [1]–[4]. Recently, Kawada et al. reported the widespread distribution of pyrethroid resistance in Aedes aegypti (L.) in southern Vietnam. They also suggested a correlation between pyrethroid resistance and the total annual pyrethroid use for malaria vector control (1998–2007) [3]. Vu et al. and Huber et al. also reported a similar tendency in pyrethroid susceptibility in Ae. aegypti in Vietnam [5],[6]. Resistance to photostable pyrethroids is believed to be a major problem for the vector control program. Moreover, cross-resistance to such killing agents in addition to knockdown agents is a significant concern. Two different mechanisms are involved in pyrethroid resistance: Enhanced metabolic detoxification and the insensitivity of target sites. Pyrethroids target the voltage-gated sodium channel on the insects' neurons. Single amino acid substitutions in this channel are associated with pyrethroid resistance and are known as knockdown resistance (kdr). These kdr-type resistances have been observed in several mosquitoes, such as Anopheles gambiae Giles [7], Anopheles stephensi Liston [8], Culex quinquefasciatus Say [9], and Ae. aegypti [10]. In Culex and Anopheles, kdr resistance is associated with the replacement of a leucine at position 1014 in segment 6 of domain II with either phenylalanine or serine [7]–[9]. Brengues et al. described several mutations in segment 6 of domain II of para in Ae. aegypti [10], and Saavedra-Rodriguez et al. found additional mutations at the same position (I1011M, I1011V, V1016G, and V1016I) [11]. Chang et al. found a novel mutation D1794Y, located within the extracellular linker between segment 5 and segment 6 of domain IV, which is concurrent with the known V1023G (this is identical to the above mentioned V1016G"
}