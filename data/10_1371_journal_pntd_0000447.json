{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0000447",
  "doi": "10.1371/journal.pntd.0000447",
  "externalIds": [
    "pii:09-PNTD-RA-0043R2"
  ],
  "license": "This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Strong Host-Feeding Preferences of the Vector Triatoma infestans Modified by Vector Density: Implications for the Epidemiology of Chagas Disease",
  "authors": [
    {
      "name": "Ricardo E. Gürtler",
      "first": "Ricardo E.",
      "last": "Gürtler",
      "affiliation": "Laboratory of Eco-Epidemiology, Department of Ecology, Genetics and Evolution, Universidad de Buenos Aires, Buenos Aires, Argentina"
    },
    {
      "name": "Leonardo A. Ceballos",
      "first": "Leonardo A.",
      "last": "Ceballos",
      "affiliation": "Laboratory of Eco-Epidemiology, Department of Ecology, Genetics and Evolution, Universidad de Buenos Aires, Buenos Aires, Argentina"
    },
    {
      "name": "Paula Ordóñez-Krasnowski",
      "first": "Paula",
      "last": "Ordóñez-Krasnowski",
      "affiliation": "Laboratory of Eco-Epidemiology, Department of Ecology, Genetics and Evolution, Universidad de Buenos Aires, Buenos Aires, Argentina"
    },
    {
      "name": "Leonardo A. Lanati",
      "first": "Leonardo A.",
      "last": "Lanati",
      "affiliation": "Laboratory of Eco-Epidemiology, Department of Ecology, Genetics and Evolution, Universidad de Buenos Aires, Buenos Aires, Argentina"
    },
    {
      "name": "Raúl Stariolo",
      "first": "Raúl",
      "last": "Stariolo",
      "affiliation": "National Vector Control Coordination, Center for Chagas Disease Reservoirs and Vectors, Córdoba, Argentina"
    },
    {
      "name": "Uriel Kitron",
      "first": "Uriel",
      "last": "Kitron",
      "affiliation": "Department of Environmental Studies, Emory University, Atlanta, Georgia, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2009-05",
  "dateAccepted": "2009-04-30",
  "dateReceived": "2009-02-13",
  "volume": "3",
  "number": "5",
  "pages": "e447",
  "tags": [
    "Ecology/Population Ecology",
    "Infectious Diseases/Epidemiology and Control of Infectious Diseases",
    "Infectious Diseases/Neglected Tropical Diseases",
    "Infectious Diseases/Protozoal Infections",
    "Public Health and Epidemiology/Infectious Diseases"
  ],
  "abstract": "Background\n\nUnderstanding the factors that affect the host-feeding preferences of triatomine bugs is crucial for estimating transmission risks and predicting the effects of control tactics targeting domestic animals. We tested whether Triatoma infestans bugs prefer to feed on dogs vs. chickens and on dogs vs. cats and whether vector density modified host choices and other vital rates under natural conditions.\n\nMethodology\n\nTwo host choice experiments were conducted in small caged huts with two rooms between which bugs could move freely. Matched pairs of dog–chicken (six) and dog–cat (three) were assigned randomly to two levels of vector abundance and exposed to starved bugs during three nights. Bloodmeals from 1,160 bugs were tested by a direct enzyme-linked immunosorbent assay.\n\nPrincipal Findings\n\nConditional logistic regression showed that dogs were highly preferred over chickens or cats and that vector density modified host-feeding choices. The relative risk of a bug being blood-engorged increased significantly when it fed only on dog rather than chicken or cat. Bugs achieved higher post-exposure weight at higher vector densities and successive occasions, more so if they fed on a dog rather than on a cat.\n\nConclusions\n\nOur findings strongly refute the hypothesis that T. infestans prefers to blood-feed on chickens rather than dogs. An increase in dog or cat availability or accessibility will increase the rate of bug feeding on them and exert strong non-linear effects on R0. When combined with between-dog heterogeneities in exposure, infection, and infectiousness, the strong bug preference for dogs can be exploited to target dogs in general, and even the specific individuals that account for most of the risk, with topical lotions or insecticide-impregnated collars to turn them into baited lethal traps or use them as transmission or infestation sentinels based on their immune response to Trypanosoma cruzi or bug salivary antigens.",
  "fullText": "Introduction Host choice of hematophagous insects mainly depends on relative host abundance and proximity, host defensive behavior, the density of blood-sucking insects, and the spatial and temporal concurrence of hosts and insects [1],[2]. Examples of innate (genetically determined) host-feeding preferences are few, and convincing evidence with both experimental and field support is scarce [1],[3]. Fleas (Xenopsylla conformis) do not have an innate preference but can discriminate between juvenile and adult hosts, and derive a higher reproductive reward when feeding on juvenile hosts [4]. In Lutzomyia longipalpis sandflies, host size was the main determinant of host-feeding choices among a human, a dog and a chicken exposed simultaneously to laboratory-reared sandflies [5], and its feeding success on chickens was density-dependent [6]. For Triatoma infestans bugs [7] and Simulium damnosum blackflies [8], the proportion of insects biting humans was strongly density-dependent. For Glossina palpalis gambiensis tsetse flies, male flies preferred to feed on cattle rather on reptiles in a stable; the host species selected for the second bloodmeal depended on the host encountered for the first bloodmeal, the between-meal interval and the interaction between these two factors [9]. In mosquitoes, acquired feeding preferences are reflected in their tendency to return to the same villages, houses, host species and oviposition sites [10]. A non-homogeneous distribution of vector feeding contacts on the same host species leads to a basic reproduction number of the pathogen (R0) greater than or equal to that obtained under uniform host selection, a result that still holds when groups of mosquitoes and hosts are highly structured in patches [11],[12]. Triatomine bugs (Hemiptera: Reduviidae) are the vectors of Trypanosoma cruzi, the causal agent of Chagas disease. Triatoma infestans (Klug), the main vector of T. cruzi, is a highly domiciliated species that also occurs in peridomestic structures housing domestic animals [13]. Like most species of triatomine bugs, T. infestans shows eclectic host-feeding patterns [14],[15]. Host proximity has usually been considered more important than host preference for hungry bugs seeking to feed [14]. In laboratory-based host choice experiments of Triatoma sordida (a species typically associated with birds), first-instar nymphs significantly preferred birds to humans [16] whereas fifth-instar nymphs feeding success and bloodmeal size were significantly larger on guinea pigs than on pigeons [17]. Triatoma infestans preferred caged chickens to guinea pigs though not in all replicates [18]. In a simultaneous exposure of four caged vertebrate species to separate groups of fifth-instar nymphs of T. infestans, Triatoma dimidiata and Rhodnius prolixus, none displayed dominant host-feeding preferences among dogs, chickens and opossums but toads were only rarely fed upon [19]. These authors [19] concluded that T. infestans showed a slight preference for dogs in short daytime experiments and a slight one for chickens in overnight trials. No measure of variability in host-feeding choices between the 7–22 replicates for each triatomine species was reported and neither were statistical procedures described. Within the restricted experimental conditions used, the tested triatomine species do not appear to have a fixed or dominant preference for any of the study hosts, and"
}