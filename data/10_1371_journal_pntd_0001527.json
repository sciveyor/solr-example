{
  "schema": "https://data.sciveyor.com/schema",
  "version": 5,
  "id": "doi:10.1371/journal.pntd.0001527",
  "doi": "10.1371/journal.pntd.0001527",
  "externalIds": [
    "pii:PNTD-D-11-00906"
  ],
  "license": "This is an open-access article, free of all copyright, and may be freely reproduced, distributed, transmitted, modified, built upon, or otherwise used by anyone for any lawful purpose. The work is made available under the Creative Commons CC0 public domain dedication.",
  "licenseUrl": "https://plos.org/terms-of-use/",
  "dataSource": "Public Library of Science",
  "dataSourceUrl": "https://data.sciveyor.com/source/plos",
  "dataSourceVersion": 1,
  "type": "article",
  "title": "Regulation of Global Gene Expression in Human Loa loa Infection Is a Function of Chronicity",
  "authors": [
    {
      "name": "Cathy Steel",
      "first": "Cathy",
      "last": "Steel",
      "affiliation": "Laboratory of Parasitic Diseases, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Sudhir Varma",
      "first": "Sudhir",
      "last": "Varma",
      "affiliation": "Bioinformatics and Computational Biology Branch, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, Maryland, United States of America"
    },
    {
      "name": "Thomas B. Nutman",
      "first": "Thomas B.",
      "last": "Nutman",
      "affiliation": "Laboratory of Parasitic Diseases, National Institute of Allergy and Infectious Diseases, National Institutes of Health, Bethesda, Maryland, United States of America"
    }
  ],
  "journal": "PLoS Neglected Tropical Diseases",
  "date": "2012-02",
  "dateAccepted": "2012-01-01",
  "dateReceived": "2011-09-13",
  "volume": "6",
  "number": "2",
  "pages": "e1527",
  "tags": [
    "Biology",
    "Infectious Diseases",
    "Infectious diseases",
    "Medicine",
    "Molecular cell biology",
    "Parasitic diseases"
  ],
  "abstract": "Background\n          \nHuman filarial infection is characterized by downregulated parasite-antigen specific T cell responses but distinct differences exist between patients with longstanding infection (endemics) and those who acquired infection through temporary residency or visits to filarial-endemic regions (expatriates).\n\n          Methods and Findings\n          \nTo characterize mechanisms underlying differences in T cells, analysis of global gene expression using human spotted microarrays was conducted on CD4+ and CD8+ T cells from microfilaremic Loa loa-infected endemic and expatriate patients. Assessment of unstimulated cells showed overexpression of genes linked to inflammation and caspase-associated cell death, particularly in endemics, and enrichment of the Th1/Th2 canonical pathway in endemic CD4+ cells. However, pathways within CD8+ unstimulated cells were most significantly enriched in both patient groups. Antigen (Ag)-driven gene expression was assessed to microfilarial Ag (MfAg) and to the nonparasite Ag streptolysin O (SLO). For MfAg-driven cells, the number of genes differing significantly from unstimulated cells was greater in endemics compared to expatriates (p&lt;0.0001). Functional analysis showed a differential increase in genes associated with NFkB (both groups) and caspase activation (endemics). While the expatriate response to MfAg was primarily a CD4+ pro-inflammatory one, the endemic response included CD4+ and CD8+ cells and was linked to insulin signaling, histone complexes, and ubiquitination. Unlike the enrichment of canonical pathways in CD8+ unstimulated cells, both groups showed pathway enrichment in CD4+ cells to MfAg. Contrasting with the divergent responses to MfAg seen between endemics and expatriates, the CD4+ response to SLO was similar; however, CD8+ cells differed strongly in the nature and numbers (156 [endemics] vs 36 [expatriates]) of genes with differential expression.\n\n          Conclusions\n          \nThese data suggest several important pathways are responsible for the different outcomes seen among filarial-infected patients with varying levels of chronicity and imply an important role for CD8+ cells in some of the global changes seen with lifelong exposure.",
  "fullText": "Introduction Infection with the pathogenic filariae, Loa loa, Brugia malayi, Wuchereria bancrofti, and Onchocerca volvulus, causes an enormous disease burden throughout tropical and sub-tropical regions of the world. Interestingly, however, the clinical manifestations of infection are often markedly different in those with lifelong exposure (i.e. those born in filarial-endemic regions) and those that acquire infection later in life through travel to or temporary residence in a filarial-endemic area [1], [2]. Indeed, filarial infections are less likely to be subclinical in expatriates [3] or transmigrants [4] compared to those with lifelong exposure. Expatriates with loiasis, for example, are more likely to have Calabar swellings and other “allergic” phenomena – such as marked peripheral blood eosinophilia, elevated IgE levels, and urticaria – than is seen in the more chronically infected patients born and raised in endemic areas [5]. With availability of more sensitive assays for the definitive diagnosis of filarial infections, it is now known that infection occurs at much earlier ages than once believed [6], although relatively intense exposure to the vectors that transmit these infections is typically required for acquisition of infection. However, expatriates who acquire infection are not subject to many of the environmental and familial factors that affect those born in endemic regions, the most notable being the alteration of immune responses specific for filarial antigens that occurs early in life [7], [8], [9], and that can persist long-term (decades) [10] as a consequence of in utero exposure to filarial antigens. Moreover, polyparasitism is much more frequent among patients from filarial-endemic regions than in expatriates. That individuals living in an endemic area are exposed continually to the parasite, irrespective of the infection status, is evidenced by the Ag-specific antibody responses seen among filarial-uninfected endemic individuals [11], [12]. Indeed, both susceptibility to infection and the nature of the immune response has a significant genetic component in helminth- and filarial-endemic populations [13], [14], [15], [16]. Several studies have also demonstrated differences in immune responses to filarial antigens among filarial-infected travelers (expatriates) and those from filarial-endemic regions [1], [2]. Filarial-infected individuals from endemic countries, while having increased antifilarial IgG4 antibodies [17], have more profoundly diminished parasite-specific T cell responses [12], [18] than those seen in expatriates [1]. This parasite-specific hyporesponsiveness is reflected not only in diminished proliferative and cytokine responses [12], [18], [19], but also in the increased expression of molecules (e.g. CTLA-4, PD-1) known to inhibit T cell responses [20], [21]. In addition, filarial Ags and live filarial parasites have themselves been shown to induce proliferative defects [22], apoptosis of T cells [23], and impairment of antigen presenting cell number and function [24], [25], [26], that cumulatively may alter T cell responses. A number of studies have directly examined specific (or candidate) pathways in the cells of filarial-infected [24], [25] individuals. To examine more globally the differences in responsiveness to filarial infections between persons with relatively newly acquired infection and those with lifelong exposure and to evaluate more comprehensively the T cell responses (both CD4+ and CD8+) seen in these"
}